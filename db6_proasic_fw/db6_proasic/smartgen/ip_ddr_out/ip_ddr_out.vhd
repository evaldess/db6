-- Version: v11.9 SP5 11.9.5.5

library ieee;
use ieee.std_logic_1164.all;
library proasic3l;
use proasic3l.all;

entity ip_ddr_out is

    port( DataR : in    std_logic;
          DataF : in    std_logic;
          CLR   : in    std_logic;
          CLK   : in    std_logic;
          PAD   : out   std_logic
        );

end ip_ddr_out;

architecture DEF_ARCH of ip_ddr_out is 

  component OUTBUF
    port( D   : in    std_logic := 'U';
          PAD : out   std_logic
        );
  end component;

  component DDR_OUT
    port( DR  : in    std_logic := 'U';
          DF  : in    std_logic := 'U';
          CLK : in    std_logic := 'U';
          CLR : in    std_logic := 'U';
          Q   : out   std_logic
        );
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal Q, \VCC\ : std_logic;
    signal VCC_power_net1 : std_logic;

begin 

    \VCC\ <= VCC_power_net1;

    \OUTBUF[0]\ : OUTBUF
      port map(D => Q, PAD => PAD);
    
    \DDR_OUT[0]\ : DDR_OUT
      port map(DR => DataR, DF => DataF, CLK => CLK, CLR => CLR, 
        Q => Q);
    
    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.5.5
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LDP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_DDR
-- LPM_HINT:DDR_OUT_REG
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:Z:/D/Documents/PostDoc/TileCal/db6/db6_proasic_fw/db6_proasic/smartgen\ip_ddr_out
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:IS4X4M1LDP
-- SMARTGEN_PACKAGE:fg144
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- WIDTH:1
-- TYPE:
-- TRIEN_POLARITY:0
-- CLR_POLARITY:1

-- _End_Comments_

