-- Version: v11.9 SP5 11.9.5.5

library ieee;
use ieee.std_logic_1164.all;
library proasic3l;
use proasic3l.all;

entity ip_ddr_in is

    port( PAD : in    std_logic;
          CLR : in    std_logic;
          CLK : in    std_logic;
          QR  : out   std_logic;
          QF  : out   std_logic
        );

end ip_ddr_in;

architecture DEF_ARCH of ip_ddr_in is 

  component DDR_REG
    port( D   : in    std_logic := 'U';
          CLK : in    std_logic := 'U';
          CLR : in    std_logic := 'U';
          QR  : out   std_logic;
          QF  : out   std_logic
        );
  end component;

  component INBUF
    port( PAD : in    std_logic := 'U';
          Y   : out   std_logic
        );
  end component;

  component INV
    port( A : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

    signal CLRR, Y : std_logic;

begin 


    \DDR_REG[0]\ : DDR_REG
      port map(D => Y, CLK => CLK, CLR => CLRR, QR => QR, QF => 
        QF);
    
    \INBUF[0]\ : INBUF
      port map(PAD => PAD, Y => Y);
    
    CLR_INV : INV
      port map(A => CLR, Y => CLRR);
    

end DEF_ARCH; 

-- _Disclaimer: Please leave the following comments in the file, they are for internal purposes only._


-- _GEN_File_Contents_

-- Version:11.9.5.5
-- ACTGENU_CALL:1
-- BATCH:T
-- FAM:PA3LDP
-- OUTFORMAT:VHDL
-- LPMTYPE:LPM_DDR
-- LPM_HINT:DDR_REG_REG
-- INSERT_PAD:NO
-- INSERT_IOREG:NO
-- GEN_BHV_VHDL_VAL:F
-- GEN_BHV_VERILOG_VAL:F
-- MGNTIMER:F
-- MGNCMPL:T
-- DESDIR:Z:/D/Documents/PostDoc/TileCal/db6/db6_proasic_fw/db6_proasic/smartgen\ip_ddr_in
-- GEN_BEHV_MODULE:F
-- SMARTGEN_DIE:IS4X4M1LDP
-- SMARTGEN_PACKAGE:fg144
-- AGENIII_IS_SUBPROJECT_LIBERO:T
-- WIDTH:1
-- TYPE:
-- TRIEN_POLARITY:0
-- CLR_POLARITY:0

-- _End_Comments_

