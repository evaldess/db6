-- ****************************************************
-- Created by: ACTgen 11.9.6.7
-- Date : Fri Mar 05 14:41:15 2021 
-- Parameters:
--   WIDTH:32
--   DIRECTION:UP
--   CLK_EDGE:RISE
--   CLR_POLARITY:1
--   PRE_POLARITY:2
--   EN_POLARITY:2
--   LD_POLARITY:2
--   TCNT_POLARITY:2
-- ****************************************************

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ip_counter is
  port(Clock : in std_logic;
       Q : out std_logic_vector(31 downto 0);
       Aclr : in std_logic);
end ip_counter;

architecture behavioral of ip_counter is

  signal Qaux : UNSIGNED(31 downto 0);

begin

  process(Clock, Aclr)
  begin

    if (Aclr = '1') then
      Qaux <= (others => '0');
    elsif (Clock'event and Clock = '1') then
      Qaux <= Qaux + 1;
    end if;

  end process;

  Q <= std_logic_vector(Qaux);

end behavioral;
