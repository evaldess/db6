quietly set ACTELLIBNAME ProASIC3L
quietly set PROJECT_DIR "C:/Users/pirovaldes/Documents/microsemi/db6_proasic/db6_proasic"

if {[file exists postsynth/_info]} {
   echo "INFO: Simulation library postsynth already exists"
} else {
   file delete -force postsynth 
   vlib postsynth
}
vmap postsynth postsynth
vmap proasic3l "C:/apps/Microsemi/Libero_SoC_v11.9/Designer/lib/modelsim/precompiled/vhdl/proasic3l"

vcom -2008 -explicit  -work postsynth "${PROJECT_DIR}/synthesis/db6_proasic_top.vhd"

vsim -L proasic3l -L postsynth  -t 1ps postsynth.db6_proasic_top
# The following lines are commented because no testbench is associated with the project
# add wave /testbench/*
# run 1000ns
