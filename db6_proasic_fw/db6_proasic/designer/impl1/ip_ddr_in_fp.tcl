new_project \
    -name {ip_ddr_in} \
    -location {P:\TileCal\db6\db6_proasic_fw\db6_proasic\designer\impl1\ip_ddr_in_fp} \
    -mode {single}
set_programming_file -file {P:\TileCal\db6\db6_proasic_fw\db6_proasic\designer\impl1\ip_ddr_in.pdb}
set_programming_action -action {PROGRAM}
run_selected_actions
save_project
close_project
