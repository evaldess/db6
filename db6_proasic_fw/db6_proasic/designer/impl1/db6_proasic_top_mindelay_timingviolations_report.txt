Timing Violation Report Min Delay Analysis

SmartTime Version v11.9 SP6
Microsemi Corporation - Microsemi Libero Software Release v11.9 SP6 (Version 11.9.6.7)
Date: Fri Mar 12 15:36:40 2021


Design: db6_proasic_top
Family: ProASIC3L
Die: A3P250L
Package: 144 FBGA
Temperature Range: -40 - 85 C
Voltage Range: 1.14 - 1.575 V
Speed Grade: -1
Design State: Post-Layout
Data source: Silicon verified
Min Operating Conditions: BEST - 1.575 V - -40 C
Max Operating Conditions: WORST - 1.14 V - 85 C
Using Enhanced Min Delay Analysis
Scenario for Timing Analysis: Primary


No Path

