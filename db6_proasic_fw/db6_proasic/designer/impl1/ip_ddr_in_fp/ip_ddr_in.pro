<project name="ip_ddr_in" version="1.1">
    <ProjectDirectory>
        P:\TileCal\db6\db6_proasic_fw\db6_proasic\designer\impl1\ip_ddr_in_fp
    </ProjectDirectory>
    <View>
        SingleSTAPLView
    </View>
    <LiberoTargetDevice>
        
    </LiberoTargetDevice>
    <LogFile>
        P:\TileCal\db6\db6_proasic_fw\db6_proasic\designer\impl1\ip_ddr_in_fp\ip_ddr_in.log
    </LogFile>
    <SerializationOption>
        Skip
    </SerializationOption>
    <programmer status="enable" type="FlashPro4" revision="UndefRev" connection="usb2.0">
        <name>
            06403
        </name>
        <id>
            06403
        </id>
    </programmer>
    <configuration>
        <Hardware>
            <FlashPro>
                <TCK>
                    4000000
                </TCK>
                <Vpp/>
                <Vpn/>
                <Vddl/>
                <Vdd>
2500                </Vdd>
            </FlashPro>
            <FlashProLite>
                <TCK>
                    4000000
                </TCK>
                <Vpp/>
                <Vpn/>
            </FlashProLite>
            <FlashPro3>
                <TCK>
                    4000000
                </TCK>
                <Vpump/>
                <ClkMode>
                    FreeRunningClk
                </ClkMode>
            </FlashPro3>
            <FlashPro4>
                <TCK>
                    4000000
                </TCK>
                <Vpump/>
                <ClkMode>
                    FreeRunningClk
                </ClkMode>
            </FlashPro4>
            <FlashPro5>
                <TCK>
                    4000000
                </TCK>
                <Vpump/>
                <ClkMode>
                    FreeRunningClk
                </ClkMode>
                <ProgrammingMode>
                    JTAGMode
                </ProgrammingMode>
            </FlashPro5>
        </Hardware>
        <Algo type="PDB">
            <filename>
                P:\TileCal\db6\db6_proasic_fw\db6_proasic\designer\impl1\ip_ddr_in.pdb
            </filename>
            <local>
                projectData\ip_ddr_in.pdb
            </local>
            <SelectedAction>
                PROGRAM
            </SelectedAction>
        </Algo>
    </configuration>
</project>
