***************************************************************************
                               Status Report
                          Fri Mar 12 15:35:27 2021 ***************************************************************************

Product: Designer
Release: v11.9 SP6
Version: 11.9.6.7
File Name: P:\TileCal\db6\db6_proasic_fw\db6_proasic\designer\impl1\db6_proasic_top.adb
Design Name: db6_proasic_top  Design State: compile
Last Saved: Wed Mar 10 14:21:58 2021

***** Device Data **************************************************

Family: ProASIC3L  Die: A3P250L  Package: 144 FBGA
Speed: -1  Voltage: 1.2~1.5

Restrict JTAG Pins: YES
Restrict Probe Pins: YES

Junction Temperature Range:   IND
Voltage Range:   IND

***** Import Variables *********************************************

Source File(s) Imported on Fri Mar 12 15:35:25 2021:
        P:\TileCal\db6\db6_proasic_fw\db6_proasic\synthesis\db6_proasic_top.edn


***** CAE Variables ************************************************

Back Annotation File: N/A


***** Bitstream Variables ******************************************

Bitstream File: N/A
     Lock Mode: off


***** Compile Variables ********************************************

Netlist PIN properties overwrite existing properties: 0

Compile Output:
=====================================================================
Parameters used to run compile:
===============================

Family      : ProASIC3L
Device      : A3P250L
Package     : 144 FBGA
Source      : P:\TileCal\db6\db6_proasic_fw\db6_proasic\synthesis\db6_proasic_top.edn
Format      : EDIF
Topcell     : db6_proasic_top
Speed grade : -1
Temp        : -40:25:85
Voltage     : 1.58:1.20:1.14

Keep Existing Physical Constraints : Yes
Keep Existing Timing Constraints   : Yes

pdc_abort_on_error                 : Yes
pdc_eco_display_unmatched_objects  : No
pdc_eco_max_warnings               : 10000

demote_globals                     : No
promote_globals                    : No
localclock_max_shared_instances    : 12
localclock_buffer_tree_max_fanout  : 12

combine_register                   : No
delete_buffer_tree                 : No

report_high_fanout_nets_limit      : 10

=====================================================================
Compile starts ...

Warning: Top level port p_gbtx_reset_a_out is not connected to any IO pad
Warning: Top level port p_done_b_in is not connected to any IO pad
Warning: Top level port p_gbtx_reset_a_in is not connected to any IO pad
Warning: Top level port p_gbtx_reset_b_in is not connected to any IO pad
Warning: Top level port p_gbtx_cfgsel_b_in is not connected to any IO pad
Warning: Top level port p_pkbus_a_in<1> is not connected to any IO pad
Warning: Top level port p_pkbus_a_in<0> is not connected to any IO pad

Netlist Optimization Report
===========================

Optimized macros:
  - Dangling net drivers:   0
  - Buffers:                0
  - Inverters:              0
  - Tieoff:                 0
  - Logic combining:        14

    Total macros optimized  14

There were 0 error(s) and 7 warning(s) in this design.
=====================================================================

Reading previous post-compile physical placement constraints.


There were 0 error(s) and 0 warning(s).

=====================================================================
Compile report:
===============

    CORE                       Used:    757  Total:   6144   (12.32%)
    IO (W/ clocks)             Used:     45  Total:     97   (46.39%)
    Differential IO            Used:     11  Total:     24   (45.83%)
    GLOBAL (Chip+Quadrant)     Used:      3  Total:     18   (16.67%)
    PLL                        Used:      0  Total:      1   (0.00%)
    RAM/FIFO                   Used:      0  Total:      8   (0.00%)
    Low Static ICC             Used:      0  Total:      1   (0.00%)
    FlashROM                   Used:      0  Total:      1   (0.00%)
    User JTAG                  Used:      0  Total:      1   (0.00%)

Global Information:

    Type            | Used   | Total
    ----------------|--------|--------------
    Chip global     | 3      | 6  (50.00%)*
    Quadrant global | 0      | 12 (0.00%)

    (*) Chip globals may be assigned to Quadrant globals using the Multi-View Navigator (MVN)
        or Physical Design Constraints (PDC).
        They may also be assigned to Quadrant globals automatically during Layout.

Core Information:

    Type    | Instances    | Core tiles
    --------|--------------|-----------
    COMB    | 486          | 486
    SEQ     | 271          | 271

I/O Function:

    Type                                  | w/o register  | w/ register  | w/ DDR register
    --------------------------------------|---------------|--------------|----------------
    Input I/O                             | 6             | 0            | 0
    Output I/O                            | 16            | 0            | 1
    Bidirectional I/O                     | 0             | 0            | 0
    Differential Input I/O Pairs          | 11            | 0            | 0
    Differential Output I/O Pairs         | 0             | 0            | 0

I/O Technology:

                                    |   Voltages    |             I/Os
    --------------------------------|-------|-------|-------|--------|--------------
    I/O Standard(s)                 | Vcci  | Vref  | Input | Output | Bidirectional
    --------------------------------|-------|-------|-------|--------|--------------
    LVCMOS18                        | 1.80v | N/A   | 6     | 17     | 0
    LVDS                            | 2.50v | N/A   | 22    | 0      | 0

I/O Placement:

    Locked  :  42 ( 93.33% )
    Placed  :   1 (  2.22% )
    UnPlaced:   2 (  4.44% )

Warning: Only some I/Os are locked

Net information report:
=======================

The following nets have been assigned to a chip global resource:
    Fanout  Type          Name
    --------------------------
    116     CLK_NET       Net   : s_clknet.clk40_main_sm
                          Driver: i_main_sm_clkint
                          Source: NETLIST
    79      CLK_NET       Net   : p_gbtx_testclkout_in_c_c
                          Driver: p_gbtx_testclkout_in_pad
                          Source: NETLIST
    78      CLK_NET       Net   : s_gbtx_clk_a
                          Driver: s_gbtx_clk_a_inferred_clock
                          Source: NETLIST

High fanout nets in the post compile netlist:
    Fanout  Type          Name
    --------------------------
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.2.proc_main_sm.v_st_clk_bus12
                          Driver: gen_main_osc40_sm_tmr.2.v_st_clk_bus_RNIN3HG[1]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.0.proc_main_sm.v_st_clk_bus16
                          Driver: gen_main_osc40_sm_tmr.0.v_st_clk_bus_RNIJ3F1_0[1]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.1.proc_main_sm.v_st_clk_bus12
                          Driver: gen_main_osc40_sm_tmr.1.v_st_clk_bus_RNIL309_0[0]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.1.proc_main_sm.v_st_clk_bus12_0
                          Driver: gen_main_osc40_sm_tmr.1.v_st_clk_bus_RNIL309[0]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.0.proc_main_sm.v_st_clk_bus16_0
                          Driver: gen_main_osc40_sm_tmr.0.v_st_clk_bus_RNIJ3F1[1]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.2.proc_main_sm.v_st_clk_bus12_0
                          Driver: gen_main_osc40_sm_tmr.2.v_st_clk_bus_RNIN3HG_0[1]
    14      INT_NET       Net   : gen_main_gbtx_test_sm_tmr.2.proc_main_sm.v_counter_0
                          Driver: v_counter_3_RNIVV3I3[17]
    14      INT_NET       Net   : gen_main_gbtx_test_sm_tmr.0.proc_main_sm.v_counter_0
                          Driver: v_counter_4_RNIDMJ65[17]
    14      INT_NET       Net   : gen_main_gbtx_test_sm_tmr.1.proc_main_sm.v_counter_0
                          Driver: v_counter_5_RNIRC3R[17]
    14      INT_NET       Net   : gen_main_gbtx_sm_tmr.1.proc_main_sm.v_counter_0
                          Driver: v_counter_2_RNIH9KT1[17]

Nets that are candidates for clock assignment and the resulting fanout:
    Fanout  Type          Name
    --------------------------
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.2.proc_main_sm.v_st_clk_bus12
                          Driver: gen_main_osc40_sm_tmr.2.v_st_clk_bus_RNIN3HG[1]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.0.proc_main_sm.v_st_clk_bus16
                          Driver: gen_main_osc40_sm_tmr.0.v_st_clk_bus_RNIJ3F1_0[1]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.1.proc_main_sm.v_st_clk_bus12
                          Driver: gen_main_osc40_sm_tmr.1.v_st_clk_bus_RNIL309_0[0]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.1.proc_main_sm.v_st_clk_bus12_0
                          Driver: gen_main_osc40_sm_tmr.1.v_st_clk_bus_RNIL309[0]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.0.proc_main_sm.v_st_clk_bus16_0
                          Driver: gen_main_osc40_sm_tmr.0.v_st_clk_bus_RNIJ3F1[1]
    16      INT_NET       Net   : gen_main_osc40_sm_tmr.2.proc_main_sm.v_st_clk_bus12_0
                          Driver: gen_main_osc40_sm_tmr.2.v_st_clk_bus_RNIN3HG_0[1]
    14      INT_NET       Net   : gen_main_gbtx_test_sm_tmr.2.proc_main_sm.v_counter_0
                          Driver: v_counter_3_RNIVV3I3[17]
    14      INT_NET       Net   : gen_main_gbtx_test_sm_tmr.0.proc_main_sm.v_counter_0
                          Driver: v_counter_4_RNIDMJ65[17]
    14      INT_NET       Net   : gen_main_gbtx_test_sm_tmr.1.proc_main_sm.v_counter_0
                          Driver: v_counter_5_RNIRC3R[17]
    14      INT_NET       Net   : gen_main_gbtx_sm_tmr.1.proc_main_sm.v_counter_0
                          Driver: v_counter_2_RNIH9KT1[17]
====================
Flash*Freeze report:
====================

The design does not use the Flash*Freeze feature.

====================


