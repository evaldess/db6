*****************************************************************
Pin Report - Date: Fri Mar 05 15:21:10 2021 Pinchecksum: NOT-AVAILABLE
Product: Designer
Release: v11.9 SP6
Version: 11.9.6.7
Design Name: ip_ddr_in
Family: ProASIC3L
Die: A3P250L
Package: 144 FBGA
*****************************************************************

-----------------------------------------------------------------
Flash*Freeze information
        Pin name  : L3
        Pin usage : Available as a regular pin
-----------------------------------------------------------------

Port |Pin |Fixed |Function       |I/O Std  |Output Drive (mA) |Slew |Resistor Pull |Skew |Output Load (pF) |Use I/O Reg |Hot Swappable |
-----|----|------|---------------|---------|------------------|-----|--------------|-----|-----------------|------------|--------------|
CLK   E3   No     GFC1/IO110PDB3  LVCMOS18  ---                ---   None           ---   ---               No           No
CLR   A3   No     GAB0/IO02RSB0   LVCMOS18  ---                ---   None           ---   ---               No           No
PAD   E2   No     GFC0/IO110NDB3  LVCMOS18  ---                ---   None           ---   ---               Yes          No
QF    F1   No     GFB0/IO109NPB3  LVCMOS18  8                  High  None           No    5                 No           No
QR    F3   No     GFB1/IO109PPB3  LVCMOS18  8                  High  None           No    5                 No           No
