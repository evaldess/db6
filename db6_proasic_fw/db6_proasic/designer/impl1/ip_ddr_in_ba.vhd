-- Version: v11.9 SP6 11.9.6.7
-- File used only for Simulation

library ieee;
use ieee.std_logic_1164.all;
library proasic3l;
use proasic3l.all;

entity ip_ddr_in is

    port( PAD : in    std_logic;
          CLR : in    std_logic;
          CLK : in    std_logic;
          QR  : out   std_logic;
          QF  : out   std_logic
        );

end ip_ddr_in;

architecture DEF_ARCH of ip_ddr_in is 

  component ULSICC_INT
    port( USTDBY : in    std_logic := 'U';
          LPENA  : in    std_logic := 'U'
        );
  end component;

  component IOPAD_IN
    port( PAD : in    std_logic := 'U';
          Y   : out   std_logic
        );
  end component;

  component IOPAD_TRI
    port( D   : in    std_logic := 'U';
          E   : in    std_logic := 'U';
          PAD : out   std_logic
        );
  end component;

  component INV
    port( A : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component IOIN_ID
    port( CLR  : in    std_logic := 'U';
          ICLK : in    std_logic := 'U';
          YIN  : in    std_logic := 'U';
          YR   : out   std_logic;
          YF   : out   std_logic
        );
  end component;

  component IOTRI_OB_EB
    port( D    : in    std_logic := 'U';
          E    : in    std_logic := 'U';
          DOUT : out   std_logic;
          EOUT : out   std_logic
        );
  end component;

  component IOIN_IB
    port( YIN : in    std_logic := 'U';
          Y   : out   std_logic
        );
  end component;

  component GND
    port(Y : out std_logic); 
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal CLRR, CLR_c, CLK_c, QR_c, QF_c, actel_gnd_net, \VCC\, 
        \QF_pad/U0/NET1\, \QF_pad/U0/NET2\, 
        \\\\\INBUF[0]\\\\/U0/NET1\, \CLK_pad/U0/NET1\, 
        \QR_pad/U0/NET1\, \QR_pad/U0/NET2\, \CLR_pad/U0/NET1\, 
        AFLSDF_VCC, AFLSDF_GND : std_logic;
    signal GND_power_net1 : std_logic;
    signal VCC_power_net1 : std_logic;

begin 

    actel_gnd_net <= GND_power_net1;
    AFLSDF_GND <= GND_power_net1;
    \VCC\ <= VCC_power_net1;
    AFLSDF_VCC <= VCC_power_net1;

    INT_ULSICC_INSTANCE_0 : ULSICC_INT
      port map(USTDBY => actel_gnd_net, LPENA => actel_gnd_net);
    
    \CLR_pad/U0/U0\ : IOPAD_IN
      port map(PAD => CLR, Y => \CLR_pad/U0/NET1\);
    
    \QR_pad/U0/U0\ : IOPAD_TRI
      port map(D => \QR_pad/U0/NET1\, E => \QR_pad/U0/NET2\, PAD
         => QR);
    
    \QF_pad/U0/U0\ : IOPAD_TRI
      port map(D => \QF_pad/U0/NET1\, E => \QF_pad/U0/NET2\, PAD
         => QF);
    
    CLR_INV : INV
      port map(A => CLR_c, Y => CLRR);
    
    \_INBUF[0]_/U0/U1\ : IOIN_ID
      port map(CLR => CLRR, ICLK => CLK_c, YIN => 
        \\\\\INBUF[0]\\\\/U0/NET1\, YR => QR_c, YF => QF_c);
    
    \CLK_pad/U0/U0\ : IOPAD_IN
      port map(PAD => CLK, Y => \CLK_pad/U0/NET1\);
    
    \\\\\INBUF[0]\\\\/U0/U0\ : IOPAD_IN
      port map(PAD => PAD, Y => \\\\\INBUF[0]\\\\/U0/NET1\);
    
    \QF_pad/U0/U1\ : IOTRI_OB_EB
      port map(D => QF_c, E => \VCC\, DOUT => \QF_pad/U0/NET1\, 
        EOUT => \QF_pad/U0/NET2\);
    
    \QR_pad/U0/U1\ : IOTRI_OB_EB
      port map(D => QR_c, E => \VCC\, DOUT => \QR_pad/U0/NET1\, 
        EOUT => \QR_pad/U0/NET2\);
    
    \CLK_pad/U0/U1\ : IOIN_IB
      port map(YIN => \CLK_pad/U0/NET1\, Y => CLK_c);
    
    \CLR_pad/U0/U1\ : IOIN_IB
      port map(YIN => \CLR_pad/U0/NET1\, Y => CLR_c);
    
    GND_power_inst1 : GND
      port map( Y => GND_power_net1);

    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 
