# Top Level Design Parameters

# Clocks

create_clock -name {db6_proasic_top|p_clk_osc_in} -period 10.000000 -waveform {0.000000 5.000000} p_clk_osc_in
create_clock -name {db6_proasic_top|p_gbtx_testclkout_in} -period 10.000000 -waveform {0.000000 5.000000} p_gbtx_testclkout_in

# False Paths Between Clocks


# False Path Constraints


# Maximum Delay Constraints


# Multicycle Constraints


# Virtual Clocks
# Output Load Constraints
# Driving Cell Constraints
# Wire Loads
# set_wire_load_mode top

# Other Constraints
