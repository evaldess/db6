--------------------------------------------------------------------------------
-- Company: Stockholm University
--
-- File: db6_proasic_pkbus.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::ProASIC3L> <Die::A3P250L> <Package::144 FBGA>
-- Author: Eduardo Valdes Santurio eduardo.valdes@fysik.su.se, eduardo.valdes@cern.ch, pirovaldes@gmail.com
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity db6_proasic_pkbus is
port (
    p_clk_osc_in : IN  std_logic;

    p_pkbus_a_in : in std_logic_vector(1 downto 0);
    p_pkbus_b_in : in std_logic_vector(1 downto 0);

    p_pkbus_a_out : out std_logic_vector(1 downto 0);
    p_pkbus_b_out : out std_logic_vector(1 downto 0)
);
end db6_proasic_pkbus;
architecture architecture_db6_proasic_pkbus of db6_proasic_pkbus is
   -- signal, component etc. declarations
	--signal signal_name1 : std_logic; -- example
	--signal signal_name2 : std_logic_vector(1 downto 0) ; -- example
signal s_pkbus_clk_a_in, s_pkbus_clk_a_out, s_pkbus_data_a_in, s_pkbus_data_a_out: std_logic;
signal s_pkbus_clk_b_in, s_pkbus_clk_b_out, s_pkbus_data_b_in, s_pkbus_data_b_out: std_logic;

signal s_word_a_buf, s_word_b_buf, s_word_a, s_word_b : std_logic_vector(31 downto 0);
begin

   -- architecture body
    
    s_pkbus_clk_a_in <= p_pkbus_a_in(0);
    s_pkbus_data_a_in <= p_pkbus_a_in(1);

    s_pkbus_clk_b_in <= p_pkbus_b_in(0);
    s_pkbus_data_b_in <= p_pkbus_b_in(1);

    
    s_pkbus_clk_a_out <= p_clk_osc_in;
    p_pkbus_a_out(0) <= '0';-- s_pkbus_clk_a_out;
    p_pkbus_a_out(1) <= not s_pkbus_clk_a_out;
    
    s_pkbus_clk_b_out <= p_clk_osc_in;
    p_pkbus_b_out(0) <= '0';--s_pkbus_clk_b_out;
    p_pkbus_b_out(1) <= not s_pkbus_clk_b_out;


proc_input_pineline_a : process(s_pkbus_clk_a_in)
begin
    if rising_edge(s_pkbus_clk_a_in) then

        s_word_a_buf<=s_word_a_buf(30 downto 0) & s_pkbus_data_a_in;

    end if;
end process;

proc_input_pineline_b : process(s_pkbus_clk_b_in)
begin
    if rising_edge(s_pkbus_clk_b_in) then

        s_word_a_buf<=s_word_b_buf(30 downto 0) & s_pkbus_data_b_in;

    end if;
end process;

end architecture_db6_proasic_pkbus;
