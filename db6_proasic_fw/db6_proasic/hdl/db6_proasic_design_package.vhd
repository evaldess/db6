--------------------------------------------------------------------------------
-- Company: stockholm university
--
-- File: db6_proasic_design_package.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <library design package for db6 proasic3 firmware>
--
-- Targeted device: Family::ProASIC3L Die::A3P250L Package::144 FBGA
-- Author: Eduardo Valdes Santurio eduardo.valdes@fysik.su.se, eduardo.valdes@cern.ch, pirovaldes@gmail.com
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

package db6_proasic_design_package is

-- constants

    constant c_zero_tmr : std_logic_vector(2 downto 0) := "000";
    constant c_one_tmr : std_logic_vector(2 downto 0) := "111";
    constant c_key_all : std_logic_vector(31 downto 0):= x"5ab5abcf";
    constant c_key_a : std_logic_vector(31 downto 0):= x"5a5aabcf";
    constant c_key_b : std_logic_vector(31 downto 0):= x"5b5babcf";

    type t_key_tmr is array (0 to 2) of std_logic_vector(31 downto 0);
    constant c_key_all_tmr : t_key_tmr := (c_key_all,c_key_all,c_key_all);
    constant c_key_a_tmr : t_key_tmr := (c_key_a,c_key_a,c_key_a);
    constant c_key_b_tmr : t_key_tmr := (c_key_b,c_key_b,c_key_b);



-- new code for proasic register arrays
    constant c_number_of_proasic_regs : integer := 2 + 1;
    type t_db_proasic_reg is array (integer range 0 to c_number_of_proasic_regs-1) of std_logic_vector(15 downto 0);
    type t_db_proasic_reg_lut is array (integer range 0 to c_number_of_proasic_regs-1) of std_logic_vector(7 downto 0);

    -- names of proasic registers
    
        constant proasic_register_zero          : integer := 0;
        constant proasic_fw_version          : integer := 1;
        constant proasic_gbtx_state        : integer := 2;

    constant c_db_proasic_reg_lut : t_db_proasic_reg_lut := (
        x"00", --proasic_register_zero,
        x"01", --proasic_fw_version,
        x"02" --proasic_gbtx_state
        );


end db6_proasic_design_package;