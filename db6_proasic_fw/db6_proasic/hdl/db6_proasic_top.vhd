--------------------------------------------------------------------------------
-- Company: Stockholm University
--
-- File: db6_proasic_top.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::ProASIC3L> <Die::A3P250L> <Package::144 FBGA>
-- Author: Eduardo Valdes Santurio eduardo.valeds@fysik.su.se, eduardo.valdes@cern.ch, pirovaldes@gmail.com
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

library proasic3;
use proasic3.all;

library tilecal;
use tilecal.all;
use tilecal.db6_proasic_design_package.all;

use IEEE.NUMERIC_STD.ALL;

entity db6_proasic_top is
port (
    --clocks
	p_clk_osc_in : IN  std_logic; -- example
    
    --1.5v
    p_gbtx_rxrdy_in : in std_logic; --std_logic_vector(1 downto 0)  -- example
    p_gbtx_rxrdy_out : out std_logic; --std_logic_vector(1 downto 0)  -- example
    p_gbtx_datavalid_in : in std_logic; --std_logic_vector(1 downto 0)  -- example
    p_gbtx_datavalid_out : out std_logic; --std_logic_vector(1 downto 0)  -- example
    p_gbtx_testclkout_out : out std_logic;
    p_gbtx_testclkout_in : in std_logic;
    p_gbtx_reset_a_out : inout std_logic;
    p_gbtx_cfgsel_a_out : inout std_logic;
    p_done_b_in : in std_logic;

    --1.8V
    p_gbtx_reset_a_in : in std_logic;
    p_gbtx_cfgsel_a_in : in std_logic;
    p_gbtx_reset_b_in : in std_logic;
    p_gbtx_cfgsel_b_in : in std_logic;

    
    --ldvs
    p_gbtx_tms_b_p_in : in std_logic;
    p_gbtx_tms_b_n_in : in std_logic;
    p_gbtx_tms_a_p_in : in std_logic;
    p_gbtx_tms_a_n_in : in std_logic;

    p_gbtx_tdi_b_p_in : in std_logic;
    p_gbtx_tdi_b_n_in : in std_logic;
    p_gbtx_tdi_a_p_in : in std_logic;
    p_gbtx_tdi_a_n_in : in std_logic;

    p_gbtx_tck_b_p_in : in std_logic;
    p_gbtx_tck_b_n_in : in std_logic;
    p_gbtx_tck_a_p_in : in std_logic;
    p_gbtx_tck_a_n_in : in std_logic;

    p_gbtx_reset_b_p_in : in std_logic;
    p_gbtx_reset_b_n_in : in std_logic;
    p_gbtx_reset_a_p_in : in std_logic;
    p_gbtx_reset_a_n_in : in std_logic;

    p_gbtx_data_a_p_in : in std_logic_vector(1 downto 0);
    p_gbtx_data_a_n_in : in std_logic_vector(1 downto 0);
    
    p_gbtx_clk_a_p_in : in std_logic;
    p_gbtx_clk_a_n_in : in std_logic;

    --2.5v
    p_tck_a_out : inout std_logic;
    p_tms_a_out : inout std_logic;
    p_tdi_a_out : inout std_logic;
    
    p_reset_a_out : inout std_logic;

    p_tck_b_out : inout std_logic;
    p_tms_b_out : inout std_logic;
    p_tdi_b_out : inout std_logic;

    p_reset_b_out : inout std_logic;
    p_done_a_in : in std_logic;
    p_done_a_out : out std_logic;

    p_pkbus_a_in : in std_logic_vector(1 downto 0);
    --p_pkbus_b_in : in std_logic_vector(1 downto 0);

    p_pkbus_a_out : out std_logic_vector(1 downto 0);
    --p_pkbus_b_out : out std_logic_vector(1 downto 0);

    p_leds_out : out std_logic_vector(1 downto 0)
    
);
end db6_proasic_top;

architecture architecture_db6_proasic_top of db6_proasic_top is


   -- signal, component etc. declarations
	--signal s_clk40_osc, s_clk40_gbtx : std_logic_vector(2 downto 0);
	--signal signal_name2 : std_logic_vector(1 downto 0) ; -- example

    type t_leds_tmr is array (0 to 1) of std_logic_vector(2 downto 0);
    signal s_leds : t_leds_tmr;

    --type t_std_logic_tmr is array (0 to 2) of std_logic;
    --signal s_a_buf_enable_tmr, s_b_buf_enable_tmr : t_std_logic_tmr;
    signal s_a_buf_enable_tmr, s_b_buf_enable_tmr : std_logic_vector(2 downto 0);
    signal s_a_buf_enable, s_b_buf_enable : std_logic;
    

    signal s_gbtx_data_a: std_logic_vector(1 downto 0);

    --signal s_leds : std_logic_vector( downto 0);

    component inbuf_lvds
        port(   padp : in    std_logic := 'U';
                padn : in    std_logic := 'U';
                y    : out   std_logic
        );
    end component;

    --component tribuff_lvcmos25
        --port(   d    : in    std_logic := 'U';
                --e : in    std_logic := 'U';
                --pad : out   std_logic
        --);
    --end component;

    component clkbuf
    port (PAD : in std_logic;
          Y : out std_logic);
    end component;

    component CLKINT
    port (A : in std_logic;
          Y : out std_logic);
    end component;

    component pll_osc
        port( POWERDOWN : in    std_logic;
              CLKA      : in    std_logic;
              LOCK      : out   std_logic;
              GLA       : out   std_logic;
              GLB       : out   std_logic;
              YB        : out   std_logic;
              GLC       : out   std_logic;
              YC        : out   std_logic
            );

    end component;

    type t_pll is record
        POWERDOWN : std_logic;
        CLKA      : std_logic;
        LOCK      : std_logic;
        GLA       : std_logic;
        GLB       : std_logic;
        YB        : std_logic;
        GLC       : std_logic;
        YC        : std_logic;
    end record; 
    
    signal s_pll : t_pll;

    component ip_ddr_out

        port( DataR : in    std_logic;
              DataF : in    std_logic;
              CLR   : in    std_logic;
              CLK   : in    std_logic;
              PAD   : out   std_logic
            );

    end component;

    component ip_ddr_in is

        port( PAD : in    std_logic;
              CLR : in    std_logic;
              CLK : in    std_logic;
              QR  : out   std_logic;
              QF  : out   std_logic
            );

    end component;

    type t_ddr is record
        DataR :     std_logic;
        DataF :     std_logic;
        CLR_in   :     std_logic;
        CLK_in   :     std_logic;
        CLR_out   :     std_logic;
        CLK_out   :     std_logic;
        QR  : std_logic;
        QF  : std_logic;
        --PAD   :     std_logic;

    end record;

    component ip_counter is

        port( Aclr  : in    std_logic;
              Clock : in    std_logic;
              Q     : out   std_logic_vector(31 downto 0)
            );

    end component;
    
    --type t_ip_counter is record
--
        --Aclr  :     std_logic;
        --Clock :     std_logic;
        --Q     :     std_logic_vector(31 downto 0)
        --);
--
    --end record;

    --type t_ip_counter_tmr is array (0 to 2) of t_ip_counter;
    --signal s_ip_counter_tmr_led_1 : t_ip_counter;
    --signal s_ip_counter_tmr_led_2 : t_ip_counter;

    signal s_pkbus_ddr_a, s_pkbus_ddr_b : t_ddr;

    signal s_pkbus_ddr_a_datar_tmr, s_pkbus_ddr_a_dataf_tmr, s_pkbus_ddr_b_datar_tmr, s_pkbus_ddr_b_dataf_tmr : std_logic_vector(2 downto 0);

    signal s_tms_a, s_tms_b, s_tdi_a, s_tdi_b, s_tck_a, s_tck_b, s_reset_a, s_reset_b : std_logic := '1';
    signal s_tms_a_from_ds, s_tms_b_from_ds, s_tdi_a_from_ds, s_tdi_b_from_ds, s_tck_a_from_ds, s_tck_b_from_ds, s_reset_a_from_ds, s_reset_b_from_ds : std_logic := '1';
    signal s_tms_a_tmr, s_tms_b_tmr, s_tdi_a_tmr, s_tdi_b_tmr, s_tck_a_tmr, s_tck_b_tmr, s_reset_a_tmr, s_reset_b_tmr : std_logic_vector(2 downto 0) := "111";
    signal s_gbtx_clk_a : std_logic := '1';

    --signal s_osc_clk_a : std_logic := '1';
    type t_clknet is record
        clk40_osc : std_logic;
        clk40_main_sm : std_logic;
        clk40_pkbus : std_logic;

    end record;
    signal s_clknet : t_clknet;


    type t_counter_tmr is array (0 to 2) of integer;
    signal s_gbtx_clk_counter, s_osc_clk_counter, s_gbtx_test_clk_counter : t_counter_tmr := (0,0,0);

    type t_db_proasic_reg_tmr is array (0 to 2) of t_db_proasic_reg;
    signal s_db_proasic_reg_tmr : t_db_proasic_reg_tmr;
    type t_number_of_proasic_regs_tmr is array (0 to 2) of integer;
    constant  c_number_of_proasic_regs_tmr : t_number_of_proasic_regs_tmr := (c_number_of_proasic_regs,c_number_of_proasic_regs,c_number_of_proasic_regs);

begin

p_gbtx_rxrdy_out <= p_gbtx_rxrdy_in;
p_gbtx_datavalid_out <= 'Z' when p_gbtx_rxrdy_in = '1' else p_gbtx_datavalid_in;
-- controls the buffers

--i_osc_clkbuf : clkbuf port map (PAD => p_clk_osc_in, Y => s_clknet.clk40_osc);
--s_clknet.clk40_osc <= p_clk_osc_in;

i_main_sm_clkint : clkint port map ( A => p_clk_osc_in, Y => s_clknet.clk40_main_sm);
--s_clknet.clk40_main_sm <= s_clknet.clk40_osc;
--i_pkbus_clkint : clkint port map ( A => s_clknet.clk40_osc, Y => s_clknet.clk40_pkbus);
s_clknet.clk40_pkbus <= s_clknet.clk40_osc;

gen_buff_tmr : for tmr in 0 to 2 generate
    s_tms_a_tmr(tmr) <= s_tms_a_from_ds;
    s_tdi_a_tmr(tmr) <= s_tdi_a_from_ds;
    s_tck_a_tmr(tmr) <= s_tck_a_from_ds;
    s_reset_a_tmr(tmr) <= s_reset_a_from_ds;

    s_tms_b_tmr(tmr) <= s_tms_b_from_ds;
    s_tdi_b_tmr(tmr) <= s_tdi_b_from_ds;
    s_tck_b_tmr(tmr) <= s_tck_b_from_ds;
    s_reset_b_tmr(tmr) <= s_reset_b_from_ds;
end generate;


p_gbtx_testclkout_out <= p_gbtx_testclkout_in;

s_tms_a <= (s_tms_a_tmr(0) or s_tms_a_tmr(1)) and (s_tms_a_tmr(1) or s_tms_a_tmr(2)) and (s_tms_a_tmr(2) or s_tms_a_tmr(0));
s_tdi_a <= (s_tdi_a_tmr(0) or s_tdi_a_tmr(1)) and (s_tdi_a_tmr(1) or s_tdi_a_tmr(2)) and (s_tdi_a_tmr(2) or s_tdi_a_tmr(0));
s_tck_a <= (s_tck_a_tmr(0) or s_tck_a_tmr(1)) and (s_tck_a_tmr(1) or s_tck_a_tmr(2)) and (s_tck_a_tmr(2) or s_tck_a_tmr(0));
s_reset_a <= (s_reset_a_tmr(0) or s_reset_a_tmr(1)) and (s_reset_a_tmr(1) or s_reset_a_tmr(2)) and (s_reset_a_tmr(2) or s_reset_a_tmr(0));

s_tms_b <= (s_tms_b_tmr(0) or s_tms_b_tmr(1)) and (s_tms_b_tmr(1) or s_tms_b_tmr(2)) and (s_tms_b_tmr(2) or s_tms_b_tmr(0));
s_tdi_b <= (s_tdi_b_tmr(0) or s_tdi_b_tmr(1)) and (s_tdi_b_tmr(1) or s_tdi_b_tmr(2)) and (s_tdi_b_tmr(2) or s_tdi_b_tmr(0));
s_tck_b <= (s_tck_b_tmr(0) or s_tck_b_tmr(1)) and (s_tck_b_tmr(1) or s_tck_b_tmr(2)) and (s_tck_b_tmr(2) or s_tck_b_tmr(0));
s_reset_b <= (s_reset_b_tmr(0) or s_reset_b_tmr(1)) and (s_reset_b_tmr(1) or s_reset_b_tmr(2)) and (s_reset_b_tmr(2) or s_reset_b_tmr(0));

p_tms_a_out <= 'Z' when (s_a_buf_enable = '0') else s_tms_a;
p_tdi_a_out <= 'Z' when (s_a_buf_enable = '0') else s_tdi_a;
p_tck_a_out <= 'Z' when (s_a_buf_enable = '0') else s_tck_a;
p_reset_a_out <= 'Z' when (s_a_buf_enable = '0') else s_reset_a;

p_tms_b_out <= 'Z' when (s_b_buf_enable = '0') else s_tms_b;
p_tdi_b_out <= 'Z' when (s_b_buf_enable = '0') else s_tdi_b;
p_tck_b_out <= 'Z' when (s_b_buf_enable = '0') else s_tck_b;
p_reset_b_out <= 'Z' when (s_b_buf_enable = '0') else s_reset_b;

--testing gbtx reset
--p_gbtx_reset_a_out <= 'Z' when (p_done_a_in = '0') else p_gbtx_reset_a_in;
p_gbtx_reset_a_out <= 'Z';

--testing gbtx cfgsel
p_gbtx_cfgsel_a_out <= 'Z' when (p_done_a_in = '0') else p_gbtx_cfgsel_a_in;
--p_gbtx_cfgsel_a_out <= 'Z';

--p_gbtx_reset_b_out <= 'Z' when (p_done_b_in = '0') else p_gbtx_reset_b_in;
--p_gbtx_cfgsel_b_out <= 'Z' when (p_done_b_in = '0') else p_gbtx_cfgsel_b_in;

p_done_a_out <= p_done_a_in;

--i_tms_a_outbuf : tribuff_lvcmos25
--    port map(d => s_tms_a, e => s_a_buf_enable, pad => p_tms_a_out);

--lvds to se
i_tms_a_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_tms_a_p_in, padn => p_gbtx_tms_a_n_in, y => s_tms_a_from_ds);
i_tck_a_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_tck_a_p_in, padn => p_gbtx_tck_a_n_in, y => s_tck_a_from_ds);
i_tdi_a_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_tdi_a_p_in, padn => p_gbtx_tdi_a_n_in, y => s_tdi_a_from_ds);
i_reset_a_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_reset_a_p_in, padn => p_gbtx_reset_a_n_in, y => s_reset_a_from_ds);

i_tms_b_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_tms_b_p_in, padn => p_gbtx_tms_b_n_in, y => s_tms_b_from_ds);
i_tck_b_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_tck_b_p_in, padn => p_gbtx_tck_b_n_in, y => s_tck_b_from_ds);
i_tdi_b_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_tdi_b_p_in, padn => p_gbtx_tdi_b_n_in, y => s_tdi_b_from_ds);
i_reset_b_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_reset_b_p_in, padn => p_gbtx_reset_b_n_in, y => s_reset_b_from_ds);

i_gbtx_clk_a_inbuf_lvds: inbuf_lvds
    port map(padp => p_gbtx_clk_a_p_in, padn => p_gbtx_clk_a_n_in, y => s_gbtx_clk_a);

gen_gbtx_data_buffs : for i in 0 to 1 generate 
i_gbtx_data_a_inbuf_lvds : inbuf_lvds
    port map(padp => p_gbtx_data_a_p_in(i), padn => p_gbtx_data_a_n_in(i), y => s_gbtx_data_a(i));
end generate;

gen_leds : for i in 0 to 1 generate 
    p_leds_out(i) <= (s_leds(i)(0) or s_leds(i)(1)) and (s_leds(i)(1) or s_leds(i)(2)) and (s_leds(i)(2) or s_leds(i)(0));
    --p_leds_out(i) <= (s_leds(i)(0));
end generate;

s_a_buf_enable<= (s_a_buf_enable_tmr(0) or s_a_buf_enable_tmr(1)) and (s_a_buf_enable_tmr(1) or s_a_buf_enable_tmr(2)) and (s_a_buf_enable_tmr(2) or s_a_buf_enable_tmr(0));
s_b_buf_enable<= (s_b_buf_enable_tmr(0) or s_b_buf_enable_tmr(1)) and (s_b_buf_enable_tmr(1) or s_b_buf_enable_tmr(2)) and (s_b_buf_enable_tmr(2) or s_b_buf_enable_tmr(0));



--
gen_main_gbtx_sm_tmr : for tmr in 0 to 2 generate
    --s_clk40_gbtx(tmr) <= s_gbtx_clk_a;
    
    --proc_main_sm : process(s_clk40_gbtx(tmr))
    proc_main_sm : process(s_gbtx_clk_a)
    variable v_counter : integer := 0;
    variable v_counter_us : integer := 0;
    begin
        --if rising_edge(s_clk40_gbtx(tmr)) then
        if rising_edge(s_gbtx_clk_a) then
            s_gbtx_clk_counter(tmr)<=v_counter;
            if v_counter < 4000000 then
                v_counter := v_counter +1;
            else
                v_counter_us:=v_counter_us+1;
                if v_counter_us < 10 then
                    v_counter_us:=v_counter_us+1;
                else
                    --every second... 1Hz...
                    v_counter_us:=0;
                    --s_leds(1)(tmr) <= not s_leds(1)(tmr);
                end if;

                v_counter := 0;
                --s_leds(0)(tmr) <= not s_leds(0)(tmr);
                --s_leds(1)(tmr) <= not s_leds(1)(tmr);

            end if;
        end if;

    end process;
end generate;

gen_main_gbtx_test_sm_tmr : for tmr in 0 to 2 generate
    --s_clk40_gbtx(tmr) <= s_gbtx_clk_a;
    
    --proc_main_sm : process(s_clk40_gbtx(tmr))
    proc_main_sm : process(p_gbtx_testclkout_in)
    variable v_counter, v_counter_us : integer := 0;
    begin
        --if rising_edge(s_clk40_gbtx(tmr)) then
        if rising_edge(p_gbtx_testclkout_in) then
            s_gbtx_test_clk_counter(tmr)<=v_counter;
            s_osc_clk_counter(tmr)<=v_counter;
            if v_counter < 4000000 then
                v_counter := v_counter +1;
            else
                v_counter_us:=v_counter_us+1;
                if v_counter_us < 10 then
                    v_counter_us:=v_counter_us+1;
                else
                    --every second... 1Hz...
                    v_counter_us:=0;
                    s_leds(1)(tmr) <= not s_leds(1)(tmr);
                end if;

                v_counter := 0;
                --s_leds(0)(tmr) <= not s_leds(0)(tmr);
                --s_leds(1)(tmr) <= not s_leds(1)(tmr);

            end if;
        end if;

    end process;
end generate;


s_pkbus_ddr_a.clk_out <= s_clknet.clk40_main_sm;
s_pkbus_ddr_a.datar<= (s_pkbus_ddr_a_datar_tmr(0) or s_pkbus_ddr_a_datar_tmr(1)) and (s_pkbus_ddr_a_datar_tmr(1) or s_pkbus_ddr_a_datar_tmr(2)) and (s_pkbus_ddr_a_datar_tmr(2) or s_pkbus_ddr_a_datar_tmr(0));
s_pkbus_ddr_a.dataf<= (s_pkbus_ddr_a_dataf_tmr(0) or s_pkbus_ddr_a_dataf_tmr(1)) and (s_pkbus_ddr_a_dataf_tmr(1) or s_pkbus_ddr_a_dataf_tmr(2)) and (s_pkbus_ddr_a_dataf_tmr(2) or s_pkbus_ddr_a_dataf_tmr(0));
s_pkbus_ddr_a.clr_out <= (c_zero_tmr(0) or c_zero_tmr(1)) and (c_zero_tmr(1) or c_zero_tmr(2)) and (c_zero_tmr(2) or c_zero_tmr(0));

--s_pkbus_ddr_b.clk_out <= s_clknet.clk40_main_sm;
--s_pkbus_ddr_b.datar<= (s_pkbus_ddr_b_datar_tmr(0) or s_pkbus_ddr_b_datar_tmr(1)) and (s_pkbus_ddr_b_datar_tmr(1) or s_pkbus_ddr_b_datar_tmr(2)) and (s_pkbus_ddr_b_datar_tmr(2) or s_pkbus_ddr_b_datar_tmr(0));
--s_pkbus_ddr_b.dataf<= (s_pkbus_ddr_b_dataf_tmr(0) or s_pkbus_ddr_b_dataf_tmr(1)) and (s_pkbus_ddr_b_dataf_tmr(1) or s_pkbus_ddr_b_dataf_tmr(2)) and (s_pkbus_ddr_b_dataf_tmr(2) or s_pkbus_ddr_b_dataf_tmr(0));
--s_pkbus_ddr_b.clr_out <= (c_zero_tmr(0) or c_zero_tmr(1)) and (c_zero_tmr(1) or c_zero_tmr(2)) and (c_zero_tmr(2) or c_zero_tmr(0));


--p_pkbus_a_out(1) <= s_clknet.clk40_main_sm;
--p_pkbus_b_out(1) <= s_clknet.clk40_main_sm;

    i_pkbus_a_ddr_out : ip_ddr_out

        port map( 
              DataR => s_pkbus_ddr_a.datar,
              DataF => s_pkbus_ddr_a.dataf,
              CLR => s_pkbus_ddr_a.clr_out,
              CLK => s_pkbus_ddr_a.clk_out,
              PAD => p_pkbus_a_out(0)
            );


--    i_pkbus_b_ddr_out : ip_ddr_out

--        port map( 
--              DataR => s_pkbus_ddr_b.datar,
--              DataF => s_pkbus_ddr_b.dataf,
--              CLR => s_pkbus_ddr_b.clr_out,
--              CLK => s_pkbus_ddr_b.clk_out,
--              PAD => p_pkbus_b_out(0)
--            );


    

    i_pkbus_a_ddr_in : ip_ddr_in
        port map ( PAD => p_pkbus_a_in(0),
              CLR => s_pkbus_ddr_a.clr_in,
              CLK => s_pkbus_ddr_a.clk_in,
              QR  => s_pkbus_ddr_a.qr,
              QF  => s_pkbus_ddr_a.qf
            );

--    i_pkbus_b_ddr_in : ip_ddr_in
--        port map ( PAD => p_pkbus_b_in(0),
--              CLR => s_pkbus_ddr_b.clr_in,
--              CLK => s_pkbus_ddr_b.clk_in,
--              QR  => s_pkbus_ddr_b.qr,
--              QF  => s_pkbus_ddr_b.qf
--           );

s_pkbus_ddr_a.clr_in <= (c_zero_tmr(0) or c_zero_tmr(1)) and (c_zero_tmr(1) or c_zero_tmr(2)) and (c_zero_tmr(2) or c_zero_tmr(0));
--s_pkbus_ddr_b.clr_in <= (c_zero_tmr(0) or c_zero_tmr(1)) and (c_zero_tmr(1) or c_zero_tmr(2)) and (c_zero_tmr(2) or c_zero_tmr(0));
s_pkbus_ddr_a.clk_in <= p_pkbus_a_in(1);
--s_pkbus_ddr_b.clk_in <= p_pkbus_b_in(1);

--i_pll_osc : pll_osc
--    port map( 
--          POWERDOWN => s_pll.powerdown,
--          CLKA  => s_pll.clka,
--          LOCK  => s_pll.lock,
--          GLA   => s_pll.GLA,
--          GLB   => s_pll.GLB,
--          YB    => s_pll.YB,
--          GLC   => s_pll.GLC,
--          YC    => s_pll.YC
--        );

--s_pll.POWERDOWN <= (c_zero_tmr(0) or c_zero_tmr(1)) and (c_zero_tmr(1) or c_zero_tmr(2)) and (c_zero_tmr(2) or c_zero_tmr(0));
--s_pll.CLKA      <= s_clknet.clk40_osc;


gen_main_osc40_sm_tmr : for tmr in 0 to 2 generate
    --s_clk40_osc(tmr) <= p_clk_osc_in;
--    i_ip_counter : ip_counter 
        --port_map 
            --( Aclr  => s_ip_counter_tmr_led_1(tmr).Aclr,
              --Clock => s_ip_counter_tmr_led_1(tmr).Clock,
              --Q     =>s_ip_counter_tmr_led_1(tmr).Q
--            );
--    s_ip_counter_tmr_led_1(tmr).Clock <= s_clknet.clk40_main_sm;

    --proc_main_sm : process(s_clk40_osc(tmr))
    proc_main_sm : process(s_clknet.clk40_main_sm)
    variable v_counter : integer := 0;

    type t_sm_clk_bus is (st_rising_edge, st_high, st_falling_edge, st_low);
    variable v_st_clk_bus : t_sm_clk_bus := st_high;
    variable v_sm_clk_buffer, v_sm_clk : std_logic :='1';

    type t_sm_key is (st_locked, st_unlock_a, st_unlock_b, st_unlock_all);
    variable v_st_key : t_sm_key := st_locked;
    variable v_key_buffer : std_logic_vector(31 downto 0);



--    variable v_st_clk_pkbus_rx_a, v_st_clk_pkbus_rx_b : t_sm_clk_bus := st_high;
--    variable v_sm_clk_pkbus_buffer_a, v_sm_clk_pkbus_buffer_b, v_sm_clk_pkbus_a, v_sm_clk_pkbus_b : std_logic :='1';
    
--pkbux rx
    type t_sm_pkbus_rx is (st_init, st_sync, st_check, st_locked);
    variable v_sm_pkbus_a_rx, v_sm_pkbus_b_rx : t_sm_pkbus_rx;

    variable v_pkbus_a_rx_frameclk, v_pkbus_a_rx_data, v_pkbus_b_rx_frameclk, v_pkbus_b_rx_data : std_logic_vector(31 downto 0);
    variable v_counter_pkbus_clk_a_rx, v_counter_pkbus_sync_a_rx, v_counter_pkbus_clk_b_rx, v_counter_pkbus_sync_b_rx : integer;

--pkbux tx
    type t_sm_pkbus_tx is (st_init, st_locked);
    variable v_sm_pkbus_a_tx, v_sm_pkbus_b_tx : t_sm_pkbus_tx;
    variable v_counter_pkbus_clk_a_tx, v_counter_pkbus_reg_a_tx, v_counter_pkbus_clk_b_tx, v_counter_pkbus_reg_b_tx : integer;
    constant c_pkbus_frameclk : std_logic_vector(31 downto 0) := (x"FFFF0000" or x"FFFF0000") and (x"FFFF0000" or x"FFFF0000") and (x"FFFF0000" or x"FFFF0000");
    variable v_pkbus_reg_buffer_a_tx, v_pkbus_reg_buffer_b_tx : std_logic_vector(31 downto 0);
    variable v_counter_us : integer :=0;

    begin

        --if rising_edge(s_clk40_osc(tmr)) then
        if rising_edge(s_clknet.clk40_main_sm) then


            s_osc_clk_counter(tmr)<=v_counter;
            if v_counter < 4000000 then
                v_counter := v_counter +1;
            else
                v_counter_us:=v_counter_us+1;
                if v_counter_us < 10 then
                    v_counter_us:=v_counter_us+1;
                else
                    --every second... 1Hz...
                    v_counter_us:=0;
                    s_leds(0)(tmr) <= not s_leds(0)(tmr);
                end if;

                v_counter := 0;
                --s_leds(0)(tmr) <= not s_leds(0)(tmr);
                --s_leds(1)(tmr) <= not s_leds(1)(tmr);

            end if;

            v_sm_clk_buffer:=v_sm_clk;
            v_sm_clk:=s_gbtx_data_a(0);

            v_pkbus_a_rx_frameclk := v_pkbus_a_rx_frameclk(30 downto 0) & s_pkbus_ddr_a.qr;
            v_pkbus_a_rx_data := v_pkbus_a_rx_data(30 downto 0) & s_pkbus_ddr_a.qf;

            --v_pkbus_b_rx_frameclk := v_pkbus_b_rx_frameclk(30 downto 0) & s_pkbus_ddr_b.qr;
            --v_pkbus_b_rx_data := v_pkbus_b_rx_data(30 downto 0) & s_pkbus_ddr_b.qf;


            case v_sm_pkbus_a_rx is

                when st_init =>
                    v_counter_pkbus_clk_a_rx:=0;
                    v_counter_pkbus_sync_a_rx:=0;
                    if v_pkbus_a_rx_frameclk = x"FFFF0000" then
                        v_sm_pkbus_a_rx:=st_sync;
                    else
                        v_sm_pkbus_a_rx:=st_init;
                        --v_counter_pkbus_clk_a_rx:=v_counter_pkbus_clk_a_rx+1;
                    end if;

                when st_sync =>
                    if v_counter_pkbus_clk_a_rx = 31 and v_pkbus_a_rx_frameclk = x"FFFF0000" then
                        v_counter_pkbus_clk_a_rx :=0;
                        if v_counter_pkbus_sync_a_rx < 8 then
                            v_counter_pkbus_sync_a_rx:=v_counter_pkbus_sync_a_rx+1;
                        else
                            v_sm_pkbus_a_rx:=st_check;
                        end if;
                    else
                        v_counter_pkbus_clk_a_rx:=v_counter_pkbus_clk_a_rx+1;
                    end if;

                when st_check =>
                    if v_counter_pkbus_clk_a_rx<31 then
                        v_counter_pkbus_clk_a_rx:=v_counter_pkbus_clk_a_rx+1;
                    else
                        v_counter_pkbus_clk_a_rx:=0;
                        if v_pkbus_a_rx_frameclk = x"FFFF0000" then
                            if v_counter_pkbus_sync_a_rx < 15 then
                                v_counter_pkbus_sync_a_rx:=v_counter_pkbus_sync_a_rx+1;
                            else
                                v_sm_pkbus_a_rx:=st_locked;
                            end if;
                        else
                            v_sm_pkbus_a_rx:=st_init;
                        end if;
                    end if;

                when st_locked =>
                    if v_counter_pkbus_clk_a_rx<31 then
                        v_counter_pkbus_clk_a_rx:=v_counter_pkbus_clk_a_rx+1;
                    else
                        v_counter_pkbus_clk_a_rx:=0;
                        if v_pkbus_a_rx_frameclk = x"FFFF0000" then
                            if v_pkbus_a_rx_data(31 downto 24) = x"F0" then
                                s_db_proasic_reg_tmr(tmr)(to_integer(unsigned(v_pkbus_a_rx_data(23 downto 16))))<=v_pkbus_a_rx_data(15 downto 0);
                            end if;
                        else
                            if v_counter_pkbus_sync_a_rx > 0 then
                                v_counter_pkbus_sync_a_rx:=v_counter_pkbus_sync_a_rx-1;
                            else
                                v_sm_pkbus_a_rx:=st_init;
                            end if;
                        end if;


                    end if;
                when others =>
                    v_sm_pkbus_a_rx:=st_init;
            end case;

            case v_sm_pkbus_a_tx is
                when st_init=>
                    v_counter_pkbus_clk_a_tx :=31;
                    v_pkbus_reg_buffer_a_tx:= x"F0" & std_logic_vector(to_unsigned(0,8)) & s_db_proasic_reg_tmr(tmr)(0)(15 downto 0);
                    v_counter_pkbus_reg_a_tx  :=0+1;
                    
                when st_locked=>
                    s_pkbus_ddr_a_datar_tmr(tmr) <= c_pkbus_frameclk(v_counter_pkbus_clk_a_tx);
                    s_pkbus_ddr_a_dataf_tmr(tmr) <= v_pkbus_reg_buffer_a_tx(v_counter_pkbus_clk_a_tx);

                    if v_counter_pkbus_clk_a_tx>0 then
                        v_counter_pkbus_clk_a_tx:=v_counter_pkbus_clk_a_tx-1;
                    else
                        v_counter_pkbus_clk_a_tx:=31;
                        v_pkbus_reg_buffer_a_tx:= x"F0" & std_logic_vector(to_unsigned(v_counter_pkbus_reg_a_tx,8)) & s_db_proasic_reg_tmr(tmr)(0)(15 downto 0);
                        if v_counter_pkbus_reg_a_tx<c_number_of_proasic_regs then
                            v_counter_pkbus_reg_a_tx:=v_counter_pkbus_reg_a_tx+1;
                        else
                            v_counter_pkbus_reg_a_tx:=0;
                        end if;
                    end if;

                when others=>
                    v_sm_pkbus_a_tx:=st_init;
            end case;



            case v_st_key is
                when st_locked=>
                    s_a_buf_enable_tmr(tmr)<= '0';
                    s_b_buf_enable_tmr(tmr)<= '0';

                    if v_key_buffer = c_key_all_tmr(tmr) then
                        v_st_key:=st_unlock_all;
                    elsif v_key_buffer = c_key_a_tmr(tmr) then
                        v_st_key:=st_unlock_a;
                    elsif v_key_buffer = c_key_b_tmr(tmr) then
                        v_st_key:=st_unlock_b;
                    else
                        v_st_key:=st_locked;
                    end if;

                when st_unlock_all=>
                    s_a_buf_enable_tmr(tmr)<= '1';
                    s_b_buf_enable_tmr(tmr)<= '1';
                    if v_key_buffer(0) = '1' then
                        v_st_key:=st_unlock_all;
                    else
                        v_st_key:=st_locked;
                    end if;

                when st_unlock_a=>
                    s_a_buf_enable_tmr(tmr)<= '1';
                    s_b_buf_enable_tmr(tmr)<= '0';
                    if v_key_buffer(0) = '1' then
                        v_st_key:=st_unlock_a;
                    else
                        v_st_key:=st_locked;
                    end if;

                when st_unlock_b=>
                    s_a_buf_enable_tmr(tmr)<= '0';
                    s_b_buf_enable_tmr(tmr)<= '1';
                    if v_key_buffer(0) = '1' then
                        v_st_key:=st_unlock_b;
                    else
                        v_st_key:=st_locked;
                    end if;

                when others =>
                    v_st_key:=st_locked;

            end case;


            --v_sm_clk_pkbus_buffer_a:=v_sm_clk_pkbus_a;
            --v_sm_clk_pkbus_a:=p_pkbus_a_in(0);
            --s_pkbus_a_out_tmr(0)(tmr)<=p_pkbus_a_in(0);
--
            --case v_st_clk_pkbus_rx_a is
                --when st_rising_edge=>
                    --if v_sm_clk_pkbus_a='0' then
                        --v_st_clk_pkbus_rx_a:=st_falling_edge;
                    --else
                        --v_st_clk_pkbus_rx_a:=st_high;
                    --end if;
--
                --when st_high=>
                    --if (v_sm_clk_pkbus_a='0') and (v_sm_clk_pkbus_buffer_a='1') then
                        --v_st_clk_pkbus_rx_a:=st_falling_edge;
                    --else
                        --v_st_clk_pkbus_rx_a:=st_high;
                    --end if;
                    --
                --when st_falling_edge=>
                    --if v_sm_clk_pkbus_a='1' then
                        --v_st_clk_pkbus_rx_a:=st_rising_edge;
                    --else
                        --v_st_clk_pkbus_rx_a:=st_low;
                    --end if;
--
                --when st_low=>
                    --if (v_sm_clk_pkbus_a='1') and (v_sm_clk_pkbus_buffer_a='0') then
                        --v_st_clk_pkbus_rx_a:=st_falling_edge;
                    --else
                        --v_st_clk_pkbus_rx_a:=st_high;
                    --end if;
                    --
            --end case;
--
--
            --v_sm_clk_pkbus_buffer_b:=v_sm_clk_pkbus_b;
            --v_sm_clk_pkbus_b:=p_pkbus_b_in(0);
            --s_pkbus_b_out_tmr(0)(tmr)<=p_pkbus_b_in(0);
--
            --case v_st_clk_pkbus_rx_b is
                --when st_rising_edge=>
                    --if v_sm_clk_pkbus_b='0' then
                        --v_st_clk_pkbus_rx_b:=st_falling_edge;
                    --else
                        --v_st_clk_pkbus_rx_b:=st_high;
                    --end if;
--
                --when st_high=>
                    --if (v_sm_clk_pkbus_b='0') and (v_sm_clk_pkbus_buffer_b='1') then
                        --v_st_clk_pkbus_rx_b:=st_falling_edge;
                    --else
                        --v_st_clk_pkbus_rx_b:=st_high;
                    --end if;
                    --
                --when st_falling_edge=>
                    --if v_sm_clk_pkbus_b='1' then
                        --v_st_clk_pkbus_rx_b:=st_rising_edge;
                    --else
                        --v_st_clk_pkbus_rx_b:=st_low;
                    --end if;
--
                --when st_low=>
                    --if (v_sm_clk_pkbus_b='1') and (v_sm_clk_pkbus_buffer_b='0') then
                        --v_st_clk_pkbus_rx_b:=st_falling_edge;
                    --else
                        --v_st_clk_pkbus_rx_b:=st_high;
                    --end if;
                    --
            --end case;
--
--

            case v_st_clk_bus is
                when st_rising_edge=>
                    if v_sm_clk='0' then
                        v_st_clk_bus:=st_falling_edge;
                    else
                        v_st_clk_bus:=st_high;
                    end if;

                    v_key_buffer:=v_key_buffer(30 downto 0) & s_gbtx_data_a(1);

                when st_high=>
                    if (v_sm_clk='0') and (v_sm_clk_buffer='1') then
                        v_st_clk_bus:=st_falling_edge;
                    else
                        v_st_clk_bus:=st_high;
                    end if;
                    
                when st_falling_edge=>
                    if v_sm_clk='1' then
                        v_st_clk_bus:=st_rising_edge;
                    else
                        v_st_clk_bus:=st_low;
                    end if;

                when st_low=>
                    if (v_sm_clk='1') and (v_sm_clk_buffer='0') then
                        v_st_clk_bus:=st_falling_edge;
                    else
                        v_st_clk_bus:=st_high;
                    end if;
                    
            end case;



        end if;

    end process;
end generate;



end architecture_db6_proasic_top;
