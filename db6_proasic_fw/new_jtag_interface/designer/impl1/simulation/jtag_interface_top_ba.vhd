-- Version: v11.9 SP5 11.9.5.5
-- File used only for Simulation

library ieee;
use ieee.std_logic_1164.all;
library proasic3;
use proasic3.all;

entity PLLBA is

    port( CLKA      : in    std_logic;
          EXTFB     : in    std_logic;
          POWERDOWN : in    std_logic;
          GLA       : out   std_logic;
          LOCK      : out   std_logic;
          GLB       : out   std_logic;
          YB        : out   std_logic;
          GLC       : out   std_logic;
          YC        : out   std_logic;
          OADIV0    : in    std_logic;
          OADIV1    : in    std_logic;
          OADIV2    : in    std_logic;
          OADIV3    : in    std_logic;
          OADIV4    : in    std_logic;
          OAMUX0    : in    std_logic;
          OAMUX1    : in    std_logic;
          OAMUX2    : in    std_logic;
          DLYGLA0   : in    std_logic;
          DLYGLA1   : in    std_logic;
          DLYGLA2   : in    std_logic;
          DLYGLA3   : in    std_logic;
          DLYGLA4   : in    std_logic;
          OBDIV0    : in    std_logic;
          OBDIV1    : in    std_logic;
          OBDIV2    : in    std_logic;
          OBDIV3    : in    std_logic;
          OBDIV4    : in    std_logic;
          OBMUX0    : in    std_logic;
          OBMUX1    : in    std_logic;
          OBMUX2    : in    std_logic;
          DLYYB0    : in    std_logic;
          DLYYB1    : in    std_logic;
          DLYYB2    : in    std_logic;
          DLYYB3    : in    std_logic;
          DLYYB4    : in    std_logic;
          DLYGLB0   : in    std_logic;
          DLYGLB1   : in    std_logic;
          DLYGLB2   : in    std_logic;
          DLYGLB3   : in    std_logic;
          DLYGLB4   : in    std_logic;
          OCDIV0    : in    std_logic;
          OCDIV1    : in    std_logic;
          OCDIV2    : in    std_logic;
          OCDIV3    : in    std_logic;
          OCDIV4    : in    std_logic;
          OCMUX0    : in    std_logic;
          OCMUX1    : in    std_logic;
          OCMUX2    : in    std_logic;
          DLYYC0    : in    std_logic;
          DLYYC1    : in    std_logic;
          DLYYC2    : in    std_logic;
          DLYYC3    : in    std_logic;
          DLYYC4    : in    std_logic;
          DLYGLC0   : in    std_logic;
          DLYGLC1   : in    std_logic;
          DLYGLC2   : in    std_logic;
          DLYGLC3   : in    std_logic;
          DLYGLC4   : in    std_logic;
          FINDIV0   : in    std_logic;
          FINDIV1   : in    std_logic;
          FINDIV2   : in    std_logic;
          FINDIV3   : in    std_logic;
          FINDIV4   : in    std_logic;
          FINDIV5   : in    std_logic;
          FINDIV6   : in    std_logic;
          FBDIV0    : in    std_logic;
          FBDIV1    : in    std_logic;
          FBDIV2    : in    std_logic;
          FBDIV3    : in    std_logic;
          FBDIV4    : in    std_logic;
          FBDIV5    : in    std_logic;
          FBDIV6    : in    std_logic;
          FBDLY0    : in    std_logic;
          FBDLY1    : in    std_logic;
          FBDLY2    : in    std_logic;
          FBDLY3    : in    std_logic;
          FBDLY4    : in    std_logic;
          FBSEL0    : in    std_logic;
          FBSEL1    : in    std_logic;
          XDLYSEL   : in    std_logic;
          VCOSEL0   : in    std_logic;
          VCOSEL1   : in    std_logic;
          VCOSEL2   : in    std_logic
        );

end PLLBA;

architecture DEF_ARCH of PLLBA is 

  component PLL_SDF
    generic (VCOFREQUENCY:real := 0.0);

    port( CLKA      : in    std_logic := 'U';
          EXTFB     : in    std_logic := 'U';
          POWERDOWN : in    std_logic := 'U';
          GLAOUT    : out   std_logic;
          LOCKOUT   : out   std_logic;
          GLBOUT    : out   std_logic;
          YBOUT     : out   std_logic;
          GLCOUT    : out   std_logic;
          YCOUT     : out   std_logic;
          OADIV0    : in    std_logic := 'U';
          OADIV1    : in    std_logic := 'U';
          OADIV2    : in    std_logic := 'U';
          OADIV3    : in    std_logic := 'U';
          OADIV4    : in    std_logic := 'U';
          OAMUX0    : in    std_logic := 'U';
          OAMUX1    : in    std_logic := 'U';
          OAMUX2    : in    std_logic := 'U';
          DLYGLA0   : in    std_logic := 'U';
          DLYGLA1   : in    std_logic := 'U';
          DLYGLA2   : in    std_logic := 'U';
          DLYGLA3   : in    std_logic := 'U';
          DLYGLA4   : in    std_logic := 'U';
          OBDIV0    : in    std_logic := 'U';
          OBDIV1    : in    std_logic := 'U';
          OBDIV2    : in    std_logic := 'U';
          OBDIV3    : in    std_logic := 'U';
          OBDIV4    : in    std_logic := 'U';
          OBMUX0    : in    std_logic := 'U';
          OBMUX1    : in    std_logic := 'U';
          OBMUX2    : in    std_logic := 'U';
          DLYYB0    : in    std_logic := 'U';
          DLYYB1    : in    std_logic := 'U';
          DLYYB2    : in    std_logic := 'U';
          DLYYB3    : in    std_logic := 'U';
          DLYYB4    : in    std_logic := 'U';
          DLYGLB0   : in    std_logic := 'U';
          DLYGLB1   : in    std_logic := 'U';
          DLYGLB2   : in    std_logic := 'U';
          DLYGLB3   : in    std_logic := 'U';
          DLYGLB4   : in    std_logic := 'U';
          OCDIV0    : in    std_logic := 'U';
          OCDIV1    : in    std_logic := 'U';
          OCDIV2    : in    std_logic := 'U';
          OCDIV3    : in    std_logic := 'U';
          OCDIV4    : in    std_logic := 'U';
          OCMUX0    : in    std_logic := 'U';
          OCMUX1    : in    std_logic := 'U';
          OCMUX2    : in    std_logic := 'U';
          DLYYC0    : in    std_logic := 'U';
          DLYYC1    : in    std_logic := 'U';
          DLYYC2    : in    std_logic := 'U';
          DLYYC3    : in    std_logic := 'U';
          DLYYC4    : in    std_logic := 'U';
          DLYGLC0   : in    std_logic := 'U';
          DLYGLC1   : in    std_logic := 'U';
          DLYGLC2   : in    std_logic := 'U';
          DLYGLC3   : in    std_logic := 'U';
          DLYGLC4   : in    std_logic := 'U';
          FINDIV0   : in    std_logic := 'U';
          FINDIV1   : in    std_logic := 'U';
          FINDIV2   : in    std_logic := 'U';
          FINDIV3   : in    std_logic := 'U';
          FINDIV4   : in    std_logic := 'U';
          FINDIV5   : in    std_logic := 'U';
          FINDIV6   : in    std_logic := 'U';
          FBDIV0    : in    std_logic := 'U';
          FBDIV1    : in    std_logic := 'U';
          FBDIV2    : in    std_logic := 'U';
          FBDIV3    : in    std_logic := 'U';
          FBDIV4    : in    std_logic := 'U';
          FBDIV5    : in    std_logic := 'U';
          FBDIV6    : in    std_logic := 'U';
          FBDLY0    : in    std_logic := 'U';
          FBDLY1    : in    std_logic := 'U';
          FBDLY2    : in    std_logic := 'U';
          FBDLY3    : in    std_logic := 'U';
          FBDLY4    : in    std_logic := 'U';
          FBSEL0    : in    std_logic := 'U';
          FBSEL1    : in    std_logic := 'U';
          XDLYSEL   : in    std_logic := 'U';
          VCOSEL0   : in    std_logic := 'U';
          VCOSEL1   : in    std_logic := 'U';
          VCOSEL2   : in    std_logic := 'U';
          INTFB     : in    std_logic := 'U';
          VCOOUT    : out   std_logic
        );
  end component;

  component PLL_DLY_SDF
    generic (VCOFREQUENCY:real := 0.0);

    port( GLA      : out   std_logic;
          LOCK     : out   std_logic;
          GLB      : out   std_logic;
          YB       : out   std_logic;
          GLC      : out   std_logic;
          YC       : out   std_logic;
          GLAIN    : in    std_logic := 'U';
          LOCKIN   : in    std_logic := 'U';
          GLBIN    : in    std_logic := 'U';
          YBIN     : in    std_logic := 'U';
          GLCIN    : in    std_logic := 'U';
          YCIN     : in    std_logic := 'U';
          EXTFBOUT : out   std_logic;
          EXTFBIN  : in    std_logic := 'U';
          VCOIN    : in    std_logic := 'U';
          PLLOUT   : out   std_logic
        );
  end component;

  component GND
    port(Y : out std_logic); 
  end component;

    signal \GND\, SDOUT, FB, EXTFBDLY, GLADLY, LOCKDLY, GLBDLY, 
        YBDLY, GLCDLY, YCDLY, VCODLY : std_logic;
    signal GND_power_net1 : std_logic;

begin 

    \GND\ <= GND_power_net1;

    pll_sdf_0 : PLL_SDF
      generic map(VCOFREQUENCY => 240.0)

      port map(CLKA => CLKA, EXTFB => EXTFBDLY, POWERDOWN => 
        POWERDOWN, GLAOUT => GLADLY, LOCKOUT => LOCKDLY, GLBOUT
         => GLBDLY, YBOUT => YBDLY, GLCOUT => GLCDLY, YCOUT => 
        YCDLY, OADIV0 => OADIV0, OADIV1 => OADIV1, OADIV2 => 
        OADIV2, OADIV3 => OADIV3, OADIV4 => OADIV4, OAMUX0 => 
        OAMUX0, OAMUX1 => OAMUX1, OAMUX2 => OAMUX2, DLYGLA0 => 
        DLYGLA0, DLYGLA1 => DLYGLA1, DLYGLA2 => DLYGLA2, DLYGLA3
         => DLYGLA3, DLYGLA4 => DLYGLA4, OBDIV0 => OBDIV0, OBDIV1
         => OBDIV1, OBDIV2 => OBDIV2, OBDIV3 => OBDIV3, OBDIV4
         => OBDIV4, OBMUX0 => OBMUX0, OBMUX1 => OBMUX1, OBMUX2
         => OBMUX2, DLYYB0 => DLYYB0, DLYYB1 => DLYYB1, DLYYB2
         => DLYYB2, DLYYB3 => DLYYB3, DLYYB4 => DLYYB4, DLYGLB0
         => DLYGLB0, DLYGLB1 => DLYGLB1, DLYGLB2 => DLYGLB2, 
        DLYGLB3 => DLYGLB3, DLYGLB4 => DLYGLB4, OCDIV0 => OCDIV0, 
        OCDIV1 => OCDIV1, OCDIV2 => OCDIV2, OCDIV3 => OCDIV3, 
        OCDIV4 => OCDIV4, OCMUX0 => OCMUX0, OCMUX1 => OCMUX1, 
        OCMUX2 => OCMUX2, DLYYC0 => DLYYC0, DLYYC1 => DLYYC1, 
        DLYYC2 => DLYYC2, DLYYC3 => DLYYC3, DLYYC4 => DLYYC4, 
        DLYGLC0 => DLYGLC0, DLYGLC1 => DLYGLC1, DLYGLC2 => 
        DLYGLC2, DLYGLC3 => DLYGLC3, DLYGLC4 => DLYGLC4, FINDIV0
         => FINDIV0, FINDIV1 => FINDIV1, FINDIV2 => FINDIV2, 
        FINDIV3 => FINDIV3, FINDIV4 => FINDIV4, FINDIV5 => 
        FINDIV5, FINDIV6 => FINDIV6, FBDIV0 => FBDIV0, FBDIV1 => 
        FBDIV1, FBDIV2 => FBDIV2, FBDIV3 => FBDIV3, FBDIV4 => 
        FBDIV4, FBDIV5 => FBDIV5, FBDIV6 => FBDIV6, FBDLY0 => 
        FBDLY0, FBDLY1 => FBDLY1, FBDLY2 => FBDLY2, FBDLY3 => 
        FBDLY3, FBDLY4 => FBDLY4, FBSEL0 => FBSEL0, FBSEL1 => 
        FBSEL1, XDLYSEL => XDLYSEL, VCOSEL0 => VCOSEL0, VCOSEL1
         => VCOSEL1, VCOSEL2 => VCOSEL2, INTFB => FB, VCOOUT => 
        VCODLY);
    
    pll_dly_sdf_0 : PLL_DLY_SDF
      generic map(VCOFREQUENCY => 240.0)

      port map(GLA => GLA, LOCK => LOCK, GLB => GLB, YB => YB, 
        GLC => GLC, YC => YC, GLAIN => GLADLY, LOCKIN => LOCKDLY, 
        GLBIN => GLBDLY, YBIN => YBDLY, GLCIN => GLCDLY, YCIN => 
        YCDLY, EXTFBOUT => EXTFBDLY, EXTFBIN => EXTFB, VCOIN => 
        VCODLY, PLLOUT => FB);
    
    GND_power_inst1 : GND
      port map( Y => GND_power_net1);


end DEF_ARCH; 

library ieee;
use ieee.std_logic_1164.all;
library proasic3;
use proasic3.all;

entity jtag_interface_top is

    port( gbt_tms_p     : in    std_logic_vector(1 downto 0);
          gbt_tdi_p     : in    std_logic_vector(1 downto 0);
          gbt_tck_p     : in    std_logic_vector(1 downto 0);
          gbt_rst_p     : in    std_logic_vector(1 downto 0);
          gbt_tms_n     : in    std_logic_vector(1 downto 0);
          gbt_tdi_n     : in    std_logic_vector(1 downto 0);
          gbt_tck_n     : in    std_logic_vector(1 downto 0);
          gbt_rst_n     : in    std_logic_vector(1 downto 0);
          proasic_tms   : out   std_logic_vector(1 downto 0);
          proasic_tdi   : out   std_logic_vector(1 downto 0);
          proasic_tck   : out   std_logic_vector(1 downto 0);
          proasic_rst   : out   std_logic_vector(1 downto 0);
          gbt_rxrdy     : in    std_logic;
          gbt_dvalid    : in    std_logic;
          gbt_key_a_p   : in    std_logic;
          gbt_key_a_n   : in    std_logic;
          gbt_key_b_p   : in    std_logic;
          gbt_key_b_n   : in    std_logic;
          gbt_clk_sig_p : in    std_logic;
          gbt_clk_sig_n : in    std_logic;
          gbt_clk40     : in    std_logic;
          osc_clk40     : in    std_logic
        );

end jtag_interface_top;

architecture DEF_ARCH of jtag_interface_top is 

  component AND3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component IOPADN_IN
    port( PAD    : in    std_logic := 'U';
          N2POUT : out   std_logic
        );
  end component;

  component DFN0
    port( D   : in    std_logic := 'U';
          CLK : in    std_logic := 'U';
          Q   : out   std_logic
        );
  end component;

  component IOIN_IB
    port( YIN : in    std_logic := 'U';
          Y   : out   std_logic
        );
  end component;

  component XOR2
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AO1D
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component IOPAD_TRI
    port( D   : in    std_logic := 'U';
          E   : in    std_logic := 'U';
          PAD : out   std_logic
        );
  end component;

  component DFN1
    port( D   : in    std_logic := 'U';
          CLK : in    std_logic := 'U';
          Q   : out   std_logic
        );
  end component;

  component IOPADP_IN
    port( N2PIN : in    std_logic := 'U';
          PAD   : in    std_logic := 'U';
          Y     : out   std_logic
        );
  end component;

  component AND3B
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AO1E
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component IOTRI_OB_EB
    port( D    : in    std_logic := 'U';
          E    : in    std_logic := 'U';
          DOUT : out   std_logic;
          EOUT : out   std_logic
        );
  end component;

  component NAND3B
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NOR3B
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component DFN1E1
    port( D   : in    std_logic := 'U';
          CLK : in    std_logic := 'U';
          E   : in    std_logic := 'U';
          Q   : out   std_logic
        );
  end component;

  component MAJ3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component OA1C
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NOR2A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NAND2B
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component OA1A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND2B
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component INV
    port( A : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AX1C
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component IOPAD_IN
    port( PAD : in    std_logic := 'U';
          Y   : out   std_logic
        );
  end component;

  component AOI1C
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component PLLBA
    port( CLKA      : in    std_logic := 'U';
          EXTFB     : in    std_logic := 'U';
          POWERDOWN : in    std_logic := 'U';
          GLA       : out   std_logic;
          LOCK      : out   std_logic;
          GLB       : out   std_logic;
          YB        : out   std_logic;
          GLC       : out   std_logic;
          YC        : out   std_logic;
          OADIV0    : in    std_logic := 'U';
          OADIV1    : in    std_logic := 'U';
          OADIV2    : in    std_logic := 'U';
          OADIV3    : in    std_logic := 'U';
          OADIV4    : in    std_logic := 'U';
          OAMUX0    : in    std_logic := 'U';
          OAMUX1    : in    std_logic := 'U';
          OAMUX2    : in    std_logic := 'U';
          DLYGLA0   : in    std_logic := 'U';
          DLYGLA1   : in    std_logic := 'U';
          DLYGLA2   : in    std_logic := 'U';
          DLYGLA3   : in    std_logic := 'U';
          DLYGLA4   : in    std_logic := 'U';
          OBDIV0    : in    std_logic := 'U';
          OBDIV1    : in    std_logic := 'U';
          OBDIV2    : in    std_logic := 'U';
          OBDIV3    : in    std_logic := 'U';
          OBDIV4    : in    std_logic := 'U';
          OBMUX0    : in    std_logic := 'U';
          OBMUX1    : in    std_logic := 'U';
          OBMUX2    : in    std_logic := 'U';
          DLYYB0    : in    std_logic := 'U';
          DLYYB1    : in    std_logic := 'U';
          DLYYB2    : in    std_logic := 'U';
          DLYYB3    : in    std_logic := 'U';
          DLYYB4    : in    std_logic := 'U';
          DLYGLB0   : in    std_logic := 'U';
          DLYGLB1   : in    std_logic := 'U';
          DLYGLB2   : in    std_logic := 'U';
          DLYGLB3   : in    std_logic := 'U';
          DLYGLB4   : in    std_logic := 'U';
          OCDIV0    : in    std_logic := 'U';
          OCDIV1    : in    std_logic := 'U';
          OCDIV2    : in    std_logic := 'U';
          OCDIV3    : in    std_logic := 'U';
          OCDIV4    : in    std_logic := 'U';
          OCMUX0    : in    std_logic := 'U';
          OCMUX1    : in    std_logic := 'U';
          OCMUX2    : in    std_logic := 'U';
          DLYYC0    : in    std_logic := 'U';
          DLYYC1    : in    std_logic := 'U';
          DLYYC2    : in    std_logic := 'U';
          DLYYC3    : in    std_logic := 'U';
          DLYYC4    : in    std_logic := 'U';
          DLYGLC0   : in    std_logic := 'U';
          DLYGLC1   : in    std_logic := 'U';
          DLYGLC2   : in    std_logic := 'U';
          DLYGLC3   : in    std_logic := 'U';
          DLYGLC4   : in    std_logic := 'U';
          FINDIV0   : in    std_logic := 'U';
          FINDIV1   : in    std_logic := 'U';
          FINDIV2   : in    std_logic := 'U';
          FINDIV3   : in    std_logic := 'U';
          FINDIV4   : in    std_logic := 'U';
          FINDIV5   : in    std_logic := 'U';
          FINDIV6   : in    std_logic := 'U';
          FBDIV0    : in    std_logic := 'U';
          FBDIV1    : in    std_logic := 'U';
          FBDIV2    : in    std_logic := 'U';
          FBDIV3    : in    std_logic := 'U';
          FBDIV4    : in    std_logic := 'U';
          FBDIV5    : in    std_logic := 'U';
          FBDIV6    : in    std_logic := 'U';
          FBDLY0    : in    std_logic := 'U';
          FBDLY1    : in    std_logic := 'U';
          FBDLY2    : in    std_logic := 'U';
          FBDLY3    : in    std_logic := 'U';
          FBDLY4    : in    std_logic := 'U';
          FBSEL0    : in    std_logic := 'U';
          FBSEL1    : in    std_logic := 'U';
          XDLYSEL   : in    std_logic := 'U';
          VCOSEL0   : in    std_logic := 'U';
          VCOSEL1   : in    std_logic := 'U';
          VCOSEL2   : in    std_logic := 'U'
        );
  end component;

  component OR2A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NAND3
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AO1C
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NAND3C
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component NAND3A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component OR3B
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AND3A
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component AO1
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          C : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component MX2C
    port( A : in    std_logic := 'U';
          B : in    std_logic := 'U';
          S : in    std_logic := 'U';
          Y : out   std_logic
        );
  end component;

  component GND
    port(Y : out std_logic); 
  end component;

  component VCC
    port(Y : out std_logic); 
  end component;

    signal \jtag_loc[0]\, \jtag_loc[1]\, \jtag_loc[2]\, 
        \jtag_loc[3]\, \jtag_rem[0]\, \jtag_rem[1]\, 
        \jtag_rem[2]\, \jtag_rem[3]\, key_a, key_b, gbt_clk_sig, 
        GLB, GLA, \jtag_out_loc[0]_net_1\, 
        \jtag_out_rem[0]_net_1\, \jtag_out_loc[1]_net_1\, 
        \jtag_out_rem[1]_net_1\, \jtag_out_loc[2]_net_1\, 
        \jtag_out_rem[2]_net_1\, \jtag_out_loc[3]_net_1\, 
        \jtag_out_rem[3]_net_1\, \jtag_out_rem_tri_enable\, 
        gbt_rxrdy_c, gbt_dvalid_c, gbt_clk40_c, osc_clk40_c, 
        \GND\, \VCC\, \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/U2_N2P\, 
        \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/NET1\, 
        \proasic_rst_pad[0]/U0/NET1\, 
        \proasic_rst_pad[0]/U0/NET2\, 
        \proasic_tms_pad[0]/U0/NET1\, 
        \proasic_tms_pad[0]/U0/NET2\, \gbt_clk40_pad/U0/NET1\, 
        \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/U2_N2P\, 
        \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/NET1\, 
        \proasic_tdi_pad[1]/U0/NET1\, 
        \proasic_tdi_pad[1]/U0/NET2\, 
        \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/U2_N2P\, 
        \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/NET1\, 
        \proasic_tdi_pad[0]/U0/NET1\, 
        \proasic_tdi_pad[0]/U0/NET2\, 
        \proasic_rst_pad[1]/U0/NET1\, 
        \proasic_rst_pad[1]/U0/NET2\, 
        \proasic_tck_pad[0]/U0/NET1\, 
        \proasic_tck_pad[0]/U0/NET2\, 
        \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/U2_N2P\, 
        \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/NET1\, 
        \proasic_tck_pad[1]/U0/NET1\, 
        \proasic_tck_pad[1]/U0/NET2\, \clksig/U0/U2_N2P\, 
        \clksig/U0/NET1\, 
        \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/U2_N2P\, 
        \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/NET1\, 
        \gbt_rxrdy_pad/U0/NET1\, 
        \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/U2_N2P\, 
        \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/NET1\, 
        \osc_clk40_pad/U0/NET1\, 
        \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/U2_N2P\, 
        \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/NET1\, 
        \keya/U0/U2_N2P\, \keya/U0/NET1\, 
        \gbt_dvalid_pad/U0/NET1\, \proasic_tms_pad[1]/U0/NET1\, 
        \proasic_tms_pad[1]/U0/NET2\, 
        \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/U2_N2P\, 
        \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/NET1\, 
        \keyb/U0/U2_N2P\, \keyb/U0/NET1\, \clock_ok_tmr[0]\, 
        \clock_ok_tmr[1]\, \clock_ok_tmr[2]\, 
        \fc0/HIEFFPLA_NET_1_2375\, \fc0/HIEFFPLA_NET_1_2376\, 
        \fc0/HIEFFPLA_NET_1_2377\, \fc0/HIEFFPLA_NET_1_2378\, 
        \fc0/I_5\, \fc0/I_7\, \fc0/I_9\, \fc0/N_2\, 
        \fc0/counter[0]_net_1\, \fc0/counter[1]_net_1\, 
        \fc0/counter[2]_net_1\, \fc0/counter[3]_net_1\, 
        \fc0/final_count[0]_net_1\, \fc0/final_count[1]_net_1\, 
        \fc0/final_count[2]_net_1\, \fc0/final_count[3]_net_1\, 
        \fc0/state_net_1\, \fc0/un3_rxready_0\, 
        \fc0/un3_rxready_1\, \fc0/un6_rxreadylt3\, 
        \fc0/un7_rxready\, \fc0.counter_0_sqmuxa\, 
        \fc1/HIEFFPLA_NET_1_2353\, \fc1/HIEFFPLA_NET_1_2355\, 
        \fc1/I_5_0\, \fc1/I_7_0\, \fc1/I_9_0\, \fc1/N_2\, 
        \fc1/counter[0]_net_1\, \fc1/counter[1]_net_1\, 
        \fc1/counter[2]_net_1\, \fc1/counter[3]_net_1\, 
        \fc1/counter_4[0]\, \fc1/counter_4[2]\, 
        \fc1/final_count[0]_net_1\, \fc1/final_count[1]_net_1\, 
        \fc1/final_count[2]_net_1\, \fc1/final_count[3]_net_1\, 
        \fc1/un3_rxready_0\, \fc1/un3_rxready_1\, 
        \fc1/un6_rxreadylt3\, \fc1/un7_rxready\, 
        \fc2/HIEFFPLA_NET_1_2334\, \fc2/HIEFFPLA_NET_1_2335\, 
        \fc2/I_5_1\, \fc2/I_7_1\, \fc2/I_9_1\, \fc2/N_2\, 
        \fc2/counter[0]_net_1\, \fc2/counter[1]_net_1\, 
        \fc2/counter[2]_net_1\, \fc2/counter[3]_net_1\, 
        \fc2/counter_4[0]\, \fc2/counter_4[3]\, 
        \fc2/final_count[0]_net_1\, \fc2/final_count[1]_net_1\, 
        \fc2/final_count[2]_net_1\, \fc2/final_count[3]_net_1\, 
        \fc2/un3_rxready_0\, \fc2/un3_rxready_1\, 
        \fc2/un6_rxreadylt3\, \fc2/un7_rxready\, jtag_loc_en, 
        \kc1/HIEFFPLA_NET_1_2211\, \kc1/HIEFFPLA_NET_1_2212\, 
        \kc1/HIEFFPLA_NET_1_2216\, \kc1/HIEFFPLA_NET_1_2217\, 
        \kc1/HIEFFPLA_NET_1_2220\, \kc1/HIEFFPLA_NET_1_2224\, 
        \kc1/HIEFFPLA_NET_1_2228\, \kc1/HIEFFPLA_NET_1_2229\, 
        \kc1/HIEFFPLA_NET_1_2285\, \kc1/HIEFFPLA_NET_1_2287\, 
        \kc1/HIEFFPLA_NET_1_2288\, \kc1/HIEFFPLA_NET_1_2289\, 
        \kc1/HIEFFPLA_NET_1_2290\, \kc1/HIEFFPLA_NET_1_2291\, 
        \kc1/HIEFFPLA_NET_1_2302\, \kc1/N_145_i_0_a2_0_2\, 
        \kc1/N_173\, \kc1/N_174\, \kc1/state[1]_net_1\, 
        \kc1/state[2]_net_1\, \kc1/state[3]_net_1\, 
        \kc1/state[4]_net_1\, \kc1/state[5]_net_1\, 
        \kc1/state[6]_net_1\, \kc1/state_RNIFF7N[4]_net_1\, 
        \kc1/state_i[7]_net_1\, \kc1.N_188\, \kc1.N_189\, 
        \kc2/HIEFFPLA_NET_1_2213\, \kc2/HIEFFPLA_NET_1_2214\, 
        \kc2/HIEFFPLA_NET_1_2219\, \kc2/HIEFFPLA_NET_1_2222\, 
        \kc2/HIEFFPLA_NET_1_2227\, \kc2/HIEFFPLA_NET_1_2264\, 
        \kc2/HIEFFPLA_NET_1_2265\, \kc2/HIEFFPLA_NET_1_2267\, 
        \kc2/HIEFFPLA_NET_1_2268\, \kc2/HIEFFPLA_NET_1_2276\, 
        \kc2/HIEFFPLA_NET_1_2277\, \kc2/N_145_i\, 
        \kc2/N_145_i_0_1\, \kc2/N_145_i_0_a2_0_2\, \kc2/N_173\, 
        \kc2/N_174\, \kc2/state[1]_net_1\, \kc2/state[2]_net_1\, 
        \kc2/state[3]_net_1\, \kc2/state[4]_net_1\, 
        \kc2/state[5]_net_1\, \kc2/state[6]_net_1\, 
        \kc2/state_RNIGOBS[4]_net_1\, \kc2/state_RNO_0[2]\, 
        \kc2/state_i[7]_net_1\, \kc2/un62_state\, 
        \kc3/HIEFFPLA_NET_1_2215\, \kc3/HIEFFPLA_NET_1_2218\, 
        \kc3/HIEFFPLA_NET_1_2221\, \kc3/HIEFFPLA_NET_1_2223\, 
        \kc3/HIEFFPLA_NET_1_2225\, \kc3/HIEFFPLA_NET_1_2226\, 
        \kc3/HIEFFPLA_NET_1_2241\, \kc3/HIEFFPLA_NET_1_2243\, 
        \kc3/HIEFFPLA_NET_1_2245\, \kc3/HIEFFPLA_NET_1_2246\, 
        \kc3/HIEFFPLA_NET_1_2247\, \kc3/HIEFFPLA_NET_1_2255\, 
        \kc3/HIEFFPLA_NET_1_2256\, \kc3/N_145_i\, 
        \kc3/N_145_i_0_1\, \kc3/N_145_i_0_a2_0_2\, \kc3/N_173\, 
        \kc3/N_174\, \kc3/state[1]_net_1\, \kc3/state[2]_net_1\, 
        \kc3/state[3]_net_1\, \kc3/state[4]_net_1\, 
        \kc3/state[5]_net_1\, \kc3/state[6]_net_1\, 
        \kc3/state_RNIH1G11[4]_net_1\, \kc3/state_RNO_1[5]\, 
        \kc3/state_i[7]_net_1\, \key_ok_tmr[0]\, \key_ok_tmr[1]\, 
        \key_ok_tmr[2]\, \tmr.sig_net_1\, \tmr.sig_1_net_1\, 
        \tmr.sig_1_RNO_net_1\, \tmr.sig_RNO_net_1\, AFLSDF_VCC, 
        AFLSDF_GND, \AFLSDF_INV_0\, \AFLSDF_INV_1\, 
        \AFLSDF_INV_2\ : std_logic;
    signal GND_power_net1 : std_logic;
    signal VCC_power_net1 : std_logic;

    for all : PLLBA
	Use entity work.PLLBA(DEF_ARCH);
begin 

    AFLSDF_GND <= GND_power_net1;
    \GND\ <= GND_power_net1;
    \VCC\ <= VCC_power_net1;
    AFLSDF_VCC <= VCC_power_net1;

    \fc0/ck_ok_RNO_0\ : AND3
      port map(A => \fc0/final_count[1]_net_1\, B => 
        \fc0/final_count[2]_net_1\, C => 
        \fc0/final_count[0]_net_1\, Y => \fc0/un6_rxreadylt3\);
    
    \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_tdi_n(1), N2POUT => 
        \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/U2_N2P\);
    
    \jtag_out_loc[3]\ : DFN0
      port map(D => \jtag_loc[3]\, CLK => GLA, Q => 
        \jtag_out_loc[3]_net_1\);
    
    \jtag_in0/_INBUF_LVDS[0]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/NET1\, Y
         => \jtag_loc[0]\);
    
    \fc2/un2_counter_I_5\ : XOR2
      port map(A => \fc2/counter[0]_net_1\, B => 
        \fc2/counter[1]_net_1\, Y => \fc2/I_5_1\);
    
    \fc0/un2_counter_I_9\ : XOR2
      port map(A => \fc0/N_2\, B => \fc0/counter[3]_net_1\, Y => 
        \fc0/I_9\);
    
    \clksig/U0/U1\ : IOIN_IB
      port map(YIN => \clksig/U0/NET1\, Y => gbt_clk_sig);
    
    \fc0/ck_ok_RNO_1\ : AO1D
      port map(A => \fc0/final_count[1]_net_1\, B => 
        \fc0/final_count[0]_net_1\, C => \fc0/un3_rxready_0\, Y
         => \fc0/un3_rxready_1\);
    
    \proasic_tms_pad[1]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_tms_pad[1]/U0/NET1\, E => 
        \proasic_tms_pad[1]/U0/NET2\, PAD => proasic_tms(1));
    
    \kc1/state_i[7]\ : DFN1
      port map(D => \kc1/HIEFFPLA_NET_1_2302\, CLK => gbt_clk_sig, 
        Q => \kc1/state_i[7]_net_1\);
    
    \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/U2_N2P\, PAD => 
        gbt_tms_p(1), Y => 
        \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/NET1\);
    
    \kc1/HIEFFPLA_INST_1_2168\ : AND3B
      port map(A => \kc1/HIEFFPLA_NET_1_2217\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc1/HIEFFPLA_NET_1_2288\);
    
    \fc0/counter[3]\ : DFN1
      port map(D => \fc0/HIEFFPLA_NET_1_2375\, CLK => GLB, Q => 
        \fc0/counter[3]_net_1\);
    
    \fc1/ck_ok_RNO\ : AO1E
      port map(A => \fc1/un6_rxreadylt3\, B => 
        \fc1/un3_rxready_1\, C => gbt_rxrdy_c, Y => 
        \fc1/un7_rxready\);
    
    \kc3/HIEFFPLA_INST_1_2174\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \kc3/state_i[7]_net_1\, C => \tmr.sig_net_1\, Y => 
        \kc3/HIEFFPLA_NET_1_2243\);
    
    \fc2/un2_counter_I_9\ : XOR2
      port map(A => \fc2/N_2\, B => \fc2/counter[3]_net_1\, Y => 
        \fc2/I_9_1\);
    
    \proasic_tms_pad[1]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_rem[0]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_tms_pad[1]/U0/NET1\, EOUT => 
        \proasic_tms_pad[1]/U0/NET2\);
    
    \fc2/counter[3]\ : DFN1
      port map(D => \fc2/counter_4[3]\, CLK => GLB, Q => 
        \fc2/counter[3]_net_1\);
    
    \kc2/HIEFFPLA_INST_1_2128\ : NAND3B
      port map(A => \kc2/state[5]_net_1\, B => 
        \kc2/state[4]_net_1\, C => \kc2/N_145_i_0_a2_0_2\, Y => 
        \kc2/HIEFFPLA_NET_1_2227\);
    
    \kc2/HIEFFPLA_INST_1_2193\ : AND3B
      port map(A => \kc2/HIEFFPLA_NET_1_2213\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc2/HIEFFPLA_NET_1_2277\);
    
    \kc3/HIEFFPLA_INST_1_2131\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \kc3/HIEFFPLA_NET_1_2226\, C => \tmr.sig_net_1\, Y => 
        \kc3/HIEFFPLA_NET_1_2255\);
    
    \kc1/jtag_loc_en\ : AND3
      port map(A => gbt_dvalid_c, B => \tmr.sig_1_net_1\, C => 
        \tmr.sig_net_1\, Y => jtag_loc_en);
    
    \kc3/state[6]\ : DFN1
      port map(D => \kc3/HIEFFPLA_NET_1_2243\, CLK => gbt_clk_sig, 
        Q => \kc3/state[6]_net_1\);
    
    \proasic_rst_pad[1]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_rst_pad[1]/U0/NET1\, E => 
        \proasic_rst_pad[1]/U0/NET2\, PAD => proasic_rst(1));
    
    \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_rst_n(0), N2POUT => 
        \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/U2_N2P\);
    
    \fc1/un2_counter_I_5\ : XOR2
      port map(A => \fc1/counter[0]_net_1\, B => 
        \fc1/counter[1]_net_1\, Y => \fc1/I_5_0\);
    
    \kc3/state_RNO[5]\ : NOR3B
      port map(A => \kc1.N_189\, B => \kc3/state[6]_net_1\, C => 
        key_a, Y => \kc3/state_RNO_1[5]\);
    
    \jtag_out_loc[2]\ : DFN0
      port map(D => \jtag_loc[2]\, CLK => GLA, Q => 
        \jtag_out_loc[2]_net_1\);
    
    \fc2/final_count[3]\ : DFN1E1
      port map(D => \fc2/I_9_1\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc2/final_count[3]_net_1\);
    
    \tmr.sig_RNO\ : MAJ3
      port map(A => \clock_ok_tmr[2]\, B => \clock_ok_tmr[0]\, C
         => \clock_ok_tmr[1]\, Y => \tmr.sig_RNO_net_1\);
    
    \gbt_clk40_pad/U0/U1\ : IOIN_IB
      port map(YIN => \gbt_clk40_pad/U0/NET1\, Y => gbt_clk40_c);
    
    \kc3/state[4]\ : DFN1
      port map(D => \kc3/HIEFFPLA_NET_1_2247\, CLK => gbt_clk_sig, 
        Q => \kc3/state[4]_net_1\);
    
    \kc2/state_RNIGOBS[4]\ : AND3
      port map(A => key_a, B => key_b, C => \kc2/state[4]_net_1\, 
        Y => \kc2/state_RNIGOBS[4]_net_1\);
    
    \fc0/HIEFFPLA_INST_1_2166\ : OA1C
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc0/counter[0]_net_1\, Y => \fc0/HIEFFPLA_NET_1_2378\);
    
    \fc1/un2_counter_I_9\ : XOR2
      port map(A => \fc1/N_2\, B => \fc1/counter[3]_net_1\, Y => 
        \fc1/I_9_0\);
    
    \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_tms_n(1), N2POUT => 
        \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/U2_N2P\);
    
    \fc1/counter_RNO[2]\ : NOR2A
      port map(A => \fc1/I_7_0\, B => \fc0.counter_0_sqmuxa\, Y
         => \fc1/counter_4[2]\);
    
    \kc3/state_RNIHMLV[1]\ : NAND2B
      port map(A => \key_ok_tmr[2]\, B => \kc3/state[1]_net_1\, Y
         => \kc3/N_173\);
    
    \fc2/final_count[0]\ : DFN1E1
      port map(D => \AFLSDF_INV_0\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc2/final_count[0]_net_1\);
    
    \fc0/HIEFFPLA_INST_1_2184\ : OA1A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc0/I_9\, Y => \fc0/HIEFFPLA_NET_1_2375\);
    
    \jtag_in0/_INBUF_LVDS[1]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/NET1\, Y
         => \jtag_loc[1]\);
    
    \kc2/state[5]\ : DFN1
      port map(D => \kc2/HIEFFPLA_NET_1_2265\, CLK => gbt_clk_sig, 
        Q => \kc2/state[5]_net_1\);
    
    \fc1/HIEFFPLA_INST_1_2202\ : OA1A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc1/I_9_0\, Y => \fc1/HIEFFPLA_NET_1_2353\);
    
    \fc1/counter_RNO[0]\ : AND2B
      port map(A => \fc1/counter[0]_net_1\, B => 
        \fc0.counter_0_sqmuxa\, Y => \fc1/counter_4[0]\);
    
    AFLSDF_INV_0 : INV
      port map(A => \fc2/counter[0]_net_1\, Y => \AFLSDF_INV_0\);
    
    \jtag_out_rem[2]\ : DFN0
      port map(D => \jtag_rem[2]\, CLK => GLA, Q => 
        \jtag_out_rem[2]_net_1\);
    
    \fc1/un2_counter_I_7\ : AX1C
      port map(A => \fc1/counter[1]_net_1\, B => 
        \fc1/counter[0]_net_1\, C => \fc1/counter[2]_net_1\, Y
         => \fc1/I_7_0\);
    
    \fc0/counter[0]\ : DFN1
      port map(D => \fc0/HIEFFPLA_NET_1_2378\, CLK => GLB, Q => 
        \fc0/counter[0]_net_1\);
    
    \kc1/state[6]\ : DFN1
      port map(D => \kc1/HIEFFPLA_NET_1_2287\, CLK => gbt_clk_sig, 
        Q => \kc1/state[6]_net_1\);
    
    \kc1/HIEFFPLA_INST_1_2124\ : AND3B
      port map(A => \kc1/HIEFFPLA_NET_1_2228\, B => key_a, C => 
        gbt_rxrdy_c, Y => \kc1/HIEFFPLA_NET_1_2229\);
    
    \kc1/HIEFFPLA_INST_1_2136\ : NAND3B
      port map(A => \kc1/state[5]_net_1\, B => 
        \kc1/state[4]_net_1\, C => \kc1/N_145_i_0_a2_0_2\, Y => 
        \kc1/HIEFFPLA_NET_1_2224\);
    
    \proasic_tdi_pad[0]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_loc[1]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_tdi_pad[0]/U0/NET1\, EOUT => 
        \proasic_tdi_pad[0]/U0/NET2\);
    
    \kc1/HIEFFPLA_INST_1_2171\ : AND3B
      port map(A => \kc1/HIEFFPLA_NET_1_2216\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc1/HIEFFPLA_NET_1_2291\);
    
    \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/U2_N2P\, PAD => 
        gbt_tck_p(1), Y => 
        \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/NET1\);
    
    \fc1/final_count[3]\ : DFN1E1
      port map(D => \fc1/I_9_0\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc1/final_count[3]_net_1\);
    
    \proasic_tms_pad[0]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_tms_pad[0]/U0/NET1\, E => 
        \proasic_tms_pad[0]/U0/NET2\, PAD => proasic_tms(0));
    
    \jtag_out_loc[1]\ : DFN0
      port map(D => \jtag_loc[1]\, CLK => GLA, Q => 
        \jtag_out_loc[1]_net_1\);
    
    \jtag_in0/_INBUF_LVDS[3]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/NET1\, Y
         => \jtag_loc[3]\);
    
    \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_tck_n(1), N2POUT => 
        \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/U2_N2P\);
    
    \gbt_dvalid_pad/U0/U1\ : IOIN_IB
      port map(YIN => \gbt_dvalid_pad/U0/NET1\, Y => gbt_dvalid_c);
    
    \proasic_tdi_pad[1]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_rem[1]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_tdi_pad[1]/U0/NET1\, EOUT => 
        \proasic_tdi_pad[1]/U0/NET2\);
    
    \fc1/final_count[1]\ : DFN1E1
      port map(D => \fc1/I_5_0\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc1/final_count[1]_net_1\);
    
    \tmr.sig_1_RNO\ : MAJ3
      port map(A => \key_ok_tmr[2]\, B => \key_ok_tmr[0]\, C => 
        \key_ok_tmr[1]\, Y => \tmr.sig_1_RNO_net_1\);
    
    \keyb/U0/U0\ : IOPADP_IN
      port map(N2PIN => \keyb/U0/U2_N2P\, PAD => gbt_key_b_p, Y
         => \keyb/U0/NET1\);
    
    \kc3/state[5]\ : DFN1
      port map(D => \kc3/state_RNO_1[5]\, CLK => gbt_clk_sig, Q
         => \kc3/state[5]_net_1\);
    
    \proasic_tms_pad[0]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_loc[0]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_tms_pad[0]/U0/NET1\, EOUT => 
        \proasic_tms_pad[0]/U0/NET2\);
    
    \keya/U0/U1\ : IOIN_IB
      port map(YIN => \keya/U0/NET1\, Y => key_a);
    
    \osc_clk40_pad/U0/U0\ : IOPAD_IN
      port map(PAD => osc_clk40, Y => \osc_clk40_pad/U0/NET1\);
    
    \kc1/HIEFFPLA_INST_1_2210\ : AOI1C
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \kc1/HIEFFPLA_NET_1_2224\, C => \kc1/HIEFFPLA_NET_1_2229\, 
        Y => \kc1/HIEFFPLA_NET_1_2211\);
    
    \clocking/Core\ : PLLBA
      port map(CLKA => osc_clk40_c, EXTFB => \GND\, POWERDOWN => 
        \VCC\, GLA => GLA, LOCK => OPEN, GLB => GLB, YB => OPEN, 
        GLC => OPEN, YC => OPEN, OADIV0 => \VCC\, OADIV1 => \GND\, 
        OADIV2 => \VCC\, OADIV3 => \GND\, OADIV4 => \GND\, OAMUX0
         => \GND\, OAMUX1 => \GND\, OAMUX2 => \VCC\, DLYGLA0 => 
        \GND\, DLYGLA1 => \GND\, DLYGLA2 => \GND\, DLYGLA3 => 
        \GND\, DLYGLA4 => \GND\, OBDIV0 => \GND\, OBDIV1 => \GND\, 
        OBDIV2 => \GND\, OBDIV3 => \GND\, OBDIV4 => \GND\, OBMUX0
         => \GND\, OBMUX1 => \VCC\, OBMUX2 => \GND\, DLYYB0 => 
        \GND\, DLYYB1 => \GND\, DLYYB2 => \GND\, DLYYB3 => \GND\, 
        DLYYB4 => \GND\, DLYGLB0 => \GND\, DLYGLB1 => \GND\, 
        DLYGLB2 => \GND\, DLYGLB3 => \GND\, DLYGLB4 => \GND\, 
        OCDIV0 => AFLSDF_GND, OCDIV1 => AFLSDF_GND, OCDIV2 => 
        AFLSDF_GND, OCDIV3 => AFLSDF_GND, OCDIV4 => AFLSDF_GND, 
        OCMUX0 => AFLSDF_GND, OCMUX1 => AFLSDF_GND, OCMUX2 => 
        AFLSDF_GND, DLYYC0 => AFLSDF_GND, DLYYC1 => AFLSDF_GND, 
        DLYYC2 => AFLSDF_GND, DLYYC3 => AFLSDF_GND, DLYYC4 => 
        AFLSDF_GND, DLYGLC0 => AFLSDF_GND, DLYGLC1 => AFLSDF_GND, 
        DLYGLC2 => AFLSDF_GND, DLYGLC3 => AFLSDF_GND, DLYGLC4 => 
        AFLSDF_GND, FINDIV0 => \VCC\, FINDIV1 => \VCC\, FINDIV2
         => \VCC\, FINDIV3 => \GND\, FINDIV4 => \GND\, FINDIV5
         => \GND\, FINDIV6 => \GND\, FBDIV0 => \VCC\, FBDIV1 => 
        \VCC\, FBDIV2 => \VCC\, FBDIV3 => \VCC\, FBDIV4 => \GND\, 
        FBDIV5 => \VCC\, FBDIV6 => \GND\, FBDLY0 => \GND\, FBDLY1
         => \GND\, FBDLY2 => \GND\, FBDLY3 => \GND\, FBDLY4 => 
        \GND\, FBSEL0 => \VCC\, FBSEL1 => \GND\, XDLYSEL => \GND\, 
        VCOSEL0 => \GND\, VCOSEL1 => \VCC\, VCOSEL2 => \VCC\);
    
    \fc0/state\ : DFN1
      port map(D => gbt_clk40_c, CLK => GLB, Q => 
        \fc0/state_net_1\);
    
    \fc1/ck_ok_RNO_1\ : AO1D
      port map(A => \fc1/final_count[1]_net_1\, B => 
        \fc1/final_count[0]_net_1\, C => \fc1/un3_rxready_0\, Y
         => \fc1/un3_rxready_1\);
    
    \keya/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_key_a_n, N2POUT => \keya/U0/U2_N2P\);
    
    \fc0/ck_ok_RNO_2\ : OR2A
      port map(A => \fc0/final_count[2]_net_1\, B => 
        \fc0/final_count[3]_net_1\, Y => \fc0/un3_rxready_0\);
    
    \fc0/HIEFFPLA_INST_1_2198\ : OA1A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc0/I_7\, Y => \fc0/HIEFFPLA_NET_1_2376\);
    
    \fc2/ck_ok_RNO_0\ : AND3
      port map(A => \fc2/final_count[1]_net_1\, B => 
        \fc2/final_count[2]_net_1\, C => 
        \fc2/final_count[0]_net_1\, Y => \fc2/un6_rxreadylt3\);
    
    \gbt_rxrdy_pad/U0/U0\ : IOPAD_IN
      port map(PAD => gbt_rxrdy, Y => \gbt_rxrdy_pad/U0/NET1\);
    
    \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/U2_N2P\, PAD => 
        gbt_tck_p(0), Y => 
        \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/NET1\);
    
    \fc2/HIEFFPLA_INST_1_2196\ : OA1A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc2/I_7_1\, Y => \fc2/HIEFFPLA_NET_1_2334\);
    
    \kc2/HIEFFPLA_INST_1_2143\ : NAND3
      port map(A => \kc2/state[6]_net_1\, B => key_b, C => 
        gbt_rxrdy_c, Y => \kc2/HIEFFPLA_NET_1_2222\);
    
    \kc1/statese_6_0_a2_0\ : AND3
      port map(A => gbt_rxrdy_c, B => \tmr.sig_net_1\, C => key_b, 
        Y => \kc1.N_189\);
    
    \kc2/HIEFFPLA_INST_1_2194\ : NAND3
      port map(A => \kc2/N_174\, B => key_b, C => gbt_rxrdy_c, Y
         => \kc2/HIEFFPLA_NET_1_2213\);
    
    \gbt_clk40_pad/U0/U0\ : IOPAD_IN
      port map(PAD => gbt_clk40, Y => \gbt_clk40_pad/U0/NET1\);
    
    \kc1/HIEFFPLA_INST_1_2149\ : NAND3
      port map(A => \kc1/state[2]_net_1\, B => key_b, C => 
        gbt_rxrdy_c, Y => \kc1/HIEFFPLA_NET_1_2220\);
    
    \fc2/counter[0]\ : DFN1
      port map(D => \fc2/counter_4[0]\, CLK => GLB, Q => 
        \fc2/counter[0]_net_1\);
    
    \kc2/state[3]\ : DFN1
      port map(D => \kc2/state_RNIGOBS[4]_net_1\, CLK => 
        gbt_clk_sig, Q => \kc2/state[3]_net_1\);
    
    \kc2/HIEFFPLA_INST_1_2142\ : AND3B
      port map(A => \kc2/HIEFFPLA_NET_1_2222\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc2/HIEFFPLA_NET_1_2265\);
    
    \tmr.sig\ : DFN1
      port map(D => \tmr.sig_RNO_net_1\, CLK => gbt_clk40_c, Q
         => \tmr.sig_net_1\);
    
    \keyb/U0/U1\ : IOIN_IB
      port map(YIN => \keyb/U0/NET1\, Y => key_b);
    
    \fc2/ck_ok_RNO\ : AO1E
      port map(A => \fc2/un6_rxreadylt3\, B => 
        \fc2/un3_rxready_1\, C => gbt_rxrdy_c, Y => 
        \fc2/un7_rxready\);
    
    \kc3/state_i[7]\ : DFN1
      port map(D => \kc3/N_145_i\, CLK => gbt_clk_sig, Q => 
        \kc3/state_i[7]_net_1\);
    
    \fc1/counter[0]\ : DFN1
      port map(D => \fc1/counter_4[0]\, CLK => GLB, Q => 
        \fc1/counter[0]_net_1\);
    
    \kc3/state[3]\ : DFN1
      port map(D => \kc3/state_RNIH1G11[4]_net_1\, CLK => 
        gbt_clk_sig, Q => \kc3/state[3]_net_1\);
    
    \kc3/HIEFFPLA_INST_1_2182\ : NAND3
      port map(A => key_a, B => key_b, C => gbt_rxrdy_c, Y => 
        \kc3/HIEFFPLA_NET_1_2215\);
    
    \gbt_rxrdy_pad/U0/U1\ : IOIN_IB
      port map(YIN => \gbt_rxrdy_pad/U0/NET1\, Y => gbt_rxrdy_c);
    
    \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_tdi_n(0), N2POUT => 
        \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/U2_N2P\);
    
    \proasic_tck_pad[0]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_tck_pad[0]/U0/NET1\, E => 
        \proasic_tck_pad[0]/U0/NET2\, PAD => proasic_tck(0));
    
    \kc1/HIEFFPLA_INST_1_2209\ : AO1C
      port map(A => \kc3/HIEFFPLA_NET_1_2215\, B => \kc1/N_173\, 
        C => \kc1/HIEFFPLA_NET_1_2211\, Y => 
        \kc1/HIEFFPLA_NET_1_2212\);
    
    \fc2/final_count[2]\ : DFN1E1
      port map(D => \fc2/I_7_1\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc2/final_count[2]_net_1\);
    
    \kc2/HIEFFPLA_INST_1_2152\ : NAND3
      port map(A => \kc2/state[2]_net_1\, B => key_b, C => 
        gbt_rxrdy_c, Y => \kc2/HIEFFPLA_NET_1_2219\);
    
    \kc3/state_i_RNO_0[7]\ : NAND3C
      port map(A => \kc3/HIEFFPLA_NET_1_2247\, B => 
        \kc3/state_RNIH1G11[4]_net_1\, C => 
        \kc3/HIEFFPLA_NET_1_2256\, Y => \kc3/N_145_i_0_1\);
    
    \kc2/state_i[7]\ : DFN1
      port map(D => \kc2/N_145_i\, CLK => gbt_clk_sig, Q => 
        \kc2/state_i[7]_net_1\);
    
    \kc2/state_RNIF4DL[1]\ : NAND2B
      port map(A => \key_ok_tmr[1]\, B => \kc2/state[1]_net_1\, Y
         => \kc2/N_173\);
    
    \fc1/un2_counter_I_8\ : AND3
      port map(A => \fc1/counter[0]_net_1\, B => 
        \fc1/counter[1]_net_1\, C => \fc1/counter[2]_net_1\, Y
         => \fc1/N_2\);
    
    \kc1/state[1]\ : DFN1
      port map(D => \kc1/HIEFFPLA_NET_1_2290\, CLK => gbt_clk_sig, 
        Q => \kc1/state[1]_net_1\);
    
    \fc1/HIEFFPLA_INST_1_2188\ : OA1A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc1/I_5_0\, Y => \fc1/HIEFFPLA_NET_1_2355\);
    
    \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/U2_N2P\, PAD => 
        gbt_rst_p(1), Y => 
        \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/NET1\);
    
    \fc1/final_count[0]\ : DFN1E1
      port map(D => \AFLSDF_INV_1\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc1/final_count[0]_net_1\);
    
    \kc3/HIEFFPLA_INST_1_2133\ : NAND3A
      port map(A => key_b, B => gbt_rxrdy_c, C => key_a, Y => 
        \kc3/HIEFFPLA_NET_1_2225\);
    
    \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/U2_N2P\, PAD => 
        gbt_rst_p(0), Y => 
        \jtag_in0/\\\\INBUF_LVDS[3]\\\\/U0/NET1\);
    
    \kc1/state_RNIDI4B[1]\ : NAND2B
      port map(A => \key_ok_tmr[0]\, B => \kc1/state[1]_net_1\, Y
         => \kc1/N_173\);
    
    \kc3/state_i_RNO_4[7]\ : AND2B
      port map(A => \kc3/N_174\, B => \kc3/N_173\, Y => 
        \kc3/N_145_i_0_a2_0_2\);
    
    \osc_clk40_pad/U0/U1\ : IOIN_IB
      port map(YIN => \osc_clk40_pad/U0/NET1\, Y => osc_clk40_c);
    
    \jtag_in1/_INBUF_LVDS[3]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/NET1\, Y
         => \jtag_rem[3]\);
    
    \kc2/state_RNIH1TC1[1]\ : AND3
      port map(A => \kc1.N_189\, B => key_a, C => \kc2/N_173\, Y
         => \kc2/un62_state\);
    
    \kc1/state[2]\ : DFN1
      port map(D => \kc1/HIEFFPLA_NET_1_2289\, CLK => gbt_clk_sig, 
        Q => \kc1/state[2]_net_1\);
    
    \fc2/ck_ok\ : DFN1
      port map(D => \fc2/un7_rxready\, CLK => GLB, Q => 
        \clock_ok_tmr[2]\);
    
    \fc1/counter[1]\ : DFN1
      port map(D => \fc1/HIEFFPLA_NET_1_2355\, CLK => GLB, Q => 
        \fc1/counter[1]_net_1\);
    
    \kc2/state_i_RNO_3[7]\ : AND2B
      port map(A => \kc2/N_173\, B => \kc2/N_174\, Y => 
        \kc2/N_145_i_0_a2_0_2\);
    
    \fc0/final_count[3]\ : DFN1E1
      port map(D => \fc0/I_9\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc0/final_count[3]_net_1\);
    
    \kc3/HIEFFPLA_INST_1_2139\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2223\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc3/HIEFFPLA_NET_1_2247\);
    
    \proasic_tck_pad[1]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_tck_pad[1]/U0/NET1\, E => 
        \proasic_tck_pad[1]/U0/NET2\, PAD => proasic_tck(1));
    
    \kc3/HIEFFPLA_INST_1_2140\ : OR3B
      port map(A => \kc3/state[5]_net_1\, B => gbt_rxrdy_c, C => 
        key_b, Y => \kc3/HIEFFPLA_NET_1_2223\);
    
    \kc3/HIEFFPLA_INST_1_2132\ : NAND3B
      port map(A => \kc3/state[5]_net_1\, B => 
        \kc3/state[4]_net_1\, C => \kc3/N_145_i_0_a2_0_2\, Y => 
        \kc3/HIEFFPLA_NET_1_2226\);
    
    \kc3/HIEFFPLA_INST_1_2155\ : NAND3
      port map(A => \kc3/N_174\, B => key_b, C => gbt_rxrdy_c, Y
         => \kc3/HIEFFPLA_NET_1_2218\);
    
    \kc1/state_RNIKP4B[2]\ : NAND2B
      port map(A => \kc1/state[2]_net_1\, B => 
        \kc1/state[6]_net_1\, Y => \kc1/N_174\);
    
    \kc1/state[0]\ : DFN1
      port map(D => \kc1/HIEFFPLA_NET_1_2285\, CLK => gbt_clk_sig, 
        Q => \key_ok_tmr[0]\);
    
    \fc2/un2_counter_I_8\ : AND3
      port map(A => \fc2/counter[0]_net_1\, B => 
        \fc2/counter[1]_net_1\, C => \fc2/counter[2]_net_1\, Y
         => \fc2/N_2\);
    
    \kc2/state[1]\ : DFN1
      port map(D => \kc2/HIEFFPLA_NET_1_2267\, CLK => gbt_clk_sig, 
        Q => \kc2/state[1]_net_1\);
    
    \proasic_tdi_pad[0]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_tdi_pad[0]/U0/NET1\, E => 
        \proasic_tdi_pad[0]/U0/NET2\, PAD => proasic_tdi(0));
    
    \kc1/state[4]\ : DFN1
      port map(D => \kc1/HIEFFPLA_NET_1_2291\, CLK => gbt_clk_sig, 
        Q => \kc1/state[4]_net_1\);
    
    \kc1/state[3]\ : DFN1
      port map(D => \kc1/state_RNIFF7N[4]_net_1\, CLK => 
        gbt_clk_sig, Q => \kc1/state[3]_net_1\);
    
    \fc0/state_RNIM29B\ : NOR2A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, Y => 
        \fc0.counter_0_sqmuxa\);
    
    \kc1/HIEFFPLA_INST_1_2148\ : AND3B
      port map(A => \kc1/HIEFFPLA_NET_1_2220\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc1/HIEFFPLA_NET_1_2290\);
    
    \proasic_rst_pad[0]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_loc[3]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_rst_pad[0]/U0/NET1\, EOUT => 
        \proasic_rst_pad[0]/U0/NET2\);
    
    \kc2/HIEFFPLA_INST_1_2191\ : OR3B
      port map(A => \kc2/state[5]_net_1\, B => gbt_rxrdy_c, C => 
        key_b, Y => \kc2/HIEFFPLA_NET_1_2214\);
    
    \kc1/state[5]\ : DFN1
      port map(D => \kc1/HIEFFPLA_NET_1_2288\, CLK => gbt_clk_sig, 
        Q => \kc1/state[5]_net_1\);
    
    \fc0/un2_counter_I_7\ : AX1C
      port map(A => \fc0/counter[1]_net_1\, B => 
        \fc0/counter[0]_net_1\, C => \fc0/counter[2]_net_1\, Y
         => \fc0/I_7\);
    
    \fc0/ck_ok_RNO\ : AO1E
      port map(A => \fc0/un6_rxreadylt3\, B => 
        \fc0/un3_rxready_1\, C => gbt_rxrdy_c, Y => 
        \fc0/un7_rxready\);
    
    \fc0/final_count[2]\ : DFN1E1
      port map(D => \fc0/I_7\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc0/final_count[2]_net_1\);
    
    \kc1/HIEFFPLA_INST_1_2160\ : AND3A
      port map(A => \kc3/HIEFFPLA_NET_1_2215\, B => 
        \tmr.sig_net_1\, C => \kc1/N_173\, Y => 
        \kc1/HIEFFPLA_NET_1_2285\);
    
    \fc1/ck_ok\ : DFN1
      port map(D => \fc1/un7_rxready\, CLK => GLB, Q => 
        \clock_ok_tmr[1]\);
    
    \clksig/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_clk_sig_n, N2POUT => \clksig/U0/U2_N2P\);
    
    \kc1/HIEFFPLA_INST_1_2208\ : AO1
      port map(A => \kc1/HIEFFPLA_NET_1_2212\, B => 
        \tmr.sig_net_1\, C => \kc1/state_RNIFF7N[4]_net_1\, Y => 
        \kc1/HIEFFPLA_NET_1_2302\);
    
    \fc1/final_count[2]\ : DFN1E1
      port map(D => \fc1/I_7_0\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc1/final_count[2]_net_1\);
    
    \fc0/counter[1]\ : DFN1
      port map(D => \fc0/HIEFFPLA_NET_1_2377\, CLK => GLB, Q => 
        \fc0/counter[1]_net_1\);
    
    \kc2/state[6]\ : DFN1
      port map(D => \kc2/HIEFFPLA_NET_1_2264\, CLK => gbt_clk_sig, 
        Q => \kc2/state[6]_net_1\);
    
    \gbt_dvalid_pad/U0/U0\ : IOPAD_IN
      port map(PAD => gbt_dvalid, Y => \gbt_dvalid_pad/U0/NET1\);
    
    \kc1/state_RNIFF7N[4]\ : AND3
      port map(A => key_a, B => key_b, C => \kc1/state[4]_net_1\, 
        Y => \kc1/state_RNIFF7N[4]_net_1\);
    
    \fc2/counter[2]\ : DFN1
      port map(D => \fc2/HIEFFPLA_NET_1_2334\, CLK => GLB, Q => 
        \fc2/counter[2]_net_1\);
    
    \proasic_tdi_pad[1]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_tdi_pad[1]/U0/NET1\, E => 
        \proasic_tdi_pad[1]/U0/NET2\, PAD => proasic_tdi(1));
    
    \jtag_out_loc[0]\ : DFN0
      port map(D => \jtag_loc[0]\, CLK => GLA, Q => 
        \jtag_out_loc[0]_net_1\);
    
    \jtag_in1/_INBUF_LVDS[2]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in1/\\\\INBUF_LVDS[2]\\\\/U0/NET1\, Y
         => \jtag_rem[2]\);
    
    \kc3/HIEFFPLA_INST_1_2145\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2221\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc3/HIEFFPLA_NET_1_2246\);
    
    \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_tms_n(0), N2POUT => 
        \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/U2_N2P\);
    
    \fc0/counter[2]\ : DFN1
      port map(D => \fc0/HIEFFPLA_NET_1_2376\, CLK => GLB, Q => 
        \fc0/counter[2]_net_1\);
    
    \keyb/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_key_b_n, N2POUT => \keyb/U0/U2_N2P\);
    
    \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/U2_N2P\, PAD => 
        gbt_tdi_p(1), Y => 
        \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/NET1\);
    
    \kc2/state[2]\ : DFN1
      port map(D => \kc2/state_RNO_0[2]\, CLK => gbt_clk_sig, Q
         => \kc2/state[2]_net_1\);
    
    jtag_out_rem_tri_enable : DFN0
      port map(D => jtag_loc_en, CLK => GLA, Q => 
        \jtag_out_rem_tri_enable\);
    
    \fc0/final_count[1]\ : DFN1E1
      port map(D => \fc0/I_5\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc0/final_count[1]_net_1\);
    
    \kc3/HIEFFPLA_INST_1_2157\ : AND3A
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \tmr.sig_net_1\, C => \kc3/state[3]_net_1\, Y => 
        \kc3/HIEFFPLA_NET_1_2245\);
    
    AFLSDF_INV_2 : INV
      port map(A => \fc0/counter[0]_net_1\, Y => \AFLSDF_INV_2\);
    
    \kc1/HIEFFPLA_INST_1_2172\ : OR3B
      port map(A => \kc1/state[5]_net_1\, B => gbt_rxrdy_c, C => 
        key_b, Y => \kc1/HIEFFPLA_NET_1_2216\);
    
    \kc1/statese_2_0_a2_0\ : NOR3B
      port map(A => gbt_rxrdy_c, B => \tmr.sig_net_1\, C => key_b, 
        Y => \kc1.N_188\);
    
    \tmr.sig_1\ : DFN1
      port map(D => \tmr.sig_1_RNO_net_1\, CLK => gbt_clk40_c, Q
         => \tmr.sig_1_net_1\);
    
    \kc1/HIEFFPLA_INST_1_2163\ : AND3A
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \tmr.sig_net_1\, C => \kc1/state[3]_net_1\, Y => 
        \kc1/HIEFFPLA_NET_1_2289\);
    
    \kc2/state_i_RNO_1[7]\ : NAND3C
      port map(A => \kc2/HIEFFPLA_NET_1_2268\, B => 
        \kc2/state_RNIGOBS[4]_net_1\, C => 
        \kc2/HIEFFPLA_NET_1_2277\, Y => \kc2/N_145_i_0_1\);
    
    \fc1/counter[2]\ : DFN1
      port map(D => \fc1/counter_4[2]\, CLK => GLB, Q => 
        \fc1/counter[2]_net_1\);
    
    \fc2/final_count[1]\ : DFN1E1
      port map(D => \fc2/I_5_1\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc2/final_count[1]_net_1\);
    
    \proasic_rst_pad[0]/U0/U0\ : IOPAD_TRI
      port map(D => \proasic_rst_pad[0]/U0/NET1\, E => 
        \proasic_rst_pad[0]/U0/NET2\, PAD => proasic_rst(0));
    
    \jtag_out_rem[3]\ : DFN0
      port map(D => \jtag_rem[3]\, CLK => GLA, Q => 
        \jtag_out_rem[3]_net_1\);
    
    AFLSDF_INV_1 : INV
      port map(A => \fc1/counter[0]_net_1\, Y => \AFLSDF_INV_1\);
    
    \kc1/state_i_RNO_4[7]\ : AND2B
      port map(A => \kc1/N_174\, B => \kc1/N_173\, Y => 
        \kc1/N_145_i_0_a2_0_2\);
    
    \fc0/un2_counter_I_5\ : XOR2
      port map(A => \fc0/counter[0]_net_1\, B => 
        \fc0/counter[1]_net_1\, Y => \fc0/I_5\);
    
    \fc1/counter[3]\ : DFN1
      port map(D => \fc1/HIEFFPLA_NET_1_2353\, CLK => GLB, Q => 
        \fc1/counter[3]_net_1\);
    
    \kc1/HIEFFPLA_INST_1_2169\ : NAND3
      port map(A => \kc1/state[6]_net_1\, B => key_b, C => 
        gbt_rxrdy_c, Y => \kc1/HIEFFPLA_NET_1_2217\);
    
    \kc3/HIEFFPLA_INST_1_2146\ : NAND3
      port map(A => \kc3/state[2]_net_1\, B => key_b, C => 
        gbt_rxrdy_c, Y => \kc3/HIEFFPLA_NET_1_2221\);
    
    \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_rst_n(1), N2POUT => 
        \jtag_in1/\\\\INBUF_LVDS[3]\\\\/U0/U2_N2P\);
    
    \fc0/HIEFFPLA_INST_1_2200\ : OA1A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc0/I_5\, Y => \fc0/HIEFFPLA_NET_1_2377\);
    
    \fc2/HIEFFPLA_INST_1_2186\ : OA1A
      port map(A => \fc0/state_net_1\, B => gbt_clk40_c, C => 
        \fc2/I_5_1\, Y => \fc2/HIEFFPLA_NET_1_2335\);
    
    \fc0/ck_ok\ : DFN1
      port map(D => \fc0/un7_rxready\, CLK => GLB, Q => 
        \clock_ok_tmr[0]\);
    
    \fc1/ck_ok_RNO_0\ : AND3
      port map(A => \fc1/final_count[1]_net_1\, B => 
        \fc1/final_count[2]_net_1\, C => 
        \fc1/final_count[0]_net_1\, Y => \fc1/un6_rxreadylt3\);
    
    \jtag_out_rem[0]\ : DFN0
      port map(D => \jtag_rem[0]\, CLK => GLA, Q => 
        \jtag_out_rem[0]_net_1\);
    
    \proasic_tck_pad[0]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_loc[2]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_tck_pad[0]/U0/NET1\, EOUT => 
        \proasic_tck_pad[0]/U0/NET2\);
    
    \kc3/state[1]\ : DFN1
      port map(D => \kc3/HIEFFPLA_NET_1_2246\, CLK => gbt_clk_sig, 
        Q => \kc3/state[1]_net_1\);
    
    \kc2/HIEFFPLA_INST_1_2205\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \kc2/state_i[7]_net_1\, C => \tmr.sig_net_1\, Y => 
        \kc2/HIEFFPLA_NET_1_2264\);
    
    \clksig/U0/U0\ : IOPADP_IN
      port map(N2PIN => \clksig/U0/U2_N2P\, PAD => gbt_clk_sig_p, 
        Y => \clksig/U0/NET1\);
    
    \kc3/state[2]\ : DFN1
      port map(D => \kc3/HIEFFPLA_NET_1_2245\, CLK => gbt_clk_sig, 
        Q => \kc3/state[2]_net_1\);
    
    \kc3/state_RNIOTLV[2]\ : NAND2B
      port map(A => \kc3/state[2]_net_1\, B => 
        \kc3/state[6]_net_1\, Y => \kc3/N_174\);
    
    \kc2/state_RNIMBDL[2]\ : NAND2B
      port map(A => \kc2/state[2]_net_1\, B => 
        \kc2/state[6]_net_1\, Y => \kc2/N_174\);
    
    \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/U2_N2P\, PAD => 
        gbt_tms_p(0), Y => 
        \jtag_in0/\\\\INBUF_LVDS[0]\\\\/U0/NET1\);
    
    \kc3/HIEFFPLA_INST_1_2154\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2218\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc3/HIEFFPLA_NET_1_2256\);
    
    \kc3/state[0]\ : DFN1
      port map(D => \kc3/HIEFFPLA_NET_1_2241\, CLK => gbt_clk_sig, 
        Q => \key_ok_tmr[2]\);
    
    \jtag_in1/_INBUF_LVDS[0]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in1/\\\\INBUF_LVDS[0]\\\\/U0/NET1\, Y
         => \jtag_rem[0]\);
    
    \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/U0\ : IOPADP_IN
      port map(N2PIN => 
        \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/U2_N2P\, PAD => 
        gbt_tdi_p(0), Y => 
        \jtag_in0/\\\\INBUF_LVDS[1]\\\\/U0/NET1\);
    
    \fc2/ck_ok_RNO_2\ : OR2A
      port map(A => \fc2/final_count[2]_net_1\, B => 
        \fc2/final_count[3]_net_1\, Y => \fc2/un3_rxready_0\);
    
    \fc2/counter_RNO[3]\ : NOR2A
      port map(A => \fc2/I_9_1\, B => \fc0.counter_0_sqmuxa\, Y
         => \fc2/counter_4[3]\);
    
    \fc2/counter_RNO[0]\ : AND2B
      port map(A => \fc2/counter[0]_net_1\, B => 
        \fc0.counter_0_sqmuxa\, Y => \fc2/counter_4[0]\);
    
    \fc2/counter[1]\ : DFN1
      port map(D => \fc2/HIEFFPLA_NET_1_2335\, CLK => GLB, Q => 
        \fc2/counter[1]_net_1\);
    
    \fc0/final_count[0]\ : DFN1E1
      port map(D => \AFLSDF_INV_2\, CLK => GLB, E => 
        \fc0.counter_0_sqmuxa\, Q => \fc0/final_count[0]_net_1\);
    
    \keya/U0/U0\ : IOPADP_IN
      port map(N2PIN => \keya/U0/U2_N2P\, PAD => gbt_key_a_p, Y
         => \keya/U0/NET1\);
    
    \kc3/state_i_RNO[7]\ : NAND3C
      port map(A => \kc3/N_145_i_0_1\, B => 
        \kc3/HIEFFPLA_NET_1_2241\, C => \kc3/HIEFFPLA_NET_1_2255\, 
        Y => \kc3/N_145_i\);
    
    \kc1/HIEFFPLA_INST_1_2125\ : MX2C
      port map(A => \kc1/state[5]_net_1\, B => \kc1/N_174\, S => 
        key_b, Y => \kc1/HIEFFPLA_NET_1_2228\);
    
    \kc2/state_RNO[2]\ : AND3
      port map(A => key_a, B => \kc1.N_188\, C => 
        \kc2/state[3]_net_1\, Y => \kc2/state_RNO_0[2]\);
    
    \kc2/state[0]\ : DFN1
      port map(D => \kc2/un62_state\, CLK => gbt_clk_sig, Q => 
        \key_ok_tmr[1]\);
    
    \jtag_out_rem[1]\ : DFN0
      port map(D => \jtag_rem[1]\, CLK => GLA, Q => 
        \jtag_out_rem[1]_net_1\);
    
    \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/U2\ : IOPADN_IN
      port map(PAD => gbt_tck_n(0), N2POUT => 
        \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/U2_N2P\);
    
    \fc0/un2_counter_I_8\ : AND3
      port map(A => \fc0/counter[0]_net_1\, B => 
        \fc0/counter[1]_net_1\, C => \fc0/counter[2]_net_1\, Y
         => \fc0/N_2\);
    
    \jtag_in1/_INBUF_LVDS[1]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in1/\\\\INBUF_LVDS[1]\\\\/U0/NET1\, Y
         => \jtag_rem[1]\);
    
    \fc2/un2_counter_I_7\ : AX1C
      port map(A => \fc2/counter[1]_net_1\, B => 
        \fc2/counter[0]_net_1\, C => \fc2/counter[2]_net_1\, Y
         => \fc2/I_7_1\);
    
    \kc1/HIEFFPLA_INST_1_2178\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \kc1/state_i[7]_net_1\, C => \tmr.sig_net_1\, Y => 
        \kc1/HIEFFPLA_NET_1_2287\);
    
    \fc1/ck_ok_RNO_2\ : OR2A
      port map(A => \fc1/final_count[2]_net_1\, B => 
        \fc1/final_count[3]_net_1\, Y => \fc1/un3_rxready_0\);
    
    \kc2/state[4]\ : DFN1
      port map(D => \kc2/HIEFFPLA_NET_1_2268\, CLK => gbt_clk_sig, 
        Q => \kc2/state[4]_net_1\);
    
    \kc2/HIEFFPLA_INST_1_2127\ : AND3B
      port map(A => \kc3/HIEFFPLA_NET_1_2225\, B => 
        \kc2/HIEFFPLA_NET_1_2227\, C => \tmr.sig_net_1\, Y => 
        \kc2/HIEFFPLA_NET_1_2276\);
    
    \kc3/HIEFFPLA_INST_1_2181\ : AND3A
      port map(A => \kc3/HIEFFPLA_NET_1_2215\, B => 
        \tmr.sig_net_1\, C => \kc3/N_173\, Y => 
        \kc3/HIEFFPLA_NET_1_2241\);
    
    \proasic_rst_pad[1]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_rem[3]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_rst_pad[1]/U0/NET1\, EOUT => 
        \proasic_rst_pad[1]/U0/NET2\);
    
    \kc3/state_RNIH1G11[4]\ : AND3
      port map(A => key_a, B => key_b, C => \kc3/state[4]_net_1\, 
        Y => \kc3/state_RNIH1G11[4]_net_1\);
    
    \fc2/ck_ok_RNO_1\ : AO1D
      port map(A => \fc2/final_count[1]_net_1\, B => 
        \fc2/final_count[0]_net_1\, C => \fc2/un3_rxready_0\, Y
         => \fc2/un3_rxready_1\);
    
    \proasic_tck_pad[1]/U0/U1\ : IOTRI_OB_EB
      port map(D => \jtag_out_rem[2]_net_1\, E => 
        \jtag_out_rem_tri_enable\, DOUT => 
        \proasic_tck_pad[1]/U0/NET1\, EOUT => 
        \proasic_tck_pad[1]/U0/NET2\);
    
    \kc2/HIEFFPLA_INST_1_2190\ : AND3B
      port map(A => \kc2/HIEFFPLA_NET_1_2214\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc2/HIEFFPLA_NET_1_2268\);
    
    \kc2/HIEFFPLA_INST_1_2151\ : AND3B
      port map(A => \kc2/HIEFFPLA_NET_1_2219\, B => key_a, C => 
        \tmr.sig_net_1\, Y => \kc2/HIEFFPLA_NET_1_2267\);
    
    \jtag_in0/_INBUF_LVDS[2]_/U0/U1\ : IOIN_IB
      port map(YIN => \jtag_in0/\\\\INBUF_LVDS[2]\\\\/U0/NET1\, Y
         => \jtag_loc[2]\);
    
    \kc2/state_i_RNO[7]\ : NAND3C
      port map(A => \kc2/HIEFFPLA_NET_1_2276\, B => 
        \kc2/un62_state\, C => \kc2/N_145_i_0_1\, Y => 
        \kc2/N_145_i\);
    
    GND_power_inst1 : GND
      port map( Y => GND_power_net1);

    VCC_power_inst1 : VCC
      port map( Y => VCC_power_net1);


end DEF_ARCH; 
