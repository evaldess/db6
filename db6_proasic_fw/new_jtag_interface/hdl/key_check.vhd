--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: key_check.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::ProASIC3> <Die::A3P250> <Package::144 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------

library IEEE;

use IEEE.std_logic_1164.all;

entity key_check is
port (
    signal keya : in std_logic;
    signal keyb : in std_logic;
    signal clk   : in std_logic;
    signal rxready : in std_logic;
    signal ck_ok : in std_logic;
    signal key_ok : out std_logic
);
end key_check;

architecture arch1 of key_check is


begin

 proc1: process (clk, keya, keyb, rxready, ck_ok)
    variable state : integer range 0 to 7 := 0;
    variable keys : std_logic_vector (1 downto 0) := "00";

    begin

    keys := keyb & keya;
    if rising_edge(clk) then
            case state is
                when 0 =>
                    if (keys = "01") then
                        state := 1;
                    else
                        state := 0;
                    end if;
                    if not (rxready and ck_ok) then
                        state := 0;
                    end if;
                when 1 =>
                    if (keys = "10") then
                        state := 2;
                     else
                        state := 0;
                    end if;
                    if not (rxready and ck_ok) then
                        state := 0;
                    end if;
               when 2 =>
                    if (keys = "00") then
                        state := 3;
                    else
                        state := 0;
                    end if;
                    if not (rxready and ck_ok) then
                        state := 0;
                    end if;
                when 3 =>
                    if (keys = "11") then
                        state := 4;
                    else
                        state := 0;
                    end if;
                when 4 =>
                    if (keys = "01") then
                        state := 5;
                    else
                        state := 0;
                    end if;
                    if not (rxready and ck_ok) then
                        state := 0;
                    end if;
                when 5 =>
                    if (keys = "10") then
                        state := 6;
                    else
                        state := 0;
                    end if;
                    if not (rxready and ck_ok) then
                        state := 0;
                    end if;
                when 6 =>
                    if (keys = "11") then
                        state := 7;
                    else
                        state := 0;
                    end if;
                    if not (rxready and ck_ok) then
                        state := 0;
                    end if;
                when 7 =>
                    if (keys /= "11") then
                        state := 0;
                    end if;
                    if not (rxready and ck_ok) then
                        state := 0;
                    end if;
                when others =>
                        state := 0;
            end case;

            if state = 7 then
                key_ok <= '1';
            else
                key_ok <= '0';
            end if;
    end if; -- clock edge

end process;

end arch1;
