----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio eduardo.valdes@fysik.su.se, eduardo.valdes@cern.ch
-- 
-- Create Date: 07/06/2020 12:44:14 PM
-- Design Name: 
-- Module Name: db6_gbtx_i2c_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_gbtx_i2c_interface is
  port (       
    p_clknet_in 		: in t_db_clknet;
    p_master_reset_in  : in std_logic;
    p_db_reg_rx_in : in t_db_reg_rx;
    p_gbt_encoder_interface_in        : in t_gbt_encoder_interface;
    p_gbtx_control : in t_gbtx_control;
    p_gbtx_interface_out : out t_gbtx_interface;
    p_scl_inout 	: inout std_logic_vector(1 downto 0);
    p_sda_inout    : inout std_logic_vector(1 downto 0);
        
    p_leds_out : out std_logic_vector(3 downto 0)
            );
end db6_gbtx_i2c_interface;

architecture Behavioral of db6_gbtx_i2c_interface is
COMPONENT blk_mem_tdpram_1kx18_8b512
  PORT (
    clka : IN STD_LOGIC;
    rsta : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    clkb : IN STD_LOGIC;
    rstb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    sleep : IN STD_LOGIC
  );
END COMPONENT;


COMPONENT blk_mem_tdprom_1lx18_8b512
  PORT (
    clka : IN STD_LOGIC;
    addra : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
  );
END COMPONENT;


--leds buffering for debug
signal s_leds_out : std_logic_vector(3 downto 0) := (others=>'0' ); 

--i2c signals
type t_gbtx_i2c is array (1 downto 0) of t_i2c; 
signal s_i2c : t_gbtx_i2c;

--i2c state machine control signals
--type t_i2c_general_state is ( st_idle, st_busy, st_stop, st_wait_for_bus_free, st_set_reset, st_set_address_msb, st_set_address_lsb, st_trigger_register_operation, st_debounce_trigger);

signal s_gbtxa_dpram_read, s_gbtxa_dpram_write : t_gbtx_dpram_tmr;
signal s_gbtxb_dpram_read, s_gbtxb_dpram_write : t_gbtx_dpram_tmr;

signal s_gbtxa_dpram, s_gbtxb_dpram : t_gbtx_dpram;
signal s_gbtx_control : t_gbtx_control;

signal s_gbtxa_dprom_tmr, s_gbtxb_dprom_tmr : t_gbtx_dprom_tmr;
signal s_gbtxa_dprom, s_gbtxb_dprom : t_gbtx_dprom;

begin

p_leds_out<=s_leds_out;

gen_tmr : for tmr in 0 to 2 generate

s_gbtxa_dpram_read(tmr).clka <= p_clknet_in.clk80;
s_gbtxa_dpram_read(tmr).clkb <= p_clknet_in.clk80;
s_gbtxa_dpram_read(tmr).sleep <= '0';

s_gbtxa_dpram_write(tmr).clka <= p_clknet_in.clk80;
s_gbtxa_dpram_write(tmr).clkb <= p_clknet_in.clk80;
s_gbtxa_dpram_write(tmr).sleep <= '0';

s_gbtxb_dpram_read(tmr).clka <= p_clknet_in.clk80;
s_gbtxb_dpram_read(tmr).clkb <= p_clknet_in.clk80;
s_gbtxb_dpram_read(tmr).sleep <= '0';

s_gbtxb_dpram_write(tmr).clka <= p_clknet_in.clk80;
s_gbtxb_dpram_write(tmr).clkb <= p_clknet_in.clk80;
s_gbtxb_dpram_write(tmr).sleep <= '0';

i_t_gbtxa_tdprom : blk_mem_tdprom_1lx18_8b512
  PORT MAP (
    clka => s_gbtxa_dprom_tmr(tmr).clka,
    addra => s_gbtxa_dprom_tmr(tmr).addra,
    douta => s_gbtxa_dprom_tmr(tmr).douta
  );

i_t_gbtxb_tdprom : blk_mem_tdprom_1lx18_8b512
  PORT MAP (
    clka => s_gbtxb_dprom_tmr(tmr).clka,
    addra => s_gbtxb_dprom_tmr(tmr).addra,
    douta => s_gbtxb_dprom_tmr(tmr).douta
  );

s_gbtxa_dprom_tmr(tmr).clka <= p_clknet_in.clk80;
s_gbtxb_dprom_tmr(tmr).clka <= p_clknet_in.clk80;

s_gbtxa_dprom_tmr(tmr).addra <= s_gbtxa_dprom.addra;
s_gbtxb_dprom_tmr(tmr).addra <= s_gbtxb_dprom.addra; 

i_t_gbtxa_dpram_read : blk_mem_tdpram_1kx18_8b512
  PORT MAP (
    clka => s_gbtxa_dpram_read(tmr).clka,
    rsta => s_gbtxa_dpram_read(tmr).rsta,
    ena => s_gbtxa_dpram_read(tmr).ena,
    wea => s_gbtxa_dpram_read(tmr).wea,
    addra => s_gbtxa_dpram_read(tmr).addra,
    dina => s_gbtxa_dpram_read(tmr).dina,
    douta => s_gbtxa_dpram_read(tmr).douta,
    clkb => s_gbtxa_dpram_read(tmr).clkb,
    rstb => s_gbtxa_dpram_read(tmr).rstb,
    enb => s_gbtxa_dpram_read(tmr).enb,
    web => s_gbtxa_dpram_read(tmr).web,
    addrb => s_gbtxa_dpram_read(tmr).addrb,
    dinb => s_gbtxa_dpram_read(tmr).dinb,
    doutb => s_gbtxa_dpram_read(tmr).doutb,
    sleep => s_gbtxa_dpram_read(tmr).sleep
  );

i_t_gbtxb_dpram_read : blk_mem_tdpram_1kx18_8b512
  PORT MAP (
    clka => s_gbtxb_dpram_read(tmr).clka,
    rsta => s_gbtxb_dpram_read(tmr).rsta,
    ena => s_gbtxb_dpram_read(tmr).ena,
    wea => s_gbtxb_dpram_read(tmr).wea,
    addra => s_gbtxb_dpram_read(tmr).addra,
    dina => s_gbtxb_dpram_read(tmr).dina,
    douta => s_gbtxb_dpram_read(tmr).douta,
    clkb => s_gbtxb_dpram_read(tmr).clkb,
    rstb => s_gbtxb_dpram_read(tmr).rstb,
    enb => s_gbtxb_dpram_read(tmr).enb,
    web => s_gbtxb_dpram_read(tmr).web,
    addrb => s_gbtxb_dpram_read(tmr).addrb,
    dinb => s_gbtxb_dpram_read(tmr).dinb,
    doutb => s_gbtxb_dpram_read(tmr).doutb,
    sleep => s_gbtxb_dpram_read(tmr).sleep
  );


i_t_gbtxa_dpram_write : blk_mem_tdpram_1kx18_8b512
  PORT MAP (
    clka => s_gbtxa_dpram_write(tmr).clka,
    rsta => s_gbtxa_dpram_write(tmr).rsta,
    ena => s_gbtxa_dpram_write(tmr).ena,
    wea => s_gbtxa_dpram_write(tmr).wea,
    addra => s_gbtxa_dpram_write(tmr).addra,
    dina => s_gbtxa_dpram_write(tmr).dina,
    douta => s_gbtxa_dpram_write(tmr).douta,
    clkb => s_gbtxa_dpram_write(tmr).clkb,
    rstb => s_gbtxa_dpram_write(tmr).rstb,
    enb => s_gbtxa_dpram_write(tmr).enb,
    web => s_gbtxa_dpram_write(tmr).web,
    addrb => s_gbtxa_dpram_write(tmr).addrb,
    dinb => s_gbtxa_dpram_write(tmr).dinb,
    doutb => s_gbtxa_dpram_write(tmr).doutb,
    sleep => s_gbtxa_dpram_write(tmr).sleep
  );

i_t_gbtxb_dpram_write : blk_mem_tdpram_1kx18_8b512
  PORT MAP (
    clka => s_gbtxb_dpram_write(tmr).clka,
    rsta => s_gbtxb_dpram_write(tmr).rsta,
    ena => s_gbtxb_dpram_write(tmr).ena,
    wea => s_gbtxb_dpram_write(tmr).wea,
    addra => s_gbtxb_dpram_write(tmr).addra,
    dina => s_gbtxb_dpram_write(tmr).dina,
    douta => s_gbtxb_dpram_write(tmr).douta,
    clkb => s_gbtxb_dpram_write(tmr).clkb,
    rstb => s_gbtxb_dpram_write(tmr).rstb,
    enb => s_gbtxb_dpram_write(tmr).enb,
    web => s_gbtxb_dpram_write(tmr).web,
    addrb => s_gbtxb_dpram_write(tmr).addrb,
    dinb => s_gbtxb_dpram_write(tmr).dinb,
    doutb => s_gbtxb_dpram_write(tmr).doutb,
    sleep => s_gbtxb_dpram_write(tmr).sleep
  );


s_gbtxa_dpram_write(tmr).rsta <= s_gbtxa_dpram.rsta;
s_gbtxa_dpram_write(tmr).rstb <= s_gbtxa_dpram.rstb;
s_gbtxa_dpram_read(tmr).rsta <= s_gbtxa_dpram.rsta;
s_gbtxa_dpram_read(tmr).rstb <= s_gbtxa_dpram.rstb;

s_gbtxb_dpram_write(tmr).rsta <= s_gbtxa_dpram.rsta;
s_gbtxb_dpram_write(tmr).rstb <= s_gbtxa_dpram.rstb;
s_gbtxb_dpram_read(tmr).rsta <= s_gbtxa_dpram.rsta;
s_gbtxb_dpram_read(tmr).rstb <= s_gbtxa_dpram.rstb;

s_gbtxa_dpram_write(tmr).ena <= s_gbtxa_dpram.ena;
s_gbtxa_dpram_write(tmr).enb <= s_gbtxa_dpram.enb;
s_gbtxa_dpram_read(tmr).ena <= s_gbtxa_dpram.ena;
s_gbtxa_dpram_read(tmr).enb <= s_gbtxa_dpram.enb;

s_gbtxb_dpram_write(tmr).ena <= s_gbtxa_dpram.ena;
s_gbtxb_dpram_write(tmr).enb <= s_gbtxa_dpram.enb;
s_gbtxb_dpram_read(tmr).ena <= s_gbtxa_dpram.ena;
s_gbtxb_dpram_read(tmr).enb <= s_gbtxa_dpram.enb;

s_gbtxa_dpram_write(tmr).addra <= s_gbtxa_dpram.addra;
s_gbtxa_dpram_write(tmr).addrb <= s_gbtxa_dpram.addrb;
s_gbtxa_dpram_read(tmr).addra <= s_gbtxa_dpram.addra;
s_gbtxa_dpram_read(tmr).addrb <= s_gbtxa_dpram.addrb;

s_gbtxb_dpram_write(tmr).addra <= s_gbtxa_dpram.addra;
s_gbtxb_dpram_write(tmr).addrb <= s_gbtxa_dpram.addrb;
s_gbtxb_dpram_read(tmr).addra <= s_gbtxa_dpram.addra;
s_gbtxb_dpram_read(tmr).addrb <= s_gbtxa_dpram.addrb;


s_gbtxa_dpram_read(tmr).dina <= s_gbtxa_dpram.dina;
s_gbtxb_dpram_read(tmr).dina <= s_gbtxb_dpram.dina;

end generate;

s_gbtxa_dprom.douta <= (s_gbtxa_dprom_tmr(0).douta and s_gbtxa_dprom_tmr(1).douta) or (s_gbtxa_dprom_tmr(1).douta and s_gbtxa_dprom_tmr(2).douta) or (s_gbtxa_dprom_tmr(2).douta and s_gbtxa_dprom_tmr(0).douta);
s_gbtxb_dprom.douta <= (s_gbtxb_dprom_tmr(0).douta and s_gbtxb_dprom_tmr(1).douta) or (s_gbtxb_dprom_tmr(1).douta and s_gbtxb_dprom_tmr(2).douta) or (s_gbtxb_dprom_tmr(2).douta and s_gbtxb_dprom_tmr(0).douta);

s_gbtxa_dpram.douta <= (s_gbtxa_dpram_write(0).douta and s_gbtxa_dpram_write(1).douta) or (s_gbtxa_dpram_write(1).douta and s_gbtxa_dpram_write(2).douta) or (s_gbtxa_dpram_write(2).douta and s_gbtxa_dpram_write(0).douta);
s_gbtxb_dpram.douta <= (s_gbtxb_dpram_write(0).douta and s_gbtxb_dpram_write(1).douta) or (s_gbtxb_dpram_write(1).douta and s_gbtxb_dpram_write(2).douta) or (s_gbtxb_dpram_write(2).douta and s_gbtxb_dpram_write(0).douta);

s_gbtxa_dpram.doutb <= (s_gbtxa_dpram_write(0).doutb and s_gbtxa_dpram_write(1).doutb) or (s_gbtxa_dpram_write(1).doutb and s_gbtxa_dpram_write(2).doutb) or (s_gbtxa_dpram_write(2).doutb and s_gbtxa_dpram_write(0).doutb);
s_gbtxb_dpram.doutb <= (s_gbtxb_dpram_write(0).doutb and s_gbtxb_dpram_write(1).doutb) or (s_gbtxb_dpram_write(1).doutb and s_gbtxb_dpram_write(2).doutb) or (s_gbtxb_dpram_write(2).doutb and s_gbtxb_dpram_write(0).doutb);



gen_i2c_interfaces : for i in 0 to 1 generate

    i_db6_i2c_master : entity tilecal.db6_i2c_master
      port map(
        p_clknet_in => p_clknet_in,                    --system clock
        p_master_reset_in => s_i2c(i).reset,                    --active low reset
        p_ena_in => s_i2c(i).ena_in,                    --latch in command
        p_addr_in => s_i2c(i).addr_in, --address of target slave
        p_rw_in => s_i2c(i).rw_in,                    --'0' is write, '1' is read
        p_data_in => s_i2c(i).data_in, --data to write to slave
        p_busy_out => s_i2c(i).busy_out,                    --indicates transaction in progress
        p_data_out => s_i2c(i).data_out, --data read from slave
        p_ack_error_buffer => s_i2c(i).ack_error,                    --flag if improper acknowledge from slave
        p_sda_inout => p_sda_inout(i), --serial data output of i2c bus
        p_scl_inout => p_scl_inout(i)                   --serial clock output of i2c bus
    );
    
end generate;

p_gbtx_interface_out.gbtx_side<=s_gbtx_control.gbtx_side;

-----------------------------------------------------
-- main state machine to control gbtx              --
-----------------------------------------------------
main_sm : process(p_clknet_in.clk40)
    type t_control_sm is (st_initialize, st_idle, st_busy, st_stop, st_wait_for_bus_free, st_set_reset, st_set_address_msb, st_set_address_lsb, st_trigger_register_operation, st_debounce_trigger);
    variable v_control_sm : t_control_sm := st_initialize;
    variable v_counter : integer:=0;
    
    type t_write_gbtx_sm is (st_initialize, st_busy, st_idle);
    variable v_write_gbtx_sm : t_write_gbtx_sm:= st_idle;
    
    variable v_i2c_operation : std_logic;
    
    variable v_i2c_busy : std_logic;
    
    variable v_start_register, v_register_index, v_end_register : integer range 0 to 435 := 0;
    variable v_gbtxa_register_index, v_gbtxb_register_index : integer range 0 to 435 := 0;
    variable v_start_register_address, v_end_register_address : std_logic_vector(15 downto 0);
    
    variable v_gbtxa_data_buffer, v_gbtxb_data_buffer : std_logic_vector(7 downto 0);

begin

    if rising_edge (p_clknet_in.clk40) then
        
        if p_master_reset_in = '1' then
            v_control_sm:= st_initialize;
        end if;
        
        v_i2c_busy:=s_i2c(0).busy_out or s_i2c(1).busy_out;
        

        s_gbtxa_dpram.addrb <= std_logic_vector(to_unsigned(v_gbtxa_register_index,9));
        s_gbtxa_dpram.dinb <= (others=>'0');
        s_gbtxb_dpram.addrb <= std_logic_vector(to_unsigned(v_gbtxb_register_index,9));
        s_gbtxb_dpram.dinb <= (others=>'0');
        
        s_gbtxa_dpram.enb <= '1';
        s_gbtxa_dpram.web <= "0";
        s_gbtxb_dpram.enb <= '1';
        s_gbtxb_dpram.web <= "0";
        
        if p_gbt_encoder_interface_in.sc_address = c_db_reg_tx_lut(stb_gbtxa_reg) then
            if v_gbtxa_register_index < 435+1 then
                v_gbtxa_register_index:=v_gbtxa_register_index+1;
            else
                v_gbtxa_register_index:=0;
            end if;
        elsif p_gbt_encoder_interface_in.sc_address = c_db_reg_tx_lut(stb_gbtxb_reg) then
            if v_gbtxb_register_index < 435+1 then
                v_gbtxb_register_index:=v_gbtxb_register_index+1;
            else
                v_gbtxb_register_index:=0;
            end if;
        end if;


        p_gbtx_interface_out.gbtxa_dpram <= s_gbtxa_dpram;
        p_gbtx_interface_out.gbtxb_dpram <= s_gbtxb_dpram;
        
        
        case v_control_sm is
        
            when st_initialize=>
            
                v_register_index := 0; 
                s_gbtxa_dpram.rsta <= '1';               
                s_gbtxb_dpram.rstb <= '1';
                
                v_control_sm:=st_idle;
                s_gbtx_control<=p_gbtx_control;
            when st_idle =>
                
                s_gbtx_control<=p_gbtx_control;
                p_gbtx_interface_out.busy <= '0';
                s_i2c(0).ena_in <= '0';
                s_i2c(0).reset <= '1';
                
                s_i2c(1).ena_in <= '0';
                s_i2c(1).reset <= '1';

                s_gbtxa_dpram.rsta <= '0';               
                s_gbtxb_dpram.rstb <= '0';
                
                --set up propper address range, gbtx has only 365 writeable regs and 4xx readeable
                if v_i2c_operation = '0' then
                    v_start_register := 0;
                    v_end_register := 366;
                else
                    v_start_register := 0;
                    v_end_register := 435;
                end if;              
                if p_db_reg_rx_in(cfb_gbt_sc_address) = c_db_reg_tx_lut(stb_gbtxb_reg) then
                    if v_register_index<v_end_register then
                        v_register_index := v_register_index+1;
                    else
                        v_register_index := 0;
                    end if;
                else
                
                end if;
                
                s_gbtxa_dpram.ena <= '1';
                s_gbtxa_dpram.wea <= "0";
                s_gbtxb_dpram.ena <= '1';
                s_gbtxb_dpram.wea <= "0";


                s_gbtxa_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxa_dpram.dina <= (others=>'0');
                s_gbtxb_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dpram.dina <= (others=>'0');

                s_gbtxa_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));

                s_leds_out(2 downto 0)<="000";

                --trigger code
                if s_gbtx_control.gbtx_trigger_i2c_operation = '1' then
                  v_control_sm := st_set_reset;
                  v_register_index := 0;
                else
                  v_control_sm := st_idle;
                end if;
                    
     
            when st_set_reset =>
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                v_control_sm := st_set_address_lsb;
                v_register_index := 0;
                --s_verify_bus <='1';-- and s_probe_out0(15); 


                s_gbtxa_dpram.ena <= '1';
                s_gbtxa_dpram.wea <= "0";
                s_gbtxb_dpram.ena <= '1';
                s_gbtxb_dpram.wea <= "0";

                s_gbtxa_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxa_dpram.dina <= (others=>'0');
                s_gbtxb_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dpram.dina <= (others=>'0');

                s_gbtxa_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));

                
                s_leds_out(2 downto 0)<="010";                                  
            
            when st_set_address_lsb =>
          --s_verify_bus <='1';--  and s_probe_out0(15); 
                p_gbtx_interface_out.busy <= '0';  
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                
                s_i2c(0).ena_in <= '1';                            
                s_i2c(1).ena_in <= '1';
                s_i2c(0).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(1).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;
                s_i2c(0).rw_in <= '0';                           -- write operation  
                s_i2c(1).data_in  <= v_start_register_address(7 downto 0);    -- transmit lower 8 bits of starting register address
                s_i2c(0).rw_in <= '0';                           -- write operation  
                s_i2c(1).data_in  <= v_start_register_address(7 downto 0);    -- transmit lower 8 bits of starting register address
                
                if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then 
                    v_control_sm := st_set_address_msb;
                end if;

                s_gbtxa_dpram.ena <= '1';
                s_gbtxa_dpram.wea <= "0";
                s_gbtxb_dpram.ena <= '1';
                s_gbtxb_dpram.wea <= "0";

                s_gbtxa_dpram.enb <= '1';
                s_gbtxa_dpram.web <= "0";
                s_gbtxb_dpram.enb <= '1';
                s_gbtxb_dpram.web <= "0";

                s_gbtxa_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxa_dpram.dina <= (others=>'0');
                s_gbtxb_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dpram.dina <= (others=>'0');

                s_gbtxa_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));

                
                s_leds_out(2 downto 0)<="011";
    
            when st_set_address_msb =>
                --s_verify_bus <='1';-- and s_probe_out0(15); 
                p_gbtx_interface_out.busy <= '0';  
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                
                s_i2c(0).ena_in <= '1';                            
                s_i2c(0).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(0).rw_in <= '0';                           -- write operation  
                s_i2c(0).data_in  <= v_start_register_address(15 downto 8);    -- transmit lower 8 bits of starting register address

                s_i2c(1).ena_in <= '1';                            
                s_i2c(1).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(1).rw_in <= '0';                           -- write operation  
                s_i2c(1).data_in  <= v_start_register_address(15 downto 8);    -- transmit lower 8 bits of starting register address

                    
                if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then 
                    v_control_sm := st_trigger_register_operation;
                end if;
 
                s_gbtxa_dpram.ena <= '1';
                s_gbtxa_dpram.wea <= "0";
                s_gbtxb_dpram.ena <= '1';
                s_gbtxb_dpram.wea <= "0";

                s_gbtxa_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxa_dpram.dina <= (others=>'0');
                s_gbtxb_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dpram.dina <= (others=>'0');

                s_gbtxa_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                  
 
                s_leds_out(2 downto 0)<="100";
                  
            when st_trigger_register_operation =>
                --s_verify_bus <='1';-- and s_probe_out0(15); 
                p_gbtx_interface_out.busy <= '0';  
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                
                s_i2c(0).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;                    
                s_i2c(1).addr_in <= "0000001"; --"00000"&s_gbtx_control.gbtx_side;
                
                s_i2c(0).rw_in <= v_i2c_operation; --'0';
                s_i2c(1).rw_in <= v_i2c_operation; --'0';

                s_gbtxa_dpram.ena <= '1';
                s_gbtxb_dpram.ena <= '1';
                s_gbtxa_dpram.wea(0) <= v_i2c_operation;
                s_gbtxb_dpram.wea(0) <= v_i2c_operation;

                s_gbtxa_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxa_dpram.dina <= (others=>'0');
                s_gbtxb_dpram.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dpram.dina <= (others=>'0');

                s_gbtxa_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                s_gbtxb_dprom.addra <= std_logic_vector(to_unsigned(v_register_index,9));
                
                
                if v_i2c_operation = '0' then
                    if s_gbtx_control.gbtx_reset = '0' then
                        if s_gbtx_control.gbtx_default_config = '0' then
                            s_i2c(0).data_in <= s_gbtxa_dpram.douta; -- transmit 8 bits of register data
                            s_i2c(1).data_in <= s_gbtxb_dpram.douta; -- transmit 8 bits of register data
                            
                        else
                            s_i2c(0).data_in  <= s_gbtxa_dprom.douta; -- transmit 8 bits of register data
                            s_i2c(1).data_in  <= s_gbtxb_dprom.douta; -- transmit 8 bits of register data
                        end if;
                    else
                        s_i2c(0).data_in  <= x"00";
                        s_i2c(1).data_in  <= x"00";
                    end if;
                else
                    s_gbtxa_dpram.dina <= v_gbtxa_data_buffer;
                    s_gbtxb_dpram.dina <= v_gbtxb_data_buffer;
                    if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then
                            
                            v_gbtxa_data_buffer := s_i2c(0).data_out; --read register
                            v_gbtxb_data_buffer := s_i2c(1).data_out;
                    end if;
                end if;
                
                if (v_i2c_busy = '0') and ((s_i2c(0).busy_out = '1') and (s_i2c(1).busy_out = '1')) then
                
                  v_register_index := v_register_index + 1;
                  if v_register_index > v_end_register+1 then -- writing the final word....
                    v_control_sm := st_stop;  
                    s_i2c(0).ena_in <= '0';
                    s_i2c(1).ena_in <= '0';
                  else
                    v_control_sm := st_trigger_register_operation;
                    s_i2c(0).ena_in <= '1';
                    s_i2c(1).ena_in <= '1';
                  end if;                                            
                  
                end if;
    
                s_leds_out(2 downto 0)<="101";
    
            when st_stop => -- wait for i2c master to finish the last operation
                --s_verify_bus <='1';-- and s_probe_out0(15);               p_busy_out <= '1';
                s_i2c(0).reset <= '0';
                s_i2c(1).reset <= '0';
                s_i2c(0).ena_in <= '0';
                s_i2c(1).ena_in <= '0';
                if (v_i2c_busy = '1') and ((s_i2c(0).busy_out = '0') and (s_i2c(1).busy_out = '0')) and s_gbtx_control.gbtx_trigger_i2c_operation = '0' then
                    v_control_sm := st_idle;
                end if;
                s_leds_out(2 downto 0)<="110";
            when others =>
                v_control_sm := st_idle;
                
            end case; 
    

    
    end if;
end process;


end behavioral;