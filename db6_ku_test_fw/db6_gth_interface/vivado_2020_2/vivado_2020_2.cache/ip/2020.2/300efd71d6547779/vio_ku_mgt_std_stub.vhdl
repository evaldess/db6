-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Sun Apr 25 01:43:49 2021
-- Host        : Piro-Office-PC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vio_ku_mgt_std_stub.vhdl
-- Design      : vio_ku_mgt_std
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk : in STD_LOGIC;
    probe_in0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in2 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in3 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in4 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in6 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in7 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in8 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in9 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in10 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in11 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in12 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in13 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in14 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in15 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in16 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in17 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in18 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in19 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in20 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in21 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in22 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in23 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    probe_in24 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    probe_in25 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in26 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in27 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    probe_in28 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    probe_in29 : in STD_LOGIC_VECTOR ( 20 downto 0 );
    probe_in30 : in STD_LOGIC_VECTOR ( 8 downto 0 );
    probe_in31 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in32 : in STD_LOGIC_VECTOR ( 14 downto 0 );
    probe_in33 : in STD_LOGIC_VECTOR ( 14 downto 0 );
    probe_in34 : in STD_LOGIC_VECTOR ( 5 downto 0 );
    probe_in35 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in36 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in37 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in38 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in39 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in40 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in41 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in42 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in43 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in44 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in45 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,probe_in0[0:0],probe_in1[0:0],probe_in2[0:0],probe_in3[0:0],probe_in4[0:0],probe_in5[0:0],probe_in6[0:0],probe_in7[0:0],probe_in8[0:0],probe_in9[0:0],probe_in10[0:0],probe_in11[0:0],probe_in12[0:0],probe_in13[0:0],probe_in14[0:0],probe_in15[0:0],probe_in16[0:0],probe_in17[0:0],probe_in18[0:0],probe_in19[2:0],probe_in20[0:0],probe_in21[0:0],probe_in22[0:0],probe_in23[8:0],probe_in24[8:0],probe_in25[2:0],probe_in26[2:0],probe_in27[5:0],probe_in28[11:0],probe_in29[20:0],probe_in30[8:0],probe_in31[2:0],probe_in32[14:0],probe_in33[14:0],probe_in34[5:0],probe_in35[2:0],probe_in36[2:0],probe_in37[2:0],probe_in38[2:0],probe_in39[2:0],probe_in40[0:0],probe_in41[0:0],probe_in42[0:0],probe_in43[0:0],probe_in44[0:0],probe_in45[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "vio,Vivado 2020.2";
begin
end;
