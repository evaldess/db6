// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Sun Apr 25 01:25:19 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vio_ku_mgt_std_stub.v
// Design      : vio_ku_mgt_std
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_in5, probe_in6, probe_in7, probe_in8, probe_in9, probe_in10, probe_in11, 
  probe_in12, probe_in13, probe_in14, probe_in15, probe_in16, probe_in17, probe_in18, probe_in19, 
  probe_in20, probe_in21, probe_in22, probe_in23, probe_in24, probe_in25, probe_in26, probe_in27, 
  probe_in28, probe_in29, probe_in30, probe_in31, probe_in32, probe_in33, probe_in34, probe_in35, 
  probe_in36, probe_in37, probe_in38, probe_in39, probe_in40, probe_in41, probe_in42, probe_in43, 
  probe_in44)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[0:0],probe_in1[0:0],probe_in2[0:0],probe_in3[0:0],probe_in4[0:0],probe_in5[0:0],probe_in6[0:0],probe_in7[0:0],probe_in8[0:0],probe_in9[0:0],probe_in10[0:0],probe_in11[0:0],probe_in12[0:0],probe_in13[0:0],probe_in14[0:0],probe_in15[0:0],probe_in16[0:0],probe_in17[0:0],probe_in18[0:0],probe_in19[2:0],probe_in20[0:0],probe_in21[0:0],probe_in22[0:0],probe_in23[8:0],probe_in24[8:0],probe_in25[2:0],probe_in26[2:0],probe_in27[5:0],probe_in28[11:0],probe_in29[20:0],probe_in30[8:0],probe_in31[2:0],probe_in32[14:0],probe_in33[14:0],probe_in34[5:0],probe_in35[2:0],probe_in36[2:0],probe_in37[2:0],probe_in38[2:0],probe_in39[2:0],probe_in40[0:0],probe_in41[0:0],probe_in42[0:0],probe_in43[0:0],probe_in44[0:0]" */;
  input clk;
  input [0:0]probe_in0;
  input [0:0]probe_in1;
  input [0:0]probe_in2;
  input [0:0]probe_in3;
  input [0:0]probe_in4;
  input [0:0]probe_in5;
  input [0:0]probe_in6;
  input [0:0]probe_in7;
  input [0:0]probe_in8;
  input [0:0]probe_in9;
  input [0:0]probe_in10;
  input [0:0]probe_in11;
  input [0:0]probe_in12;
  input [0:0]probe_in13;
  input [0:0]probe_in14;
  input [0:0]probe_in15;
  input [0:0]probe_in16;
  input [0:0]probe_in17;
  input [0:0]probe_in18;
  input [2:0]probe_in19;
  input [0:0]probe_in20;
  input [0:0]probe_in21;
  input [0:0]probe_in22;
  input [8:0]probe_in23;
  input [8:0]probe_in24;
  input [2:0]probe_in25;
  input [2:0]probe_in26;
  input [5:0]probe_in27;
  input [11:0]probe_in28;
  input [20:0]probe_in29;
  input [8:0]probe_in30;
  input [2:0]probe_in31;
  input [14:0]probe_in32;
  input [14:0]probe_in33;
  input [5:0]probe_in34;
  input [2:0]probe_in35;
  input [2:0]probe_in36;
  input [2:0]probe_in37;
  input [2:0]probe_in38;
  input [2:0]probe_in39;
  input [0:0]probe_in40;
  input [0:0]probe_in41;
  input [0:0]probe_in42;
  input [0:0]probe_in43;
  input [0:0]probe_in44;
endmodule
