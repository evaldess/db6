vlib work
vlib activehdl

vlib activehdl/xpm
vlib activehdl/xil_defaultlib

vmap xpm activehdl/xpm
vmap xil_defaultlib activehdl/xil_defaultlib

vlog -work xpm  -sv2k12 "+incdir+../../../../vivado_2020_2.gen/sources_1/ip/vio_ku_mgt_std/hdl/verilog" "+incdir+../../../../vivado_2020_2.gen/sources_1/ip/vio_ku_mgt_std/hdl" \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xil_defaultlib -93 \
"../../../../vivado_2020_2.gen/sources_1/ip/vio_ku_mgt_std/sim/vio_ku_mgt_std.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

