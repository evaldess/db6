onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib vio_ku_mgt_std_opt

do {wave.do}

view wave
view structure
view signals

do {vio_ku_mgt_std.udo}

run -all

quit -force
