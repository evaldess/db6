onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib fifo_commbus_opt

do {wave.do}

view wave
view structure
view signals

do {fifo_commbus.udo}

run -all

quit -force
