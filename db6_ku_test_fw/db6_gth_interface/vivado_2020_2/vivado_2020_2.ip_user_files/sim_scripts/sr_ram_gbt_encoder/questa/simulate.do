onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib sr_ram_gbt_encoder_opt

do {wave.do}

view wave
view structure
view signals

do {sr_ram_gbt_encoder.udo}

run -all

quit -force
