// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 10 17:31:59 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top counter_binary_32bit -prefix
//               counter_binary_32bit_ counter_binary_sim_netlist.v
// Design      : counter_binary
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module counter_binary_32bit
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [31:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]L;
  wire LOAD;
  wire [31:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  counter_binary_32bit_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2320)
`pragma protect data_block
kCm+Cru8ozJ0cXHaGWi7R+NV0HnZuuAY5K+fy0NSDvyewarHyD8SJZZjhJC+Iuhznjp04gCfS6eU
xdpOFS9B3O8frBlIWxDyJgj05NnbU6wzvA10e4NC2ssi6uTYRU0wouN1LZ9hbGEP7E19hd335NAU
MlSgdv3UGKjJXtbAJycFccQby2y+bN7SeCrIquLuGBaKuiUQGbg9wVsP+z8/YVx0+gveOEUvX6ov
cCqyYsTcwp52buqCUOeUqmLG2gl8vv4NJY0eYXYtNuExkbA3wNtbrY+0jLkgfGUnbZOHuUEsFtZ5
+waAl62mOguv+woLSUYS4/EOItVPsc3NM85GxttJQItte6TEQSq146Lnaw4C7XRmNebqhavJwEah
8NqSY+TKNni3KGBiN25dkeyGP5nCTSqMrH1fg0tUGiKuHQjQtr1SeHTI7gTfAtRntsvw3Q83VcsA
ZirUdhMjAOXVe9Q9pCxb5IyP51jMkl9CR8MEIHcrmBvyiKFoR4YmCQSTp6cKtKad3fubUaZ+gYuG
Ns/LriUNMrO1rv//dV3T0tBwmEZECiSP08ObNx3wYQi1zfnxHZebpx5G+71J673MBMcNKFVS2Ekc
pPB8znHG8TLkE/QPcFlhGcWozjbjS2GXqPR8vwt+E4BqHSBZbwK5xz+3S6yUYZqAbaa0PRaL5+cM
k1LRbiUdVFeuUCKJgNK9zfASSKIXVFHVz4rNrhx1ryLwKl3p0+6YVLHeu5+5QQMtRJjoOmojhruR
y6ueSgGipYK/0swxmGOzCuh0AOxmd5IDA5T9ikxFcVwxcNSxE/e/e8Q+uIAohaSJfW1ypqOM6e+6
R58348UIYP8yxm4pjibB4AmdMWPh4//Mwl8G1a2YairtZzJNKf1+9vFdPsIl8XtPo7u28JrXxDX4
umNCHyxXRmDjkIhGyUKI94J8mr7e/jCcngMSqMWp7ThvwcwKfTHsuxj23NhoSJlJQJBohBHoMbyX
J2Drel2xAD9cbSUEAo3LiYet5oEZKzEJHW7alzd0qrm4F6J2Khj1ucX2ykwdtowlcDobBIu2hACG
RelCs3XyrIg6LnERPABtcRATalmOt8JoMsNW2MC7bs2/FKBxGikCbUVd7ITw+PayzhxMeUmWmZER
uM96aTpXwCBxJhcaNXUhQ/Pi72PuqYxYZSiVAjp9Yg7oFf/cRCbtL50527S69kBIoV8cSlW49gpK
SrT6C1JXCZXAV0+GB3xKzpPYUr7DHH34SbbT2NCMqOfl9zvS0aew5dFPLJgPqBGTGyzhj9+HZ1Jq
yeePgPjVVtRc6XfmFTGqqDMc8/ZZU4rmDkIxOrY70jV2GDCFYKemN7tBY8a5rkWyhTa7wb5U7ISO
r/MivQ/ecKXfIPw8F7oaUJKoTo6foBoLmCyraDqAgAOBIXQrkKJc2ZXQ6mgKzgr3Jcm7+5t12i07
ALcQkasY6dQk2vGOyGyFOJzQpud5IHyvN+t+IgeDZtASq9/RejEbUH8unoLcqXkdsGpWvz/+XG+r
u45neUYnCuFVO/7K7NLPbdwrEzp9KvZoawVwcyDFiswO29dW2z0zb/vhXVMZW5oxzdHO+KMzjbTy
SNfsTzvB1n90YEtCcft+vP/AGAlcsjxXIlzWEUL/qUSAgANCNjZcpoU+6UXAtmTa0cML7v4NtvSN
EESTl3d8Tg/c1garEB7/XKdXW5olycVF30VgBr+koKtbU7Y3P/pwzi4y9rO4MEFtm18l6HWk++yC
SyBBRpEcNChFQEQVN0TlVOrC1zVSXK2gyNzq6YRK+MGV4TW6zOK6Ums+rD1RoXZlgPOgJ6THgsu/
GTVF+zDkBQrwPP8act+i5b+c8LapIrpzahgdInq+ncYbW6oJ1OHvo2gsO25+o9HxJfMReaLC9fHM
ruCam0YAeQAbef0lQZoX5/OT2gr8BPHQ9/oJUT2kmUQH0mwrRRRBB/+fnsuwwPh9BPJf+4Oo1XdI
oLkaL2arT1EW4jdn/xCdxr639weG9j+f5FSOlEMAKC/IspYK2mgr8P1/dFKy6aQfGXm7QcNwYR3h
4uTFWxgWa3X5qPt6PqsmsDN+nJcOupflvGfL6MW+ZjMbOZJpT0pGtSMWsGlo+3dVKus3r2M7av2x
9VHqpSGFicsI7Oz2d+rdzhzoKmnFLPqVcFH0T60ndbWZewl3QHccNk/p0kG+35lXjYWIIDIv9P1R
gzCnaLh5kbvmcukxdboxvwKzNlz3ZXmthlbSfyLiiWdi/NjOfOO2YR1XH6fa3SjP/2yn5uPfLVpx
4atK/99KjjpzCnKy+BzvUZOHyahb8i1ar5CND+6wr8oBln2W5F1NJa6VcVw9X1oif0adOFw9yuxw
EDnfRsumq0sskUjme73ZxtpUn5q4Feu5zeDiEER6J+Kf27CbhyKaNSZPUK/bVHHbhLNrbRzgaIJF
Au7BTRPy4tOiDFlgu9gBmrH+8ZBwdMCt1eiO6o4xULVo99Lk/H4G1EazgfW3dgv/+bMDS/ghVQ23
M3tTH8Vv5Ix/mUr8z0ExqOpkf9Rs59A4ISVpwD3hA14YPXVniCdroQ/+NaRW79ryVIRV6e2dwl79
TYkPJ3zPDjrbu89GcNIdkvf3B3h9YkSvXzhCXUqzHd0/qmMAHgekrc++F62DsVL7TNWIzJTR6RFW
41JFEX89o0vPkGYQI4ERW8lf9FUMZM+mtrfBDuvX0Dm7A0OAnsR2dehH9E8lNvOrNfyvHa1VE76q
Rd+mWbmAfQAnCgL2wX+T3Ip4xHnQTOMZ4bGmlXF0mXzNppqVmMPj5wtWNYA3m8ai40+vKT9pe6GA
llzx8RccX36CmsHMT6VnblzD0H+i5hgoHHb4iCYoiPrit6bUnEYk9evFaoYXMDcR1dzNb5TatiXx
C8T02bVzX2iPa6pHZ54egJFlEtZYENN5qRkKrGIvTGtTHYsoGk/y2IigOdlU+f6jrEEk0Oc6hLGM
I1PYEQvZMqIfR+jnZJmWxg5hahUtJPyRjsp6KQtpJdPYy4wbusrW8HIY7y9LJTLcpGUJatGuQglc
Btz3rmu//3b4d0AKnQcyz/dIx7To+HRd0sv2cpL1VS/3Xl8VGkEREA==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TgmiIGNUr/yrN35s6FW940ceOPV2CVBKRXdgUm4aopH/EJBt+bSf9arEGUu42VCKqt8L0tIK3z7T
eoapIof30N6hcWPiExmvna61f4HNX1LNpfMROdaE1Q2r+dxMyaWTD4kgNH0+AGkusUdt1N+U0oiY
TglGf/n/aYw7f/OKMqyam+wInzn7FPcP2Jzq/fX0OIRD9UI3fRJHJ3wO0gcHHr7ATzTTSEKfiXhz
vxjEQ1+fC7IHQ50xjn8jbPwogPbRZ/9+W09iIqxoIOJLKTJB2kFOtLIldkpo3yVlX3AQVj3+OvtG
C/8zocpwfThEPB/gLluiB3dOIsWGqZrvPpK+sw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Fuccn+j6xUn1JnNddHRaJlmLbdVMEkYMEEkcOZGhVQKl4jjJjtSfb3uTUQAOd+zUzwb/8qMGUFK+
EftlfWGdQBU0uTQyUvmowB724MA91Ba3hEjZRna+R8F+vUSKEKZamtYMTDG9QRrt/jjpJPaaYbWq
R+XHj57SK6FqIcZkj3tIP3nC8NKskSBxCRgpff9/Uge4VKQirw+2bsSfjhfj7O752dVo1eg1FVG4
DPUmlrVBEgvF22qASh8kje/KZ20Gb4PkPIcTv3u5DIwFrrPvLH2GSakiLq3i6G0h9VNs9JIXLWhi
s8ZidVHLM5cpQ6ztA5XW//6PG4CeFUdh3eBiaQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11216)
`pragma protect data_block
6eefgV1acVMMMyU50WNOcVPJDHH5V8qwLKGaRUzQNDb27ZUVh6QStv/xv4dKLzg4zosIIJoqO2RK
EiV+fcmOrFlKtG0huS0QF8y3w9UbLDkX4TI9N5ySk6CHs9lPL8OafFW/VojI6XrCMiSWUQ6HU4sC
w5i4lg+oo1+It7WY/kr53I9TjESSkf0iZ2GtbOzmzqOpLjv0YfmNNghYxKy63R98INJ03OBjzKDk
KlkZcz7a0puKnbE/IWNrfqkj0fsCYS3/mv6HkjpawukCSmgJfcWkWWq3jY3avtlxX3aHfEYob5R7
CTd6xSk2xMgyNqVWaajONekc16VbOUuY1h/YdwLYvqG3TsXrmkIS5lSJ3Ars3FGK6GnxDN22sjVp
bKTqETkAibgoeLVgeOcE8eBJTkStunrmBg/iapCr9JccM8XOnza9z7CbRFk8Rv67jDPVGbxoGLxF
i52ubwuSVBnb3ZhhrHRFjfdjwizbUP6ByqtRiA47iH+MitlHMZiDkAGd+uveDhCJz+DZrf/eErG4
XnKn2SwHyE3PRN+BjfHqwMngrk84ooAyitvt8z8syI7D8ti+9suwOvY51q9g/SYbKsT7aCThX0j8
+Z9N7UC0OzY1cnUYYesXwRuKcL02li1QRuHW20LGQhabfAU7F53XxywHocC0HcxznYBdLemCoLNI
FWpBNmypZgMFMmm/zvuE13l83ya3AkglKEzmKDGgp/5acWiTWE1iYSIRnUBazH7ioDvA7CQtEeaj
2/5/+g7RHg4eqDu9qxzUAkYmZvbyYYr9Ne9KEqsTTyidyDFf1Z+IYwxjFCCyg98+kTS5NMuY87yN
wOmFQUKOk8wmz194qFm4V9NKCRBeAov9lr4a8DFrWiIixO7P0B2ny7N4yKDwT0zpTxFpIz2tbp9j
grrASc9MIU6UkyDUOdwRk3E3Be3PkJ1F0MiC5oMIOBMlyGqt2jzSwjQBuji4XxT1w0C2kakgTJEX
JhUiqOcoZmS3pLYJzUhxz+fU5QJRAu/OdBJpxKC7OU/aB2ezzIMazSQ77hWCK4hVWqPKjI9/+8og
pPcudWcfSqupx1hOGxEoQGBJ4anBkWvCQOU5pFqRqttON6ugRs3lBFqyrd5fJ/zi14049GuRIObg
m7uIGP/D2hXbZOqQPTNCNQtCnW2K6IOzB9CNGQbrcxux72oLk+M5naxtK0ikn+Z9Z2mf7cdL4m54
06s61i7rL+Vvwxd3y0vb6CVdusFZ/Q4Tzz/us8G5BpT4jJNX+M7hjXwzDXxD8yj+x1ui/5w/bLFn
eTgAkp/CM73A6rwGJZ1IMPqVH7H64nE1f0Mm2PeuUsQB2R84TO7SnJsaE1urirgjrdVWwgk5pPnv
Bqliuo8SrnRvwgAb+WfiwRRdGqcbxxqIZX2xPgvG/PklvBAlXarwagz2tOfnWdlDVcsaYRErYkj5
ukQNDu+eP/V43SH+maD4uvo9UuOfLrOUTtBcV6E8/TK049qlzpsp3bWwI9Bko73mIEK7VAX1+U2x
MOxOoI8kB1K1aU3AGyliPouBPbWy+tnUPg5MYQlq++QIFqnKbxa7rJqwo7fxXCFB3WjzsptkFb01
Fg9PTOBJTUrl37CtkZEitde/iMqfIS1krYU5965FkFTw2AFhuvs9uhyCemOWtVQ+7E6ALoHit5yH
pNLCHdllLmvDlMgHbMzaoN53KrlZ3ysvVrIHbKmKRCvv1lumlpMYlNbGmFVAZFXx8GjncF4C53IY
fw6/IvgCpoBBZ4U9bAJP1TcEExEJE0IarJgh9sE9+7yrl1bMSiLzoH61zS4ETYxHdhxCNE88jCt8
Z4OlJsZEJiyBOm10b/sl1ZEmWxAD9j2veNnGC8r5ayNDr/Mm3fYDhs1awQEfmFYYQu4m043l30mG
DOvCe4S43eQs1D8at7YP+OyAiihfaxHsqEEpWlhnaSTa9THB093kqoL2L8cJnJvjc2jv0/1vbkmL
4l6MVbBEYsKyqdw2lNGJ8oU/VsOUAHX6S9PUNM7+bu3t/eFyHcYH8ZOeFvIjI+DbXXkm8IKXkZ1h
gUFKE7rdiJEh6x5Yr57Px7OoE6bdexs3YiomiN6RDFTLFF3iJAVsgqIYT8Kyad06cVvBpqERnVZ5
IJLYziqunUsqrtwUHN1wdtDwL2BmssHwelv6qzsURF6N03OBD4acCofHng3CXnpKgXViPE6L0T87
y884VlIybfh0WkojbgepU3NFeQjLPAvpXtnvWQZyDoRRwavQI/qsHMoUMcxwYCPaLBZ0fBHvjM10
J04YhLKORNBtajEB6jK/uNm0EbTqWmEAhJYq9pfQ6zwWkuN9hXAK7mnYuGMW7OqljreNff/A6lEz
6iqv9WKxQoyh6gNaXyFPOWC0hZ1p1ak0/Zyg6/th0gLCcZg/pmp+ca+C+6hx5TTyqvM7UQBPXyFg
Km+THIandC2lOyxRsQDGOhZTOvlEvCfe1kk/OKEdm1NmHiLXjxAzV6zqtWoQ+mq4IndM4w4e7RHM
/H3h7rgPlBPK2SLiRnoN315ni1m43SZHahvBoO8X9sjGnsfw9qF+qlS9ZkfLJ0qDTgBR+JrXZvXI
oCkTpxFf1VcSc8z43SU+2msrImzR0DFWND3RoSt/3Px8tMd9YDeKbAde0LORd+Xt2QoRD2r0oAMf
G+Xv3/OXPpCfFd4h1ERcyV82A3tDkJRq0N+RY3PSibuCowqkkWveK6fN20IaL8aTp2gkOm8qPXer
NwAgGvFPuxKUxUh7UoZJZkPdFklwKrIkOYzzRUXuR0dxqM2huXsT6pZrGwStVn2bBwT0+k5fN6h9
1oi9bHkM6vncGHlSm1vRuPJ9DET596oxSqsMl/m5nWZiyik5fI/UHtWntzKcdx/O75+dGQnyA0Ey
u4u8U50BjQxGlf8IlJCgcUXUst73m3G1UUHkqbK5U+FvSX+GRRtrgVxjI54EYrAKklr90uK2iQov
dOdIxlUzQYCiyfvJJRarMz5V2dS6O1P1PVQscx+Z7bbklwiMwELdEKhh4DXm1QB/mY2nvrjlRxCH
GhJEmECrg0fv6kN+ztPhPwM7QWFQHkvwa0ux5Lu/CkOz5mnqj2td4g4wmFZzBBODxfxFzYXItZuq
twmmloJXYP4Q/rJxcQ3lS/aI3N3wk4FkXhebW+OyYyzSA1WaPgIWSPvRtEs595ZeU5ta5ls9Q3tK
RAYegWOpsQbya6n/CHD/jGzKq3fOpXsahO5cO4DOJfn7cFly7VKoHcI0JwzFtmarOKxM+Oy/ovl5
CAPUWvPUfOR4EDFE7EwXeW8PWRWUygZAJg04QRP4SlD8DxU6U/UuXZqQ1A4DPwr+0ZsB4UPUbeKA
Ev9odVb3Igh3xE/6YTgLiEXBC7HWDRiLoLF4YyLJ76G8iHqIqkxsAjEePd7b1Ri+CYKm7lxjcHQb
GqMCzAaGZCQNh58dqUCHn32p1wZS0PoXYvanr25VDeuUr+SFJvKm/yKhm8iwKGFLTvlGnGK0WY6T
VY3ZKl3Xfa56lXV5K6Jn2Fzgzc8Gm2z9g6oc1q/Eh1euRjG1L9JwhKHhvu3btUhdRGgEHayiXm84
6Kj3yB7ZQakT1PWgPESoeTZjkiWhUGgABr/DN/D6bgetAUrhr9f+7/X9B6oiL60XnoaonY6wg3Zp
ll6HYBCzlFUKm720dyLTJ2dHVvN1bAIo1WpLrwAoMkS4D0dhBqFjDcvaRp8u8Yr5M5k9tEqa9cIe
+m7nnWeMEuSxO0WOp9WXSinDgKGcg95bftlmY0qlp1vnjWUhrDgAFDuUYBh38eJ/kk2VttX8a4wV
L4Mf3B9YMdD5TJFn3FDLjVq8ejvY+rdqA/onsvvibf9iM/nfUOQcpmK6b5wFtWH+9lOiJ3rGbhFO
wNYC4oGn/79gkfKDLCgTOHKbGyJI+ShpQOz66kdW/FD0PJnAhoQ52kY+jNdnIt0RXG00vZaNT6t9
1buSzXciQEKvaCREmqZHk0YVKpCCMBaZ1xGSAi4iznNg0TFZ+ZtZcX7id8JhSaRJj7YyoP31T+Of
SOWHhWFIosjqTee0ds8COH4pT+UuNkMQ00pdHBH79YJDhNOTaPs+2RFQz0T6Ryar5AJfhzWzyGI1
NG1rdbqqvB+bcG/kCjSGe1p0BeLOWNJBuP7BAMawGQMobWE4Adqb9klTNMmJJuMHkl+irC0i2jvO
FS8tfsrTbFtyTHt5gueX+DU3uj46BBNPVBpIyuW6ktIL6OPPgovTtRgzam2Uq5bkUWk76V1yQKdE
ncKgfDkpdwxw6jFgI434dXM1yrSp8Sl38ilMKhk2dGM5masBaTtWv1n1C01hpYwba22T/gbP4ckM
360yV5X5K0/Mf7UhuW/jfH4rrUR6NKhmxCLP+mAkG9H16dSxnZkZSy9SQoWg+iZaKVByDA7ezAWi
MFpaoQMtefHVstC/0xj7Fu+aAPsFtG+sSHuStFn/SPNFs31bWB/extyNT3Bu6V+htj8aNrOLF3Nw
wGEBkomVlq1KFo+eVP0A8heskulfNgSyZmnTYuUXSlmYj4ij7gOpGjrskiuDS6V5l+munnEPwZGA
6cpWP+7i9oXh6fTb8X9WreDss7lu7lrVeH6PTd8WcaPZbonJvq8jETRm940BkhwQY/TjB3cLkqca
tpvHUDXIa8CtyqdsmYdVkXSHKMb38tlUDz0bJ6MEzmOsyfFggQh9/rM3jTuVQV3l+8XlzJXq6jSk
+OFKaC7kA0KJSNVjLmbJovsIbHlNlba4Km1975tbDiS1NK0WYuZGtYUPKept3UKDebSfjQu4dCEW
7VuAXVrR7ouG75VL2DzmQI3QIB6RQCyTeTCENdx47IMS5WRCDVfherzBY40UtZr5MagpZCoK51fJ
jcVxFXBex/WPBp8PMmvNwJB7xljTVcwGsCKkde5NQDAHj2lZUhB4Snk194XHvoTaC+M+B228/Eo1
oeSjDDqTX9TmH2kkQE21T4Ebk8areURD7gQ3U0aK9v6PX6FwrQXLWOuwaKHaHWV99NjIuMiaVaB6
v+TGlb7SqEy1Q22Hf/qFEUErY/643/X5DRHdhQ8iUBHgKfJsz2/h29D4gdnl7jLmHClGkiATK7GC
MkZiv9KVhRkmNK2+wvbWQk4waGJj4E+gxqswP0l7QK0Ude7/mTQ78Z6+8JRurDadxvewh2Y3yiU2
J3k3qxzM0Y47EcYf5q0RlC491tmBQMqmcf3CQ/vG804a7njCQJFXG7eYhD9RIzcrtb8qbxeeEuYw
ZF9W8ZVW1iJbZvQGXQ0DETAbvlNzu6Ios/V/f/uuwkaAaQKJDms/JsomekV5c1wot6ZD+fxvb0dq
zGRa2S+Yuze+bU4Moi0G9URsgpS4k1O/V1WTt6dIkkRWhAHiHnrdKAGkdxS3cnvzdAHPklAcGD9e
VgWrH+Va6SBALZ5B3pIot205rrAUrPrOqMzeQm/+XNAv2YBfrBGlUCDRbuYswOEpvK+ZB1tEYPUh
D+SRQirGAPEHuCxgU6+EcVKlhbLHaw95shQsF70HFzjhKTfpa6lT5Nbj3X05fsy+lzbpJ2Ag1jQ6
Z+YyRSrGkYat6P2y874roklg5A0CD3EoP1cYYKyxUg2ACWYbrvAHAEhnFq/CamvYBuG1ntuyO0lJ
63oXFOzbaS+5fxQUCjp071LCfY2ylD9YxgwBXR5GMKxC+D/rZ8KsDF2ZyLEvCE4x2vAsU1dXFfC4
hjsnr3D1by2iOE+8j11lW1eVLE4e9vgqQCWaF26fPBYFCUmm16vsK9/ukIagfnJAx9QbKKq8BoXD
Kk9k+jFno0O8Fx6Unh3dzBEvhJJxUjl44lUlgEB/ST363RE+wfpVAApJ91H7TZw/NM4OKy+o6Fs6
aiuBh9rf6WOcl6cPU07fJb4/7+w5RWy0uycHvN0XBVNwYkqAXLdvww0kv+gaNui86BEjOzdsCoZ8
0Pak4nyldFJFML0moENAB8F9qN0nJpqsJ7jgcHt9G0t/5BNyB9HzkvNcKpDJp9JQhnRz5kff8zpV
/+WfyltFW3O5JKWq2YizOpx2IIgEcpvjAn1CjORYJYB6PaK2uRL5SddBkFBzWGV7apF27MawmouV
HSmwSVEh24UIMfqdnejDXdIeTGi3j98BglnSRGp3bP1ATsifnDezSxnPOJttOkQ4lCHhRQuhSR7b
Ry7B3fY76q1n4lov1HOShHUnWqbIkEum6uuca6MjaY3sGd9mBgc4QW9EBJ/jkbPiM+jMHKYrYuQh
f6TRfiNPdMtEXmWsKT9p094mmXaIpU3Utujd8MOtUkswVfqTgogGbXiiGfGCXir3WDJv8FpQpwjh
eF2XPuo3LGmfkkPFFD1EC9dlTBdS1ev0UeJtDJAXxvU6i9CzcxsA+yfdnvAaYkA06EG4O4+/vX8h
ZoiW2MaDF2kmjiWuxojJXEMxtqAM9Hz3Gwz0ELe9zx9zed3BIwWsv3ExaCmSdIWowfmCvzCIF24d
Wx8a4S21ax6bhUk6gXMxA/AmAhPcjJsErVLBRc85tGlX5Gn+lMBP5AVzPOIXBitInlYsU+NolmjG
Z4yM6c8LsxuxyhL+OTA/dPv2mZ69eZQQaL2kM1J+Vf0IIasKTqw3RiuY8Wh015h21NuG/Z3QfYOE
gUXmkSrhz8rJxRskBRWGGl1IX8pvOV7GAZCIg+JBr+fqX/WS0cLjH5X+cVBFZ2ZlM1XAKR3LrWZg
nY63jPT1rwr11/8BZ8F0ySyM+DqVrzJXewe3CBh7chpqnuiQ8oQbZ32ggqtDQrpxaKsIV9ZJufoH
p+YbNeIHouK98ql+psEzVQtRqt84po5pYn5/h6F3l6umwq+cinkod57CETU3cFLIUQJY0kj8Fzwy
zx9R0+EBwJSpKvCloth7CZ3X0TwUnxljXpmdgTT20HumyVhyLcO6BfG689EwU9KIXlKj8kfUYtRo
lWcIieIJzlVFAIwlvlofUpcDuSiXPqUwCLWavRF86+a10N2eEfgUYplgxhnCGZ8zW2vKWaoqP8Tl
jhnEJWmh2Y23flFBHNL16mJ6orGqE3O5o46zwbTGjyPBk+s9S/t7TU55DVQ/DdVu9l7hO9/uzmg1
930tt03G5Mm8TMnjIUzIimgWeNSBOTFU68sWdkyV5LZ0XpP/MhPf5/mV2FgmDsT0ivNHhy2CuZnC
7l4taz0lrkVtKWqy/E2kyURdvgzVWaSt3dgAsehFdqINLGSDfYjBAFsSwh4jYa3okW+VA3kyeDXy
IPDzPUSFucDLY3yu6r5bxAEfWqy6ExBZGYKv2mEJ29S6tiL3UJwzEzsMcPVufjYT4h6Gjr2/NSFw
79QPOdw47pJxYy9qhGl5eJ1je1unCK/t85xW+8bWePNcwFuBu45kyhoSWWUqAx3WX8BX6fV7GQi1
IHsdK4RPoHxJmCVFTiR8fQrhVFS0ARn2tXzYddL9OUNslL8t2KCGjSnqMidXJ+pUxUaLCDsgUbXM
lt94gXJMqFi9YPG8u1g7ZNKIGzXEDo2WmHQ4DfoEQ2ezNj3KhcyCjdXsBJKSGbJGkLjW+fZh7aiB
FV6J7TjZtfgL4/MwJpqeXqWS5ZWDGu+9EPwmYJaECHvnVE2tZKJhirdDR/+7yiINSW2cCtyk2qfK
RUnjJsE514lKg5FsmBoDOkF54embVGbLhu/hmMqmAeQTSnkPqfJwtwzcfAVg1WlmIn3EDLf0IBgN
4+Z2VO2XIqK4ozJN+ljUBJeguLrCbVvC8F5qoPoFqy8mdDaFBJLXc6rGP5SPcaqvyak4Z6q7LsvZ
d267DcnA4jeVnjeV2J7woPyvQ86eSXTjN7Gf3GafdzBcCoShUctpxoBW9TaK7ohpIkmzOWECPh1t
0SDKFk6J7fFK1sKMrlLpVQal5JxR/WDmRQi1kNFPYAJkevpBUeCSjmpNsfO0F44UszftDCewe5uK
fHREPIOZntyCu7nEfjR/w5kf4ngngPD5KqPIbA/q/4CRr+0APtMiZtaPiQITBfDFHICiyk7Pe3JK
uDD5Q04YAyOtXRs0a50iTJq6ccThwAZm7SAOrSO7w59ga0IW9Nuo31+xe14HyC+fGjWzcJ1Hzdq3
5mnsj4tpFL7xsgHxegPcCc3YHH7xbXA+1jtyAQPzWudl3v5LaFz7Y97c4t8EqyEzejsR5OSCShWA
ICRhcT2DEO1vVzprk1xppvGSV5n6A1aHTvkP2r7tC1bdYB9Xt6H1p3IgzKjp87HlYkcs+nOeOw7t
jueLbs/uktN8CZhCSOiJ79ri5BEyHYIcfuoNd9JhGB95gxmM7QLCiGLOXyilLtftaCvvXZdyd6y4
diLMbd631d6eJP8Ns40awNw2y0VDLl+IAJsxCIbPHBbLmTsIYW83EvOzvn9WHVH5Q0YNfopIaCZp
umy8gHwllQSBJ3SDuACGO7EYaHMljlNi6V5RaGQUtuv7k8V5Kn1dyzTAYBvWokT2CH9NQRVKJoQt
t5ClE6Gy5BEGeMxbQ7lEYijnZoAkY6rQBdsN/l5Hrd+7Dg5AaPFND8jM/14qKo/DI+RpAsQAzkJS
dpsYZalHDUiwGkasGD8p0t2EUm5W1L58CxbpkcGBMINwvRFBCAXkCtuSuTffXd3nlAPuJ4Bam9xr
rHrbro7HWoE1cVxG4Fw15nO2dBbSmL6pCzO08C1zio1/UxQk6VGePEFpDCXoiaHB/C4HwVpGv+LY
pprl0HSdbnQY7xdecIbkrmL9pUCfypDYfHYwYFNqcCbXJhyA6rulxu9AYQrTMD0f0BSfDU0Yxd53
0pH5U5TpU3UHiNwZWZ/m8+qBvLHQdgzhHyV3uZ7Z8I7JTrvSyfUl19T1UX1i6bYxLdzG+6MsXem0
5lC+P48a6MJS7zPcSR5Q6eydatioxW1RJ7rlx691QALw8HCdgAz+88JeEWuOtGjzRnRGFM+aIhsW
/CQaOs6jUw1u7r8AoAL3pJRiMnuYQLQa5n+lDJCGfDM+RPoQPAnpz9F8mHlYgq0c1qAzywjnZpKZ
TAvmtnBzKEl7VHfCZ4l6uslbKNDmoFR20hw/ZzR390seiAJtc6UvoE11pBckwu0nO9FULPuy+/cK
l0Z/8nxomW5fcRyMc5xGRxsm5SG7XUTWmwXHBaq6XKMVReECKPi5vQyhM1ngqed4uMq8Qh7H4dxP
eFDkL0BNyLl6LeyNG9y9FuBIIvJMItTI1E3lXWtXzAVwahhH5wYjaCloBhHiYP3WoynW/Ebd3+0/
y0g+Aah2wzK0HZBqRT4AUDuXok+ZYheRrpqA/OlGTsd714lRhGWjRxRuBYTQIpBqRQg1btLJ/wPr
3LahpDtxVTOKnZxYv+TD1eGpwSO4zCWO7cnGV6kDjgv7EUXfDPbkqafripLeyDBBkHdZ573NKdfK
nIoezzbT3p6/Hn45KriScANtfj0RwMQkVcHl8OHdJZQlcn0WY/McZiCsgpwQvx8IxtMqiCuhlJKd
hkDHkxMaH6zLEHJBze3K00+4LPNl/QkUmYH0zOcuY0nP3d89ZpcgnXYS1SUZpMpdaMpUmQR880jO
W6n0WIArIVbu6XQmI28plCzutVNOqugPjw/7LFDPAYI2x+QNKq/u3eDmlIfgiYEuZtsGMfXO7iMO
gmo+M2z4FD0Dtm4HWEhQjqb9p2PIexWbTLsiUhvbW++C2dJdB3Hk2/jneR6w+QQcLG5Q1VYnkigL
9W3d3nxbptGKI+yUDAE/PiHGTgS7jFhJ0/bIP2XvHjUFpP7s4lXD8Zomabxj9mmCwlRyzyggLpUU
u/eOa8TycWCnYHYQmemFpcnPYx0tvpHcEnLstKSt0HTgT/Cd+F8T/hAvIvl45RxoRzAAy2vzVJ0X
1CWMQ+qxr5l3s3aejH8az3+h0hlLtOB6h1LLZUlhZaX5ltoUljK85p/cPSov2s4pSraLT8vzwfKn
eSJrpPpR6YNX4JFQ/e4TSebo3vJ1UQ/OidldW+inzxLaSbxvkEFdphfxsG+gqY71JPCN+kogQziK
0LqJikAKxlBpbEZ5B+J5fFvQA/tyfL+SER2rQrkjskd2xHlkERhjtXtDzSuMsPPDVv18DxVf7bQq
Rx0TLi/8JalyHV0Jm07gT/Jhi6SxGm7vSFgiKJhw7/f4KXC7e+SrlNlxrQThsZFUOh80vWHcQ5rn
q9DkgKhfWuYQRj6AreYqHqjfKcEg2njZgjkh1AOUIB1Q2ewa0qocZdLzUphsQGWfzws2dZ/7ohym
/QDZUHk5eR7PqyImb7nJwErmuowZmdIJZO+LbYX290d72NTCKVu/pSZfjudEqnDOuF5s92lK/p/D
+Y+75NB2Kf92ogbAhxddOuuFRw0rRcGUVopXELKiuEWGMVVk9J5h6yliMvG2IZt+1g+ChVlHLBWK
w3YKf7Lsd2A2IN69qpxW4HdgAdle4R98z25wrhDDg0lxS/UFvHojMRMVZWJN0QZUQ7RvnoutPpu2
pGI5qQ5cPsgNNUnxapUykmiEWKJQibQxtk5Wbi4JplnewCyRxUzn7qIbgTVVxefEXJk3RXtju38U
DDFLVC/DV8D3qhZl/yzR1FUAk/5xPagUHYIAN7qXoMdBj91P+EtuU26G60bhKGuPhdyRj4yIGNh2
AcvwJCQ8lvoUhJOyJ3+WlVUDRVo4nOdhs+CCWF8c7ogcfM6GGUtjyBCFa/vdxWqU8jmv3+h8wFDX
oXEoWZ9WMtPCtbEkdy4VyH4siAnT7RTPbRKW16go5hkTKOU5i8cXHd5rXN+xafFGAdduSJrN0UVB
kLDT7gydT/+9cH+kOaP2vttuMM997/CSQPmFLwM5g2JSuqbDzhPnVgsnNcWNOHjJEf727ZtwBw6b
PJH8r/1e/NLkT7nu8h7x1CRz++w5VFZExZWclFx7nfIXcPhcjd54mWqoDsSFle740cO4skKVw0Y3
0WG3nCACAutus0SYfj1/0oUIaGqwV2KNtXmsWwE7YZ1o/dE/MZk5GzkRUKon6nY/3D3ve+lpEExt
q2k47pXooLt5lpU5AruvmzkXrDwcD9HPyjc8zq0av7gqxlTSgl5doJKfOX2GvFTpuDMjnnZhcanc
H7xcZFUlGS9d+fDC7uiQK7zefdt85qmhRM5WOStAMPFxSeftqawFKmfb/JBp15mVxi0SG+gKYRs5
fc+fIjCBdIKPoRhIgRdkhj1nWT4Mn52x4Lgouo16TNei1mFcpHAYIoAxbcxUcSjVaxehshwbzSqI
LlLv8wUYwBDAdQ088JjQaWTbDxoF9pywhH/vNBKFlNZT6OpI8whHwLDJYcQszcKt6p1/IyZfFsWo
RMKASgWhcTB02j2l60HQNGvlhWc6AuES4a302olev+lc2kgl4FLAANinWIpmKN4R3F3aL7/yrQM2
AaHBLiYZeebOie054Hly5soUaGXvzdZMGaAjkQQTzuZ5RfzwNhSlw4uNTzKaP/ezfsQpLXrtajIZ
KyPAN/nPTL9sTOnsLIGJMtbt6BVINwVcBwTU2ft2vZ+STt2628fec0L6I7NzC47YDH28q9l+ANZM
Lrea2t3DyK4k0pKvnHTp7sqblb2mgOSSktHzBVH7oHmON6SKNqm3UgofUCVltJ22iok/jclm/2/4
VRpCY4nZFV/KvRYv7fgCbgXtff+d0KRghuFsFrIGy4p0oPFbzmI7nrbtXlZRyeLQTCD+FdlL8axq
U3GyRMhschCOgrogCdQ5sx8wAkviPew3RVDyUysiOITHY/7Qmxvml1VqtlI0KkOYvHkJycUXsLgY
/IZsQu1OZs6uuJEqEwN5jTeFfYM0k3A3ugR9U4NouVaNMLiGp4sFSElsMqFkBbfVu2SY987pzvZV
bjZkfttNBW75/PheQuytTrFAvYeb3mSUi1HRjYB0PStJzB8vyuOlo1Wg/5iVPXbICJBPlQBf2Pmc
rjyxYRyZvupokxhbIRhjA+3OZ6ndFSzUkfJf6OJ1zKXhfFldhDzqyvhw9EiC9aO6WySV/1mSZuCf
snWXqX4h7GvY9PCT5FXuly7lAS+oD9pqhFnG4yX6zBr96CEys0TP3Gv2JeTn3w67V3pvzZOCGoTq
oa3XX5LF+dgdJts5ckK8ApCNtJ8wy3GBGSjvXNK4KUMJZxchGdQEnhbRb+UwqwMlWyuizpHYpnbE
l/JGdAt0uURFExlg+qiqAfKMKYyOv5X85ZiV9UNb6a7hMyfTXJ5yr28m8rvJ9Ctv3kUN2KSppo1h
aoQ80tOHBaqV9TkCDgCOCffuLf6rsbsh20dIH1WIDe0FGis6OKMLXMIThny36G8FP2o2yYfkkpET
iSI6bDoNQQIb2smN7TIUmBIR8/r2UShQIkF/iT7dkI+m7Pzi7HbvUE2mLrOJiGYHKSQ1hvyJd3Hq
KB9Yqa3ZIX1tBH1SGo15JQVQKuKTX0WopwdvGIK0UBzTpVZCAeCNSgMxsHZ0PRNu9NlybfoTS3w6
bKbu7UNWlhtaK9rSEtVn7+mbvqDhPUiR6M97AKtURk325jw1Txka7dlBZnuaAHGmCQ+GYgDEvWl+
IIG5o7SrPCmEebyPpshWTLdL8vlBxSLKNYe3qcAVxW0WAktZjA/EAToPDNOkllUrpQ3sg73cVtOd
vXAGW1Ghp6lA4CyvRoJXAJrsbtD7h8bntdKyhHG3SbLNt6Wenw6XN4YTwhN49xjw9dDDU+I6/NUD
UsJMdYh2EWc8pMYB/6jgqtlXC3vgY6lXKxs/16d59zrjLChYaTVIpnh6+12TV8CUaSdyu0urJvtX
Am157RGZKmRFqM3XRgus7KrwPvy8NXbsSJJ1savV92zkz/qwOYAUZbiKf6V9SC6L4jCjk92GPzWe
QKOCPQQifnRBlV5JusIPDnqLuDmrICO9Eu8iB5PJKgySQNQJ5Eu00y1L34BLFx2BFp/vzAmPtEAP
Stnp1gTa23u7NvEB9ACMWozdTHF+ZsAWuEK/0GF114iRpSrRPqvyByMXGDTozyoT11KfixaA623K
U+XZTn0wFK/sCI7qJ2X422jF30it3wGlk6ugMCouj9DZaQLr7ymIJ3y7GdRdW7SGsPU7Rt21NjM8
U8x15sJ7Z8JSrSTmaUreJPkqLIUt2V4waTbpoOuQyY0cuTwwrywXKAx5SI7Ob6Uj3PuYJhd+fUqG
+4ctT4XX0lu2nbBo5MU/VF0OhaXlWrM1N0XcmbdI54YKwMSpDG/s2N5O3KwoRZV5gsDaeWpXv7CR
CHcgfPYrNAe9OM94fQDbAGMOwZ4sal9bBAJ6gCnWwjV5Hp0oEXw2/isR+tIoqH8BAI/2Od9/3Ue8
Ym8IPDWuyDdRjPRCVK3j663Z90uVOBV1Ae04JQlpBHWzLKjvri5ZxPSVMQb1Yr1/Zq3ckF+CaSRX
UxhJGF77Hdd9TmujPB06I1ofSlZGKR4iAVaP7W/XobeGboCvOumcewnke6Jm1hbBGcmVFZsN/1z0
aaearXyGIspINkl1EArT5uasrdDMyj1gR6jVlWy4xTpe8DNM9QVyCuWXpONCaM45vhClHMeIkCxP
Iw1bAa1T0TUKBGaMRbBTlDpkXZrE+UDPlUwItCSTnvIO+gYWV/Aw1HwNZpe4rhKnTuN+F6ghAVjU
okVYEaMIzZuDEiEsqpx1xEJ54WT84rhEj3d9ubrMXDtV4pcU8//8TnG2nldlpkYFVHp4/ZbTPGxY
JizKfSu9dW6uBCGc/2493VR2XR0A/7vvfIagluMUvXNAv/VdZxegQcwPykqJ+poIrTEw1uuyY7XZ
AKpGziFjDP1EwC4T7p5xxq3wnKQx4vaXydsttBEtvkyJoVUtcnk+uu3U/mAIjsEXJrE4X5B/NIRV
1I/OxXnyoYKthpKkMLmY3xvqfQIyUl/JjmADc6TieYQxGQ5zwu7BlQWN/GvmrsgErJd5OBeenqIH
nU5ayGxNMAa/rdxgGSqtYccdMvxfLQI3X9gNu2AiGMhQKyuSkGOvtgFxNy4bwL5lbM8doXYNqjql
r8GGEUC11bHEB/kgxSYLTTGxEBx59FSHneJpU13XMF9lH7e+bjOsHmPqmmaShmu2jMWhvu/iOLly
EijtJSrhRajdVDj56fMJhBk4qpPt+3KWMPY+gfGKgD/eZU6RtrsWe0HfB+7SBSaiqEN0m1rtjafA
ukcLhg/7ektj2cSLLnuQ8k0xNfoODQNrO9zHFMztCCkrA5oubIoSAbz8dcalurs/L4wfaMK4LMEB
M/Pw3DKOYmRfP9coSgwnDwKQgnisjZAfK0aA2w1iMYsuo/6cLWXEMncdaaEsqQhCmGqb8bkj07+G
gFTc+Gwu2VpNKb2pAqrDHbF1xE9uKIl1rpkooy0SnfgYOZTKPbJDyPfk34tM+7Blp5AOmiFJe7Sr
Szn+28dY+4lZIfpSDswz+8MnpOban8zOJzvoeuhBgfBZ0Olgc5d4yIKxdkaD8abmLym0w6WsfSGX
l2U9EV0S7/z5yfizs4XICfAItD0IXUZk/9pCqYlTzyqRHgKuJwEzh6gMyXz6jK47BoZzlozP8fVf
M3bVE+vefLFoCN4D73cO3V0GPxJN9hKTWjW5FwVtQzob9+BNnWyyLqrOAnuBjjXX67QdxiA/v9Uk
k14rZ/XB05caku+1aCQzhNWIGEsPnSXnp1I8QkjTk972XZ0DZDuzmWdmcrAERx677fp2mfzCk7hp
ade49ARPoHFiOJ2GUDNRlDYldfeARABbHm9cVa1XRtd6X24ksP8KstZWlHaRwscPxaNKWPyAtez3
f71sStWS3ZU0K3ky+PY6EwWfSGau7zaAC2Iy9okvoZQOBassbcHFaYiYTTNlFO4GjDpxTP4PAwpZ
8EfIr+BD7FMoxG5id033j6RufyqAUBMkzSYr9GHyQy/WcQi4+5u1diKRIhIhdaN7Yqk5YDD17q+z
scTXfarHs8wD94upjLg7B+KTKgI6y57eAY8mClY+1QVVd/zYi3FzXoYIIzFOC/EE8wBp7u3jEupB
2K2k8paAkx2jTD8M15j3MAFjDOx0fVyZTp+WhXG6xEDInhNxSkPyOBE1chE=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
