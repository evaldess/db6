// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:18 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/sr_ram_commbus_synth_1/sr_ram_commbus_sim_netlist.v
// Design      : sr_ram_commbus
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_commbus,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module sr_ram_commbus
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  sr_ram_commbus_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
XHE3IrNUR0rAgOSs7TaneZOCem+xKOaVUndAgQMQ6fiqQ7sNz2l5jVXfMEx0J1E5drsp/vFpyBfK
us9s0XKVnQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
iNP9Rj01ArmVzHoVSW7lElSGoWnbQe/FKLklfFiFiJRRgWHkBTgJfwNby6KYAgA4XLe1eWz88cQS
FukoZ18JES1Zuf+KwL8zwISn6iD7iixfZNEwpWFYjyj8XUfUUjAVZiCjZg8f5vwPfWs79Kh7gZBj
vgDcYNXjxLehTwCVO1I=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
nmobDEi1pust/app0GNcoN+V8y2mMEri09/oF7dQ5ZiEiG2p7rMxs0iS5vx/JpQ6fiI0X0AJUPZb
worjx3dSanWZxlmpvUQW1C+LK9h5RA4c6zjOdaM5qZ/K+NCauMad2OY8ZgcddQsrreoTh1nJ2DWa
TaZPLvv5pf3U+x90B55qP2fEPiqbYkbzpATAH9u4NTH7sLWgjc2AhgaoW5eC8oXtXFv8D/e6aVTG
z+0zADy8vVe9/EfQm/dJ7Jg0DqAR5qYWGcVn7yVF+tPiL3kEf6FJZBjo3JgKIu+qAthsglm8Cx+j
2KVIa2CX5Gw0SJbZkMW71N8rkZU8FopYgshYqg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
sQodddsOwbYSlSsSDMNCYLeaJ51uv4v/ftdtzRqygsJNUO74ZhxTo7+viqM/zY+gFJjqy+vyVh6/
lpYCCvOfPW9ohlsyigMit+d9OfUAHtHOnSwar6P7DvEbD+534I8OBinFHuDcHnDIFirvT7RdkfNd
uCfMWv1oGIMacpnu8DitSYvvt8DCB+bHlF3ijp/IC+P6O1hD15eQnQpsDwpKg6nnVcZHA+6NbT95
rwOncIqFR4E+wPstj6ayfvxsin9AXJ/L3hE0nmxedSpKDKOwBjtiGDED3rRIS/N2OZSt7dsYgyAa
MHSfsznlBT9CuauHVihH/u5MN1losnUyYm2/QA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PcTPY1NzlVv/1miCbWVLH41v6m5uRKf5NQUVNklgE08sx21KGWF+V/ICQGqfMvIC5eom8kSFM2HQ
dFf8l+zO8zFaHEcwmOu/VP5gnGydh7qelqNx+0jPz05q2jp495ez4dMFlOZ8sQGQEzx0VockI9xn
YjRJ00trguEtLmc6trk=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lmC9ahCx71j1/ZSeKA8Rkt1tIlMKGNu+RHHj5Xtwh0bt4FfcPDS17km8+8ppXi7OUTyBXSIFrdK0
NooakhmRZCmMYOTdKwnxgk20HqIlahm9Iu+bxjgvH97W6T5jJcYvFslglttPbZrvLoRpnSfUfQT6
o0EtaHvsEFdvL9+ScRUKPku8EqkOu2Bw/VZKo9IMnl0FoU5KXba9O59tKh2rkrbNw5L2gwOiI4hj
K6KuGhkZNMCIC23+bh94VLvhhAbeZ4zYdMXlsjm/BFrp9rW2/KEFj1X0Rlmh/dk5PzuDb5p8oOdz
YKZejj1J0rHlMYssmi6qnwXn/kI09IersaxdRw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
anbwWs0l97JVYhigoT6et3H8TOlASkW/Y/8eTKUdRC9TcUSfTU88XxtY8yyw1fQpzUYR2pxNi2ri
ijWnRd5cdXyd57zrFR97a5gvOC1uBQO+VwZqLcjkcD+uCBspFim6ZUmqCQtPaJptG7SMYEatmSeu
5AOckCi1UQBo3bcklZM89hRwua0b9rPBtFacTvBkGGMEj+3Kb+3nEBjrhaIJyprIebvMvsj2unDq
NZN5AyhAJSQgoJgaiptXgMjTKV1UKRQ+AUYG3Il2upp7ugSL5p+QJ/8P9M8v4jzmg6XOd+GGtyl5
iWC6yFcF9Yjeui98q9M6xYivbpBmKndva6F27A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SEfonlyNG8YAcVnPx91iCPk8borIGPaWiJLZAjQ4ei/rFpUclmCrmdDaAEKl2C6egNjlAS0+sjPS
Y+zDUbgB1zmvlc/tdhSobfHENw4E7nVpOiO3LpH0RNW+vE5gVHIgH14HjipI+MnMpA0WPM1yKTc6
9vNke9I8uopfYKPwA83sQD58OW6+jvJsOUI+g8qfuRMbZKYy/Y+NS2tS4ypXR8KfAWW6gdUxjrnw
P6T3WgTbG/zxJarG4sORWn96Yc1NAiD44AkpnonzeL86+briHkw7CsuzAVLHENNjRtcIeC4zYXDr
LMlHg9gcMiK++n43ZX6hfeV9cJnsZRPwcJdMvA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lo9lKufC+4lUbxCisEYQ3GipTP95COa6tmahcp8LSG8DdAWaHT60LT7lpmYwIBAutlJSIqVJnIHn
qUrADSaI85BggWmFFPiBJ9l8F429HJ2/9X1wD1vQmQTxvt/NBuo22uXQ/9tVB5jGm66HwdD7M91B
vQ/PxfdS7joZd4HlMEsJLq/DbvxI8yuhcPiR9juvFHiU66JL+blx5ETQSQ7BUFQg9UthtE/ZNgFO
J3eLiChOF77wzbPzU9J9Ypvm/Py5gy7KUuzfP0RlH7s+PK7XKwdoCXUWxfvIJ8LKfFQP+lp1RpWV
4tEypdUV2MqqFIbhXuNGlk4AdOtkcO7Vh1IvXw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2112)
`pragma protect data_block
S2MZ7c9ALgqJmj6QHdD9dvaO9oG8DRN+D7vGOvUlyQMi40IgPjWDR6Orw736oQD6Irmq4/Qi56cX
CADRAVq/1M5yCpUNUYlH0OJL56YJREQgWKcozDXZS1+/nXN9n7PhJOlz7UPX90a3CAWhPjocFQWp
sFNfynGsYtxaolcpIAcoJODr+mhNe5VxzkLImd9jy/QHeeXgTDNhFgLTbVIcGnriaE7h2SOdCoL2
lNw8D2HdbLYNLaw2Dz4hgoN2D6h36tIVgG85kvm34BY/1PUjCmWn/xPn4jGF/8N4QGGsdPvf+jNk
TgDoxkf26phnLpTRwXvd0tMEUquSbUffaSLu2Kf0lnxDSE7Ek/L5b40VmWTcu+q9QsvYHOtxyxfe
UFmJG/Vh5wQgYYCaXmeq1alylR4Qle8OdosM+pgF3uxgJbTvKzp7M0OqomgbzFBTnX18/4bMEiPL
44rTEJTWVidKrM+4IjWkgNUKXgrkzoApz8IA2jC2aeT3YA0520OXJ9PLQv++hr4vYGM2t98ywnKS
81b++LgNBEQU/dyFwx6Z7zIOnj/vfgwWeniuSzS2xzWUTRNww09LVyc92x2zSPQGgiUO632Js6gc
U/S5A7KBtL9rpCX/O0SPlRQRzYkehnZ+Gs9ygs0m3DYyK1RyMwHsViw7kaX5lRCCUBZ+R5/tqhOJ
rsefzmZYMGYBkAR9BvllWJOmd9Sczwdc7PPsjhTJWxllT5h5sUJ4VSgWtMB16k6/sIqiVLDEaWfl
TeHHN3+fwJy2nf8Qshy0YQ91i1jxQ+utcZoJZc5OAvvxRDDaeFGp49oG2o2hOS9EJc8o61VHpkbt
aNkcdItfOMZ1rHLDpQ184x1Xqmeqs+o997siDUORn8Xwbe4ip7QRIIb9KSSxx4YRGLqOUYONt8cC
StYvWExZu8mB+OZPBb+imb5heXLIcN7pt3duE53rrzB9pigyL4M3feS+qRdK4dy5D00EnPVv+rgV
lb3eobIG1IPS8hdRNNOTwryotMGGnjghfVJOfN8EI2gihVfKxXoXOPigDqKP2y+ETwuI/FkjpuQ8
LyQEi1bf48Kncd7l0O0jfbGFEpZCmrgOvFcQ0USOnUacT6EgVDObUYFletgtuSOW+5YzuG+VG2sP
CIg86Xunk8Td9i+stf+WZrEMRD7NgekIyfEpiePLUSVu2Tk2cnFwmCR5FJTw5+txhLf53PKvwZUQ
20zXkp7rjyASZNz8Pf5qYAgKg438K1Na+hvm6lsEihlGlsG8dApsPXFx4UqjlqWgdh7ARkf9ihr4
6+Xfa8by5dpyXIbvMO5LiZMNHjJrUOpCH7sdvaIW6kfk6HYq9hBotThu08QzUICdId25UcmOrbwl
6XPW5zjLcHe45lk7cbPYXJFcxaFThZ2LZNoNHD5O+g3tQayu8cIdtNBm13pAMz2wGV2FasV4A2pW
/HAQJ4vVjNX5qe7KrNI6bS/2e4SYbQwzQCnQX97xygrmh8hy/4hEiTuRK/2+UxrshYVgHc8X2Xd8
+yf3SGZhwzstz6mvAjYEYjfBUaca2imMyfIOaJcgPX6ijn7fbgy5NB3Hs4Kpd8D+PJFRIKc5MRor
MJ/1Vxd7KK7lnK2dEo8xkjnCjFShuXvpDkDKEFbzi9gIcUQsejSbglIuwJU0EQSYCecxmfc7EvYM
fevixpQDObythOw+/wHGeLbls3uiQPELW+yD3oW/F06ECHmTHlAUMe5bjIjcQRRgDbs1gsKZn16l
8UzjCfiqbVGpUy+fsXQvd4HVRqH5n6O4mQMNr99JPd+W1rOgZuaE/I/UxIEOnAUBzGaEl2aO9gaP
Jq+UIxbF+GFE9jjKZFTHdPwMycBoX3YjaXU60M0IcrUOvkbrNSBKz2262rRVPGVi7qjSez89rvEg
bjRZs7dO7vwQ9wfNpcNN/Ch/CoSimPlmzt0TsXEM19A9JbIR8oUblkHRaJGsMQqby+NjHXCA3PbJ
Ig+w5XcUY9HZd4rnFHFt+kbQFKs3yFQE2StCNDk/HlUWe3l3y1SlSVAMvrgeqryrVL2IRivEydLu
epbM2VrqE0BrM2mpMSe0X0XWOKFCVlplDeRx/SZgKQh+hT56Ua8TXOB2HB1tGm18S9miJI7/d9z7
bX4tO+XZp5Gt3sVuffIHVDGMs02QuoMno+oXcDJDUz9c7jADvDqfrIYuevqy8PUTV0s7Qn0JAZg6
+PWZ/+OvgoWzBm2k18xSsvMYO0FNAZGUdUPgOf0nkTLCTLJ3HtyE5nqwzHjaADoTt+OOm4wZ5OJt
Q7nlgKHPpx0j4VbKWeYag6SGX5YeUPXEXEh0zxAjrYW0AlcyQv9f/FALG/vFjZyqcUkaxlFxIqzB
hKH2tYRN7nYdG76eCqHbzVqUKh3zaIomQTX2pdm/Fsn1dL6XVh7Ici8eXqWnFVldYD1IU4YER2d3
an4AO0wiNTMA+4DAYAJg83lnopHfVkPMXPvNP04IyTBtco2wr2XbSXhXne2r4hHhy8PZG4zVZenK
hIGBMeNerUF4vrq0bkjpNyWv/RzZwEMgi4BfZLng14Dw6l3iXkBGP7cOW4yGmoBnJX+0csmnWYmm
lYuzhwUK1EalqNnqmcneICmRD6K+0nA+yQZGujnBsfl5a0eAeud7NfkcTC7i9A3kAimfPfyy+LBx
YN0rG+yKaPce3KebGNB7/gYuWN/Cm7YZUILHf8uG2nDSAdWkyn9UAYJlcw3gSJ0j+UolA2IqhEui
2VVLWwGeQNMbs1P19TndL/O+qoD3kAalfvBnl2+TNg27R37zW13HHDi6UZhTCxb5N1f6OwiOLvpH
wj9W
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nwI9apodsxWnt8/qZ84l2L5r2ru1rYRvzH+cIiU2LZ7ZFrYGVhrKUku8GacxvPmk04mNLHGAUf3D
0KN1yrZ0UA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Sm1hR/bXnEX5hSLJC+m0q+qTo+GE1jW/bGh9GYODVR1B61WO0x3DI91rmMkLB3jXabqZYmZaVRnk
N8AiDf+w3tD5cTm9k3UfnHfkmqEgj8LBJAWCYHciLWzjmW7DKTQG5Copg5YaoAmLrkH/R11p2QBq
US3uTE+2f5z8QlQwimE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
y/EngzI5VWuiEHV+TKhmZG2qH1QkzhsLqS3InhpMlNY6l/FsFenjJYgIcwfRB5cHNIe7FLSQt6Ne
y3HMmpsqF6xetN1AMKtt7yIa7k99d/5TC5vyU4dMYs9g27cqHYJzk93esgZCvjIZLHpcXw/tu9/b
4U5FbTjst4GUWQQ7e+FOVWa1BC4H7jo6ZOE8mZ1oMeTUDMRBFFBQWv4xUZFg+dKul2euXKFScShR
h6tknaycBcdNbA+6dQJo+VgrTTewvfrkpNyifPBwk9vIitRhFkJJJVGsR6T+AF/UJfY5dEYYFuu5
J288ggKjbjEUNQnIyNWOpZiuhpClTTay3laNkw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
htKUMvAlzdN4BbAAeNmEM6Yr1UUCORwvd6+1cV737AnX/e5QyMGFY1ZuaVzrrzfIKK+VWd/bFDYR
WeL3jKvGUsyl0cMQ9jcxLrsCI3RnUD8yDbbqyDu9KMj34D7UA/k879CbEg7mJQsE/OUuwmk5Rusa
S2E+UVp+HrYNnNymuLmmn6wOTCKRZjZEMW81xyRvJrDTTqf12SjMprM/ubdETBwwiEzoIwLeibWv
EE77NEiYVwYpzXElBkB+JN+riXCrervjpMbAzHbeomW24pwXmffMMvkj1nRzaEI2QRT19Hpc4iqq
tT7PSLFxC6iyyFn2bd5a57kSCEK5ZaaxszxEVg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
ST+OORnrF+3QguD7AuqTgC907V9FPxT3xpP2TfPbwAQB2+m85/czQ7xrlMYLNRNl2qldRPC2JAtf
yRLJmvKEgyRtR6tv/9gg66CdnvMVGbBmprZnmsgKpHGXcIGIVm6FR+ifL/5pZcFZyTQCKYlbE6bz
YNrIQ8EskAk5YXNHRZU=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Zz8HkbKk2BMn9pYqHdEWEMFHnKjJed8tZnBzajqsks1G6q0CzbV0FSYoWS1nKj84tIU1JkBaGDIt
9sdF4TFidxOJyhtrmpNfTChKxpMr41K8vo0yCOwdi29v/VShuI/rkIBCSgrdlmTBWBEgiBS9aabp
Jqqjo1ol263k6jlcp9rOjaoU+lcQMEXCkHoZu/V2+VWtTqhoSiWKgDQ0jJptGQig3wemEM16ctGQ
xX4urrzlEYCVTlr9g3mn6x8NgAjEFjJqmg1uE21AWGXfsNowkj2dYZLCXuVTF108ULXlOgx8TBHk
tPYc56T7eylPXV3Y05Z7agtvOLTYldGNSnm7qQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VHzNHo3jyVixjpbjlcbNuO7IrIjCuYoXTAjRb06/SIYnbUS1pXATLQwryf5S2ETq0CYvThlIAGS0
xbNOLpEIhHMaY4VNrUdhUPBHXcXHWUCHudYKaUCB/Pk28QZKLuHYt3FqZh6wdzI6AFJdP/pykVJb
M/Pyyc+uLtqsAqyWqtJ0puNrBSpFPSM5259v7Gum4dwYGluRNUyJPq0CnQOQDcjaKw42cmf2DAtX
CSJb79mvoLdsFiW5ePQbcfrrcT/FhIkNj4/DqMVl2EB85zQgcPJw5Up3lLGw0Qd2Cd1jeq3A4qcf
LraHhfdfhy6tS33yDqFUeXlzvLfkicvxivScIw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ir7vg+6icGbLB3CLLO2WEVH7p5OyaYzRs27g9ktjlLGEA8UZWJVD/LEebYJEdrotzhB8SWmHZMDV
/tU66bmEBeBvDhzPDFffP8JEne90WI2d4WsOz8gc/qUmQrWkWWpKaGeRzRKobk6HEaC+nXg3PqfM
0b03fbE0S205+4xE/rEnuHBIRBfZd3xmeVaB0HKBt0SGPD5SSQQZpPD38QOtCELjuuuA4RtmpS90
kaKEHc7Je6wpd85YQOJtbSfSfwms8QmBrV2vuYX5vgvFoWdrKhFu6ei5xOtYRK3gX3JKdEXLebbV
49uISo0iQ96Wfdc+51UDQD4Z2sSmPF/BKuQ5nQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LpdRmMYH4gdKs52wqPlK6TsP8t36Rz9etYG+uFXIxoYPOw77GvCpHTnPEq4wgKvtHfjSBYM58T8o
VFR+rx+dgG80Vv61h2/ALXu7WMVNRnj432YN7jUfiNGlmdGjYf7j5bb6jDSZd9SGg9hOG322ua8w
FL0iNhZ1+8bqOC5DHZhVoYhtH7wentMTqEBB4I+Xy3zK2H07hbY20A+hZ5iviyCzHMtmQ5LCJzAb
8LeBnGRdOv8ntIJz3n1voQKFpamiYGRWqDwIHC+A3vf0VlEiw8M53hPC9SjoIQqQxSnkzTditbkH
fDStRcfPfMIOJ9yoREe7QoWlh0XCwpflnMvnNg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pjE+lHwc2nKpDi/QM6jpgx42cu1gcltX5Ddn/AelFMAYjcyHV9XocTNJyD3S+qK9d931gC2JPgoY
6t3sLunZvs33p8hH35ySdyH4AbmU21lalynCUSxcZ7Cgusg+mMccHwJk4hbuRJxIAPpVURUlGVgx
YMqCJ6oIO4YUbEce0gl7CX1g2sPZBtY5Wz+a4k4Y81VElYPz4fLr6XKsViM8dvMs4Xjbu3X4U3mv
g1SAav+AV2mNwk3ShlOssYL3zSKFOOHm7DvhpRJzOvw9rsPF4pNQ0vN/WGdU6ER6TxEStMoK/MHw
JUJ6vW75PEpY9WCsoDk09ESR/HtRCAFCXPPsHw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iSGQAY0H5wvw6dJ4sQ8YUWL/YfOl8XAhkKalaCjZjjwepooPAzIhsHS+O6Rrt0Qi7tp64nYlJUcO
241rPxoM+LL+3okfIij3jxQIZy63hrdr5zphHhCv3BnfIkjXEwgJSGtm44V2Sav5BDNP0IC3dkUD
hgZFLg3JHlUAriG87189dN+DXlt3eR4WmniWKgUKL/2Wmpd3rDAsVMgNM2XqLNFvbOM+9H+0+poK
MbPYmr2G950cdlWuiAT7E8lxjQA/Js2cfYwGzvD62Yyp4W/gGKwIIa9Wg5GUNV4jtq+Yb2S9Yd+a
dvhWNd4O6Ks2J9CE5nEvSuPZjx9/Hh5ZkkvYbQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9360)
`pragma protect data_block
ugzYDgJ4fHZtTh3+RyA816VoYTNaHvwJi2UpImLFhI62JT10XvRMvRqzamPyxl8nlO4S/kWI2J1f
lvhIiBFvr7eVFjkp2eadpIKxoCbPCKqkTxnEqIjPOJxF8M2aV5ncTr8xXSesxJSyxBn2xsaqAc9A
wSttHJmEVAQfa7HE7vKMQR3KmdC1yKrWiBy5jkZ+neO21usehXJBWM1RJGt4CCVAGSOHk0PJWt3N
lRlV9KDpU+upbpZAqfK9mITrr/Zz6IRnjxrCSS8oaiFocJuOUboa6q1p1Y9kI+84tMq2IlF7LAxd
oJirYG3zZVvnuU4n9U7z+f3pbtMsz6fCoX1EDJaNhTWsf3yauOSA5SxMCIS7FLhhK/TfLy7gwuZJ
JlBPDyssJPS+rjQG2kGqYdpmrbb1vBwTplqxLJxp+zDDVDnYdrqBUwL0bcb/Q43vPZVJC7G5QZ8Y
sN5foaRSeJIxHBS+JNM+E3IVOIphO9BLtwcuKeDMC6i/wqs35oC2wr/kCn8qfrIpfcyZXPkimDvP
nPuIwmMHjaiatRwTrriXq/9pQDF+8M1YPG+JOQH6B80uRp6Y2E9cFWkV9IBT9NqN0Qaw/JbjtaGI
qRxVjAAvylCeLl5QQn7qelQdN0DwEPdKO2Gm/2Xvn/h9yNkBcgTn2RVxJCz+CTW6kfhx1CEnO1wU
Hlqb8ze1WmrX36bzXXaloQJjKG29E4KWxYbFmVG3QZ8f9vGzBvhO7Z87vDaebNDzO/U3aHQo2IBm
HzcrC6qxLv045g17IZ+9o9SIv040/owYBjvb693o0pPKE/YKhYUhgg5NmFdfW5dzeZxpuBtEEyKf
5GXL3YbSDhEQgRmLOw4RVe7jd0i4JrugiErI+k7GvywVzRNj4odbjSp5tit2qmQaWru3EO3Gut2w
Rna1xeOageG9YUN+cwRL/KWlFNXGkaiSbXv8vSSKl7NO4gsDnRKTy65Y72yfJxlMwiRKTm54aHuR
Km5WuMc9H3YqetE4XnGVN31D19szy1hx4/GOwUDYvoek/mBhKUuk4F343BFrdfjLPSerpTuMuf5f
7R7u8MlFk5Si/1TiuidOB2EZCW8CmxX8dKUObYODZMBYdKUVzVHPSRX/nhHjasLLffp6V/RFYkgl
uJn8YrxhK1oXymHEH5aQ7+6L7mez3N6wQS0m2R7fwR2EEzWDJnxvrJWTEkHkAZo+1aZXqBfvWmYp
U8cUGof3VxfTq52NRL9Hdezf9dOkGXyxR9RZTUmrgbwTP2jSSQ1IhUyvuEs6ugF+45V3qmnu1Jf8
mm0njwArNTylG+JMuP13RVHBQqMkifcIMdt4WDgLhGKAo1ze9ps6ZaEhuA82ySrQ7q5hBAFrXNPf
NVFe0AOWD6929yNW/vXf8q4Zg1y4loL+jBY337Jla9qc/blLW+pTzEEcR7BBMaWM2JAx6NrhMX7B
gz3zql+vUqsmL7UUYoNNhYTUuVOVzciWKCSEiwFBVhgmoWhWTyT/MaJpfg9zNBEQL8gVDjn+OysR
AVYnu+9e/cRd3Lb5R/iZFQtI0yGtOOP+lobUeQg2rlVnCFNscX1E53BUqgZooAYAgx339sdElCjX
V3zX8IQdISUK5KOz+GCODarR2swVQkIr/E3OtxbY5x8ogN3tf6eew1RYKyChps1LQd54pzeUyG2g
UUivGf8qTXReP7Dy73Mk6pm36UCa3NVkbiHtT3YVAlMtUtCj3zURCep6XzktLmgi2kyI8nJpnipT
cCpRlSkoFKtS119XaOF2t4CliFhpucoAWsVPSEOyCep95g4+WcOrnlUEeTuk1molkEvC1GG4RpJ3
LZcuTrvuw8MXtC+0w3jQpy4BwKmMh/v+7ExeRQNT6P14jWCMB9Yco7K4026ejFJZ44uc766fuRsv
VklsXsWc0y2Bmm6d5VAkLgAsXbfyuXc8jqs2ZxUpx9YDQP/zzvJJ1uKBF0YF69tnzkZPAh01HmBs
KYPa5poGqYKbeGkjTmREeNtlUiKsd/1n1uQ91BdlZ0isdyiKn2+HZAGO2CUHH8n0esEGl76t7+RI
SUDh9GdbxvKiIVjxVQCI9v9SKOLr+DkcGiIDHH7CXgNyDOyo3yXRXRAFdCP7zxpJuy9dIK0BdVrB
7XYPfXnT+U1pDw7QWJr7tiYIcE8uJKqV9SlwqIa3qGbSH3PSDwa4GsZZZTtC7hRvprpuD8dqax4l
1M0iCoiDKF1PyirgmjgM1864dcciAjLNK7AYfxKe64N//zIIOZ61faCLP4k+HQnJUikX+x8C4l10
gGCdJbDSQ7nIkbjg7wQ7n14Ep/iES8WMdpDpwUKptAqBGFKZ2EFDyH66/YBdFVuigS8muVlRvwC6
YFnFQgkgTxsLuNmyyHEvlbN9CD3h4ItCfKNUXty/WKFWuCwPuuXQsiXJBWjUG0dZhag4B9czFReL
FHkmQn1ucxQUljcuMYNC9JF6XkfCx2/v/d6J8kLdUSChz34V4n/2/JJ6+QvBoTsCufRowfwK1z7O
rmAuEnFkYDvZdCmz9xOb0nBM0sZgL9AVlZiejaZQZTgcAZAXs8/EPQ64QYHFvIuizLC9B677hLFH
iJK1sDXDmysv5899wSW5J2IKeIfSP9Zag+KQb7LFGKFyTk8RYRTNFXD6/musAk4itJIUPC0o5rtc
KZw2/Pyn1ceZmt51wcWt8jWX+S8gjKTiDT0mOWF5Xq2p3WXWyTxUDRuhyY+dHud+2O032O4RwcNa
p6592MP6FDyQsgoyb4zmTUOgQB+Boy6lvyUGvfi8jIH1bFfU6KvrX6TYxkFjlYJtMOQRU7QSOeVg
5yeJlHdRo4sTgsXy78JOc2DjoL0MbGeHzk1WSJf6AO0+KgKM6L4SuwHqsOD4YLgczQ17WmDZtiUK
2U9VEd8Uo5U97BrD5QM5qrplduQdXBr0HEMQgOz7fC/e5NsNuC6uWBpnzLZL64GXn6g5gXBruyPR
G4pi1A+TFrJTnQkOAcEHjCaPXIaFmNPZIYlrrY+B6qYidjDc+KqowPq20T1hACtRd0SP/QZvd7q1
RGf99cnnAYqCojMmJMA9MzKM3F7OgERje9aG5zPfGa50uqGeZi8aGroEKgY59HwWD7l0oQKQxCMS
P56FiM/WskEYr6E826+wjptEUvH0HK5uykxysfp2G8mY5ZCkkxca2aopDdCqxkgpR+26KM9/mfTE
i4vnanteIfiOV90eDXb/yPKEUjtK6eV/BmIR9Mx7nZ/ZJLxdr73B9JxHV8rcJ87cEYg8Xv947beZ
B+spLupkpNodlfV6fdZzrEsHoDI2rt64I0Mz7yuwwxShdDyNy9lWCkrScJfl7lb1ryrU7TfhHZYA
lEzOXr+JkDo7k0jk6CaIX7hxzIiKz5CLCWPiBxiVBFo5Z+Pmt7OWN125WUe3/flVb7o1agoIgbkZ
v0WGBiWTNDfQkGN67Zz8BxTOTkGrd4RFtzC/XAB+QC+Slq4IjwW3tXN8Yaxr5ELlkuZGrhVa1+nh
10ux2IxY6GeshipfzU8SeZ/gT7eydVeDXB1uSgMdUr6x9cyaFtWHxsPTJyLuBYQYL7TrHbv1Dxs3
kmweZIhh2eaBSYXhLMtmogpGKNIF2+K/dZW3oCZPeYtF9mFRZmITNeoPEq0aXE6zRsFE2HyNTpgA
x0DxW7uQ53qjf4M5X6WE9xJa0htvLZa3497tKv4P/Qd2wCcp8LHdHcYOkRCzhm+FcIyuSrMKCd8R
bb9fyIIPqwnrFq1HG0TUTAgLaBKAIJDBzCNzxUE2T5daQiNTNFQJ12+ICFVL/dGbLnfIV6mBA8bB
MC3W/C92f1s50Sth1Ca0BHmpE2OyBcUuJ06wft4Y3KWm4sZwaQraFEVCPN95l7f+bkVjpGLce1uB
7qpdLuPDyOFWkuXF0Kujzz0lXNN5rs28LqiBDQ1ACnLRblHc52OBLviEVAeffaU7B7W3QDTLAmKo
zW3RbfiFFvVHmwUwqAcf5zxyaKSUGQasXiN/jaVu/wPns72DkWWknKFS2pxtdKt9sbneHe+897Ru
KjbxCQyCX5r2Aj2vuW8JYeoyht7AN5wzxpfyRt2Exb2MswV99avGx/Iv6mlZYbKtjv0KcfapJtO2
JVBjKw2KFVBVAIpGbU+zFsaGt4Mgi1LKSbalL6TuNxWFisJN6wQAqq39c6unAG2CUWB1vX2kshD+
fr8AGldztL5wemfUb8Sw3soQ1n7gPbTSJvLSFpiO+LnxMR93YZrXJ0j/nh37alQnjR7kKp7s/SJZ
DUsJviooMMYgnQubajty857l9sSaxNypVeU8szSul6IB+hrAHQMdZ/4eDfbGzAyKTnSBSnz03sxt
zk8/SseVWnau3Ix9vgQVczk1AjK+LNnIGwrXK6XXUpXbJk1jQRyMOCiTC4DioseNcCuRjljp9jqW
RilzIrwAiRLomvkF3wQsB5ne8eVDiNqZUTSwb+kJmbTCfajnpsiJiByKKZE52+C+Ojo17MJI8N0i
bn927E4XAlQFfnEvEFXXPmJVIpMaHVUyLLxF3nMthMS2z6Q3di4xDngq82u91L/UfY49HhmseiBe
8ZCy3B6d2DYipyLihQ4eGcxszdJmDgOOqDTvQmPMAtx4NjjTl14QpAZNHhJeUyKUVZHggiwkftft
V0oEndbRtQjcRP/8L/zV6mRUoxT7pLJAcJhNsrmgWQ7glcSCj8C8dbNHgA2cVD2HmDzruqpHBmKl
MLX/XUj/dTRhp19QkU/ITMnGNXKZs8O1LLsOMSAqGqxFJBRxJkK4O03f7+SMGGSgzXjFiAt/31uD
IY6ZvxfTKhkRugUplTKpPP3BlYYDBbgNo0+niVK8S/8wvR76eBzab+QCy+ewqdiakm3FDDoQZ7xd
2Uf2EEkujUSR17sbn3HTmoX+C1/pImaCeXeGN/H4Q4AfNINzzB8j5xJLH63NauSdryYgezLEdTba
N8QaO1VPc6FiZzzNmq1Srt+7vtHhtcaQxnmYNhIDNlmWtzgpoV3qPP+DYpIgAGz5InctmQXxLtp0
0h4liwPqYos7EwdT3BaXVuAUXeDEjrCcr5RT04vXTqmNzk0/hg8DRJAZPtZ724lNLkOWQZgb8vZt
UGlqTYvH1nsDbu4YEQ9TvCyrdr+Mx6H9afqHYk1w9vUb0Gz/DWQbvVr75LH4Z+8MKKtPs8DfkK9/
9ucPdSD8XjTHkiEZd+FtLVK1zEmshY0pFsJLvDXabPQBoC/3m8+FCC9+8EltdnVw76rBxlddDo8K
NreY03Hiv3r+BsZ5NTnakDZZLHj6g9M+GaTNchFr9Oo7RD9q7zuUU8yLOtYZzdAVTbbrJfo2MSks
jFKPoC1riFwVYejUvS7N386dBP1XEwUhNXo3K0Yn48ZuDHnAEQUY5osQZEXyL+6FKtDK4UKQ28sK
IO12mcrJIYOs0/OLZnjOGPgk17nbSmViC+N3ec7u9UKwXRe2U6l3tsqvcZqjyU8seU4sP6OOyTnH
Ni3pZ+gmo+1M6sc42nnNh/c1qL8egeR1oGlhxRXy0BECIZD3mgb04sldTmfZK+k3gfIQ+/oaew+k
exMUj5U2no7Tz3L1bK48cclrvFvd+e4hITwy+lOkFy5MdF7/gkJ/eX4EOKdCEy9qhAgCHKAnvxvQ
2EkD517GATUIR6BcE7xTCf5ssQ5Tqh6Y3M6runbU+nL5Q3yE3cYBkZE7ouHIoTqg5sKcka4zpeNM
zi1o4pH7x5L6J0Xh/JOVRZe2/C34Pldwq8MusQyFna5+Ryw/wNRUDO4tBA8aaSWd1C701JKxZJv9
ZA8swXz9P/kOd6MaFA54rfsrz8tST4Vs0sHm6s6m2WMjqRlOR2p737n4R/4VDAPTqKb1kT92+mO4
/K+TkUSLhzTCgilxDpK/E25Vv5yE2n62zcH5djRSTKbw26KWHi48b8/OWctkbY/lSEjaMLKOTBG2
71L2IVwMaA+7HONrnUl5AyJtJ77hPRajRRVa/tsKnsq+H3lTANSLfhjtGdWKSvyL/xrXHO73W93h
nVCG6wpw9HebTZGKdZKzsuNWyiuOO41wbdm2FkpQ3vti0u0bV40N9i8ib7naVHhW8M/9Hpj+mDqH
bJa6Eu6SwSZEkwcuYU1WBh22xvvkMO1vJ5fdJd4s8EzePI6Z4bGTtbv7jkg5NUfYBaZduWZs6SqE
xrRraFjgmkxR3WaO5YfkMK5UZsP5oN5DLux7Yq9n8T1T1/3C60/pqLgISrtJR3B9noT8FJmFKZAw
YgaA3DqioZTdEgnKHVGl39hzUhERJ8RvTVWRBZ3F5AbI6WME1qhq6xnDvbDIo3WGc64Vc/V7Hmao
YRnfW3ECo61DpZE2Sn3HbW75B7U4ZPx0Ql1eSdjm74kGLNpkBWVAbY+pz4x4XTmZUuQhXaAPW2+h
4oUqEcixYAknbY0DoRew1NwrbV61TDJEZdBswYq56lI9xevbHXGngyQ5V/LnRiIuVeBJhLdbgHxY
0wc+DflcdPRy2waNmNPWNA3KpzE5vQXv9DmkZXsrI76HPgXzdN3hRM02tdgw2PMhMUQ+jvAa1Nju
I8f9TfAGsgfUJsrec1SVcmc0Hmh6FOK9fe6gxjAusWQTE4XyTbaQEPwASPgecpt/fofA9vls2RWT
jVm1lkvLwATHCjhRQHtE28iLWl33bGDM/8tUh0wS9+FUxsa6zXbWJOR7SDmCZ3EEWzlnLQatTrGn
JS5hPRFZtOeDJtNFWvkYNtHfzuRyPvRnsSd6Oa5X+6SObixcvq0RZhost/ZxEg0JHwuFjHYlqqC/
IN+wnXDr9jDoqRCRV6uRnXwEhLln0eEo5rm4gW/R4iszg5V/6i+/HrrxZocwn6JojqdQjut8yGy+
5oUwgvYJYUZAyToE23fq0nWsmHX+DcFhzsru3yY+Ing4RgEPI+kxsGYLYCW/zmLEyXt7QTJWyQAq
KjrXekt43yEIl9zO5Q5OmkAZUZ4JaDIkGokcRrBgNrIHEavm3WteNZWc9AvgJjiTuLUQXwOuAs+Y
MBCqpC8vWRgDLH8iJwQZmDUEOm5bmn9mXRYKw/AX1aqR16eRUg7pVco75wo6mUAxhPfOjJuqRaq2
rmAQpYr8hyMBs0UH3K1L7h/BQ/h5JFt9u3B2qvJ0+Nzn4tvXzwpkf8VF7GPAWyvXuQFg+TljNNSp
E/5mFn5RlS4wCsyvF76ROFVyfBAI1Y/awpJsrsU252/GwJpBx3lXEA3jml2yfM2+pzp8OBL05yre
k9kkJlRe21lz/oKCJ3WPMoZ3jBybjV9omJyyjJ+Tcctlehrn9sXZee4vnx7UE92Qrxo/lXOuPeu5
iZ1Ui/nXcNw4DAyI791vTP/2hM+ysRfXHKPXntQR4LsBVJPxUKTRm2O5xZZm3JRXgpz/THJSJpnT
+Wur5uTQSdj5YSkMGVn/kR9L3Aa+iHFqaL9zhZzoXYRmKyD97QA8dlyQSv6eev26rydg8HJTIj5z
5BJc0RltCu0nxMMDeJt9Tmj95JS8c8Z58S1nDqognM3UvtWk2qPj6CJ/kbmProMOox2uXMHfeh29
SwZBGXdZ61t3++gf4ab7S638HGf+v54XhfPupGO+k860Esjpj6hk+nfIpNk1neBWXmxflpZcIBku
GA/5DpsC8N1ySaN7K8jTiZP94M5r1oUr0zSLJBvT70YhIJQR1A1TQoeW8stUE8GWFt+TNWCN4alG
0JXUn2LJZ7KsO7tL8gYF6EFJDcbjwsGhL5772EXRXSGp+6HW1QQbYwYvfrw1YPElqE70CxnT2Zj7
gG5uqDQIFVRDLim8EG8k0/mjf2AMpM5QAtA5/lM7mLbN5mP9Y/QR3Vk/cGNz7gI/E0JL8Jr6Y9sj
hqWnqWcwZNXK+mOCMPHnbiZX1Ss+qh418RexJJPpvEZTBBK39KjET2W+dU4/GzhQpPd151tKPczD
nOM86NC8KOwZIpVaNyKQAP0FNucqlt584ewiv3yhFPNLoAq6XCZE8N+3Pr8Vgu9A8B8vw7JLTn5u
LjbZv16PWapk7MsEq8VwGTpzxQ3FpqOHOBi6LD/qGJA5L0GUtkbNMHg8oKSfs/fZGsnuVexoQqiG
9XgLr8jax37/yZhneQvY8MouAuUFzLk5dYBYCj3YEaGpY+MXZSE3hyl8kUE7mF3/+Scyk7cr1f5B
lGRPg/+w9g2pFU7GbZOdayEkCxrKYvMvBmPSLzVOoBkNrvbFolzPE1wX1Le+cHfqEufz36EUN6an
eKP0lEBSO5sc6g95RnHYoCAEey/JP1F/J5exe1BhLJto5vmFeSQgEEREAAGpgzBN96jX0+yiswev
a+C3yk9Fnwn3OyziQxLi0Hw4JYcFavTyLf0qDmZGMCfTZUOqX3e3+4vj9cVvSERnZYxUgUHj3Wba
AXpfJUeKcTe0NQw/r8KZ6T589jt2/4DW6tVMARxSuF0oLS/3N4eA2AApLNdqgrRmXiF22sktGrrf
slb62eDntDszs5PrIRTt+RnX8SdsCdhieIvVe1QectNlNakqYa2HSNVcBqJ54WLaeMXqHiMyygib
p0tBaV7AddHcvaqYeBY2FM/Xlu9EqZvNzDYyDoopp1Q0ep/JtHyRXqhkEOxAUC6HoOP8O2VM3R1e
kJMiW+4gt0jLi22fOOovQTHDACpmT+6lQR9TLXPDOOxBxOZG+fel2g+DiyoYbnH14j4RogURMCpd
0070C9A/4PqL9NuNd5unHIyvJ0Ve4F4MZnBgXNAM4OSGXaaH9kktD7nKkcPZ5MimnmTDFXjZXl1j
Rua6uO+/9BXp0V8jywUG2MdBV5auAmtRcJKqkPHgMYuWvChicCwwOEG9FRB48Y74c0/IHvrYx2up
zi+V3TAQ+SPIphOHPr9sFtFN7yLQ3J7+zi+zNJiWX+KSF0dzTRJSXG6U8yqL4140LsqTVmLBMbtw
h3txgh/bc/OhIQQ99x6VRmhLPtc14hOmI0HHvHtQGmuLwUE03/BfjeRR0G+FITNSQ5/banhQXpjQ
SHDYOfK+0hoOGH5h35edwPlwiDK0w26gUrZ2KFyst5Tzz7CaUKyE4dODcRHNUtBFTb4wU2zkobpY
bE5+x9GxqQ1rdKtS1pK4fj6Eg2nRZpJxBLu0mIQKoEJ/wQkbkHaL6kINiOrWVpM+tG/sHGYFbqAw
vRlMSC0xvCzEZvua8iP34hoDjqiB3lhpT/iv2X3wMzFKUR0Ytr+QDlYxOyipyyEYxS9ti9WfscjI
h7RLEC/WVSZUeahOiScLvpOhraAZwyW3jpZALDFPGcGRoznzCknndolmKogXb7IOLGWKRnwBIVam
GWB1uELG1BZ1RQnWZgV2iAEtuuQTHg2hTjoTOnl4TgDmBKBElYRWWey29jC4/JmQ43tyYjiQ0e3M
QiGT7XSaoqjHL/5XDtvfTbXoWYXVBL9+gvC9VD1UaHkk+IcQmVV58UBKvJwtHT0SVo6IYA7fZZh7
RQwbP1qrUw/x7hl/ADmn71a7mIMtDOupb8Yw5Qw9ZS9V1cVXzReE1brAQC8/uHQ8P3qjk5WzbHpp
XyA0kQSeMfnNroaU36UWHmO9+X+xWbgDYUnNXCVbQvueSQTbfGjbtPje48SlreTsPUzo2GXpHuR1
Kf5505n4g8aswOIbsx0+2eyUpr1VsE/PTwkSq/hTjeurOMn3pJDqTi/YYF5x1xYa2s1JPtNFZE9t
fQqhCo5zEMr8b+UbHfuaOsN30r+tzIQqImw+2hjtre3QNyANONN/RO5uaV9+7g0ppnyW9k//Zbw1
ZJzZ3nj6avPPDzZRXVUBVDwacN/hRKJhK2btg8pE57KMJhRKaRIJYLGA3cGSQg/gOV5v0t6a/OOS
IOOW5jDcqYdk9flQy7HmoAAOGwsuE96+kxqr8FGHKPOVpb65EGzxZff6EisdYhJlsbg6SYumPeLk
9RA5E8DOO2siEowqWi4K4l0dzqa1uKZs9YfzkwOHRubD4ZdL3lUVl69mW1FBVj18wG62Fb2I2281
hrGSOVdHQZZMnumvKe4MSEcZHVC7NdA6VRGeDHYT76MwKhgmm49VGGoO9A43+Tr1YEtn5sMd7Fxm
6hGQF8ecxrw8R4YFdYiT6du8EznTpBbgOGkDRqBijxQ4A3wI4A55T8hxXe5X3BQH9xCqcFPswSrT
W5txlb+q91gKe5rAkusYwKBn75yRrgrdB+UoEPpaN0csIuA1lkBgx2tQdsCshbc4XDZctHl3vx5a
tdXiyJkXj+OVchSvHNdZa188BCNTpALVwfaRLqOvb4x2+0txXFt5q0JEWREUpzf/WlpLeE0hNpO0
a3BOMjT4TsMcLiRtpMJqOyNAUDjk2KprxvMCJlULPboJqqN5L5h/eKuOIw8UnX//rNybL1jVyVOy
LijxQ77+WKOQugYoVifJk9uTnXdB4QY+EhUFTxTbg8uCEiMoByVZ+OtNh2y5umi/KLw4F9mkL6VY
kdtbbzh6b9pqvRgx4tP2aTHuM5QHKyRe4h6WNDIhXfzd+mIK/ohRKrfs0xllzKPs/ZSzqEppgLLT
jzQdFvBxp3A62+/Ei1oE/iN+CAN0gBHbLFmAuwVJZ2oD0uCOzp/OI45At4A3U4o1uLBo9AUyWf6m
gyQr3mfTPRH4vU2956Hkz72ZvFADc8pV+Od6aIybs5R2I/GSXOCS6xsG4ChlRNezMbp+5LGlDKPi
ySHYMhLd+NucGieyruT1+A/F19jODnx5qCPNgejAYKkJv8hx7uWqR/YEeLbAZsnK2+HGOnzqkR/c
bOXJ48ARc6jHYsfB77wHCNYxVplJLoYzXTKL2T4zu8Ok1YGAxI/UgNGhRACcdDVAK/T98WAoxL1V
iQgNmTnv3nxDi8bqE3oniqBhly+UK04pHwHrsmoaCYnnAlFl9ah+ofNs7oHApvnBKC3MwXYUXRGk
lZK36K9wGwJsLSx4IXvpIHpvRRa8bWf9CKS98a7rRJphFm8lENqmQaWjct2bkdal59/cdh2AuNMA
hvYWmeYMpZQNrJC/tVVxZO3hC1gUju0smnx2fsEaahuxUdaZ6qad+RdH+dMdGnKXIaM7c6V6+YJd
9cpv4+D3K4CL6n/D/QAfX8Ca3n7V6jiZenUq7IPn5v2oWmwTj7vDYt01xlpCtVRLxPWTNdtL+5Fc
qZOxUOsiDPcaz2QyzpWkFi+YnF00Kq27lb9Pak9pXOKkO3Hj6kLL81WmdUigGrnzATBC6pVnaZRe
WxGItpQmNR85OONRBnl0zNK02ukqzsSOlBMzd+f1FHaH0ub9qulO1NuInqxLUlBOUI0veUravEfz
MhKBvgRB3XxgR4H6QUR19mHw+xl+59bAKL4uU1HoVSZL7bkfAFRGmqafYoFZs32gSsu+ZwJHdJ7E
7z0x9A/MRiqZqOflnh5pC34pfW5ONKG9+kKy7GyydPQQTDUVFQcI5BtiREQHxfszkiarDSPSoN01
VA8fu9AOchoOJeMvZnD83ztXrOqt2fwhzLumZGOH13oiCknm2M2cLGS7E3zXHTxZFVlQNqFwFPbi
e7QZxF42wSaRZd2GU4SfePk1o14k2IuyOtAyu2IcTkLTlVX+qjUg5RUX06YJx9t5GYzwukisHM7k
Y+vq9MNYEgt/mDtQ8s9yBIVW+xbmdzwefSR/lGGj8ASZB+AbPtFmyGPo5Hu5MZMUxRKOXch0ORtH
8P2scmhhyoXUwShR0443l9IzqPjkq8MrPKsPTELaR05XzWKdBl3S+GLAy0HeeYMTegXGM5+IoFpB
jTBlBr36/x3aN2LvsgQnnbOOZvfqtfxGu+TBrndcv1s7tkh11eElPCaba8THY0XpZVczLmupZ76r
Fdezw8uhxd9w42qXPNbAKr0ShY3NOiP62xw8rUVWvTgWcBtN3mZUhHclEipC0cubqQ0LtLVLZ4qh
X109tgUKgmi1JX76Xrwk+F1d4fuHS1ycg25M4frJCMJ1Ar5kRXc2WV92KENKXD2Qukofrp7xb3Pw
5vNgMhcitOkvpE2v4D7I0RlnHy48jXEfxn+xpVH7f2pqTcWS7JDpavjR+b4CwvQYlLNLjgrBPAI5
QEPEg95D9Qk9SodxCJ+atIrtcSo90YNlxOAqIh3M3QB8TtjmhI2Ms1SysTr4xvGbyrx7aCEsvMiQ
/Es6hzkmrGWOjiOVuglGM0uiNGlFKFt76UEojQK41SQ1G7uzretQDiBHF2BWW018Ixj9ECmcwwBS
QGQ9tD9gph17YmCrrpocKPzYpUcAQwhZi8ADhUj6VcYG88QvYCJoD3EzfdiEiS9YVRXzwMIganwI
mxdGA28uOrb7eZCNacEFwr6lendu8OWbJvS1YGsppFm3XYzpSQnQzsvRlTSAEEVpAAy7XGDSm01A
CiDr+U5S5xQkyFJsOIOHOGsKembrAyjqG5jqgolB0Enz1OoEFMYRZKOOZOhteGplO5+acfRGAKNB
Rec4ZbBMVTgnmokn6dZWrLsfqePHdLFHT3zBgE3+6UrkymkmzIYuLXQu7ztrKIS36cjXS4RVE+Oa
Dvfot2LBGVB9T9Hh
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
