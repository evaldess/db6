// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 31 23:41:10 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ counter_binary_8bit_sim_netlist.v
// Design      : counter_binary_8bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_8bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [7:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [7:0]Q;

  wire CE;
  wire CLK;
  wire [7:0]L;
  wire LOAD;
  wire [7:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "1" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2352)
`pragma protect data_block
lA+3t6Yq5GaWijiDKShKFOAaPCgbLtrGy47wYat0devc5lk8WxGcr4ctmAdZ2lM57zMePBkT+7yZ
J4APlijB/0Y3Ql3g4eygAtHb5g/hbbTdCZORLGxJbAp0QnTsNmQrpoxnbo+7/VMCKO5dRAJEC5/o
Tl7rNysE+wxpBX4pfAu0H/iSuzQv7Yt+uw7wTnK96l9GKfiCdjHkE/H219ozuPN1S7AU256qZ2r5
X+XzzpuaWSitttknqVBHaqAWUX8xPaUkayUmUEBiiaAULfo7BRJmvIDdzhXfLP8iShIceQK79S2U
gBsQth4YsvlQgv5Q9hBQaAxFgceb/N4EwrArWhPLp4iKZ6tqMcZLx+kDuQ67rXM+WrJkhdY2nViQ
n5P6ZaELMvit586ehlX0SZu9DDgyQ1f2LCl7vj1sV6LbiilzaGvjN6Kdbm+p86NT7THI7lUqYOk/
/dWI3d1PHcKc/Lnx3SHPUMr/QSlHFWH6hjJg1TAaqGhv5LuGJ6o/pabceT5n0G4+aLzxIHuiFkGi
YC939VlvwoCmaN32UOJSOgaBNacChHS2hxHIH/Tb5kMycpad4w+yElC2ex+sRhy9ryXyqvCWOgiq
2kPM11lz3dqMlndbr8h80X1t5dlkDrIwV+f4DG5eytec35CSCb3RjkhO1fBJLxAPOih8D3/b45x4
3CFa8m/C7MmObUqDXRhUZhiFuW/xrlQ36MKPxeNcdaL0ANWKc6KZnP6kgPc7tgDBmXVQVvJTRW4y
30BjOrOqBGUD0iAomEtNxuFboW1rhTPXtT7e4OEVSQ3SbXin1jIL1Uiwc6JY27g2AtHeB7teXmhq
yt56/ziSyoYAsxuKh0WENyU0xLDzAQuNGzU5xUusZ8P93u04YPIOnn39uCALVOfHkWK824DSQU25
ATEsyEIdYWRuTqVWQnF7ikTQh60j6q1lz4lKZICDrQGg//Kr6XMp6ut9kvhCCLJRY1B6IcPSClJr
x4em78NJ/sb533PDTQi9HvDuUXGH9VXLM04/wwzLcYNZx9GMyYMaPFn3EmYN4TPOqQmVEci3ev7C
Ul1QvoVXdUq9JlgK3y8tABCmAOAiSn864hsDiUbAB4av7Wl++d5gaeQCPfbjv1/mhuPRTWV6W2hf
Ez3kHCI+sGmX9YRkSKXN3Qz2u+YGHchP6chfLLbg0h747CYnTgkH40QqgeSBxb6EjcVrGJPQvM+8
hOD0Rx32uTUY5b5Hn5Jr8yJVw/mRNRyvCBKOs5ISLJvCKY7OvoJejKvJpO3s5j7uYhI8ZNbZY6oy
5LQFMsgsR4Y14q/nonCWqheR9hfaZG8btE/iy02KLj0pKKWwamCIGZkomNtZOE06hY2QAuD01F7m
IyfDqBdDTHItQZKtlq989Rexg+iMJPSnC+CB1Y6gYJSzmW2Ga2g1qCVsNiIIgnnib+SF3uC1TVoI
AZuEWsFtmF9eDGjQ1EvFNqYHeDL0RJAJfW+IU1iaz6RCP6RONsK9H0Bjzm5oIMya3h/PCus9wESL
VGQByVjbIA2t5YfNnOzvBYNgfwTFLt8xci1kaObrdA8e7Ew5sdcgip23km9HZYTNAr7iyryy9DiX
nFl+1XXAJSyqhhsxpC4wW7BONBCA9iuxK08X4RfXYL6XpuEnifMMViiEuMud5s4eNe8EwhaCgKBr
fziC9SpLVRqFhhZFBPfkVPKx3fRcbaQebxx2H0cvhzYb0TFHqRGPeR/RKem9dmiUYBvx4uL1qj9k
ziEEpbJyUBAnxYyYa6DWXjArujOqsGUvY82U79zdasPCthiD42TgMPEpUIrQ1YQEMxDeAqxR0nOs
IuUSjVV9BbUQ/c5jLaqBlnHXtKaBsAdmbtkVIjZgdbsU1es3s80Na0IaKcZZ2OLjyo4GoEEHEzEv
EcpGVJRY2vQpKU8y4VFq3/DgOFyYSk8p6x41A7okc6vWOb8SSvctwPHhXwNI0U6Y3c0vSUjTvVif
jgfB3oJPnu2WyL+fvdRDuYQU3SdAcCZZH2rwErFOJWWS+n79UJ8O+LIUyMCT0Fi/Dnir+psH2yNM
ZlMA6wV69WiV5f3r4wEO2fOaSh6T861carLYTSdixwW1/oUoY8Jh36EsgUYjaBH7L8AXoQb4Dala
vD/kP/oPjFi8vMdhpWScuRgGfEiwYTq8Yl3pN38BqSsMySRecAFU1fJfpmA3SHiEK3xZkAFMFF3o
dyf5097VAGpKEWBSQvhmpe8lR4XwGouJGKyl1Cfr435osjjDgZzac8l1Lw+T1GC0dSq6SLFmzO05
bjq2HWfEA1Nih7xRtAVCQ9rrJSbeUUw+AITVTgy85DKshu+LkavnxExctNqXigHe+V5fkG/aJgya
uWtpgPnBupPmcPGsAytx7DEfltSoSlHyz4UJr1lD9U2s9HDr2e152BK6G6wyteELGepFrsAhE8Qn
WS4RFyoy4BdcTQvaPHEVV8owjofhIQvHOP/FQKmXRdPZXTUsMJH/pCUtisNEpxdf8NKm4Jk6Ii4d
m5U14CRQHBc8WoNRhWntse/n5iT5KPABb+YY40C3vXp4/jmFNeoILuroFPzstTbVfo/judtp8Wrl
EbcvqReJ23ssebbkWATBu61XxJb6dAoj6PK27ZZZ+pVb3VJR1Hj2N+gZXbwm42MS2+sWcCaSKx1j
mdpJq5MQA0a1UIKzakCcf/1xa93ydlAR7vX2Ki89yowmWMJgVksJVczZX8iEojxUVY4rbZpKtzEQ
7Oi1xT+eaA8Aey26Z4AKaaeVHCenStvwj3b72lCkAokNmOXlB2n3W7dqErgiatOjj2+PXZRLwMsA
pwbhKuLc26Xs5BmKvMw82EnSR7hJ4Q7+X+KmTevLbxG52bQMJwZc8b7DYQdHc6MVvgIbGogZiZm3
Dr0fa9VqLA2sAiIgELEr7vzepEUE2jfVP5i4cQWRFU7Jxhj8tJ34xyDkmTsWCOL0NZhbBB1VZ83c
bro28nmfiO99PtWraOBTQuJ2vhk74xTz23JhTQ+Cjw6ZkPqJaKcH70FWXXhuoDi/JmtyXQxzSMAh
dF/IxSTyBiNFsZIVm5LqJj4XUf2h75zwSi91ys8uGd8P0MquIPHY2I8s46jGDWcsMaVWCfsMO/M0
p/adcgkUvwVJrMBLge4Q
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
c7/svfhTUeFe1oa6rIPrd9Xm+7x06CS6A1SL0uAQpca9jROFxiQYY7CXQWVkRU2R/VVHT2m5JeHZ
wM/6W1Nt5o6mlsP6Kiz8ydSn4wCMiHzY436rNgXDvbsVIQjDV47dnLUQT9xl1/lk8prw1o75lPD1
GadIGu23e1uCKLt3rYfB8hOU9WwE9+iIONbAFMqQPrAvGBTz2lmuYqxblrOC+kMc8Fr9rMyujdMn
7zI8R0t0LF63wpm15XQQlUtL5DJCaVnfbA08vbUuZTIRFyd0SX3mYorFl5vS61P2z65GXruoqzW6
1TVbVFzLbJzAInkAYVlO2PFolxURWHN6PUQNTg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RyK5WtzxcTLsuTIGUqwPrex0p6CZP6ElCsRN1Fx4JPOTcIfsmhTjQzf+Qyo9scxTYZeMdsDtmmn9
+QquV1pioWj+0zCausicq/HBBcWgk9wMi6WibXihewExxf01U5j80zGE5q0+YU+w0Pr8J77x8RXT
7OYo2IOyiYQ4hHLC8grCaEQDqD4y3Gyo7IGa8Qoj2FxIZYoKwPqlwnH4u5XTvlid065mpR/Uslwk
sYQvXraRIYwPRSjzynkWeFi4jczGxBjUFWNyKTtdC8KP+MdeSIIgYc26YpOlx2nmplZ95Pp8qAyv
cFR6XP98SzierprAH0xgQkQsG9Fy9ddmr0DLjA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12608)
`pragma protect data_block
lA+3t6Yq5GaWijiDKShKFOAaPCgbLtrGy47wYat0devc5lk8WxGcr4ctmAdZ2lM57zMePBkT+7yZ
J4APlijB/0Y3Ql3g4eygAtHb5g/hbbTdCZORLGxJbAp0QnTsNmQrpoxnbo+7/VMCKO5dRAJEC5/o
Tl7rNysE+wxpBX4pfAu0H/iSuzQv7Yt+uw7wTnK96l9GKfiCdjHkE/H219ozuPN1S7AU256qZ2r5
X+XzzpuaWSitttknqVBHaqAWUX8xPaUkayUmUEBiiaAULfo7BRJmvIDdzhXfLP8iShIceQK79S2U
gBsQth4YsvlQgv5Q9hBQaAxFgceb/N4EwrArWhPLp4iKZ6tqMcZLx+kDuQ67rXM+WrJkhdY2nViQ
n5P6ZaELMvit586ehlX0SZu9DDgyQ1f2LCl7vj1sV6LbiilzaGvjN6Kdbm+p86NT7THI7lUqYOk/
/dWI3d1PHcKc/Lnx3SHPUMr/QSlHFWH6hjJg1TAaqGhv5LuGJ6o/pabceT5n0G4+aLzxIHuiFkGi
YC939VlvwoCmaN32UOJSOgaBNacChHS2hxHIH/Tb5kMycpad4w+yElC2ex+sRhy9ryXyqvCWOgiq
2kPM11lz3dqMlndbr8h80X1t5dlkDrIwV+f4DG5eytec35CSCb3RjkhO1fBJLxAPOih8D3/b45x4
3CFa8m/C7MmObUqDXRhUZhiFuW/xrlQ36MKPxeNcdaL0ANWKc6KZnP6kgPc7tgDBmXVQVvJTRW4y
30BjOrOqAmBPWp2PtTh8p681wDdWNDfLj+8hdSImo9xhHKFsJfY1cTymnvo5gXG7445T7bIKyzpf
skhQw3TgmiEhsDAjWEviom8/ndv/6d2OEQ5gwA3j7vXjTw95xpAAusiLpNF+pDt/zvEIAXkPUmd9
LCRifjuSeERh0oT4W4koC2zBheT4nduSWE8Ttsd6mKHLHPJ0qC9B1Y1EzwIT7sg5OyG0i/q4TV3x
sfGjNs5aZ8etr6autIUlHVoGMdChwQglUEhYwO2E8GeE5sU+8mbDfPgn85OsztpDQzDbDSNrW1m2
tT2579zreI5g635QJufl0l//oYVpvuVdHOdI/GLF8tpo1wXgXKP7OnG+gBz7sW/Y9WLXFOIpm9rO
fo26fv1b00e1icpTcXam64Sx+gkpp2rY3irwjqHka2FUESoOjjxoAxv+XbjgCl5JzXpnt2O74LHA
5ZTyseduUkrq/1y/UuGmNigtz2HELKoRaEQh3xGlE6QbTQVlbDafquz52LgRySLZBbfHhmimzDMV
JHJ2fBNaYZ2fcp0zlKnlkHYVLLLNkS5AkCyim/xTwhATCQ3+tO0EvwcelquXgAeswd6o6Zz/zyuk
MB7xeWJLauwTvmrPVtxA7PTNQSj+yuJHZYfdCeZyguRF+H+AFR4zbzzXaZP4JapuGXBPD7dbUNSX
Sx5eHMu4jvomJ8mqXYinsH475S09hQ0MyKFEbm4VUWE3dhz9pWY7hnNMZa3x1NEY4j2097J3/nxQ
0O4OuHe0ocStZ8DspLGvvQ880nnAbmpDFS3OdrPxA6A76CG4Xh2LB1yOlHCN8/P/M1TVGsaZmRZ7
pY71yHL/fiq/S2RBivewQzXiqJBF/NKZaxMNqByViahuXprEjToUs+u6J408+yXtcjOjOA5CXcrm
yHKhzhWl/crQNidglQ90aV8em9hjlU26Q6EYdk05D9K46YTvkBnRn3orTctl5JJpaY3iqVJ7l0MP
UvoImxZD/adI3Y6J+NsI1yPiPbkNx4r3Z2H14NfpfU3qq5LrlsBbpUwBgsyMy+Gwp3o1mRuSId3t
KMDpU77e9397L9kFJTtdTaimCWe+9q6U9L5h302QhPjMAOE1BbH9auXsVEDukbOCFlCpJ+8Z5RXj
OH8M1Gf/nketolGAiBYTSn9r3ZpiaiLQeR/1KXBh7+sGRa+ynCs58XphHE2yWjowz0NCyW4eeO/T
8C8gAw/U5vaBzO86IM/zJrzUZJXr+AiKI8r8nzOQFceRSkBuKSD0mTHDO0JvDjo5F9Kja1z6xxTH
Zw8Kxk2pt+8uCFWSNejDVXCIX8hGJ4Ly0IrOg5riRA+9nV7FYKm911pIl0LLP6yJ7fNcLBl6868h
XyHcxHYBJGHnFuVyq2o+356VdkpD4eDl1xtEgbvdWL9u/krNYfl51U5n25CI+++6GDC+oYmV7PEu
I48mWMceaOGcAKvzbvJRENB0GLZCrINJbBOxGNCK/YuWeHzmmjuQJhD5+ywaLNfT0YPRJIIx89bH
9QfmVlAf4zfVPy43GdR+ydaIADVSLzA897MS6IQveHfmF29vOtgi5fjSKKaMOc6SkvhRX5e9G1jz
+s7P/ewgysteysm782Fo7sVq57FtWqCDpoAeRZ3JOnjZb/oAFAH4XhVwN37LY9CgnU0/ewdG+N1E
sjBP9Or8pWXsDR06bdGH/0TSa0fsVaLqkaRwHow8kzlTqN/6poIAKg+z8u8MJok1q1VL/aPTWiUs
/yo4ZXB0h3dpKv1FRwcRlGaKH5gUxGIPITDCf5dHnUzZ7fEaVucxvqwUnCWI9rJ5ARU2hYf+dmlw
a1JoNwv38uyEly1ozWr+VjT7zZylLmrQIgn2YAO11HbSmfu15U6wjT3mz1NXhjOYsRgT/m0cI2Q7
npq/jMqDD+pSVIcQpZH2eRH/acXm1lHWzVZlpe1CLsXzUGkM3dl1d0Bf6R7KOaBfPFxMmcMlSmCX
QTwZRa/I5dLflNJA36OvTe0i/V7bU/rOGcAn4S0zaNPKvGa4horuCofQb2J1AhTzhUEkB6zAHWLM
L8j5BZ7UnYDwp/cW9ebKn3TS9KMUJfiOC5uYrmLjAxt8AQICB1Ikfd/qbIaDk47do6gcyAXXBvpH
o2zzxaCU4U9i04X0wvY7zpeNjcRAL8LwLNObDhmNHsYncpt++su0n50CdfPL/ICMyIqUSdTlnppH
Z4mM8UWIpjiNh101OsBScyGaypWgyyj+d61trsWgUrvFFX12na7iPLcENSbW//rpKrJWnWIrB3VN
gA75a/880aXQzmee+PNdultciP+SwKlXQyarT/saQ6MK/jSzJH/e7Wqvf+2NW40DLoUQdhH5b24q
htEqzEd8/mdd3geRYDnCJzKlRmPR+63/2tFBquM3sq+EnzpUCHkBvj6UE9y9gH9z65ac5W6UByxv
gexgj7PGzzd2VN64KCLzem7BTuz0VNoj/8ET9RAWXLCdVqAzfNGZM9OntuBsvU7nkPeggaWGNNXQ
YpxgKum7UdTDMjSLb6JyqjjwP+PdkRa70VrFYZnIalM+CD4THaDxmt1r8ZoRmSknwGpKNXcIunle
j/iT+YNt9RDgOCkM75y7dz4B6N2nSgs5VnFr7ZQxGOY7meSB3CQcdUB1EpikQ5HKvhvX/1tmmQMY
UdypS5PyX1xw6TqZ55opMq4IbnF1JgbP4MrdYEh47SkGYh+4O6fbDcuCbnbsB0BbgM+4s15V57aS
/woFxxv72gsGlhs3qA6zIpAx5MNHuCFCXYLeSom+dvkum9uyPxSfTKbvwQ0U6j988MaWFinS4kCi
4lfr0E9VZlPlVtvA6nlv2T7+TcMU7Kxp7Vtmh9PysGQOcWfV1mrZdw7ynRjXHglvVMPqkbPYBn9s
Gfwx6D5yIW7qR1VFY1fTasaOvsiDsrl5aulxqp4uD5wwshBR6HGPKgLRn5Ioaa8wHeED/n66zvwe
mQCwxiu+eVeD0rhlE/r7T/06wQyVXj8bCE6dHLYSeGVxpMYDZJIp12ViT6WueDh9k4z43MI1/jGV
PHm7BXE/Hn3wwxaxQ1mFRHmU1G14biYfubbKJxdXTDWhOcU+jDh9mffemHYq3HEmCArN2d3e6vs2
EL+W+1j9KjR7IocfUQZrlV84yA5fBK+oIS3Lq4urnIj7v5UjDwtnPpuF+IL+HcGCbCxNZT96NRXt
+MEVsyilM0WGsBrsqFqSN5yZ4I8vzrGAcp4O2Gl/OGU1nZxk8G8pCQTLjYcijI7kejogkZykvEMS
NxwjdTJMQZroH49YNteLryb5qGzKgoGUx7hlSjMbV2CI4UDHjhZ7w7Fm+tojK3hVjNnqTnDiDgRO
GBYz0gsWGVVsyOv/tdYCVjHXntKGijo6eWdHNp8kY2QGGM9w1WFmWh1H0GWrM2KqP7XOc6Ugie8n
OhYe1B5V1o6uLHJQ2tSaw9QaxyzVEZDWAFPjiYjgHR4Bo+bwewsFMlEVslZZj1xlrpw2if64w2Il
vL3gfkW2qldK70ZZnQE6lGbSFk79hLf6FYvhFxkbY/WJNFXmSn+ZZwstM6q3GVrVE3EnVa1LaU/4
T6eEIA3SsuRkS/GUjU6yRSxvGSXuT0kSboz6yK49eKmfbOZ6zhd2UAH3Cg58GfDZxRNqI1DzyifO
PtKCQKrX4CvHpUxNyrZiGEpcOTQuLjuhnW35hq4U1Nr0VnAA3yasFYfAs3y+FZryFQqx83Bweaxw
G+dXhbZzMaNB7qSsa1AMbhMgzDZ3mZE6DB45DybnS1a0qISeQEDXZM+AuAxZcIEFBQXU4ZIL3cuW
dP1W9iTljdi1MpCuXcIO3ChzDVKEFM1eOg6yYvEMLwoRjFJJn+4BiU+a8rRS65ypEcertluODv5u
67K1JGmr8LXisBaP+qTkuSJ+UqH0uK7ZZnBItI0e/+pVgzQIrx0WfeUUBmarDsTh8IqX/KHjjn3d
Rx99ePnXD8sKKZG/+8BE4Z/7hUUS57QZ31HBi0T0BRZJikJ1hbtLSSdWV9eMZuylezBtZwVg9lKn
PLURDuWKDigNTG5VH4hqfJOSnzGnir9SnqmvUS5H8SoYtMFjKlXfMZxoHyQTzSK/bHnMviocwf+V
FCJvgqfbV/qLdnNbhyEjHjRl7vNzu+acRj45Sy6ADekpouFSvBD+sVUIRefirkSH9v46RGAHtU3i
yE2jvaQT64/572tYfrSMKkzpvgYHu7vDezNBr49T3nwtg6uMUyj2W79ciKBjdIiWKrw3fyd4PdXp
Jin01m0PcoQxHavAT4BELpo6w/8bTGIZ3DHiLGQOuUkBOYWEX4bfxPVxA6IGstbzI4Ml9L8oRfoy
SSJucYCYJ7CLQP9PlFuNPXhjhDtbBgZ4a9Nt9+QCjVBr+SIervTEpSEhfmC7LChz7RrrEif0KFq1
851Y1ZWAECaixzoW5eJO1j5CJq4aXQzatxBe/qvY37pougEjz1ea2uXhRkyFvGvHXlxMqmIDnoOG
S07vfEi7OqQnQyT6uT38ZLsNc4z59+gd8aaBO0mqnvyrm+dbgoN89nio4wBarjNd++KEylZ49vIt
cX23+w9BW36Ba4DP0NfvNHT8bd4nJxMVGbu5Xfy5iOf0GiCaVRlaf/9Tt5FpCL5ZjXbWyfP33Nt5
jueuyhWCvxKK8aM/kCYnJtWLcdfszmyTzO5ZqednkBPnVGVaoLDYPW3bqY0EzNFcziQLFQNFtxyk
kVVn3x6b7NE7VCBOFb4ou+kUhIcfkGE3QEZdLuyXqL8fybH+l3JFzwWvzwzK2VnBaeOwK+8SermD
jR7dA7NE0+luxuElp3W1d1nMJsb7yHuvjRhB8uTS6nh6d8tW0f9+pySg3OK1OcvmltQk9uLw+mxg
9eqlqukxb5i3Ul0J9iUHrdhFLTsHkBzQDvQJ3CFp0+xb7pF+O58+koxb3evteDzyu6Vb8aeMGoy1
a4Or+PU4JzwUEJCvMv6yrkAOLkmYGnR9fsxmQqupqZ/btdiGxaWqdf8/heUrQX66Fomcj0uUP0UI
U67QNQO4Mv3NlDE6N2bakgM0E0F/a88fgvGotx9RQlIbtOOXUqlen1nhYF2xAXecoE/YZoXlWiSg
vL22/brgPUQ6SKvxiFHjTeZINGCawCVUbTWnQLYThvaMgZHtObkt+HmKDNa1uZKD17ORExKcwVVJ
0NB5to1fhRi8kHEPJ03vUC5RtZIeiHETcjYp9H1cciJmwI2XQ4E38L19Bo4neCwIxpOtGuGyPR3/
zN3uYSZnfjTVeFoA6rBDJ04Itn3RG5tlMWmWRQmGzyE1R/PLNxzXX/oCTizSkbu3+e5MRHoGFwjU
nhFnPrddRHJqLXSGpLVbRS1sA9pDr4fsjpzs56iaHGasTibRil3yF1T2Gn6U4WDdSFGE3SJ2ioJw
frYrlO0Nx4lrjExgFB2lDEiKR8XlNiR/6ak7uVKO1unR23vwwoP1W4iVsS52GC7zNT2pux/mKxAP
oINE8g17Cck7nS4yT32HrRjyYaFIBaaYx+uv2umcsxQG6oHhQnRLf0lbO/sBSIbtl0X30duHuWEd
nrBeTSGl1UtMRLpohj7ziROOYSi5oniY433czLvLO/BKWdcB86BKY8imV0Ze+mVasPKu9s6HXXlZ
d/180JK9UOkcmTw5iO97bR9HBQJ/WjPSDc0idhAANqEV/2zMvrZw049pCuUOGuSSwK1qf/o62Csa
1EdW8+OPJf5hNUVVB6eXBqTTNPIbf7e4vz/7g+iN/m+8lrqJtklOQdodHrKyMLOsmAzlJBGtYS/p
HfIAdcDI70CwwC1uTL+1cegI09kzs8qKtM78NfywJrwnps1xO15F+AA37ir2gdsls6HRMGLI7DDV
kPVzlDOvc9V6RD7PHX/G3B1g2/v+1zoKYKfoOUOli1Tv27fODHc4B913bfunEga9vQNGRIaEJJIY
2JEdYSjmHIcdvREBvDHKTQCS1Nn/m1pEUsKwowyzRDt9wjKsduKnIUP0Ca1NPiSFsuwDSHQr9ru1
9oCBRM8YWDNFxfTmZAGjBDxNHGl94iCQvYWyPin46uuCTDGDLpsRTBRPdSj9yppObD/8mcEhJ7wY
8fF1PrXezU73wqF1iHR1cXWBKtCcpPbc7nIFTeN5cj2LPhRcQG2vgbto8L6P+lKhOnt9YOXyLiai
xvSwWy84AXCvUUdxZZT1NMGfpZgfxgClhU7pX5lYmGDz1V8VD70N23f51oU593BQLgvgDFc62Wdc
PYYmmYl7SKwn+MnrG787YZkiO6flNWm0vrh4FXBOLF7jROK9AHdy1xhkQkCGPEKBthCdXBWElXH3
JrlFJqHTslYmTFqda5orl/4bNx0x0Y2dVmZVKRW08Zpym2RcIuCVTLfW2aoQtDNUXVbOJl6v4Rjk
ewPD1lKm8DGwIxTkrgrkHjgyFPVW3dIhsQij1gUrypnYe2v23K56dLR0Wp3uOormHs7ZIrkkVnkF
DnELqDq+sAqDPXIFDI3hLehOEJ8s0gcN9tUFk+jVzTezEDSEGkQiLn3Wj7yGy0Uk1yCHAvCX/JU6
c66wbPul8Sa1jGkfRWwjnaUX3RfBFdLhu0H4l6rqWEYNgQEFxaIMACc9PW+P9GnxzocUheMggBgC
MOKZ8aFM9mg1qpqYeuHlVY3v8XAn5p/iDlNch00aDWu6V7y/cQgJDPBvNwj8HzsVF6FXxkXjPqmK
R2iXrTpIGKLRKa2zX2tIhRgwcLuFsQXWUJTGHIl/V//PODNjYJGZD9KidLocRjKXhFFciJdWXr+v
/P9+E77R2t3rhJT16VvELiT+OOdVcvLKymdqc943nKh6bXi6ShDHObLd1YoAN+fsmhxMNH0AZiAa
i9hrYKHX8FsQN7Y/hiuyPb9+iBpbGAADghpJKXNBWlgYh/YoTBn/Cok4llu9jHLjcP+aO9AEerxa
gqXq04/nA7yBSKBujqwmkpo6QKnPDUY0o5jmUrQcaTQ8w1lLeLfo/9un9D5Brwn8Uk9RWdNcbqtC
oK1Qw1cLLxU7GKqXYllB4Bj/AfShWiOIxwSoY38p7/0Q0nqJ3MDPxRTkwfZ4YxtLcZF3c15rgQvV
NjnGCbUqgQSiNseOlmLKl1cBPWbGD8//hLrsH0HJ00nIrMBIWU/0DO7A+sg/KqMel3CPBGLPmVql
/XsOPzcOg4jxmBGWCv1PTH8prN7Iil3bj0qSmL4UVBafkqA0hXYqBhma+M4LoaUZJAj5SD+wb7AG
neoW1aclC4pcgrwgH3Rw2GrFKZFvbcJsCG3v/E6MQEBqlbsnPY5QsEFz54jD4JX/gzigxWqOTr1c
Nes3aetmEofGr0/uozgYqCb0pWcfo0H6brNvrX/BX9r1QQpLW9wDDVeLuxGej+mzX1os4L6Ue/0g
D9SCcaXwV0tywqacR+KJCE6N2Tu1ZggdpC85axShxZ00O6WllxYZzeiJTXfgcfRpMwU1QlQZUKxx
h3F22sQisKy5q8rahfO0tp+AY8gmtcUqenqlo0LFbiFaNgYP1j7tbAEoVMT2w4dycszDW6+ZCT1H
XP3n4eBwNqqoPwHbdgqlvzPMEuLhG0JRqAHUmw9dqrszCGa6JxQGR01a4oO0e/GG2G/l43eX4RX9
0LnYqXtzVwslkwsJgJVnFhAoBFF0/F80FlOJnLcH3CDyuCu2vTwiYtZDZUPlidpA+OVcWdFP+Zxh
zxZjWcbn8RrkW/T0nQVg49NUBhpepu8HVT7IGDXw/dgsqTLcqjSd5ObqEKuOt1m0k4jo7pHWFBWe
vde8d6nwA3UzCEfwWyfWoVQMfvja3ucsw+lMM7n4k70+SmD4XSbO/e1DDBjnvef6nV8WNa2BHQ7W
G4cE+wqjMszQc0x9ejFfQxgQEwBiHWgfj0AcgWUdOxkJILufzJBAq3x3P4TPFExc3wfYbhxfFt5X
4Namt0PECpd2XehF8f/Om9ShXUEANSVANyeBoYav8/uUCebM7pC0rMYSkD6LTPVDFM2EsvIE6Rz0
JAzKGDqqtuUBaxcko12GeJMNp8BUA//fzLhFNZHzXVMS/FnhGLMK0Ry13fDOKfaoz30skq6QBU3b
874qqih5zY0fQEfeT76sXWGK7PbMc9f7oDTSNyIp8L0qZHWt0su6i7N8GfxPWxrph7Hkx4Z6xjy/
OknfwHyBlW6DP4wQLl2gpXno55Cm42OIpzeuxxR3D7lQszGXL8tloPG2jvMWgNIw+i15yxCe9/uM
gBmpJBa9lTdMnOhfzebkPXjn8vYLkdbcED0JBmTlRa66jiHkAAjpPkokUPii4co2iRvh9biFQ1Qw
38qn3S2OpU+sYHAJqeAm2a+r1qrBt/1SIwk8+RzZEwFl+d6VLuNZgULsn4MVcR7LGgl1buiQjG0Y
xseCEhQy9aObHuXFqtOqHdWdKEREoqEORS11pi/dr0UGV3MHHXZxUxnw9CJG/QJyr6XfpYBJaMKk
CEUOqebmVyhoYdJX2HhNa//T20IZGXZblcA0+JccSJsceevORAaGdy/d+cYgMF0Y+iQ3bEid5t6U
opeX+nURGhjvHHTwSzqKOEKQLaI9TKd/LzD/TgnXbRUOIAceLxeN2+3SbOXdEvnZAecKtU5J7h6C
Ahb1uc8VNniPsm+4v5So/VI/wiJOJuDmYXMtHqIRLJfcOcmx3v4OQQLMoxE/jd6aTnR2xwee7g94
HKyxxF/aRaDYox19ygGduAYMOxtN7KgF1Vip8BQXDhepKKZs0FwXsjKkL3kDMHmHV3+x4Eq5ulf2
T07EAa50O3VCLe3vGAJR9wIQYX7Xwv4YWozUopGnR3UVbv1V0QP4czmg7gCLqmmzYltvOtskdBeQ
M/CzuzbCNi19h1OIslk/m+xfq2/SipVHVMmnq2Mel6olm5IDH6zBSUcNC+G4HeU7SY8EsPhRC2m2
DM9yTpVSZizj/irDs+04dnGha8qFdLe/zQdzrWdKBjuom3iKqy+ypf/35gsVYS3epg9luM3lq4Yz
uviIqjB+rIO23nujtzg3A79uC8gZvIBuKYzDAK379GY6x/KxcrKHxaOBWMW8IZXSRS+8uCsxOazS
CG499Yazq9DlJO+SfQXfHO/YElVrpGvLJyINA6fFpdlhwcr4aK2pn1blIWDZLuTgIcF+Z27x5ZK6
MWfDjaSnlTzK0+XBFBj67zD+W0VshfzscvZtlGWjls3unrWeNW9NTowpdJTa+6CSr9Z9M98URKZ+
UICaijPcTP0WAKTf7vQjSaIKIMin7BG8v602s2qnGFFAqLLa9RveITtmoqqi3RB6bOx8swZjCy/7
KqBiY/P5sQuQ889qN9hQeWa1wEa2PmR+dFP5DubvD8sjTnDhyjv4uEZs/yppxtOgwY7FKrKaW008
ZDf9T6nqBmpWOMZEeSGU9+pakw2sIrB6LTEOsPbe/DUU0zvRgwEzfyF1ZaC4BF8vsB6IctwCcZSt
SJ2hG9+0p1ra0+dXRO8r5KioooFHFGP4cYy25eMTX09D1I+gIBWyid9vy0YRQQ2/j0MJ5aYRWjM8
fTVqbFSl1V2oxgIWgcdKbWQGkza9FWGKAVaYsRxdmbobIESpPIIlP2RfCFXQLVfqsIpzLGhCnxHA
S5B8LOW5Ki/py74vr0fpHU7YoYQBQN+qq3KMShGTc7Skx6nCNnUiWszTV+m2nznY5HabaCNJ981z
7ndh77dTgYquCnZ2e+0d0w93VEYVs951N2FpkDTq5tE4K0x1xzV6on1uTOoSR/MkDT7rbjkErYh1
Wpz0qGpxIONlFIUGJfXZbXtcaswH+Jz+Db/Ayr36JpvENmiIuFeaFKXFSCx2UP5Dcp0JnK5gYH3F
jmkIvHFtgZPrDGB+eek1lYSq77boEygpav20452R00BRakN2dqYg6qChK9cuvK4bSauhtUDkX0Ln
hPE9wO3rUH19mVZZOqHZlZPEEeUqKKsPF5Y1m5jyp8MXnjILyLFhpv+yKyG6Fan3ZwHuNgD307/Y
msp0wJ8DemwuhQu+ryO8Pn9N51XSvVb8Emyf1HCxKh1fZDxQ2bhfNCIt77Nm/+dYdfAL0zYraruQ
/f4Z5mCOxOHTypCUqgu42duTLDDl63TXqcBq5rOwjCQ8DJasBbw2P9UNAxKVuId+EbwPCrfqBeeI
UEOitx4U7lsMuQtvZGQXv3Pej+MvdtOkUm4iiaK/6xU+s2wubW0iFD+MBuOMmbjetG8FjP9dZIe4
JjrOnuDIOPNjEAZgqV8UQTQCCF/lx8WzV3ZrIUbMuCHEodMC4jO3hyPdPamjEp5tKLc9poLr5Smp
WD7xu2VdsX0X3Za2G5X6B0pLEx1wBl2lH9kD9bOMi0N9eZMIDkvkKFD0DyWTyRZeLL5i+hLOJi0L
I1pIkOrz5JDqBEfRm6lEtQBaBdGCDzPtsOQd7u4iwCoZMuflZPnPgU50N3opbF7EVlcvQfREmHiv
XIrvOJp3tc5rq+QMv5iuw6DJ931VpRTmDGb4LqranalKDPzg1B/e8CB2JFnhiIyh4Ep4koUD+AMs
lwy864BmCUXTAWjCDsYxttKmloJPLNmk9oDsS2dGTWmcVA7RIoF6HyQ8eONeGnURkqMpE8yBaB8z
23vPrDXhDOiTt+ThAp7tOEsPZJop3FIsCYWpn4LOHVHGpWpxRnwEvSzHyCvj7Aw6mWV/BiLJXary
3UQrltmKAsKm0e3vUJXdEtat/0aMzXcxXA9DOntUlfom8srNnWWRqnVNmdGdaDQRVoRjQtNuarC/
yzo2FvQ83Khcdg0o1GyAGk/ryE9xkpL3ZU4C/4pIJ3V1hg13PC9PQbv+vN2P9DUlVH8JT2//PjvA
xZhtDV6jYkquvW/gHJIRnLWhCftSKDeXuv7cJjGpvq2o1eV4x1DbXHfs5JbMTmwJA0w+AZk3uxXv
Bqr4pac5v6QGNsdAQTtruVHiPiGQCPvoPrEvzKZQTqg34UWjS/VpEXFal5ea+Xt6rJQyPeaMErqd
tC5OOl3POl2H36RpM0FC6hrgAjs6WMivOoPyhFGjCpeULoIkxJi59Qr0Kj53h1KPp5q546NnpxFu
my4f9HMGpu+Y9yvqKQrBFuG/KoYzUbZ9nZ7xIcS0Qzy8eIrhxtBTSMXMYdpwtVhfQZLm+st/SL1U
/IuoZbWk0zPLkO2MjMYGIhX59aJcR4U+fbyaFgcgEKZG+40sHc+fjBSEWWqpy79+e9m2ocNT7aaA
BhMrRmqpbSiJmDGvAC7cmuUIbISN33vc97a9doS89nKy/niPQLtBDO3stYny7+Wcd01NIFtSaVUm
j8gWPmSkKkbCPdS62P/JDgC6HoZfRDEMprqF2ByyUH9Tk0BWQJ+kFnqKvthshv6xV+cy/DL3uPFg
YT9EEY1d6fmaMCNhJHqGxV4ThmVSID1azsliZzUMRbKqF/1YPCivjqL29iy4olNFCvEetx1ywaFK
VNMoFD9wQeK+wrLwPqf570q4FoNXA7lqke90iK+SgW09gz32XUBoaal89dZAQICqVZSVfeJLWv0Q
qgs9YdY36spqlNRDZktFQjlw/bc1QIBwL6SZ1uXt3J3ygr/RWlHHla77QMn9iNe7KIcSIyW3GTrk
aXxAhF7j1SWxVIPPsjEkUBywX/s7rHDIb0A1mbfslFrfr4Mj1s6QV/Zg5uDS07Sw0CKcvduYP0lm
zF2yrp/Z6+Q7+bfR2K0R4X5SPPxVHG68SLNa9eWosoevxvw7HS8XxCnhwi4y7ML/g21Po+qXOinL
itkz9SsErqG4W+0V3dnmwDyxaT58VV9ud8cvWChnRzQWI2htS0WLQqUfddDjzN034jSdrfcU90FR
GEYYuvlscE+y8ekyAYCGdEK5zbCPV0k2EuDATiK7XmuNJW6y4NjJQBEWZgYE70j4shM2H4IZU4RT
t3OuMU+97zVDbH91kDhZ35KX2H97b/JEON+CAk8aAmV+Ptx1vlkGebkb8SJOEMpvD2D4yZOqWvvH
XKqthE6semuCOrjqeudmYa10AUaCobaPcMak3PcVdVvcHmvHZET/9r/Fsqunl8a20APUEHOLwri6
bhiJ8emmF/7CDC2xiN+GvvGSCc2garUgSenAb6eYAutsOLDGJ8s/GV8U/aiLwOBpZDhfMg4L9snw
zcPfwxcXjRX/a0MnxsHuLiFiiCcaDughYehIZVala/STso30CI31Qp8I8b2VHBhbmVXJu9NckNSA
qbhNtYAOvlgfPAW4vSZTzGBzTtF0LNYkWaUUpW4FLhbc3P/oal5KwFDuBzUas0d5IzdheasslGgy
ELukh1Ox6922AvLCHg16rrc3eIFrI0RrsEdHZa2YXvB+X60W6A3PzbIKh+QQfRPOePwi2X7kekZo
LPUPuAGKVrJ/K8QIZEFLG780tCBEhE2mZL29eFIemYBQcMGgg86KRIIOYDyqYqmrH4P0Jl9+4YyB
DsSFsOPwYiKCiADOJPz/+R355nT9BQrJzFLsJeE/AB2QzUZmgC9hGrSAIcrQfm8qr+m3Z9lvtbpg
9FKR7Ysoa+CFVw/hfQS2BjoAFVSNvo3vikDrz8NZw9iGl7/ZbFYWKzdhFJ3undXPcIrCo6n4ZWHQ
W5zEeZmu7kuiP+xg55PDoVr1peD3nwuLPY/OyT6ajKMrk53+63Glcgp6lNSD+j/FZbJw71MabLJ+
lLkXu38XtrmMGs3LgKIPGVro6KIGIeqcBFBeAG3xwxcA4vZER1rmCvBIpirxcjn1LMgX/o5/cQL2
SwqhP+g93sgPcSxuJoCNG0UbL2meqJbFNK+f6KWQLLal6m8Tx7jSPaU2ssQ6LYeAZ4qNQf4VtMcF
gttRHJGb0n9kmgRy0K6dcNAEX6CUm2lxt6duFOi1G0z037VALl2xH7LXwFKaAIP02WCb/8elLuxr
plgUg1J3uSd4TQeEYJd72O2OTSMFW4fqahBafRzaRrT3KVNxtSX2rapTcOo+7xVTA7PjcLBHgJCW
8MipN8cJPSsk5wY1YaEPOlSe+cq5cxniHAYSWf/dj1fCYZlSZI7xg6PjqWP13OxPQreVReUS9js7
YvKEhTs9w+pqYPnGzj2A6vRkDKGor47XRkSf+EZ0sLAaOCVM/y4ROZhp/fyoRn1T96+ppH6DXSxp
X4VfC8L4PaIP+eFsMC4G+I7REsoozRfRvymNRuG4H/HDqrgfo+i/C0B3UEXgwLVpMfEEQer5vs8s
JJlx20njVnDFt8lCy+jmlXXmdA6npQKBi8iyn7y56p1FjdbjJ2nOnHVyQjfoHO3G42TDqIPgmyWL
MDdMYB2GORRGQDzU9ilUW3/nXa8WqpYfNW5kk3yi19vtme7CVfKGyboICRG/+AIjjDs4LOtwwNP/
ENwXXMxcsjRBiPOO/obFqN+53+Asxu/2T9v27kaJms7G0t5tGPAEUiIV5dnQlckeCZ86CQfc4Ght
U1zY07ex7BXIpG0l+TPhFS5BpXBdrwiC/jW7mNBcatZgTGsrYAyPFOiHSvo0OwQA+SiK0Gb1TH9B
FhiO9ZZUZ+67vA3XU2qADyku26vL8tU2080iH+TSF7LF0VnA791RnHn3KOW/snZ9xN9AJ5P/IDqh
ysic7cM8lzbfqEpvQ3ZNp7xpraAL8aAlNUVgbC9YqCXhKXooAqgSgljAe80iWublv52DbaWwmM6L
w0o51ysSPBGjwQAosXHDQdJMCQdAeAOA37NC/JBCTuO0v2r5Ume1lw4zBmtnjUe9OT7YiD3qoNiU
JWij2/GaGNr9E27bCBV5A0/X2hdd+oiRSjQarYE6KfqvkszkxIZpEFgJkDVW2ws+ZeGEaJyVlTDM
R7nNDfx/Ushhwl4yFQVBCI8So7Q/pJirG+58cOvKaky2TM9RuIJVoPNls5oY5Q7oSY0jX5QYd7LW
ECeXWAq3bqnWKdaTxKU1TIXDE/iWewh8UpEKrsZSmmxQivHc68k9bVHfndDl0165LV7RMHlS/4gs
QJd4n9FpdoCpLjAwuT0E5DY6HDHeP5/jwWlRwtRwMcxF2vB9Byt7pjDebwxXvd3p35ujRvmmfk+8
jaKtEnXZp0riC9Qa5wbBEpOYeidWnLh+1obF1DTQuBMK9ykXODEhWOdaTIG83jomTBbDBafiTiCd
Pb+2aBGBjtyASM5wrZ0lrxZ4WjIgPPDy5h5YZemrkcl7TTv4F9QCsV7WpnVlk8WjSt7gmVqhui/0
EJY/bSL3GAlyGqRF9Dg6yFWkRoJDYD9Uz7wCtnKIqfdPq/Nn/0lC7t+mxqxjSGO6B532s4eULiqi
ABgxhf7ZNJA2hcdZYL1/iymd/NjqOlm1LOVNvYvSdD4tQcKEqtrrNmvgQZuF1h336Nsn/i/+VOcL
wDelgrmmPHJiT2xJ6jjnmRXrm1CkuYFejej84G1SdNO3GjMZ2HYaJehZJsGTCStLskkyyTiScG8V
c2CdC/nFio9JAp6FxaHNYCqfzXd1zmEBzogR9xB0f7MyYuUQZvNfVVUv8LLvTmjl6Pb+PcWegZGK
TuKKP41ZGSfFmxSgxZj9NugT2LGKsnQjryB1u7zRZVYrINf8bKbpvJywA357B+9QEgxYQKuDGnUa
n6ufN5Hx4CC16W2PpbHN0MmdC1KP0+VPlA0cgOmjQl9aUxG6QQM8NeDNbZ13CYyPnMbYJ7I/2I01
GZeit/T6nRBtUmkDypaEPEQnVfbB4+Ykm2GtxOD6gV1l0CPISngtzI75A2R2jOEWaZCmqN25o77d
9DEky4pjXjb/7njEq30H3axYsdUN4rLo54bLYbrQtY2dZr4otM9sstxzagDv0ohxrlUFa/D4BJhc
k8ey90gbj/VgqlxOVDX8QaexUyZrP3QgXF8/0mHEmXbDqmuarGFaQNNhcxmoqWFArlVfBTPJzcIX
m4EMxWPbhAyiw6dfDXnzlRGdd0Bc54Wu37cM4he884P0Gjl7cwrGpS8/437sCphb50ZHjjL9XGUj
x1MrjX/oMbApfrfc5hgtZ00LhrCmCEn003w9SlVflANh5HbzXyrpSf/2xfZ0BJdyRyAlID3jn+Nc
aGVWXgYCqCtg9r3Pa/hEsGwVgqRx6gGThHV5B4IBenNrp2VGK4uTdM5hPeAe1CAw8oTtKN29UzPy
k0kmEbwfUwyrFxYi4KSaM32F/J6mMUgQ4qLSqd8CRTqWhnfnWoqAu4TLTkDVhEmBIHp9vPvIzaVo
UoNcFR1rD/3TZ1Z0WHrA0fPV2Dw89v5acb7W60xGjdBevC+jfaYNonS/728QkGXuVq2RnN4znReF
yFD75WHa43DYr/U0y51qkhh5GhR5NSO3u3rt51KHz2VvDqHDgP7gSAXSSoA0I3BuqVG+xip8n+Xx
e6E6OBUDZR6VS+G04iKT+vtHwRN25t63QLg3X47/7IrxH/awXBbL7Yrvzd3lfTbsOdqI3AOV2vTw
00IkqZBqILKQgvpHH7Hd2i9y2rbLy1UleDFVV9N7bk7BBogeuqHqbIfrjhSXUoLzMDZWuKdHw91R
zrmVoK3cQwYbQf8pgiA8vOmojqUqSJiJ/gkPu/idhwybZZDGstmH4UgFhbCCbj/YGgQOXUqGSTwd
0CpIFN0TFhGeRdwwCfxiURMdEpRmto+nh1IqYU3XAXC5j/gqtQRhyziFBb7ENt8Lx+LvsW6snI5w
lnedoY9076HkdSRK5p9dejaMS3PZwrTbSEfxahAhbkdkef1uIfAjznqF/nIww6RTwi6iJZ29ByNk
in4w1af96Z8JJainTVeAIvslibIWyX50V+8KwbHLXFEy5eERADeN1wtJYNqjJR47OwO1axXkdnGa
SfJC0jlx1pS653j2R1VEctzC6oYhjgEBQRzgRrypE8NRQdheLkDKDJkaB/wZDEEpJKCraH4PVIx/
EnjpWyHxpP1IObwrDf+/+uGwla4euDLtWV89BXtuP2JAtmjNX1iimD0Tb1PzGcflTiDUHbO9PSlE
3dWwBj2UogiYp4qBp9yKG1uF0150m0abLDY7ijFHh+G1gheUp1Qeo/8XfDFlV9s/pV4CyQL9IJeo
2tCj1AxHRLi4SYrV6gB2t7Gybwoiuo+gGiU2OOPzzuYWAKkES2MCElicwBJVvTcY3e8FMK2tZoER
DxrkHht3MEhgcHc=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
