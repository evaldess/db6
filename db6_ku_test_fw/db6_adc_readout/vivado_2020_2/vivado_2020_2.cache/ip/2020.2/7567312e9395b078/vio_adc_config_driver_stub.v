// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Mon Mar 29 18:12:31 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vio_adc_config_driver_stub.v
// Design      : vio_adc_config_driver
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_in5, probe_in6, probe_in7, probe_in8, probe_in9, probe_in10, probe_in11, 
  probe_out0, probe_out1, probe_out2, probe_out3, probe_out4, probe_out5, probe_out6, probe_out7, 
  probe_out8, probe_out9, probe_out10, probe_out11)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[0:0],probe_in1[0:0],probe_in2[2:0],probe_in3[1:0],probe_in4[7:0],probe_in5[7:0],probe_in6[7:0],probe_in7[7:0],probe_in8[7:0],probe_in9[0:0],probe_in10[3:0],probe_in11[3:0],probe_out0[0:0],probe_out1[0:0],probe_out2[2:0],probe_out3[1:0],probe_out4[7:0],probe_out5[7:0],probe_out6[7:0],probe_out7[7:0],probe_out8[7:0],probe_out9[0:0],probe_out10[0:0],probe_out11[0:0]" */;
  input clk;
  input [0:0]probe_in0;
  input [0:0]probe_in1;
  input [2:0]probe_in2;
  input [1:0]probe_in3;
  input [7:0]probe_in4;
  input [7:0]probe_in5;
  input [7:0]probe_in6;
  input [7:0]probe_in7;
  input [7:0]probe_in8;
  input [0:0]probe_in9;
  input [3:0]probe_in10;
  input [3:0]probe_in11;
  output [0:0]probe_out0;
  output [0:0]probe_out1;
  output [2:0]probe_out2;
  output [1:0]probe_out3;
  output [7:0]probe_out4;
  output [7:0]probe_out5;
  output [7:0]probe_out6;
  output [7:0]probe_out7;
  output [7:0]probe_out8;
  output [0:0]probe_out9;
  output [0:0]probe_out10;
  output [0:0]probe_out11;
endmodule
