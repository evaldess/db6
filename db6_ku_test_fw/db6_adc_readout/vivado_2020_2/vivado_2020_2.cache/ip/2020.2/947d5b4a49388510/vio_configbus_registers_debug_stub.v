// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Tue Apr 13 22:06:05 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vio_configbus_registers_debug_stub.v
// Design      : vio_configbus_registers_debug
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_in5, probe_in6, probe_in7, probe_in8, probe_in9, probe_in10, probe_in11, 
  probe_in12, probe_in13, probe_in14, probe_in15, probe_in16, probe_in17, probe_in18, probe_in19, 
  probe_in20, probe_in21, probe_in22, probe_in23, probe_in24, probe_in25, probe_in26, probe_in27, 
  probe_in28, probe_in29, probe_in30, probe_in31, probe_in32, probe_in33, probe_in34, probe_in35, 
  probe_in36, probe_in37, probe_in38, probe_in39, probe_in40, probe_in41, probe_in42, probe_in43, 
  probe_in44, probe_in45, probe_in46, probe_in47, probe_in48, probe_in49, probe_in50, probe_in51, 
  probe_in52, probe_in53, probe_in54, probe_in55, probe_in56)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[31:0],probe_in1[31:0],probe_in2[31:0],probe_in3[31:0],probe_in4[31:0],probe_in5[31:0],probe_in6[31:0],probe_in7[31:0],probe_in8[31:0],probe_in9[31:0],probe_in10[31:0],probe_in11[31:0],probe_in12[31:0],probe_in13[31:0],probe_in14[31:0],probe_in15[31:0],probe_in16[31:0],probe_in17[31:0],probe_in18[31:0],probe_in19[31:0],probe_in20[31:0],probe_in21[31:0],probe_in22[31:0],probe_in23[31:0],probe_in24[31:0],probe_in25[31:0],probe_in26[31:0],probe_in27[31:0],probe_in28[31:0],probe_in29[31:0],probe_in30[31:0],probe_in31[31:0],probe_in32[31:0],probe_in33[31:0],probe_in34[31:0],probe_in35[31:0],probe_in36[31:0],probe_in37[31:0],probe_in38[31:0],probe_in39[31:0],probe_in40[31:0],probe_in41[31:0],probe_in42[31:0],probe_in43[31:0],probe_in44[31:0],probe_in45[31:0],probe_in46[31:0],probe_in47[31:0],probe_in48[0:0],probe_in49[0:0],probe_in50[0:0],probe_in51[0:0],probe_in52[0:0],probe_in53[0:0],probe_in54[0:0],probe_in55[0:0],probe_in56[0:0]" */;
  input clk;
  input [31:0]probe_in0;
  input [31:0]probe_in1;
  input [31:0]probe_in2;
  input [31:0]probe_in3;
  input [31:0]probe_in4;
  input [31:0]probe_in5;
  input [31:0]probe_in6;
  input [31:0]probe_in7;
  input [31:0]probe_in8;
  input [31:0]probe_in9;
  input [31:0]probe_in10;
  input [31:0]probe_in11;
  input [31:0]probe_in12;
  input [31:0]probe_in13;
  input [31:0]probe_in14;
  input [31:0]probe_in15;
  input [31:0]probe_in16;
  input [31:0]probe_in17;
  input [31:0]probe_in18;
  input [31:0]probe_in19;
  input [31:0]probe_in20;
  input [31:0]probe_in21;
  input [31:0]probe_in22;
  input [31:0]probe_in23;
  input [31:0]probe_in24;
  input [31:0]probe_in25;
  input [31:0]probe_in26;
  input [31:0]probe_in27;
  input [31:0]probe_in28;
  input [31:0]probe_in29;
  input [31:0]probe_in30;
  input [31:0]probe_in31;
  input [31:0]probe_in32;
  input [31:0]probe_in33;
  input [31:0]probe_in34;
  input [31:0]probe_in35;
  input [31:0]probe_in36;
  input [31:0]probe_in37;
  input [31:0]probe_in38;
  input [31:0]probe_in39;
  input [31:0]probe_in40;
  input [31:0]probe_in41;
  input [31:0]probe_in42;
  input [31:0]probe_in43;
  input [31:0]probe_in44;
  input [31:0]probe_in45;
  input [31:0]probe_in46;
  input [31:0]probe_in47;
  input [0:0]probe_in48;
  input [0:0]probe_in49;
  input [0:0]probe_in50;
  input [0:0]probe_in51;
  input [0:0]probe_in52;
  input [0:0]probe_in53;
  input [0:0]probe_in54;
  input [0:0]probe_in55;
  input [0:0]probe_in56;
endmodule
