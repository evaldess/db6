// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 31 23:50:50 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vio_adc_readout_stub.v
// Design      : vio_adc_readout
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_in5, probe_in6, probe_in7, probe_in8, probe_in9, probe_in10, probe_in11, 
  probe_in12, probe_in13, probe_in14, probe_in15, probe_in16, probe_in17, probe_in18, probe_in19, 
  probe_in20, probe_in21, probe_in22, probe_in23, probe_in24, probe_in25, probe_in26, probe_in27, 
  probe_in28, probe_in29, probe_in30, probe_in31, probe_in32, probe_in33, probe_in34, probe_in35, 
  probe_in36, probe_in37, probe_in38, probe_in39, probe_in40, probe_in41, probe_in42, probe_in43, 
  probe_in44, probe_in45, probe_in46, probe_in47, probe_in48, probe_in49, probe_in50, probe_in51, 
  probe_in52, probe_in53, probe_in54, probe_out0)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[13:0],probe_in1[13:0],probe_in2[13:0],probe_in3[13:0],probe_in4[13:0],probe_in5[13:0],probe_in6[13:0],probe_in7[13:0],probe_in8[13:0],probe_in9[13:0],probe_in10[13:0],probe_in11[13:0],probe_in12[13:0],probe_in13[13:0],probe_in14[13:0],probe_in15[13:0],probe_in16[13:0],probe_in17[13:0],probe_in18[47:0],probe_in19[47:0],probe_in20[47:0],probe_in21[47:0],probe_in22[47:0],probe_in23[47:0],probe_in24[3:0],probe_in25[47:0],probe_in26[47:0],probe_in27[47:0],probe_in28[47:0],probe_in29[47:0],probe_in30[47:0],probe_in31[0:0],probe_in32[0:0],probe_in33[0:0],probe_in34[0:0],probe_in35[0:0],probe_in36[0:0],probe_in37[0:0],probe_in38[0:0],probe_in39[0:0],probe_in40[0:0],probe_in41[0:0],probe_in42[0:0],probe_in43[0:0],probe_in44[0:0],probe_in45[0:0],probe_in46[0:0],probe_in47[0:0],probe_in48[0:0],probe_in49[0:0],probe_in50[0:0],probe_in51[0:0],probe_in52[0:0],probe_in53[0:0],probe_in54[0:0],probe_out0[0:0]" */;
  input clk;
  input [13:0]probe_in0;
  input [13:0]probe_in1;
  input [13:0]probe_in2;
  input [13:0]probe_in3;
  input [13:0]probe_in4;
  input [13:0]probe_in5;
  input [13:0]probe_in6;
  input [13:0]probe_in7;
  input [13:0]probe_in8;
  input [13:0]probe_in9;
  input [13:0]probe_in10;
  input [13:0]probe_in11;
  input [13:0]probe_in12;
  input [13:0]probe_in13;
  input [13:0]probe_in14;
  input [13:0]probe_in15;
  input [13:0]probe_in16;
  input [13:0]probe_in17;
  input [47:0]probe_in18;
  input [47:0]probe_in19;
  input [47:0]probe_in20;
  input [47:0]probe_in21;
  input [47:0]probe_in22;
  input [47:0]probe_in23;
  input [3:0]probe_in24;
  input [47:0]probe_in25;
  input [47:0]probe_in26;
  input [47:0]probe_in27;
  input [47:0]probe_in28;
  input [47:0]probe_in29;
  input [47:0]probe_in30;
  input [0:0]probe_in31;
  input [0:0]probe_in32;
  input [0:0]probe_in33;
  input [0:0]probe_in34;
  input [0:0]probe_in35;
  input [0:0]probe_in36;
  input [0:0]probe_in37;
  input [0:0]probe_in38;
  input [0:0]probe_in39;
  input [0:0]probe_in40;
  input [0:0]probe_in41;
  input [0:0]probe_in42;
  input [0:0]probe_in43;
  input [0:0]probe_in44;
  input [0:0]probe_in45;
  input [0:0]probe_in46;
  input [0:0]probe_in47;
  input [0:0]probe_in48;
  input [0:0]probe_in49;
  input [0:0]probe_in50;
  input [0:0]probe_in51;
  input [0:0]probe_in52;
  input [0:0]probe_in53;
  input [0:0]probe_in54;
  output [0:0]probe_out0;
endmodule
