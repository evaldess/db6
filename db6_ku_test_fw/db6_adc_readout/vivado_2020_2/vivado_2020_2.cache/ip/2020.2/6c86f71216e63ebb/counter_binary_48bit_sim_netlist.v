// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 31 23:26:48 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ counter_binary_48bit_sim_netlist.v
// Design      : counter_binary_48bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_48bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [47:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [47:0]Q;

  wire CE;
  wire CLK;
  wire [47:0]L;
  wire LOAD;
  wire [47:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "1" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "48" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
ZWVcYA4DkQ1CtGJdztnWhFraIPp5RKP5YwC8dBVoayvzO/pDsP+RUxDNnApDeMrGMIlyUeuGw9k1
NbxwHxsy8DfG0rBF+IG+ZirlM7947Cq8/C5YG8RgEBtIfEgtxl1NfFmMU3h52fJ1AYv7W/yZile2
qsQ/M1YqiwZ16unJuFAE6zchG8kMzsT251DOKL79vfYXWvEbiO9DKel+YF9eT2ciuIZ5BtBXLN4o
jStyp9nDyAUuh3YQCWDwkf1hOSjQDbCZ35pBUCyiCo23hi6De3Ldlu02FVH2bGrTIu2EbdZh9w9E
U1p26+y4M2k/nT7KAJpTvK9s3Bw9NrSEzUTTetUDh8xEcKIiWjEr7VYsD2hU7VG9RgC8U/pfQ+D8
zvi1HyUtS3Zi/KYZEv9J/gUiojvhZlMLm0HhiGl8bN7kTbK6eJ8zCwT1Y3vanRNhFd7N2oEXFohn
BloPqOpYdkGsi3nArJRQPEE4GJrh8R5OtbX4SW1lNrN0MMLPKPL0t1O6cziCIjsBf3KAkawCe7pC
BuUq4UTKqXWhCLqML5ZBNyF6fvEF+m/56Azn2za/SD9sFypQJ0VFvi11Uu3KZuYqhZTnOxhu4TA5
CrFu/tJhT+vrzoKuvWOWP2qQxmWLiqFFVwdsYNododoWfnvbYE3pz+YIsheXg4zKWDBpC74A6sCM
kgKUkkVcbA1Sbqn29kTD+sCRcGlizqUMV4N+gg7HuQ9rKR55LzQ9Fznc5QjgO68OP7A/+yDQGqlX
oMhmAkAtlIQDjNwmrs1UvoU7rS2ud42JIdaqu2+1zniMHqyO5XY1/jZrE3p52NywRC3j6nzTiqKu
9soMERVjscHR+yCS2WUFic8MsZGPijdWy8YO+DPAUqWPMZ54sTBdxu5uUm1U38ogUyvxg/aFhbmW
zVCCq5sFrUpUuMR0uv38IZ4SXOJih3marMTMxLRHGfT+OSZ+ThIZl0N90NNqg1muNrCoP7Kn3yLf
Tt8lrLN1wiX8hkTuGarjD51boVuKr6taVYoXbccF4s5Dtc2XsvGoYTlFce76ucjG0gl1qEshE6cO
biqEWfw79DrQvzUxXg2s60Cn4tjFTBcwzbvn0ij3SAsvG/oJQCS1mqqC0QX+rEx6UNvd0L4ONT7M
zgALRvbSRt73NVC3lGyM+BWTM5rAE6fXJeaVI9YJm4GejnVnTmTTMkHk5qGiobioTkJscv8IjILm
jAyh0fE0SJFbj+ZC0inX1Sfvr9p6Rb/602iiu7wDOwjw/YVsP3JhFgEJRSAeQOHVeOk2hhmImGNI
ZDGtDC/TAx2JvrIzgnl33obmeFyw3fcI437Gjc2PDZACxW86VBSPsPMu737/MMG1HJAXnPZLE1FJ
sfEDq1sXvSNWX/KKutYOIZF39M4OZfu4z2WY8nY0TJWacQb1cNo4UPyqGzy8bpDc1BHW/6h29ivF
8UxkugmXZ0alL0pB1Nm3MnVEXrHbu/fHwe/AKMT0mvfULxXSzKtKXeJwnIrHu9bExTxvUE/jqFMI
DI0uxECCGgxRuhrVxFjaFwF9kNASXa1qNuqHzjvyP8TQrF1pov9CAbzjDYeCr88pJdNvpPvQC/oH
92FxC4K9jMJ33ohfL2tpxQCrsLNKZDb/tz5Pk3/yNJJm962IcZpr0r8G4c9BekPW7DD+zt19t/UR
U/jdHAvJCSO8Cltk0HG0aduvBuSwPh7athJz1di6QJBHAJUWKhH+OC8i+mxuF7B19O8zxDON+kjO
rwNRt2KcQ9TbXL+DQBD9GYV30EYVXN8pg2QcDpDY1inhyqaLtq1sJqxJhNVb3gosofwR82MVd5Ih
ScCXz98jMEUn3L22j5lCrCK0Zb5hQZaVrDSXwInMZjbLrBtoeVLuRzAF0P2LhG9y2E2w1Bqw+ygA
kTHKyzQHAfnAMldBSTIaDZyxkf0BonqVr7tBFfl7CDkLBSA3P/fbAIRVMQ2l4gYzyQLpreN9Lm3I
YqiHDT1K+3PVFgNZ5qqGRXc53QFSubfogeVnoS/74b8klBZ0kfgFztVejoVEoyeVhPNMw6EPQwW+
IgimcBi8T5ibXWNZDXkNHs3uhyYD2S5KAOVOmHfvO/J9ffDB4mt8HnUUlrKxQ7vX5aSxQftMdeIP
rhpbQbhQgkjq7wsSlP5bggH1Z8/tJNFK42yAf4GFBIv+I46v3cK7mCRQ2Yez6/svh6iWiP/8Bcqu
iq0rXDJxlDsJ1OI2nwFQbHfDzTtu7x6YCAKh7pZltTHh/ApKExaB+RGpqLw3wVwpH0qWjAkjrc1u
u4IdyUSjTD9qsjPXqP8mcPC5EK1PcafdWcSUbxgqiuauUbpYn9Pc65sx5anM5fJID9kGzrYRCaoW
qI5xEpCNYkXlztcT1Ds6mss8NNs8nLQVpVNKPGPewInSfU2W5c8Ew3xHBpz3sWzdKtOTgCfJZ+jp
m6wwwnBfwVeltvqMPiNSfnHkx7qONluyN97e06Mq0lfOsUOXUjZ/31hbEYlTXSKBMe3GWZDcq2hW
QNyJW3EmVUcaUrSV+faX1rD7GWA3SCrzG/jMF57e/WjDq/AdY8cHtoEI//PZzQam9AshaRJuauox
7n84tIJGPJJbhVZxFf1G0YrMJwR9ZEOEDEgycQTe2qs9MrQv7f40wxRTbBoCmYVQ5IQTBJvrpJpR
HO4qIS5bcv5z5TxBKYmhSdZUEKwo80EuIf2eZQIy/jARPE19/1BwjyCRa1uV8IdSfhjqPuelX5To
lCAGzjmjMORXk2xuYAqV/WN+mCUuwiKJ8QsujO5+sNx8+Ecwgn8mnp10AS5+K1xfSUkEJIL0p6J2
raASR4vMkqSOeIIUPTufaMZP2UfFkMm1W10gOETZWdxt1FWYbuemgKiYQINQCYyHxBydHyGpkWla
oBlscBEf6itxMO5X3bee4YhvOLFdPLRnwgjOrHmDjqqj/LHFn7oFS1Nsx94LSozEa7Zx94Fog8hw
WXj+aD7n1sNd26/k5A2YywEA4KNjxVN/x2OdtdV7zF9/AiCtQRHbhYXJTOfduYsUpaAN3s92ucRp
S35BngrL6ZovkpRktTuCrtOCEFUxtG10BRtSpauZ8RGySpcN/FbNVov2/I3qz8ZDzH3q8YB7qH/c
lRCVKu1Mf4hrmYFzPm4GlruQiu/fspkgwXyeZ9GqcQ==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iYC6lNk/HQCf5lz4YU2Jxi/CRCoJZ0nXSOuEKzt0r6YjDkRWa7OSOp3OPStMdPL/2dA0UPPie+mY
q+2aaz2Q1AfjdV/fldyXoyTWl7E+M2Cy84beY+LnwigkdcL/iJfEAx88I194Q8e2uYKEZhhNtD1t
GdfUZLBFMC9+eajMzfzTfr3Wsd07TiQWRdOzgdtzFOzXQPRMfq+Abpq8/wPwiLoyl/b1fRqk++13
lkoCvDNopoZcH8znaNwPJMsQAR4QJ3WQGLiwP5q0EV38zD7mTPovvnfsDaUyKr1KoL4h1zsj4EcW
PI26bKMT3SxX/I7oEYoiIwSFNTOqlbUh/ufQNQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EsCqri28mXzaPTq6hl8zYF06rS2fiEkvmIn6JqfUSAYrdZ9fKf4zM6FM3KvaD9ht0D/bXN/NixHS
sYGiJ7ZWco5t82Ww+3KKwWAwwzTVu3hTv8RFhJvuZLoUf8Xhmc+PjT/zbXsydRWJOTJ8fUX1AGve
V7T3C/cPH1jpYkEr8gBryQFzru42jrymWSW0P3dJXrWCcP8/zuTHYR7Qsvp7NVQDjpuaVO5WQdgn
Xh1c5CJBhKmwPjrKPX8mM1UNKBh3s8L5ikCoWC2b+UtPSP2gwCVX2bOuWSC6S7r5XN5w3EP/0SR7
2AK3PlEF0/ii72yg5bwBne0SJ4J2eWkyCQxxmQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10736)
`pragma protect data_block
ZWVcYA4DkQ1CtGJdztnWhFraIPp5RKP5YwC8dBVoayvzO/pDsP+RUxDNnApDeMrGMIlyUeuGw9k1
NbxwHxsy8DfG0rBF+IG+ZirlM7947Cq8/C5YG8RgEBtIfEgtxl1NfFmMU3h52fJ1AYv7W/yZile2
qsQ/M1YqiwZ16unJuFAE6zchG8kMzsT251DOKL79vfYXWvEbiO9DKel+YF9eT2ciuIZ5BtBXLN4o
jStyp9nDyAUuh3YQCWDwkf1hOSjQDbCZ35pBUCyiCo23hi6De3Ldlu02FVH2bGrTIu2EbdZh9w9E
U1p26+y4M2k/nT7KAJpTvK9s3Bw9NrSEzUTTetUDh8xEcKIiWjEr7VYsD2hU7VG9RgC8U/pfQ+D8
zvi1HyUtS3Zi/KYZEv9J/gUiojvhZlMLm0HhiGl8bN7kTbK6eJ8zCwT1Y3vanRNhFd7N2oEXFohn
BloPqOpYdkGsi3nArJRQPEE4GJrh8R5OtbX4SW1lNrN0MMLPKPL0t1O6cziCIjsBf3KAkawCe7pC
BuUq4UTKqXWhCLqML5ZBNyF6fvEF+m/56Azn2za/SD9sFypQJ0VFvi11Uu3KZuYqhZTnOxhu4TA5
CrFu/tJhT+vrzoKuvWOWP2qQxmWLiqFFVwdsYNododoWfnvbYE3pz+YIsheXg4zKWDBpC74A6sCM
kgKUkkVcbA1Sbqn29kTD+sCRcGlizqUMV4N+gg7HuQ9rKR55LzQ9Fznc5QjgO68OP7A/+yDQGqlX
oMhmAkAtqmk0/nNbTDss+w4h700dsUw7eHTmtYzsGbiQIqBLxeAjct1dnq4v3vom5al36rLikOOL
5xXexGElUPjLta6wIgXSVAylVWovOwMY/8v9PLdG7iuosrJbiwri/IqV9XrcQX0BjpKJKL+DVao2
jqS0RwEVIWU+qu/1tel1vj7kVDgcJCv6SiIXQ4x956kZiqJYvb/alDS2Am0byaQleI4SpFA4/Br8
DGmFOu7mOzb1vdQYsMUP6/mbMo2SzWOYWQGva4/xTqN7kIbqWNphCtEPdyVIn+kd/M/I1fnxzPJs
FDoIw3VqT/PaMIWjM8rxr+8Ic41QDDsSMwuvKvkX983HYZ89p2zpk3KYO76q5nr0KWdxGbDI+l1T
4XKcqsHSGRAlvOOvKsZqCdrrgbC6xRqG3yxUjZVC5ndkS3wICgVEXNjH0g0A1IgzmayT0HqqmNRD
Y8U3UPL98hf1Ecn7K5EKwUdxd3H5OzegqY7r3wUJUIn2NzpOT7+cWD8kMtWEWkjfEJqpi+13w0gh
QO9SrkYGbkCXgCkHaubUkcMVXR4hA7p0E1+F5OBLn0tX35qtE04Q0bX2AyAO+qeX2dfUxoZZaRjY
kP2YMm+xPigIorsr5LxhfT8UHQfkHHhVvuTzXusU75TtY1P+19Dgw+ZCVKcCRikkvl8Ve06v/Cju
Aw6R3RKrMyveZOFs9d0vdG3SrK2YL31DQlvtJ6ciNRlSTD6bYG1zKDMM+9dyuuKwvBODjPmonE1Z
y8MOz+gTB6RdY0GLTpAnyiLWFiZOil/ILvVouJrjWW/lvrlLltKRmRlQdDKGXBbZ0Jw4lH5TEFMk
DPWT7BMb0rUhX5NyTJrU7FU5lmLcpPeaZ/sT4ZrHtBNtm+x7x9tPJEOIUcrgeum4XWC4hYz/8row
WIkgffVc8sd4suMwxLwz9hY/tt8HjMZ+kS5vQKTS0xXZieuNY1ByhZ6f46EdA/tIiwQeo6nlNByl
svGVVUvj3Dkxw3m358w7rZsqNUsKsrGPro/hzIG1rafykGWAdJBuNp/sWU4NvPb/8VRvDEFKJmMc
SK3MAYoz+xfQJn02+2bHhGxpZgRrKbtAN4iMZaYnWL9Yok/1OkzPfz91NtMgOInPoOX9ka0dBehs
9i9Mc+U1ZN3E3A2RQ+EQJjB1GXIPU4b4lvjzpnenIPpgIF1xkiAqXFPcBGT8lQQxDmT/wU0lQYPW
SaMXmJ0pBO1T2o8FwGms5Hkq6TXTXw4Frr2gFyDAlVnEoToI+KP7CcFrbD1YIT8WfL6ykyF0H4AO
0AlNHvxlbSlwEoUQI4DogpJI12040p6ihyVbJ3NNRH/oTcV51bHAGZHBVRyAOodv78iuToWceOkM
9Wt018M1HduPFdWfp4jzSUqAnF7pLgIZ8oxyKIashW8mtXU0Kr9cda9T6o/pdb5/LUxd8ilO2WJE
QXNHozZc96twcL2jf9K7kBePs5Tzs72q5VWXCEXOo4oHH3XuOWUpjqNHRNCEIPQoeaUicfIpRE7/
DC/rjUdqsE9QskuVVuhcNPVPd6JuMK8fb9FNs+yaRG3WmctctJl80+6P0Hu2Y9/idaDhZD7l21LK
3yNB0h4s50uEJ54oP87u4ilsR9rcC+Q3Qapi2z2Z5xxEiqMpoPpwwpM3tYw/lHKntv+p++Uwd8i4
CjRvrQbwDF1zuh9G1r7S5HozQ3ZdPGl1Y6yfdumXafxDmVBaeUYJxPNYDv6JulkMeEYS9Qjbxoxt
0X3jaN8KBH7YtA3tp2Oj0FuTOo2Mq4VfJADZ9zME2EssyyoQap3fPxxLV9uaZxZVby+wYnsgMsx1
AUDMgEqTu3SIXaM/pjmagTCBf1OHoI9ok1SS3AW05cSBP3LOrhGdV6fGrVr1VgcTP2HNWV8Wc0W0
47DUgUMLTdHyZJzEqbxpihbS9S9i+sSKAaTppZ7A0m1QbBWv71A+8b5y9UglM+f85KzKdQH6ngyy
+hmTRnMxdYVgJxD5PTflFjPI2hWwoLmjq+ocTU/Nl5w9muqyLpBbyBJd3A95+e9k8G6e/KH4sS47
RjAF65X/WlYbTQD6hPauWWIEZBKPNrHnQlHnJhSWN1UWoTRkV8ee8kk/hPGmpenGHMOImPk1FJEh
cFpPYAsVDXKa0CDjGF6m5AZ6crp2CI7Vxt8mdaEDxV8ZBMvsA+uu4d+QHEXE9dAyZMjTyh6B8HH1
zQnSsQIxk4FKwK+fi/aKmRuQGQr1yRpCghLJGTRxWNUAOhDH4aC5ZwGSlBuEAIMygNA9bcX8K7o+
fVf5VmQGSxZtxfWP4QjgveyFhRbITthtF0hO4dOIy6GFe+/ZtPCW+Nfn4OGjnDnJMbQGTKAllhF3
G/OmbzTLciKp0wdA5SQy45VrFpM59CMO+IwexLZy6vTPO2IuzQ9ZxNJsyuYg9u+Sopq9N8Cu0KBq
JuVF3GJZ7ISWYex4TCgFz4DDNhq1C5XSzYy9bVtoITfvUHPh5C9xmcHNO2GOtUoVqUx7T0Zywsir
n7C8PaspurBJc4ClKcwf4+VIx9cztUV1g481BafuIpicw3MDchkduzT9QxgeLV58WXLtQTJznSgf
rQuUPY9u9/z3YGr8lsCpu3/u1TX2hdfNsgGmqON4c+Xdbhbx+Wwp+0nGl5Rt8CfOrGtUZBWq5UxL
zbnePtwh4S4WCpqRMX0VFd+QH7EZbOiWG0bd0mcDBO6jE6bZ8jQE997xU0cJVn/tB6j5Whc/n2u8
eFEazhcDGoU31UlBIbJSMXk9ufH9a/W5wQ2fVIMniRr5kBW6OnMcpL9FMkJxbhM7Cpe+VU24i8bz
eJViIRuzxVYEPErt0COQccbyG3Mu6cDnw5Fb/AjHCCBl7QT4fjeKfNmGaeqmIk3qQ+BhHMb8nEHt
f6bJmZxXKxr6/4UORPxLIogW28xK2IhfGUnoQFAd4w60oWq/Z4rqCipHtH0MrkyImclqWtMW0bAY
DlbijE0gmTHq3VB4VlC+D8VlN8bzZCTz7Oo6OTCGn0415Iz01WIctUNrScw7UtPusS9CKD362TVk
3q2A0p95BuNyT+blmwqCTBwZxoVxghABGekgpwzMefQyZUag4EZLwo4BBulbDTMXH05fchyr8gDe
TjLL8vtOeuY+hb21GZvrsip9r6GvRHtIJ9yQRyyLbVURfRTHSjCQEC6t3FzHSItKzyyRELuQxhv5
3RFSHYIVQ6rqsz5t3+4D+EmA1fvyylu9tot1vDZeBQ0hs00D/VaAOKFvq94hArT0oB/ftzhCeb6I
BG3GL43Wo/WnhgPrugq6Sbpst04nEdvGbsKJDAqye+UetFR0M9qFyEjNLcLb2SmJ6zvpjLPB/Vey
wFXXBWgm5bQBE4Yq8O1hmFHBT4lM8BsgGV129La4sBohhoUC3oguB+Hb9FeVb5A9ZJ/mgG9uU+UU
8KpLJp2uAUWr3JzTp4qfunUVoHpfmAZ7ZY9aTXHIa74WMbnudkJ8+mJDRuG55Wm8xhtsdWWUuxX+
OWD90yZaIcTytWlVR6Nq2itDXXTpEjjN2DTuNAnotzu938c6fNwC4jf/xWmOmIMLXHWCBAyJjBcs
fi55u0VlSvcmmlNKRBLX9aQWnskOYIH/+vM5So70XOQy1VTg+5wg3sDw8uys67zHQ/h8t5HjJmVq
/DvAhGnm4RYlmEjgJH2bJRlGJAf8z/TTvzylHNc5KUllK0DbwtzUglFaGMxwvfTtf0uk+g1GmVR/
eVA+maEiFAnsIAoVQKLWkw77ABtDAn7AJtlJso6p7keGtTfeZ9OINVxR3v01qquWcVwXlU89tJQf
aIQZ7rDQqU8v2phc/a7jGJvfrtTVybynTlXM6LsMcLJ1ViA+YnYAbQ3riJwz978JUEU8YpDe/oeU
rGEvxSh26C1xWNj3EUdg8klScC7t0f54ApKkPXQp48C71vRCE6B2thyTlSLmgmbRaQxj1SzJBRoT
PMVk01GsKdjP1LIhjfS4e6d7wtoSwxi2QmiZcHxrCtfCWctF4H7IS6Rc7bLr647HM3wmwld7+ccm
7HDr6orttffweJhd+D95AKNdtANo3aSsprjxobuHSM38hlzOmc1kAl9od6To2REke1a8njU4/MOj
uwhZwbVnF19rXD2mFlyDQEIZs3KXulSzeuHgj8tI2F+aTY0umaWEkkJphf1eL1w7lVlJkZBy+i8c
ONBaxHf7PqOc6SBP1X7poIHHb6pujE3oTl6O4nlJ0qW2RsUHOy6xeFApo/vQV79u1RCGg98t7h4u
4muVGez9gkQnjnyNE6+1eydlZsAJT5hJkkTRG/mDKBUdxxBrFYbhEer2EXvxCAvtkPu60DVyX+++
u4GjGO2a+h57NM2wI/PXQ6d8Nl97NiOKtr0IyvL5uONClUfRaYecuYZHkrTbFpRwaF+x3GIeNlBz
TYFFUZQ6X4+ZudeYTTY3b78vF2JvK1AMBaKpp3Lb29jZ5Fr/vM2hwSJN31gmhUYGHfciRQWayBlH
J1RmBWXx5CXlh0/Qto8NM8AoXvR94xiV0AKWBT8mLCpj9+/UunDLoibQtr15sHTims+p6fwfAhs7
hUs1IJ5sl8rTHsVsesjXGI3fIhx7zb9VncO4XwYffdZvRaX0WACm8KZY/UPVSDg7Xmb5f09ZjJgu
NEd3CRycVv8u3gJ21cNv6SlcoPzAanupGXk0DYRTIwgw/bgGeNoFpxR48hVqS7LzSkHttYySOH/Q
Pfb8pmKEZ7q3mVq3gR0jSd0q5MMXCpPYEHSqs/1/tIi08sXUPVc4QNufyIsea7XdlauiqSOdD8ne
QFUTURfIPoterIfO2yzMGQQY8YyACkv//B1oReZYnayzNunQRiSgmkN8NIIRw2s4YG77O+mtCWAZ
XuBSBgqDcDfArfAy5EyIkysu/hHBkY0bBjOhn5toHZaCm1y/5MW6Zo30M34ELyPe2l44Fr+mhVhH
WQBQJ9cJRQvA9T8PMreAHuYhaDakZQTcZlotre+jsO0jgQXkbRXJspGvebzkdG8IZmexalCia0Bd
qwxPn8kj9pLYYvuS3Sxc4eMAhE13AsCE6aHvWt54NnZAEYHHz27lLzdk/OAiuv1idH/qil72/lk9
iBxTV3Jh+9JVCjYlGYAiEXeggSjdjzLbdILJWDILxm7s4m4poT9EPPXA38K8KkGNM+WxHqyO+/jZ
w9u3JvC4Xla3jd8ievaEwHuqQSccqduWHX6z6960sLs650//dmz+lOngabMeUFu7Ju9kME0tH2kA
b+yRbr3B2pEz6hR0QQFBIVCFeGcsNlakdASF2ZgtK7YGCkMF3/4hZ6tlpWJWhRVvRxMrwPmS2iKZ
lgT7pT/VDdDUHQHTNQ+VaNqkANRpf/q5BElQmJIRpVGk7yxKa4OoZ6nFy/OdjrrvyARPebirtiN4
ZYi7VDBfpL0V1pRPxk4dHYrac4J/OpyEioVMFbyoSt3R5IcqiYrg0r0b/wy53yxuu1Kl6hEMChiv
C7oySI2brrnewbiO3HQq2mUaq4jdEeiiLYQ3nzeBy68Mt9fDhqJma5SdQfbGh7ncy6KhQjtoMrtT
DuCxYFnIb5nwbatB1PRFncLpiaZtkeEospJq+jK17gAYfecZerffkoRijXF8/o4OAvujgLA/XMR7
5w2Hoo38Lzke1Q6f4veeReneJYhHVv6IisXkzMKzmsgGlxiNVmb+NDxvZG/gAJKGC2ALTXQyk1ox
Yk2fQQI+KjKbGVI0O5lgu8OHC4D+zWd4qsmj8jllmnpqw3jA355OFG3XA+EwAbSjWnGBMWHTd3FC
JwPW9uJkl2yK7j7dYNWCBKarIUQxOwKT2SvuGUhQWsu85vfBPqAMyufJ6nF9x+mWuj+7ogW7tu/o
mw6Tz/t1urFUQZ5C5WByofKfMi2QSbbsxcfx7gTIobV/eytYnsZ1gn2+4tyJ+OvzJFIiRCLzJ+GT
qJ0BWOiUHOsXcWT0S51CbNzz+yjNWLZsFWqFc2I6Q+IGudFXFOvKU1COHpI5MGZfcKdneyhSClrb
lDK9Uawd1azJM6cQU98xSl34+1oWDy0Nx/c5nYAP6Hfnn2qW4mORFGz0+RLvIbPkMKJqqI1iD4/1
HV9BXKC1R6DnT0Ccz5KhC46apb0Gfc2h/bZoMp7P2cX5iA4HJBj00FZ5tj4k+LnqjZq+fcAeJwmi
kIat730uJn4qaHwYKBKI3OxwysF5um2a5r0vj0d9oNAJ4vVGcdwRh1Ovof90WSjO/IlvVksMR5/l
n/72ERgGl4ocKsQiqArzuuFvUSdMEeJA6uBLLfBN8YAoeOgyafO82q5TZ6I1H1LozKBQwdBe2PZ3
8B72FLfC7xhrqI/aBRS2JKdaBfn0XHVNmK4s6qvxiXgB8PLDM5FzfzFPGsscvI/HItdRYmj+L3VJ
rnTxO5DjvDF5KK7BeSrj6GQc7jax4JHz0gq8LT23o/LsjkmscLnigjgsVmvlpG8leMMaDbGA88za
p/D3Ld8O/34OOImDvl8eDxdt7Faq2c89lcQC41rfFdwtE/j5pNYDixvGxzwzi0fyOGRbbqx2OMNI
5TSrU4tQvDGMrdtWzVynhzPYO8OrTLZgsYbaIe4xo9znYB0ooNJ+DY/ATbvIbILiS9nDurO/JRnT
lS5CMtr1gqk6EXdp/Qyz7572u9PR+VA70kGVOi64I26Cjks9AsoFjlSsnREZMacsl54IjPL5sE4y
Qrrls53vYQLwMhxSX/ut93AXwKVTvJ79p43YAu9TMt5YHzbRVRpxRnuqdjmUr5HwORX4ZfmhA42U
oBI16BRaeyIwG42t6qp33Gk7D6AIAy1z4B+AQNwPT4mcmRvMNjIOlZYeFr9jxZLBCd6CdKG4Y7Xk
9w1RaJN8qKs3WQN2H3ns4epNpETJzyijfdDoSQE2GLiWjgQs8HwvuPPvjcdlgQmdZREm6nouv9vi
aB8xLpAwBbFkdA2i0UyN2XVi1k/L3Po0C4OmYbs07Vuttstg9qe6x1+V2CY2hZg8607OBzbfmmKE
JfhE19kEufpNCJJOaNS9aX7rI7TLjJzVMRjHS2DCvJQIuBikE3shK5dWgMoqfgdckF15xxIv/2OQ
s7Ydj1gwidQylvGtPn6xb+7XSSwE2Y7HJj/GrqMN/dD929bdvAJGrMH6QOoor8sYQKXgQUtVL+oH
2C11n4pfNsocTJRDTQZ5GMfT9+NkNsBna6eSKyHH9+edm6LjNAOfCOWl/UgcZQZZeJk4df3TO7Qn
C3mSNEqGUb+FD2Gb3xlnYSC1Xso/cYxr9HcRZaMLf74AAP/kBSIdpgyz9mpLI+aLzKjgWrBD4cci
o1GUNYfQXmIpV7tUohMCbYEFUN1iKXx1KUrcfB5anLjBV2+FkXp9EEzRWKKAKkku6cSLqnEkfrIU
sHM10ABFGkVB9kHaj2FklJxdDOmFrPt50+J3MuVzc9+r70RArKuQ3h2lB/IRDybkDfhZn99IKqS9
3fGbvwVqfSWIiaqxUJ7VENIYRHqlDJj88UOIaTL53ivNYSrxmmZunX2DQH+UCIoWMnGQ7XmX757r
iJPOBbUMzYnvZBXq5kopMW0f+3TRdQDpn8TBIcY/yy6sS+PCtn4qblEe2WHHVMGOkDEpGU0onQXs
TJ+WwhzoB/YVWESS3GIreI2gRz4uHYhmD1rfTEgkop6LdfAjYtcfcyWbwj+EpEs4Nkl49dTxe31M
m38SJvThqaYAHTM7Fn44O4qpeDpWACGftMM7vZwC2GMzSzUlvQNOm7NmOu5MpQJ9z7ZPIKp2lUlU
qtpnGJrWjHQfj/LrDJCpI8iHdstAjlsfb+E4/BeuYNAOVSfC0BgvNg2L3LCy265Tu6p+VQm9LL1T
HkSaZOwu6VTP7Laq+Iwx9el3yX2X6kj+LS5sQvpDtrTE4sOxHUZ3XZI1Gmdut32vhE09wekhqO/U
l6vvBi3Mm/nYE6EM6rgsuQCLIiO8ksED31qcGl/1317g8dHU6ZuSBFvXfco0YOuuWctMhe46DWQw
TR314RZ54ixZ56hiDbPuKeTXzuUQoBzboFdMNetWj50cjJVuHM6HB+kzD89AXLyBWYfRukxikolx
D3zsL1EavzRzSpL5je8E+Q8jDyZNYrX0cuqsJwZAzPqpY7cjcmyjZJ15L/U0q5Y5ZaReANjL0gmi
anezfumNdTCPbchfCc8VxLzgr1s1/39vJaxEDBV69RMaMhI1DAw0FrQdtvMFI5pKM5TKsOHk1gXj
UfzBh/J+P6xPpwpkaEJC55LLECd7Nw7IjX5hiJgOg9TRcJF+EBwRrz0gA/Mpogy+gINhmbRecbSv
FzTVL6fKolHa0kv8DnXJTnq4j00+4bspctKbrT5+8/BkcWyeH0XrBE52qfAiNDivjUU6SqxK3rCp
fM3qtjfI9Qk5+KiFNdGoy7c5tiXnxTe/pQtEmmx0vjpOHvrtIudRmj/Ua3/Z7FguiRmCaPcWPHyh
IOqSlm3MpC7K3IXZ9mQzReXLoRq9UzrdiQ/fEAvJPZXlve8dkRaMRaWBexRZ5tAg19H6t5Ox3S7a
XQYweOL8u32LqkBAwFpV5Bfezg4dmAvIubJfSHiuAKuw7x0hBxnPXrn++4eN1YUaGS2R9/y9qGnl
3ZEXEZSZPfcOnGdv6e/QHDSpNk6LTQqGjNpmE32thGv2FZaHzQhG5gdjL46Lry4GVrNy4zQJO6c4
hkC3MWiz66NzaD1MWtMIxBCVWePyWpPTWLpbo0vWjPdLZ4cZiSk4dzKeIHSyNTikQT7QPLc0Pnp+
LW9ADXr7Ev5WXMAq0uloJB+cuYAozzZDjHZisOqEtiD0s0v94DjsrMKgWoLGCK9S9XNVUQbiuKp8
2yW9AEg8/6KrH4SgitTD+xYJUWRwfNfmW2zj66IanFanf7PdNcJN2NnzlDHxF4tURn6UAur0mjrU
UqzVm+5Fk2A874Unq8IHWBcUrC9s3QqgmwMq+eGIUqaisEjttswC/3+StNiV2yvhl0PkXtoMmOIH
c73XIDDV7IFLZCY3xnMpZpDPJwQB20p7g1t/wgtNgnpug4DDiZJuzuiG4JeyTSmUN/UC5zbWT26o
CndjY1pJ45Y3LBrlsF5Wknf3urAy7QM8OsRp5M/bmQL3q3d5S+zX/EtSNL6zsSJsecCNtx3nuvgf
dSX86bXJ3lxr6YwTALmeNMxaVogImv1vYc4VpS7JXddlnNRSgpWG25yWP95O6H8YmXFtaINuwMtv
OOInchhjXshmiEdrtvuwmlqkWy7l2xdZEMHXGVacF/e2DOTjvF0UXWMDq+3REN1YmI9A1m3QF28o
wxfalZSqU3AXBhjrgiDxIYeTDG4iqEKTBr8QbrINLw6VHi3OxFKbIrlRfLF/rC0KA4F1hEEDEB88
tpLaYMrSgPapByiMvKoTtR1bsgJDuhu1tKJqIQv3mJpU4xHkMEaQwEuw0oZ4e675kWK6Fppljr/B
p1xeX0Tyq1lrtCTOfNonP736uD6m6oywRX/ASAVZ1ZzOlAfikWj7rTaFJ6nZpslA7d9/YXuhtp7n
GJ+zbMR8sZnTvZ6qFLJKLxspoP0c20pG6mLpTlSYAPgIwYe4NjqrPwmwBOXiVcuGxq6ScMYMyX9q
bf4yWYI597usCWCqlA/MItv2o1asVo7UGjD/NN716mDmdyYus3keFA5leSKbMp9Y2WZ9QTcwn77t
r1MoN9oVwOPD5v3M8tE2T88XW5Xe/5wUAVlyiHezqgRxrlKwT7d9tRK4Um3uI61DhWNK4vJg6UTR
/5B0B1EpT2iACPnljv2FhMkbIKKHSn+0kz0uPSK+GLw0KyKqrI27MSWWhh5VTNJgAfJIhn7gm1wa
Y83DmIvT0KAMrkih4PyAtU/sGIa/Yh9zTb+MJO256U93QM5+7o125zg7WTdj/093w4ajNNkmYMLZ
XHmDsO0x4nkfNISNJvUJEpJo9j+DdK6c65VDc0EkGeAOABpc+1D3UFqYQixA50zc6yyongw8tcqE
LpxL+4EK8qqIY7E3X9V+66OCy64u5oaYKJcLIvBmZd0SAjvfRUHJmUF4kxAngb/IbUG2wOMVawGk
focrincE1RhWRg1P/EEQ26Y5acQNVerawIzXICRJtBF8OrnptWBPtxuUGWaUptvb6gVKW9COMLXR
HuT485wuZy4WBlR+U9niRkdaXyXzlEypLOK/dihCasGG7diLkfni8eP4XsTP1MLATCfPZn5EBwC1
KYy4FG8lrLF4O5HV3LwTHff0QnMfhANJKwTey8zqP1TyJt2k7dfdMsP5zDgDJuw+JbfpHzT6Rt1u
YDHi18VmC+ejboLoXk8gqkE15i/PW51O3N4FsnjXJthrHmyCaUdBqhhiMC6vUn+i3K+hkkyGv4J6
Pw4wk4NT4kmvSFuhGba3a6eSR1TnYA4UP7mLr3ymIHc33nHH0D4rbvL8/NYDyljg619XXVT2bjOj
EBmvYvouUFoTkIg+jRio0cHZcP30K58509v2QsMr/611dIXlSykMdAXtMmP1ysRsjZIkvKhT6PHy
DfAVt+SbG5Ar0P3PpVUZ7+WN8FJfwiG8BQ6WmqdRIIKeQcV80qBMudNQGcRH1VLKEq7N+Csz7BSN
uY88FbBlBwXC4GmuEph+BEKQrAwJTjgQphsu7sGTaA9ZsHDepFY1kdFoT6H/eo8fnk3fJrlAR3OO
qjUAaUGVkWOwDtiNr3BZtQuZalxPWGAbyndSFd2k2XUn6uuQD1KSentunsqucJqcWjc1XGNpeweV
JTEcH/RNgW0z85zgIDtyjS7lsgDJauNDpHJtcx8H3OxS6WGBPqeiOctEBHYZJH2hor0QPZKwcjvM
v5A3TO6KhxTTD2BNidO/+Gvh1JUXO2tWycq1uBmONprwt0TQ9A9XjVCZrcda55QgA/1tJji//7G8
Qq5KxKqxAWxaAl+pjGJgUIVw/iNgJNL3Oo07SjQe7j0fh95dm8SFyiy2kAMJ1Ns3u8d6LxXGNOhU
ueFjGkLCii6nB3l2R0fqzVk/0vfyxfgU4mc2lJ+0bEHId57orU5KJXebTCo+wvVSw79kgCiz8Kar
FEDb4dSjlPByLpEf5O8eNYECw3Md8fPKU1ZvLvAqxX9unm9iK0ufhMWOTBOIvpVnC9UdU4z81WIN
IiV+fUFmn2s0ZUDnk2bwzG2ilq2tFc9JMkM+VKk6a316UVMFM+EpXjiEQn/+hto1eYvsFbxszM+s
ZK3iOq9DlAXjqBs8SqpQyHaQ65SuhfGi1o52ruEYm61puL8rl2vY/JlWPVMMXqmjLXjejEjpJWO0
mct+s/9+4xPxRzcgBA6W1WKPq8pU1ZKqXEoFUiMOESYaiRs1eSHp0DLZAUZp8VMCIOtIVWOUvRv6
rayEMDwpRFRiQrr8W/ZkrCLaWAVTnDPpGzLOdxMzE8LknU6sSvwlJDMFBzaiDu/MxlcL/6lRs5zT
ui65cRS8YfyYyOn4Le2I+qPtEUaSdc90XwZIYb8QLWOAh1TbDQ06HylgpIq+KdTAZCX+g9l3bXOr
GZ9oiAZQ5hZcw9ddKFtfWHASyhKiCh58elMNd84sm/3pJ51prXphel45k+mtLfVYRTKiojkrdJbx
HEU7Yy/iSIJ9vlsrKQ1T+41DuTlCFwngmn/6iedDxxDPfZePUuXb+gj3sz71npZqyaPUOYTDiJvP
b+6oA85aCypW8lpl47MqVPizQgbp1f1al+np3ixztfsdR0BTM+4MUn0FtiDL5cka4HuZYhSqxS2r
uv8kytFwxDLFGxVvsGGAlGayu1NdWOmUO3Ygfl1J9r90mVqofBGnswWbDgg80mOrnF7vHcfnvjiq
6OY25BXATIAzRJxdXIth66JQ7sez1Qa0rzGCgap0TnaojMOUSaizVyj3FlkBPxHBKNWcMhP22ln9
pUvFJmhiN8RlokqJDlmbKfxBNmsIAXeW/aJNCfDU6k9ukEmbVBsyOyNEU94ve0lO5LRbA3sTcVzx
wOtzaMPjRr/bma+ssx9zNwO/yLntDVLbA8jVcptJArN8c45uXzA5JMsM13+JVFE2V4Oy1ToSRTxF
jVJzGpgYZk4spClKptgO28Y+0E5/eZtj3wVx8v8kvmSKizh/TPCqScgBjiHchR6ShtQGOrwmP6tv
seQ3jJdf86ue3Opy1A0DqOQ9Sz0bQhUofHR/HkExI3gbMEn75AM3rv+3IyduEByAaU5O4mmTBM9f
Nc7KACuSXTg3IyFDEjSdyQKPFXKdC1MXpTHTXE7M52VHW1/wwQDxR6naCG47RKWelYhVNyXQbYBX
cPYyA/utvr5ucjVzldRCV/OOMwmvHtukD7nUFGlR3zMn5zxQCTiCjlg0DjaGGCBK8o6zj+cCx8JJ
IubxY9rEnnbU2MKFYZDnaKEfXEpJAwgpSzUs43wuWJ/6GY4Z96G66IzTHnQMM0Q86i8ZuS+n/OeV
KD0ZzvupVXTb0NVWHnpPvt6wQCgHs9Z9w4r7k1tk208Ks2KTdn2kQ2phg8Hn8uk/zHLHPHOQSvIG
aNyd/mtu+hcvcMdaAM6G7P5dziQ0/t5PsXIC/bz/tnhyQ9Qm+D7oFpUbHdhryXcZ2DVfa9+oOFea
VbMgMJCesGbVcTEhz+SgPeBT8TNQ08T3YnXq2Zs/lYh4kT7Qhot2f8OUmx6c/a9vCpZ7HT7jOO+i
VTX6UrN5z380B+l8fmHW5CiyYyodOTGqIIvDxVJ6/yx1QQUfisdmO/H9w4nIpLnKVu2c4olsCrQt
v9T6+lqPbnk1d1iA3tOFm6CbDPamKoQEknF2l78TZGA3ozsodQYxIl5tLcTykN9ShfAiDgXIxFHI
htaL1UEk3miTWP88uUHy+Vsi1nD4eLxO4K5awtT8uCjHEwq9551RlrFUB/+aRZ5+YHlgxE9Dlffh
DFTxduhBlIvU/Ckpk4UmT9vfogQdHtRZWGWKEI0gQQVAb/g/sosOYX1uQr4n/JtYR9Gd5zfjCNqf
GEEd2ZXHFqZ+5XifYsuCQb/VIB+cU/NxlDyYIn4gGfCEJoGQ8KuYlUwz20x+ZWOwIMu+8845NgaP
up2FDPTXlkSWbcmKneuU2tyEHTMnaygIyRy9pyibhjzArHMdV9ftaSKqMq0PDcWmTPAUDpsl5iZl
OQLufPQRA98S+R78w+Zj2feyoEngebQYVJ9odcsv9kmYH9BYTMQFP+3LLiQ/7IDspTABoXMOLtmG
LeGoPSbVvqgETDb2loXQjy98uhGw56JgKrlwYV1llIx9So0WWM06E8E0OYzOvhMEda2Sv4r1v6gr
HQz/y1ITy9mZ4RH7I6Gerl4cskKvzCBEwTAO7S/isPh8Ho11pGh/QPTNIoWDHBVVdNQNKFN9wxxp
IJz5wLvejqzefpkv56LvabhqQiBZDWz0kvweDyUo8F0lN5VAgeEKI8KINgQJOVqwxBsdctojGZ+2
kc23ETN8MRArN7D2lo3UMMcI3G1EyY3CyzEiQwWBXzFE9UoA/wrVFC8XqfYek0/LRkQ7cC2VAlzE
lzvW4fGmIavHn5dw7QVSwzUCBbhuPvO6n7Wi+1rPzIa94kAOIoS8On8V1lf7w0xLHKyGp0oXr7Q3
P88L6Kpa5OodGspTHpKJBJTjZyI=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
