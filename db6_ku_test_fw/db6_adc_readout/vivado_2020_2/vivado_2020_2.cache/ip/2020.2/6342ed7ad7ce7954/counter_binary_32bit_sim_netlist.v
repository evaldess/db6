// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Sat Apr  3 01:45:55 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ counter_binary_32bit_sim_netlist.v
// Design      : counter_binary_32bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_32bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [31:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]L;
  wire LOAD;
  wire [31:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
coD2WCcbCOwUHqo8N31vGBiruSXb3praai/WehJNPlxbRCVMZLV0Yc9yNjBf2Ae1pENo+P6BmM1S
2ROw6h5rAzubdREiCa8JLCWKhKCmHoyL/x/i4ihtVKdJyRGETxhxMFEWBqy7/Wj6bMly/IOeyLA2
WMC+YpkzRG6j4PvHBUa2EzEPuozvQSQPr4ddEZgG2aAPTzOrCQiT6dq7frsOFzyNgsn9qS8H2vdA
K8ZvlrWr5rFqfDd2JG7tmFDaJl/4+LHFAUwOIxfvjzlC0ExrKfc/vAVZu+XAP3TJ7WEMbNeZSWHV
xwi9AWyYya9mksSaK9x8V7czPaHCO58Kcz6aSLePf1a/kSW/uhLWGCQ8nya+83tqhhs1Not9gcmm
935BHwmJNz3k01x0238WENukKqyRgDojpCS9ZR3GmIEboQRQgsSHxQTmXQjtOZjfxjMQvpcfH+hW
FWQWsVWb1wgCl/NJDToT2mzoYIGRDOAPYB7jsumkcq82RsmXE+TOs5IX2/Y4fCzmv0tXaax8pish
EeyBMM1CZYYK5Xlaw4ycF1CnrPpE4lcZDuVupKDLrxJTK9fxR6tS+RK0Cr21L+mFkRTi/FwmUAl/
HoiCXhVDSNPObfy9BxJE7KgNqyCwQA+47erc/5MGVaAvJBt7zcP53vj2pJuVYwXtdV9tsxOUdTQM
OSmxJ+wlv7+JpKpPfhty5Mg6+Kd5tPHlwTQMANFMkEPlUkNdkSg0ZOnR87OfvKW5Hfjk3eUNX/+T
/YSBkJf4Si820hFRBnqEgCCf1Ge7kwniTaa/66cdlfaA3jh++peR25c8aNAtIcBloXmdlDLxhEwJ
JmTyOYgXV1U+kRl8Dx7eYBGk0Q7PCSKulehLCDagFXdobp+fxxnQJRrQPWFU6e5sfZWPM3B1Szam
GLe2GELwzTMje8sxF+YdSqad1Vz2HUaKI0FBcZp8fljqrUZoGwm6YZgUSEwTG8+r8LqBrPRC1EJo
M0nhsD/XYiPUMnu2qy8eOsmL4dse3DbSOH5TEPAKKQ2tK7Z4sCUNmT2sH3RhF2xAmksGydvZlhPE
Iqlht0oIigfK3bUmK1ieH+D7izSA4ME57dO8pCIkUwvfcJlkmfEwXh45abJFTYGP+QcXUQJT1oUV
uRb9J2f2mT+fb3rvlz1LvGGbJ6DlL74xdk2eD87knbt5bVEqfAWpXILYHbAFrjEy+yOIPMcj6YbK
whTFhd+os0gP/ekARD31+JECDSviK1XordxfV4vjzBz+YVYkMS9Qxge3v5Aun3m2eekZfNLtk8ad
pDOz1w7Z+kozZ7up0X2y6x4hWjFhGKNA9c4vwmR48I9ziB2reRSKHdHdNXGQ+icyVv8hvO6W5MsE
hZiwAgXRzAVGxtuHAw5Z2b4NjLkdmjPsUzgcacwzSMxKKTVONoVepdBR5BbwnY1/TiWMOQf5alGx
bJOQklBLE7nlt6ZUndWp7wOdTLGO2J6w/G3DuMghZL06T11Q+pSzw05RkF3igL5CmS0DHdJA9P9B
sQua5MnDe0ORduiALIrXVqhvBNhFsPaE6krz8SYnLlDwYPpF0BoBccbLmFWz1Ym0RycZByyn40jE
79XcVyMxy9f1Cx9Inq9HnlxGVqGPBlQGmcXSYIF/uyJEsJo0b97nQDvzp9bp+ZG65Uaw/8xvkj87
WwJhUAYiG/48w+g3UZqNRcLC4+gipjlts9H6IZmbhlAkrAOgQL4qFhtiK4jrw6V0l27PRqBKEyNi
iWE7PbXU/hO8akhVACY/dwISb4qah7UhIh4HTB/8p1Mvh06yFxqudeouPTrQbULfpOfmjGJxt6ok
TaOG0Hp+HeAiNyUj/bZxgFRCwk7u/DbPaSFxDuLcc9E/w2FR5qjKM1Yjiqg6A524ttsGla0FH8D2
RfvjRe4q0TmH3WwaEmxy2v/rhAkOr3nbn+sFCCpK+VIyW5ys3J5GWZ1qpGp9KhxT7jiHyuHuNpJl
3lD/UTyhrg+Om+X48Ye8rSf6yTPxKBNcuDfdu9Yd2l5j9sPBacJmh5LBKydgYKk6+PUpC8e9ajjy
4kXtIx3hqM7kWr439ym1gl0THkp1iTfKFEuHhI47n2JtnCyfJ5e9mvWYeDFVHYxqUEPUh9LDGeKS
fUycZgHov+bjrS9DG0plqPersAZkeoLESlU1ArkK3p5K6vqs3sgupZE8FmG18SMhaNXB+Yo84O1J
BdHhhCFenN+NwugsQtLhOVU1HK9nW42Jp52V6pNP9he2R3IXipVB+9yjvN3cefBosFVk4w4hTeno
1tS0yvc9hcblxGAJEU/f1JtaxaeGoqZVvwyLqVj2J6CVnVq5AdB4TNSyrkGsqlN48f0shNd3GnId
iFcVKaosKsFsJJ4JZ/1Xn1ykWN1wKmO0KZZ7FRiQxZxcpTIYi5eFmNXkKbCzNGFtDBYQNZJfDJ+e
8thC7z2rM+ranAlVoOk/HZOis72XpIgCPVzf7g5Mtkef/cq9IRtJADxcZObO2D7M3ZhyaWpqVzNH
JWJb7LeyXY0lSX8hBnz/Q61qub9qIRgErfUeqAeg+eEygwKSRBmi7ZpJZ8RoaIMsxQcVyvyQZQ12
exGV1RqDSJOC/g5RR9L7Df5oqrOEovHs1wBoEMNWyFnZ72HSaRXE+3Md80Ql+e+w7x34CiUeTfF3
PbJEg3nkyHcLEN81cEhMEH+41Yy+2T1pDy/X68FNbYRrqDVLN82QfYIY0SqZP3pm1YX+PPjd6qjP
A40drlRAXaFy3kyhNh18xwNud6wBE7YdFpLT4Cevaux2ZabD8JKv4ne37EGh4R7rQhbwXumGiEAE
9avF7OGDg8MbWK/V2nMs3Z6MFxD6eXPLJJn4T9jDe8yw9IRI1TdtbJN/h+OsD4toayXvoGdekDPD
j2No/YQJCAcvVhQdnwWWUSV/wtbyT/psBOplpdHCMV7VpgsBG82xvCO4p9ghy/go4IxG+jO2O5gv
OfAaGyLIAGWqZdeQdAc/DVGAfL27occRY96LYZm0Ms6IzoAd2FjfqazQ0ea8FHDSrCx2o4muRs4f
dB1BpL9yH/kTgZxh4KWinml8rVza8Tg/5Yk3A606kASUz0z9dfQ82sWsfQIMe14q/vV1LRz+amPI
kZQ7CuEO36ymm81jQQtQLO0WioAAfEvhkqky7LuUlQ==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TE0w41ybiQJsSaiEGNIVHYtWwX1/wHAWKYozhrJycsSPy9/E2KCy0EDZOSivY5RDNWAyEmGjkuRW
T0FvLQyD+6JPCb1MwI6zs1jwTuS0cm4DUXMMARgZa4+V8j1fUp9UTrjAjAGcdXQ4SHElQyIQq4iZ
uKqo9GUaR41BDOflDka4yw00EuSRcogoLKJqD+UcRh6yehegGHJK85kV7Z6qeYXw/dXdkoPDDIya
iiFFrFNtMz2/ZgfrdvDzqyW12MLLu61DKvgfdZ4zw3zX5vbY5GqOggvK2SzyuYap+WyFoGo8r2O6
NLu73K8kL9zpEiVb6Tpp/vc+f8Nyvl8X7RJfSA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
hYh/bucHMiQ+beOx0ESrBJQ1q6jhnUKoW3EUJuq2Xb+aXO4Tk00t28HgZJac8kcfGPGt3WZxKSDx
Dj+zbrI1j7QR2OTmEJXpYw/ti/0G4u5VqsWh2pByY9HlVYxg20C/FCOi1bz8jkrjuwSsY99e5rcB
lfADdJaXDI2RINMiHnHVXjWTI2JksOyMcqCz7j12UG9Z64hgbYZpSSuXunA9ldSzzyVo7AHNXO/n
VYpfamb3QwepNwDFY5Q12id3axHfeKs6sbs1IOTvIEFF67lgFv7eTeLxHhh1QPxtv/E6Z/WY9hWz
BAKkd0iHvm332xyTfyd6Bpt4ZtL6rncPUrqQ0g==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11360)
`pragma protect data_block
coD2WCcbCOwUHqo8N31vGBiruSXb3praai/WehJNPlxbRCVMZLV0Yc9yNjBf2Ae1pENo+P6BmM1S
2ROw6h5rAzubdREiCa8JLCWKhKCmHoyL/x/i4ihtVKdJyRGETxhxMFEWBqy7/Wj6bMly/IOeyLA2
WMC+YpkzRG6j4PvHBUa2EzEPuozvQSQPr4ddEZgG2aAPTzOrCQiT6dq7frsOFzyNgsn9qS8H2vdA
K8ZvlrWr5rFqfDd2JG7tmFDaJl/4+LHFAUwOIxfvjzlC0ExrKfc/vAVZu+XAP3TJ7WEMbNeZSWHV
xwi9AWyYya9mksSaK9x8V7czPaHCO58Kcz6aSLePf1a/kSW/uhLWGCQ8nya+83tqhhs1Not9gcmm
935BHwmJNz3k01x0238WENukKqyRgDojpCS9ZR3GmIEboQRQgsSHxQTmXQjtOZjfxjMQvpcfH+hW
FWQWsVWb1wgCl/NJDToT2mzoYIGRDOAPYB7jsumkcq82RsmXE+TOs5IX2/Y4fCzmv0tXaax8pish
EeyBMM1CZYYK5Xlaw4ycF1CnrPpE4lcZDuVupKDLrxJTK9fxR6tS+RK0Cr21L+mFkRTi/FwmUAl/
HoiCXhVDSNPObfy9BxJE7KgNqyCwQA+47erc/5MGVaAvJBt7zcP53vj2pJuVYwXtdV9tsxOUdTQM
OSmxJ+wlv7+JpKpPfhty5Mg6+Kd5tPHlwTQMANFMkEPlUkNdkSg0ZOnR87OfvKW5Hfjk3eUNX/+T
/YSBkJf4mIYKSWYq5hZXKfreAz3o5G/orgxy3BW2r57uCV9jav4B3VRvPnz+L72vq7oFj6297Ptg
60LYPggsuKa2xY2Ugs6ORDNOWt1pugd7z9cN8jMObknTRksj6fx/mBxk6ue5+yjNvuVLpxqp5Y4E
uTfTRVaT7sfg2BpKep4EY5zFWnXttJb9BZMXpo7qbqLyczjU8OFMvLOUc/fD+nXn03x8cx6g6Ebm
u4UPAGMN5aeDZ736HzvX1WEVNPpPcyh+Lv/DriXH3m/pzRDB52WY8f2q+K9YdCej3521CF2z6UtU
aum950UytYHgyIiu5okhC1lLsJl6rt3IOc2WovVe1klXmj2wAJvvhbPusVKwHlWfg7H8vqwcsfnB
O1tUKeDel1XU6fe55oRN9eQ2ULqQlPzzc+4tv0p9jHzg7jRVGqNxzEEQFG22Zrkzg2qBipJtzk8q
8c68VdDldL9B7SCbxpQ36f1FAG8I92wTlhNDPh2Nk3gwKmPlV+ld1PZNn108+nggI5hpc6DwTEDC
lUEbM+MLR5Vyf1RbBSpg5xvJm/jnrYuznoBgdDK/wPHaB1vjmX7UrwmFLOUNBXyoMOgFB5l33AYm
sdQKnsvIPxY7dLCFki5c6wdHLubZIT2arL5Kdab2su0t3ogOlKDtxZJw4sN3dZ693mJGfWyKQEl2
vvitOPfS5Gtz6b9yvjLUAhszPK+tE4kxd24x1AE21WHuuU9SA2PbIDW/AkqboIfO1FiT2V3R7V+J
d9foYtZ4GtrdOfG/tvZBz+hMAS6roXv0BrJXd27ij4t6Dr7tkZY20dNWnUdjopbdsF6W87zu72pV
WHbMQrcn0XVv1Y5O0eNcMEKgKAOOH9HL5hnVBopU2ITl+ltkNlU2dW/3tPglYeO6wyqjEk7en4p/
/H44hbQfWJnPWavpRb+TLaSbuAiV9Q1qiiIgkb1mV2z+W1ud0bOvqT1Va9j/ftm3w+4ZpG3woCvc
DHf6nhSo6NccErgGqACYLaiUBNXxsG4wf9PJ3KcP0gkG4mHCf5Nu3X2WUa6sCLkiQIoOttkZClQb
LHGNLMaUrLvzncFLC/ElHD5iUN/d6wW65uCojUq5v20kpZnoRgioJ4DF8jrdsUvJqgd18OpdvzMt
3lZv69DWETBwc/VujQ9Rlhb/EVq8xnfzhoWRZQ7JCOGUK4QieBcoX2zR1mkKeqZfnb1mcP0P6FUB
tkivfEMJf8v+i9FDo78ZU5QHLLoUXsPkT9TP8aK7xv5A3NOanb2KpcVJbpCWiewcsNi3VdZJcY5q
IJqrgrRgx4QO6vLfspkKX9Y14qPsHBYTICvEJRO4J6dQ8TCgev87y173Yv/uwUAoINr1vt4WAbdS
ZPJluF1NkZ7pOtO9WDBYsX2dqiU3d7i66T6eLVDLXPCpPCh8aMDKdogxSvCoDStn0HMwOeCDqmch
I0DD1tgAjgTjIbuG4GLuGMaFDQTuAzWGsi+cy6WrY7lH0hyQ1gD8vdl4+hxFucUisPRktjxY/brC
ihvbi4DoGvtpMmXJR1VOlPhWCPoIL1nkPof1bu9F93jzzzxDkJC4XnXKaCLcHR8V4lzfoBbvrXBq
ciyM0VKR3ydshZEJuCl0Q1CeWzpwpoVOV4u7Ivw87N08nvm9yKHj59DnihwxOM6dEvTn9zSmrgk+
8CeNvm8fLDs6cc82AobyccG+JujQx7RxThoK2sfoL4lbBmovKIVtSEwVPgatUdxPlYtFncpnGBKY
aOB9MQtrA8O7rBC4d5Jtzh6ONktpyn/XuoQ/5BpAuqVt8LUC5+Gowdq8aVDLP9HslbqOOFxLCgKe
ia9sPOoh9hXrGidAzLCi648HkHX7qat2jEIVU8VlWIQOzgGGzME0o80jEwWrgwuAZ8rpjQDfoQ60
eGyPYzVpFnHZZ+uhIucHawphGL3apJqyMLUx2l4k6PD5KN2iFBOGtSg+lLtm4uOP4d1kcv+s9yU+
lTMGa+QYgNUk/JJREGeP0a2J+jJ4wsLr2k3TjnRjafFoWLfiGCeNXNsLTNPXWoDNpqjmxebt8ERv
KYBIxLu3oCHiv0OWe6R+40a+5TuyiHp5YiGIJErfDFOhA06Lr9l97HR55oQxkmkOVUumhGPoumOE
lszLOokKf67kAjG4/rxCKdFId0TYGFSEnUAF7iirjvQzZ/Huj6anFjrwrAwlJdAvWwrecqbsMg/1
ouSuC2zm4NkAe5Wimu86RTRC+No0h23WF+ePpgjBoPm1ckJjc6+vAXeLmowgXX32XK6/d+67XR6x
1tk1ht+RCfQVpcdigl+lnBr8jeOvNFeJW4bOpix1f+gWzhUGcZbN4v5jYmfxAl3EUpzkQq0bfacf
gDK2EphzuXAvyvKnNci/iqMNWcr/Qgx9eszW1DQeuUTF4EF7Aq3dlDrUvieACDGaXTacn18lkOu/
LQ3eEFPwmJEemRs1190pKeJ3s7o8u/iIIwxe6QMtaSYQ/c6Or8N8t+FBXhxJ2FbaGZyOCN39yZ3Z
zapoT0D7u0odHARy5bzFKIgJAjOiii567YJBb3n8H6YwLGV9UuSy30gIv5rXY1iTvAnsOIOZc5vy
L+8Y5MwgZIuraJfAH0Zfd6rPUbfk9n+dHrpEFmV9C8D7cN950yH9Pe0k0jWkc/44rM91hscfnim8
IZLjZd0Sqfq4nvUQzz1fR67n0ubyiM7eyd5EwISDk4Hz4ksv2dbyV8M2ivgoRxFWdHHzDWXR5/fm
5hoThXf5ioiD1pvFkUPsGuGa7hkkb5KoIeX+RQ71LoGsJAPMY+SYQpOn3Zh6ykjvBdv4+JfQw5qp
0tBR8GPcpV+R3AXvXE0o8jy35nyMv83dORJqHCkfvkYsK2Mz1L6lq87k+FttJgz03lRkGCrjLg2R
XGsXODcJ5thEoYz3LZlhxovnQvtZDusgVN8O3JV9DL+e8khiuBP42iwpoV/TNKr9RXhgFpysIlWE
pNIcamPiJ1SsHAiiDt3rgh//aanr5jdc78s3rVw0JHCaPeW8SGJqbooACTbU+Wx2lc6FJqTh3TkQ
5CfEAzy9B/reKekpSlxEn74cvSw6HBbK6gmiPaXg5guqdLlTuoG6GNaaiOssbmnZGF5KJI/zx+ym
CkoQH1dfw3JhDQDvqEylFw3yBQ2Tkc3ImFZw3KZxRPHsV2dETSAdaWN2NgWUFz0gnVgq51lp91Zx
npaQdGhV1e00W3xldDv+1e8be/OztBU/Cn39B7wTTkDeXJSFn4M6tDa0P5NOvJ1ee/H1bAdBwCNv
aL//e3KtCPdJ36Lgx3LQsHvZPb7+VIIZ03MfIEMHHQs30jengASOq7hj1ot+jjdidJseFPGv0NOs
7psCItPdCXsefiLIW5gtZtOz4oH2c6+d3MDKNgVw9vyn/Y9X/FnXseBHP6lgo9pEbG35xQ1+Yeio
9DYaYTNFgyUzEGfiZz4GLklhC+ecbuI1/cJFRE5ytvtkol+aMPDdDAcggjUyxlklnkH8zXIsHLQJ
ej6Gp+/0LRmAKlI3wZsOOBByLaSSTNldi9bj66sefgPMipCqZUlLGNKpLXJw8Dzfnwb61/jY67Xc
0yNKffEclc+5xvi+8FJ36i+yCxrDtgPx3y3jZN1Wc/wTfIbHoVW4UTQ6CMShUO29ZKZ8ygKLDS8c
U9t7OZwvS5UDfWq56wvynH1ZI4+Nv211Ld/oPTF4Xtsl9kkxktMnPt2A+fvjfHhuqoWal7+geeGV
0qkmN1FCjC8N+4wk7KsL1ssQ+/2RXQGXBPMcbg3GpD7dy5j9EfN9YVxWWWdnD4GNqg5wJXMttksN
pfSp8c84/raltyao1O50sunjdR/2rN/JyLuWPLHMlx4bSpVZBBCupyoXjWCnZcgO1EKCNvcnWNg9
BG7U5HMq9BwgeMC5A4ZrJAl2xUQulPe3ZIfcKWaCaNEaKRS87lvWBoNV7C2RiUz+BoYkr34Kb5px
VW2W366UmppYoEuRAixSbBMCKVH3eo8XMvAObXdPS2RAR0RzeA9+mpLvVsUWQ7hBHSZ0898bkLNt
5CHfl/NRMVtk6mtKw4OB9burbdzWj7TO0aNHq60jpHOzGO61mV41hA5eBqVPBZG9SRvXmVj9egRj
Feaga7cV/nbfTuh/gKgZWShVhmb/ABvVAxv0R3AN6po7QvWAcBNHnquZmUHOo6k/fcJ9kys24A7z
FMTaQ6I4u8Xq/5SPMBr9ZrcwNoVktgQLtQ2WLl/a6sjO2RTeKzyDsrU4dcRXX3wOfc30nkK0Z/AA
PfJwSDCHaD7yzISW0Se0blhRLrCe3aIoouwQaSa4kwVzWYRQQCUxaUwG3cmYM2c1oK41WCVzT6+y
wxzfq4vLZ9oi+20vHoNmZGDktYQosvb6QxKYy2Y36ET5qxE0ySE5aOgcFV35Lu6UdYWyI9Kk7mJR
zrYdAYxqqbBntKqPrhBAjUrkPLJ/YPIaI33JrnliEQccCRue2AI46jv15M0Bbv2i3NIubZ7ZBmAE
bfxD88/eZotRUe4LBzVwsWFKtaBG+eTdpQpD70i4Het6eRwg4Bzg0TW18XUamadOJiBMw+rhIEIb
wEmRMOe9stRaJ6HYAeLUgR8WAWOQgeqwn8DkYwkHWgpcko8OFeJBVX1Sj09mtIk4oEW2PGRYnwuQ
XMcLSgqjwLgBR6/Im93B33aQvlVwiFMDf890fAMqiXeBFRwKCwF8Ydxc+xOjiie0pF/8yns/eahy
r4utEP8jw1kMqN8BfToj7Pv1Nj1drhfWRGIeJSrBJbSNEl5Pwbpj5biLfGYI1OuoAAOe0YLPjZYv
JrTd/7IqMWyJdzlghm9LirujV/X4FUM3RQBMliQ1aeVRfM+jKNBurYN+pxekfY5/fQXWnXJcHNa9
Th3m/4ONzcU7ZbP2Ivf3pWB7D8vcvMp209j8TenLZe3waaxGYVjnZ79hADtFzBhtTquhQcJ2HPWM
6qf/e2pojBgoHv0de+v3/0T2zbv2jCzBNWspf4xCfSGzjFNEseQmXBDnO9WHzc/WPhDI20att/5G
nHdJBwhQ6DCgN/wJ2BrmSysSlNFyOXuiVp0TtmPHE3uYT2WvB6ncSfgZ53hQjg0OgRv7EyZQcnhp
scl1vlbkS4zkVQrm8D1vqjl51RUaKPuOdYJQiqnzfPj9r+yox701aexPm7M0g0oVoWzq7ImY/4Lu
TzRom1WLLc+HtSN/WqH4mOifrrTRGt34S7KPbmy4AEpkbFx0M/+46r2XR8p49BKNBurJFfrYRyVq
QFPygJehKez1ghItEQxZen6RN7GrWD3U6QEHVGprezyKrnN1E8YZczDzTaFjl9Ez49YXdDALds1B
9227LAuIBE/2nmQkKmvhF3JF+fxHz202BhKV/wU8uS3FQ6As/OHbBhZGyBBiv1tBlGCm/y8P90w4
qeGSJppPalxDD4rcKeTNC6H9Gc44oP8HKXTGc2xmDQuMXT1bmM+ziloCUtwdA1o9xJ6S9afsYYUq
wpJ7PQYX519INXvf+maegajw5n87u4boSfzR04B/H1KpKjl3dqURw0QRy+OMyw8hyAQCNS4eM72t
frSH/JStB6ttwVctFTN7v714x+YHE5f1KwIOaDJq2dmxZbCrY4cYXcZTepAPtJMnG4utpinBEG89
xJrxWgnBaQIgKA24Pd2SwC9eAeh+qYWqIPybsHDALBMp+0cY0BmjM0gpCof0Ztg1gYOKRFp+VMxi
gZcJjZ7JGDcbbw9MtwWzg1oXxHtpx/5FT1V63hobPji1GvhXPX0ogBgbz71VKgzahMJntuI4EfqX
VLu+PfGYbPy/QZMvBG/FIR/wmpuITK68ZGSnsehS5/NU8P8NrVu5ojh9olI1b/6siHfkiHXgvlfx
2v0jBfjhxsWhHJgvkUHr38+xL14js+6Qy79XVUEMg3RZ/5gtwBWeYpvYYZdWcropZ6fkxAV9v91a
M4ukofOkgWDel0DHnAn2Yj26Q+6WR/tWSkQ8P9tUKTTvygBYYmo1qPu26KfyiSLy3rZJxL+aLsCL
ApyZWg72lBnG6sFgOZ0jwMw3HTSZ8rgz2/QXBq3MZDWUJ3djd16+1y4pBD+ulwzGq9aywOESnHrF
ZsDovyvocsY8fla5nwibKyuRH7MHnidGaK/WDJ+YCH+AoGrT9whOj+KKGoJBRh0v1eLrS8lMyX9p
FI+3/U5Jjahc0ekQSMd2SS39p0y8LgMTe2Eg0dBPVy+RLzDl8U2YgONfxzdPHf7LxrNFuxQ27EWJ
Xvd88VsFWGA3pb2oVSEtBl6Ep4LTYcFidZK/66iwjwhxMurQI/KaHUQp7+IP61+qdnWLhVqGHebJ
GaJIvIMSkTIPDA4T5eEZJfB+bgBNMu1uAUiKDs6rlLDv1AVaihsuLAWgSfC+qERbljMTYjA4xNaO
b8+O9Zd+VqpEUAdwV0g5pdzoSiYxb4xosp/wQ4DQykH/GCG3aRJaxQ3VGkoaRdTaLsU+J/IKijlh
oRwkxZ8CkhAJw4uVzXg6N3Uonl+wNn8EUdGDJgwIgmr2p38xMfT58gvnqWo0fAkb5MkA+wFX99go
Ns5SHLSfMR2y2ipv/CBv9xpDZQPm85E+XSvR1d0lnDxvTNy/qTEgG523hKWe+eLZ4CKUsrdcgOTw
Y1aDIVFdNv5vaeYWcWseO+UH818632HFwnInhU0t3fJ30y3p3WEcJIBxr9qVCitGjwPAjAeJobWl
IHDkXv1b3umIYuuZZxObqQ8B4KOsf9uHpQqXnGVdmk5vGrd77y1R8A6xFnhgZ1/4hp2W27P8JWsL
TvnT8sU4RpqFwyl3RQ+WUAYtWG38YLb9XpJ/RbaA0oIdkrEVZxT7ErTKZ3MoQdRFIaCEnqlNNiCr
bbg5wHHZS3BGBKJak2U7hytmnzpV2eNAbS+d0I7URe5xsQopKXvaOPn57ovKtKtpR/384N4cv0iq
eTIoxZ1nyoC5lfHJgErfHWNxe/TVxFkalfqK6QzaI8dQllYX7TWkAFJb1Tp2RecInP6F2mTNntrx
50j8o17cQJdyMX2sriVKt47eOrfKGnSOr1bOFj7K8mO30CfDMSGv6YFvXHOJP1d08tzKpJiSXyG7
f102jpm1TjcDW7sfb4XEXntV/NBK/t/Pm47f/G94k5lY5cD88mrn3JJmi4R1ye13N5NfzQddN4Fw
uuWzF/zjKEg8EOusNXNWgK0VUtButVz3wRZXjSc3QxNcgaA9MvBG/1a3ZTWZ7ynSAyT9KgCOxug0
OJGibujmY9qwgnjlm3ItSusQI4pswOnIwUMQx3qdtjMKLuqEF6abUV1q/AQWypjB9tcKiLNRmQQI
Qndz1adIZDsn4N/qFFGQHHFGtIrRT3ZWo8nxkz7MP9/N9dDHt32DDWdOz248D5GF+V5WZR1EbK6Y
zVBibjbcUP+/W6JTP30KbV+vDa6dnf0iFqlVljmC5OQa7nl3sY8wWCI4DrxyM7834z718XzMgRsx
NcndQ3yiVIPQ2eITt3EniZi48NgA7Bl4NskSb255PlSuA1XHxswgphgfS7W/cVLCvGbAJt8/xN1+
IRYjWzf3vMCCu0N3CMCWdRwn+scHSR04U0vVgL5VpumIMJr07QquvUPDiMWImX4d8nYD4AO/Qq4B
nDL13oIQNEiNOIf2/g4UihfBDAVZx8sjc97D5fd4SIjupKReJDUc0EWMKwqxnw2yUGE3icdZoTkB
L4csQywZdLMaR4/RRKOrIpecRgxA4FCe1lBZ6paLlkq2KnGnZHHK0yAkeEh91TFsgkene3/huRox
HEaaMoJuILb3fxUZUlKXZQOOPIvMc/xi3CSu3RH2F6Nijs2L/TYMq4l3wVvLOpYa01J5GMg4fDv7
0rjhk6qKMED1c72QAPShm0cbIN814bhdqGXtZaY7Ceq7FMsVqRzspgGfgcC8ff93NecKiWcWEiVR
GL85pGa/vWFVUxkZpQ+Le+ENRzvHDB4WOQJNyZ3y9eP/Fv1o+vHsGxbhAFw5IK+j11QdGX8z2kLZ
ip/KFdIv5M3i+zlqygwnGWjGxVjl/DZ+7v2cNPJP2L2aOOZoWizS+qJ8GHGoG3NtIde76dUAMdzL
uiozPKF155feqV38AO7B8co4Ep+9ZEd6VF2s5u3Zjq3Sgr9f9t/XJnFsE6iCzFUwJhKpu7tK6hK0
jWWE6ei+LvcATjLACGuACSl6mcGHBeX0hX4RohgUGiAORd89llhIgYLdoNbBS2pWt5NNxyX/yEYI
oYLNJx/V5B5zGXXcf3m1BqLBvPhu74rYptBYR0xo5Im6EcPwl8lIaYudwg2cwsMO4A9bv3XMeZTT
4KR06Vl96CkDgFyLjU+CzbiRdFRRc5TijfijRtM1+9DaNAtnqIrA6mxoeY2XC8nVCVuQky0Aokfv
JROIikA8fg3VtH/7GkxV4RG8lAwPWG/aoK6AzGhyYxD7vClYW9t7zR2b+DlLw9BZ8Zb2LuXNPl8D
M8ovfrmtBBOketv/iRkB9S6DQV3rpkR2W7bqqPNffR3wqtkmdbEGbV+BixNeT+b7Zie4E7poNQUL
XKLSwgQxK5O1X4+AablmSoqUrbfwlhS4mIV1grJuNvbxHu1uVeYDBdWGoJE0FZoYHM/j67HjiM6L
IXo+q0AwhkScA8E1FbiGO97yYSflQ+OnMbsT7A8nmEfSsnqQa88irC+aLKASiZJ4gzxZ6YyQe/0A
CGYn9UcU8BTx1Ak5XzEf0lgqMl1Ujk9q9R+HqqhPDzzQB5MFRVd7B0b+mBE4OruaoESQXlPHJpSN
egoxNaWQ8+HWxnZ2UgvjtEu8FiiClkyX3FLuPIlTIUR88oZruPIxdcx9kCZaVltaITr1W6yybVeK
kR/CgVUI4rI77qNZL8JP0/qKLVnGZXk9GyNgW5lzhsXo6nrL6rDEcAfklKbmH/vI9rBc0j7xdC2f
EtPBBqADD9P3SggJRagMdn/P+xxKNIzD9fk0H/5GuilJWOilKHBycOtakbqCdPDhqjqff3MnsSUS
8Xtf2MZ01jwShqvlIKgJW7L+sGTQoq5Jcf9XUvXHkAjuM7oay6g/trkY1T0febmR40deaMD/PIGu
dlLjRB4v4Pjo1eBA+rw9CfeES4bXFRjMXz/FY35r2MgmXoSqTJfUjKRwuDQzyL9E8O+v8/klf2t6
2LeCxuA24VmsekY0mFZKSqsVvkg4Rr3oXVzark0afWfiTBH5tYx1uRZkHpHCuTG0j2mrzvdKbXlP
fR1KyTVReTd9ej2SmkynSrVOpjZzRhnrci3wmmGIyiJhI0hOeneQI34hJU2b4pEC67vZKr/CWDoj
Ctit20h9btuvchcEdLEJwySpkGGcBXtOKnnjmqxEbgoe2J94eOEy+iiBzlRsS6aEaYxnpgeBfAeN
V9zfDzjZxXA91RL/yodbYDUzVRkxwe1BRmqSoKedLQI413apHBC4BDLZTBStyXvGfsW1yiMVib8B
rhJB7Sn66zI8zK+fMT3UkXftvWkWAq1sIYjPYYTuA/FS3L2nlahiBQN5wNJqxnzQggeL+gETFluP
yQdGqL1u5dIY1JzQAHkF7myX8Xrr7fNvtUiemTw8ga4tysZolF2hb+Pu+3cDnPcfrKnz3UGEta0V
5RBo+PRDnMnhm4Du8Jb8LECWismAPbiW2+2z5Xx1C0xBocfXpdwOVmwmoulaxa9Nvprz8xv9aoVD
EeaOWG5VeukdA6SvUQSKmSWnIJ9u5a4rlESqA72ROQ0oOznhWh/sSvKisgpK2gtW3wqI0a6uNk4U
bO2qbGF7hIUSBbknaGdYkAra7YPmqdz6E9lhlFUWxndh6+AkWEqVM19AUwbf9gchudzbeP4DHskr
XWTtvNhChBX2iI1dlYiglPmCAbiPoiD1KIDIr4Km4GtjJhqHjLGFJFKvvjEIbc5FfK+8xGdttAJt
gAkXUijeFDSdHCxQ2Nhj5UaF/ZuUwXjEspD8/QIvMJzw/kVUA+pcbHPgMhWTGDQbRywpLF6NAgyk
j7TQHwum+9IdhIwwvFa8FaDfWeOYtBACygqIOM8ygtJWOgaJma8DbwaSpBOlMPjrVQtnAPe7c8x1
d1OyOc5/7pxEki7lPlmJCFmS6rTiRV5gEhRvLTQWWXcWR5CT9jw16J9rqJlE2NzwmICMhHGArAjZ
BONXGokXnB47Wj9m+GJNfoRDi4unOrhbXC4ooqf7gL0YRJ108K5kil29TX0/F3m9ZizotAXKg6Jm
78qIwID2KEULliYmOQm0lnrAL0kpuMvrlmyV8IUMWtY3BTwBSv0eyxRazxAbWrF0ATKIUjqN9QYg
irESQOJkGzcITUSHgOA6+4LbPuETxtsRm8gtC0pv2JqsCfCY/I0nCwysUOLeNLLZitgCRrpfQryK
Pi3aggNsTuUh9fxB3e5J2CW5upY9RcZk2AmqYdOjNjdeNZPDWRdvvyJpA2pzIvfbZpaefl4XhD9h
pZ6NJiraKSYPW/5rj4v4rUyiEFc2b40/XzTTa5zt3xIt4UWWbB7me/osNxCpU0Iaya4QtYYt3vGu
22O5hKsEthX/xLXDg///uN9cTJ8W7x6xqM3p3jzoDSRo6W5TdQm5jF9AtUnESwJd//P1mbdIOy40
yNzJcPgRQDYG0xcg7hg8l2H3POFZ2vSDnIQALmAwFWGa/pDqH3d0TWRoownFiUAiCP14wSxb/qTb
qcLl6kTjpzJiaFwqFFHg8e5Sb3fz538lCDCui4FJVBrmZ5jrFbUnxqiKzBKPg6vfrktAFtTMJR2y
42vB+6p+UT9lCu6MW6xWHuQY2Lt+U7VN0YoCvg5yE7jA5XtI9dyNzDB0D9lCxWKbbuXmU/tjet8x
QxvbZqOmZ3Ylz6zGQK1MZCHodOLgHvJ7z0WFYBSVwZ8KnncCIIhwU0v/hxl534hETRoW3QqwTyvx
Q54OPHxpKLDYBzAQENsH12WNDSyOUh39rZx5zUsYP+wKPEf/7D0w1O3gO/0mykb5jvU4/yUtPPrG
Al4MnGVgKl6/uvEgfJyeQgugy4zAOBmSUkFwFJbEJtI1vI1i6j7vwtWoc0/x71xdJ9FToWJ94Cu8
QqOhcyNU1D2RadAyRdPm1ILkwU/jUGsIvGsdS/IaN+JCOFjcYwmeSV425cTmgAWLlqwmvioWk8Be
MspgwNPM2zVpx+lIXkYvRxTSrWq91z4GsnJk7q1nQrbpWwDAIZHQrbxB4eoKNyXU5I/K5wj9vSZW
0Kmff5AP+dUJ1yj0+8UZJBkiARymiEAqpjwgwL9rbIANO2d0ErADZpkuOqqD2i+DXjK0UVOsjhSO
JD0aPl0yEwtdbK/zeE1uDXngCzeV4H/gD1JoNHHI949xX/2ymi1rXxJkHqwF4rzPg+WD0yrhe0Ch
S0cG6F3/OF3UoKedPCmG5beoz0xAe12ClVeulRxS2enkJDj1cG0lUSyukazER4W9eeXgOXKZdGMN
Ubf8IIjIwMAUuQ12i9JDuoc3LOINsDfkv0o3C5a8XStUBeeEbpWcqHNzZWTpiBVwsd3pKnOOXeyf
hsKD0TUWhVcTlPDalrsvu0fDPfpAnMseGdFFoFWvQLps0va98u+Jlh8pYgKxnpkFdFHgmYBONAEv
131/xSIDqkpQQhrGHvMtZWVZK9W2E60RyPIwO9+E8+d/0iyMEu9J9vVsx07RiOxExepp69ZurVYt
ag/Nf51+HDd4xBEPIa4xzoQGqGKlW/hMINa9pL8x9Vr8QuudETSaKL8pByMHXz33wA/n+9m4dnDx
mITGQdop+3En8YVPEI4RvQsklex0gaFYxgLLepcbQAT5bIFn5bD0Prl6aq8vxcHbD7RO/UQZ9cXU
b32wLBrPu3yqA/nTBq+s5wVk5pJ0rRGCZjMEf/9+sRr4EQgpkwOadNI7rNE9SOFKnjx4ChCLCCSg
zf61Fb97ewVFO/IVTwdgZmaItTSkFBe3DpJzqLhN/m5q6s/QP/vUdSVu6PPlkjsJxcDIKJLdHRsN
QVwLcFhBdIZXw/v0DdsEfs3KHvrAK4y67BizExE+/FroaJqCXXK5jVKAA/egp9iV5sVPxD+RuACl
RYzsY38P9nmp5KUHnjGIbyZf/6B2VJImrprySr3Ekc7TfFPcEvMdRyr3XAUGFIImkESSvd0vyCly
c5JVKhOhyhJsBCwPu3XBjtdAEI2tknQaBfFUUHqbKfhtBVLsnGBgr7U2NqByaO3XaPkAlWpIgZx9
hzN+js55w2Nl94Qd5Yf+hIAyyvGX5Rpw6KAPpsUeQdhyaWSp/P2u0CHHGLjtK+kgcqwXgikP2TCG
0ZNDJbS0Pzmpm2h5o/+AFgVEgjXDNtnYeeaZZ3t0/ATunAHdv+8vSr4qqDWAnwvJpVrjjkZ0azof
4p91Z7FvLVRvb5cvaxEe8YRrWEF3PQHAg9y4Fyw68Otm5S3mLvGRsGk5FEqeqvCRrl28asCtohyL
xnUS09aI8JRux4q0ULGFjPRkL/S2VsUN66p3dTbPKUxxJL0V2jPsOvWG5UeesxJjFQlnl9aXROBN
EZAQ0VZOPzX/mXA/6xFi7X43hnFpFH27A7C674W1GbG1o/wY5I1cxsRu45hahZSANVBKUBG0e3JK
c5EltrZAGQeMXp2DC7QKYH8R0hpCAw4EwoMBIk2+TcQfcbVXa1WWL4o1M2FqXQY4Xd8D0sd2n/dP
U9qnQ1b/kw4ka6M1+d7WNbZcXKfCcjEHX+Ntrs42bRXCs8vcUJR4y2y/FS40qlYBjwrRu1gc27c/
J4Y3bJy/YfKWEorUAARr5qzAxMs4abqp1gFWPtxykYSpuInwOKtHO68jNYAxZnvfJgRcd0hz1pfL
MckbhHbvAjiFdM1PwlPNgtMQLLJGsqiIv2Bcy06z2heUu7EG2bMePaUynUYUvK8rdiIvDQUzajuN
oqm5yMCriuOS4Cb4KjjE8/g8FkIjmb/SompNU/vov+1mCnqkvIq39tvHD276sbIwKHDmrX+zAIJC
4pb/Se1pr7qL2Xu5aWasqYRjPI7b73z2SJzPs8chgU3wbyCT0mnWDwQz5Mu5xS6nPkxiGTlOm+DG
TaW3eLb81xybiZWZvSCyLd5DC+NesaHDbwg5TBfupq2FwJknOJLWOq8hc9bAvQUPQ/5nfYVSeP0T
ZsHlKovul4E6eIT3eJ7umlsf0Rji3nywvHUFVXrD84AMeXekM0e98bbgx+6RCY982fJ6v5V5tHZ7
IwUR6EaBaGC28oapGdKuyTV6zqLuuv/Hfo1ba3Cg5jmXP511RGCdE1MoQj8gBkKKZmzl5d4V38rk
mTJTn7y6j5tXTf++uFEU5bBSDiIi3PQp8hY7f183cTZwd0VpdDEY9YkjHsrQ3L+fWOiQUButpsAh
yUWz/1WIGAHXdoVjQlM82LAc89r0BmMZ/HFvn7NhZLAYNL4RwrNVpFH2UAYurKZhldHLu6rFjPlO
rO5mTQWtmORb95gDSkoBNId7jz+Ekwo9+o3/3ffFPvjYOVClZAHWZ9XdR4zW52ORwKdr/EQ/aS/4
XecF9AyBRD+ZqdQxzjadWklBsqzuvYbvWDkBCmfUGXi7jjtY2oh84IMu7+OvtnTjrO8V8YCaiRbZ
MJg7Ej77f152pIN1yF0q/ksu64bTlupu9bP0q509lFnr0wpqpB/NjyesLEesw8XtzgFUQZhD5gsK
aERCNfaNdRzXtDf/3co58CqRYdtGDKUQq4wQzfi1MP6/EjY7Do6euTZ1pEkNKpgwxioAKtUHzD9d
Bmqa5FvO19A/mtpZbIGMRbhDcake87gV90JLL2vvzZh6C0EmHd/1D2uSRwoPRgxqn/XSwRqyGMPd
i8x3phGVF1k4KUZKyOG6xKMnRrkfkCfiMx2csgRMOr9fMjo1FZWv52lPNH+zNGzO9S3QF2nYvOSC
KhbZ5SIKnQjwbKzjasYOuS8JoIyiVMgca76aPIUs+VPT0eHeGXybJTtGHbXDb6EkPPvPenI+5OoN
rRKOdIRgG6MBwdCaA7bFwFGRGArhIagiLR6bsQb3lrBtmVfw6R5+2fY5HFeUjLit17Ndov9tGI5y
v7cCwajg/Blh21bHxeton2ALfMcUGxBsm53XXifKJUmltbMnha7lbMpXJqty1rj1I+10eAjINauC
xtejpFRBNjCAUR+kGLeU6gUmULwAOS3Gu/bT/HaJukehRTCl+pkxNvK/rNZebeApYiUEPxIc5PSA
lTkzCz9Zi4mGq7U3pFmLpZguUBKVibzPqTfHYTDwhERe+A2UU5dEw0VZt9J91sVuubLF3ERoKHMH
0yEsSAjYKDRrSGLEZRU0eVxa2jUNuruQSOzDI3L1gPeAiMa34TCRlj5uuyVS5bzxydkrhn8BGLVV
9O1bRtfBou0rYX08gBmFwbJ1WLg0cTk4HZuHEJLY58mG3yYeAyrlJwcV6+DWBM4S9pcgRlowsXR/
2fXDqdqIypFNnh+0dDnsK78=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
