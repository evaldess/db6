// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 31 23:34:49 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ counter_binary_32bit_sim_netlist.v
// Design      : counter_binary_32bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_32bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [31:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]L;
  wire LOAD;
  wire [31:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "1" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
JEc8/Narnz4PVyZL2vcIxB4eGC2WOUTqGqUN/+5PFWqalAjpqJe5OdZWo6ii/SGFdoiCBSv5yNtJ
Xsn0iCysPM68yCLKypJ7VK7pryz12rmRBpg54y8bAKAHeIzC0/hLlbWx5t2+D247AMTJ43Nhq9qC
0W1yzcWgDcJRur1f/nIlo5/qJVm3upvkUCAFrSnC7Tk2y5rrDc4Y1h8XTAuMR2zu7AgRQUEQq3Zo
te6c73ZjHNYsOa6i8l/YfN23b+2bfruhRBR7AvMn1u7FaSH9F7gwZ1LGVxUQMFf8AsLUQJC8kMU/
RTqsxy9+f/UaLuuRQLtY/C4Get7s/RvOC+z7hOCoq23wGLDlfekUjNB3lCPI6Qg3aY9SAM9G0bAf
vo/7DiUkd6oM4mwyKuWALzWP1thdblBG7K9OVxijBZvpFcGVRmpRTpSIQ4ArF9LcRQdPo5miv/Ed
0BytmGdL6mprzTad0D8nBVUyHHVPkfVvhj2jnuhXSutA7t+lSQUvmJ9T/TmTzTIcBOLdD4E2FeMd
9qaftaVtzN+Hno+hJe1BCxFyJq/dxMx4ky6gE0NO/GPcPaBtwBZt3eCWtnVDRIEipyAnM24dJpSd
Wq3o+oTZ2kg8HzFAvD//0XqHXFKf0zdOJP/ottw9ynRNFE7aDNNU8ejm0iA1weIlt+7dO5U8Q8Kx
oQe0K3UnWdEZvI9cwuwBsbch1KlRGiRe0EtkfGfftkCAZ619NWm7Gnuu4K18BuFi7L32q3PSISrP
rI5S5f80Wy7KfRD27tgJ1Dc3pYuF7p/UH8C+Ihbf4KTrPSYrBFzUrHHtodyC8BmayJFG6fUtaXeJ
NJDEG7tKbv0tfeiMvHe1Pb+0FGoqI6pEYfWCJzcElKFQWOSrUMxlDj5c91K1nnIfElfGDt+67y8b
u6h851Rnfosz0FZ11wu5LuqEErkjatU8hV1zpxNK7XFWCjBGaOIAtYf66qIIpUv6cwtj0UOTA2wm
a2wTaXrkAV4ojrwufcWx26BH0sNzUSZBoZOh7vN83JF5/9hGeC2CjM1yS80fiS8j1N+vswNIr/Ch
nwbRadwkjZxXxn3pm4SmbsC8hXw1pGSp7bkPjkFYZ6We6/Qnw6zOoqP/YdLxpRnaDwaniiG8NHkD
4DjpnDk3zVBtRWCUE64mn4E/dDjQi/xzcD25IaQBg1j3hGEPwdp6glxfrmPd7MR/Pwwp2aQO1/Yk
0RydLftJXw/OLOb8dkDp/Kozj7JVXS6PVvSNW3UAFmW/CYMNqHT1cfGeXHYxjpUt0DNJzuOjqSBT
LX2jG5iaH/sI6vrbQAX8eRysafqjmB03zz805DaucAPPpulpao6+0QBYQWeb0dfPj9UMGtt+JwPG
POyBfQANae+ULhG9WdNCCT38t5sSolK14oNqvMxn0dgzaqYj4CUXK33JHf3VfXdvfC8uddWQBqqf
OkDVIYcP57zHq1Sxa8svErVFl3xZEJcMi9u6IV2rmIToxbG5shj2svTSOcGPxsfKNH2XlnkX0baY
v8XDBEuju5P4+s1JFmZdutfvHq35VERsi7IC1L3yM+g+5l3V1SJ6Ihp59KSuzgv9RROKb2jN8C8d
KIE7BT5FDhjjctDif1wWwIOl5+LuslZpas6UxzGKPJx5dzhcN+VSCIG4jTfzBbH3Sz3nd3HrQvSM
iSLC7hBXJApNqzXEMKESU22RURUWjGa0uBHrX/ACRxrzK6Y+yj00Wr6Inf9wIDiU1LB9dDjVInYG
biLC2Pz79wXMvOpm05LCkdSGFtIe74F8XEArGEuUEpBFOjblTSTb7SaA0k8grRG3a+dmwl5sJYd1
CbV+9x4hET9nMJfnQknC9iASTuHsqMlrGSkaCJBs/g+z66mqxMA3YslsrjEQOKmJUGVeQoDnzAMM
9P8LuZHvCqeIIje8lkVEV6Lz0U0k5ESHnPNYqjl8uNVDSnnjWkUf6cKiOGGIj68mPOMNmdJdWC2v
jM0+K1D2kliWvRYNizdyvhbWiv0qCSIQygzo0spoCAl2XW7shyBJwHk+R6+Egm0vG+uyoLfKjeLF
BmR9bop+YTnN6uryXJeNq28wFuXpQjIQtPisiqy9EbUVievJCY0FoC70gT+4txh7lkRpMlHmi67g
YY5LjjUdmmKrIdHzq/CtMdl/EFTsKNzoNpho/mjqwH5Z00Ig+d1age0Yds46+Om+as/iDOzOgfsX
20ZGwzoAqkRyikfZVa56s3pEPLBHc4sv8yZ/AK25I6Xwi2C59HoF5o8AN/b1/KFyu5x+/LmNudSU
dMWPTv63Mo+tSFnx9gCEwf4diZ6FBIdJqv8xy1xMpmgrOtScmWeELTAflKUlOOucTqe3UJR9q7sm
E4kmR/4A4uQyn+EKt11eJk5JCGpuDrhEEwJKdaZQFbG/4JnMVwn6y9wx/l1aAPP50jlmBplGl0ry
mLwVcDSA86uSmks2WEH9wH93qFpGClrOHQHK3B3DlPIEXDdTabwjQP5kPfmQ2wPgYoayy3cv2Vcj
jOE31/S1p05jFN7Tca+WYDTvrDdzcuwNUMEK4d/PW2D4oUVV5RRbpzDiyw2TEc1Y7Egs5/298xP8
XgwCzGfP7LeGQiOuKlyzB4NyPJ/9z0JG0bKSS0E5Bc0BYvXNtXQMemLO5NrCDEfMkVagMI4/fcWl
lh9D31fElm3NCBrXqP8Y6mat6LZhSW0EKY5+qTdr3eZhvfN6lUm4F06ftF2VgBulNQaB3/8gO0I+
45Y4qoo55HHJB5mjiyOuFwhPfuqJ1Cyh55TZry0Z9swV+e/05TT43lRpNIMP5f1ynvZIc/VE/Qjn
XNRWw9iup594NJdHjEl+1AHtpTW8cNyDcmQIT2HsjbZWzviuxEDkMYupPk3jXYopcX0zfHBPcGpl
Gyi4KyeFmRNyA6LPiPGmR+Xw0FMErmSKK4k2gQotOArdNGiGB/jGPrxbO3U6Kefp5PC7vfuvsjrF
kuWq139Bzk3wbevApgcED/DPzVDzeRno8w6XXBtKNhGakvuezcvPULLrB/EW7bWYhkYpF+G0PabQ
H2vT4z6k+OEY81NMw5vojzzSbtAJNtQ5euCnYXqWlXFB91rBfb0KnvAXvU0QQik5v8h6T758zquG
8e+QqNwaVdiGRpP1JfNVhSbxEnJXU/0gjq2RjBs5QQ==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CzIyITpL3mewJ+e3mD/8rw9DAHJ8MDtY1el1Ay3mnUzI5PggnaDy8Zfia/Rv3e41uK93ZtJrx/3l
jbkh9gZLDNb43OmiM7jNep3r6+FeOZXfSsXOIMQZX7S8UPni2ClUGCovlY0zSAQGq4Ld1rsJTniW
GQ46S+EwblnU2XgJGJzuwpvx96WlBn3GUV980yzX+c+18UjSwsq0Di+is1HaiUk5TwWjYT6Th1LW
SD23HCtoGIJF7HxoiovGqnoSwt9OtHwB2/DS1x2gAvaEggPLscYJV5PIh5mERGcWp8wQVtRzQSqy
GoqQJpJa5poO1dkxmK5CC11F+xDfGWSh9ls9Xw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
mH0LZfapmpZvdjwfwuB2ehLmXDtaHvtHrVJVVGxwzIi3EAhYLNGJG94xu3K2mPaddSyLKv/ooOoD
pLyweL8o5BUED6IyteWlrdYPR151CWqDN2ycnZHVhT/BdMP8+ToLVEjb7uaX4SWSpJCyTsTwWcEU
oUf1Gf1Z2rCyTOOnlux58Gqsn9IlIeDO7QtnJFYfXnlme6/cCFPeufWAf2+DbF/vcRaMWWkPcGOW
8ZViLbuuOyBfQ30uxKpAXXdqDW418N0p+hWbj8aeEgvcwqH7IY1Nvuh/hmzJoNqG/zf2QRawzGkG
mAHdEQzwWzb7fJ+t3xZen7mZ7A37k8HuvcKoCQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11488)
`pragma protect data_block
JEc8/Narnz4PVyZL2vcIxB4eGC2WOUTqGqUN/+5PFWqalAjpqJe5OdZWo6ii/SGFdoiCBSv5yNtJ
Xsn0iCysPM68yCLKypJ7VK7pryz12rmRBpg54y8bAKAHeIzC0/hLlbWx5t2+D247AMTJ43Nhq9qC
0W1yzcWgDcJRur1f/nIlo5/qJVm3upvkUCAFrSnC7Tk2y5rrDc4Y1h8XTAuMR2zu7AgRQUEQq3Zo
te6c73ZjHNYsOa6i8l/YfN23b+2bfruhRBR7AvMn1u7FaSH9F7gwZ1LGVxUQMFf8AsLUQJC8kMU/
RTqsxy9+f/UaLuuRQLtY/C4Get7s/RvOC+z7hOCoq23wGLDlfekUjNB3lCPI6Qg3aY9SAM9G0bAf
vo/7DiUkd6oM4mwyKuWALzWP1thdblBG7K9OVxijBZvpFcGVRmpRTpSIQ4ArF9LcRQdPo5miv/Ed
0BytmGdL6mprzTad0D8nBVUyHHVPkfVvhj2jnuhXSutA7t+lSQUvmJ9T/TmTzTIcBOLdD4E2FeMd
9qaftaVtzN+Hno+hJe1BCxFyJq/dxMx4ky6gE0NO/GPcPaBtwBZt3eCWtnVDRIEipyAnM24dJpSd
Wq3o+oTZ2kg8HzFAvD//0XqHXFKf0zdOJP/ottw9ynRNFE7aDNNU8ejm0iA1weIlt+7dO5U8Q8Kx
oQe0K3UnWdEZvI9cwuwBsbch1KlRGiRe0EtkfGfftkCAZ619NWm7Gnuu4K18BuFi7L32q3PSISrP
rI5S5f80SJSDAX39XJq/02L0dx2wDWvEuQYBJ0p1fibjR66n5MmF6Oq3+tyeHSxmRxB3SFUpW9tl
Xca4vItsLHdB0jXSkkjbKJ9Cp69m8UhDAh5uhCijyep7E2e0xG61V6J8cNDZzVN6O7OgwM/DdE9s
q6dg3oUjzJsHcuDFwIPnm2zlsSerSsvE6DEGNoYU1cVAcsRLsUFphcypWdUNFbkZwqy1vi7Kor4z
lV82dntZ1Lyc77iQ1UxQbfx2pruydKlUyEH4iFbElAutGFSkOyMgzDaWJ88TiOw/rFwgtP12GFvu
HUOmCsy/zj+e0PshAl8JsUBreSyxrW5g+HdYq1OakPOX0W69uxVtnC/5I0MrTTFVVOPGbs8ldoT3
lwk84si/iPvBvNvfrgUDXJcGHBjjqARrTTLwJKKyhbRPFu6kx10XdzPS7UjNAvPAHK2YNKok1vMi
0Z6yD3jaFFOrRGWOSPzXz0LuM9+xpeOaCJI7TYEkHW5Jhur/ZKa6PFk5spv9+pgCh3++RsOlmQwd
bfx3E93p0mLusUGrcI03BoVKxWR1WgGRuPmZ5e3KsIK1nrBOyjkgAEEybO5sTxT2yr4nej1jRicu
EylLko12CAjv15MrcZb6uzq3KJjqLkTUNnusS30REINW9w8C9mvrj8OkYRKKHbGl0rSOMnRVKEEX
V3sGlRHbYRr28hq5xq7BLM/egH+rQQsKVa/54golf0dYMek1FMQBnFkBMfKDW8lH7tuGjsbdD817
ZW5dzjBkYJzZPKP6B/27UlVYRFfbQatLJKw4e1Wy3udYQeRhRw2twD0PVzDfCHlvpZ6qN0ugQREk
sQhZrqrRbbIQjsDkt0w1eczpzPWqDHOPshMGkQiqcbjQqBJ6YhcopYGqzGmshJAyWq0fFbvG1sIk
u/dJ/2SKE8GfPUXtgiXChHY4/nK0bj31JSDq1l92tKi7L+aq1uYfQ06Z7xIyWRmW8t10rHHJF1RE
xvQh1qHTfS2q/zxnJuSF3KjAgEOiOB3AFuOt0Smg9IkCqLRsb7JpjnU8M3/CS2CVOlnIIY8oYjBH
dBVSkk6Vserd8uMKJ+7WrbSPORUShRDXCpoahxcR2MRAb479RfmR6Q4cfru0eC37bdW0SH0B7LEZ
TnL7QSJot9gn65v7t65sJcwx2ue/4c1AV0kdItoUzo+ogH72nFvlQSeMw+KGg2yDJqXfBvJjW/Ga
ZTLlOpFnx0Cpyit+LvQqMNZbcbnn4f+C5OexIW1Dkh8pomKz+aNDItZ5eYH61bdFyzuZvkMJoND2
5mBPjV8HQGhLkQ5OPMy/oTvTKER1Vks55sYHjU5LZ+QCCLjmL/wxqyouzChlwRiJ6hPLBA7HNJbu
8Rk8WLJxGzYiIVA+Dw+WHNw331+QE6l2SRK7xeGWKvTCcuPOYZuJ7sdH9ACGwwkBIiWfwmeMUeL4
Tux/JfscQ/XKuoBlViS9IcXlbqngG0vGYoowU2e4wVV65UBYK4pv7UU4oIrRE7ZlM98uDDR5bAfC
MajlYAB1DR0adeZSHqoHdU63L9hvT15zOnOCGLJ3uKFI4YuEQJ77fE/Vm0MxQMpuePArBQJsB2U7
Upv7bIxaVOhBb5iIxcjRSS7Xy9evYg1Oa9oKkmoE7GhoGjeSv8/YCGI0Xt8eYjKJzX0T9eYbRhuv
sQ1lPc7jqtVpOjLJciU/xi1wuNj8I3bvka9RHZX5xzhPzCyKtbuSVcEbAjEcvNcaApKXAtHEskU3
lupYXLqR4lJBhlkJIlaHvgl7VtUKB2Z8K373FrBDx+1Y26ev5O6biXl9cw7x8i7gIoEWTidpHgXq
AXVFcrI7vEuIaiaFMSZabRW/3LBp0zkJA+rq3111NuXO92mkXPwJVzNn5e5dhQzD+Shdmrxowdjj
Sbpz1d0wN6o1ncpwt2037SLYREKwoB8WDr6Ls5DeNCgE6FjRmg+PWtXaftzmX7Pud15nnLMw6QWl
Lt3kuxiKK0g9ZPCxONLtoty7nFpNqFHg1BOAg1Pwwqoh8ibhiatOrdi520C2wCRAADxdJmzgqbN8
ynAQq38Hvg24AG1MrSqviYUkFcsMDCBf9s+AzHu1HHrmKiS3xHvXiHYEe4Be4ei9qYL1Zho0ZlPf
CiyYnCVAueZ/lfppihGEtwdoO3N1dyS4YoHb8CnVyZOvQD7NDTNNnmW0vWTFU6TREKfNFDBhRfnq
G+ntM1bSZ+jgYq41RGtDAtupIy+zL5rzFmGk0zjkxfVuBeFMPpdJDhif/jngVyfTZ6mW+x5mP/xT
W5wS7IkZ7bEkIvFIWDmwGn0/nMsPu/BgJN/joppcxTQAIg435zmJtspuuZwM6Dcj6c3JxNMrMJPP
wgqLgBgh/++b5AheDWcsB2UYKRIvUazD4aILtLDgbw+KHeo8Rgsuma23Y+Rrgk60Kb8m631EUxzD
76v2Oa5awuZC9ed0QW0Q3PwEh0YpjMGTxdAUjpmoWbeMaI+NrHVpacpbPmrpBGSdQEW6jH1MqDLD
gUj4HHtoJd3tDc5Rzz4KKvIzxBTgyCzoah7agU0z8VMGo4Ab/AtD264+H8BpCheFRPdIApN0RFwk
PuYN96oTnT4sp2PUr0WzyliYhkDa8ZYyiNVi4bKTBBQiG6EDhHENFmCWY7dtPZMFU/ap7P+Qpt98
4ZAI6BLULKICGhAb7Jg0PcGlyUw8B4zzXLxsaiM8BiVexGHt6mpRXzN4cK+M6o4AbXnFcga+x4E1
7hkMprNaoL5O+5GLB5TDC7/B3517+jKlVS0Mye2YmVf38ztBxpMZ72t9yq9az5PXhGdPdA7S+ciN
dYABJFXG67MDVcjXgx4lE2/yAkcRmjSwd/uwy3y10Tbu6p84+mIhs2XOmRvoo46JkhSgc57GMftz
o+9OCKpo4QKKTQzgyEksiUPmG+v91gR/Rd8PFNoj+LyqBhJVWB+VCEYgEwjymuc4IyGWDSG1/fq8
tIjRVlK0efsj3feZYxOlF+OBQGlfDl1ZITEeueHnzhbxpDO6VhZoTR9lsWMebZ38sD9NJUMtas/g
Q1OxKvwZibu8pq7ecVaHHtBhOKmwR7YPpeDDOPukW1egLs5hZDlUm3l3AW4hjrqjyscvTZnSu5fN
MfAazEFioMoInokItLpAXjPsBtHRyF916GaOU7MUjOARxqkQOKwwJc+Tx/DNK88cSyR4gWmBqf6J
njdmr8NgDJ9AhiuvAjQa308bITxQgbFizHOGkYxRfmZMfpTKiZTLHYFMqcH4duXnoFVrDCanvivC
RDeFFA1iSb1qOyIAwru4HZLqsGvWXtfsShIwRHXIF5nPcRm+pqf9XHVi/bVL+DN8c38AFCFVSRCt
fLsJuDrlqvsP1C2MK1x1S3aB0XHQMFqAebkY9ZB08tFSt9slEjYVfNAeZS+HTdijllLZMR526eJp
5Nn3ge81zDyebcRPUt8rVyOCd+Hv4/Gw6RO6+LJRotq6bKlOrRrQBIM7E8mrQH2If72uCY+oHCvx
jexVNu7NwZUTMI4BT32Cww23Sx9e/wNlJu/3a71pC7ftT5/GTdWEZWwrfZURrrwWwtKuAiGz3FFN
ts2xOsgwQofERMnEPCKJYrlbYFc9gnkbteNa2u+ODb3J6Jw/DdZBWnJDHQY0NE6zvZ2W6LL9N7eE
Yq5nI9vvVb4kMDL0JOOPjrHSiqGSzyJKcs5T2N7xLKFYywQXTKV9+XB628TLmA5CUgwj3MsBQatw
rWaOLr4KkUvfjN20CLjsT5XibNTVld14mFUV2nV2eQI3pTBdEFSFhDPwzzDMFsg1Ec85x4OhltfX
TZH0FoZzBRW0dHzVYwJ+ZhxzvEVBtbDdvi6N7V0KUQL3xah1wd3iSS9sJsVv1FEGe+bQAlrygjZ3
YOa3IyTzkBLFmtSHhVQArYhgZL5jA2uZx1Vt8mkW45Z8zrKn7ghxEWVCw8/6MTFnkoCB5vwfNcnW
+5EfSU4eLvOG7c0ihL2xNQwCCt/yo4OQhct72RrTq+IbVg1IH7ZvrcUzAm0IVy0hnDMV9qGHvHk7
7KtndrmiPukAeK+FzZT/Sz0r0ZF0LJXZBRHSQAi+abTbRPD5rvkeertZRsUC5NBNZG9Y1fwXt9EC
Y4eu1nUFXDs5sFmGtDyhw07S7KHVgCKHNd6LmK6dPnGfFQ1t2GfVv0U0OGZKwOa+AWyfZW9oDHAp
2NbtgsGOtp9P4jNnLsFRQvGIp/3GJ2UkY79O4RDI6hf3lmHYbd0gx5EViW7a3FivAhlTpyyWxXDU
a/NRUu8Fe0ZyR9Ql+72Gda8q2s1PtIZGmCGo7+0oxHYAXG++0TSOfv65xhvJp4ZlC30tFF6ZrM8/
iNN9Uv+mWtVPd/2v+EknWtAWorS37Cl/ayHUX12VAnpCvt9IB3oHGxGMpz+mOm4N/pRn99ZxliKu
bp/5K2skJGFZbidJDxWXmdT8s89I0ANJ7kiTk2SHoK1PiUVOTzFTrHiVa4e3u0LW9M55A5iCBq8e
B/FlOqMMY6+01zYbYi+k8TcYF6rSU8JFdoEr54UbtELIqvgYGzoPnMTRHNIzT3VBRwvxga+jV+/l
bquyPLn2nT4U499qaODo6E2+RQt1AaM+hXuAh8cXURFLMJu87vkIoBvlNvgwb+m2DmJptan90AFs
5uArI9E2NDzOisSQ7R+/bX0JhYeSRhb7bQa2TDOCoUQZYpHnl8AEV+L2PYUWaG/+3CvYGLNyh4IX
HdR+0J/hRdT58hzxk3N79rgN3QW6//BaUR7rWEKVPpwJibxyjtRMNjTtqgti6GMmzkwXec5uUawa
T4J0/fOgIXeXJl+zAf8tqMypEczeUbaYxtaakL5SL0+ejDFqRPsfBjScubyNYTwylm+Ua/UAil3P
o/K+GG5kSPPry8GUd/yx7eTwA7w4uDfCa4ldoLBhfPOsrOyemaohOYBrKsCOvNT9KTLHUH7lmO+S
Fa5D1sL7C4GnZxeEYRJS87F5+r4BcVG/O8qjUa+BF+Zd60+9y3xlteb/Z9dQCYqIFEc6/OL9jdAw
vF5C0GnxN4BZfJZnc3WKX/03S/LnHORWhnKNdPKAsHQ1agYI+Q918JNEqafgYyM8Pc3Zg2psnroj
Vxh4h9OjMaH65RWILif86vcCdsFEruBiU1kOmXWZooAvm1pjSaVVxTAhB77SHW10xoxulO+gkpvb
pZ0IJFV6PaOT5JpodegfjTGbNjlczFzbDlVa4SjWUY5zRMRIlvcVelZaGnXEmhatMVoMhGUuQa7w
o2/LgxF11GmP8hwz6ty2RTsSEVqK7brxPUFNJfWNMo4MGODJ4A8OhhLW4Bm4JuCU+FUmmaczgMLM
3nRH7ky1GZ6SSnlT1SmSZfir16htaryG8Kr2bnapvdnV869YzyTFT4ft7RwrX99+KqBLiOXYFU8W
hNsiJcHmMnpfiwZdWwF55xyREqV4miF3zNarnm2nts8D96lHcyT7IbkPqWw32aeVoirjJq1pumxc
QASU7UbAi0RgR/En2LYsl6Hy0nebF9YmU+8gMawMCyol0MMmwGACYhDQjjKyO0cxzEz0KnijSluk
CMKKGGGl+9MEvug2Mp4vtWhgDsZh9qogIGh6l19zlFBqMLf+00VL1EdlhSvkcX6srb+8is7Kr8Ke
NYPLzOlCLmXdYcJNXmeA2Xkj1Y9xX7TEs/wG+o9GrZ3Wu/5HaHHWb7AGFvesdNw5lxKOsOMSmU1h
ZQ8kGq60/dmQMg03k5+HiUWAa4KRr1ty8QEZK3M1jY6YXokcPBYFku+ZzlLpXrpC23fU55K49mTb
NZ/usbRwye9NDIzeNZcZUW8QrhtDKE2cNZSfpEm/fwCAGCXA5UxILb1UqSzFOxept9DzTKgN8dFh
gKSGxywTFQKeLj12JKXRpsPVW3ne7YOtZRny4amTj9uhl5EdV49XQLqldAleaEWQrolWXQDY0OQ7
qiL1OGRd7nCU8FkrBz+rmgQ+ujvjL6tZTgdeev0Dmrv9F7/j8Auc1csES6m868D5m5PAJOHGFZDc
QZ7wvti18hTbnnlConKeRm90V/XyUmnECb4AzarvqdJQ8kjQUAWJCin78jBo2in0xAAznGMRLBWG
GLrWfoncVsbZ8FRlP5QqTb5wIp6Po/ux4f9OIIXORZk0oQ/KzPZAQUXnk+EYIQg6a9lXIa66O1ch
XMyuRsQ+2+ULbqh1ZX77DUVyeLXVyCp8Ws6LgrTC6aC+HY9wqvuWQspuikZ2DKXzpiwDx++KCxTp
XnxkRDIFDPAF9WsiS7lt4FnFLYYgxvoDUT/3cYrim+ZefKAwoEh+25OTeGA1XkULx/b8bwIfN/LZ
MFJZ0g4aoS0yCBo0AWemDzI7GabogvhAyWHHzn+L7rW2QRTwWbh/BG5rby2mXHNEHv6hREYPRjbn
uMWDsi+tWz6HPYOe+OqxpQFNlN1Xcrce2PQmmo5gPF1BxpoHyGApQCL5I7b+uBZlDsBsgG6G9Bsl
2URVU2DYqlFyyQfVxiD+IykS7sLCmxNiKqRNYuMh0IfXcFhN7x6JzFxnL9J/Mc0HlNLfLDxF7FhE
WC6sb6Bxams5IzpCwHkPopog01CbNVppu3fhkp3tNKV0yI1ZSjaxAWorSXQqkcyPONhP68bc5MwI
fwPwh7ldjUuR/qbDfNatgPWY3EGhFCAMr3bRm1pYYvzNj4gfUtRNS74aCARk2XH6LjkWNbIHH6qk
zoXaxVsvHYaU/BWBNB9t6VVRQad4cBUUojzBp8XK455QliUmuDtL4gi2dZElP9FGXup5OGhaObTE
BoF7cjrqFF2zn6kQYoSGIE3+EoWyxTryOBFttPi+yeydR+8jDKPbuXrOleRDvA1A6OZnNB09UshZ
zZT54dY5AgUL9rRUmYbbDdhw/0xbBucKdGZXEQnmktlhsIlB25VBA+Nc64k+o0s2sLintsPyDVIV
4VOwMxCt4XnRsghHfgxSo72Ng7wbztrLdYrTnFYbliSVY7NzNTWslGACbYipBAhiqQk2rNFk1kK4
KztnusekCpr/cp7oAmZRY1tznHQd6V96AhEdF7FaKdYe1cne5+1CquzTjvb8cBd+7F/l2tUzd6df
l+SbqFfVXpyLsjagQ4f8584Pdyioonz4BYS1lXiC7/PjqMVvgc/loA10M8cZ5FCBBE263DqPyS6J
yyA77VkeJl3bFnCkbxEkrmggtPNhFJXGFO3QrxqqLOlu21C/nDU9UcVZAM1TD41j9I4cF0CpO06l
zeSYvgICFjTnEFYW1W+ljtqcPuAcRbqSTScroPKfG9ceM0sIEnQqjY0tgfohTIMDuk+HIBuC1ePO
DYqF83vA2Gsknv41x/JjackMFNOcZHVK9qcVCT8YWtDVzW0p9wsF8k3Lu6rMhl04Q+X5uZp3YRYj
pcRzB0Tmu95q1X6HyxXOKG4q7dGSCn7OVg50+zM0Mh1ExI2FydZPVL4jhWV63rXiLUhfSovS8g/b
DfHWGEF8SUVP0RMZeI1EziQ3rmKnsMFNgkimM5YLSi/2908KeCAJdR96zbYG50YbwZOVLR1ivHDB
ERlAhLL3nTrtAqOHeVTQ1D4MJb7bcFrhDpxIDZXWQmpFyt+uDGZ7jhy/k0Cj7z2I1UTb3H9c5pz/
2AMI8FmTl6BAljxkfBHJZK1hU/weM+EWaid5Dk1QlTYL6XgGhxcQSyzE7e81Lg6VGhDQd5T+Po85
NYSl25my9d7dRvh2aicdor5XMWVXxXoU5N4Vckes5f5urZXfqc5uyuo6jNT+qB1BmG7YIF4HcMnU
tVGPigRj/bTKG3pw+iXLJQp1u9j6g+s06R6w4Qf+aSoaWaC73jKCOv1PADMVcULHiY4l9qsYKRKE
rpo1W/xByYpNHIMuFC3sFm1EJOjTsrtbisFoEDw76aaBgc9oKORDexPyN5HJzaXBEnlODfxDmY/f
I/gwnDfCM+MroK2L6IgCo1pcpUdNkpIDMPU61DFjdCBXIFjcs5Y5Vk6AUGe3LLqY9VrIfGUsQdKB
zJIzAWQ3S3lX+YGv5DS1emKBw2glkozQXwlSwnhsU/FThIFBbblgTynmJcC/4CT10/QKerNLWO4x
Jw/w7ENjNzF8M4fRERD967CB2ZC6O7dsKx01aaTFenVGKETS0JgXrS4WLC0bG97tQAGOj1NJtML1
QPtQezy7DMgdDKR0u3C8qxUAn+is1qq5mBXCacfAwZnrK4MUACeE86ytWuwaN7PwQcyLlw3TmpAM
AfKJC67CjgKfppTlYFTVm5KJfwKvF9MYwp1dEh9Szwl9rUsL7vnn7m30HgxlqZVeoo3Ku4qI91U3
KgXXlBz8TfFe8I1CoQCDPGSxvlAf0IRTKoBK8WQeR1s4/MKcYCjrWrbs9pc2Xe6JTn0+L+DFoaIX
lEE8YbtczWkJHci+/TqHC4cqId6r8Jm1/RJMeqlf3U4ngACcfnitCCxJKARJBhmNolZIv4Dk6Ja+
tOiUlITM3PWD3mEqFiyH/MXn7w5E06u2qZ6RtcYGjBeM12x/By5U/9gCTijXt+0huSN2KluMoXfh
Mf/hzU1m62xcqjhI6LKQp8JatUP77OcosXdx8P+lufiaWmpjwQxNPjHvS6eNk2zzvxfU431otb1F
3OVRAoekeH1cdmrcA2EYGtIgW4dKN7RzFcWrjdVuSIiIg2hdQXwo9j9QjO8To+4a77xgYEavAnXX
wPxp4NJzIdv0xO3a7fjSjRjVWjxhB+HWU9VwhkjhaC0JG3q1kT+qXI4DVzrbbZy31vngx5gpqFo+
CpjO8XjTxdivIQkOFceMXd8K9Wu1Ey6TjbBXJGVAcup995VzU3Q0NG2kPONn3/d73Pr12CjKYjDh
VtBp0gWqmWuY3HvIAi73r129v1WTgo3J3rmAOQg1KORJ8+1JR7t66pVFzFw82CLBWcqYETfKmWLJ
xVaoSdMfvtF4hbwhE6P+ICeXT1AT/hCwGoZ41KyB+bl8j7K15WMSc1T+/gGsVKyUALNkMcj4DRMS
dcQXV20rFEZ3K1TNCnxR3IXwiY0BW9dvSnI5gUmqFJH0DQbUi6crQ1D4jQUgcYi1MojSQs0jvEiS
3Ch0G9NpUatZ68USFEpo/Bv/MFnRJaAzbpN2c27nFoSlN9QUs7bXR2bZTbjQQ2w7et0JMIeMHZ6F
0Vf85XhUIzZE6LJ8Q2mPdDCY9CIYLCzIPvUyBLfvRYhjUReVDvW7KKrNdUO3rEeyxyAzKcXNj50g
gt6y4IlwJ5F1ga1lOzM1aHE2TImrJgf7yzwvcDCtBpTYX7V7ZIoQe0p+scqnBhMGmdkwa/J7H2eR
dnutkQyGI91xipRiJ2TosAUu7akvbImV2hEGe5Ca4dGxL5Y9++AjrTxSUGJbf+xPBlc5WLvXEtXf
BLp3TMhKpKsj1z5GGCPB8pG2LWYQ1gWmeuvqBA6BTfqocPY3tCxKET4tNj9Y39tRTdZDK7i4yES+
aHBMIKKOAkfFN0PEpTE1KWTj/EaVRyrLEksLBWluwX2Ee1U4j5Q/Ltnsmzq6gGTnrYa2jpxUvBg3
9E8zLgqJh6wStcL1ADL2FsT1w9xB63pJpFloywBcR+2PxlF+FEKvnK9fnh45zMDmnlYGHKR6hQme
j3poxckMVR9YJdCyQyYTuzOnvE6JQN65Pcl0UHVMVFcmrGu8hhwVjB+ZQZB+AtWCwAjc/ur+wsik
g5kOUchSigczOdqYdjUdOixTVmT9sDd+CoKe2DC5y0bLDdNs+ITjcl3ZAXQk7ZSoBG6Gs/TAhVZE
8J/MHopS7zg9fwJq5RGrt5hjDdI9vs1xpISXJ8O72Cr2VfH6TBWJNhjAzhm3NENDqfLYa2rJKVYa
+n3vg9fp8jsFM2wFLh6j+/MV+tSqXClfisJTORCIqVytpoScd6moxhIYlt2HLmZDoYzc2Zr3Eb1l
5RNlzE3HqNSLuumDBJ0UoRvK2u51NxRDmIz17dBi77ZZ3QwQiODFihQdqAyLzybjbrHAjmn3XUIA
D9Tjkny24cxnp/q8HIsCAJpjac7tb7IJ8xRNoMv1wO6SZGamgcw3F5dzqdunL5zwZLgWLeIVWq2A
joZGdY22jqI0qEc43BlcjuNqxfJanVEdhpZyz9vRK3qOFf5nzDp1r7Tb0o7D7SpbDRY2pEfp5CcG
Oeqwj54I5vrOAgBxmWMoas/0DN3iB9QUHJaFA522F9lFgvu7LlRsGd+/eL9m2sj4VC/ROnoK/YEM
SjT1Y45E9qysnFawhh+iyWKGiZMtbwqBsjTWPm06iACDD8tM2yn/YAbqYO8HXbyHyWt4pBU7qYeC
zoxKzqr/UaqoK/ciE7Qsv1+71m6qsBwj6uS9mV5ilHATE1ejHuun4rvJd3HF5SaedMvpoVMYQ0WC
ELZVyotsCfRJygveTvu34fOfgqXZyTnN0lZ/0p2dBPiMgP3/SzPL0IkHcMHI1LAH3XM+/jcK/pCN
V/cpETMoua5WdyLaUMlL8NiCrO2+QG3htRQUNf0GwSEhBVclEMCVQVdDgT5AHwDMax0nmFg3HsuK
W8Owj0hH31JwX+FibveTI8SUb3BoE8+AN+b29YJuMlcanG6nJaPbfVzeJdPCDwth/u8iSq3wml7d
UZOuVdQJra/2djSKLaHF2wSOMyjSKydPA4ajqyfIaxa8B7rKzlHuHThh4EQnc5WWZkM/nrU9hh7Z
juWDdgW0DSrV8NTGGIyibiaygxrUkw4gbkufhlHernA0T4G6cSd70lEMnmYjH79WVSvpuj/4Jxuq
aeDHolHLaIHzzKos+88lUKNKFsiwlak5AOt65bQ87iJCi1rMzYwH8RCsliltusus86GaEnhYWG3Q
FIWNTOvKwwOTH0/vvs7ilxMtHuUzk5brhrnuqaYlMf9vbqigRz8IVKpiVdp1ZwO2LYorPWLGbB70
6FPi2fL8RotS0DM1iJe7mu9GQQvukBNuGduxXiTScDTTwPsRJFIny58MlQ2bI2O6TV3U73rsSLLC
rx5x0+fL+77VLHmVnUXX9fN4iNBImJE2H33Md9vnWhsrnWSknmJe2ZLjw1qbZvtQqzpXAGrIRKh+
BwMZ2twxgm0dLt/skglcFKw5JkOvxMC8qWBClS+22qPq3EWIPv1ZVxypnSJs/B94H4in8LvNIJFv
5Z3PZTERYejM2M6dbdja+mD5eLm4zNU/nHlNxZD76Ux5mlOVLuQcANxn2ItQTuEO7A+/x2n52Nzi
TaRdguKnQ8NmQ2/f16YW+yaiD5/voC6B4OtDUIjZkROk63McMljYlHZUHav5OzcMbUefitjM8+V0
eA9B2urD18qAwxlZAXbNYtpRkTbe15OMkIU0RmxgjwOe27Ph4gLY3QGcCs6vr7qwUQ3wQH8zVLro
i5cVwLsSzEfYYABzpP3MRLKP8yNP+qq06ESrnGQQHm4rZ8GErGk630JwjGw1KI+jjjolhTagzTUS
xZ+/Qp92XIsH++N+m001Qd02fLpFDhhsBIpQ9bOROLNAlqfA8wOfQUrh4tEBmMIxn3y4T9YU19Ba
jDezwy9RhUoM6fRC95GQ30+/U1bRY+fCAyXu9UaW0EuJkKctBz2y3E/OFSpGlTl6y3gGzGSzn95S
sh26t5JzExZjSJuAYIX/pwZSnZP88YjEU3c3Pp6Qx+Ptg517VyD9vORfuiT3F9F5OGKdAGfAg/mM
W88Aza3hapKWLMtgOX/kjqY0BaduZOeG1rognILXZvqaPM5lOOAKqq5ahXys9aaYN7gxuQ22ApMK
/r7Z/WEIdLn3+K0Erd1v5VT9yAMZQEOMbRIVf/0CjcO95uJtNFXz0tzkMXMKDEi3bPRuyrwKgUdo
U8S3xanuJz0W9IvcK76YipsXyrb3fTAmJXQtUhpmxOcQTIEzjTXx+gJqd8qlAWvDE3Z9FnZw4zyB
blTGBlSeEdedndSi0eoiGo8yJnKC2nOTKAVa2ENXIplzsvNmpsw7yfvlZ9M1YqumxyX5hTZrK9UJ
CHZefn8gErwijr1uZMeWe9clxWKFHR1vTSkZ8/AyjHyAJKIXSWPdIwPx+5ioSpCXRDX/9VYEM56x
WUALQhb6k8qaEdFwKUoTrbs7N+UwM2xACjIrTiPUudj+BWBt11HUsLtf0K1SYQ4MHNEFD9JkH0eP
A5oXVe4zmE0j9UKIKyw/y8zLLXsXl4WwjtpJdpZUoXIwjvukOWGERZdxnFWWIowXknbYkXBVZk1K
yxbjxeVJyVMNzWmf/sq5Ue7nr0m4v88lY4i6nYxWnj5//hHXYbSmy/UhzLHHt/CxmYjnjJ0DuMr9
JUbaO1B/3MjU1hhlS8LamKEZpaToqyKfsoyEu0h6lFPTmrivnY6TwibqlW8zNgLu0UjpWLNgK4kv
SzmZJ1w2Vp4Y6hjCc6lSrC0D3B+ly+8MZsRqIMg68LukQUz1h1fUZ2BZIavOwf/QtkLvjlgwp+Sz
Ys5DWFwzZ0MB26pj3n4OfGC8ZtXQMQhvSsVWSzPQdodLhd1rLtjLbf0yo43vxd3/R/WM7VKiDzJ8
ZTlP+Uc4iNcBJkEwWeK0Zko5TRaQk+VRjcppP0wsixi/9zIACZOMmS8NZpBvIP4sVQTGD7ZfRZTr
x6xQ5GosayjMoQ7MZZQcGalKJCEa1mNfr6rmmWKAQOu5+eP1Vin0JL7idE6L6nTdeY08cuXxSvg8
QTlwUJ0E2bT02cVbchwvF6Agvv9JPwTyLEiB87vfQ/ZcmN1UhMOJebzZquG9odoo6U13H7oNcZED
8DaRTFWMPllzskTiPKTt1ZUqUtlIV8v0gLVFNA23yxgfoCvHExP8zeObQRKOoWibMNTD/D4f+Hgp
P+LvQsRH2378jbKjyCtZg3Zgip5MKvDMsOLzbYrVlpZBOK2rksj9XTDz5Xem/4oguxL5reNTkw+W
Xzqg7UMq6YmkwthYuwFfnJpT+2m2mk9BPFonX6KKvg57KptxOos0H6As2taHNC1gsLZPbh56vnRh
YuYqOJPw7QyEbVjPGFD61qGMA+CpgxTqLC+qY5jrloAPPDRYlzl/3k0Dg8gCs4jZfB6HEJIasTZO
LgpuDeGXELdgzYWZsnLm6V6wPXiceJUyrz5v2x1hbaZ+QzLL2eXUKuFeFPXGoIvtUZze4uz7KnbH
l8wsADwmAYqL7WhYEyUYE+7Dv8UaKdAEoMCFTxFiXUbyaZh80nRBou1ecakc3gk/9rOhaDBLCoVf
propjV0HoyjkWRn/FgY7h3e7dVakGA9hvWu7fLdsBcLSm+aGLBNNDTfA53EmRvt+Z70BPBmml1nV
dg353UcKEew9IG69bnwrYBzI7NstU54N5c19y+PMHKooOUTnLZIjacV6veeF6YqHhjeHbuhx4Shl
3C0s6KT+Jgod/LUoPIOMCLvsZ/qZRITFzfoxVVNECPI2nfJvXw5DR7avJwWF9VSv0my9Xtt4VZpX
mzFaf6A2hBCLnPUwobKOg0WvDmLjR/IHwJI3fHcdRwb7KdVfZYGPG1inwCb5K3f9EfJWux2xcGn8
4zVaUCi6hlLdigLXR4vtchTR4CweaI5CwW0BuoOxmui6hcmlOYq8zZrRhJDAiVIyLNSzeXjr+Ma+
VQE0HuX4H1IdcE3bWlsZYpkEKXzkAmibHH4TCMcyXlJqVtIKBbD0lUHqcg+HXjQM8bgWwajhmW8i
7TfIEniUHsimpW4cgm0Q7NwAaXs8uUO+vxOQPc48NkyBpPEpD/f+TlpEOtDn2a5KKIQ6Arzbh5Ba
+H4yLp47YTo6UlpFHU1dB/wdiI44kJvHdhgw9YQiIgACx/JH+TmBdC10F1BLYLFvmqhuCX8ubV84
DRiuZk3RhQ3HPXcJhXM1xMVmXYNNxnSm09giOLVatNSdSCVazaArE1BR2fPBnMFDNCjRlSs7zRty
W1iXfBxOqzqzi0r6e2vYabbqkElVfZyq/oVPE8E5V49VkZi1FMWvqWVWkyX3pMfr5r/mjrt0Zeu1
o/c8NF2AXXbOjEaPYhVy+ENBMzSypGXEbHBZiw/dyQ8hS85ND0l3B696aIDkxzvmbNCiJyRBFqjJ
aIXhC4BzH5//306/t7y8xEKesHMGNg1Eccaz90luv4v1cPx5oogTHLoEo4+Yres6zvxY70U1wygA
P0WIa+wiy6f0eycyWmyuT2m/1Jq/1nX4vH0E8FPw8qr464WuPQe86+bS+aC+b4XkJ3SIoXJtjnVF
utAqx5rnwZQ9zGuxL4gHggBWhF05Man5DPJIrdW0kd+/EJxUIRRZPgY0w/G2xwdHoQ7ngOf/w4fC
T8KDQ+Q2klDzMWsLJTZ9DIZZRjxeCv7blT7MytvzrLmR0s1x70ct13Pgwoncj13Z879HbiWOb36s
oaA/GUTpQvtRZS0nwKoP3KiLOxvs3n3tM2rrVtfzM/8kF1ojfPSVbR0iax8XMVFr13b3eXL+N+4D
1R7BroeOsJHM4qFCEFS6xAN4bkKxWTClWZvhaaA5dvuRN7BSibobSnrEtfaF2lg22VVRhNJZl99J
ovPR3BoIb5CcPBoR7aUKKiqEXuUPEeESC3jvjErTdwdqs+wHnrzEIAVRK9oSi4K4OBuwr0TYukkx
pRcEaq4/8Rv5qdzFa4765+sB5yZ0BRRSNzOdLXngCw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
