// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Apr 14 23:10:52 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_addsub_sim_netlist.v
// Design      : c_addsub
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_addsub,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    B,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire CLK;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "15" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000" *) 
  (* C_B_WIDTH = "15" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_OUT_WIDTH = "15" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2656)
`pragma protect data_block
YOf71h1ONImRHfIt2QmpevJSjNbVcz+66m7jvgkIm8schJ1B23mMap8diC76SagcyzKWXu40Nq9M
H94TByazhJkjewCIohr/5Y0bMe5OTE0XBYLdDgVCaRrt5msq6JOo3MlxaA1cJ4Z/j+yUeeLOLRNx
UsAsF1DpTI+9M74FnnZ8g4uOxqQotGOIww+FHB3kLQSXGDQCLAFFAaCG7TmegdJwQW1hRj/DTTkQ
OogXwMn1iANMZUjeWRCm6Ro/ASDet32bE0k3CIZmYfblCqZ9GeOexgVB2BV+P60E0t2lpfzUritD
XBWi8xAslTMVnkbbogKB5Lx3MYXwoYglmTzySZWhvKkx09Erv4vf+6A+JZVrWgxiQx/vb6S5KMOD
a6Zl31k6teEfGDWxsLfpvjQOP/1ynVdwAnfNn9GR1JNDgZxBe0CYbSvTYbkUiuTLWkf6ZZnyWe82
RKjP/Yem+BZmgniFMuWRldQxt3rxEE50E7KCtHGU7zNNHc09r8x+JzK6SD9rCD0KtXX1jbhKriji
kGu1DlVZYmJ8IukwXId/ZRqeQNPSwPQszejYEYDRJs9B4FH3dpWwKcIf3sGgAm24ybCWFXU9HSNj
Ri65YL91ESf3ZsXvfzDkXTqImta4+uwubiVndpfnBMMUdqdAh2NDtTtg38UYDl7of33bAFxUeTFW
5cPVSKeNk3gh4cJ7WqHqbPxZc/4HdfrXmsbncm7Y51ULkY6/JPbPNdX3SpZaun5u9n4R5awIBjIo
sbIwsEKaO0bcCjUQmDVtx8NrHw0ESQmfbxgERp0il1xd43My1UDc1GDU4NpAWmDl+Dcowor1h1lP
KAN+LI1Gk8i2sBHcsKbi31zmANYM35dDwmCJS39rVSRfe47jWStPxvOrTL8q+6C3AaJIoakQoOYM
sTmndmtMusqb49H5wn01TceoGUxBONPI4HbhVDbO4bvTeMm6bY8I7cXAh3AuFJPlQYG/Q5NvJX8E
BUYMzDHbnj1HVmlBUwTwXOkGO0+5hs8S3lB6cY1c8Ap/Jr4/06DYzGZtLahRjhBJYGppkzOz34Ou
KJf4DQdnlR2Q9QM0ZHc7ZiuJ5sxfwoYy0dUQ9jfai9z7Qh2TsuOz6bV4MoiI1XNrO/rCbeKhgW7A
4f/vXNEC0b5oYzyDJj8Oo5IHP0V3HMMahpAZe7/PB0is5LiO9XFADYlffVGzZgOwxEsx8q4+VeVb
hb0JgxHm9WJDbP1dhuoOwczNGZfLhu6MTWD2OkpyGmbwIlTn6A0tyRoiT5CnIaZdHfwDS/ZAKvWH
TzG9nKY7GsRGH/6VZbhyGzB66eV2kwts9a6B+riu6dBUtxcB1MDJi8fEG8bp5/VYbjmjnO8Z/HoG
rjf4dY2C97EcGBYakO83h+B0M2kZV9Wi/9BYsJc+5SQh6Ow9MxTfycXb9ckRKwEx8bCfRl432xk3
0Az5DSa3j1IHQnbHH0sqa47LypDN9mKqdHVwnWMqngNN7DsnrdzcufJmbyGFsnNPNNfQGxdb+O4w
z3rdsyOEiVqv9eN9qk/6DXqs3QRsKIy0eYxLwvbOdul4ick/wSjRfjzMRgDzkqhGr1ZNzWK5W6Pp
RBTcFvkNCmMSCGjQKebXj8XzSEZ9JQzFRNVN8TvWgu2SpxEX+e+Av9TzHQdn6MPou8d7k/muM4HY
+GaqGPQSHUpn2txH30Xhbio5Of2bCy1JRUH3Fc2vuCQXzvMHy5x+D/8iWh9mryFVReI8nV1fT8zS
/rsxjMhfBk/zIUTAh9LfDiWyNGBE9JALlf7lPBcpgCjTilaoUNP05JHI6S0sijy39Jh22dL2NiiU
7FDmxBhGaEM6PtLTpu3fT30wIhfxcsh7izb8Gm/iD3HAb88PAzsqsnbPVDRYU+8WBl7ImISa4qxo
YayVXg5u3bxI91VkJgCyWXssaMoX5+v227tAY06qk3Hvhcl7pgdaq7+EVQyA6ijDeSJH3TFxorEl
DTyqI6GsNeQ/TQDnuIbNMiQ09E0D4eJ0ggRhNe5aIVdyQeWvuHgT2yHFk9fMqLrUN9SQ+CG86Jtp
k7HuDGSbc0a+WmHo2eeFpIyEx5jMcKCMRf763YnO0pVX5RgsAHUVodZ57pOgrUo3FX3hXzb+3FPT
LnNukm4HHnsKAmdLb5I7eGc9WLQaBD5pSF9j2Hm3MBo/SE0Kxhdw7PbfumQTTFr15rYMS6KGFL49
j+OYvbUzQN9YYM+8dKCxCwWbmtjg83atWlo4lUd+qo7+G7k9PByT7RIbkskeEM4lNlAr2hTx5Ra/
eKS87Yu8+LSzFlX/e9usW8ZaqpO+A6lA3ro0d7KMRxBnAKezlxDhLpDLtax/p1y6qOjSymT6hgVL
hKPd2e8qhwmMiw2izl7ZG9VtL2zp7p4gUfe+B2tD47Aez527/+IWvODRqWaGk5RLHMjKBN3JrJI2
ulCkxiPjxMYjkSny/6n0lVK2q0WVx8diIYPZpsb+xXOdhcFKDHya42WPSe0TGp/r8TuTK+ea2Ucm
V2LcNEIOO3pYR0CFIoy8g8wqOfpSeHho9ku6uzm51gPy28RFwGJjDHGk/hWYl8EbSyoaoFqQFAVi
HrUd9JijKWiFDhMygvxvzHd4ysY+i29Bps7h8zs1XsMRdAtG9yOvMSlTRS0Dpcz3rER92XGh8Gm/
hUlJMWW8hMC98e1LoCFMhfAA6hv4jy4/R+zS479FEnv6chG87PQrG/wKmwU11VP7ACl8YMp+hwkF
fuuGm9W2a/rcj9x2AYxrLILPV1WwSkxIbEOwFEZ242xGw+Dsr5tAHG7sGhTIRQEp2CxfjxTzi7q6
0mnRJEpDx/6Mj5iJ+Yfeb5cr1sd85HRLCbuiTAzFM9hZaL+aC+NToBhv5cHVBEFfxq8zusRqGoez
AA0Wwmm7rTH7m7M8z/27zl8vZOglRftTD/jKymvLZdSK3kUKuca35eoM2LGx9lSam7xyiObfDT/j
6FLhWrDKph//OOSvTJImJs5FvBYTiS6f/YE6iNbo/2EC7SwcdBzPJniILn0osbSgq9Xoxb03xQQ8
FvCkb4VWmQhca0I+YXWm/KTrT/LO9ZwjnBZmunLatHnO3hSLg0PO5g0O9/YpoWUbfjPcjyC4qHcl
JPEY9mJxheTbSGpGe8Y0j3UXh+l6ZGAaK5Zob0/acwIknxXLPpTq7ooxm4+lgORNhhl1IsgBLZo9
71b/4wghO4Tual3bMcnC6U1KGgyJ4xJmYgzLHzb96a8T6PxnuuCneCioD0yWBrQJtrPVYsLsKg1M
XkBH+vabuYwCBv19JS9TMFDhYYSC3TPutdY1WW+Lmw4lfr+CzeXNHTuZeeu15w+UP88fqdA3RRIG
NqH+H+RFce2ZjtIFzBC6VqlWxYPawPYwKqS4QKl6w6GIVoP028jN88kcTo88dDGl88sTA8WY02eE
v96lUbJXeYNs//4PMxELzpRxAAybefWfg7FTdIG69Vd558qGLTOIDOxRFUzxRt/WNX0WONf7hSVo
YSHQEOH6V+3+/dbINoXchEDsiwHlDsP8qd3FnskldK91kw==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fOu+T25bYyLXcRq7MrOO8H6cUS/fCrV3vPwdENNzu9POWXJHjMRs2G4wnT0dr0mJyXYoyZM3vyWP
pX0HSuyoZuhNwyJY91XLsjzta8Gq4G5WtedLtBP0AczgEU+YMFXEkPS2yn8XyAuXnvAC9ajLH8Ex
34L+n3jgjd0NX9WC9c3zGfvnLXvOeF5j2IgXVnJ1nZbqa0RBXku1kzOIBsJhivwiPiwv6AjaGp0M
ACgesSWVSNoqLP2s+B1oXJCpwLofXCVqj4ACQzhdmH33e/LhjurCg3Wiw3ByFtFhTXNpgMJ7Y1cc
hcBqhHt3D5IKMhjGmxdKaSBA6/4aGAh0FOYRgg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
nNoYV/qWbTdSyG1imSnsxWt3UlF8cow45JW65wtNJivLsNCfnr7Iy9O5nLgibDpatA/RV3OMBEUr
jJQcGMlUeHllqhBQum2uzb/cvO7NSNV8BVfH0eTzhOliSZSJ7T11/Q1M2UNgS+ZviKjo86yNAbWI
mCPWQz74VKcP/R6VEU+bifa2GnWHebPkSXSoBZCoILITx+XfsOqVlOXPLxd1g7aR1FL44UYxhVeS
4j1mO8BAKW73TDNp7JqECNw5w+F0LBdP3dzM1zc5JRIAely3CSyLyjEJrCqZx3gCHWjHJWqYsXFu
hxDCcPMeAikaTRV8gO9IqnIwne2owsH/v7K/xQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12864)
`pragma protect data_block
YOf71h1ONImRHfIt2QmpevJSjNbVcz+66m7jvgkIm8schJ1B23mMap8diC76SagcyzKWXu40Nq9M
H94TByazhJkjewCIohr/5Y0bMe5OTE0XBYLdDgVCaRrt5msq6JOo3MlxaA1cJ4Z/j+yUeeLOLRNx
UsAsF1DpTI+9M74FnnZ8g4uOxqQotGOIww+FHB3kLQSXGDQCLAFFAaCG7TmegdJwQW1hRj/DTTkQ
OogXwMn1iANMZUjeWRCm6Ro/ASDet32bE0k3CIZmYfblCqZ9GeOexgVB2BV+P60E0t2lpfzUritD
XBWi8xAslTMVnkbbogKB5Lx3MYXwoYglmTzySZWhvKkx09Erv4vf+6A+JZVrWgxiQx/vb6S5KMOD
a6Zl31k6teEfGDWxsLfpvjQOP/1ynVdwAnfNn9GR1JNDgZxBe0CYbSvTYbkUiuTLWkf6ZZnyWe82
RKjP/Yem+BZmgniFMuWRldQxt3rxEE50E7KCtHGU7zNNHc09r8x+JzK6SD9rCD0KtXX1jbhKriji
kGu1DlVZYmJ8IukwXId/ZRqeQNPSwPQszejYEYDRJs9B4FH3dpWwKcIf3sGgAm24ybCWFXU9HSNj
Ri65YL91ESf3ZsXvfzDkXTqImta4+uwubiVndpfnBMMUdqdAh2NDtTtg38UYDl7of33bAFxUeTFW
5cPVSKeNk3gh4cJ7WqHqbPxZc/4HdfrXmsbncm7Y51ULkY6/JPbPNdX3SpZaun5u9n4R5awIBjIo
sbIwsEKaO0bcCjUQmDVtx8NrHw0ESQmfbxgERp0il1xd43My1UDc1GDU4NpAWmDl+Dcowor1h1lP
KAN+LI1Gk8i2sBHcsKbi31zmANYM35dDwmCJS39rVSRfe47jWStPxvOrTL8q+6C3AaJIoakQoOYM
sTmndmIjlRDWTEEG9NEbrI6VwbEARlHkwELMkI0PaaPMybiWHM216b2hplzVtDhx9PF78lOcp0xl
7O9ZKEww7ltqgisT2EX45H2yynyC7YZWUWRxjQhw6cbk86bBaPAsyo3dhyg2MNdICSLEGjrww9rh
bAvy/57yq7WgNWm29l8CJtUhRzM3rCv955RqCNmR9UKL57mELWf036ilY40MU6ya6WrRA3mJ/MJT
9hmK1u07NIIeTkUem8GEYLMdGcUTLucj1tR7gjkme7zRLtece6eW5KCxE0UZ/JoCGHNrJZv4ldIT
Bom9B6KuTgsFbjMZsdbxd/DoJoqvm77iB07mW2+igGCOjlNd1Yt//8K4TtHXzRu9EGDMvJFXNsuf
gGTE0lBaFYt7YeuAI+g0LzGabGGQyJTvaDzIM1Fta3+WK1a1d8ezCCsPvmbeTcfwVHYqLlCHMZVS
TBvxSJiLeJdMbKrMO7Qacuc7maJF/rSCCkQ0ERuca0bHAfh82vcv7eu3og7zseLBN3zz/HkDSxbW
FRzMBU44vaa3X7oCmajodCaDg+bHx8E63+xashpKfEjmLHosLdjU6g+Emk8t+eXxVeAb5Usz/Okd
CE6ePt5jjQdtgRGIU+T2pKkRS5TMrzIDFJKqCjrSMlUUb1k7zEj7VM36SA1zwyUOvjVGOteo7uET
pYEoZw7NCQGee8YZJlwoCuc77YwIQghhWv1nphRvSSlDuZ+6FETvhXjnZDe20ZQTy0vAv8179QY/
seN1Fp0BrfOO5elFlCxxSY80n6IGswg4fILxop1dBz5CApsy6IZob6TyKT+4CkzgQBxlR0oKSUS7
RbNC8A1+XZMZYw3AYPAb4MqeGwX1/ukYOc8uC3YjwzhHtmkkbt+oo25dp5yIF61/2jPalz5WfCIY
dBkMHPfhIZjQrpQ6wi2DNC4n6OBENZfmfWN5jdLWV5Srg0h4NvLhALUjgyGn1+K/qEnNGdH5SWdw
uv37jWPbl5ivElPHFXkN55QTV2qKZ1S//44TEfiPmg0GVrwiS0M4Sv5kDpnhPGad3xh5YXCak5ed
Bv2IJYPKOAlVf6AoWmNwGgvi3P9kY9w32zsxPzkvLY8+rvs8WWFaOzzWqB5dsLa7u9PxIlVAuDx7
JWVQ0TT/4r/WNMBgn+qQnpny+yzWwD0E9jziLVkpSHtalt8QalrIH+87bJrsl3zl8o0t3MrTG/6p
yFfFSqdL170wbkbPR3Jtd6z9Swkwy1CvFKih5lpHEz/kToMHhCWyla6cJ1fWhp/fyPUSXwSpYcwB
3RbqJe5yJmKjkruPvf4o9+sIP97a0pk3upJq3fhZ8gDmWXOpXDhHYgzA1V7zsK3MerM7Vf/tOlms
/ZODaZ6AjHS41vsAtG5TgyfVui/9lRpxGUf/EYz3fzhIrBxv/NHGpY/smum/CWT/3fyuXn/QhTzr
Aoehdrgs8gVxZ4nUZuCaUbI4aoNcdir/Jwv0H8SgpWdfisnR74/RUZ8j9dAgUuhLlRPCiftLSkWl
CdLO/bj/Yn5JLs4jfdLxlrXcTHU/W3SAf5L4xxoRSLSqnmXmGbRQBvmsUHvsw0KLeCTJfTeldm2D
7EfVe2sg6XrxRgClgvs+VM3nr6b1Dp5goairQQ2U8yxJCnrFW6WYZBYfOjd0u77w4YjImZ2PZFPL
Nl3K/kVwPiPoWiBJPUPKMvizad1goFnpT3OqY+YEUPcuufCo8iAqguJXdQ6Go8dwVPt6vZlbxrxs
jWbrNanlumA5oIlrvGtNXxhJlU7eGEh2/zYf2XsIbQSVaGgmQbDp29XJDt7k6A5P82aMsQFkiWgd
Ev8/kpIfhnFH9aejWOyBxVWoSGpyC3mZZiw2bPGB4vQR0nzWTJk0HtVlvnlCYFDg/NYLE6nNdNJw
aYtaeKg9PR+dqIDgNTCpTi3uLBirtdEgzEdnSa8Eemt2QZv5GLqxLw3eMb3m2MF/ZFMpdfyJ2EG5
VRodVInqG8NyY+HkWR4gCEXjZ+YZ9QSwAyaXXdReo5Xs3DPmkrhgjQHra1enUcZgpYh5GrTjiLEV
UF1P0TI155ncNXFbFPrca+UCIYD/Dde+ZwCQzbB3pT8t5C3VbcJYAwKwAVIjZoGmEL0dGAYtZH2w
LBTGqNLZD12cYaRxcHLXxS076FiCKn/c1Z6j8NBsXP6DkPYmVDxVwnowmyYNC5eR8xzBDDtvAsgX
mSDNbMJQllBkw2qiNvOdM0j365fPq7qAvB1RmgDHSgyEjCqEYbnweDgMIs6KPziDq4YA+mH7rELj
ZhmsobAf94oPkh0flpYkLkcJbEo9828seuiq8Xv+NFFJGZGDY9gGmkUzKlAOBAnZSQl04kcIygxB
sYm/UdoovjcqnhNp+I9a/qwDLZIIeJt+CuqHn/GfV4/oO7c8PTR11JCNEUy3wSqidE+KJ37465x0
aE5Kv9BTOPYECM2RJiciLcI0OnwCqocUpk7DxKpioAwQHhQewRKImra2zrSmB/NTuJT0rXZG2gK0
klQboTM/T+aD+TNcHPy9wQrtZviNnRe8E9ejZJHprGkx0WOv4ZT9TKT/NTOS9FbEXRjVNB9FPvmW
hG+hOmvRuqKGW619e8Mf8vcCFOANOi/z8kA8A3b16vcSZH5jbnJ25dI1tQ0tioNE2nF1enA4JwkJ
RfCAvFOKAIaLVTx2qHhb5jUJQIUrEcdRGNHxFmrpJZ0Lw8jeYQ+ZzdiVafmjOM1yFQKKB9tXII4l
1A4qE7CB0BqXy+OstsCNTdzlgUa6DRv9cQyiawHHU6mpZ6j5oxpqn0XuK3oCLvTM+GicBYAzyiOU
q4/iKGOhgWU1MeJsIPDCMiYOXIXa1C79Js9JnHgJcvvJpIl1zorzu5XpGrghxQntE81OGFHKBDJ/
d1KHIjLwYrAL6OzyzagS3Xpi+NbULzEOZK3nMAZVXe9mMdW3stZA5ya3NqJAy8x88M3vYWqlcHm+
WhXyjbT8WWiTJLjoiWJWqJOnpJK7X1OTjEY01fjvbaUnRGGRq5ADFxSZIhZPkm8XdbrMwTOas7ln
zoRtR7KOjQeS6KdqiU4eKx13xGAlCBT+VyFmFbUYhCLh9V3j5Z3laQ8e2MrNap9ZLwJg8XMvAmNi
Ls7AH8U+nTKn7cc9rd9nzqjkwosOZuFMzLtAj/SusMvo7eekgXiJjVhZL3ux5p+rc5zq0U+4mEqB
ulhbsxM0ImmfyyrtB0qb5eRvkm6J5mBOWg6zIXuHNk2SdQSnAwZyttRPgJxQuSxZOfpX6BhCUx9l
ZFbbwHz4DJdkilBZkGYd0siul9m8QR9BBMfeNXsUei2KrCDn4b9IzqkkGmcRo7lkVdNFCqwM+tdi
7nVP/tSAHtFB5gBwP9kcxHKi1H1k+WS6t8b1Cr4EFTigmFslczFwVVcTYo6zndv4tcLeWdbN+Mk0
NP3WeGbvNBnuLmDRNX5LaYxL/7Zn3m3LoI093qPITkcsWfsGDN6H+j7WHiX2NvGaaFAtyZWvnSvi
/2mQmeWdy2ZFVF6i8kE/QLHBNYdxy9aAWPFOYrgtzp2uIATZ3S/4hBiIENMrj9PaZAe/AunimGNn
W4svwiW0DN87sQ6qCshmBVkGIVRxIK/nmCVv5Bb6Gi3ceCd4D4ApROGdLklwP61B3PHxGXYya73T
BhZce1JKhRLIifHBvbdM3qkQ86//+venZvuPf7pI4HasynF4UoqsfJYmu9OcRgiDOMxxUgerYaBV
zaJjzRhIcL9yOWe2KAuVk7yV2jBsRXsCjyxq/V8dvMy0++qi67CyKSOT6mxsffi2JM/8ABg0y/05
BWsIvsu8c1adpsr1OVK1u9qaqryp/roB4eNVw2jvh+6VFefqEK7avVH+lqnMjx9MEheWWjh5f9ws
dLnwsMRzsDeiXA0jKXlSETXcnWvc0iDIRCT7HnYWOIQpppkc98HGZQust076mBTg3bmm5I0vGTp1
0tbbEYbZiLZwCUdE7BNdIS+gSEcQMynbc/R7OrXXVyw6yvbcW5ETutOJtj4Ep0LLO2usOaC5FlFF
4bRclwofx/5gWdUFGXuSFVTgh0KqxrOggU3M3LHF1GIiYj5Eu70dM/ZhbSThNYO1wRqMSRjoJfRU
Y7Yl0MnqFUZqu2hEUtiO9tJIgPGxu+qztfhupNHbWozK3ZhXxiNCnlzDxAhF4bzg4uyi4FTC/0rq
KC7afFEdT2XZTMn79o1mS5md2Xn5l5h7rRMu4V+ebUDYsUC95Bnw3MgvrsH0i3PQz5uSiTuooRhM
cnqbMwHhCphvG7JQZFcAssgPZONG9AmNZsZiMRtyParUdJHp4iMO+jHQDZKh05xsP4JVvPxJCxWq
Yol0/A/UYBKqltzhQ7u410HuGT7a5E11yjB09CiGIKwqhV0zy+iW3E4rxiMy+LV1ag5+H1HxkIkX
HqUcEvwsXOf2IB3qDAYWBUIkM9/efWBEh+WUzKqMQSfGF3kT3qihIhhzP4W3l4HRFuaKDZhfjcdA
DMBFBF9txzyJ4UJwlBK4Vkh8DQFZEssexQ1Lijsb26N/gwT87grr0VguhnzuxfgRGNWffjx7uzfM
aHR1LOf0F3CqQkPQ+ZExa9/daj8Q066LTESoUFyFbTpGpQRWRwYJ12D5NS23xys1j0UyN9ftW52p
jL02AbVEMBK15BcC2SzIAI42GmMttjrkKIVxoJXx6LER1OcKmsyWI4neKK354SKi9Do9YjUk4Ahk
hzIBpJ2CpPBGNWvLRcj5YyqsXGUzKiQZKMq8KgghITRLq3qMfVTcyA9PL/6F+FrZp8DXK4lhNJMM
Fmn6lnEv8XCELTYnDLGzF3YdsPtHKlXwM0KayiegoA7qrYYlt9rYBicMPY73FazvBkWbs7CYqv/e
lsQDYVrl4cF0Dy0DaFDfHooLZWJiQstFxcwa5XSg8ApcX6ztFrtcOe4k28v4PhmHWk3YxvoJVvaf
D68Ht0umqxwU/Vv18jS+J+MAwMJtNP35A16tZNgH4nN90GXzhMw1j1Vm+n07vNIk9mEMGzsjKhlO
SLzu7Zq18Ve57321R0yQdBrd4DomWhQ/RdHClMPD2A2kRwo/Au46i25sToB7Kpo2EnZ4CFwVuPNx
XFEOBhIID47npqUQj6z943Umh87k5uaFUAvjjkH0+hWg6Z6c3kbxHz+gOF1j5kYmopLNMwcUebq0
UGCbllCw4bDwq91YrqJLhitfku9y1WhrFako3VhYjc1A/ar+r5lxPtX6BPM03QeNOczV9BkjywV7
rYDZ40Ftl7IExVMTAPc+9Jkki0zghjpqCJnw+Dx4TaQ/3CItev+x+YGs8i/mkR/nfvkk5YFp1uED
CwsMk32nJXsEk3RHeiQWOGJCHd4HfX0ffiHjpNlKQvFq725MMYW6X48wyNEKeofUF8VegiSkxa7d
dUIvvglEMaNc8noCx4dXsNWTI43f4SU1+wBFMWkhfS0wB3bAbkMSzJNZeb4WcxEZtiV7jbwX0bTU
tYd4aCsSjIe1Nq3AajDMtQxG+TrwjaPU15vAoOkBvHGFsM051WagnMTO0pnPlEojH+NnEqdjFU/w
nkdilDIh+Spv0hQ/844dByAEuMQ8VIPOgkt4edLjUhYLWJvnWYD04kvEocr2B4ry1/SFnk1DzqVp
Au7QUb14GUVq5z2DvVNc0RkeubqGTpZyGR0QEi8Aa7QBdxI2rDIampdE1mkp1B8ZqWlF3bD9x6Pu
t58pKy86rscvcLAcfFAVgZanSWE7si97g0hNfp5Jey743+gl+23lh8YLqzRzgxXsl2ia7SilIffN
FFqThaB7v8fcITObzjyqejQX3DatC95HeVmGio/76uLcfLjEkIw0Xt61y1F85s2XOsp6B+akL19W
pFgW+i+ofbXHs6LY4CNQeS+nzbcr5l/C3P9GdF3FcjvoAZYAchyqHlRHF0PGYQTODK+SKvTEorVl
E0cz99VvgwUDgM3JW+Rwcgp6Zem95UtHZmZnyIM26YhRuILZaej5gFELFctTcK0jOJoc/Os1V3Kb
gAqKUgcsrGoLZcyFcjfekm0QmzNxLNGxJdwBt4X2acPVlDQ/Ll5Zw6EXcUDrTn+2W1yifxx1eqau
dOCPFM9yR8KwRBRt7EDtkJjkTnWTZv48JjTFLTxhT6jAsbkUi16Mgcr2hWWpp88j+aDi7d90ZBiL
347vGBXzEs+ur8laKhr2Yt5tx9SJYTWwZtwtg1Zea8V5NEVKzeKBDAfYDLUv0BfqlYXV5j0ykdYe
du9N7NK6+HehMJs8sKy3Sq1kjcOiflcBrYJwD8ADlyLn+zaHvLd2zwdk25axl66kPT6hKh9IkrKj
D3cNuWtCXEJsEH6VpyFbyt+x+o5R1tufKClNJv/B7cnUOgc5/BPUzQBxImhPCbRelzGjMRbRrRiM
7WUXSu9JRhL8S3NVFaxBmaGmvOl3nRacHLcNcs3bk/P6EQVk5nQb0AvVqP4sFPR8krHWjt4Uvs3i
vAheF6gJTLs1SCirj/WWv4waMPREFSToA9Na3JSAFniNKTlajCAWVKQkS+SODR1WbKhLLdhDVb25
yU+zbuljt+0qpqIpAYfXqQFSW/uAsROXN4V1N3EBX4IomsP5eODyyTwfpyHsdqm0shPNiiExRNgi
y+OmMzg75cmRfNJZV07HljRDwFI3zLaJosIY5UKVJ6gMQQb+GNHtBC/UYaR4MwqtIB3GvecG+ICg
S6yJt17nSFOvKl9V3B2oQjczBN+Sqr/6ExPFeo4z3VsZtmlMTARTQCTIBkHswZtJDc/2ws6sQeXJ
NMjZlc7To8MU3Q8qOEyIY0bMnnu41C0a+6F+eaSNjF6S7+50RZ7H1PUaMHh8B9056FG0/1GOm1Xg
041WBhSNkHEpU+hQnoW2dL+DreYepf/iZH1lIa8rmGjEVRKk/ajybU69Xyp5Pu21nPG5NYSL95P8
c7zwLPCb3aSdzDKRDbQjB52iz7w1gHvLrLnJ/UtLJlGY0X292Uj92/hrdo3lXiTciBztDI6raT0z
Mu0ruAlWw0KxmtJemxIXYNZ/juNo331KYpz644fRP9U4fyFeKg7Tvch7a7QntEztkIJirzcCt+lm
rQHZ2HFlfdPbZTtov4C9EmPWC1JX1cpLl96ykp/oQ3OdwcZyRJ/5s5tG/xOd6p81qHmT92iSWTzj
nxn7jzvGhetSh0vx/JWJx33hRzcMNefj2/0KA7pH6xXRp13dwzv6CKVv/gV9PF1y9K3iQW2vP0TT
E2uP7UwJkCDdmQlj9ueZmd/4XNgSN5qmCCqIRJ/T2ldt/Xi6Rec5ALKgo2/ueF1KGWvD2wELUELV
h/kOEjGu+Pw1t/fQ13DEL+NxDCbd5nPktpcP22TEff2iK7VIyjGZZuNvCXIIf45tkETuyBWz7M+P
KDblVyKpyHd7Ps1hIdhyYqMsBIIXplEQAjVcr3XZlR2UXQydouB1c49K3jI80DrKj1nGO/uPWSXf
lMN6hPzG5yWD5K9YYOg7bvHNnz7+MXeDkUJQjVnNfgx+afVS0EnhJ1274hkT5la2shJF01LLl11E
HZx57+hrOf68f9zOGvP7zJpiwg3Nk/i5lIHA9nmHIKFV/HkGLEoGFB1l1MuGYreeXT0J6kAo9cE/
yoNeOKUhyTyfjk4CIhLCMVQmhex8ErWumRjpnjuIeBW97ph0g++264nVkCmjCYe3y8OsQad8TAOQ
Ep9KTMk62WLQVI1OQh0dwDumi1e7guL2Me/z0Aabl+bz1ylDxBornKfJ704fF94XLfW+OMYGYA8C
X7GGeC+JqqySRnGxTkoYX9CeSgFQA1DM5fQcXb2i2kCtJc6amafYTvt2Qo7rsck4VkzXXVmKB5gG
iSfjQ/atUAh6I7Zr3bbNxVl8oq2SY1wqq9eTZJJVUSvLmIRt7gXkyvJ2MrmOOcXGUsVdK1vE72PV
TwO/SSIZZod6VTIRJZQ9Z28cb3/nZk61vrf0ef1oaSKSwlrZmxFjnWndpzgQIBQMPHYAwBtvJkJ/
R/EPuUk/jvZGOCii/MW+Fb4irEwqGf6dW8EC4fq2UxT3cSvXqQJBxwfuRlv/OFlsMW+PdHxtNsEr
8eossPp4US+JyJS1p7vnNLw/toFvl83jk7JnTlEu1kqNH/Lbrb5IMrePwR9dTobl+BlrhovDaeJO
aBJo2Kcfy5J6B/ZbX0+0dkqv1ct59tNdWGWMuW6d22J8Orjut11ymy1t9Dz9kB8Ydgxn2WYX6M6z
EXlBYCmo1lcj4y78fsbAD3mGFVr2SgpED3D69YsCv202vyhL/v3SPwv6H00kLt2eRDSKPScjOnxV
8wPNycxG3rAlhbebHDGSkqdfwuEH4dBkUVcQl4mGbT9Ek/WnEEP/kUizjUJJHDManAc7Ua5Mn5AL
ohcjVWZoSfrz3Cideh5iJ6gCZhDw3/KPAbPhVruNPv79bOjSOZ10swytx7/gkkp2F2WZFAZe/XdI
357NJ2718jldUUPoqoRvfI0/racN8gxKshlOt46LuAKDuhS/+N2UsxUtHXiRdpIeG5dNW6IFbfdK
Yp9OjzoRqA74cnU+pVdcK93jV2W46xy0WQBFBZpMXFpp5PubuHAQLLRzzibSpUpvsH5b06wLSIKu
WqH56cyXN4f8iRcaGJFLzudU2a2Yo1F8SuJX5u2QwT/rrEzqHVoLMc3tHfFVDmlNUbA/1j+Qp1Zm
J2JdoNeuQ4fIHjAL8HD82G31mWcOAZsXOVsDAKFeTQ/D+wIF2ajmjyiFX9x3N9NI6WXjUvE+7Eul
5XpSiLw8ah+5cbO1q2ixCoFAd98pyrkjVSvq2lJuuCP+ICtNbJWt5RJFYi46KSe/9fUzGUw0M1j5
XYD8iZPx4rB1PbhQWUdzK7msqHrs8625653OLIy4U3f1cZOQxAnfSt2r73z5PWXTSwp2fuoZcpHh
ElNr3cLW8/o9iaOv5IQCXYYvHBUVbFaYn3f1/J7errChwiVt5Qv3dOxTjqEo0hXXNu1vxXgW88Eu
FJueSbigu9FAqM2zPNtSbwJr0I6BaiX+bcEN7Bwya5Nn2JaQj/ZBQqL9eq2LqE3QgOUel4YQ5Uv3
vH6GhePcq452Tr9JEkdhr3HndMHjqut6fwl2fxc6k73Y4/YOYERKNzivyDBN6dlGHmnK2IAS2+5r
7bucGcPnnkuHlByP6mvOnCxKF7HJIFzMPRJFQWG/EteC97QlkAVzQXmPre7U+LNqnjmbBvzHih9U
WqP5nmIptJ75hkR50i0XVBRBuj+IR3HKO7KqkcUBW/QGN5SMT18J3RsbQkoGKtvXt8w+GseKka5E
z9gvGI4CiVdh5xdVsRYtrYE86jVZDq3Rc2NXLgGeH6x87abG9n4hqeFHhc/JBYTf9pj5x19dazrQ
0C7VAPx9uXV6REqUvGT70vowhwfblXa41cVUSaI7vd1kH1A6ajWeEEjTFiRPNYdatehO95c357B6
k3P+S+ztx7u32ypzRZq08TCx6BmaGnlgFuYM+OYT8uxwsxi3Wpcm0BeENVexJGn6X0MP6NA0Q/ht
wN0P0b308x1gV9RibBhgQLVFzkLGRddOB6QM2EkhY0ouClfCSJuVZhBReJKVnrfUz8BcNgseUxsx
8M7wgRoghFh2Nzngkx0V+g83wTaQdSBN2Xb02B0MVGAiNRkyBpMLWF2aUZWvrKRDyBTxXPtc5dhB
P9JIYNxL+QLFwXt1Xi3X1aj+U0N9U6peLxSKY7QUhq6g6J0YrqwtpLCDL3MF+JucM6mcDpriHVmh
b5w/GKNGIHqu6GV9Efjd+Exz/A2ONgs6bPV8r9WrHZ+vmW4JHG433N3DV8tkKsZ6thGs6cbXTLw+
wkc8rhglbRcnF90Y28DwoJkMtW2b9mvr9zQx8/ztxqPd4XjcAP63a5dycB67XrIzlOACAvEIPcW0
hyirSmAAwrTXuDgf5rXg8C4SYtVGQVXV6/CDiZ75l6EfvGjoeut2G/utif90ZmCViDcHPi2I5sdu
wBE1fRskqQOczCVhT+aVJ+op0w6/2N0dK62jB39Nenblma1Zi+t/BfcPJPmwX7adDV5kRsbYYmf7
qIw3nDBzve2HOgtvJEq3u8xXTJCOp0b6z0YLFxD7d6JrsQuftKgzesOddJuL63/NSL9OMQDtaDp/
MsohwBfARJCEjcgy9isq1jAvF0qfOSvHIExzAnGG1cAsRy1CVdUxQDwR/SY8i9ob1dDsw5Cu6Bx0
ixeabgWdveFcbTOwGzKe4tRLygwKl4+wO1eCI1Goy2IfwJur7nQsdFirO2EBUDN2Vwl4W9gOEjYm
BBYOJdd1gRcaAGsm+9UGvD9C0zztI4HpnEEIt9pd2sTET5xVrgHmiJfEr7qhKRyvxjvpG95SsLkT
Uoj44UHaZ82vz05NqBHPzGofCcF8Xwno5o3rEandjRzRFosL3a+kFtVSc3cCVbmCYN8JFIv3yTrw
E8sEQqNvvmtNk2FTTJrSuhogiO7MRkqTyUua90s0u5aYQT+LSSc+nnbYbLjfOZzKwLLylshT3yjY
Vprz5VHzXtvkWVvd37bjZVoHjdCT3Xt/yX5G9aDxv/WYlm+dnqc98iGP39tdRiuy24W1WZfX8udn
X4AiZguPjaaseuhTpsDkCwuszqGcx5SHXv30k5I7LCBLXIbPQWu4q7ansvwegyZCT0/9+cu5FZ46
hyZmG9d5TCHytJprHvLFSVWuwfPd9NIE2BpaVD3lObhCY+LhIlX8Se4C10f5SAkwQO3T103dnbQO
SweSu0Y2llEooS5daB2B3veTshDvsFVNIbGyO/A+PzanZX0pr7RiJkaaexngRrMs8YBHKb6rWYbK
+9C7bt2MbSedMwrtmNwtDtvQXRs6w1TC7U79UJWjUWPjs8h+aRZwqI8+GMbBr1AqmmiMKksA/s+v
jJyuKAxr5hrHqdhHQffa11Vjv0RZN96eC0e0xW9pLSf7v++olcQzhPmB2yczxlhOeEdUspPGhotS
//EB/CC2/4lrN5e1DZIgE1HIzCYRUCa4gSviXXgk+qCrHaK+1Kq0d3wX2RNe1etDwNsbPethoea0
TkawI1dzmsDqqRnbM09GwuSLoORJe1CCsX4Rh6khgLKt0IUap8ItpOW9EwJ2BahS1DjLpfKJKexo
bpSJ0cG/HKx5lc87EZe9yJ9hPJucN0w5/7nmlqx/63JJxi6zhQcyfeipfUfcuhswumQWNe9RTkGo
G4ME3gGsHcyfvmTgxRvR6kFhoveiUBOkSzbbWGDL82JSqZ/D1nW7OClgpLFSE+vWnOtRSfN7+9mf
6VeNDi4ZKZOEfrFBzHdWNr4wmhXFlSF0gCwDLI3OvtXkJka/wkwzHc5R5zSwGdK5puEvSzkINeHf
Mkf+0MvL/GSUE//Dwvr32LiwsTwaL9umi5oIgkmRiGVQEGh8Za6bv+dpq2WeWseMe8wJXKZYAZPT
RYYFXNV5FkyjrMpbA1ONqDafqLeTNkQY+frfRiyNMP7CVoGRNc2iGe2yG575hieA+XdiRMLU3+LR
H6TpP4VnFRFrfXGMIi6zKcGCzos5nS3vydQdZay7ueF0j9zgkIA2WxOpfB0wt+hqLrQrNU8jmcmy
pMRB5OPcEhuyhXyMKxvLReUQ8JilORCzPbGI41XhHCaX8krGOvi/tdOJ907Gvr/t6AQiFNZNk4lj
r5pi9i/3IFm6wVTgfwqkGuucBszS8s2+SAfG44Txit3QSDSw5KEQjWkeAEmPItBWM9YKhRHT7Pzn
fSQpXETb/iG81i1GN4xdBw5y0wk6L6C75m8P/SeA2jejB/B5zJqFfRhsKVWT9O+TitJGQIgTl4VW
sRsYHp9OdpqwX8+QoS/MDzs5alfi06Oan+IflndNq86iYj0V59nhnnNa3+yJmI6Jx/F8MlpAhnNJ
D1j6cz8HuKEh2JGQBgriOcsnBVyFlsYPVP2iDQL6qZ9MBIhYV7+mKBdMyjdD1RVfTD/7i8hxYsBZ
uCRKIl7mgRsiWQWRQsTEKThsXiXcInF/ry4t2l8lRLUUrm+S/XW45JBBb/jjaTqpUnBD01BGQBka
NK5ikyXVrmh0CgQF8mFskVaf/0z4BkENRWd9Or6maovlWnZhkRwwRFkSqMhUiyIoJ+dg0DCcWqKc
1LWQOnEEunYQoUYXuye3LMcdmZdBvgfdT7mF1h2Ezsk/rtz06fIoeB4uREAsR95z8GSqX7vbEFTC
8pEkVCUm8fVqv05uReELJ6CZZJMz5dVQ9bBa4f7vKYhzSn0SkI8F/pyK/8hcEveJHjkn3mo7D2pa
UGrYs+3yHZ+088fdyZJaKTiucdcScMgMFRd/dbCGotELbb2013TCi4BZdszK79CU71LTPrVR3rdS
ESOiNhYras1ucZR+N86GSZZF/Rq9AnMrxS6UuWqDAc7aVcc4jnlJjQ6zYRAVj4+5yFAZbuU07wtJ
8uZ5cgS4XpBRJM+dRvHyCnZma6OCqeGoHHsHi5YrL0XjPHkgt5Y1WHGd1pqD4uAkuM4CkF0857qd
q8pcHY8OUvpkxAQfQy5dBs72s+CxktJU0Sj6pfax0wKf17JCldIeG1EKquKtIbmKaf7r1ya1czHC
zDTdpYrvYFmHjVBE82hAPSPwzR42itx4vJOrAQaig8CIQDNeU3Z28wv4hYW+daiYxAoJLcRjSwy/
LWRZRy2xE66L+5FcUuKRNvDvoseIMWNrFbTro5QmiV2JMN9mNyEo27h0EnUphWefBygRcIlmYkYf
uzQY/5+LQ4vRXuKhvKcgoi7YlpO85mgVsPGknqdaRqMtnJi7T1TWTWCYoldQeCPCpnX+GMWgusQ5
uKqR1rFXAKr8ZxZs4QcQ7VDcwTD7GP/lQSNqKMB8kHRu4pNJTO5j0aBnFjSL83dCwu6Mf/WnVB4k
DvSzd2MP9QFVZrETwZnR3EVb5yvqQrrkABwGApYMGZTUV3gOLCjWoG4hcmvn2OWri8nB2JkIpDAr
7B1xpzrq4dhJWzOXz8P9ULpMcTKM1Agz5kkiwF408uks9uJNy8yPlPlwUTUPKxbE+N2PT/3xMgFh
wnEiYH9WzLNxiE4lMldkTt/ma7zkoMp9UV1oYsADJIutD2fkPkb1LBiZgx/9U9nQfCfIjZKTPSVd
Y+Za/25YWyLgK3dI7Ufe9c16q2ihBAK2uAH9OiVQj+OL1/2EavDnWLCW32imRj7xi2meeuOT4/I1
lJLZtxlx2MAezefH2qr82XxldQpR2E4fsl6ohNgiHxRQeeCjts6gKbnfDBl2gJCVcafN+Gfa3FxY
JfEA/3nc5UQcdQckpHnMGCdFCLubSv27LxU7852k9bX9uxcD+lWfbxqsLbvCnf2EFxx41dlgmpMc
olBRI/1ihtaAk9Ji68UvfiUdNn/OX2+xpjdbxLblebK9jC2gUFbMNpID9GAcgG32mwSEmzZym3Nq
BymlklYxhZEN69og0GDQp7tRKUS7cTaBoRVIJRgbAVe9nAE3SrUDMr5QrzbAW26aCnVIRa5SUBAx
mYPAjz35hVMNdMrmmp8KBAGWhDeVT44e/rZOJqelgUMI/PsZltdfDI8CDrmUWnb1y3kJ1vz/hBvP
l0BLmSNABVDpY9xNY3s34WaLkHzpHKEfTZfUW1sufiDmBJmRuXsMRKmI37JAJuFumnDBmt7q+wEL
m67gSNrXf/k961uQNmGB7uCcwZdM30bNaFgPfoi+KcGXRbfI8s+wEI574qHmLkH/1MALbK8O7900
B5a0wsjjLKvnzfLGx8mbdrQTl+PsxrLZb81+xMRB6yoa/ZW/WZ1zEDfFI94VE789dsRAY15pt0u2
qGcZJsxopew7CRSGQubzDPpqQKPkFoBs4jB6QIVm2IIrXs1XH6mhFvrjLZLS3ztSO5PNm8qKdXom
N9zpaO0n8ao25ywtq7KPZiZVV4mwcErLkXKAnYCIZSI1GR7zhWQhQSklf9fbAKioDAXahZqJg+GF
GRlrucI7orjgh/4EZ2GcGYS7YO9lMVGIfTAGOQIUcELkaFCJenFQoRwb/h1KoIplo8aba0t7r/kf
I5L2BsbSgjcwNKJcxt0bMQjAjVIAoCDZjqYItGr0ruzwddP3ChvMfACmI1+LNZ5Y6Hg5hWzracv+
u+BKwYL/ZRdIM5bORdnC+u1vcfF2phHW4GCKz9fMABch+G77z2rJZFv9DvzQ0tWjYhIEH5c0Ezys
lNg/imuAwQUWz7P5Eed4bOZ32MLtBe0C3TuBeXe5UfDt1HDvj7+GlaqLxR390P7PmAolegK7QnEm
PvI9GzLbVxl4H8IT0gvTCVcu4Fx/Vv6TMSQt7msOciWDM6nG6nIxdDThPDrlLe98pYe/l2bmWTJ1
kTBSZZG6UK/8x1jLwLwXQuzBGJoBqMxEZ2T6aHPJ8SAHGiFuHCr0hkE4f/A4VDyKcQogz+wsF/7h
/o9P9z4VK5x4Vmnwms5kAYz3/YJQJvozzE6K4vLOdA6C4H4buqnbjUisVb8a5MA6BlHOnShDPfqm
4X0Vl6iuC2/8FRXLV2P2Ghs0yBsOdB0Ij2D2TuQbUKaTzBd+I0jTHWULkcgG5FtcJnkpZGQN8V2M
fP5Vii7v/4k7iYZZT2uUsmPuauOeAcESTsNSs19y6gfX8/QgaQp20hn5w6tNqrbXToqgv5F3+Ebf
nVcuBjqs4fb0w6tyUbLutVHKbNyc5zUakfc8HOfPGvTHmCyqoQKWtQbTWwLkN1aqZOXC2gMEO+dL
b90OzakwdCNqNT99wayP3wg8E35CEFJgk35aXlbHoEZzcj0IDN63v462SZMiWenJ+UCTQZVav+vu
UIX0oUC2A79lbMx/bUwRTKYc4o1zLj0g2BWUSi71Or1Y2rcqjsy5JtxW/04Zv3/AmWSSliiFV+8e
a1yYCbTBrb8a7/UX7XY/SR1UJykywuhXhsbrUJxRYW7oh6MR6qSb8+2mnvJBDlVENWbsqZ/oLQPq
lAlFrCk6Mn3PV/5WZTO6h0NttqbHf9+sh4GX+GcCMGB3z3W4ShQPkZ2yZAX1Ao8KJFgmXVKtP2gr
B1tabPJLuoiaM1voA+8Xy5F6+8ktPe/8FN3aMidfoVuS2tyJx9mDSWYYjPjPT796exanoyUSJJCc
SYyKzdKWa0HYWj8WhMgIX0RVAp5cDqzYiGeg/SKQG31OTsatH06Hy6Pl3Sqo4eAucdpfa4MIqumU
FeNByVIWSrCQ6BKyL2j7ICdaJNdIoZOWaB0pFF41+uRjRaWT7uXTy5wAGwO5BZV0UMYHcGWkL6y0
NRXHLLyYQTMPCjf7c8GbdJUuNwHXnUj+dUS/HhUvLYjYFGQxOMcblnb/cv423TejO682qkIPqeNM
4E5EfFR/mDgqte/VKfEypaXHo3EmxdsT6AMooVljtMEyV9MrKlWBanvdch81AnG+LJ7eV29VReh2
CCcIUg4EWvEDiQ+AnsEBa4jYNC/cqOKe+6JoIJKczJSBeSet96tPjKACAN+SKlL7QbnQxB5dLu22
/5t81j/Ib5qafyfqzvuq4VqehZ1pEEHmkhwiZuJigC1AqEJl5A2IQfoypgUJaZdrYon8XAo2zKiW
MMEjPaXywL6s7Ux6DLfV9R8oB7rJ3j9cc+wWsrPW3MgGWDXQaws/OhOuPT0cuhdTYTVOv4x/lae3
K8W51A+97qRwketniuDewCcbgSsceW/yAmUSf7zC2+oiGjynGIZSPbtmPjnoj0+w5Iq9nIPQAjhZ
HMu2+y8+BymD/in1OUaEAQ8BO9hKAE/IPs9dDLyFJzGn4Vmh1Va+EYPYMR74pf5GY0blJGBhA9Jd
03wKmT7yMelDt59ay3QkMkPGG1ELRDUbhIxGPOjC8VdvbEV6/0iLiaXmVwQ/B3cncOCGObrFVIj0
AAdhvkyHA+qm0BtbaerNmR3au26YD30ln0OzT8wda202ix4NuK+lNcrKrPnwTHZpYJwvy+dIFGsK
aATzS4Y+/WwIa5zaPNiErAhxPolLmrMDs0Ibd1X9rpDv4UAM5zW9lLw6PiSKDFO7fIw8W68VoYQ7
FDeCR5AgVmI4Du1WHVD1JIWezqpL1MzQmoz2Zr1HscuD9xCo2n9o0EYC1CmrTzieUQmsQIu0tqIT
iXDu8RZSS9fDpIJDYh9HHuTI1+W7zN/hsop5FWc5s1YpoYtctW6rYo5/VBCDTdhIAj6icZ29KwNd
4zgi/ICH02+znx7GcY5pjWcuLRPxa8gs2eEIxK0apiNDB+dg4tb9jpQIKIR5L489WXpSvyJiDeMP
sO5FcYeY0OPwiZjC200DGjOL/JFxmEnHIoBeWkTDMurLyrkhyEl7
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
