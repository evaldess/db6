// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Apr  9 17:21:44 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ c_addsub_sim_netlist.v
// Design      : c_addsub
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_addsub,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    B,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "15" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000" *) 
  (* C_B_WIDTH = "15" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "15" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2624)
`pragma protect data_block
5IGRhKxBLSv/Ck+XPtJvHxwMRGTViTn66hnkkzE7VPZLmMwL19v9K85HL7AAcs3T7IAyTBasiLdJ
NcwY5+QIvKaZkph0GGB6yjVulXyWgqXxnFiDIj3Zwl13kZiaZVRpt2beBI8RGsAocTQ5YBJ5J+yE
8UxrFvV8yUYsB6z9Y7CFw8OjocrTrew6u/bYMk0dLXLZ96/l4AVyFitmX3bQlrIX8q4SOTuZdUHz
/w9/aD7q8ABHfajE3HdORcUuehoLeUDoTL9Kx+zF0HASiu2VwQeCE1U6PE1w5MqTSR5TTvWdTHD9
wluKKpbOrfFo7aIKA4+QFhZ5Okz+PKqjsakpawldfS6epMgSq5P0/Ay5OFmJLCrshrSFam0t5v+z
N3dFij7hUepafIpgSMyit3IOO/4sveI1LvcHGIb8VxapVX97lI9B/kZpQgRmb+bWLyNSdS0abEMu
WSPBBLD5A4gMuYRazwgHw+2YxkTB+ixE7KVf3OZE7b9bS2jkqzRz7xsPC1RbEdkF7ASChYasrHyI
9kGyl5no6KZ5aQdECzA1aqLq/MuZfbrJRLf1czXjOPnxXURYGKbwBj2W32/RW0O2QSAL2v01l4Lo
wsESUj5cqQoxnAtE3fb0fDwDpJ/+uvPTi6Co8OziGXJPjpWm9y9eBixNSZFZB/DRKHzLtsvFDwFU
NobHqc4U+/9vTMVFdn9TgUwGWQt48CCWBL1JVbRLcgqmcBigCa13hEKOhCEeI35onn84qzg93c6c
NX/parc6DJU0JGn3Hfn1PJLt/AYmYEfjlHFYF4EByrtVDHC3CCTYugSUEeL3Vl7YZkJ66V8j1bl6
3dx7VLKU3BSnR7gYwVHD6ZlH0/3UUD/t4u4x/sMOsktxtrQMkfeimVF08HHeaDfx1fJXOJXmperf
sFeocQ6i7HEwqwe6EBG/Zm1lWHsjLmjm95FdGHYVWHMD3sO7wgdWCdjFQXsQWijhwkPwb0GFxLEp
1RFfRRaKsTCfMTa2tUcxo7ww7GOuZmgbe+2zw+ALQYB1H1TTubb/DsERAWhFbIj9rhBUMTPUoS6q
VUyIMWUe8jTqcBtvCwltFVijEh2+aSx4I4wh1PYwRIYYZRMw+Z0C9XUQCBSPlg4pW3jSfcuvgrVp
IpNC0nIAXH41uAwD7Tmbk9BkQZttaGpumDEOVZ5SGnJ3sfRZsPnVcejiZmcB1eINMKCaI5wgqHd3
t7RkV2IhRWZBB8mDvw3iYqYTQTlacRoVTAIQExbeh6Uu29TLD4xsN5Hw7vulbACA2zMgIWhC/7nl
01yEoEf/h3wH2GQfD74C8zz3un4z3zTBggohJbrrZMRaE8JawaE0szsZva2MTnvWRonbm23zrqDA
UArsCyqGwFfvollBAFcBeqtJcq1wlPneQHMvb3bfIhM2PMQHSIAEuEfi0wsHn1qFN9aqqjEFRqdK
hwCV28JVGJ8NpV+2ZQ9sMot0abIOQTIM+ZztwdvG1WvBw5Wz6HfKBqfbf2y1nGdTA3EAjQij40ij
4hvCxcpmoKr3CCf3ocCM4N4AMGfwLYPoGXNhgzVq2vJWiYamqKJEmfGBbfjuz+me4troeNHxIvMo
o5Nwe7JRKHR3OpCiwwEqQVJqc22robvYhmFhuSsCvrtaFcEWbVxFf2Nmt1SjGizYHpA17wy5yykw
Aqdlq5Wjb6EwO5hCQbiDP+apyhepHIcm6YB/jqhCvf/Yf0q6dGQ1HP+feO/5yfM/tZuuySZiIRB+
hYWWFmBEeueoRI7XEyuQBPJ3w+vN292ZNn9+KSRz5uOQEgOCVrQjGN2X6NI/VPfHoKdVWGgWj19S
fEBn45dr2twf34nv8eQ630PWZYSMR1pWiThnzvHKqMc8xddOwPAVNrGoX3tHM5u3sRcYTZHG52bw
RZtEw1en1cdTFe5ck6UGO8yXy8S7SyST7KhA4OHvQY/aN/1yYL/j7Aw1LnwNcoUgnvhGcKiiT7y3
fFXoSJZOUIWpz/FKj7bho5+wo6mBIGsfoG15TlxKzKF+VrQQCINcztBeTQ/ZBEuD830FlKO3EiUR
94ZQcb4AWoOrwkpfO5C64DeV0LCS6mP3M+acQOiW/gzzF60WYEfh7IKbYCOWnB/xDQN1QvP+U7Eg
/aJUYkqopAiztkkB4Vf8uswFLk0NsOUSDOmlQXtQaCP6ZYy2DQIiMD2mVImopWWwOIg2RjT+TG59
m0GZZWhW3S2DqIjAoYoICuu2cyhCD5xd6e3O99KTiftxqVzCGpIPjh7CrTlptZ+nctIXbp7fSjx6
q94ariBBcEaokZjCnyDdh8++1GnrtgW3kX4e34qeZwbPw53/eI3zPp1SKLOb+I5Zh1G4ak1Mp2qk
hSjVoH/yO+sKPno0WQzGmpLYr3tSmuqaYM1F1vkYSN4GLBLHEkf7g5ZhKUAzRITW2d/Rq1Y2/YVr
p7JV71wtTzcxFgbEKBxOv518RJR3i5f2HgZgdZix0hWW0J79t5hRE+APAZqY4436tltdaTBY7mX1
j0+VUz/pM+3JnySYyBy4B/T8fJA1ehBF8SXmn76V2g5EL/duMsJx0mL4p6hBEAFmQIkn1yj+3SI0
ZmzMPcPFC2dnAuNjUVpmWLsVB5EvXtGvb8gn/bIpO8Rf0+iWVlG8b/epKQyHWH/TsnUn2Ob9NR6T
oHKLuLsIgIyc8e5ICZdKlygNKNDdrsuT+7OCEtZYwayNhprZRGIeD9vpkeuGKx7gRSy5KARm0/kl
Fvy+sC7Iw+FWqcrAndRriaROg7stynmnxJa9A0/bbTlAceKcQvIqLHyDzu9r+B36r49lLLIM2Own
+4/KpTOkXaC76dFlmU9T2fmE+bhcxg2s6qQmrzlbdfzIy0i3rC4cK9vOO7s9+R0BRj+w7vYgdHGx
UHfcuO05NKtTQO9Qw/Pd+j0hk3CGjs6QYWQDJa5LLb687qvAAF01pSE1LwG0Ro6yMQR1nvC8VIPd
Syd0em2XmFbiYsPIUtJ3/vzqc07jUS/VkSLAF/4Y2wqN4aM7667+oJB88f2vH797vMHp2lPbSSbV
pXahI1MymQd+hh9fRDlyrQOhkB+qwyAKNMyLVAnmdbWkhaNlpfhS0RoXY8BUr/DIQ7rkF/f9PZHo
gtHXwE4NKTdh0Zlv+QbxX/pQB5f9EBPtQi5lj/lv8MgexuS8a8hNwPm7TLC9UMp4VPoGU7Xfwr0F
CNqUFnhWokX6TU9iMvwRQXsM3+vxGXn8BadTlCA9AczwKGq0UT0T/3Ej9m/iUKPE+559JuMTaR82
ttyeyIN/g/3wz955PyvfaUkcDwg1EeS6m9IxDW8nz4Y/iDPQHXq031XeVS72bws8sOXBO1uTF7N2
BboTAh/NBWDgZ0BmNf80gUIImxhmsPE6qNc+1jTpsM6R6MuYNJC12Imu3FoHRDl0avmCvrQXZB+b
xFg+mSSK0d2RbRXH9NHvBaC1yXSR68MQD1U02Ls9LbriXLGc14hzYbFawRKkjiVbfHbkfst7m9zS
IPQ=
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JOjpTKwrkRa7WhaiwAPFGdcL4Xocbv/GgTrTaffbH8cIwX/mZVpZne9NSo2SGnjc64ErSNFM4G5N
FnkCHvxjzZTqXAtNovW8hj5GyiI8lCXgjuY+/PBy9kdSjoXRol3D+q6G8gFZR4ClzUztNW5sg7TS
+5jmGyCFHK9AP1df9jWeq241VEcEGsSFh3lnk1CezeX0XFXTVxGdJ9Om9w6+qbJNlQ7yOca9E8ni
IasLhw4JStuOUJtZ15/Qtkpfllv0vrUtzXsvKhbBTrxKf0f8dKSDWEdlRkc/qwuFB8Z7IWFdVGya
rhWPqXi7estlKZbAclvC11zldl9AoR7LlZSyrw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ER23afcMQiGtcaU0Jq5kaaC/IgcrxTHcSiDLmUi5EYzZxREZLx8CA8+2+GhH6SDyKDSb4xKyWMwR
+kt9mq9L5jWl1xauPJg2ZBnxnToefnq77ATSSJn62/sXkgAiLxYWGR8nrjM43lWZoa51zZMjkHyp
NxZHyAsgYvn5fKT5WqsK1qiJJTM/6bGOj1aviEwJOM0D2jsqTOxMdfF89M9Zc0Vn4LeGX8xmW4a4
Z8ubymFPXWCwU7oaScVq4x4J2418R2e0OctTFtS8e2R3/XFAdEhs9vx9oKrqqTLX7sjZ35wWFpp2
cIyy6eq0pYhbLzynSMvFG0EjcxpcGIU18PMh1w==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12176)
`pragma protect data_block
5IGRhKxBLSv/Ck+XPtJvHxwMRGTViTn66hnkkzE7VPZLmMwL19v9K85HL7AAcs3T7IAyTBasiLdJ
NcwY5+QIvKaZkph0GGB6yjVulXyWgqXxnFiDIj3Zwl13kZiaZVRpt2beBI8RGsAocTQ5YBJ5J+yE
8UxrFvV8yUYsB6z9Y7CFw8OjocrTrew6u/bYMk0dLXLZ96/l4AVyFitmX3bQlrIX8q4SOTuZdUHz
/w9/aD7q8ABHfajE3HdORcUuehoLeUDoTL9Kx+zF0HASiu2VwQeCE1U6PE1w5MqTSR5TTvWdTHD9
wluKKpbOrfFo7aIKA4+QFhZ5Okz+PKqjsakpawldfS6epMgSq5P0/Ay5OFmJLCrshrSFam0t5v+z
N3dFij7hUepafIpgSMyit3IOO/4sveI1LvcHGIb8VxapVX97lI9B/kZpQgRmb+bWLyNSdS0abEMu
WSPBBLD5A4gMuYRazwgHw+2YxkTB+ixE7KVf3OZE7b9bS2jkqzRz7xsPC1RbEdkF7ASChYasrHyI
9kGyl5no6KZ5aQdECzA1aqLq/MuZfbrJRLf1czXjOPnxXURYGKbwBj2W32/RW0O2QSAL2v01l4Lo
wsESUj5cqQoxnAtE3fb0fDwDpJ/+uvPTi6Co8OziGXJPjpWm9y9eBixNSZFZB/DRKHzLtsvFDwFU
NobHqc4U+/9vTMVFdn9TgUwGWQt48CCWBL1JVbRLcgqmcBigCa13hEKOhCEeI35onn84qzg93c6c
NX/parc6DJU0JGn3Hfn1PJLt/AYmYEfjlHFYF4EByrtVDHC3CCTYugSUEeL3Vl7YZkJ66V8j1bl6
3dx7VLKU3BSnR7gYwVHD6ZlH0/3UUD/t4u4x/sMOsktxtrQMkfeimVF08HHeaDfx1fJXOJXmperf
sFeocaZfy8Mggb4HOYePw7yH+0/Gr7ugYBGtBouNV5tfazLvJm5QmxOQgTFpThbwQVBOovlwf6Vj
MUUW7/YUDhRFKPblr7Kca3h8gfxKX78P8ROzqu80j07uG+Lrk2/Fj1SS/oD6/T5EUh9xTWzEBA0t
aTCSv6/MkEFb67Gs7ZigGkW9QT6MYrLE8TBUJ+yKpgNjBS+q0JvxUoq40qZO3r7md+1FKddRKAPn
WkIyNaMCNIevCga24ZQifkFOARJeUsjMZiROX6OelA5tqD7dntKN97DBX5gmRQUxF6SSZJ53FcBP
H0FNjf6o7pOtM+QF1eaaD1jLQtV45mTxhq3USa5UVoa6erUy54qMbwX3KPoLwOwyaeIEYm5vPlI8
0YWZETFfR3u3z8Ve9JiTwn+6bj6HwXOqWLmBUtjbwaEF+1pFI/+RRQlzaZQRiQLjwPnpYrnNtAbL
7sNUPX2nYT0XIhNKvLnUjmtQT02IPqp/zTM2WAvJ3xhcjfsW619pJ6MzwMu7Zldw3TIWkMugnNWL
4Mkh0wnK6PQuvspssTPHCZW8A70G4J+GKrRLCQqEGcCNMqK012n+Qx4iqewkHQ1YYc5gtV82hjuu
w7uZnd2S8A0gXgoQ6BX9WRloNZ1zo5KqkE1uSidupfZsv8MN3ZsvPEqYiB6wPWzaOFp3MS3nxDpM
VOId03QfcDuJS+L3SUWy7p0dHKEpaeYywFLX6UEkyLC9axmyUkaVAoVNNftpByDuD0EpOt/q2TQD
WIGuuWP40K9SkckWiysEseEABJBFG89mOPOC+kpVO0IHkpjSIPzYOyy/ON6H9Hz0rm/VYFLqNlkZ
KuQNB1a1pt4GuEKCeR1aHo2FwQe8baZuK2/tZf3OI1KeScrWcFuUHTQ4UMxeqTIWZTzgOANY7fkf
auFLTgQ3+HP9Cn8CWJhswa+wDCCQZM8RpsTvmdgwTDcy4WYNm3ai6cu9bEiAPaOUUQj6nIIQVXW2
2gRtOnRxo76FM722Hu9Je02tqWX1DHPAfRiOViiFb/lVCqZNDfiLpc3cpSec/QgvM5fVJg+Txwa7
nXjpN6FWeKn8hcac2oO0H7uQkaT7bV30JoQo7ptj7Xl3MazmCHnJq4+P/SNY6mjIw1nFPiEjTICx
vSnj56+6DIeUByTCOdWKjqAbPRBKQWA+tnSm4248RJ/5yvjRUPJgxpDQbjJZAvQbkfVJ2RPa4MSM
3Df2ZdR01g7DRWIYq4h7Y9Q+rFw+xKWOfU2NVDEFHV2+iVnxbQIgoCyorVLekHwl5jBNO+KHb3O5
8LIcWbDqp8T2gf32fHEWUtThQkffvHR2lCJHMHHvtXCR6XFgKBMncOcpvMiMZrwmpnLyvGwvLboA
lcBK8l935899JZcO8KdTAp9R4ygCR8EDTVpaMybNJDnw+NERuskDm89edpg/d+fYx4Z0lVlP9jfp
hACFyHxPzK12DPVskMYPcp/1CAa+013hXsREcEy26tzaGLNjXZjr5n6YbKJoC1DZiyH9sFcHOMFm
PT4bfVuOoZOgYEw+2+PgU4VJxRbKlq/1NN0ywcUWNgK2PAKafmfxmCFDgkJDhIBaqZsXTTELV4tf
5T/Hs9B+dpZY1NePUuBHypO5Vy7jdvvJvkx2cLMgyOLJm1HfFu+JH5SSFnbp222V9XqHJNi9QdRH
q0Y9m3LICMl+ibAl18q6mulYxJ2DvpJU9F3hRZ6gj1gV5O2hjWsrQ6oygUElLMeGrCijjD2HS6Y9
v3F9wOKYCkEAIJMRE+dCpvXoQOmv1xhc5bdEohoClrYx0vz1fRNPRHNgLKGHdM+sqPP72iCi8727
YlfG1263KIeRQZw2QXVJv6vudRvIGoB0E1UF96hO1LYkN+e0zBO064WsY8Rcg671Z+ja8EE2Sf7l
AVdeY4Gw3UkyLI7qjEeV9/T6RFtysRHR1DmhlTM9qBNC7OAlm/yDlLWef6jSRk2OZhrrWyfpcqUr
qmsTcB9AVQ15dHO+JjtfwOc+iPjP+ffr8dMhoEMW7uwEBLh2u+OSB21unOBvu8eh77X/8VQu+xrU
69FDJ1fu3gjVrGgyt/i2rFbXPJUAqiypLAItrR9ObJlI8YVvbHlJJuNX34vZ1xmJj/Ym9yMBpdtN
q7JP0xhV6VEP9h8duVlseeJRMPxTK8bVhZzfoBjlBEOVkpv+8cyIgKIht83U4nLihfFANKppDRW8
is4kFX9sHJVQgmORkZfY2pZYr4FpbK2wbiNb4ZYb7f3tHxyjUHWHtxXTlMpo5DOLdsAwpSQvkXbU
xXAW8lbDVZqhARq0/RDwQ/eDQkJZTXCba7JdXwTA59ynkeNiErsGSnW8v2BuPbJBxywPguuWBVbJ
6Rh4e1K1EILfABgHGVQL4YZd2vGCatyZXxkGgqvCL5rHPsVHECxf8mTZiX0RZGH/JQGURcsIvHvP
V7PxftR2narNL0HdUvh5V/kzN6baGsPXudRRPfZdibqGd8gE1XMIO4t9CrjTYAQjp2qz5Ctd2mVl
xHfJG7KBc7w5YYm3HIU/rfHPtfld78Mi5/d1efjYl/BXZVYWggyqdgelhRL444pZA94zmw/NwWLT
AGdE6qYk+mCdpK6teD73mG9ciEDurMdtyaMRoBIMpUU9MNyKDBuxaA1lJvkwr/Fx7jGs8kwVxRdX
zDuhfw+KiA1CdcM2fK8JajtOXEEY6Dqktu3WeW4sFSWGVZFciEO4aFx+NnFd+rkckdwXGIyoueZ/
8zgpKVJ+enIN5chKAHUZdTPVbOmP5I8Wplsnpa72YCtMTsCehqmKiGvcrJutfLlRzwZ83LFUCoC2
BiUYxDi3BKSmEoChTY6gh8J2vJ6z00W7iG10l7UJx49bmwjshgFOt2ijWJ7T4/LyOtCHe3pyGyD0
lSrvGmu+uK3dUAJjqoI9JvIONPAQHUi/aI2Q3G1lG445IBECAyQbx04XtStctog6tgKeh+JMKWQI
04Z7uHNzt1iKFWlPkGNpue3P6OZvFYR0jrPavkQ+2wQjnLbPMnm5mXzFQMnzFbWmIZHTMXHZrVA1
qNkquEJve34/xLwS91qjQGN1/rf7OIiFAFXgy67NG7P2sNuEDtOGJ/+LG6MTDJGC0ymEzMpZBpj0
fimmDBIhZRZyXQdpM6qVTmPZWZlT+9bsIIXTdSmHY9HGS7DlTI9QPT8OxEZ2LMNVTQD9f6a/X/hR
UkmcgtRIGn6WVTLdAcem9VAam1CcT1RG9X+hauXng8SDqCJY39h3BTOiMzGssFHO+hK9x5Rffz+X
mvppOh7cAcDKBKQeC+NsJS4JEslCAQNYCfjRm1mD8E/sPxvFJb8PTneesPBf/0C8QIdeW0fwAFSh
/lHP30PnLfbankmps2sugFSUpZKToohSJ7RRPbSLD6BwHTgaBAux6bmIN8PbN6IE6QpxXDJ/Ue6O
HyZgkPzPi/QebOI6hQ2jbbUi7KjVxFmash6OhJ68Pn5LokcJg8S22P9OkTUJth3X8nLwEwpb1DHE
dC5Umz9dXMRGROFpdAvbJfwLTOGdqTJEo+5s4ZCrTr8Zt9Ay5jvJwhE8J+wKFNNNDsYT10eGZ5S/
hPKpOPa1LRu5M+HrT2bY+EqErgT1+FBofuId/hkAg7Ognh/jraKnY2LnnXUYxarnd+ylPgDsp0EW
VJZ314NKk1xYeS0zR3vJZXqHCBKoG8Wlghb8NKIXQuAxTlympltMRMEsN4ZWEgbVihS7vOiAFuuq
bSiwximPW0WEflfOvmb4wXpF10JkzeT30ARgxxk1jEgvEnBg0sdjJHJ60tPmmUQK0VcQ5hRybPjv
DCpPThqRvu85WtvrhlUaJq/CpSlpJmKLpVA9Z8sRlbvkad9lKGPB1l1h3TcB7hoI9nIdpUPYwTQ8
5ls/yrowyFTMZtiKFAKlNZ5Xqkbva05mgFoUSDRUdpB0Mi5Twzz8O94Q2FTymn6vdSHtuRkFzW7y
IgMJWOSJ5ENfA23m6IodLYeI6ptQl0I8oW7kHwi+kLIjj05m/MZxaBn7+rt3Sa0WuXFSzZol8izI
JtUXr7ioEOVZdKiJCwJBgeDu6GvZVu7idi9t3RPbLKI3UJHC9LH1c+FqZdKlpwdGg139aVXWKRrr
af/RMhYqwbMIyuA5OmRvEh5F0ObRRZBDYtG6ZrdPr2LS5SXPxFPc1C0jnpTwpuEON8zQnOn2GtmD
pI9PRDHHR594HZbz1POKRdT7YFBn6Y3LWRi756xnt9iYLK2vI6UJPISFCmYCOw8wrhsFwzQ0b8FS
g7+P6Jzi8bxMGtKXXmSnC4BHyCKu5Atf26hMSb2Qny+sdIKjRXrTHqTj7BsHwW96W9mEXUdPLQz2
mr1nccs0y4n8nFs+f60Bse/xUT8y61gECRT1krlevrMqrpPeWHkWk0sw7s7BZ7WsUbBgSdnetcqp
EfQd9Bjde5MjKvVVStsYd0bFBaWWDSTre8XfsBA/2lyt4q7vCv8ce4idoaF9Xu8gN2iOvb1Mb13k
kWYXq8vTitjSToEItPmbzXSwPOjKVThCg41RtXD6rU5hWPEd9h3SPHNcFEkvIBp8i3sllCzcTYFG
cX3SC6T24IjSMhTfSYC5Wj51xYviASLy8xikmK6DcyupgKcgq5wtxN0SzDNqtmb8r5EwiuteycQM
xjSY6Py+d7NJ78dT+2Rr9k3Rt167RuC3GIUvMWtaPrsIsIvk/tG2G19unz4Vjh4Vjad/t13NYb6U
DX0Hu6koIMJ/y7CthPNFf3biI/I2lZWF2vXgWaNWadGl6JE0/cCqWKKQChfQrHy4LPRYUjVOcKk6
TmxCjJLRvSo+zIcyyIpmWAbuWCH+hsMitkKAkAHk9aaW1c61TGrGtcelvRa2aTCTsk8zxtlUW6Z9
7aRBCxOGYxkzE8G2MEmTjQ4hBlA8NO4bTvHEkVLmQpSn2CfOWobFTX7uYXPQVEObJ+CJDElya6nu
K6zJ4ZMGsqu+xDFSrYFB2su9fmRifU7UGy+o7FkkE1XuoOPbeHMZ1kKivYNBMe//G7hUcySH/+8S
Wbx8dAF+XJMiiJryQ1IjolzFwjkJK7kbX4jizEmAcuC2LSgEwT6xgMRNOSSAbeDk6h1lE9w1xd5I
hMw6FKoZXLqw5oq0qZ1DNr0HVAVgyLKNegqLKKihC52zmWXYWEHIJtbbJfbvxgPoVJWRMgyOn4d8
1A1SPy9f3SbanUPZZ0H6axztr6A0YKQx27vYUXDgCzDH6sfBCHryLWZUmvENMptzWBEkxtjnvCqT
FFU+0PlBWysgjHQ4Y+OfodzjDfVrdjqPqYcBOv/O4K7TCgVqKfRUMflkKs3vU/UsajyXf0qDem33
/A97/EIEeV1Xd5A+z70HKIjV6C8puitemKhSPSFmt6LEwLiNbMBKi79gQvsOCkSmAReOfW6nhQ7r
48u59kd9IewjTumgWt1joSv3j/llT3bkAcJ2QekxWL2hwpfWn153OJpEJa/She/FuCHrq4yhU2wJ
HpLLFH/xKCu81vmjS4js00oLkWc/zEItdsCHLWEFvyiwm0tnoXk7WNhqyUW0c6vkcitctFFdl6pi
MiRLFMEpsznCJv4bJz5Eh3lOT043oNmpBrUFuMLVjYgqubzSV2kqSB9STrjyNcUQRsadsI9ZGWiU
kiFqNqpGl94iNcAmC/w7qu41fhRNrh4SRklCkorXX/SXuA/KRZwOf6rOCHkig2Vz1lphH/TBEWAn
T2LbLF3sq9xFmSHojL6GaUnU31btB1T9QXqB074KXpdMxRSskYxnZUNichLc4r3kCNnkq6sgAoR9
iGV5MSb7YRaNN8mTgW88X7srVrLLac9C1IyZWR2haigHdG7FMiBZV3yoeuPMsDpmLGyeSIUh3HpY
ULmyPfd5eYXXClST3DAZ6jKQfVq5nU2gEZeTE8PsVq1+evCxxoZuejUuwt4EtWMOJGJ9OUDxYDUl
LNWDokYYbPPlgOaj/MXxi/dRTiSrLGLc6AGcxYws/Wjowqi8I3rMMr++557ngd4/De22O0dTA939
S/H706O4ZUfnkzWU8Yzri6cNJDTbpBSaKIqar40TC8KPzt99ytXpdkhg5HXWvfEkvtBQLFiA5LPl
s6apKA51bg6qutyHVmdWYOEAuj9l67ZIYdjeelMtGmt510/SRsDkb5c9tuKKPeEhw9EtFgOkc3xt
xN/R8TpAmmpWaN5tRCfEqsCgK5ZqGcLqJe7ULmEcYAKC4LZAirOlDAZnUTkQbMNdzQGXAL2GW1jI
A86CzA3deGFHUbapKMqdigapPtgfSKFbCxJ9zHZGvi32D8WLcmxNnm/6aj69UqRBGCBYiPYsEKqF
Km+BOArLxQW4Qf2jZB3Ed3O8WYbyY1prs9mk2vmg6B6GySpLcfGOwuunFiKIbXfdGwLdDOIJt5zi
+tcCY3u7RbY7W9foh54RF4UnsENnlgG3s/KQAETo8RkoSnncBJJ0E4OsSCay4UCpBz6QFigIYoq9
OJwMzcg3fZN0EnLprGGTAh/ZW2EnaNA5GDV8rAftCE+1bGDehq6jzxE0gAekXOjUE8PtCjHlFLZ9
Xi9JSilXY2C+Ut8ha4W5//SyJPKZXwzlpRFjxK8qBtAZEVa4jv+h6XGpCioh4OPF9nyz82CINGl5
0g6Xc5+30loxnDVuVXTwVzGPN5A0QvWc1o4Cw421fM1UobAlFL8PDL4DqRCN3xLua8OyxqfuaE51
zrQPxhgOfYNQLEsHd+bNdNBxMQe4wN0PV6dO8HDY+Vv3zcfpdo/qEzDEBetXQOns7Yt15q6kHY3D
TiwfDcRp8j1Av236IzFL9+7BCGmnvDsZFG/tkdOwG6X4JtrSgQhsy3Bd5yfdS3qbjkkUk3xJmknL
RN5nQGh4AbTADJZz6F4OwBu4MVKh3ggtah8svzIa9OeT5qBWscMHyjaewfdXH5Uq2PCr9MDwKu2k
CzCfTmf0Pv8cs7jFAxnqPI+t6w+e+vj0Q4lkQlZY4WKBASZQXVa/c69ejkmjDhF715Q6LosybxRo
nyIVKd768UDez8c7kNSOvRrJZ/d8OpPc4umn57dVaiHtrsedN99j+6tnN56+g5DTaxSf8EY/5iOW
9Up5Gi99O3MWaNi+rIiO7R46SBku+6XyXt7FkIWO8xAZ1XeUdoV4j60TESqBPbxNDjxNTa51+I39
Kdh3PddsFOQVjAxDeVo0ukKc2VpbMKya6MYz1+9hyTT/DvrEUZ6b90qIUBhIvN2fZtFV9bHYjyhv
z0t9E8dseHSWgflXYlt9DrWiYxmb1cG8L27JjQ42EmRXgbOVPAGWCDMgoAfXcJGpBc47yeo49k1z
5sAmOigdAbSp98ALikQ1p+ItFyLLoB8mu7zTef214mMaxKr5V1Vm8gT97U4VyXChTpNKyqmYQcIt
h8MHuHI4Ki/LtvLmbZ81F5hrFSf3jYveZ/EP5N6pjpX1d8qjYaAp/gXf92fTXEXyXxLeQXqGFPUl
BQGdfn+1Q5MLGVQ6ItwLQncfB62wWVoz8BS9fVmY0rLNA8KdIxbfJbCwP5ZCx7eyKPGh3iYSByz1
cyhi5+PAO47JbhPh+yBePQjTtr2F1XSM12Jmw3qIsnGu57LnbC3I+qG/JDkGlR35YTtfauqAjdS2
pP2Z+W6O8wP8iyAxdbJvee/5KcM1s38sK1z3VjuUuN37vRB/o+04elLuQ2ntTyVBM3kd70WB3wBq
pn3rzITANYm+iWBdq1WdL0zjjzREcIxl3MGNm4jYqm05Hdm5UJ117CVo2HnIKQiRgl3T9LIm/IED
KaS7IGVSjL+KDqDg5xt9M/INq9+xUFPqStt9SuVztbsg3b3ze9Ly6Iy7ATT5VGrg2jIT7v/cSv3p
mW9cXQRSviZ9EZag/tssBMIRB6beXElcJ6hUsVQDhrv50WueOc2wrS9Li6zI6IZLxN9F3FvkKZYF
HaeknjuGY3piC6jqiQtQD7rqIc/79wpSIasTWYXBT8D4svOix4oPkt0DUW31GtSuih8EW1Tf85ab
SiwZhwQYC2kMTzeXDSEkJjCBDmE/25yfq1XSQDBEawhzvtAcpUFXHBFc9rCo+7D6h3QEbEg6cwgK
ZY0sOXSM8GX9wOf5HjFprd/HiSU8Jp+LQtGN6QsvKcFR57SRehuNl/hoO+c/yX9yICxW9tez1Uxn
f0vb254q0Sn/kgYJ/9cYFiqJ4uBVxsPebYMMcPJGZMuiTEROHnxtitVWd6FWMltLkv+UFca0ntdU
lY3p/gcMsgDgprQJaL6Qoz2pQ8x0G+xS7G3wGfo1ihwkbcM7rsumolnlGF6POcVNn4nqVp+RD7+K
2GMpqeVlUB+XFEt80eLnH9Scqkm1ESTfzXgXcUY41+Awo8k7AacCsfS4g9rebsrY2ThlVn1OxSlr
mWQOJSxkdV/k5AwI04S2XkOw6v++4s+w8Z9ce8ercpKbLSO91JCc2BxWw3oYOwEuNyvrUfItwzkt
sxGcM7dTKJkX67Tz7na5Urhyy0e6PrTpGily/7QpuYSfIZrq2YkqSS6PgfHBdjTq9lVRqXRVxe1S
JMb9uCqXlCL7x+saZiTKWNeG9FgO1guqiiXqckmG6+A+ROclQ5dET+rKPjna7z7bh1gMKVFfyU42
oK0q5ThpJv4hddimmjXn5QVH3vGMq5m4A7ph5b9NwldFqAucJA9d/X1s5Bn2l7g1CxrSAB+ECoIY
eNy1g+EMKZ2Sli/RWhuxGYnvySW3Zdkj2bm8JtCqtBYEcLdD0fByEKuVi/ysCTje7DfVXi7i7xUW
MpLfoRzVhmSQz3wamsQAxEXsfB0Dk44KjCHT4rN+XmMkTtprrrnMN5PQ9NkCaG6j4kWr+oRen0hJ
b2yGp6MkeiFjerGjUgTKZ9nVzpX2qfwT6AK2LUDoQZ+lWK/n3Tg5wyQ3qSNMLqH+oHxGl7H/G7vU
ayWLBWrdZ5UI+9oMA715y7gLAmVAtmCcMYvnGR5fLH5ZCs0pll43JXXXO6gZHRl1/DlX8rLeFukG
PY/iAgpKejCihpDC9akJZIYVhZ9XoaS9ql2BlTjSEUruWYgVhIzQp7qJD+gGMXKfYFO7JTubnvuq
bbkVRSnai2Bgx2JSl1HSmuvIDtFG9mAtSn3e0uJR7RahSli20yabIHOtRKnraYlcinJ81btPnaeU
vkgKNU0YDeZ20tgXxlYtwXt+whRFexoUcR4v3Dbgp+4BloiooWXC2/I+P4EtXLq58YJUDtiZ8Mkp
6da0kk2tgyAPhwRzSxZS1T/YfXmOCRM6hawI5JDTl6uc587+UFfX6ByCNmtyC7EFMHzZ549Oy3BG
RlU1Q6lG/M9TkcZGVOnDW4eDXMxk9GvZ0nyNMwc9cxuN/bqCLgcxaHnMtKovNVYDbBzNGi8ZCIa+
jro2le9w7NUTRGXtfQ11KzEmi4AeveWZDPpS1WfCHFPD5UiU2C/O6JKxS9J4v1zwoV9dASKnbchJ
Fi6ZqVXZGkJ0yLoMZUfcl32AjG6uZp7AXovErjbQUXvo5q2aE8+3867dVhg5Vi4rlHBrPc9XjHfd
N5rESdbs/eXafjC5hVwP7atMKZ2rg0w5Sur6TG7HRRKJPZ1nJldcTSU1wWP+3KqDf/xNUgUIjYST
aHK2iNoa5YDQqfmRlxBXykMxZRfNfsLIPPwtjlOfsjcJi/fVOMwJTP+kw+0deim5pfFCK2PfWDQ0
tnSdm6H6/FgpqmEFzQk2RgVIIefcwBrIBzIU2XlaFRe9knENcsRET+0UKKRm0/0b13BSvHO9JPVB
dyAdyOzJad0hqxDoSRzxlToHd3AG3VMTsQqAbLpz95XhtZQkCOwiT5ho31uTXcATjLx4MbFPqpVk
O/2qyWV4pFhkMezEe+puNehTXYlak6Ss+bK5rk9sEjMUmSm0kmEMlAuf5xbTYX3bDWpCWHb39Tul
BV0GAfdwaXpSnxeaoPfYCm7+/IvmqewlM5wd7a0uP/kAMisnDC6+UzZ3YcWkpHTKLLENz+9e842w
NpaeqUrX7zf/Nv3j2EvAD5cPfQTJbOSPE4uwEKoRZbQE+MxfQGfoQvVRyDWFi5JnNmgHmuenqSh1
7jlbh9A+XK7kxbwsK+RXJQ/3i74eZxhDPDcg410WBVvpdz375QVTYTc+VRRkXhb4IFXxgsfSgfQC
MmLODcgXAQpFPbf0VqlyLA6jvQALur6sFuaudXIzEa1Yxw0YgaqvnIU9SMEobl/yZiMG0Yru4Pvp
eo2o6hWEtUesmAhKHqWvjYZ4Gg7fAwvpWob/dndBqp1/VzDt1XESqm7388ErEgfbOr2f4kMzpZX3
WEXWEh+/+t7haPgRJVdfTPux7LHYm5UUFdrXaKsTgGLoVuEtkWX7F6T8ErOVYc+QsOvHGbccEkDp
NNZD4PuQaP1i4BzxCUp3qbm0PEQJqTWks4yVHSSMsDIYLWTDDRdc1sUyr0XevTEHKn9joeiGWtfp
DEnTe8kIoozIJU6ftDVv0K9y4/opodN9jiN3CiYfhzEU7l9eJdvKnCO32VI40KZFRP4nJMywpVFs
47fpLPE1CItDP8oSLOrVAqXM9l76bEHB3t83qa5jmasHH0ALkq2mVL3E0QZF50djh77mmNSpP3NM
F8sLJXkdx6JnhMPWNFiArOccgJN7Wjz1M5Fxiz8Uy7wrswt0OqmkoRxh9QeeXIGN821ENO3t8Dzu
yJAEVUlQF1Vmtvw4TBJeewHORPAvq+JE4ihAEqXv/GSaiYFI+XZHKwD53Vf6wy8ek7hzOyNUb9lP
h0om5uHWWYvOdO/WZfKPHZz7GmCH7vWoBgfdv7+5hnKPTh87SqNgO3snLXKcG0fgDLY3CWg6mzyw
tEN8ke/y1pUy8+VtKFKyVab/+JIXV7SYVQG8BzWqM/a1rI8AWYhMLZ2u4ONJc9/4pcLp3OCRmxAF
J5t+NKDmJXqplPPpYkSRQPYlQ1dndXIVqrc7Te4GyDFbNU1y5UEO0lQirkeGSgrVgG4Xh/SR0bno
/E0DcBawL7HSn0MIqw0u3HM5o+vbGWXH++pJj7UoB+NwWeNrXjkZ7IdjO+mDCr4QKlmjEznQ01/U
yFMh+OJHaCSX/TPMJ9VvOR7gVkswh+jZyROq/3gPZvrQaWD2QnU9o58AiehZgi4jnhB0d758Ge9w
Wu5zn29e1fF+G3k3Jp/VkUEWCbbMJBT4NKFGgjm8NZ11v0VnIT1PdFQicfEmNCwkVumbpbozrefY
398YPRKiqiCqOcWq8iX6oPC7srLUj62QbRtGZLbplyOoyoZ4ORnEDcz3lkT4nK/jvZ9H8rCy45mw
JRXPftewaxA4SFFEAppC0OWvO+zzUcA1jrLWTaR+L2MRoUi+pJ1z1IL+44oQdtRHRHeNDHB9cSwM
/gaYszuKXhM8m16pDWriQmfIpvw4TCBGRkGpcVe4+4yAWG6/wC76/i1S/wUnZX2OP1f4TPms2TTh
0kHYmxWyS19hLWOHRzPVFO+zVULzsaXIq/22uGD43wMewegEbR7oCRqmUN6NGAPEMQqsu/Wcyo47
03mCxVV+bNCorb3R3iIsVLdAZ9RcMtPPNam8qLupxHcLzrZq35HtRCl1QziMz3lcWUwfoCYEKSvQ
IA7o/yEhHqqnXOBE2lfUSq57vbbHte/2bBGvE7vpfUrgFD5pA0+luhSr230NYugKS1fc8XtrJ+z+
VSLxcSUC+/DY1mZtLQJ5bQKy4DbuuABS+p3dIPUeaE8mCTyd3fcfcoCCn5M5fa0W0fQtSSO7nw6E
98OHNfpxblpHpRwecgaMTSesojI8GLzEFIkiYER19scCZ/SLcx43IXSb0sxL+vT/x7NPx5wLG//t
JreL2mA23pQluw6zSMCKV/hCoM2ffiZojnCMdplZb+9MaYn6wyDoQhX0Wd6f3knHz8PkP5jpVVpc
8b12iqVWlXgLmPp66keWVMO19cAJgWZL2k/Q3MMiO0Rwd8debSW2w+6aNI9A7rgZ0hmEb2QMttlo
rbUiuOiECHgKL25Bd735hPaK+FWcLHSAZ0jGtKkNpA8wlbbsUJI4Rb/ZIyPpzN6E+58hSyLTXxu/
SZm1SMS1rIpmZcMcxIwp02G77HxK7U65/g0NoOPw/xIvMh5LxX0lYCJ0v6UitWsQ96Y0xP0s8JMK
42huXAGlC6bi9IV15ZhIY+GnEg8oeiSfrSgej7gYY/H4lVQCu1qlnaM1G6/28X8ZRUfHH24fHicK
dGdBDVUfCT2H1vIeMOF30xbqzdqRlK3Yqi7Z7EmmxCiW3R/+eVH0+vQFLVFejLM10vtsip6W7n4i
K68xtPzFKZ0gpO4grI5x9xaZTCsmcuvFE8Sg3euFnSXAbUONBlU85gpGMWXd5gjQJk8BmA4EiVyo
A5mUk5bem4zxra0qBcYAttJs7iGtwL96A9DCNAdy7VNBMgOh/GVKnTJJ6/TpBKRY3wXQJsc6TU0I
YC52GWnGBCZpnUT2PxEphQNm88LvpVEg7bu6AAt9V8THSqLA7t4LjNl2m/fPFEXO/hAyksSFwuEY
TquLvtY5WiTG5zDQMiRRL+8eiGoq7iiRYMIRJbfED5OaqqC5VyZngvTSHnL9Efp987jdY4TvR18X
KBmmNqGRCXCHcCpOJ7inVJWXcU5m5RBiE5D2DHD3nSqjBIT/LXAPWhFs73FtJSPlG/1QM2y8ii/F
rJ/qdwPCTyz3BxC49S/62KjiIn/K+612Lw2raWw6u5XRIR7jWwmgHFXH/KiMGyoKtTusFPAgWMje
XTSC9frXa5fhA8n9B6rfyOuiq9a1Rx0tAtveHVRvRDsIDzRfXSw4kc7EBF+Z2Cqjq9+HoHIm1HKi
Oat43BB+b6BOcJiy37uSmjTQQakLJGi5hZefgkqZo7CIvJ/m5qckQn8f+/p0ng3KD3Uc8hQb+SqL
B9m2QNAHvqE+zltwPtNFW7SebS7LD/K/Ry8J/3i04K4RhrpHA5mQg5gjHA5zwrGYMG5Y3/hARIw9
B98PJa6P1KL2+ipeeTj3STunvgXVf1+59XorLAiFR2E1HBUfh2RuEeGdRsp6ic8EpD09N+/yNI78
To5S8FZRmdWE39BCOeLuhfZAi7oz1QMKg2af+qpjFLnBymadJFN+4U+6JfAa5ATchCBw0lx+Fr+W
tky6Dt/KsCYqleo11rPYcqtTA0+n8bRAuOPg11inWxxWfV28AvWcF9/konBiwx7VfitbXzxCa3TE
0/P4nCrsXNKguox4+5gNEzHt55JNvf+1Nnm8Jc5+KWtsmJ39S9xoOP4kOYs28CIslw/RxqyLx/Bl
QPRazUhqufpSInSnaa/qYInl5imYyHNXPTkKqk+xyo52eBqSGZy2PCLLc2azVenOP5ojdlpzTJ3S
2J2mVnFflFobXBz2VISmXPM+iHSFFFynfaEw3MxxBY/NGIvfGhn6dy4o0/0d58MKbKABn/985OjA
wCYBqgpmk25s5KW6WHz4m1bEd1BWVbWEcRaI/GtPlka3bA7yVfj7YUinbg9XnR+/ln8vOogmEQXl
NywxZE/UPdWqDatsj1YQ6yzzgCE+CEAAIl4eBJDXOprLUVzMx+GcKPqe+M7syj/yPAVnB8qNZxQa
hA1Pujrw/p6yUx7r/SshSZAwynsNoTlIN+ulzfoMVWCc8f268/1sUp0mTua4OIy5TQsKrkl2VbqI
9diZ5TUr8piOWSwLQQ9aZrVhREa5WDj4yQ0ttkGb7nAU54doL2QumqsnJ0xlKWCwQS46lYSAivvn
NhBubUvxO6lezjcDFzFQc59lu1qq0tRXZ33O+ZU1mOtc+jfoShtqnvu9skTcOr9Yhmb5VCofajZh
sx0svhaSgWxyuNCINUSsgmeQZ3wxaHiKICvBJ12ZUyuV7blvoJk4uHXVwzhJJFcbYNuv01pO213A
WHGIuWuMDA2qZNV/8ZswQfmaSnq5NeUzDSmX4tBCuIcwG/JIhUPIYmwE0JAvgUKTCSyBhuWRqdbL
et8ZlWjoJadatKIMMcQa1n0BXNY7396OCLwRA5K0kNi3axl8GMMacL8lv2O7dclTZ0eU9EhHFJPb
9aHmlfh1gBoNNYDL2LhjHhf60pgW2fbVTO69k3VIovnaPatI1ydVZoCCpEFef8KeC7FjpM49DsGr
l1zxQQcIXYDEJQnqrH2jfSgnKGgXtA05w8ESzkp5LME1r447a1P/zqTCnhraFmu4x2SbouTRADs9
pw9LovYmm8qFsw4po0FObBa1v88yW3+4F1w7JKWiSO+0RIOumrWY2GCxhHH80Wv5bZWnViFI8+F2
Qm2cILMJPDZUP71pSuCCkqr4MMreKeTS77mx0jaCgVagOzm1ocQW92xM01+dQjRESvcJEvcJjkF2
zQVpMyKirQKHPCGbfxs8MomXKnl4Mk2PW+wCrXxeKFwClgmxFFijSmT4seXGX67QHguClrNyRmXj
KKiO9oCgSE+IR1Nc6cnop+HGfAddyW6ymvLjkGonJrJpPTU3I5PbUzPeLUbpX4qbxWJsMz2pvwT9
WKY6EM/0g7188pUiLhOywn2MdqUz64JyJh1RxKrj9E8/s6q65SnaAQ91278stB8QCneQc7dm+3k1
PfdkhSb/qSm66SStSJuCer1LCzbJBVu53NYu6Hw6Rq3lQnr5K6tSWT5GYNkXwCqejd7l9RXTk/WW
HxiTycDaxBgB30AKmI0EMGZkTQgTeSqC6YuTNOPQoYIYzNvZh/EBopdSB2gnPYjhf9xrSTRVsrU0
eYJcYop9JOt3ZvaDG007YxltRF4MvNzUD4mz4iJS5ihspqp6obe16o4PKV5C9KpYZDlkF0nuYiVU
A4BisNxhF1CTo76INvgg4NGJbJFghVF9wV6lEh0XanBXKdlFy/ZYhsRMNTw/EBGTzka0+KEx7y9Z
bdiFBCNex3+2QozJUyTNzR2ETarqxpoY5mGP9A6/kMbuuusxMPEnfbdhLo4legRlu3+U/fw2qQyL
43d/U4T+/E7pz1rMgmpykwsL8M2S2gXzhSQSYoh/z79D6gTPhnxCuLaoz7SRO7/R+nZ8eF13jUQO
IwgYGnCu1U8EMAQX9dpwVOeqfLdDA9RTk+s/VzAcJG2PKhrU+UTYR1CjcC9zYW0Vnm/550OwWsFS
3kmffr84iGSOe4Me/6v7MQnQhZxSwU5b8ee5pyHNKmpnReI1lDJji93EyxGw8mysj9QiFzO4Jk29
KDxTe45uzm2scv/F9Fyncxvn3o1e+LhW6iSWoLl+d3QhgSuYV+89TmiTUYVXEW8hrpZRmHKQK/0C
WlhPr2/o2jesBduOlYM5O3X6LCVW0yWJXdP2Yy+v7/vhimOXpq3YXJ4f7Sr13lJvy1A6W+59JaJ0
tt/Cqy+gG7adUzAXedXQbR1Co+ATTj2og7r5dr5w2Zlmfrs=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
