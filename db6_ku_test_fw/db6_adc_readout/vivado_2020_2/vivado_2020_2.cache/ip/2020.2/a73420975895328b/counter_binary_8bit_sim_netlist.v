// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Sat Apr  3 01:45:54 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ counter_binary_8bit_sim_netlist.v
// Design      : counter_binary_8bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_8bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [7:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [7:0]Q;

  wire CE;
  wire CLK;
  wire [7:0]L;
  wire LOAD;
  wire [7:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2352)
`pragma protect data_block
/moYrEZxWjDogVb/sZa41BYjuhgz1UygeGKeD/vcpyCYhRFFIdubz12zCHiVpBKoLmewN97pGuui
R6RZXf9Q9UA+TaTKqJTAb17YXUFhlAyOfXew/TBZyDyUpZASU9ibJBSVcCPM69wnKdEkyqf+DPuq
YjH+kTsToql/nB/6kib8rhrs8LLVpAD+Nx2xwCpcgzDhU+vCpsLQt3NC0veKa22i6q1DyqLPxbHo
Mw2eKoDSrGXvWebVZUqiZJmhx1wV/1bd+CXbS5IdypQnPTL0RVZEfVahOjLva5fcmUqcxpU/BoVQ
BO0mRkYl/qPWX6UdcJm8t+CCZ0NtdMiQdLZguXwXIiPrVewpz+igIMNyYw2ol3z2bdMArOx2/2q6
eyweaifFzm2+3sipIGYwqt+T59Jk0chZ2xaDbPTEG8x2d0ekKYbsHHV5eb6BYwgDNV5QTJfyGvaB
bWdrHPlcWo7TP9qNyOyMqMJ73pUeuGiweHXGnh4zfPZrs+HOF6nh+kfzRjpdX1fT63k0HJIkxLDZ
WKtsOTbngW6Q2X9Irpw+OKJPhtKIRaUJWo4cq4EFTGZPx/4/Qvjz1W9j8BtFrXSVJTXvs0aSvxbr
QAYpwjZ68sDwY68YtomLHFZFxmgWfN8rbWhbImuDhdAJjtu0KvOPtvj3dBfUpMMFdoUkMtfKjYDt
KIjlRTnkD+UylaZPO+jKvi2XrIcfurpK4CeU/TEmNmmnVNY7Ck6jgUBYsi6NEl28YsXJKcL8WnG2
X2JrEf3pNKaYbNVh6QF3EoHB02x6MXaQeBF7F6QcIK7JcCQA2FcE0vY7SF72P7tsjHJiHpC1hTno
/V7FVY2CNwqX4rnM6O4JdEAb/VSAajyQUHkjjKEwdAABoc9rD7lMNASQzwXXopF/wvIkiS1Di2ck
qnN1s0hsvsfHBpP2RP7saG+SGiVPRAaLO2IoDwd6VhsddZlwstXj132m1azrvSdr6uc9onYKyZ3H
Jx7TGNkGoHe1cEHAIAYwMRttygyJ+cNuW9qjwn9Ok7Y1sSUpPqvDP2vLASMhG4UoprUJ3NX6pCZH
Z3A+eJ9tTf4e8/BtwUmiex0hcNVpLa4HzGavtAC8OQzKLti/zNlOEcHirEBLZGBlXorfac4DT0X0
8w/IOMMWvvqIe9vmj5Pia5n7ph2+Pd6qCTdQh4Zw4ANPfceKilVw3cYnrSFKtMn4nw5bDDVKZWou
IraOOTi3EC3MVf17kO75osieyO/xWS3LVaNY/f4YVvZuixwsdTQsGgKJdKSQCYmao/wKZeYmeKGw
qWLrLNz0qi4nMMELHyp8voWEhOofjIKWEo2SacwsMfCBJ29N2x9TFNQULLGdWys2byaUwL1V2sG6
J7Wi1ZZsWSPNVrG+54xtq82M//EfJfD1ez7ICMMVSxrgavl6Xdz9fY0KPbqarb9qeg32C8DjG1Qu
aoeeZo1ona5o9ILxzyPBFikGseINH2e2I/uEn+Vu1LT65C67eOTOsbaOj5t41s7guBNXTAmH4dXd
r0iXyO87CG5yibnb35qlDYu4PeZVCBRCzzNysF7R5Lzval2Yh1esGxpK7vuGOzo2/73qstcXSdM6
1DHaawlsjIhvEE6yOy3AkYfTAB/Pm0elRCG8bBPNYWZke48OqHA3ExhocsiYNDY+uodK+VRwkzeF
/TqQ3XynUvGbDyTs9CMhdrAy1iRxSPKWmUtPtcwtbUAzj1iLwKf8VH99doQjDVsKnJ7ETWpJr7u9
5jaXbzUisOf7+4a9tWd4fTRspOT0dXT2Hwpimnbxq7+osA+vWfKIYRUDkGpKUVMNLPfVCIiH7QHC
c/eKTu9SEKkgKjT0WItyY5z+GFTULKCMa5R7wEyT/Xiqyd+rk22oaDk2I7D5Fh5XBurfRUyta3Iv
SF0H/BjMKrBEeYayfr9PvqlJvAoTMebTaV8QB95oF868izXpMJDQg33I7pCgQmuoi0eCXe7G0A93
Te7i2/umxoDQAiRtE0daFk+rMOUTyO7s7HZyGgjQqJiH5er8rQnf2V4PQ/+B3/JbzX1MGaAP97JV
S8uEzlaOszSMPFECGdeboJ0bWk9xF0R4mCzFhCFI6E06RU1NWCwezEnJp6L9ta5JgkFqfFGAKjpR
3UrXTKOcu1ED5Z+8ewBYSUwv6RO+vhc0l5Bp2JQzLITD6RM9HY2kqufRIA9bJG5y9iaOuQ9T4PgQ
rj7dlcS2z7e+WXioq1aQygJlTampLHNCkIovQts0/pHMd0TbAntAKDsgU1RWULOZCvjz0X/y+Fld
UMpbetetu3AVClg4PPDi3oYq9yQCP2OnMxh9zqzJJHQLpmoibtWpFG0QsKQ8rL5GCViEPeLTQS/A
yzFly/+IDZjxXBVppQb+ymKmiRoiNi2t+Fo5gqmFlqO0CyOdmG2c4e9oyfLF2VwvXEA/l/+JPfqQ
dIyZSCW2JhvQViNIvpzTfRN6I8Cd1F+F+U7hsXB+wVca5OlcYUqSkSRWOxxr90+kndz/W0hFQToP
AWMdlPb4C356PV30SZgDq7kfkDk8xUeb195g0Cu+e6iSY2aSMldb47I5X6zA7zi9Zi7B+WThV/b7
juVIMxl7m+NJXEKYV2bcWbXIG/FeII11LWIXeYzrEm+ZOYceldt0aTNWqOGcCOp7dNGBMM5LJGC6
ZYmeSM3Oj+anh9vyzDjjZd9jSPGk2SRABKb+2b6QidOxDgwvHzbKTMDQDsIUa6VIzqXP/kxE6Klg
1wOMUs3QeQaRAZNTO4ul8fClD7kbn4bY7NBj+3aiAmDqIQffmXIDIeZaCtg/iFG2NVTraxhceIGy
wfpozyWPgUdCwP3gVXIIO9I57XPQuWYiG/2T6Wmd/Wrm3mGHenlj1T+Zas4a6G0GZGtsklZAluOp
+AZc1z9UwOcpgZDOADP8YM+HAztrxMkOnTjvuVkIls5TxqWp/lT6ImaFwFfeOqT/2TEyBXW08awl
B+ko4ptigmOpb0SwEfyN8I0ljR8d9JuJ03pRUte3/qfToTCIjT41b+wgOTQXqwFB4fYzCTk41Mbj
bNFlFwFW4zCr1gRRou9LiFX4TgUcQMCkO+UXbBAe36+zQM3ekoB4VczA8QMjymAqGkxEoXN6R0/B
GGQ5qMd0knhuiSDg0VDK
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
nVFaNQV7zcepokP5w6LKlpo3XsWDxO6rMIzaln30n9L+hkEOXdqvN9kwakiwRzCPwlkRCvzLnZcX
qXKzgmY04J30Oywn8MySFjAwQkVtHkWUgMoMp4d7//tM7PnJo7ceX2kX26Tkx2+2VPEd7ol1PGeO
8JriguLGpW0oKWDU///jPSXb76UMOMQevrTIFMjVirz9Mz4gszR7K70q3lbONEGM9E7BZyg2hAJy
byFVDOdDf0PQyuWm2lcBIl4qFLCbp0W8SIUdPZ+L0mMKCN6A52gj7Q+xYnKE/MVm27bfs/mY6nQb
zPyzccE6Ik5XIf4aBxr8IjBaUDCj5i8Ercv8oA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wJXLdos+v74PlSB1qSwJQJlXoEsT1LSjK7tYW7oO+GQPHnWpt7WorveQmkI+SvjZRASpM/S617uC
OcLfuwp0V7cov2hXrjh4KZ3QEINYTFKDdTmYg8/+pVPY6qudH3Lp749ztxRukOWM77dSAEyTPpmA
eXoNT6dG0IWFVpcq8N/BTaJKs3lSsVdzmanRC1YFVM+X4LcwWCFEZak5VTQYI5RfJXsCtDDJuPsI
I8celN/vAICvhwsil6YPoXdO0Ai09lPGM3WBUcaD7ZUmXn0g7gY8a5O9J8WDEYY+32JSKeZCaqS+
bVgy7xieve8AL+PYbirjMmpgryK/IMN/DW5mTA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12480)
`pragma protect data_block
/moYrEZxWjDogVb/sZa41BYjuhgz1UygeGKeD/vcpyCYhRFFIdubz12zCHiVpBKoLmewN97pGuui
R6RZXf9Q9UA+TaTKqJTAb17YXUFhlAyOfXew/TBZyDyUpZASU9ibJBSVcCPM69wnKdEkyqf+DPuq
YjH+kTsToql/nB/6kib8rhrs8LLVpAD+Nx2xwCpcgzDhU+vCpsLQt3NC0veKa22i6q1DyqLPxbHo
Mw2eKoDSrGXvWebVZUqiZJmhx1wV/1bd+CXbS5IdypQnPTL0RVZEfVahOjLva5fcmUqcxpU/BoVQ
BO0mRkYl/qPWX6UdcJm8t+CCZ0NtdMiQdLZguXwXIiPrVewpz+igIMNyYw2ol3z2bdMArOx2/2q6
eyweaifFzm2+3sipIGYwqt+T59Jk0chZ2xaDbPTEG8x2d0ekKYbsHHV5eb6BYwgDNV5QTJfyGvaB
bWdrHPlcWo7TP9qNyOyMqMJ73pUeuGiweHXGnh4zfPZrs+HOF6nh+kfzRjpdX1fT63k0HJIkxLDZ
WKtsOTbngW6Q2X9Irpw+OKJPhtKIRaUJWo4cq4EFTGZPx/4/Qvjz1W9j8BtFrXSVJTXvs0aSvxbr
QAYpwjZ68sDwY68YtomLHFZFxmgWfN8rbWhbImuDhdAJjtu0KvOPtvj3dBfUpMMFdoUkMtfKjYDt
KIjlRTnkD+UylaZPO+jKvi2XrIcfurpK4CeU/TEmNmmnVNY7Ck6jgUBYsi6NEl28YsXJKcL8WnG2
X2JrEf3pfC6eZlcV/qqLtiO7Ee2C2eNGCjGaUfgeRz/TGiAJkF0hYOgLoLLSqiKTIwYuXBl3Rt/F
nYU/8nM3SmxnKUgyGroX0yyu1utTzLNJLPnQug92uEDJLT35r34g4J9feqixWchmGeawC9Q/yGTK
v5apcoSvX3YNi4HbKbQ9A9B/8I+0kXyIklHqlW690ZO8JTNRHDu4vZqE37ycBac5+kHnLCqrTR6J
xRqoyx1y0g6pVRRRjw30zqBnNJz6hVWEfT9vbtvli+ZrmGE+94RBdgwHA5D0vRnzBEXfyxNXnk0Y
ziOheLGwi7Nw/bI6mchbcZzTjwFE/WTNQfSciiZB/lphWPLx0ZCqVl2v5Ev8I5fxKMgk1y2KGhjV
47IiNa4Pu+02KNuXxHJ+pMMVQ/AxQfGiFXwj4jADRL62L63g/RhBYq8W4jwS/fgsr1sKeQ+Zz08s
zZbwjAJDSBmelT6uTIGbaQrtwEQUEBE2IhqCB00aoVDmFHofIHgz9gdQRVbEUzf36a9kLLSpRB62
XNn3iJOvO0EpgQDJ9ksryLZ2g36PkOY8qQW111efehjs88+x67M/V3qCWUjVMov8g6aCRke2MIHv
q5zfGEwztXzvP+eatU6j/mfxG9tLpN17wI8QS804DfqIT6dVEPiRIXgwpuBLyTyRmpn8sqRhncIr
Xju0YRvQxa6jzOjsk1Po9RjZu0CrnDKCMRsfzjviD5xrrF5AJ9c5Vuy9qI8fm4T66YwvcxwynNjw
YE0zsLGwK5ZRD783SIyr8qZTvuLv4tRkukME4+8RJWFOsyxpPbmBMQhECKUv5WpB55dfNGg1OvXm
nisWpYVD/bIQMBlVTrgOGdgWyWOO8+InmmTtBacc96iwlJKoG7JifZAhwsuPQw+xHJ0TkvPaglOJ
kAc+I+gDHNjaNt3Btq5FSBOVzerwvGFagdmFWsLlshqCU9NuayzAUxOCBODa7qFepQOlbn/zH1N+
91zGVzPJxYla/hP1+3sa7Hj1Aq8Wi4GteG+RRJsDKpf5gnWp/jaSXyP6RV0MDjf2xZ86z9c+09ZU
ZhiEhcjKgxMufhZek/Vb0hYyXkUts8GRyp4/BWrQscszZMPZ7VgpxaQDWSO2ual0LwFlDiUpLJLT
8CYvkuIJobLSn6/rlKG1Ml4ae/WhigJh/zVyyfvh5CFWv6DTurRAtxI8fRJKIT3uQAS2YmblJIqb
RBcvgiyyJahcgNJIeuGpYfWBrGM2qz1fLJHLZ0q0EhWkkM3NteO9VEaCSTbikYm1UdPPqKrz41TA
KO2tBFQHBH/nGjWyZve8TplcRgvX+KrPD+jv2UCrTj/02YMNXY9Tlg52WnjI75HjP+nFWfvMqTsm
nP3HehEUpQPjSci8S8NuImsKVzj5JlFH1t4uXiBShbeGLMsyJES41BNqYFdkU1aRU6LD8E1YqrHE
PjjLw8IdJgySpbb3K9yseJ2BR2SEIrA8iz1qM6qZ7KHnq4qV83ej7swEjxKUlyXUlLKinF+NMEC3
ad6ymCtfR0tT1zBzs4DDCjyTloiTDQGyAt/Apxz9OhtHNRXcmLFi381xtacsD1bkEpyj22pUWX9F
5V8d4sZK46ZrMgpSrMgOSTnbyL/n/5Wpzp/eXE3Hd7heub2u8wFTFBLKaUk0RWwmwFMaME+XIu1Q
YiGTS1Yzpb2JjS4IMJhrZgb9GsMT3lahttnnTn3aPwp9WwbkC0KP/fWZn6LbV10CoymmwZBUMwxF
zIHbqgBA032ghy2EKH1M8qz2tyuFwee8m1NONBZwH1G+3m4ahDAxO8TqlIQJ/B/DtiQvHHdZAz5H
0AFBAwBImMJxg6LCHwV6zn210Z+ZxCLUGRMoWWBcn7ANRvyH9lxOSBp39ov9Is5Gfl3PoIBBwIwv
W+Osb1Wao8CrcmNoMDaxjLtti1fwpb5rYKAddh7yaHQwEY/h0s2NHmVBWjkZy/iOpnMyD4b1ykII
gWsixsy8B9lYdQIPuBfmbljZ4CskTwM7rRG12wYBvOuqb7r/UBkPIK4rBEOeNcmOX8YEmNiUqWwY
R35d9mB5XSSgtB1WKfSGbTkM/dSVGTYXZWbL26l5NIpEioVojKBFtaNPeOOhuCGL7T+uS1Cxreet
oqKCaBFafjuYHdENkf8JDJFUxrGFSG2MiZPcmgECC3aaOAF+PikFgFUd+kGaQXFt4jIdWFFqDF7s
wptmspJHBc8PF/ZUq8rvQuo7prOYo1hlpbQRfgQ3+2BbMVcvgoXZE/vCyfzxinAJz7VrN5E3RP+y
vDyzfEXmJJKcMyTbBLPSyGrMDFqjvVIryNi5faAX4XiLWs0WiWv1Uc9Wg29/lSKnzl8BScz4cEWV
SX8qW2ih+5cpUkjiz+eipXpeZCawASz5ngMhZ3DXoSmMpy63BjDLAHXpEBK5VZO4Ljcqj+ToEHEi
SgmCK31g3S1ac8wCUXgTYmDs2DJyf56twnmQTDnMV9hG0sdIFoxlMRkc6uaedvNGcF9Ev8ucWLvw
OU9VaSSjW2ubix/ftpUPCA+Cyq3O97pqvLn7v7jcOpV9Td35HpWlIJC0iWEbdwVsu4inza83+HvK
Bgw4Tbja2SQJNuMfXsWfB8gH49xX+kNwPeszTJMbiVr0G/c4t2+g4b56oreYOhPSNQpgt4AMME+Y
5sRlUAhsvJYAkSutv6v59H0kvmFLWdrL+K9z4SdsqvN2Duz3bkVnAq7XUBuB28ihRaVZH9cmF11E
qY4B+v5awsNHa2JsRel9fuV24ZvDiBQ8YKvfdAd5azO0rsW2SBMYVWqsKjMDOjagM7M8HM4UrUS9
PlD0TrWlQLKhx8NkcBjui5KtCzWsV1FcFQrRQ3kdwfQpK6Hm2sVtxAwaQQepegbq3OLrMP0KXlvx
OyzAdqkY/ZjiZr84CNdc1aiPvgK9zOeJe3BdyX+8FzQ8sAyQb9XDozsy9FM04Dy62nnOwrHXAMdj
SpObjtjuS5Awpkg4AoE075GjvS2/MdrhKp4rMzjqmR2w/zOuL8qtxIimMCWDCXAlVYwf5ANxZRxJ
4Dlv8s2t9CbsZ5gVPC1zWvlDSjmRBlAKW6BrbysFICWUadlFsbD5YOt9OvLCn63VEO6qgGYBtlYQ
tvjnUtobMNGjFWwUiC02Nr9CYX6ZWhhSdkKVgXJWCi1YFo1yy1NHuYdW9hudw8OfLwUjU12YXULj
5cYwICLmoIplSzdqn4DOtD0PH1wv2Ur6oyrV2dOjkUpg0hSOSsZVt7sz/LJ24pY5DBRag/LRc6OM
uK+Y9SjZTy4ydRFSDnsyUqTuwX4TSoNg1RXyLwsYf9Z55A/gtBdQ2vQXWllXRwBgAGTgm79I0HUV
J2ay4mDjle4w5RLp2Kj2W8WJ5zQI0lHyvr/MJpocSf0bYuVZ70L2Y5cfs0/IyE1a8PNvNj7gsWij
WPvQWNoafGHI82FyUY4eZ7hgcl6DZI6n2PvYtnna5yVNL25rAzIoUsfxNo4SmI2u/igvpguNTjdA
8oxZbAcyE41LlE4kQFXsPn2AJ8SsqWBNhyNNr9mpdhhdN1X86fxHOjcuysts8pDTW2yJpYTdVTA4
fiwokKqyohrcGx3auOfczoYNEVoT1bgtTfePsnt7ieqNpp1j6/b9FbQue3KZ4r24niB9GXSgJJDN
m2ecoUycsXSOuXAyIrcs0m7/Elg5Thuyr3vgizg9L+fQo+py9tWUcd1SWlgbtXp4zr64+uyehkvL
gq2yHaQ1mjV7rUGccppIiZYqn0hsKfOV/PzeuE4Ejzj48CPSmOCR078YZzE0x2E/cFe0nnlitGd8
o19TP83K7v/sYCNW2oXAA5gDBGw/rpiwo6TlV6V0ckOIUuiWwL6+YhugeCgFoCIwrxnt/j1pv3xj
XOTDZl/biM3D9w0nHuVBtGUgZWzglI5G1JYF6NQauNsgGCrZqnFl/ftle1bwPm9SIPZoLgs2rfDT
y3jPYid/tN3nkSHHFsF6O229mdaAI4UC/91Rb5UjF1cKjVgzDKi3rO20xtBxw/xqgrAHMvTd/R53
9bFVAMR59ZodT9iTFfXKK1txkBPHVCraFMMrEhsF8O3dlW0qbw5yvkUpKB1BG/zNrRglomeQg+tD
ebyHDFR0ZfE9U3pmw/SG2MlD4qUG7AO14AqktjKLPtQY21xw71dRK2x9+JibthJkBCo3W3d7FCez
d/MFuos3j/q/E2cy5c5c5XugIkuAJdZMYmPmSiQCMDWbX/hcPPz/G3TVPINOqHijIBmZNTBl3VHY
Tnv3WDrCSrAj9P+d9kEz81Z7SmakcPsnxBOiXj3zTLhoRrEIabXhP0UXec+8Y6mcG4Qv68PNMpGy
wG9V5dUyuS+vgl/d6q6JYSHYL15B8mKBv57SAitCOuii/a5u14Jx4m4BWwTlSC/bOh5zZx4mOpHO
hmsNnphK3kIDcWPztUer0gc+PUTx1r0CpCzH4P/5+p5TUpQg/8LkKjjaV7IksLOAApAqCRpmY+ay
NsQPaGqRNJumMIHUSf9jAybHxP2rKEYHHuEPOpgyaERRQAxyrCPjgSWyqm4bLlEZ6N0NCUCoQObx
8II2Nri6hUU5wi3iNQWHF7HO4Vllb0QmnbLQ+B+6kDYo9giT3Nzls60iOEvzjueEQeERpfzPoQ0n
mKFcrfKGdnPYIHFxnPFJhlvgp0VFSZ74BJoFnQvC1ep7EN31eXDUgZjbUK/FX9DXcy/k8bSwHUcr
O8pbNjBWo3hUC9CnN7Nzielv0BgAOeJtI0+nx9BoO5nef/cv0VdP1RuIB0zwX1mQzlcyssKxTaML
1BzDlaGqoE5aEpDI+oq45nDRe/9vfX5TPGcmnh96XjQ9JL9uEaSGUZEjft8NUcqDtVn9zTXoUkHr
jvHFTJJn47BgBHHvNHIwFEKYv7cvmW91bc5rdFdW9f+/CrR/D6FbVZvINGFw0jZTn+E1KSwbvKPe
vCbi7T57J4ohwhjjKGxFNDtkRWtQfZf6Lnf8MpxwoHA2mB578+sDhLHViyGiUWHs3ZX5CXNghhRj
0LpMa/ERpXniWcDK8k5OMNfhlrulfD89+bNtvFqEHzhfaCev312W4LV3fFFGnLrmqg468AF1ubfU
y2ki4diokTC43NRSWOQ7/L55sdemTzh2e+8hDuprKV7dFft4/3s2YvDZdOSMt1hvU4RC6VKOEd5H
Wg/qk/kbOpHVw/9suS7D15HIuQ4HdDiJGiHQSJOOwHYC2WGDRUXj9MlUuPOhkH2lbcUa8XTqenqZ
NTZtAemuJYEsTG3xvnw6IFRoyMBTmiUAiOI+BXbLtAMVX9PuYXfuMhNJTlaS2RdEokmwSgcZC4XP
mUTEGY0x83Cpmstph7pyO1ZYG2qRJQTIAAtsrMDWIeb6Kjcd3q+r2dnsqQN9nrAvAAHIr6UkhoUY
7ei3cxgYGOTRgjOsyLit8jGSh/ji1rGVPk6EP3id7/0yLVWsprNe+O3kMx1Veu7BXHWZ4eTtDjj8
7LswYvoJ8NCzRZgXxyN9s4kBEcROLkIODfo0+YodKBAQSXxKPBNWs1+ibZ1VI9n6NtsuwILROGSg
+3PSviSnaySG7lNJ0n06E6cCihdBXQzbDdPjEkvVOnC/h0jn1NU9XdTjWGt/gPpO9xjroIa0nFDh
g05AU+jjebGIo6O7z/X/PnULFCxkSHJ+zywHkq9/qd+XPfgk49dl2GkRU+rN1fKhVTnLg8xCSuAI
gUr2Fr1JVm1pQKMfW/90moxt9ZKMX18OFFVbmp8NAqFNpU/0OJAse2ht4x0ZQGBvelkZK4dLRvU8
UFP9N7Uc0A23dA/TK6xKN0gmvEN8GQgH+IC20B8zm8gxPVa3wEIeG8K8KNSQ2EHspZ+RWHkO7iVh
57Cm/MspBwwjeBnUKKvj+tI3fDXpZBFqicgq/vFsseLpgD4Nfo1ATLgCbY9ImeN/W7XiBO8W1X12
F5KMGn9zHWZ8pJW8fDnrY5qFsVubkb8TKSC5QV/T/OA1TisAnlKV3z2uO11/RuVEOcUnKd6OIU44
rVO82mn3RbitXH30GaIvpmMTr7Bk+pzuzqsJCAveJzqP+IlSFwCRCgjAHdjjnBS0rdTLSbr7Zx5f
XXtAshLgkwGm4GUASeMi6faDRpCeiHcFBH4oQHKbjqRHKwOkQqlYCcbQFT0KJ3/U0Bz5yE4ssxrr
Ln1Uk08W+A34oZ+03yDzFNk06NpJ5GDur9tdkPYkbIcRnAbXVHfboAaPf0P1aMssMDiwfHdVBbI1
lwY86XyuDDBH5YpFSQnlz+YCX1pDDxYao51xsFevKpJFjRzymFp4enS7C9xGW2XRICLvHF6ZeDfn
w1GyWWe/GaS8bAJVjupSMNOZsZSEjxpkGKyNiq+m/dwayWBE3JtiJfRbAS5L+RA1lL3/UliCNSGH
qM8Utt64vVn/SMCtcbrAzaoYdYQ5rSmPgYdeo7EM4s/tTUUBhAVinNGN4+0WKaRCa1/myErai/jW
pdMso1ZmXk/WDnm0xf+EQbqK77I834FglJ0jcMALpQOKi+zHHwM8krU7FjLaaqZHwa8KmIUzUK/H
HpsAiWVOcN6pybIuUrDTHvTjwMwIy9T63jwIbW/r0R/ple7u2n+/9+RdhCCO9ODjwJ9GhstBLuKO
v8HkQD4YgxhZhsf6nzstK3xM36mU/0Md6mpcy8chPH/FrwGDcdKLQJR5Rj+FSJeWDDLpcfFNl6Z0
2tv7JCcTaivP4uCq9ItYo8ODjuzALxAxB8MvfDH3ALTP7xN7VYb6i9UYZYF04lVZ5istEVXkWsHI
GFhNeHyS0QaXlJHXv0uig20se3TebJ2IcaDrXQvcf9QeAHhomVdhbLDX0JR45/v/PHNgeAg2dNc9
fAPLLxLMokur1Wg/qgrwQRPoTHiMEA4VvNXqTegZsCTQgjO47EN/MYy52ICjjfkV53w9vYAmHzrF
VSzndqeAQdcMKY5VuGnPSdQyblojGWpWqLABX8cy+bOsDVDSX3xMC0Hd+UYRVl3C1cGbVMVMncyM
okJBpgvccjllDdZ5GwgumXahIf3XInJdjTQXZaaJ98hLCnh3Kqjt9ZxKwEdYJ86LFQJxnNq4CcXV
ZHyVrioUMm5dWjQkq6UHBofvBUpEuN8F+F9jX3xDMNVInK1QUGG/zQwBzBZjffOLh17uuLYYWaBM
8pjooFXYJHPjuZXeK/1nlm/OtXspj6hXakfU6SYrRuzWFZcvUE0mv/crt13TaEijJQkfPcaD4X2j
p04PKMZPAP0AOpF6yZYj6RpsX/kbGFI6BKvIu3YU5iqmS9Gt97qTyi89HGdAZdU1I7O4YyP1Lbv/
+jgZr5vrZpoRAc4ieCkNApU5ffuS6irx18RoeaLwCT+skX6uG+cVQm2MTtpk3o5IzU0McUDbE3gr
EdzhJcRVEG09QUFSXLViKLGIseWmvQn4v9ouFwU0MG947uhwb65AwGQ/xTZQrl7dXIQjlh9N9Kpk
ltiowFwZbyVxRjhVEB2Qb1BmtWdeflxcch63TncytnVKXO6FIamt7M5aQkPJJ5PK+rtkmHKxbUeQ
ZMCxB6uv9gSWo71PRypaUba89lpSKtRoWf1oa0bgsg5+V63zXmubxSU450NEqw5c2sXRXyrfpcfv
AosKqmcAbvwt2UosFn2IbJ2rMBdMFGYZf6LM0R+sbPhE2Lzz5rSmWuD69XAalfY4bLRgslQizqC/
GXNxASFACy62H5hNBnmYWQenG/USy7FFeEushDuzH+fBT0utdhDCDTuCPavagyubCNJ4b3hA17JA
rpzkXUcB6hsMMPSAN210SdeDLkJZ5w2TkCS0gA45L7x1HFuQFNfJRXMPLOxO+Ld9cjNgaMGujHT1
Sr4Ics1VbvTexbRWOejOyqnOuVjDMbnY728DsU+0OtcdsF3QObRk4kqD610atgLOEe0E/qNfED5A
s8M2pyXamqdwYSpxiTDHk1/ssP5Q8KTrPYnASeqEmzdFfL3DE+p7l07A3OZI1gGyQSOEwEUjlVZ2
oxTe8C0vRZxFB7o8WIUMiCJNXy+9uo596nn8/+fKRiTXSx91J2LpDK73g6RgBF6ax8NPrb9Y9xhH
OOzT4jdhFBs4r6WcWlGgE2hhS1xVcRJOca9vltCeE5MW8JBg1gRjTSmyYKL5SOKjgkLzj62IVUge
z99ZgyWwNP86nX2jZQImClQNn0vVEK6zfavPYfQP9kZJOuOqeHYKvqC7N6DDF/x90d9/5UxD+L0W
MWqZgqJHgsvmzcrsrCtvuGQb3RbOPL910EMTkEm0NSafW0d48mrC2TC0RbVwvgw6knY/N0aov4+g
3CxEf3EAZI7um8Tw0OdIJTaHwSvNl22NBynrc09CYUakSLpi9TGijoxC/uWknFSl1GK6E7ccXg2/
K62O68OxlibW7C8rD26thkqs5yIU4FhiB/4bzjv6Z0H1kuareIWV8pk4j2C61i+tKnMxcE+hvXzE
gcU8D94XQS3JV8ecjczv+4q/EcTX0IRcqsVQdbz50WRf1nYuIuTBiE/mmXeSWwKEjXLYMVBXQI9c
rHfvsr0USiRAb66KC+Eai+tViIhprmCAKIneOaYhpiSzvgNeQ7jqsBZENAH28AJ5Do/6wvSdDsp0
kXNXBt4WuP1AJyshcZvAqIj4qQzcZTbUGmWOmDywmTfltxA0yFsuWuNc9qh9gs/FHHUp3j3EhzYz
Iy0tITQj1MkPda65V0PUhWZGWs+65/v1x26+VcANvzEtj18/vLSpDTvpoJc1bwnlYCGLEP13wUYE
ZTQsLeG0oq3kwXDYTtGX3wQwAEzs5ADVkb/0bnj6GZlfwkb2V0A57z7iQTB4Aalpttdo3emH8fNr
hC2cGSwxkIfoivLuM0AJjc+3hHUYkWIWl5jAERehkn+H7uAnTUh24gh822oq22VUoqOO6jUQ5Idp
WuhXr+0eWmZlDDASql1/E0f/GQoa0wdj2kCqA2J8P0mwpqyyNFzxeln6/Wab6/4nywPVcVY1wwGc
FFiDqUmfXLwG2xPsU8KkjUteJOmFvPpcp0YtsvGMHd8HB5qfrQcYFI0W7VeZJSvmJuHgV1r+XFXH
2UJ0TgzMxBaTP8+7PUZ5RGbNiMUKZK+z7tdYbV2W1KSHZJ/0j1q61hnmtbH07fW9/8NUuDnHS7jm
GH3t+hT/4bW+vGjMhYhB/LnDQUWkXIkWOa10bUzlVYYsYd7uQa1cxJVCSQHjsuWKEPWQgEdeOw97
TOsp4bJ+mQVqWQwQLrV87WqiqC+UZih1tCemsCrGuWynmP6OaofbQXYo0zogA3gz7af6ymChTJuO
9I5ykrNwZg/frT/xhmnw1tLNo+iR/hOomHg5cvsqXi7bpk/InhKwmgX8qI7tCok36M0AbvhKQSdA
7wLsHg3E1aAKHjCCw3AnMd2JupxyXU9mecuVOwoQtsK4RJbQCxUHickGYxyEstbUukVkoTwg/oHi
MptnFLerGDVMEk68RMuOtFgi3lBzTcTCiNdRjkb7ATFzg/RlvwmsmhQeSpUTdNBImpVbSi2Rlw4C
6FJIKi1eO3qYdesE4LwePr3oWClxQ9pqJymtr/HLa4EAJn90bv3BOrSTotAjs0fdnuQ9Ty1r7Vmk
vDlyRS0RC8b/88zJcX77RvjhMq0738dv+8rCncBMNSzlFlm8hfGaw7opfRxXAcDgJfNGoMfaIH4J
3z3tscTa9eICnFUKH+z43al/VpjObkc/UoBPS92RfMvlT/uGkUDaAXMloL1yYfdXcRE92TyRzuW3
xsjz7P1iSNgkj0zWpof1wEelKl5c4H7KI3JwRR8vsm28YSGEpvW5pUW7ii1dZE7ZEnbGTSNhcEqh
x7Wo4uaXyvoMPwGzno5lw2ZOlt1t0VjvDMW0VnhP1+u1DJ31W/s/887tBhhwQqtrGeZnUGtrotiS
epcuIjED4EktKSqGxDlDQVGKxZWPI4n56gc6/DcNKtBrITEf+4KW804KN0jxKYKOH3hgICZTwQJX
u/wBODdRPw8qhALx9v2pvVNFGeTjhWSUQs1d49wZiZVjesaLnoHn1JEncSwCbV7/dwy+7fAMtuil
d1pc1jf21w/DgqdvK7GwqEkQkMGOZDzYKRsyfPlpRI6Q+EegBEH53/ZwcjBLTxZnNLiCPJ0FaEDd
J40Mdf72hrcV6HjaS2OH/CwtZS77cczSiEPK53clBoMFdkf4U4jfQhcj1N+6HREAd8h0jdfcRrPF
nfG1Gz658wROKYOF4ziNr7onfG3DoblHksEwZY22ZpXya5J2zw8mtfnup6vAFZvD+UGjgCDFJvtK
kFv3VyxaNEqxX9DuTs3yBBtSMEW/rVxL9TFR75/8ODn+XkgHgw52v5KKRYz97bE2VmS7ZTXvunPI
fF9JXe522rag+Ik4tpIr245c+H8h7h2xh3mk0YPO3Xa3Sxvv+1HlC8EG/YWOa6BUmFi5GRGtnvSv
WpgGtWevJ/Np6l2KuwroR3aTd09Ws7rhCB6NK3tKlROjL60I8DsnJ4gF7r9HXuGLtABzqQIsr42j
F/GP6VG5BoGVHouzMnVqqLvdlRp7cX2LoFnmDiwY5ID3UejqVCVGmuRG7NgdcSANARijx3PTF0TL
V012kiYN6ZZXfKx4SrAMe2X0hYJGFGWcxy12Ln2Pyn0E/THXkuMOWBjUJT/34UxHiXjw1vHtwNgi
9Tgv65bgU05Kx1PJ5VZjQZF+digrnyp/0c+fqzzEVoVA9sQvf/FY3DZXKrqyIxnUD04GQoCFgl2R
Vp3FuPykP3sGVT7Apzf2d5M9grNnUlhLVnog2i5xCYFzdilSxiDn42xH1rUwdlw5CTlQOKlyBS/X
G2+qfPD6NU245aKs8n8PiG2dIHivgRT1EoJVOvAUdelCSMDgLEwzsHZZSLV6VBNxfi+D8d+mFn1W
KH8AOBE8W9C6R6dEdrD11FOobedGJz7486OhYufiZzbgYVScA93F3nJgS31PXD7bKVItKt2AMAPB
6mTLPPvej5T4tbFGLY+7DElFAGHIAfbi8VadmTdmYUzVYzV5xfLILhgSEN66G+ZXDBsHvBAwgIw9
goTtgYc+/98bDsRyeh+UbWAkRIMGygABdZqoWAkUDCuEfuuGRB+ot2gZaI6a9FWo+0bTO49NRgoX
eTxueVne8ewIKnVyXQmjzjBTP0r8oXbNFSoo6tDwaqtMHkzQEOcV2/irO2EFgnBSnv7y/Dc3DcDs
MvF/QBG3eKExJX0xQ4IPYFv6kwcn4AmNjD7MXL5hAym6P1X6n4EXmEfVQuexT86ktKIW9Hzq484P
Stc8rs7bPvqEPVdMsp402Xm6dCFkppqvVwQGE8cn4ShdwwYkjyht47iPh684cBcYy5nE7iLXx4ko
+aEMC9EIVQI8EfXbUJJ8jElR5YR7Sjr+zhoepEYiRWgjS4i6//v/ynMr32zLff/KaW3qJrqyxUwj
mzgJaOPzx3i41EK4nMkupLQMRIjkvKPWa0lhO9U6nfOMBAehoOvvFeSaogS8QDUNMF0z+HTqwfxA
Dr/P6cQQhKIeapOvm0HDTNzKESnYYC09sorUUAycpOtOSaNGlWWOMdE9rD/h7U1+wBZWC1hidfi4
EjqF5uVwv6nfHC74oyRzrM2CtfZ/PLpYLgd2+kwV/Nn9G+hclbV8JYQyuai1Svd15AY5YnHHYT63
ShkcgHE96TXtbyjUdGbo8QFO/B4jsh47wpdCLFjs19d1JhKZW7lt60/dykNok2/KUAhBkJwuz2gw
jCiNDH2HTdm0QrQyu1hKR5DtOpwTwl31OJNcY7X2VNldxl2Y/dqtmJ2NAxmxuJgf9wWrsvgKiMNn
iOL6UQSJkfrAKx39Ma1jqKB91ASEA0zJEpZL+B0b44WVr8Hip2rEV7wEjAVAWnfj66nnWDEb3IIm
nZTxds9SDzy99QqgG5rsLbbvd4eSksmXO5q9M/cUGjKesFU8VF99oRTyPJK0cCuaBy+rL5xAkjzs
IRirKd3rmj6yn6OFIohh3Y64C2d7o5Q2kQrQeaohuGZAxZX9YoVoawuZ+aM/OFaegNmCbgcqZxEa
cJFDVPywwjp2E1lfo1HLlaZ9TVvMdDYBqu4uYdjq5pgaEHMUUMFNXu1T+kur6bhWqJL2bnGf3sHl
PDZVs+ezOrLZ68R1KwT1Eblrk/KwFWcQt6teMOKTVq3uquTKgYAO9woMIU7lMWjDnWKntOpZDZfK
b6cDturZuDB2Ln90HbkI91/Io5U84xmfl2cTZAczy2JxYKGJxQyHmvkdTOLx4usi2Q9cl9UG9nv/
KJAM4euwzaUwr8fMoJCkyYrXji5q5B2I6v43FF8A06BH1pDyTk5SnHGiIlOMmVq5TfnZBIveOGbs
gU7AfMyuUJzRz3Nvn4pCktnuWTA/bcFhP9CNJ/Qy1xh9KObmgLx88Agy2WTnS2Ih7tNuW6D0zN7n
8ag3GqX3qDT/1HzO7lFk6iiIhotBuc5HwnDukCT7It2xSW9sTg/9rrDYdwQw3RYHlrdhQtuoEPpU
QS2xnLI6CcskN9YWq6/sSiISaTjnAEjUB5/SwT/QUPO1wgBmkgX3F/RHekt7H+W6xSjEd0G6Mh6r
o5fJKSaB0nXc069hySi542fkgfuM+sDENua2G+Z32qv3sZhJTqjPzRRbtgkWoNJZQvu19CUTpo9I
OOkcLacL+ZYGQrKpSY0RmZ8ijaW3aGBB6jeCfeh0zgLNKc9msq3gEuR1DDEj06+m3hv7GiPJUeBh
GYphPkN0FDj3/swoAnyWEhpL6gArc5J2zvTJg0vbh4PRK4B9QOaEx6OraapLYVNUytnut4XF63mU
MaInvsJ+0H2Oc2gPc0lUV2EPcqxCcV6IIMkeDQfv0by0Ea1Q+v4WjmaMWViVtfeScDnA0b3SRD+D
eeJ70DlzMgPeFk/HxKYdhSon3KGWmItfihsbVh6vOFUEk+YgAMJnTdu02au/FmgC19p08BEE8/ki
4wDQdHHpnD3jGTCZB7M1tsMjHsB9pFK/o6VocEO4bDidiKHQ3GTqXbEr97kVQjlHdXfDH6PWaWLF
Lst1To7DXqLvmNB8RKGSlOZMOjBEnXr9xS2Rve1T+2DQ1iYvjTOS6nsKeyr+6wtNpE9pQ5oFM40S
BfkjoP0k1fkliSWkJoL6jGzsryrhfrFduUsMPKxrOiJ2AKNh363xLY9sGBSeZsYNjBRmXu4kg34A
Vjw44BZrqRfnAePhy7LlYQnlbmtk15WLKCXn2UDzYk2QjhMpA2JEe9g8JwxgmrOnsBrlOS1T/HY1
xJIzCCYS4m0LGRWwo5PBJaih2fXkhAJHEkYDXkOnGWN+TVj67I0bYouCXw0hRKADqqbB9fXLaSrj
mYFr0eL8lPNlPHewJ3oV8v6NMawTItSwNElxFZZL3RGUqQ+fCqL0WNBotVQ9pXHNWZ8+X3dbc7ji
umZBsy7NKxDyaUlK1JqxrPsgTCNAs4+DTMVYO8I9znOvk4hwUpT+u+KbZtj1anH3NNjewUUlPEft
mD9RyJrXgxPFzqT5Gzw/JHj+dZ7th8a0H7vOgfzeqm4kv+Dt70PfujPWg2J8nNSfG+nAjmfwzi64
VQt4EjMaxg3O7aQ62l1mz8FjYDwOpXToByNMc/DuYrBjof9OaezCOyE3y4w4CZWYu2fk5q7aSRuM
VUx9zyVRfaYY+3aVjoTGdVk1wOsHTzU6ySbQH6idIYe+K6vUvPr5sxDUEy5CH8skBxMoQN2EWU+T
lwAVgaszwaHcUBvoP/zyxxe2fXkdxBRIdVZl3qcBvftDW3QJ4K1Szy+pC+ytbYMmheq0qY3nih4f
iPfO4LIsLfTCRbPpv5lPRLoQ35DQcz8OKdejUWNllPQn9j6LlNOP51qMlGe7MjpZYaEWU4eRSVvs
eRSaS4mNShG3Y7qVRMQeEn74hX8Ya/dbTisZSJuwZocyvJS6UPDu34ri6QG/i3hQmBpoGWwzXhVa
q50JfkLbHzfz9fPjSl7ODns5z0NBEjZdjNw1f/swtqi1GkdXYNdi4hwapIJ5jVIlYYTVnm5p10gS
8kjJqH5kinETWX6UedGwqZ0x1bokWKjxN+NtRtppfd7sU0+DuQzrNcHx7HuMuUcIGY8EzWLTnM/l
LbrmdyusPe3ndzhB0qL576HJX0Y2lnd+96/cbvN6iVrrUMCz5ax29qILqwaEjYR+BHVLcjX/POWj
T9SF1jyxqtA58R/84Ry50Ac/L5Qey2UID9f/r7isCylvmQJNqVn3WlHz8dggnnEQGZibDctYroU7
ey1IcqJQqYlmycrcbnldeNU6vojyOotxA9E4akOzmRLdI0BEgcmhmnBfqeZBtaLOBVTg7Cr1Hk8W
g7WlJBJyGZb1H7qdS0a5ojbtk0eWi7gjTKS5RJkRkPq+kZs2aMIjDHeNhB2GrUnFirN5mUU7MWTl
SJ6HU+KDlQBTKKUr6VzRm8NkiVo4z/VAZ3zHtKvJLKxIT3tb2ik7gyCyiupPTJLF3r/QQLAz+NAw
n+R56Anefs9r2MmSABt7Vnszf3D9z7NHiV93I48DPUcBEg2Cepk8SzA4Cf2JyIp+2MJMFmlMp95s
L49XWD5JjlmydMj7s3bD8fXupB2L84kimaICYiDENHOHU2WChqhsdfSZ5NBbEqgkvTuP+u7qPP3e
ZbniM4KO96eGTgHyH6whUA+rtv025DabjJDTTnJCAyToT8Vty9Fvl87qRYL4pnTEC18kk3fkflCd
PLPVFMBL2EXiclfsPT2qaT9gs+A49tF1Q7CtsBP/jHwKKBn1VfgWW1VCoXAWIe1GrdlQRyTONxGZ
TKZHPDcIZo4UkgvFi76WyVbA3N5/wP37KT7CQ9Zk+WbVN9FVEYgwJo+lOtzhn/D3r7a6gnRHMxyi
v8X2bm4k+o52R5lh+4Q0c0LPYgsdeFxLN0dHY08OaPJGrHD6zCZN2aG2R38RD8k79abk/c753/AT
/n1ruOYPXqbNDUpVatmJcDpHCdUjgWZDzaK1ZkgCSxqFsSS9S/h1u3mE+r0q9Pq4EbWhqC4E7X8m
VFLYkMG1IW+gyVWPjOqTDZkbjlnAwXyTLm+F/9C5VDRS7C8RW1gIV+i3LQsi4pdFY4uGET1TYdZS
u7nefWR3u3GtpyjEPufUNMfPLyzd9NB+LfBQwQZzLbNgpVAkEUx5bDB/7jtwPGUjW7k27YEh18Uo
AutcJ3SDS2wveHWNPe2NarI9koye5rGAWJaHNvFGw/Xnwaza7U6nK4gx4L7H3t1khiPuFvzDpWZ5
XPP1EreVZDCs+EO6fgwTafw9tcaQXzXxe18GgaTj8zlzlfd4/ON0+zitIJi6TbRYfhTAqY3yu/qW
bUuZ2adwPikp6sY+sSs6zraqAjCVStJ5ZIYd+p7cE6m7W4FMNpqbDB4NvyjJg1znXO7mljy3aGGH
5g45EVTznEL8QpigwqJZri5g2hgHyfx//zz7++MYq7VFIL7JWeeUs27uQ340NU8HrXCvrbMLt9Oz
eYMpCw7t54rvcc9Bqb4PJOG+Ytj03aRRsq61F5wSrwT0Zume+0mp17uu/NZcj4nemd2MWHR2EC9m
i91yD7AbHWubBr8og+/jkS0DP0fqi17yOxkDZ1VmQtR6rYA2nRMw6UlzRMSPdsHnOp5Bk9yKJ41h
2cYDHML67Qk0K1lfdyLxw+s2PQLHTDC6CbXm5bNufwXT4ikGju5Y+/vHjLCSg0d3a4OMMmwOBWBs
4lOTjG0ypuI2mlZGMWgkNEryN9x5KJXfjJqA6dC0aU6yTDs4Y3XuXpAk0uye0iTADCp+Al5i6CvD
hLgPwhUWJ35+3b4sf5oHwl/B5F0Slo97F9MhPqfyg57QIziiqFSBOm4d7Smawe/J88pb4u6pNa5W
TVqvQQvSBc8GayEVvbOSNw6hrGg5OEzKQUi6HGPKEc8lbQNSat0pQP5Y7gfP6wqlhd0IZNuR
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
