// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:17 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/sr_ram_adc_iddr_synth_1/sr_ram_adc_iddr_stub.v
// Design      : sr_ram_adc_iddr
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.2" *)
module sr_ram_adc_iddr(D, CLK, CE, SCLR, SSET, Q)
/* synthesis syn_black_box black_box_pad_pin="D[1:0],CLK,CE,SCLR,SSET,Q[1:0]" */;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  output [1:0]Q;
endmodule
