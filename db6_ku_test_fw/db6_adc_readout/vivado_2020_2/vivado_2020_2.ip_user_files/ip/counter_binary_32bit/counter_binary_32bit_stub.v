// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Sat Apr  3 01:45:55 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.runs/counter_binary_32bit_synth_1/counter_binary_32bit_stub.v
// Design      : counter_binary_32bit
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *)
module counter_binary_32bit(CLK, CE, SCLR, LOAD, L, Q)
/* synthesis syn_black_box black_box_pad_pin="CLK,CE,SCLR,LOAD,L[31:0],Q[31:0]" */;
  input CLK;
  input CE;
  input SCLR;
  input LOAD;
  input [31:0]L;
  output [31:0]Q;
endmodule
