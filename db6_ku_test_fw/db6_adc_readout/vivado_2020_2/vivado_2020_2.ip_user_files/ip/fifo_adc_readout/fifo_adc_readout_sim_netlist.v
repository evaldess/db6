// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:27 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/fifo_adc_readout_synth_1/fifo_adc_readout_sim_netlist.v
// Design      : fifo_adc_readout
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "fifo_adc_readout,fifo_generator_v13_2_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "fifo_generator_v13_2_5,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module fifo_adc_readout
   (srst,
    wr_clk,
    rd_clk,
    din,
    wr_en,
    rd_en,
    injectdbiterr,
    injectsbiterr,
    dout,
    full,
    wr_ack,
    overflow,
    empty,
    valid,
    underflow,
    sbiterr,
    dbiterr,
    wr_rst_busy,
    rd_rst_busy);
  input srst;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 write_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME write_clk, FREQ_HZ 40000000, PHASE 0.000, INSERT_VIP 0" *) input wr_clk;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 read_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME read_clk, FREQ_HZ 40000000, PHASE 0.000, INSERT_VIP 0" *) input rd_clk;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_DATA" *) input [13:0]din;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_EN" *) input wr_en;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_EN" *) input rd_en;
  input injectdbiterr;
  input injectsbiterr;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_DATA" *) output [13:0]dout;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE FULL" *) output full;
  output wr_ack;
  output overflow;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ EMPTY" *) output empty;
  output valid;
  output underflow;
  output sbiterr;
  output dbiterr;
  output wr_rst_busy;
  output rd_rst_busy;

  wire dbiterr;
  wire [13:0]din;
  wire [13:0]dout;
  wire empty;
  wire full;
  wire injectdbiterr;
  wire injectsbiterr;
  wire overflow;
  wire rd_clk;
  wire rd_en;
  wire rd_rst_busy;
  wire sbiterr;
  wire srst;
  wire underflow;
  wire valid;
  wire wr_ack;
  wire wr_clk;
  wire wr_en;
  wire wr_rst_busy;
  wire NLW_U0_almost_empty_UNCONNECTED;
  wire NLW_U0_almost_full_UNCONNECTED;
  wire NLW_U0_axi_ar_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_ar_overflow_UNCONNECTED;
  wire NLW_U0_axi_ar_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_ar_prog_full_UNCONNECTED;
  wire NLW_U0_axi_ar_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_ar_underflow_UNCONNECTED;
  wire NLW_U0_axi_aw_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_aw_overflow_UNCONNECTED;
  wire NLW_U0_axi_aw_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_aw_prog_full_UNCONNECTED;
  wire NLW_U0_axi_aw_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_aw_underflow_UNCONNECTED;
  wire NLW_U0_axi_b_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_b_overflow_UNCONNECTED;
  wire NLW_U0_axi_b_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_b_prog_full_UNCONNECTED;
  wire NLW_U0_axi_b_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_b_underflow_UNCONNECTED;
  wire NLW_U0_axi_r_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_r_overflow_UNCONNECTED;
  wire NLW_U0_axi_r_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_r_prog_full_UNCONNECTED;
  wire NLW_U0_axi_r_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_r_underflow_UNCONNECTED;
  wire NLW_U0_axi_w_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_w_overflow_UNCONNECTED;
  wire NLW_U0_axi_w_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_w_prog_full_UNCONNECTED;
  wire NLW_U0_axi_w_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_w_underflow_UNCONNECTED;
  wire NLW_U0_axis_dbiterr_UNCONNECTED;
  wire NLW_U0_axis_overflow_UNCONNECTED;
  wire NLW_U0_axis_prog_empty_UNCONNECTED;
  wire NLW_U0_axis_prog_full_UNCONNECTED;
  wire NLW_U0_axis_sbiterr_UNCONNECTED;
  wire NLW_U0_axis_underflow_UNCONNECTED;
  wire NLW_U0_m_axi_arvalid_UNCONNECTED;
  wire NLW_U0_m_axi_awvalid_UNCONNECTED;
  wire NLW_U0_m_axi_bready_UNCONNECTED;
  wire NLW_U0_m_axi_rready_UNCONNECTED;
  wire NLW_U0_m_axi_wlast_UNCONNECTED;
  wire NLW_U0_m_axi_wvalid_UNCONNECTED;
  wire NLW_U0_m_axis_tlast_UNCONNECTED;
  wire NLW_U0_m_axis_tvalid_UNCONNECTED;
  wire NLW_U0_prog_empty_UNCONNECTED;
  wire NLW_U0_prog_full_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_s_axis_tready_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_wr_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_wr_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_wr_data_count_UNCONNECTED;
  wire [8:0]NLW_U0_data_count_UNCONNECTED;
  wire [31:0]NLW_U0_m_axi_araddr_UNCONNECTED;
  wire [1:0]NLW_U0_m_axi_arburst_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arcache_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_arid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_arlen_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_arlock_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_arprot_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arqos_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arregion_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_arsize_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_aruser_UNCONNECTED;
  wire [31:0]NLW_U0_m_axi_awaddr_UNCONNECTED;
  wire [1:0]NLW_U0_m_axi_awburst_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awcache_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_awlen_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awlock_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_awprot_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awqos_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awregion_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_awsize_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awuser_UNCONNECTED;
  wire [63:0]NLW_U0_m_axi_wdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_wid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_wstrb_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_wuser_UNCONNECTED;
  wire [7:0]NLW_U0_m_axis_tdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tdest_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tid_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tkeep_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tstrb_UNCONNECTED;
  wire [3:0]NLW_U0_m_axis_tuser_UNCONNECTED;
  wire [8:0]NLW_U0_rd_data_count_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_buser_UNCONNECTED;
  wire [63:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_ruser_UNCONNECTED;
  wire [8:0]NLW_U0_wr_data_count_UNCONNECTED;

  (* C_ADD_NGC_CONSTRAINT = "0" *) 
  (* C_APPLICATION_TYPE_AXIS = "0" *) 
  (* C_APPLICATION_TYPE_RACH = "0" *) 
  (* C_APPLICATION_TYPE_RDCH = "0" *) 
  (* C_APPLICATION_TYPE_WACH = "0" *) 
  (* C_APPLICATION_TYPE_WDCH = "0" *) 
  (* C_APPLICATION_TYPE_WRCH = "0" *) 
  (* C_AXIS_TDATA_WIDTH = "8" *) 
  (* C_AXIS_TDEST_WIDTH = "1" *) 
  (* C_AXIS_TID_WIDTH = "1" *) 
  (* C_AXIS_TKEEP_WIDTH = "1" *) 
  (* C_AXIS_TSTRB_WIDTH = "1" *) 
  (* C_AXIS_TUSER_WIDTH = "4" *) 
  (* C_AXIS_TYPE = "0" *) 
  (* C_AXI_ADDR_WIDTH = "32" *) 
  (* C_AXI_ARUSER_WIDTH = "1" *) 
  (* C_AXI_AWUSER_WIDTH = "1" *) 
  (* C_AXI_BUSER_WIDTH = "1" *) 
  (* C_AXI_DATA_WIDTH = "64" *) 
  (* C_AXI_ID_WIDTH = "1" *) 
  (* C_AXI_LEN_WIDTH = "8" *) 
  (* C_AXI_LOCK_WIDTH = "1" *) 
  (* C_AXI_RUSER_WIDTH = "1" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_AXI_WUSER_WIDTH = "1" *) 
  (* C_COMMON_CLOCK = "0" *) 
  (* C_COUNT_TYPE = "0" *) 
  (* C_DATA_COUNT_WIDTH = "9" *) 
  (* C_DEFAULT_VALUE = "BlankString" *) 
  (* C_DIN_WIDTH = "14" *) 
  (* C_DIN_WIDTH_AXIS = "1" *) 
  (* C_DIN_WIDTH_RACH = "32" *) 
  (* C_DIN_WIDTH_RDCH = "64" *) 
  (* C_DIN_WIDTH_WACH = "1" *) 
  (* C_DIN_WIDTH_WDCH = "64" *) 
  (* C_DIN_WIDTH_WRCH = "2" *) 
  (* C_DOUT_RST_VAL = "0" *) 
  (* C_DOUT_WIDTH = "14" *) 
  (* C_ENABLE_RLOCS = "0" *) 
  (* C_ENABLE_RST_SYNC = "1" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_ERROR_INJECTION_TYPE = "3" *) 
  (* C_ERROR_INJECTION_TYPE_AXIS = "0" *) 
  (* C_ERROR_INJECTION_TYPE_RACH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_RDCH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WACH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WDCH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WRCH = "0" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_FULL_FLAGS_RST_VAL = "0" *) 
  (* C_HAS_ALMOST_EMPTY = "0" *) 
  (* C_HAS_ALMOST_FULL = "0" *) 
  (* C_HAS_AXIS_TDATA = "1" *) 
  (* C_HAS_AXIS_TDEST = "0" *) 
  (* C_HAS_AXIS_TID = "0" *) 
  (* C_HAS_AXIS_TKEEP = "0" *) 
  (* C_HAS_AXIS_TLAST = "0" *) 
  (* C_HAS_AXIS_TREADY = "1" *) 
  (* C_HAS_AXIS_TSTRB = "0" *) 
  (* C_HAS_AXIS_TUSER = "1" *) 
  (* C_HAS_AXI_ARUSER = "0" *) 
  (* C_HAS_AXI_AWUSER = "0" *) 
  (* C_HAS_AXI_BUSER = "0" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_AXI_RD_CHANNEL = "1" *) 
  (* C_HAS_AXI_RUSER = "0" *) 
  (* C_HAS_AXI_WR_CHANNEL = "1" *) 
  (* C_HAS_AXI_WUSER = "0" *) 
  (* C_HAS_BACKUP = "0" *) 
  (* C_HAS_DATA_COUNT = "0" *) 
  (* C_HAS_DATA_COUNTS_AXIS = "0" *) 
  (* C_HAS_DATA_COUNTS_RACH = "0" *) 
  (* C_HAS_DATA_COUNTS_RDCH = "0" *) 
  (* C_HAS_DATA_COUNTS_WACH = "0" *) 
  (* C_HAS_DATA_COUNTS_WDCH = "0" *) 
  (* C_HAS_DATA_COUNTS_WRCH = "0" *) 
  (* C_HAS_INT_CLK = "0" *) 
  (* C_HAS_MASTER_CE = "0" *) 
  (* C_HAS_MEMINIT_FILE = "0" *) 
  (* C_HAS_OVERFLOW = "1" *) 
  (* C_HAS_PROG_FLAGS_AXIS = "0" *) 
  (* C_HAS_PROG_FLAGS_RACH = "0" *) 
  (* C_HAS_PROG_FLAGS_RDCH = "0" *) 
  (* C_HAS_PROG_FLAGS_WACH = "0" *) 
  (* C_HAS_PROG_FLAGS_WDCH = "0" *) 
  (* C_HAS_PROG_FLAGS_WRCH = "0" *) 
  (* C_HAS_RD_DATA_COUNT = "0" *) 
  (* C_HAS_RD_RST = "0" *) 
  (* C_HAS_RST = "0" *) 
  (* C_HAS_SLAVE_CE = "0" *) 
  (* C_HAS_SRST = "1" *) 
  (* C_HAS_UNDERFLOW = "1" *) 
  (* C_HAS_VALID = "1" *) 
  (* C_HAS_WR_ACK = "1" *) 
  (* C_HAS_WR_DATA_COUNT = "0" *) 
  (* C_HAS_WR_RST = "0" *) 
  (* C_IMPLEMENTATION_TYPE = "6" *) 
  (* C_IMPLEMENTATION_TYPE_AXIS = "1" *) 
  (* C_IMPLEMENTATION_TYPE_RACH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_RDCH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WACH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WDCH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WRCH = "1" *) 
  (* C_INIT_WR_PNTR_VAL = "0" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_MEMORY_TYPE = "4" *) 
  (* C_MIF_FILE_NAME = "BlankString" *) 
  (* C_MSGON_VAL = "1" *) 
  (* C_OPTIMIZATION_MODE = "0" *) 
  (* C_OVERFLOW_LOW = "0" *) 
  (* C_POWER_SAVING_MODE = "0" *) 
  (* C_PRELOAD_LATENCY = "0" *) 
  (* C_PRELOAD_REGS = "1" *) 
  (* C_PRIM_FIFO_TYPE = "512x72" *) 
  (* C_PRIM_FIFO_TYPE_AXIS = "1kx18" *) 
  (* C_PRIM_FIFO_TYPE_RACH = "512x36" *) 
  (* C_PRIM_FIFO_TYPE_RDCH = "512x72" *) 
  (* C_PRIM_FIFO_TYPE_WACH = "512x36" *) 
  (* C_PRIM_FIFO_TYPE_WDCH = "512x72" *) 
  (* C_PRIM_FIFO_TYPE_WRCH = "512x36" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL = "6" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_NEGATE_VAL = "7" *) 
  (* C_PROG_EMPTY_TYPE = "0" *) 
  (* C_PROG_EMPTY_TYPE_AXIS = "0" *) 
  (* C_PROG_EMPTY_TYPE_RACH = "0" *) 
  (* C_PROG_EMPTY_TYPE_RDCH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WACH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WDCH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WRCH = "0" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL = "511" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_AXIS = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_RACH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_RDCH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WACH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WDCH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WRCH = "1023" *) 
  (* C_PROG_FULL_THRESH_NEGATE_VAL = "510" *) 
  (* C_PROG_FULL_TYPE = "0" *) 
  (* C_PROG_FULL_TYPE_AXIS = "0" *) 
  (* C_PROG_FULL_TYPE_RACH = "0" *) 
  (* C_PROG_FULL_TYPE_RDCH = "0" *) 
  (* C_PROG_FULL_TYPE_WACH = "0" *) 
  (* C_PROG_FULL_TYPE_WDCH = "0" *) 
  (* C_PROG_FULL_TYPE_WRCH = "0" *) 
  (* C_RACH_TYPE = "0" *) 
  (* C_RDCH_TYPE = "0" *) 
  (* C_RD_DATA_COUNT_WIDTH = "9" *) 
  (* C_RD_DEPTH = "512" *) 
  (* C_RD_FREQ = "40" *) 
  (* C_RD_PNTR_WIDTH = "9" *) 
  (* C_REG_SLICE_MODE_AXIS = "0" *) 
  (* C_REG_SLICE_MODE_RACH = "0" *) 
  (* C_REG_SLICE_MODE_RDCH = "0" *) 
  (* C_REG_SLICE_MODE_WACH = "0" *) 
  (* C_REG_SLICE_MODE_WDCH = "0" *) 
  (* C_REG_SLICE_MODE_WRCH = "0" *) 
  (* C_SELECT_XPM = "0" *) 
  (* C_SYNCHRONIZER_STAGE = "2" *) 
  (* C_UNDERFLOW_LOW = "0" *) 
  (* C_USE_COMMON_OVERFLOW = "0" *) 
  (* C_USE_COMMON_UNDERFLOW = "0" *) 
  (* C_USE_DEFAULT_SETTINGS = "0" *) 
  (* C_USE_DOUT_RST = "1" *) 
  (* C_USE_ECC = "1" *) 
  (* C_USE_ECC_AXIS = "0" *) 
  (* C_USE_ECC_RACH = "0" *) 
  (* C_USE_ECC_RDCH = "0" *) 
  (* C_USE_ECC_WACH = "0" *) 
  (* C_USE_ECC_WDCH = "0" *) 
  (* C_USE_ECC_WRCH = "0" *) 
  (* C_USE_EMBEDDED_REG = "1" *) 
  (* C_USE_FIFO16_FLAGS = "0" *) 
  (* C_USE_FWFT_DATA_COUNT = "0" *) 
  (* C_USE_PIPELINE_REG = "1" *) 
  (* C_VALID_LOW = "0" *) 
  (* C_WACH_TYPE = "0" *) 
  (* C_WDCH_TYPE = "0" *) 
  (* C_WRCH_TYPE = "0" *) 
  (* C_WR_ACK_LOW = "0" *) 
  (* C_WR_DATA_COUNT_WIDTH = "9" *) 
  (* C_WR_DEPTH = "512" *) 
  (* C_WR_DEPTH_AXIS = "1024" *) 
  (* C_WR_DEPTH_RACH = "16" *) 
  (* C_WR_DEPTH_RDCH = "1024" *) 
  (* C_WR_DEPTH_WACH = "16" *) 
  (* C_WR_DEPTH_WDCH = "1024" *) 
  (* C_WR_DEPTH_WRCH = "16" *) 
  (* C_WR_FREQ = "40" *) 
  (* C_WR_PNTR_WIDTH = "9" *) 
  (* C_WR_PNTR_WIDTH_AXIS = "10" *) 
  (* C_WR_PNTR_WIDTH_RACH = "4" *) 
  (* C_WR_PNTR_WIDTH_RDCH = "10" *) 
  (* C_WR_PNTR_WIDTH_WACH = "4" *) 
  (* C_WR_PNTR_WIDTH_WDCH = "10" *) 
  (* C_WR_PNTR_WIDTH_WRCH = "4" *) 
  (* C_WR_RESPONSE_LATENCY = "1" *) 
  (* is_du_within_envelope = "true" *) 
  fifo_adc_readout_fifo_generator_v13_2_5 U0
       (.almost_empty(NLW_U0_almost_empty_UNCONNECTED),
        .almost_full(NLW_U0_almost_full_UNCONNECTED),
        .axi_ar_data_count(NLW_U0_axi_ar_data_count_UNCONNECTED[4:0]),
        .axi_ar_dbiterr(NLW_U0_axi_ar_dbiterr_UNCONNECTED),
        .axi_ar_injectdbiterr(1'b0),
        .axi_ar_injectsbiterr(1'b0),
        .axi_ar_overflow(NLW_U0_axi_ar_overflow_UNCONNECTED),
        .axi_ar_prog_empty(NLW_U0_axi_ar_prog_empty_UNCONNECTED),
        .axi_ar_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_ar_prog_full(NLW_U0_axi_ar_prog_full_UNCONNECTED),
        .axi_ar_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_ar_rd_data_count(NLW_U0_axi_ar_rd_data_count_UNCONNECTED[4:0]),
        .axi_ar_sbiterr(NLW_U0_axi_ar_sbiterr_UNCONNECTED),
        .axi_ar_underflow(NLW_U0_axi_ar_underflow_UNCONNECTED),
        .axi_ar_wr_data_count(NLW_U0_axi_ar_wr_data_count_UNCONNECTED[4:0]),
        .axi_aw_data_count(NLW_U0_axi_aw_data_count_UNCONNECTED[4:0]),
        .axi_aw_dbiterr(NLW_U0_axi_aw_dbiterr_UNCONNECTED),
        .axi_aw_injectdbiterr(1'b0),
        .axi_aw_injectsbiterr(1'b0),
        .axi_aw_overflow(NLW_U0_axi_aw_overflow_UNCONNECTED),
        .axi_aw_prog_empty(NLW_U0_axi_aw_prog_empty_UNCONNECTED),
        .axi_aw_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_aw_prog_full(NLW_U0_axi_aw_prog_full_UNCONNECTED),
        .axi_aw_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_aw_rd_data_count(NLW_U0_axi_aw_rd_data_count_UNCONNECTED[4:0]),
        .axi_aw_sbiterr(NLW_U0_axi_aw_sbiterr_UNCONNECTED),
        .axi_aw_underflow(NLW_U0_axi_aw_underflow_UNCONNECTED),
        .axi_aw_wr_data_count(NLW_U0_axi_aw_wr_data_count_UNCONNECTED[4:0]),
        .axi_b_data_count(NLW_U0_axi_b_data_count_UNCONNECTED[4:0]),
        .axi_b_dbiterr(NLW_U0_axi_b_dbiterr_UNCONNECTED),
        .axi_b_injectdbiterr(1'b0),
        .axi_b_injectsbiterr(1'b0),
        .axi_b_overflow(NLW_U0_axi_b_overflow_UNCONNECTED),
        .axi_b_prog_empty(NLW_U0_axi_b_prog_empty_UNCONNECTED),
        .axi_b_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_b_prog_full(NLW_U0_axi_b_prog_full_UNCONNECTED),
        .axi_b_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_b_rd_data_count(NLW_U0_axi_b_rd_data_count_UNCONNECTED[4:0]),
        .axi_b_sbiterr(NLW_U0_axi_b_sbiterr_UNCONNECTED),
        .axi_b_underflow(NLW_U0_axi_b_underflow_UNCONNECTED),
        .axi_b_wr_data_count(NLW_U0_axi_b_wr_data_count_UNCONNECTED[4:0]),
        .axi_r_data_count(NLW_U0_axi_r_data_count_UNCONNECTED[10:0]),
        .axi_r_dbiterr(NLW_U0_axi_r_dbiterr_UNCONNECTED),
        .axi_r_injectdbiterr(1'b0),
        .axi_r_injectsbiterr(1'b0),
        .axi_r_overflow(NLW_U0_axi_r_overflow_UNCONNECTED),
        .axi_r_prog_empty(NLW_U0_axi_r_prog_empty_UNCONNECTED),
        .axi_r_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_r_prog_full(NLW_U0_axi_r_prog_full_UNCONNECTED),
        .axi_r_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_r_rd_data_count(NLW_U0_axi_r_rd_data_count_UNCONNECTED[10:0]),
        .axi_r_sbiterr(NLW_U0_axi_r_sbiterr_UNCONNECTED),
        .axi_r_underflow(NLW_U0_axi_r_underflow_UNCONNECTED),
        .axi_r_wr_data_count(NLW_U0_axi_r_wr_data_count_UNCONNECTED[10:0]),
        .axi_w_data_count(NLW_U0_axi_w_data_count_UNCONNECTED[10:0]),
        .axi_w_dbiterr(NLW_U0_axi_w_dbiterr_UNCONNECTED),
        .axi_w_injectdbiterr(1'b0),
        .axi_w_injectsbiterr(1'b0),
        .axi_w_overflow(NLW_U0_axi_w_overflow_UNCONNECTED),
        .axi_w_prog_empty(NLW_U0_axi_w_prog_empty_UNCONNECTED),
        .axi_w_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_w_prog_full(NLW_U0_axi_w_prog_full_UNCONNECTED),
        .axi_w_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_w_rd_data_count(NLW_U0_axi_w_rd_data_count_UNCONNECTED[10:0]),
        .axi_w_sbiterr(NLW_U0_axi_w_sbiterr_UNCONNECTED),
        .axi_w_underflow(NLW_U0_axi_w_underflow_UNCONNECTED),
        .axi_w_wr_data_count(NLW_U0_axi_w_wr_data_count_UNCONNECTED[10:0]),
        .axis_data_count(NLW_U0_axis_data_count_UNCONNECTED[10:0]),
        .axis_dbiterr(NLW_U0_axis_dbiterr_UNCONNECTED),
        .axis_injectdbiterr(1'b0),
        .axis_injectsbiterr(1'b0),
        .axis_overflow(NLW_U0_axis_overflow_UNCONNECTED),
        .axis_prog_empty(NLW_U0_axis_prog_empty_UNCONNECTED),
        .axis_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axis_prog_full(NLW_U0_axis_prog_full_UNCONNECTED),
        .axis_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axis_rd_data_count(NLW_U0_axis_rd_data_count_UNCONNECTED[10:0]),
        .axis_sbiterr(NLW_U0_axis_sbiterr_UNCONNECTED),
        .axis_underflow(NLW_U0_axis_underflow_UNCONNECTED),
        .axis_wr_data_count(NLW_U0_axis_wr_data_count_UNCONNECTED[10:0]),
        .backup(1'b0),
        .backup_marker(1'b0),
        .clk(1'b0),
        .data_count(NLW_U0_data_count_UNCONNECTED[8:0]),
        .dbiterr(dbiterr),
        .din(din),
        .dout(dout),
        .empty(empty),
        .full(full),
        .injectdbiterr(injectdbiterr),
        .injectsbiterr(injectsbiterr),
        .int_clk(1'b0),
        .m_aclk(1'b0),
        .m_aclk_en(1'b0),
        .m_axi_araddr(NLW_U0_m_axi_araddr_UNCONNECTED[31:0]),
        .m_axi_arburst(NLW_U0_m_axi_arburst_UNCONNECTED[1:0]),
        .m_axi_arcache(NLW_U0_m_axi_arcache_UNCONNECTED[3:0]),
        .m_axi_arid(NLW_U0_m_axi_arid_UNCONNECTED[0]),
        .m_axi_arlen(NLW_U0_m_axi_arlen_UNCONNECTED[7:0]),
        .m_axi_arlock(NLW_U0_m_axi_arlock_UNCONNECTED[0]),
        .m_axi_arprot(NLW_U0_m_axi_arprot_UNCONNECTED[2:0]),
        .m_axi_arqos(NLW_U0_m_axi_arqos_UNCONNECTED[3:0]),
        .m_axi_arready(1'b0),
        .m_axi_arregion(NLW_U0_m_axi_arregion_UNCONNECTED[3:0]),
        .m_axi_arsize(NLW_U0_m_axi_arsize_UNCONNECTED[2:0]),
        .m_axi_aruser(NLW_U0_m_axi_aruser_UNCONNECTED[0]),
        .m_axi_arvalid(NLW_U0_m_axi_arvalid_UNCONNECTED),
        .m_axi_awaddr(NLW_U0_m_axi_awaddr_UNCONNECTED[31:0]),
        .m_axi_awburst(NLW_U0_m_axi_awburst_UNCONNECTED[1:0]),
        .m_axi_awcache(NLW_U0_m_axi_awcache_UNCONNECTED[3:0]),
        .m_axi_awid(NLW_U0_m_axi_awid_UNCONNECTED[0]),
        .m_axi_awlen(NLW_U0_m_axi_awlen_UNCONNECTED[7:0]),
        .m_axi_awlock(NLW_U0_m_axi_awlock_UNCONNECTED[0]),
        .m_axi_awprot(NLW_U0_m_axi_awprot_UNCONNECTED[2:0]),
        .m_axi_awqos(NLW_U0_m_axi_awqos_UNCONNECTED[3:0]),
        .m_axi_awready(1'b0),
        .m_axi_awregion(NLW_U0_m_axi_awregion_UNCONNECTED[3:0]),
        .m_axi_awsize(NLW_U0_m_axi_awsize_UNCONNECTED[2:0]),
        .m_axi_awuser(NLW_U0_m_axi_awuser_UNCONNECTED[0]),
        .m_axi_awvalid(NLW_U0_m_axi_awvalid_UNCONNECTED),
        .m_axi_bid(1'b0),
        .m_axi_bready(NLW_U0_m_axi_bready_UNCONNECTED),
        .m_axi_bresp({1'b0,1'b0}),
        .m_axi_buser(1'b0),
        .m_axi_bvalid(1'b0),
        .m_axi_rdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .m_axi_rid(1'b0),
        .m_axi_rlast(1'b0),
        .m_axi_rready(NLW_U0_m_axi_rready_UNCONNECTED),
        .m_axi_rresp({1'b0,1'b0}),
        .m_axi_ruser(1'b0),
        .m_axi_rvalid(1'b0),
        .m_axi_wdata(NLW_U0_m_axi_wdata_UNCONNECTED[63:0]),
        .m_axi_wid(NLW_U0_m_axi_wid_UNCONNECTED[0]),
        .m_axi_wlast(NLW_U0_m_axi_wlast_UNCONNECTED),
        .m_axi_wready(1'b0),
        .m_axi_wstrb(NLW_U0_m_axi_wstrb_UNCONNECTED[7:0]),
        .m_axi_wuser(NLW_U0_m_axi_wuser_UNCONNECTED[0]),
        .m_axi_wvalid(NLW_U0_m_axi_wvalid_UNCONNECTED),
        .m_axis_tdata(NLW_U0_m_axis_tdata_UNCONNECTED[7:0]),
        .m_axis_tdest(NLW_U0_m_axis_tdest_UNCONNECTED[0]),
        .m_axis_tid(NLW_U0_m_axis_tid_UNCONNECTED[0]),
        .m_axis_tkeep(NLW_U0_m_axis_tkeep_UNCONNECTED[0]),
        .m_axis_tlast(NLW_U0_m_axis_tlast_UNCONNECTED),
        .m_axis_tready(1'b0),
        .m_axis_tstrb(NLW_U0_m_axis_tstrb_UNCONNECTED[0]),
        .m_axis_tuser(NLW_U0_m_axis_tuser_UNCONNECTED[3:0]),
        .m_axis_tvalid(NLW_U0_m_axis_tvalid_UNCONNECTED),
        .overflow(overflow),
        .prog_empty(NLW_U0_prog_empty_UNCONNECTED),
        .prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_empty_thresh_assert({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_empty_thresh_negate({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full(NLW_U0_prog_full_UNCONNECTED),
        .prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full_thresh_assert({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full_thresh_negate({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rd_clk(rd_clk),
        .rd_data_count(NLW_U0_rd_data_count_UNCONNECTED[8:0]),
        .rd_en(rd_en),
        .rd_rst(1'b0),
        .rd_rst_busy(rd_rst_busy),
        .rst(1'b0),
        .s_aclk(1'b0),
        .s_aclk_en(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arcache({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arid(1'b0),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlock(1'b0),
        .s_axi_arprot({1'b0,1'b0,1'b0}),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_aruser(1'b0),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awcache({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awid(1'b0),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlock(1'b0),
        .s_axi_awprot({1'b0,1'b0,1'b0}),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awuser(1'b0),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_buser(NLW_U0_s_axi_buser_UNCONNECTED[0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[63:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_ruser(NLW_U0_s_axi_ruser_UNCONNECTED[0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wid(1'b0),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wuser(1'b0),
        .s_axi_wvalid(1'b0),
        .s_axis_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tdest(1'b0),
        .s_axis_tid(1'b0),
        .s_axis_tkeep(1'b0),
        .s_axis_tlast(1'b0),
        .s_axis_tready(NLW_U0_s_axis_tready_UNCONNECTED),
        .s_axis_tstrb(1'b0),
        .s_axis_tuser({1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tvalid(1'b0),
        .sbiterr(sbiterr),
        .sleep(1'b0),
        .srst(srst),
        .underflow(underflow),
        .valid(valid),
        .wr_ack(wr_ack),
        .wr_clk(wr_clk),
        .wr_data_count(NLW_U0_wr_data_count_UNCONNECTED[8:0]),
        .wr_en(wr_en),
        .wr_rst(1'b0),
        .wr_rst_busy(wr_rst_busy));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
SFoQ2tXDMrL2nCJbfpmHXuteJlKaWDWl3o9OY1miFvmYb8EDywmDpLUHQktJ/VoW+17fK5WHgFVI
FZV1B91GDQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mxGWDRjEAsKmBqldxevT1RKZvqK7vn0KlTODVXNGlRcGf9zOAmj0Z7Ppu79POBDb8oNQyCY+2q1q
BddzhQfh5WLIVX9BNUMIF6M6IF0elM4GMSLHGeYEwqSaMPC+thuR8FGj1J7z6rH+43gDYhtIeyY+
ZuZUz/Pqg8Lu63Xwe+0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HLwPjQzkuqv5FEDBriEJS2DikBeIHB/bWuVWooHY5ChdoHatcmqCHpSvnGxVzLwObZWHFys2nR9y
P3zxywjtgtOWq/n3cYVa5li6eyiUmGXv2OE8nw1nLnAY1kzBvGd6VwQ45t6l4Hx5+oqpIfuU2KI2
7/Qpj2atiTN3Y+q5He/BMXLIxF9vWuU6XL/+HsxriGAumcZDuESdidlxOztbW1bFhYr1/qWwou2q
wynnRVKYHL41aWycgFdkDoDEFFxv8ft8+F5Ux+J5Hg5XdgRULJc6uUQE/lDG3zOqzPftlODB52zU
d0cm8gFOvSZ2nO8ZB8THnxoAGe33iIZJfMcefA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
jlR0iZ4fp9QXiFgaT07DMAK1YFLyBpsOGOOR9j2PWImFEh8oTBt4cvmGo+2z1Umbt9OMQwOhyepO
QIsKLFzUXYUba+SFFLBoCiaww24KICecbUfd3VV5sg2bEJjAdtYTT6mJqyc3vQRvBlONeBFdIGy2
AXqdK7QtXGLsLAIF/z4FG8cfG6nSD6e16gccBC6+kl5MoShdnmebKLyoo6UKFdMbDK88sHvTcD9S
LNCau6RK7FkTZg23FV0tf6cTP9Rray9YEcowm2AAh51Wldo2lGJ2W5iiDatRKH/W1bu7FGWZG+OT
+VZE+Ckiuf4T6cuu+G5IbrtMv6a4U93R0gtxXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
p/kq+JjPPJbOTWT2SRiPJ99/iH6kkVGEiluRRXpuRN+j+cVPgJD1v4QVjw3zMWLlvTGB7OOqC+JG
Lc62Wiizd/BFfGj2JYkTZMatcOWok7A87HK+vRTjr4nZMApD2jKaneJdU1279KsIEeRfImCQ2uRl
QRNMH3PPdNGYCnOGgNk=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kyyI/O29YYc5VBwhz19i7AV7MC75r43hHVKAOTBiGBhRu8zZxCwGGcNFqc2HgHcWC6nq4jCIbIXf
S3FDzPdasegnERlWvoob9/SXM88zKsyeTbUf+DRu5lB8SPROBMaIhnj375C5XLowL17MXZdmB6fV
X5ukCg7cNhCjssKt/bIJibWkfna7hvj4ye+CLWmi3LdEiix8KTwRoBS3ZJrjM4/N6FfZkXerVxs+
txkhdsmG9ga1g/xErhTRilhqrV2WetlpX86qH/64sRGVxrWeEfNoHhMZsqEK0jWDx4WavKt8XY7W
NDzMXLZ2m5Dv5HMiJWgFG+ntPwgiYYtBuwu7Eg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
tv6UL1ZWqo3dAIlhN5UTNGzJyqzdHpCqh217JPvIvHiWJgcFh2tw1n7HWnOPcK3VhCt31AGnCEFe
HpTiinXvHna65L2X2HhtNUrsgvZlUuh/oQR273wp5JPFDPD97NQ4ELkGI+w26HTYLgZ70K5rQo87
D4AkQNRuzTRS5G12yb4RU7ZYgmkYLuq1UyqjlxyN62Del4XoqZyivOGw5H+7wlfkNRu98iQwqq12
jthZbH/ue5wxZJUcb7NmEwL+3abpyDNmWs1qORHOFoE3t97/9XMmeSCpM2+KnSKJvsV5VbuoTCOT
964fsEh7ey4IVb4aum095gQjLCqTmDm8DWFmaw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Oxo3AgNmVWgrXtMKDIThYfXr0YJfyFr7Bsjn2ge/G72mb25MA8Dbkd9ZZPtwqU1poazNnTng5Cx5
s8C1zMNEoo38jNY8zEUBjCCuasJgeMo5xsiha+3ZIBiuHS0KLrjLaPFIQZdsYevb44fg6J5YQLn5
jd1M6YdNMd1VwSezDxtbk9sN8ExPrmtwum/6L1ia9j9UlIzPTEaJ60Xz7tloPsgsbkborO2JLiIk
kIAY2q1b8tuhHzJ5DoXlvIo49wSDj75ncLrkwbAd26huob7aOmX1bS34pJLF17JzqYH0MoPJbHxb
RPdD+qUawXFsMSs2fOLnZrNxeG8L+TyAT0N8tQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CIR/vwxo0IBrPr5+bMp2YuBCQTNBRIIbqgEB18Oewkc8CuHzGCAgPyQUBUKaUG3bBy+KDOPVxBP5
cE/d3QYZAT11fyB1OMMTrjmEIZcr0Vk3nVTAnivoxxxkmdzPjkj0OcGcU9fMArPi3dfTgIsKdtCq
94+mV/70WeprgijzuZFWD7uH+gVioY/+rq/Wc1O6x1n949w8YGgSCTurUvhsobx2bonoC317J0Wm
IX17XRkSBIFgzqA8iC+GV5oCfxIGkihKmXxjIJbMamlOdCOycEkjkh3JYmm7TLNxmI65iffsabR0
t5+iI0l8eJxFhElzWeREqE43cnJYLaKZBUA+DA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 63360)
`pragma protect data_block
CS98L0YH6pQ4WpCpoD05GDbdGqfiTpoAC2AerUlfXvidbdxXfbkybqMhzXinP+oW5oelf6ehuZ3G
r//VxXyDac18P12/pdL8h7zf1Afwuq/qOPOLTry7qEPge4lVysrbyi3wAz5t51ognvXCjLogC9y/
RaFXyJb6YAHAyC9YqpsNrQwcALnI0k7c2/ZauAngcS+pYdyj7uepotebw4LtzWcXJYQ6DQyPpDzQ
Kb+KDaxLwgsJUA8AUAZXlhC7EViOIiNCJ/6f7Ac96VkwrZb1u4AIMrGW8cCEZmOYOjOKOmp/Pp93
YsxlCVe+YSPPTo9SEWaHN9kTBcX6AdGoVhIeS7nb4PbO1RuwjNPWJ4Mbgk4F2Apn6aP+SK+bDXH9
Y16LZ/jXLMFSQZ9Zp3XyFuoOtqfSclpr6HR/AfFpA7WmfmoJtqiSnx7FSW6Jqss9Ibo/cETTSPcP
aQLwodM6p33gB4qNhqSAE+hjFV02vRd7z8Nc8gnV1TU3bpweLLeqHzXQ3SF9Tw4vNYMxHiGBxuO3
fI/9HVM1w97SKO11NiEckYXfS+n2IVv9/CxfqzhuVP6pXvfbWw6/Po5YPWerawkZJ+eTWpQJvW6+
X0sz7Q0P+AfwtTNtbJ6NbUNqQhbyAtEdFxGOKEdRwshq8Z0x+9Dyhch3pqYQjPHLine6uoclkH8B
28Y4gDc0zQITH/p3owgERekw5R5WujsJFUB8bOQH3FHK2mLoqjh5c41gtaUiGyzXKM4vb5Yem7aw
Pt5e1/Q6Jja8ZSwWbw64ZNdnQo8zQvwhaEUVhtOzzRpUBm0VrUEUMI09UwNOf30tUXtUKSG4WtaU
qTvHz5hZVn3Uc0GJ07wU6x2R9qRg+4Nra0JKB5a3i3VQQ6sN/+MOpiQXhdgbwYO1FQ3HG7eHLaNY
2wJtdEGUmcoGJTNfOHmAJFwy/Tpvf1a/dsk2u70J61692NYrQrn4d54HowKsny1tSPhg0evTyqsp
iTgEbe0yqtBYt/hMLGsfQRewKdPb09zXA1bM/cNQtFRBqMmz+n9v7C4pTSg000sBc4lJ5jCKiZ/e
VrVL2Y2S5+hokjtzVve+CnHoV8wQRaBblvHvIYs41NWix2OInD/fInRdCBkO067Op9QN5UrSAJbR
6d4KMcZKdku4rQyxlT2X+ZOzl5IEbOkU5sphHFT81B6rfRdDTpmSQyqBiq+fokGsv1Inz2ZvD/0R
mzXuvzES1jaWvn2Ve+R9V9iqDipKb9eKOT5sB9YCyke6VtrxJXbUrF17oMHDOc7eRF35EuhPOYXc
qOJ1ICcTIuAkUP/9tyN/7sasmY2kcbFj5StT5fymSqv6IgfSyvoVyZZH4aq2BKboXH7gna7YVfc4
LJKqdxuQYetrarqQq6DvulQTuRHAP6lXqb7bHEwIMcMimamH8Rxe+373+Gq+0NLS+uCT1lYaCzN5
fRjlezpbd7+9VA6CLV06nzyWUrZpvfTEVvefaNatkUur5CGhGbhrJfiyZDZf2WA9Mv6qu1ikncJF
1jBelsCNvaZU8imMlw+ucUUgwqr2/TzEnzdYGTKGi3poH0J+cR1RbrvjnXxFj1pYsMczpxt4+tw6
xMDTB3AkEDC3FQR/rVzJF9PItQHOPqIo+acuRl69FseDjYGokx0Kk9Zyu8nKy8JuEAxgXLb3EUTR
msph3xezB+zEimgYT9DWPRCkX60sDMvqiyTxvKFirhEnujfcrTo1ZGpvPubNIvtyxLmpz9sVksbm
dIUcDE60zH2cR4W7FLUssWEdbX5duW8ubujuj8Kp9OySUIAcX7uC4FpWGTnKPrwUEEbsa+OPkAug
LpMtFq0h1/Cld8qtr4FiZGmtJchT0D96dITz7c5pYAcVFsZmKe7Mzr9unMkMHAQ64e1aegTtPM9h
GtKUxlmsO3Ak+OIoPUT4IjKPNpWx176a8wyF7q2ojf62gHlek8n/ZDt49Oq+VLcMoeqKkIRo3GiX
XIDj39EpUCRw3Ktrm0QirJLF/kdZhCoBQ9rwwcYUg9FFvcHCZkPI6rzrXCaQ170gMAV0E6lunuIR
REqL7mjKgSLpQ/DUWLD7w6Qjvk4rkflez6hVgLvo201fF/TYee8pFnI8Pn6gyoO3mYyaJHSUpX8Y
G8OA035+bgb/9HPy+NIe/C7IuwtU2Tm9CZIkqb8CqLXFcNz/3+Oxv2wQNduaiqdmKmL6UGPvGeI1
omtBcLxPW1e4wqo40kqSJ5ey6Rcgz8NaYzV+xTvh62O4ef1WVFuKP3ElI54gAR+64N9Iwr+7oxUm
9Bcza8wftjm/91sCIlei7UAQJmidFeY8DrS7XyXku2ukHj7Fr67yRcErSbLlC5zDT0KhmSIMZ//P
Nq8AC93ck47X5IPLMLSL1c/nT3n0jTIhnuJbSZD+3c2qhBuQxQZZW9XkAg/XD8gaVYdp30DhEE4Z
K17r2M9HGrBaMJcXsHErgquQY+mqVb1tHIPtWn8vmAnA7txoh9LRlUZIyR5ZPTQ2oGFamalakPjV
SHykyVsts1pKDAiH0wMf1GPBLlAZxSZckgpemvrgoTInZSgD46mLytkbh5prlxTrJqGNQ/FK1n66
LTQA6o4mm2rXKgi7132PIGe3aCz0McSADH7MDA1LKHYP0Utpoj3Rio/ZvQD9PaSLwOvZ7yhdA+8e
juDWW690igO05sJQquejmfyzik9dsVJO302pSr9Q9RwMti24OvDIa8InpadcGpAecYdxplSadjG9
3gGveT0iRQsu/ScJGvC5TbP5mT8xhAGIqnKv50lwLWpIQrt8/YZL+s8o49GteLrFBHiN0JJSESVv
g6Exp7c1RvNJS5/TCMr9YxflvSWF0vrE1GbfyhTXHKIKpUerV4IBrA1UTAtELdT+JI6LMhxU7DYh
qDExwWux60Q3ECq9AkSpmdaCaEaX4hQ/jSsiiyxKvTtJzzIwzyYHZKoG808gIAtZBPb3BWWWtytt
5vck4WstjWMrKtcxKa2wlSlGZMzDL+D6jvb8CfffQMO9ndb/BUU7gAUdxZZTDYxb5Jx+1KZKSJFh
snz7RsGyTilU91I4uAyaZs8trRqdAvpwh9T2HpvtsErMjj4ikQvRQM+mRljvgtvrc8cF66DGEua1
YtLHSj0A50QBwgYcQSXfOI0EIA8x+nLQ/qDS0dTsP9baYaIZsidqGAkrkdivJXEpxJCelyMeJuJu
XIg7Q8l0Poaq8GKPV3bafvtEOLruoSggUpiDiT8Bcq/I5Do99x+freNUq49NrsWzHG9Aui9cTEsU
qiPOuzf6YHqFsOo3ak2Tp3LvxvPqPdM2VX5CrzGdFSw+aYG574wSX3s+JjkP1YiDLrtDg8IomU3o
LAQBz0wP57xzc0fSCLzOSTQ+r5ghLtjI0Ghxb08wyO49h5H83MaM9CUPvHbk7EzCK77DUSUEl2b5
cdvrO7GeDQG4rHzPCsvaBEygmDtzXSfOLHqHSBla3M7FIIiUCBOcbYB/DbQnbuBeTRVNJnHizbre
Z3KXXpxlbR5dblq3rCRd2FMruOBK0a+GCyNXaH4Q/OUsdC8t9owiLWgmo8oQntYGrmAlduQrQ97/
J5LruwrJiZJyIVGOcphny83TZrG3aPsyzxCBTx4QZ5wjPF5qgCKjsrdLyzWI9JqP1NbXQ4wgoX68
kZ1tb2YACNLiWA6xE+qxkosoZZapAehY+zaI4WjaHU20Z63OfUPXC+pnadtr4TsXZAAL+Elps58s
8W749p4OzC0+6VNTmxEd3LCC6JK6L3fpDgfG1oDN4iHUfnDR4CTTcB4SQMyXVWcW/TUyGOm6F8Ol
bk582TTW9nkfaE0Wi0MEuETFWVCVKE9aOXJGLOwgOZrlgfFjLz1vBHBk+le1WTNUGiN6VTZ5J4sZ
R2E08AO3Js9Q+ODX8qVP9Udy0wlzDWgZdzZhNYpJJn1MujVngRLw99UVqix8UrU0oUPAH/wVV3xu
IKpGhX/xo67n6xWtHtt5QzJH1NbntO19XXyxYtn8T2svJBrhSClyiZC06cdX1xG3HjhF/Nfk4C0H
x38vJcXO4ignVrj2+olV0LBxCHZhosZM7QuA/8yM3wNCiQqDL7f1IePu7VByZuP4uN0SVfr7o0lz
Ro7H9/UlNOf/odfv6MkpwuVEzoqY1Gp1BYkTUWrBmDDvNd35D5/mGToUmcV3NKjo8I9QTgrBkDvn
I3nO5nP0HY8PjXuxDoreAZX5T8otn1Rpl+OgHnFuRshB8cbAvOdsXk3FHP8karMgM6JAz8xyY7Eh
kPW+KUNCF69EMP7yFz+Wqjg1l3fc8Lqlf/lGgptW3B+lcAIHsxC6uHok9scCrJr1140g9ul0m3g6
NZzvTiX0D7bDWV5N/drCMVrQABzebtk3nnHDcoRCU5D1/fjBVyt927N23Eu8qT5VblQ/pVbV64+o
k+63g+seeRvIAQH6mFfpMMnXPHv4DruKHkF3YyLGQ7z/Q10Z2TlPpBxUOCPA4tKoSTShOkPA++HL
eF2G2Fj7djWo2LQqoNuT66d0dCxd5GUBaHiSk3oEXT3AL3iebVvvBXzk9ORMzf/tz28jHIHfhgD5
To8gsJknqA3Ofh8zJwPEzwqpqCbSZgpPOzZec7EOV33ulBKJxtE+DZYCPzsyiZIOVcU2vVVNCKL1
0CSlk2eEYC7a4HtZpgai5wh1pPvxeiGbz7LEKo/hUjyhsNQ0PzAMWh0JcHd2+HpaTRAkK5utMPl6
83zkQQTFm9vxsBrxG9PvIJ3N8URwsYNYxdYdNyZrXUJA7G+gBXkKOOEWyZbCxmrqradPdBiPA+LQ
5uif3o1xhL5131ehKV+nIs6cMypA4eP4XdL6zE4aQ4DWapE51X1LWQzq/XHE2uUm7pCPgVHPJAka
fBqrwCJd/HtatYBLBMtRIKhfyxvuI9Xpc7V46a1cmG054NNtbcuDKi4J9poIK2xqq0CKMHHNz6yD
EuFu5ObmETV3ogbcsC7HTYtpyiS0Joc/1dEj+9YbtVjArSx3jzSKDATCVio+eix5q43BsTDGXeOB
4CeBZ0i2hIz4TXRbU067D5d9HJ1uNLUFFmP9GPLU++gBS/YqTgC5I0X1bsWwg61T/uLpJBWnQNo8
zxEL3pFOeiNo3Aw0SQFXdqS5gb3y9ahR4Qi/ULFCbbMk3WBh2Uz0/4K8XklT8wWsUf2Go/smsCuB
Pbej3qCCNmsLYcQlSoI/Quj+osakh+wkhgGxcyivPIQXLdufY0gkLyf5PbhhmUzuyomCbhXYnTQP
XZIp1ZnZQPZd1UWhCFjlQxZDk4E2860fWiTRFMjN06S3IUzZDSJpnkX4Hrr8oYOu+nyzDsncKi9a
TJ8Kg3pimJolKLDvX7nyqXq9ZOyyP9KcMBEe+b/iqDXc0n+ZgzCc6COwb986nM0H+opnnaU5ut7T
cg9PLA0eB5OM1dW418K0MjuOXppuSOL/Fc6OK/rgGmHrpXrdZi9o4VPOwEtccUaPHNhqCy8ho8qV
8NVFYlHqQoG6l9Fo7us33Mh3HwE0vVN+bsVQSKL3QBDjGD2fzYI1YqdxrsEmvPbr9twsuutd1Dq7
cyxGfR7UVdIx0kJDvcaYC/1lfdvbabfYGtMtX7+tLFRtHfp5JDK40vge9fDr4XTtr5amOfCYQHce
rstLjptbLhfapvxBXmntnSfYw2qusaIGXlhbDUCLlZ4ebW1xSGM2YYYuqtscW1KyQh2h5AV7hlfk
+Tm86N2ly5dEWJyHxnd08MBUSTpeXFThtoZRa4z9eam4CDqN1Lqpz2vTVwScf2p3m+e/dB9f+FQg
vbNbASeeKjIh9yCwR4A5Y+6RFVQHbXWaLn9neILVazmPNvqfzvgtUKm0lIAaInRvGqB25sheiWcn
aNCfx3lKT7mar8Ra1V6ptjiUOiwzZudm41jIgd57ElLG7x4jfwnQmNAcrn6NR6ucMwXw/NuJcde6
ueovOAkzmLcSXW9tvWiAXHYikLeYzCZ35aI4AQsVg2a3OXOXtIaU71KXr3FsK5HNKz57jLCXjr9i
vF7DWtCF/siio7fMgpYFft8kB+lGywYi2thEUdq5jO3/jSvoFQIHkBB1Ql12LZmhzvD9zSHwvY9h
jfgojv7pDGecY/2M69rLde1kVtKxbNnC+PX1KOgJQlF+Z8O+8KVKGeMvi47MRaaxmmS4eFXVVnzS
2SX5oKb1BHtTRaBN2jgPpBuNSwGdnzFdK0yLSGfCMVD9i1Jg+pOaw/Jh95xQkzM4zXWsqzRop3uG
gYRHcpcGSeOANdksQnbcuwpXNSZnLk4Jy6w52PWbmTwVzIO23lox/ezqbypPlt9ugUviyc1HYOhy
jnwyIYtCd3FtAqQ3+AQQgnehkkyCtMaQZiFZEiGM7UL8E9KR1TJfS3ZKPEWbMY5nDw0TaXeV/FTp
I56YIPderrduwU+Put2KkRwJkRto64JSWAVpdo0DEw+OK/RBD6bq1xXhHoB3gGKtPWgF1hsFPhoO
sliPa4oqbsJFc1UWS2L1++DpDdjzSSlm6vl+eliH2sPhxPs91DrijYi7uxuIZT2l3i5mUfo0W/oc
AlscKATsaPa5YHsX5mAZtctb1ZU09O0KaQp3W+XhHojrQTLVMsACpTRqGnbM6cZOf1Q7l46iRMTh
4yZAU3wF5daYunYbrmE5xpyg/F1iJqKs29NRHCcLEzAriVVRXfVlFVjWF+FbLGxg2s1zBmExIeM6
r0KV0JGUnsCD5H0q6/j3alrWeHIh+hMH3mc6z8y81pF2LLTtnd2dzEkj54ffbjTF7yw4DFu9Ailo
ckt5T7EpYOQBxJQ2wjqxgEuWy1bUmVycf3nOIXbJvWMvvvfJdOgfdgdk4koCIjqIADpmm8RhmpJ9
Bv2tB65DEKiYFWRwCw8YXwRAKxGDOTSpA5p1m1p6rAjkE9w4fVeJsGC+O8VHSmKy/pMdKzGdbfRW
Z57p4jbOlZlFDXIOtiL4tNrT2/GV8sChaEX8+SaBT5JnPolywevFWodRB8x41u94H0lU8tD64FLJ
67FVWoqGL3IXlHfzocAlTL4fRsGkgN0d8xGbLGpHh04BxW6BQRt+wNVZTCAK6264BD3wsQ18QK6l
KI2ZD9o2kbL2gWcWkG691WLG0wOd7om8DTh3pdkUnuXtEqXTErisfa5bSI7lzp6JAjQsJ5Db2FL6
O1cVYA3hf+HoaG48KREAIt7P4GqAuFWaxKcE9c1sON3beAb+ulMzbcZrCXZH4uCuIk+1csW6L/Ra
L4OnXUct/Gsqlrw9toNwLmdFNOrB5p+kf/IMyY0AQWXXitiqpLdihYafahhvv7qL3XieZueOsP1G
eron4Y/QQXVyy7yEv3SYUdWV3oYtwwrg9/6VN6AFfvLTNIjwlv6nRV5pPCUxmlESAe3b9VzGIC4q
n7pQtW+/gcqmOZfPyDY095PIrjA+sEMZye5wfd+eaZu8SP1COjyGTL+ONVhHMmx5c6anOy5LhlFI
QSfmX8un8MKrr0MGPwO+q8D+JvxzGtryRWTCRKBRgeDspEfYtsxw53K3F2FwHBZRorKeTBuUxiHt
qwOvW6xSpCHm7CkU47fZhG6RuSzfzb3zBK2t9RHQBWDksVypUxOAVCpgkgN8karWXLidpsMI+4ep
L5RMqC0rXMVAalQ0Ea1Qyovu2YBdUIf5BRZeje2m/SX7dxjbIALRoiwsHaGbpzqHxckISR3lQby4
W9JKJZfDRE2DnNm+gkPdsl8ajPGaVzzyRmF7qh0TU3xVn304YczYBQHFUIy7EZLY4M68PoJBagZS
w0yizMX8jbfiupBIcb8NOykeqG6a2IOOImCgAIt/CJ2ViK7H7qTYGJF/dSIVzeDlTs8ps+Imj9nN
6fKRhmtgqrX7CpXpxlKEK+1dvKAkT8moBWQTP4dikMTrgr9uhvC6PsHGzkZB8EjXZ4+S/ukh7hnO
sYAac5prhgg85GltAFNsZWjFVGvGRUtrwwaMk0daJoUEsL+piW4UO6JX5SavvoP33TtcTaePOEXn
rC+MH0VdYHdrlhymJzqO87MArMp0WNCmeDyMvOdcD1ZIRb4hmJRfUSQ8aoXIe9nSv1GkO1udpAoR
ByHOpqR9qgI7hbggJE1x/iHq19R6Bw5GN51ktBjFPw9ipWOk+oVtBWKyZfP5TDjevTxzC5q0taSt
cPaYaYSyq+tsTxWaKmgh1GDI3CCv4VwB7zpT2bqLIO2AI1CvJAMM1mrs0D7ku+WxAZ4XH6d9VwLC
VImEji9+DlHkpm4D20QsNADKDQpOdMi0UgUW2o1fp98lMOy7BjX4pLz5A40TbZvcJ0tVofXP11//
ZXxAjnNTuPqZDucZmCwEEPEP9CCXqtI1eB1cZ46nV5p+1GYFiecXcWFKpfS6YmuV+6ShbqbFsjG2
8xRB8XWThmwOR0URLilGVDZXEf4encvipqEUQG/rNarP2kZPStzYD7J/EgCvYxAP+P1rdX9b7n07
BcgPnL8RFFJBUVBecdng+Nj1UxarZYS5q3S2kONvXlWrMQxTcBmomyZH5LEGeMo/6lQ7dCzjbORs
wSP8FefJNq8PrZx31haHuZRk3Lc2g3z9epcgeWaWNE7Vs43G8AEuH0fQjVrifORDVyuF54CX8TvE
9Xdv6LGS07jNZz0X6mMoZ0S5Rl1382kJB2/R6qcDjFvMKgnpJKmDVJzWVAXdfOlJVnsY0xX644l4
siszH8tKsgiUcrQ4DDYAvoSQG5X1MudeAGaLg5zdnxHLTXiIgUOXe3bONBMHAvjS5hUvqraCzRSW
B3D/abZa1ieFnXtSSjPPSxjJKR06MNPCi1F+ay6llieigfx+nWFrynZTKiVxQ5hPuzwuVXZ9QZ5s
A5F6eFenN4SMWLOLFbwpjfRVKVE84lY1BxaQp45ko6AHudVmOrpkuS/rlNr6Vnb7/TN2pdyomyfa
hXqVkTncnJdS+sGAcRMSnxXAjH4KK3GaSXftYVJ/bBJD0201apnsVxWJYAr6s1124bpZBR1sREt+
MMm2fmuZnZL7TIWsiTc7znVltSSp2byazIyP8mc+gyHWritCu2BPOpuXAuFvQ6U0JVw69UfeYWsQ
ccSe86nqzT+/+Z1Ju4T8Na9kx/MtCIQroAnyXZb/mWKkatCE2uj23O/K92MI3GiRE55YgXfJIq8c
inqWQT+IXM9HYvN8wxtNFijLvQ9AJ8usiDn/vA8NVLDQNr0Fh7RTq9MedQMSBrP9A+HqrZDCu2wd
VVEwqFp0SK4yTJIiFz50BV+S4w58JQgKDLI9lF0wcvJPqQJQ42oeBbTmvvLoGSNgQNhDZ6jDohbz
uqoJ4aTQI1ifJ26PTec5uwLY8+0rdfB4GNwh1A/ylMuN6zmrxdRxGTIjhuxBvcUDZS0Oq0/WHCvQ
2VTIWX5cWESqAuSPDwbXhSr0lIZ/6eWzTROK7NLxdrQW1V8DsGbJox3AT3cc/38H1YrO1p91N2mW
vibOUI3k2GAK4/VVHGXhMtNasw2xCwF2GcGpXmJs2CbBN68OJAbWPdk1S2rKUALEApaTQDXWyi3N
pDojquwqEku4pGCkdD60bImG6vvd7Ajt7kNEmCbJ4M37iSurvKVpoTqPvidOvG9nW7eijSiU2iOa
6sY/3ZO83pjMO6ZebRiHUXlhAut7XIyrlV8XYXnaU6ZEmbgx0BVo95jep332LMgwX3z8nz6MQzSx
3X7vCKw0pv60/GSA2JmPTY2JqPrqEVbIUTePHbHZlDzCjZ6NelIXw1t3seKzquoaBxBgMDpgcvjy
7wa6mPFbBbeZFp8zc+VNr+4gxeWTUWn4JjtUmCNpDrFpp0fWZHLoJMIZIHH9VnVTh2/M5bSbKWHE
L7UpND58n8SEsVVWAvykt84CsLmkpJ1wu4azQyusSLiYizKomzoPQ7qD8hBHfZIhYcZiQZOjwJTO
hFLhXnvoH3jeIySC0GDOzR59jGCVNJuoDV2XZK3uY42FQ/E2sSmIRfBRyfAOgWXlZWcnzlck/Hx3
lZYoi1dmP4GjoPPEaPHlJQl37+NTty778m8MU5JcAdzLtgrF1RFnPMPSS3MgIUQS5SqspUV3dcMY
hz2BBe2AFa92k277l183JwouFq5NQcyNAz3CIIRWuDUM0UpWWNEfHCmTMUBm9Qc6XMAwP6n5F5CY
A0ZCHlR3x6lIECPsZOauNCkc0ZNn+5GPsTQFPxVgsg/a5l0pAcr0juxCl9j6sGI5/0aySCxFFj/+
ArV7fFXSA++lFhkqUMqJsmhTQ2U4+tCBy6Iwx7MlWuUIzJrfFsH66ZpX5zSnJfBTpmimJDYVRUZX
KZ3IpXi7dg+Z4QlZxziQV5le8t9dzal8KxHVH3D7iituP1wlif/RkZAdM8e7AoC/Wq9FExjT4jAB
os6amIiyPKB5398n7OhAfhC8ExFmKAhBA7AMbhyKW4f2umaPalFnzDOoGgFc7AujO82UCgZB7Z4p
Dj5yiLBoJL1r6QsGJ9W+vM2Fm5jyzhvjIvd3s0xlF6o/SzTlNwOUoPGmPjVkBAxi+WBbhI5KOW3n
lQxL90Tvp50Y2CdS4h7f7t6gx1q9FDsCPrQyo/If+VeE46CP7qRjZGBbHMKU1zNaFdjsARol2rTn
4m+6e/ArAhiSwxh2f/Tto+QmfxD4CAH9GnmXGGIo5aIagIEOmvGzFsZ3VPs/7fAbjCfF7Bf6H0Yt
23DbZN0jcRrmtvO6sduQt4eBC6x9QWbSXZtb4C2vborqBAmR7LHMIPn/KYS9ZfJCyPJPLh+xTCZu
Bht+yxVNFHFBlX1YXBRBnffr7+onvOBUisYK7g/H1nhsPyOWt6ayJ2uZ/L9wP9PqK38HUfyy5CaU
nS5da3Q5ahvW/DatWZelwMCm3/rENsZ6hDpclFF4t++bU1WGqt39lTbWkkKClHNSDOE3fBBTkqeA
q6SXkxqoJP/3kHtcwIu0bbrqhF/h+Z7bnG7vZ7K579EyjbaBndhNQK5rsBwhWQT2J0IQhfTy3M5L
PW3Qxm7lYko/QjsVY6NvXakb17zMXXUB5vjweBN5crzrcv8q5c/eK88DSQOeolpXYUpypQduooa7
6Zxm9I9lCcTTteVBj2ATFM64858eA8x/ww8/St0yrZY8zHLAhUZzfpdWtBJmXC1Idgjfun+CHcgo
M+6ktZit7TwFwZ6sVaY2HVmJxxD7MaSHH3NzRwDkMRP5XqmfEhiiHxhfXMI/aEQ085bfKOiBOnVh
IHJrS6tBUUcPpKdzZkHIzHGreFfDZnb7XYnlGbwp9GZQpaEol/9LwCMfg7Dsr9+evf/Lii2oZ1cW
hkLbMGdqKdylSdgMtsgZT8RzfmOSMJD2Whl58MHOXFRl8tCo7H7UaeFr4cbwtD0uUnEJkx73EwVD
dsWnsFXipqIvAeAnwB7TQPKUbdaqV0YqWIOvz0RQwI9lTPG1DBCd6rc0ehQjDqcoWCRSaFAvZZGK
OH42GiMKpiWdkO9qfwUDyChfuOAmgZDs98y3vAAKPGYDnkxkdlzYPa+x1U8gQTUE7LU+u5PLI/w1
2yHQEmK+X/802fu7hcavezVMdMpAeHUlpSqSb6Rx66tDjgS8XVy+jpDDi1AN+/lQtn+6TC32ITvH
WwfuF9xofB9yymzq8zHa+lPm1koSJGLNo5FwNhmRIZoZFLohrKbSoFwQbrYvHG55P59NDBr4ij2Z
jyjqaSi/jFILNIEh7ac623qWyBZbtdPP4FkxzRIkthtNetqAz8UkNBd1lgscsU9qBq4BUKQEoQho
ebYVTlYYZ88XGHwhuPs3EALZH/fIZp9Lzx2ARDO9Xhh6tY7IxAPBGYXg/pgddpUGyC6Hd1SNyQcv
ci7wnnqpfs2b0rBuVozCE6BNrGqaXfwtIGtojcoPpmrddwBjdFbj6CxEqStz1PiuLMvyYU6naGzG
gJD0MIki51S8bCWuuAf8PS7Ndd1pRhfvQY5+7xb3x6CQeHmg1bfPFHBMcXhLHxMITmmY69KVBH5H
b9oR9onWbKUF4p3rqLIQQ9sOm5RqueM7l22ehCw1EFa7kyFhIwJev0jp5WoD+aioitGLpd+4KwX9
menUA+r9FCiFkVjg6wXOfFx66L8kvTwKtrUbN/+SStDKN5AFjnU98ccS7NFuY/gPGBA/y4MRPQV1
/FsfXy2ABH7zeBwgw8WGsZPMbwHOPtCv6yDBG7hH+64c3gxTGTtP/4K5Kmy8eq5EoV8a2NT6woVN
IrGafUUE7e4jGWuQFmotA/kHIN1Jlxn3QHQGcIP39/Jh8+tY505Q9P3fTfZyVB9dLjfT/MtQn4VK
fCP1hKxbZW3C3E2jwl2R4MC2e/Z/eJu7oEHzo5Ot6T9UE0k4MPsL8xS1gd4C/T/goWQV6IYzolzz
N1wuOXv7AWhBRlLHGj+inULl9hjvEyB/nUwUXpwIvzn5LURIY84dH3J4U8M6G7s4jd23eaDFAOF0
ghNt8AqcsQbfNC7IwJ+OWIsBPjw+XoX72hv6EhmqjYunp65rxUr1ClTaxwNHDeTKSa22ctT1XWvt
3bFSODDWes8ttksDxcBQ25XCAO3VGwhJO0uFabhKEC6asiNXOxE2e07yLx7DrMMl1EDNnMY61SxP
dPq1rE9zf8OnCaYsPfvy8dty+zV721v3d9G2KTpSGVYDRid5R2O90uAIMespS2JQYaq7V0ZBxpDl
xUicgKCuTiVYVcastT9ZzV8V84nyHsf1RXo7Z+3GuwCYMha9QE5z2ttuuBvy7f7lfsthH8NHkXcL
pcUYI+BUs5IbMFdcVIGdCBYJfcZRSS0iSfP9uPscXTVNdyv6G2DB/FX6Izhuw4xK88HbgZ3AHR1C
GD7aC/gT/kHbSou/gz18pHEbo1y1UQgJGZEwJF2lsuO9Db54pQTiwdelo6aDRhyvV9haoF5Bh8RR
gwjoac1inO4at/CYH5N7Ro6eT2Xc8qZe/HgAQ5Gneamc6BoED3H3Npc/tiK2YfvVbJC1s5j0Vwct
O1v26WY7tZx/zRiVFLSWJlI1Hz/ixCBfJQWxYOXMXXvuk+RQ3Q1lgpn0YjP6ToiVtQE35NUcIdwZ
c/n35GkO23D8QapCbftfWPU2TO7VygPPKC5NJR07g/xdZO8H2E5w58mvx/6f27Z5wojNZ1mteEIs
DQkAOzAOoE1J9buv2ForCzpQ2Df3MEErpB3D6DWG9Sk5WOfWLOQaVc1Ub7ktOcSxN3R8NYH/BrUu
93RWRIkJ73Qbg7WofsS9++Uw8nGMREFKHS7VQ3W3/BHhViFKy5cW37+TJtrFREbIXU39Uo2gh7YR
W49iqOMP28VVW1cF6E+CFl6nuvxCpO5MZjWxrT4z6/xBetKEgml9tM/antEFW38rcG82pA6CSw+a
oH4wb5St2tQ+hX4V2Bm8qf3CPXJKW+4QV2IZNdIZ6NsktfbJv4DU1CcW99xil+nrF7dQbLkmLFZt
0E8HbaeYc1iaeNraoic22I2vdMPp79OZJ9jGvDiKa50jGTNCG7uZmNUZ8r0duEb/oYWPlUlOL+pF
6TZURzAM3Hx5UubUp/IqigSfQwq0PXb3E0xjFvtARZpdS+xMI4UZBSpnvJMj3EHVQcEQ3PlovHkQ
xh93fF+F504dVzK96AupCt9e6IhhLFKdDCCKQelLQwyqMCCqD9YNmj5DfR5pMUlVEHzFEH+eWCSK
JRMH1zI+1HsWR6uBK11pYB5onYSnWD3T1LDfIMOj7nYAe4gPEQtxrgTWznOiXBlnQ6+WBCI4P0ZD
2GzNSBYB+wzEO9SujdbLrRY9LruYQiRxiPK1cH9U1mQDxI4GHrCkBFIYpJsCMRM07Dpew3yGb7sJ
6O/Hzw4OqsN+fOPDZ+/faxho6mcOavBYMmwQVH6YUHaLkDy/jInYKKjtmjihDcq/OWOIbPYnRVoQ
UB+8v8oRpV2Z7Ey4gkXEwh4qtAplnJFW2dnM/7vmLlSsKm9T24hMzu1CxjcaERr8eMW4Oy5yZ4F/
i/6Rd8NdAjVMUqggZRX+Ji8wYgwpTkQVyr9eUbkc8YOiNBIzudPzuix7NKy81E3dgsst3fgA8pvp
zOxT+xwpzfTE/XgJF0QLbHt4SU4zU/wRy/FQrAZNih0/7KPHK6FgmWpDCvsqvcRu1Urtn3VP9h0r
TvhkMOzNacpN8f3gehtEOCOVoSEqS+nbmx+FMiFT5k3k45Dq0BIyh/2NxdTKhtiWbZlLblgrYDGn
3/RrWi6g67yTTchOYAZ5LBsHSrKL0ptwPP8LpKMsi6dvyUyy9G6K6jcU256EsXfs3e9yPw79Q6/r
6sdQkDgKwILusx/+S+C3Oo6hlgDvcZyPVgwQ273JSNvagUdWrH76rbiWs2tZ90tr+crfUKL+o3Ap
iXi3N+CehdsOa2RtyxiwOjpzqEMwvyoKVL6fSd5yhaO02VRPfDecOQUX909vnW49JnGp+aT9M9NF
bhKfScUET6Pr48PtSqeOgQFXC2tKcWOWMv/MF7n3CCnhcWUGueC+tG15Ad/Icb/aF7qFcciomCBw
2cpvDVvnmgUO6gbNqWKUW4grImHnLdIg1W3PsNbEIWAeXS8m8JyzUS8Va+u/Q+/pdGJe4/gozX4V
0dLyN+sYRI3GXG1J8FegHU1VhWYd7wQ9xxGrOd6sb+CXU+zRghTb0OpcxXF/f13R+kRXDb0SuWMv
uItyUcFdc7PaV9cbZfBgf+UGKvQS+RCQc0GKpNjbL9lPIlaZxfgV7yjngmxZpQ16f+sHs8e56Qry
mMZlZCveQFnlodbLCXD+V/GMSp5ELIvc6QpCKy5Ym4FXY4vYC8my2E0MPU8Pqq02vhoVas4l6/c9
Kt3yUMkUidGYVwCsSDV0tpE3LKI12JOGTw9g/HFWW0KjCSVEU9y4zQgxkAEzb8kdZtmG8N2N9u6t
eEvUFIfeV30EKByandScdgLE01yzs5GKezSgwjKZfTS9PSJdbyoe/hAEMnoEguIWVhH2tmvViCyy
a9jXOsWieEJnLeg5aoXkPHPT3qW2eJ8mkh65lxF07i6i2068chyB1So5MVKUebcJ/kTq3fng5pfW
84fO3wNNOIFljNRDbBd1oyAX+FZw+Qfmj+r2JKTszrcylvaYKLtc5E6jNDdc2ZvzROYnrrEPPOUl
AcX3DBi3AxXtUyw++JUCq47aLiqjCgFMm4a6GayuBvQBXqd6BJfyow21PdWpG96i/yPpl4tJ5d70
ikxpFRm0cZuWwqsh965kOApWp3nPKXzPFXoEy5mBVpvYkf4miDb/zxUPt48474UQkOCf9LQ+ywz0
MWovXoO3+b1tp+qrdm37K8m1dssIGl8bX/PH5ftVDCa2qD13Sh5IDR1M1qB/OxzmFJlilWW/T3Zz
V1ekH7qgN5/YpDyft/QUQFsdSQkcuu8YP5d1KQo5TmogBOBoSu0oGQxn9jHAczabvEodMhSAk7VM
geQ3Od72SXXJ7muAY+A6COifrEssNIEu8+lAODetLEmBuwgqMEqe1T9MEPFpkN4wzi0l5qxZwMIE
QbUCKQdW+dTkVFe87vH71XGqIrYFtS3zzGbFF2r7yThbJfOmwI7toBTTxGZNH9gmIumjAX6V60k4
Fb+mQDcMNEFwY/SCjjIEQ01rMvhObrpJ6HD0UBDOhHNRLjUT0fMq5iD4J01pok/nM4hbmwAymdMk
+44T9E3h6RUMiOAdRRr0B6C3eoxz/M+eQwWMDST+Isv2dsP4tZ5J/losahb/LDzZFRVvrc9jNHWC
1cwTOhJhPN4zQnHqBZcrg2VnWwS3UsnhD2XjojFrIhEUeWh4infEdMoNkGGcDFb7s0PMRAqaivvT
6ULobM4mTce2wzKiqkV8eoHAh39bFUdU7pxVP7iS+8b+iHnOl9DGV+5pGLeBXxrW2oi9cd3u/ILN
rVvnMK6iD/+V5Fbtt6yCQVeyZveWfUniBGBwed4CzpP8hEIf3hra3pAKj9ZDtlSlo9wX2qrvKO5E
OKxsTm+TFbusi0oMc4+F3eV1jdtpysADGx2yXL1PR1iq4npI9YlSoq4VuV1pV8RVfS2N+ctoZ4nG
NV5tWNewi/RGkrISDs9jhUnZlvtxK3YvNyk5rgTwOsP1uWkDZ9meMUOk4cirDssUmA/+mFy4i41Q
hcu7YFDr59CvNIBjjFck7DxznXngMQuXiYM/w8NjflaZsBpFCXdTCllqjVInLUSCDLE8B+A8w3QU
TUM0RME3Iup3P2/X+B8SR/Oqzw8HPKmxzuS7V+IKQ9kugEuB2qyV1JNzdFCDaxG/58Dmh8APLChd
bWBeVeFWYnQrl8pvZp/7zf5OUxGF7r+X071Gmk4DYkWvq9Yy5dyrTobFO0Fh6W2XWJBr2L9UhgbD
T3WT8sRt6K1sSbgtcYYgHwCO+axZoePZaDC4NvYYRMC8qV2m1ya/e/NdxN4lqLif2FIv9bY0za8r
JK3tvi6ur567wHacgv20YgoZQJr9Nt0gEUqz3lvSRio0ySggHzxmKdGnEx0QoaNbbIBZb82RBwaX
nwkj+ul9f8ROJsUJoniyjSlPEmo+YgqhSMOmCcBO6zMz4VBy5W1TB/TOBTk+fPp4vxM9OZMD6B2h
xkBO2TyR/PRR1H4FNMtAn8y2pOcHeQlAfVywsc9Zgw2tKglvlhcTd82JHNNBrkFcfaKsPL2e1YOV
PxlRW+UQEjnAANZYIRpstas/cb8zoRxm+0PU3Omla1OP3Pd2GXWbm2tuYM3UUWuXFaz/XbGDMT1u
plvpQs4rtl6ZUuZ3CDZYlP2+Tj6r5yXYTOaTCYpXgZhg4OL3qsjXMWhoxkIbpnH207wjqwtLAKDD
16bkRcoGo/rwZSMr2nRkqir9nheUROONEnAahxyfR8g9XoRanJeQ45kkFFLcX3WvFmdl4CZgZqCS
Am9A1dOmaTW2XMgNMCVHZTR9UnqHBgMxjEkwZWPJrPzv459i5QBCrmx6vBNYqNwezz3iuzXK7ILM
yKgxAJ1lHqUVwOfsLW+xzHpU6bMI5MII5HTuAQ1mkOZy0WzvbQ9WJvarCHwJsRhVxEmp0NWeJldJ
KQ85QijqGDuNUPcXlDJPJvQ+2Nl6LOOEwEl7VlcxF5EFXpntNmViaRPtPfI63Zox9DRdF1eMejd3
Fyv84zXM5IyJGdMsAdr4g+j3vRT0We+Gp7XwbX2D7m1q01UkWtm1jMIiUBp4xGTXXA0IJKMbrkaa
WrYdnGU1oUa9Eqp88xl1t7bW3XFOfb10JPAMzpDhRSOrleTGSaw4U8tFyM3wN5MsitzPr2p9mHSc
VCCg5i2TM5xGzlUlQcL3dVjutzVn8cM1xvNYv3oUEXUMO4MioK4JKMFrebHu88VysBsSoXkRhyql
Ka2p5KX4eDMgq5LAfmTwVJkirpuLPMnFieRHTKrrzCbpWmTGHBlIKO3DlkqpRbtMrota3Vi/JluR
8VXKLkbugpXh3+MzRR1KhdbIMf13Xltdcn9m9Mk4LxgLjx7V2SjokJyxSUXpYLnXwBqNzsst+Jha
ms7akkLCv75g0u0ExdQQi0L16jKvOh0aolq1Af97p3pL2uPQL2zBbllgoZEOxqILDqnHOnI6CLhF
/xWDjSfeH7YR5d7geSonZityeZQ01vwNS7PtclnRLJAFHjIvVJKIuJ2TQC6zmAkg+OALTHGLO0KM
douSW6XY2VZti3rHzZ6D79j9MnLpCF2Wd48TBPMhzzmbmIm1/3fltnYUxNZdWv+Z7N7I6IWrnI3j
mfbjT+w5gyneJ8qxm0hd05VdKjvkXZ1gwb0MqVjY7Ec7KAdLWvAlFA+e5UwD/r94Xgc4cXUtA+AH
R9RnWIE5aesQYSO9bJROh6oL4BoUFdT0vL7nkZ/i+6EvKmjbQ/+clwNBxuFOA0rk8AYdXCh95cMj
SMwyf2lSEGDwr0qvkyt+qksl8qdE+11CGisCO+gHXtm5DiieY/KSUji0VuEgoQX/WziOC5LczoD8
rQKPkgXZip7jfVwF27V52nst4RjFocd9Cr4PMKxlwM/uwSXmeoT4KrjIn7MQqxSMI+A3Hej+myDh
Tgy0npnAwu8Da9PZ5y15Gi3mrZ8aE3g4FsXRMVd5VXoobr7+FnhKOn0pRPtd6aAzSMPwjPrCC2+t
dTO4ZsTkMxMfdOXS0maKuBGTyoeZwkO0v1T7WlpyiSmRLzFRZGHo+fZ+w33upyDVpX4q8ju2tahQ
y2l8qXmYFPVpar885K/uAvgHk9FspVSDxcsnKTXQGhtwg5GNuXvOJ6bepDoMmKk6ktw9QAgqRJrb
XlGBDflF+KSWjm/uBPs0GW5CLGK8v1EdZRYYoezJZbfTCppEA+qnc3y6D1kWF72dhdxN0VYJihXt
A5d2kCuj56PQkIaNPF2PgfWvJN+KKr4/IPFj3H4S2Eei0lI3qqkPwXNbWZMC1x9vFMz5goCcdt9A
d770V+wDQTxacG9/aH+IvLrRZgvMDSKBaIx2oXrC9oowLxQcgzQgfCwG+ELr8bsX5LLawdKRRP3o
VVqx2tDx3fbXDOR+oSe0z4VzzFETnBmhsTxhoo7RBqfNHe0QdP58VZm9+wZjSfUm1T5XNFCj+XS1
kv03muGfWkrNn8YewLNAMY9r1BlhU3kJzFGm46AkQbMd7zfgDky8d4yVNlLtWDsj39u9DitqdVKO
NVL7FrVwMoB7hS/ICM1HYqxSnNwls8/ptcpjp580Mqq2DRXQJ2nddZMRD0hBwnoP4UKOuo83TcdA
zbKiVy5GcmC0UxJXV2Eakfac94w9pWP7xIsHQHigHwh45oxEzbABWYHKdq3XO7kuD875rKq0rQXk
xYRcpeSMWvF3vH8NANn4cF4f4EDHIxqV3UZV/VCVUfpYSaeNPe/nZRAcrGqSE/l3JDLO2God+tBv
HUzoSqzg9s8E5H6B4AEFzuLZqhMds8+MIYQNetJKhNX5iqE60vizvA655OZIPfKlveOMGDwvD3kS
QwseOxVOGzmR3lV0b/k2av2JqMSmyBSr77plaBZLQhQaUPaw3IcNZGRXw53xRnp/su45HwBjD/2V
lHQPkEeUmUeEVUqe7fmbPLPmdhnuucHeJqsyPW4hP2A44jm4b6Ref+qNIl6X+YDMIneyz5xS2oUW
kkyCRwNV+9SfNMyxlmKfrR9grkenKYWBmGbeJS91wHPySoDhoGIjDwVvQY8/y8/l5MB2b1hwOhpA
lGEUTzit9M4XimZD4Mw8ZEmWQtjoik1aMWRPS4toJUmREyYfEVV2mnLPz3BdojUEdwzbBisyQiAB
vhGlNFTjKxSExdb7BhEjN2yJ69Weeb/aa7JR9WK29RMaluZNX2MsOFqWIjuppiCx4FIPcyT+6n57
pJXgvD6fnpxOmXqgR5XQHjG0yYZcK7B2ymtjF5nxl88MX3RZUg7jNlEuws8oCCVTra35d8ltJ6TM
Hb398hEl2rlItfaqT/LuiGHmdgDLXREGVhdJ3rREPCCqwQFgyI/cd0gJMHWobkWNsVNf/HIVR+xU
15L44j3Q1l426IhnU1TWeM7xx5u9pBwjFasOiKwSf/Mys6F6fij3buzc3uI6YkcQk2BmIM5tppIw
UFOlf4iDL6YC5dtz4ouRjJItk0zTcBv1UULGr/LbrlRy+miMdCnuk+b5urOrjvdVMmdV6kxvBYuz
yXYdZsijVfjhNAobGaJ+hrUSw5taklknXgCqceW1yUKGpJMBI7hmSFXU+dFG3fGffj7qHNl3ffnE
Jfg34kwWGNOeK2yD0zCVySo92A5kVUHQmMZLTUFmIHwyL8hhz21FoqRMCBwTraHeRRuaGq6po2q9
/pbxZR5QAD8DyoU1TIZDZjKhb8CzIdU4Rlpdj1grZl/6Wp0SuLf7zQw1HrgEv+7tyl2XNfvc+hZ3
io+n7/BCclxfH1SLi8sS8k50XUElsvg1TCNJ+c0go22eRECl07zxODN2PDbKVuE8c+JJZC8Yvg4w
ca+lRnBoUxEca//8cVJ8RtwF2J7dz+A2k4eppBgc4HN0vzww9BwNgS345lKJcHfpo93Mr0sofpTh
v0nuA7ZMaLtubAsIpsaU8PTIu2YDwf2xC+wuKpoX6RDJiRC7NtavJhV7hHPBamvIpNIPFtRebcAZ
5N0NoO9t6GCYo2qA3Np9F/bQgcQwD8A0Bk9xs6lvxEd/q/CfeqoLJ1qb0jZr5ljJKhlX0d93Zc1p
kLMm6+i+ez8cGHYiJUjUTj7LmehaHsufVrdkHjUeIOrU647brkmV7qO/SMKIPLs2KQb33adQ2SRA
Srtwf9Ll6WbD/PVfaRRf5SDk/IOWLnkubUDDRWn8L6ipQChEvYumd2DrvSJzI/pJbYjB5nAdFyhr
8vcZf+RyZTWB2pTcGKsCkQDByb0CmF2zjWqN89O9bHTvAVwdq8/qfS6KLdJ/lsuzEFYuTGotCQ+a
lqRy8IrJF5wOTpiLa4/rS4/35V+odjckX21Y5fOtP5kOocBiri306oCsC1h35JdPQ+BvUsMBo4dW
uYGgGrP9MNUg7e6dbvxs006fRlA7fOZ6gaxCvse49U5V0sAcTGeOb9/iG6uR8Ut0JKY4sQUp0GJ7
34HOrzuKvO33FzWLONgYAyc8zqmDVVd6gm+wiE6qYv5xMaSVHqFbOONUA+g76ZDJ5QNlmtcZGmly
wM93nfreC9k6csuIEwMRpZjsX5nF5ON9HOCiAO3cBDRkf65W0k/Jd/rnajOWj0jfy7dtWRdzgONB
d/WsvXOpgvjd5/B2gRJ8xQOM71rY0BBBsamqUfJkrFsq70u4X4z8QUprJofHmNdIqzPm1J55iJ7C
Neqo3MsiGsPt94R1Uo1qx2gTbO8wCGGkZH0VDkeSS5Uss1mjNHNujChKrBZ7my0XEv9yWCy0m4Pe
mkX9ShKr4nFbQ5CPjPF9CPRD/PnyPIHdXmy2Fh8o3KOaLh3IlJiFnTqzjDHiwjLpTXpSGWHXffWZ
m5gEHUOgG2/jSm1KVPkAMQBh00E/o2XkiuRWs4tG/nvF0cTUWHWLUGtC/yPnx8UULCA+rGyR7BRZ
UZBf/ojAGJpCZQ/SvUbh/xjbq9hzKjJU1GEF95b0psWUsdxE+qB6OnzQ6LWNtBIKZfwla+5yU+Xc
LLxfUriIVI/6VWFsx7ps2VQSsvgFEaMo2PV6bzDgmvILFZwAO1btqBY+K8+9kcCmGrXNKk4nrBP8
o7sSLKpJmEQdl0yXkFSLjyBHIOUuLl1gUWqM9biueqOfzf8Ew7eTXugr35qYlheZZHIFMPmhiY6z
PixlMXl9C5BSANcs8xa/9LbAUQckSebm/EIcxP6La1PLPzZ99HMPJXvATyXj5adOm/a+c4WzhhD6
G7Si4WSiH3p3AfJEhFTpJnVNzYUutgj25Ai67147VV68uh9UedJquFNkf2fR5qQSzn8lS61VOLas
jCjwEy0fkQYDgSfa4gK25w2286XouHUz0nODjWt/XwZ2ChfmVC/UY1SBAHJO/N/Fzs4uq/SRJD56
sm6do2EuAbW1jfsN6p6VeXUerXcTbLcElXb1hd+kxYfTeH+tqqp4nUvvc9NJ0ty+1Nysb0dOq+3Y
y5kB0k5Hf7j/2hkhKCc6USe5+rbCehqWhJ9y4LpFHlqkbuzre7cgueRHHBlsyyhsvrG2aclMMdtj
Ml0qog2S42EsGVwT01wavAoACPsNzTzlvzX0C/Tk2nJRVM4974ND88bs7fCixtd7MC2GiKyeJEIB
9Lhkj4QWwTEaIXr33W8zFHYLq721z7QewlyE85aqfJN3gzrgmUi7cwsINssskHba3VTrFSo2XPbY
tepLCYddS+Gwj3acSe2xNmTPwehAGyHe9DR2AjkIa039r4rwb2mzfu39ZSwqwtyp5Jc1yCxMB5U4
kPKECkx0E+rMzGukhY2na0nTYJHpn27dGeI7461i1wBQUGZFetrJMcRDZPwb4AWlggYn3jkv3kBI
gsEvI+LGHzsy9mU/5tmxO5GqFnP78AjFIU7pd3HFMdzBv2JAkPDNwiyw9Lt9ZRmjc9UwVVMm4NOI
c/qRzFi39uqI6B9+Oekkp0tYrZRVCs6uw37pvTRRhmZWrOExWn9tyBtF30m88TahBiwwMtU/Rs3I
qOwRwi83+5ymzLJ+MjkyaWxzNCGOkr5qLAQs20dksF0/G+1HcmBbuK+NiF02E7agWaG9o/99CZU0
+sFbNKyDIj5rhFx/NgjYLCtcz6FlHDh9jTegxlg5waMbepS/RHR43EDRsf3jYnrrZZmzSLLRmPt7
V405dBYOxDHeJXbMGh3HtbGm2OruKZjHsYTdLUM9xUu8fGRDIvlwTndToN3HFuUJBPWztecdkEnE
CCrw05C2az50TAl5hzJEOa8tG5B8nsTPdBpsQPPqPzkl/p8CD94FwHMshNH8txdSDUN8aoPveroQ
fwVG+y11+QUC6mowyqRzScJMCY1TwGG3TQXdDjuyzM+tqgKFchxDobU2EGTpSY4Fl2wc0uNxXGjr
UCqCYok7IJSiqefDEvLD+fykO7FzSJXZBsCximjFDEon8Qs1/hC5s2TnjAk7FCyROr7Ktrp7LzF3
8ndjolg9/z6NMmz63hIb6Eb8vQ0LPvrv0Q04dyJdKPke5fccjZ2vHFFEcmNFk6MddS7AesjGNwt9
WW61/qoAnkxf74anWOja0EjbbwlLZyn8vDaMdTbn9EfP3IFcXbaw8Z6DIh9s3GEQVb7z6VaCA8Ll
dVpod3sqSzq7BMQ1XRLGJzB9X2EHk08lrp4KzCPHdlqUzjMd06Rdm6JE28EAaLJOtT6+55HRqHos
Y4Dt3f6nQFVbcqFgxnxFbvrJ+WwQ5NHgLt5hMXn/mU/TZ0V07hAd26R0cDSHJVWUVrJhGtAWO1tf
ZgfLMLl7arNBUjwcn3YlCz+1jey4+crQWp35++1GtPVt77MHzbBeq8SEp+pkywq9LwbDPd/Oc9zR
dTw+X6g+W13Y2/Zdp2oX2qA3W976ddgw3u8dsjE6LLSBzffxGKRsxTcmJLdZI1lIluv1odLvHsjj
Xf5F4fniUFQKXKUN5ahzqFjmL3I6WpTXZpY8xO1cC52W+RrILRjj6YGnXj14nXXzKoPehCapF/py
NIKCrYNM+H4PfNmQpoFPecIRjgQeJx4d4zL6tQwgNif/UgpbLDDz3cxAUe8+2rcxLO09q5bgEPqk
00rEtbRBREkgMgKxS3meUOSkMmOFw9iihtJC15Mto1k4HsXU27fde2jsRJRBDffYt7jn6dvq2YS0
iyRMdVLkxxHXuCqaCxlQlkSiH2s3hlqUL3IeXSzLp7evdQqFKDqGd5541WfXq1LZZbUulx4WSLz1
EnGqc83l5e26u4R+ctJcI82nNLpRu79GJv9VjJt+1W0ygPUXBqO9UXabXoAvkuX42/FhHkayiCgc
fnR689EOUE4bgsCDXrv8Ta0TQZVFyLr3+5uoLUBRcU4/rC1WLERCANwGESm/e73GNB9v5S7WdedN
895lA4hVXubR4E3Oe0XW22IfSIE52QhA3NWRotsJTcI3M6ritELrBlZDZ6Lhd3gTXZqwMt4vjwVW
4Mw33Dljq6JKt8Pe3am3AVBf4DB4Dwcqgn8zrNBOmJa/Qr5o1CRRSpt1IBvGTTg6j1Hqiw9dBz8c
anF+m4vkqsqwwuvqhFy7fRKUMjFfBzC/yYhe1eM+DHOiv3QdnGS+6lFP/+BYwTP+PgBAm0lQqMJl
z0V7SpZo7oGLQDTD0dCHW5uEcxPrO+1gG5cUclbSy5uhlRQp1aB036/teSd4mJ+mDc11CLkW1zDW
FeonF+0kZDbbb2jjvwPaEtnDBQVf89B7qsFlgrx9gL/KAJtfg5DKT0ABAdRpcsBC5WghrGAFXCEs
HwRHsy8TWYdIY7FgjFqW28mJy9+kycO+akjlFLhcyRsM+UDhtebJ/AW+MaGaVRCrbLAk0SBoDWtN
xxwUkjq9jyFEUWq3wJAUl0SL/d8Fi1LQUTd/ghkamB6sMoE+wAc5aiQW019S0R06nvVaTFcV4Asj
6AKWdy9qQ53RGpWxHnD7heCXpZbq9VCNSJCFLqqFC/4dwrr/W1Z5bUBhcI8B/RWg0mP2FNHiGlqO
t8jRWzpMt6VG1TjoR9Luoe4XlFOy4oogH69AD+NLRq09bWoKktYvk2Bj32sMYwoVm12YgYF+lAr1
xSDWhUl1JcyVwMyIQ4/NQaGnCO5V0TaNCEkV9Ijwh2H0SOHyOXvRjPR6r3ml+9+Ea6VFq9tL1qk9
+IT9Ryea1/ZM4vIm8k686jB90K6+gch509Gu8eOjhMeixSifUCC85fYP3aYTpPufDv4urlHkr0vB
cHUFP5jEZOwaUUbpayTtEV1uaYKkZVVMiS0eZoHjQKjFSP3ZG2NGKSeqVJAXoRT+oybzd7NAvTef
pxOYc31nMFf/MLKDtOtzoVFBhPVSSiqBBVOgdvq+DRyx43j4+fSVZyJ7Q3JTSv4BZi+GK/ftaUU3
Fd90Pz3qmRAeOcC+Qdp6VOHwAXk6PZvi/KaO2/wIwxnBdPCsmbRQyO7CmY7aBPfYr4y+sRsB5rmO
38RRbuD83/ePzvo1d+Nb9mwxptqo1RT+gwwCC7oFUxBX0uRlwNPNhB7n45wq2OfdxJq8/y6oAhYF
FsvoJpPqlicdzShg7kyXw7H+ZOhnMiQzCP0V2D20AEe+21L9ICNNVBEk1DSMj2yh0zE0VWyrZXh7
MbgQ0LXCqaQscU+MK9hAXZO7NEoAL713dMQjFDKwlLN6+GqZf0KxOicTbuwaBhH8oEerixRpfA3Q
0oeBHJiK7QPRKEiIH08BH+o1WajgQAnYUYLi/NAEmqs6L/+UtHBZ8U+OPk47q6OBDwzR6xlP9YvN
/XKfvuRyT98Kjz3gL58/DLXe7j7/nNHDIIP7yF+BR/of0fI6DmATrdIpn6JtVdc+LgKDhiFkm7Cd
0GNvLujbZaxymY01pjPsyUEXRiP9vqZ9D6tTY15OqvSmarB/PBFZWHckRtdqqMImjHX+69Irz/vd
T2y6fwb6aMTKhq0uShYrohk1IR0kmTVeaR81oXdAEC5eRU+vCmqTY0EWolvLqN1aP6hjzV8TtIjW
w1tJsq30fESyum1g+tl8NAz9vJ3T9lxvHntwqca9mzVMiewiwdbD+vWFazYt2w3+gqXPUV1w/xTB
VJeB5vo4wvyPXpDhl4++/fsYzQcgI6gkH+It9kNYLeXSiq9VDNufGNhylkwig9DZYkTVMi0RRobo
ZdgnSt830a1V/ZdDwbEItW+wQ9lBz93ONBE5jXBvytytTMnEtnzsRt79rQ+iY41gxkQcIxfLoZeM
KpkndVpFyEPaK+GDg2g0wwk+DiuW5ZU7neWc1qvR34fdkZW2spEe92uNBnbVbGvIW3wLwfvEkuUk
36QahyyNyV2AdRlxMCY+hNnJGOHxVGs8TuDuw/Rtgu2cD1jhvDVN3OY5W2+4L2B0kGIllYxd9PvP
1redpPAcSul5E5vhtD6iZRReKsWAUOLxXcF7UqsG4XnLSChsNr2/A211g6Iyxlih0cjKXk3flYpR
B6lPUCK6KoFOi9QHqCfhXVgvTh+Iz/ATq1rhbdztDE+MnjyWnA+YvLlESSa31LyJvP9AGmkdlGKo
RLBTPS7LwQlDHtSAaQivubKVWaytwGiOG6j+wiZ16Av53xoCNz1KOAzVn2aNXzNSTem3l9ktbqXl
99lwTLDbUe2QAiGlx8DwMXKsd63UoBd68eCKoOe3YgRIB6/ytr6VX0ADpAe8fgB5igPHzM0qMOPh
YXYqqWGW7+iRuYrywmtFJWfpWLDtvejvOu2d8xm12NtqO49oMQXs6aH+ZXfSoGLgS3Zp3nZ9UfY6
ey9ax+7OGkUZGkW3r79QyRcXci4ffB9x8qoNBuLZ0wAw1vSG76sdn4F+pz1xPSGrSqxUMCUwbE0t
M/+26j7Otn5l+R+9fch53pF7ELdciZdkJwRr37x3dIutJw95zmN9Z6MTXfXeCF+pcl+SOKB49A4W
h3UKPibTaLEm+5WXfhf2ta6IkFIa4RA8WWjkMa6UMsm947y3HMcnex4ZHf5WR3Vp6838mExjPBii
VOKz243ePMII7yA6Dhe5WHo6HsCixYaa2mGFp6a7UVjSZ7+PagZO+w+0/BPvZQF7uL3EjJe0cjca
bLt57GYsobV6AsT+OyMzppVKDGyTqlIcNPx4GIU+p4QI03I8D1i1wIGFlJbac+9ITk014HgdcnE8
F7Fxo98GqaCD/sUecDa09V9xzaERToMyCaBXfH0deA8WpajEN6TliUg53PMmpKO9hq+4+I/eeZFC
+TAvy4dX0EpuGcPfQVnZcIOpoS+fPn/AuYLt8p6rH9lN3R7RjMVdn2SAjHChScEM1MlI7vrFaDsb
RHfpZvINiIdZO8Ns7cuFWrc3yEBBU+609EnpbZwVwsikp8NR+hWR/NGG+tFkdXYdoDyr1FOjr9PS
4ZGmRZUGOlJm+n4bQVIFztA7MOuM2cyuTTlts32fAT/Pg3nfmzNG5WnwlkDAHFqo4GNGEm4iGM49
6Yli5qxBvb5fKkKROKCoTXd25JfjsqxDfbzZXs1I2SVgs2OBFvZBCJrcLXHE9ShCCbu20GHhrmQm
QtEyakuLwXMqCrCIqQw9AqmsmuCrIRNso2o/uwqtAzCrpILn6zUQ0xpVFMip5R+nBDg0h5MfjDyr
n/zRc+rl3qIM3aSOsu98r9tPCSVnRK40YsBpyV1/wPLPYhqjU8VFmfB8YVVTfVT6NIiA6vHaeR/f
zq+LKPZCB/qU8aUYnFik1bEx5tZpVe2onCJmg0QC8r9r5rduhZhDDelaFLxD6/qV8CWJfkeu1+Xz
dJl/D1+Ex5s9GkMnZMDvaxrJK92ETWUTyACK+OgxWFFApXTenCg/4ihmAjND87VpGMK0M2RgH76U
SYYKjP1zV1X18hkhE61o6XzuJ5J1HubdWHaNyuhsUtLpaKGRWOkTDqq21eGTy0rQkycDE/ZdcnaO
nUt0j9gH9EA54PhU40nNc5CV9QobBB4ePB0pcL3yq4WXTf0tBHbE+1+9ib/iloZAeRY3KbAdoRYy
A1xglN2jUTmS3CTU+GR9C/JUyyGOIGezQ8LWcVghezOUDcFkYARKkQsL8Uxo7dJM4GFsvmYFzI/e
GUoRPPxbyzlc+Cp23B4RV+K3frOEIgGtAbu3rcJ/iVwKvUEUKSsnFYjiiwBeaeMYvhWM5VM0g1VK
iLqhxXB7YHNb80YllK14uDDGU9st1EalPeCSZgADrLHl6n66/7EXf0BAMnozDFCNFQfG6FYDlklu
coWh7tJjCk7o9tgAHV3gotUp4eW/XlFP+CMYmBbdrXGywiko7MUPOciMY5b85QS23/qTHFGH62F8
oKWaAKNC8jCKAEMK67c0qWnf1lwO1eddPWrcZYLfyPIIfTj9WrcsR3iKsadw9p1LXPV3S0NZriV8
FRdD7Oz7q1vxBbF3yIOCUBSckcrz2CmjWU/PnVhPQ1iS4f1qxAvFaGmbyVjjleMh3gnTukbvDiar
boSYZVOqsLMZyK0BqAEEaTJa3HOLfQ+ecxiwDJviFzj9D8WcwDW3ybsC48iH/QrGUda+RvIHEdlJ
rrmjYlncyCEeqJs+AjgeRKnvrlz5DQtK/Vjj3Y/8UvAiVgmOPUFCN0KmPwNWcjZ9eX311P/ABUG9
rewIzDhqrGUV25baf/ba0hBKWGhnBxOm7IDhpnk1GdIK2A7ii59D48wI5yzrpBZGaWbCzNJHWdET
sJng0OhJkflNqhRhzQC9VCDYTAlffRV27/e6mJ+umHD3I/v2PGq+EU77gqhKiqykPCBvU7Lj1Os2
TXZXGUFo471iraKI/L/Rlj6Of85DW+GGq4cwIgTl5AWV86xthuYLOXdaqnLXC/AbLOqG6NcAO2Fn
AoEX/I83IHOGKrQWE1/cUHBZ58KzUJP81BY+QHgeD1eBphp9rnizxMj72MAjbOFbxEzGP1ZQwuGd
sW1Sn/AzfXWaUZbGppPiKkRczIkyUz2u+AJKHYXLSx56d9IPldoFfonGgOrB9m/WvrWOu4zDtLsH
5g4nk73quvct1FN0tBuK3X7fqV3y6vZzsx3g71XOgZDPKaWZTYYtsKbYghYYXwIQntGQEeE66M0l
duRaniEbpclvMulIFJotx+eXBq88H4I+Uu9p5lUnub0g+hJuu3QaRCZ2dikd+mGbHzCB9rxKCMQg
p5H5/Yeai4O0Jv0cqK1BUFYArUMo28dHnydWkzwyT08u/zc4TNiw6LZ77ZtswTzO3QIH1GwpamZG
isDp2lYgJkWmDe64QCPWaCZZJMPwmcYAeB5SfPHiY8Lng6AI7fVGntLttnTn7NXVwAoUrxutrTOH
UfaVr9FWIjHOitFtbM8zpM2jKLiXJssA0r9l5aRspArU7w6m8PaUQe6UxFG4VF4AL1+62kcG/f6T
GhV3JJA6t9UiGTm7bB11Q2pOTx0uPv2rZzo6YIZ7K08mHY7OsplhjXY0rulWlTVjqPke9LQNQD1B
3K8gD5rVt0/R99D0tzR+5YyHuYbeCDLMnoCp0dVrPbrB77eo/4Asw0CUTK8GX6r3FOxBjDMxfLRn
oguu/WB3shlM2cG5fzqFe9DTblK3C24Wp1NufufeaQbwkJLuGm0LfvLw1m6O4/R+W9Y00W1IqQ4G
Vc4PKGzPfPH56vNSQqQYhYPbBfelocARhN03+LdUl/hMAAquTVQa2KzOfD8/JSdzrFrmtc0ZowI+
OQ/+a2ftRqE0NHSFDRECQCVzjNtny18XW+Xdugsrln9Gtu78QwqPj/En53VAEEMm915iNxMrNvr/
s87gLn5fAzPGKmEf/fLw8gbfUplPyFscOWvr0OoGiHcNZ7k1638p0nzv+jKFXFPUiLmgX+08AmBY
ty8MYINcSLbcmZJCrsW2KKNOyN/PHQVsyUe1H6PPcIM2qSHtGRv4RURziP8valE/N9Rt3HW2bDYM
ma7J2nt0ncg61dHNrtx760xlSiVXsugIpsWS4G6KMkbO+Mjgq3fIAHXbngorr+S4vjbjmXqlfsad
3okr9F8yFavzNnkKWf+3zYac3WAyEyO1Dblz2kYWGgnvwALUikJ6tQ0ci+spIoHngf7FIz1DR9ib
IZN02Xt2++J/d3+0YN5hfO7wv6L2jW1b51EjDlbWITUUWG5Jy06jWYRYYEHX15zvxgCr079WAKzj
rfx8lS5ReQ8xOtBTxmPdomnxTUAOPxDIu905OL3TevsoJ073eazs0ykbpySzRLQbMQMWMeTu3EQ/
nzJmbEPxFb4RxiQkhEfgjms3zpxMpUkMEiZ4KbiZRVJWmTCojs0CqU5Z5RwYkCbldInt/2nQDZ2i
6R8JFopnK4FM82RWlaq9t33Rkb2q+UDsoouWb0sAx+amjG/uP6lkqX22oAE5qBe3V2YOjNDLnEAj
+uvaS49Z56rkDRNJtEfa2BWuUGdF1e1ORb+2BHL793rrPYTBJOjE7VluFGezuOhZZ3QgkUuZMXtB
WNSSzXGSlR3mzNjW27Kdhxzr7jOCsnlwhpNMCTtG+/z3hK41auJgBaiHn0qiS51yWkopY7riH5p+
kJdbsSydpX9ynyBcrzdByS3WvOPe6AIQA1TumaPktlLaYdVSVa3PZ0jYm4WV7AGsYZN1Mi+Vxsi8
o7gn4WdbpsYF58WfMB8Y9+T4JORoDuydZPxHV/DGZo5Z7zvo08OgdBh1D+9MnRtPxs1Ea6TrZ0Z/
HQ9dtrwPsIICepBHxRYZCRibFZ4GWVj/nUFvxKhDaY4uk/2ORy8+7kqpZoPVvGZDtuEj8c89yHpo
TQyRR0OvPWabdmE2O4VhrdF9Y7vNZOvZ95SslI3NbuiuKzbc/7KsCuaa6wUqeVrU3Hk3YDrXiD54
V6E4HQexLq+Qs5lPtg76+NunaiRxRCE/Zbhl6Gv0qdWLZVidxtt9wW7ZyB0QPJRpk3Wvmk96Ufcq
3VvqspTW89E2r7zFwuqOhN16RXZRFgdsHV1DUgSiICt0L7LtCDkNXnPwCi/giM5FzOg3Ir9PKQgD
69LLHXpszkYk56kk8myV16i7/ZO6BtiSDty08izlYrXiIyfOiyjcTnhW4Rx8lKvnJIGdXl4yz/m8
gS15YvaIBu8VSw3d343DCIn6aj5Qrc3WAxrRd9W4ohfEu/4q+xoIkgXVzu64MjE3VEQ18kUkr5wh
FKLmc90lQtRtK0QHl+lAfJO9d8fTc8awP0Lr8KBoAUg/G2SlfjH4VSat28TkO7AoEk3oZMIepMVc
6q+k44a/ujKj4yVwA8/3Ebz0KYavecslgcycKHj4GBSg5pwYZXUVknYFhMZ6F8wGilsm6ylQ/Cde
FgQ8YnhGIolvFIPUgLxilB+G5HCX6Ox6jnmHqlYupRhoEaMQNWepz1TIMYn7u+qRjnQUeARx4YK7
RJe7Bf44FG4J0Rswwd2QyifU6IeCWv6WqOJ4pMhlXMuEmBdTKODxtbH866nqke2S5xrHkhAXWEoa
kEbtYapHSPxvbFd4NxlNkP3YbdGrmMp9rUnzu4Lm0uGPpb6WZOo8vJvprh6PU70/Cw/OkG7sCO2R
dEcpG2WTjo3QYZbraVEShSXes9ETzsY6z9gOoUofUZuCyxF7UQzedkesB5hC1hzDToF7QH8BPq2W
Z521poOksUd32RJDA2qCR2G9CC8Qr03x+JJkElYWHcxcW3mOYa0z8jO4+Xm0Ug33839O6lFj2V00
TpY6RAbCHjkKYvQpbBV5DSYGWBcMw8gdTGVegqhjVkiKMDpYialNmgH+n7M+Up8yF3bKMQBHA82N
9mHHkVew97KKKPTi7+N1JC4Ru12PS7Cv5GdBRdtsZ9zxNQGZWFI7fMZFiGANvdI4FBmOQsY24u4I
f1HQ1rv4a+ZW1A1tWsXzu4IKoHQzc4kXFAVlEBWT8Wgg+eqP0UY5fP7VilEzpGhKST6jjbByPXCG
RUWU2DMhnjmqGvfbnLK2qv9r8jqC7rePiCajo3CBCgyjsCBCgyDuQ6oDIrDWPIQpG0uI6LcU6Wpr
oYvqp+QzxxRmbg5oq2KaJ5pXVwbhIeFR4QbkEjsGEH+XIwPgf2JGJZ+7BROXSSl0E11r0XdFgPBP
7onr9x42dpDp5ui9i3EBvVDhEnr+sEp5xQD6Bum6ObLGWq8kWJ/TEAaXN7cwqPQeFBEGF4rk150F
Trwi4B5sFk+yhT7NnX8sdMpYumGgEvaQf7rsLjjz10q4kR+trx1AnDLMtOph3atflpQit8KwXxMl
YE6TgPwbd9CHAh4ASu/BcfCuD2o5ksiXNCWbXa4wsKWgX7a6uaptMcEbSlYY0J7jyFRO+8J2396E
/uI8/NL3fBMdKUsbyzhlQXQz28bU+VO9gx2w24xROwUI3nm7ffcAZaGIFrqq8bLsOu1BfFpOtBp0
GBDUzVDqqZVw8KsE9NHqL59qYhd9eGXaXSKdj7JpwO7W+EELTCX4rtO3Teo2JUCR459q7LGGZon0
8UmxmYBpJ4SxRhXvMUmeYME4bHFXRWjtJ3x2qvPsnWC6vyg2BUMqHTSgt/dVWds74LpWYeu1r2MK
zC8rU6J89DqRa+dQSEXwEJ7FJh1Umg7LdhIiKTbn6/Vzz2/IR5088sdBwZ9hyu3lnZNL1DyzGWgD
c6xSBWJig72zZDPGadTqWjLjkFJq18WLHvpYBaBJs2ue/ImpgeGvFc6wb5dCgHgagJjejy/WggZ8
BTU7YIXJYWle12JBAayVW4HL6SV90DcJ+6+4jOMg7XZCLvljepP2Nngpw1PHJCmXbDUu4PlV3dpd
G2SJ9jkCdheOnUhRKiDy9OwzI6X2eNTmeFl1AL8WQSyilX4/I3K0iWLExLLXhjndd9fHJyNosjcd
AhDgSNO/6GylsjX3JDsMFMlT3om/fJFq5dXSpUc/a8vqNsb0PKy9SczMveHd3tN/ppx12hUjbxD9
WQwPAV3IsffAsR/YZmldBdJN6gOXdWEBRkBLS+tAFEpWunURyfPuz9R3Y8yvr2G+DTPeT2ZWf59C
/c1hqt/qVl6mVGYlsuo5oymTNZ1o5rb5OCvdGenhaKXS0JRvgMIDLFDFjMYG1TSSo1IY9pOXZUtK
ZkJUvfX/uPk6a5XbMxKL/2i+C0qURzBtS6fM3ARQj7nr5FFuT/ldUquieSAwbN/TZDjqEiI5myo3
fA0iu8Q7P1vYdxklt6rVuiHiZgOZDApkIwqLGCeeE1r3uWimNEKu21XwwOI3vSKBPzpQJNIiSqHP
fpS7P7fKxYsAHMu1zJ1McQGrQJG+rdeM765qBT2MAY0Itf8tTitTYUrKQSEGfksbwzNCuwox3h6X
DtEBP3zo42U8IBJTejcpts7uny/UIhOehOckII3mNznU6Vq7WmlY874294bWfIbDvBIAysdJDOV5
J3VSAg7tNtPj7N7uZGFeEAZPZuq6AFb/rsEnA7wsmn5+0i7yMWPrlUWu1JIZ/Rlyvq81Gk6f1jPx
eVloDvl/UHihiGITGAusFQdYe36Rey2QEvzKXwtGlRnjhDrCtbOU0RxTHNRGfSNXJQ0tBu7w/NwU
whe8GD5bdhnkK/oPpP4RVxLbt9TDtINr2+ohVdEHSpWX+FvUQHhCh7Ukj50c0AD10vmxiNOKFTvM
zKBdai+HNZ21p34wIoobw2vqGLNtZuQPOy5MgEEmNr2rxy6Iuvf2mLG/syfddZxhRbyueXJYAMKx
A/E3DT7boSYrOVu21G4h4ts0SPNKk1KI5nT6lpBnqL4rOJVTuFQgT8g/vc9TjPqTQFkzef7RNgo6
oZD4b2ILe0uQj6TjHjTqBizWQOI7LlaZXGf6M22ydJBkmSRywFfE54qYEGcrqUyGGiZbK6Rr0d30
eLw/qjj0/W6X9ANpzEnqDW08j4tiApnk2oDFOy2pMi2YAclszKh2OOk07wktVsGwk0VadlMOTI2+
gshbISqmo1iY824Dv1jVkMuScQk+ggYHkBrmEhVCveEfb0OEVtorURH9Fuqqqa+5diBR8LcJZIQy
GeFiaymmXMXQaYdB1GnEVqEYGjVs4Bt5xgNayF9qq1tHRedJ0X908U8ei8rDwwPQLCgb/KWJERn1
IHpKcIodzHYSrB90TeMhCb0WkYImc77kcZBV87IewTu0dok00rbcXbYyk78iTjscOXVu5jJXvW48
hPyIk7vhFNRFulBB5GvqRlBv0j4SuQOCXJyFQH+mJoXtCWw6q+FoFTjbGGfSGXoJnt6Aet5Lz02O
R6k811SGX6TJ94F15XkMhFsM3NaL91++na11Vy3mH0ill40dHMpcscmpBQ596Ssvfmo6pONsWyo7
6aGn5kxu9N+4gex5hrJyzq9iyixxyGTeJZZ6BLV1eXsedM77tbXZ0Spt9ySJMhviG8e14e//P0v7
UZj7ZxEko+TAB4o+0bulawan9UZKa9wkzw+h1TydkWfNqzB0pq6glNWPxacz6J+JAiLG97IpXmZh
L65/jbDLYxz21mDmqTAcvsAqgVO5Cwnq8RR2KVQEIez+b7q4JQ2cxiVpkG15tm2yEMZk9eDQR+ZQ
CWVh7SOYo5KbkPmPVeMuoafH1urzkVYHL1D/snwDNycEIiukHI6ly0OT2GfpbzjLH+rJ09jkylYb
+c8ZOIktMHqA4VNPhGIu65/Nl57b9HwxmMx88j8/dlXnFVN9lWMCZxQU/H0+mLDCwzfUMTupKBYm
/ewe+wCxh/nw1cx290VisOn+oSty2o8htOYH8b+bgSBtMJBQovg69J2Hr2JsSTmABZKlx+YMGv/0
SuvbMlCRECghCt6YsrAjNg5/GNqticZVBQGy7PiAlTHlg628vzPjXeC3N9J1wTVqu8IyXZcd9EPE
p8BVpyCep3HTsvd2+iR+9FhctyqYZoyy+jK4hwPTprn7amFKU4v8En6EIPbUfnNNEaEvvj1LdglN
x3sjXGI88a7s37hhc78ybf23kBtEuAxIiMjDYSUZsyLjrjycpk15UjWRSgrtLGff2LL70hwMRDUH
vTZou8Dg8R3KZh5brYb1hS87jD5oy2WsysXh3P3w1bUB6Al8MtNeh6U5n6BMoAltEJfHIpk30Htw
+Gam9NdE9qp/TrKSVXoceaf76BjT6Yk+XP8llQ3lonM/S3x7Ji7aY/QlyP6xHdkKriEuzimKViF/
DQM0GkJasFg+xfmINym1vOJx4igTQbZTHxDu/jYw8+Z1/Jk63t8Kgvze+dKsZI3P8rYV+gIXhZii
pIL/+MTd73FtyHeL1JDqyNKKXnOHJFZFXxWxSN9W01LIZpujPga4fcMeQp90ydBd9IlH/qeLfn/r
GCAo95HPJLR4EO7j/6Ca9zGSfKyRwqKWbhXjA390Lk7SeIn5WU6FTB3IiJWAqK8cHxdxvjuCujh1
hi9+DewuR9CsKSInd1oiaVKxXDmumNvjprd4DparobBtiBKcIQ7gmXtr9IO+9qm+9ExEPwZfwpDS
YITZJxRj5ygAwnQaBl52XaGKUF4J5k65rN9fEzRxS8FNMf00a4H2w0zTZfj3CXHFBL0yR/rwVNw3
kMFrEuOyNc57U3obNMZkPIrg1oWkXjpttFPmOqW/g4pKQwkhk5KKE7bDsddCWv1/zUB0PLgalidt
1P4bXxoyVKEgkY9T6iEdlTD/M4py75fqkr5fogfaSkcK9zYMuqt1iUUySPLioRhawkquMYCsJmO3
owCtrCDiAOeHdtL/cQi5IOzF1tGTXYiN+nhX3Sm2ewQGQ+wcncGmt9Xeml55R6zoqYoWj3P9CtAW
wiX49/8PFu+Gz91skkrqla+ocU5/IM6MiPfU2phnYQM+e/Nfz3EtjXXnZcOiRhK28omC2LC82DpH
flJMUHnLpnWFI0XyFa4ZXzWA2zWh7nxeIuzm5fdkDvymjKCHuD0KfryYrJB9UU1oGgI/XZl13S5N
i5nsKu4Ho+rf2zuHAEE/i7Agd+2Q01RfMg7JXulrQII2gW847C1pnbnm+QhN1FFep9TJUZ2FQ3su
H8QGFzQnXZgOByeHbarufoUf1GZ4/EgM2WM4bk7An+QO97Gop0UT3M6tRbo6rRvyQyqByUW9FJLt
lOMZ9BX1TdowRcl1Zh0KUvVMQt53w9F9WwectJJdm5Uzio32ZBe+oiBbToz5SJ4/KyXnQ8LRwk7/
o0oxdOmjFy5R21Mn/ZZD6OQ7jCyz8fKKfPvMIv7O4XnfEoL8a5X8ecLpuzQc2J75dII/X2VmLFmI
PS91SSzmhji2q8p3xLGK5ynz2G0ZK5XSG7CshDIlAbOrUi4/XKPkZYMn0X++6eTTexTHH2mf1N9Y
tJB2VH6KiI4gEIZHrJTG663094jOWtWBpwVM/uegWninoCzQ8O0dUkpgWciCF5MtynIIYrbNF6nV
9kiVT1FRs2KBBulYUcgjnS4UjlSJ0F58P0gaqfi/bFIQ0FV+U9Fqo6F1F0JSoKiVcs70F4KVRPgU
JY8dKj/kAXGZt3tGU9rC5ESSClyF1z7QIYlW0FqS+wpztCF0wrMVFaFBdhwbhv4Z8t9h3EvzE0wb
c4nkLUENH1iYeMWkFeruwkCGdDQsMWu4DmPcC4fjhThHSNnplMly7dda58sJPMP+Mda6pT+70XuP
b+aL0EmZnTTGS1glHgvnhvz55EELdW/0E3cN6qqOK4ZJ1y69JIQo2Q3s7ML3grmR0kr6QnT7YAeo
7J06zfxr/rK+TrubgxeIxy6wxB6UW8bcZb04zjR7dfVTrQSJwjQvZa4lhl50AJbsms6oQmD1rmBX
8msjgWvuaZZ2krixBu874MH383ZdBk8h2V1xD0ud7pXxuN46r7cV0HZdRKclytVW3veENq0qsvY0
AfCHlVCJAVbI3uGT8lhD0TiU288OeU4L/1hVsDMyOQm+TqMWTHRtc1ClRwqXa0nhP6wSUnbvNf5U
mEmv/4A7f0Av21u2r+hGkX8WEXGyM1ZQQW21PWO2LeJhGWd0do7MHbrKCK2zI35DaOX9rkVGQYzp
C2En2I0H2WedKa4hcsu5tPll0z/QlMnnQW9xwWRPOXbnzP+x1jNI+ZN6bfMkIKEOOCRtej8Nn+RP
jfCbGpTGCA1yuqr85nxILHi7sjLRDCJOXgBpIqRBlHAMsvvRg34eqYDtNM4aPckuCMkdv8VDZ5vc
GagRwY9OMSj/qp2tsvACPxb9ItzlbnHWRs467/bSsTdND9hfLnkedyFVaBpWJapOmX6roU5P4Hg2
EhNNtZUw7T4okmWs1yIRvCty8tPtnhQ61a0l/S9ybUU/iVzVeO1vwm4P7MTC9zAqvDzC2X+MpeHh
LuwF9W+MQdwsAUgHccJRjvhc/CH83zANQUFQHKlx/J6ViKlVMNvr38+3ne/msuhJ048eEnd9Bbd2
pSMFO6faWcPTMx7EMvT9E5294le35hyYkoUUwc0bMXzXAE0FeLlvyGuaSj3VhBhJ+Cay5KEyr1TX
aRrbyiszi4uy/1WIdaUH/O9OOLn5WLrJ7YblR1fpWtFx6WOXoM6KtHF4F9cUYpL2Ljm1nw9AEclW
P8qI5c+Kfuwx0O1oDpZ2GMLKj09U9Hc70WxrPceEbd2ExZbeiPKDs3YQwRkEhYp0e4SkwIH8sxmc
Xl5CZw0AyqRBlDSJP9L6JPM8HAZrhdtTlwb9XYSQ0jrHy2theTTeAH8TpVRFBii55NFvvLGC2rC/
idHNGY+MdsN1g4oXfIajhP5lTo9lZ3xLwsHf/48CwI+2mHtpP6RD3NaVU2yRRfB9bHh2g2ntcR8W
uZr+C1bU9q052NcmbcLV2PfDLwXmcij5HVTBbcaN9V8HoHMuvse/L3BH3X5j8nfHVCTctvxkVaIK
V+QEq+K5pQwfdisDmU99K+628g0VwPZ/UY+k9naVLYYLzXNbVDa+UPdsqvzgPeaysB/+TmbRT2ab
+A1iPkHcG5j3Eck6kbLEo5PbFOhYRNsU54mXOdy0eatSAjNtDUCEiGO76p18N0fhfem8lpX/tWlL
/jk4ogIY9Qn5FxuI7O+D3gvVXYuhoDjiiHNU+nx/KYqt1PvTe2O8LqlvITVsRRYP9ymncB5Wc5Rg
izUBIjieu6WQYDohAHIbfMNK/9yGInEOCpy48RM59/LiuEEyZy8rrcMAqou5+kOPbLX8g0uJtnc5
OMyZndIBzILGkieDGcjtt1nRMfWT/EjBJDZOe9u4kahVDX1KtkwhZXRpTfwk6I8p4H274d9T+drs
hK8neGPhYPHE8wTwFIzgxuWqo4FGwg6uldA8f/hzocKw7D6MZO19cjPhGYyf/ehafR/S5+sYVRSD
DBZ4LxvFRRkUHl2SAoRQviZP4OR2Al8p7/G4cQzOU/TG3I7gfJQn0z/caB5rTxMCp6PHZTM08Rtv
sJcdX+HeRKK41vLTEZnK+PRR/ajoaXPbHVYy/UI1hlKtCSKNJtJuJjmS0IPPtRPILqHwnZj4jXoS
iGzKTwKqghJEr7/sNsSPninnmf6edGZeImzwmvxwyddnA2f91vuUniusE/yFr/zeCuh+AccupWBc
saTibyYnfOPck7Uxvl0Ro1r96DXtnqGga/M6OdqABsegpjGTlgZbswAfB3VXq8t3ImiMoq36kygh
7g8eazTBXKF2Sce/TRerfUKRhX4Q0mEFkZu8R+giJlubiXuB3XMIu0NT3uOCYXylxFC8fpX2XbOD
lkhX2dlEHu8d0dcS4oEkUGjLJFCavxj4estGU0/My+QVHJEj4JWMv/G0MDaI/ujW/7VYk2bUGJqC
QCfzwCqgOQUd/sNb2S//vzaznPS2dTafciA75J/+jOjn8IgwYVYueatzyAsaefspfYO2AI5I9v/y
zhzyc+0zLkjgYzgfkP0zYiATvA4lmci0jBLa7ih56mQxK94c5caeXLApYFc65aWT+B6y5vhChclM
+dDp9ycWILgMJhELTWc8Vj9VyS1+CToDjIuqtKBWKjwPmUnGwGWngJZFLKiTBlBSqWKb7tazhdq0
s+sV3osu4cZkYJ/bCmUpxU5VykFMqHBsQkUVnwNVbRmnumXIoX9+LQM46gOag0HH3qfZiWPXpjyG
KX1HDy8+OHPJVKQMVlajOp9ydK8G9CIWLUunWJUJYfMQoKHCyvePFY92q7k0MpQdArMIeqhJG0Xk
ybdbRxaqgs4TY1h/3IpzRVv/+sbc0uSqRSNcQCEqHWT5wxWjj1wWWvzA8vh3Anq7sTDO+bgKFUqq
o8Wk8LAxVlH9ScFhusXKk+zs0LtvCrZFKcIoafdCkDwdjQYG5m6FRObqXNF0g4Cfuwd6OrbqjTHt
BTkKduHktT4D4PRtewj3qHxGCtte+PWWx/faPFZ0ufYckLxVWK3wlrzp4FTmuxmSi+yvaYI1oelU
A+a0qxs+SA/BBFrZ6p5BcS/d08gdL+A8sy0jPv+q/wLtohkI6riIr9IOnW3R16a/vUvcYt1GJIks
wS2u3Y5ysm199NbeWaByLoxlIRhHEV/+KcGMjhK3QKwg3Mt7cPS8otXSa+AaIp8q6FQUhB63VbBP
c2wbYFKgaPxNStltI4BMAvkSr4uWCKSCGSmePNSh2s6BEiUerMfUrncbxN6a5Tfz/HPq787bieWf
5bh2orb4zEr+W18K2HLzibQ9Byym22RQhSxX4fFf9UkSifJ7u967mqXS2n5ZC6hlKjqIqSGqZcKK
0XC2e/9ICa7gv0jbQ+6VZFNjaFWyAP6V35XhWH/CHKAsTYkkHCFEqU3kFrhqQtb09UTphSGbTMdg
WcvUsm0ccMd/YGQ5I8/MDw50L6cxnsaEYw0cFwFRciDfkeceONWBM+kFOdkdHaNsqXU+u3+Jaikm
zoE8+A2HVGHOQSODbGI9Q4b/6jceJAGjlIeMoOnsAJuZSFykOWCg33pm0A6H77xYenAgGFXnqkXG
Xr1L66a8qZtPB3zOK9R+mYSZMQDN+EbUyWTrK2b5fQASR1rnypAcRh9961vG34xAcmRq3mTdceyl
mdb16p1Db2X18NK6U+dBq7imQ9pDi3S3HIyhRTmpMINEzgtPgaE1LlazUn80QafreSeLSxaxuj2J
Z30oYSnfJxOm1GI/9ckfoUuAXf/Lyz7Yl35CdlBfQYRdsXdOtqgqdmyxpRWv1Br5rY0gqGYwlsN/
KxoFHc5TG6TdAd076Df7eZvRJQOlHVKuO8Rs4Omxe48aZVTelbkbzahdTWgX7BVvKmhCAsLKHWmt
a9Kb74wbqZcsPel1YlqglrbW8wpFULIrsaT+wbvT00KEuXaqM5XByzGgvC4lTar5/M1gOVBVFvVX
D7kKKiVzqvBQjTbm928wkFfmsrc5En5+RYcS6AD6yvtKaY4PSChOr05nB/Wp5LJZ6Sc103qLSXdx
+9QaSBpKixaB8CsezJJoy0KY3sSLDfKxU9SVwoQL2AEo67DOeYCnoRuUI0PVmDOz1u/q3tU3Uz9P
iktfPvBK4GtVxGXY83hQp4JNPDbk90OI+L8YjyzxPFB4ljNl7rZf+RWFxUmCyi8U4BHFVr89Q5DZ
nOVpFjefFC7bHew8OauzDtBSskb+EToK6zH4/Hx1DrSUGm3LEaCoVD9SUO9/2PBdsRWfxcnf22BV
j8MaWjmUariXpGV93z/KHPlwGwytXytFNcD8hDdQYSWNgypXmIpn6JMuoukB3N9nU0FBAPZXFKRI
+l0X1ECSLA3+pe3IrY8OP8amjHoXr6DBRH4w7GdJc7CriaoR0iVINkRj9UZzzfbty9klU+aVv7IL
yNl3CnDG4U+HsUl8bSSjbDSNP/tTahQk9wna3tN5vVeVyzgVx3d8VFpu8v7bRCjAmUdjrNogdRvx
l2h7RU/B4xHwvLh67i7M3ia+P5/+WOUYS4khHwkX3rE4H0J8sDbK8OZGwJken2djiRjOgplYKn3n
kNuA4u4JwDjuUlUB8TbYUFnkNwSs2xkmFXxZ/6i69lh9U6D/WLQgzv8AaqGmvlyeLH2dQRdE4IDX
/M5kyjjqTNPF71jobB6tOVNQ1rMTbUyy2v3n5YIFEWNbUX1yIixiqQ6nBL5WhVLNKeOszKIwBvGn
SvggcUXsf/VRVhuor65bPgt4ckKPcXzEdJC5hoAcGkZ8cvyTbNV2JXZ7VCQtYiov1FAw6+iSXTl/
W1huSUlCFVn5Neh7oB1ZjclClg/5jqvo7HUk2MU0itU1+j1YI/8iDtXLvrv5Rf4wMwXytJLueC86
hXfsp41Uqb/zrPrhRq27a2rElZz7ppglwfM5aMRwpv070IIhem7lxoj0+kllY9oK0HrYEfmwAARq
O+fiD/4Gcd1/XV6HMqnxdXlPRLIG3jlelmTSiBh02fk/QjJtDchzeOfdl7iXZg9LM6ILgF0s5arO
VryqnnEI7JfSlrQV9w4OO244mylI/0BCg/DVxaKwGRUv6X+/0iADSeoYDZKZ+4BE2Z+xRluQpLQa
vA09uilssWY0MzVq93wlGrzNVhKPXaYIGkIsS8zNwHVuRyL6zlFj95Z2dizDcc3wprB6f0qdIcv3
HhB+q4uy50QaXfxpX3RQtNSBCjGV1fL1tpCIVjBLaTwxhLqpZD91v2LqlKwNlaUajMdHWaaEzw8H
H1pUMhI94J7eY98ZQsG5kWTTMvpwDXfLLxgxmZNCMYbcpwi2QuzgOtk5WqE0ZAqmNg8o/+s6CP/9
cAIBcNZGZ0ev5S+1Y67zHRxxNUeQc85SZclnnB0pTCU5CWaywZlE68RTjtuKxKrt8usuuwWe+OPi
Js3dEPLZuzjqTGrt3ngRGgLHMunnT0P0hCg3V/+zRAaQjFy3G1c+Gh8M0JZvcBCB9veRMKMkFS07
RjSgVf1tCRpxIMqTgZZAt5WjYMvr43Ze8lbNeUp8XSKAZrxI1h1MA4vBnPNhw3QgLRydJov0xFMV
aBjPPwHKiyJHbfP7mpNeGWA/Bae8XaqDkQPjcXdKLy32MmobHY1EyhbJCmcg1R1xCFLcfbVK41T5
eBjF1RIyPC/P0EbbefdyC0PQXPGHLKH6J0p1OoEPJtdmbDv+rqRDm20+UklOygzFgomvdmeY32ao
7Xs5LWg+3jlysv0xf4dmQFQQNfQBZNqN2GHnD7Uu9RxaFte0r4oqKiwKNjnUqamY0yEqLjD2c5h4
JWMvK9QTCJmScnmQj02nmq6+IRskLEE2l8yPs5IA0zuQwWxjzf5HYeHECM6D5U0RTlPWTWTP0KUh
ABBkURzc1PNiES7LR+XsQNu5zcJk6BW5+bpg/Epsqi4ifFoTyR32hNPT8tANuxbYYCZxjx1p3PDi
OcfOD9XMhe15beQw4G/lh7gcIFz7Pikbcax0A1deGjbyO9895+ennXGABPy35SG3tia3Jkrdy+PS
yC57OR8kW5Spy8EOw4VAeBihTPnys8vuiGMp/VHmVi4wECiKyM9wv37Mr4h0Z5m3U+myL72S62lL
0fXiXIv/8i1Yz37tIRWfSop1fYuP0L+LriHrMfDNPXChUaOlhaZfdNjSl4F804IoxliHBOC18RBb
v2nUSH3BZmbXEiUd2AHqG+JdAk9i+G5qaG+NEbSfKndGR7wjXp/2VOiwjaB0VK0+6DnZnGwma/A+
Nx03cdbampBx/HCGXWmAGKqX+hI2jDqAv5rl7kxq+btRzPPAt35oG8YXgtcGYp2XEJcX26721vSa
/qRSGcwwQjWeWXxFqN+3HLPyoVdJepG3J70lDwqrdt4xUZZrQtp1D1VC8vRSOvcShVLjgJO2qr6D
aEBM54u+fP1DkzVkQkQEZeiPS3WXBNq/RfL8ed8nshcjp1Oaf4GtYi/0CMZFJwCZGY+uK4vgpgrp
NkrHf4nKTjrUSSyrq2OqvmSQl2zGOGl91QHfnzI/l68IE0uwWKPy7eXI7P2F3xVZMI+7Vzr3xLHk
qd65mHadi2lMiKhIYN0xZkblEIVLkObGKigiwhougssmAVmf8bulf8PRotwVUrlLeDeaQA4COK+b
LPXYJfvDjqoQjntxTRUvhr4F/XTqi6qjzggr2KBAGq1agDBc6mmoYWwVP+5ZHkN9BS6KI6h7+JXR
9/p6LWlLogyX94DoVffBV20YYl4lbrw+lai8z3k1YsyN/l9Q9gi0owqmCNK7T2ePwKDsrIcU855A
OmplZaLo7CNbATiOnX6kNJK3+Cidw/B6VqwiBxq3RsbUWtFS+yFRLAf1zra31bHbPVPPPHrkNVNB
PVjR0w/1vPAlPiml7I0lUVstp5/EkGtTHulALhVfSynreV/O7ktmdQs8FV/aeuA3qmW3p8E1ipcG
iBMp9FdftjoGR514axfy5Z7BKXpiBdhdJhFZh65NWVTq+/zBQVtBm7WTt98oDHBcxNS38lv3TBLC
E66JZDxN5GHrljs/BxADetnu1xjdlnJ2cSpoGGc28PbX90JERQY0GDqIqV6fZLXXhj2emtrEuFPn
mqIJfcoHqysT+TVWw2oRBWdMhU4dSvMe+yHVlO+orNlG98gfOIsZQZtsQz+ueBYc4HgyyZVoSdy+
IcT/CJUegSpGJXxF2L3AmLwiVMwiAXPgIR1gwDZ03D0E8eE/HYVsBqR81waqiLHDjE4zjTcYFcMk
upSnbCeVr8TSXsCvo0bH1IRegjTuxliDaY530EPZ5cRH0JGuZuvizVv/+3817wMH7aJEURVDz+oT
LmrqKw5Y9cA8Hq7MSOj+Kg0NVatLu3NUuGlVPnZu2hEwlrKs2+H2mWBB0eH+pQ4mx2LiPnPMFLIZ
SPnMkDf/QhVTMPGbRYzhaCz5y9sCPBXovlreHo3gIs3gfLv5BV5FyteqxUANLfs0U3Z1JHNOfbSI
oudE1aC58dwHAEW0WEa5iCInzWBuwjkYoMdjkz4tmXq84UkprT1X72Apq3+Dxnd6atqCWCa/x3Bj
HYTk77v1+ON3yJepdfl7n41rlMZFzzpVoyPjyYsFo6VPWHiIoBlbfVQZlQSoI42JdNxxW2tRk0HT
2JJ2tIsuOLSEY23pwHL4zx/eTgsdJ0csOi7O3GKabC77NNAsvRBvx/hZFKQBnONDvn1Bq4a1xab9
CXRjmeQJX6iDGw1d6gV5biBnvlcZVj/Mwg8vRcRorsKs8EqbCFV5/pG3XZj5YMgtrEfuEygOjLW5
14wTLqJFXvyTWfh173S+/vqG6NbZcl6xTUN8akquKOgqoe9OwlSEIVxlbaP3oXEsigKaeu/DJKIS
IS2kelqrfq9Ls23Bclf+xrkiUcmjftv0NwqOClXL0/VHSNx/bEZY7U0OC7KgnzT+rda4D+Xf+mQx
ATEIihfeyr974giEnZ536rxzx3B832IIq6Uq61PFy+CrY4fUBozoa7utbi3MWE7s6yk19qEYaZF9
5PCcmZsSmFnmMPI2kkvlgazDp8W6bNFIBbAeT63z6K4aOEla7Yyu4mYbnT8LxPkPqs63t36ltSYi
UIniORhAEnxIvZoHL8MiOv3OgeoHMSeJ1WE94Y4PbsdlvBLLJRYY6XFdHuB+vxHE94eqJmLvhARk
GDuwcUrQmGbG7f4jyGSu3l52nAlVD7mePnz31zJB5eHb6WNOUnCrMamiv8opQzsq7BXhdmJTZPFv
Da4KB+Q4kJdFLL5kwjXbFIX/yJyjdaLDJ5IxjvACpJVyTFYmksCoodAONucdxOcXoiaTjHzSCUE6
iIEHWO9pAIeZXoxRgg2IlBFvaClYN2povmdCOeIKrQx1frMMvOTcS5lCwqV/1dmjlGT5fokExz2/
WwbY1ey334LaFnfvzhVQ0HHT0aeu8MxnEKvaFiM/KQRTNWJgBOc6Dw6rnmhwMEwKCRQLD72nuxU4
WMV1lc5q7D/fMdLDvYL6aojBov7avnkPuqrKUWsA4GA1NzPAHH3w/xnPMFzCFYK0Ety2PgJnnis2
9vcqaxXmNGg1b3auAgmz0klpNvz5trOr9UFk6V7PdEzuabQNnjoho2int2okHO1H/x6u10kN4/JL
rXVlwSpckZCICqocY/vN2/Vj5Wcay6XRdgR1mRDG1NqasNvsOFfUBBpWQJ5ywGhGpy7sVdKmjhuR
SWx9wHyFIstSf5MxrePvFhZZTOyzNQY2gk8Uyz88c2epFk/Nkbjwu/jmeKAk+VIbX1IvCt3Xea0k
Auy5lNqPk61GG+4W+HfaLpfrDdGOYqu8TeTJp62TQotQYgxYrLRX4pJxJ2yoUDPIGI/EVH0nfxne
CU69MiJMSvkZ4yNOxppe9G2KRceWdfglabo6CbyCXNEMtQ/zSQV+2izFPVTBZ4giFwVDwR90uVwh
APzx+g85usR1yhes5DEUgVQDvt+cN7RFsvQYwWpbpKV0kWLYSjQ2wm70bj9XvFGVmCCx9KNYr3bC
8YGxTk1GT73qqkMUXcdc+a9F25TFFR5J6jqu36GrJmqwExKPkQxu06nEtIzwwJ/hWZFQwOa80f49
o7GKxmJG56I2ej4k+VqChXrWGl/BYJb1rUOt5TxiDSipLGuq0Tcb6HU5RWt+wqmXNYzFIGM89ljf
SrVgBD9DyZF3WSt39pX9ZWYgfVfXkIdEDBR70lGi9vFVkcCqGdZLmFD5LCnXa2U1IdAZCEMuIJCi
iUgHLkOpHiBfJxXzPB5FCYW3OZuUOvV30dJx7nEv0LENZU57F1gFa7ob4npaZZIAKAzEEdiFzC46
HoRuwfWakqy+m4aT1BEIryL2KCPrVZxUKrv9L2WtmtEf7aH7F5Eyg5Km7K4tjIqcmm9nKOG0VUe5
pCatRLWlJ8GWWQk2jrMUsBEV4ft7/dhOA+0CRgDeF4QnOSq1ysQH4vQ6zawzyQar1gSkVt/PLvCC
f0jz1x77IXkGqrKHD6gFHqIOe3fUHD4BRs+ZwgkddnmCMi6ZxSeMtaUKcgLkaumzvrVpY50zdANm
7nRKFQ/Jg4le2OFQtiFk2xbw1Xr31ZhlS71XV6T1gQKk86u3kmJAVqnqp06ZFG1GEpfmURv5srbb
92++o/f/Rlz+Oa1A1J9Qz62w27mSGcd7xTYmhfptCBfhDDy5suzaafWxD6SI8brlqktqKWT652Us
bF0pro5wXZdqd8Z5sP+SOCptVFew0iH1pYY4DGXwrNXTBUVZXK0Oht2nFWtl7uBGsIn7lbL2Glhy
kOzalQedDxj49jZeI0zmyWAWfXPlvAUMuSzIq+Diez78KhCtfsiPMndBPusLd5KbLexwF6cTgCGG
6/B2Sf0xTqHUBowiX+iGI8PYM3r5onwQ4w57ayOM5CXtsNp5tmOXItYIxGewkeWLUVYIRC0Fv74D
irTE/lMz8nao0No17vg+SfvSsNEFJmpTMal92VV/Fi2b5AjnXS02z3q7P0GkUPB29Q6i82uyLWlH
1brrDG7dykz13888yRVCE4WGjD2vKndSZByWFgz8SEDnDOrURVwsmq1asHlTI7wWDX/O+eXZCFqe
7IPHsRFm0YqWPrrQbOoTsVkMEox92HECXQ4AAnoIrMaxayYZYnBcj17XVgLwBAAhF+bulytCZfNV
iw14RpTLuRbDWNLEB4eXfeFjLW1vZ1JU9DT3TciJdgB5/CrDXbNgRZG3BwmnFNq/FgSc9UDtFnMP
1eCWheva2jSPKtXcfNIV42EVTK6oIi4zJavrWdEAFUDX+F+JFd+5QVmg+Y7BuWTunG1E3flVeNCv
8dK/a4AW4LESanvM7+Ok2soJt0loy7Re2q/or+eZwfQ3/p1QNT+OZ+x/iOhM7+swlUz2U42Sp6+1
wgUUHU/0JrGRX/8iM2b/X10CPg6hHNIwaqAWB6a9yA0+UqNDDG7xxElI5hbqjgDjhBPqro2CgRb9
n4glJhOheX1n+M897ndLZL10pI/ybY1X2yK8iXGqcNOCWGYPipw8GThCKW2iHAtH+7CFLiiF4D+g
gwh0Z35hP8CtLcf9cHYh/+o9bW5wRdxhxIrOrWa7UW6bdEY4Okk3AaeQDWqWM+OFbre4zQI2i9TK
YCsJLCDNvWM7jtX+8yO1976/FsE9PnKVBFfPVsAwLR8O57lSV5KzFEQB2YR6I6x+blf/RhGx8pmu
6DFvs/1IiM4N7L0tFrDsV8iDAE5zf+ya4C4JbOgyMJ4FFGRBR8nP1x2BWFTdKeruVXiqZWYkQBVK
QS6MsuHoVpft3HdPL5ZHHHgS3o24NJMiw5peAvGFLxwcH6yvw/EwzkmOBn/96CN40QODK0bmSUnS
fPrIjV9bMMcpDfG3l+2QtJq++d5gylmlABCwJEBdbU7WSLarFasp0YkEGIDnqM+5ZiENOMv+gto3
/va6lsHiT809CuwoJlnegcntGUgYb4TLvKUMa56ECpiCjLsOUz7GWmDOTuaT8Jsk6spbT5QVHxWT
ri+pnzEFZ1HUgBQWFcDH1tleoQx7vOOL+I9+tTwrit5SdDhuDf9X3IfWKEqMp3m0TIuxvh3SJG6m
4VjKrUu0922Zj2Bri0Zptpr0/GxWybD8hUgYSNFxpSxm3zb3VgW64bNb9QYO4crE9DEfDj8i5YZj
7j2oWTRSp8trRcjGSEBLQkjp9MvucFQnR5GPzAwFZ5cvs0+9MPJEZYswpF8iRzOZTuejtMZEL9pd
SpKhsUOKHqGVmYP4hNfeaGIH/lLUtMaOe4SfhVJHp2lSWQhiumvlybpcJugFamPhVPnN0YFlifXb
MZkKEMX/14SM8hXRwDtL6xy4A/6QzLTBI1rl4Owv0YX0V2bp4DBo7EwLrOejyuQeEjFfryPD+Mew
HxQczyPv/PRYzy8eY+UCwZdAkcVT2F43BrglhPhfkB9uPSZhcSS+6f/ZYhM5HJN9A8RpPWjx2kvs
ToCT4BilP07Nnej57kg+KiDKL+n3HoPYoz7DJM57rJOkboKvYY4MX+y5Ej02e9rDpfGkfPl2sGb/
icH0P9VjvTeTci7T5g8p5FQx8mgaw/+8SFWT82o/hhGbGWaV/s9ScWNgiNvos1wj15wmAJJwiOZo
Ig8ARK1R66OO2jX1uI+FkWazDM0/W3SM3rouwoEPJEVbbr0hY747kOJUu4j3gGnB4CuWEgUzQxT0
n04MhVklWnwjLEIki/3y/FXeHn8QUCQRPf2cPIjVQuBLtLufOwWitQVrUGxA4k5P/UgdL9umaOZH
l9EYoi8QV6JnenwTmR20WO9Wl+MA0BXyYL+RR2zHiLQz2i5T6pVEZBpMqOC1ziVhZM3mfqtBbh9I
BKGSwsrgCqn6mHbd3UpqDJTOCMfRebEqko97jeDhydDP/Yqq31Y8lAxQItyiFZRKxgN1F9K6+y+v
FAxKBunJpK560lvsV7mAM2Y9Icny2keAHLnnQSw9zsLweO/krEdenlfllhbvfYe2rHKkgfsTawSs
EBxSAeaUEbosNnfF+K0S31aVMUbbY7maqvNJcvl89UlWCN2s8d2XgM0tcVOe2YtHefYZGDTFfFor
44v+DLVfiWtV4iv+VGbTanNm7IKzHauLWa1z/xnxNkFjL4WYtljLsar/d9Uy5Dp0HmZYOKDgkd/8
OHH7PAc/PWgEGTC/782sQmSnfuGIZLqfsVFrVcSk3lBFJpPH8BKxYG/IaQisF9O+pEI7GmI3ngYs
F5GLbGcnUcav3R/M5S8Pgme0LoEb7RGVIYs7RGIjrjRS6njQT2knVo7RnQ7cj+NIzilxGvYciQDN
iBJFHVLEBFaFncHVrnAfQBnhk4pL8dbNLMoI0DjMLfVebyr0zr+OLwnXJUDeh8zlTQw5b3Ec1/M5
wN3mrisRxuleEEhf+VP12TXWdcn9ocAJeA6sI9j3Z+DuJbeoirhPprEQ7SvwE+UoYMOd9bulSszG
XOjO2rgaDh73O4tTalImwftCu+EgfQlp1Aj3GDKW964VbqouzE8UTDPZWN3sAXp7BNmE3z2HfbPf
qgNxgmWM21Bu2Up1NtuKBcc0qbgOsH2zxoUBDzV/2kWqF/dR1UyYudJstYLL7Sm+6BNDndR/6m+p
er89Y+rAqK745qlqN9mqWZ43MXCB3fNHTCe2nnMXw9eED9Ax74MZ8tJXWAQ/Qg5pntpfYZKn+pj/
B+S7cWONymBRWgDaS3wr5mvMd/N9837HIPyWkBuojmR98ou/RgcrLzNpTJdcxvhitly5Pv9UbRix
dUKzk4rqO3uJTclorZ6UMkJNJptPCZJSo0P3wHzzGVvqVlisK0YPmG3Gj3N7z9esn566qbc60gsi
wLUQrupg8d28ZydbJQq3ZkyWjp37DC54fR7gxd5L7dbL1z6D3ObOtSj4/gTQBqZKTD1cjH+/0h2T
AJliwKwAV6p4/F/fOFk9BwQS5s8TXzFv5qgqkCOes0RFyLJnLISKFemiHybUBCWH26MTDPAVWBI6
EpyIMisFqywiRn2c2UFDQ3whe68RPItUAChPVmvvdltE5FdmXDLRR6Y0Vm+KxmD9h0J4h0hCtPzd
vM4Axa3UfrBhtMeAGfS4k+X4b0MnOkCqn0V6HKMDU2Ntp8H6MRztwsSC7japEWpYeWKLc37qsb6A
hAufFCI8R/3VkTjLfw4eIzsW5o+tjXdjY3BXNxmKLMjIzQsBYrQyDMmJY6i2lX01aCbqg0m71uDU
rdt3hAGCxnENwksCywtC/ILTa6fzlIqpiTikygr3bDWikrnvC/zftpAyrm2NlBJLWJbqRqpA8eke
OSUICQKu8BiwjvO3NLl1DH/pt6E42eKbutnuXC9zGrNKUK4Ze7tqtBFOFcPILZ34SA6E8y6e1aNg
J5RwyfMSC085E3wf/CP4Kfm1aQ5clppCadSmc9IWYic/u+bYLpuuHmrVGuBb0Nwl2o+wNQ7MrtYR
dBm2UMp+8mmJ+IUQXMcz9zO2t/Vg9EEJRtzZv5bKRC0U1QGG+bT0cDBpwdxPb4SVA17k7IfsWWQ9
PVepThmwZi+WaV4CFmWjMmtPtZGDJuWG5KYcAesMLUFl980h9lxFB/s4u3m5DoMwZQH3QuaCkiMw
qXseZHPcPO7LD5Bnrb66Z0PI5DVP8DAJOTICDvecmDUfEn6StoRY+zoTSa6UnuA2gU9cMbd/qaMf
/QWuipu+qvae97AmDxpk9pgP/JkzIJ4qbsoBzNy+DJcVBgMZFuU95zbtOQewqQFvzW19pOT8U+8d
c+zWfWcdDrJwrnoJidl3gMPEFp2jEDiZar1IVa2/xKc+w8tohM/3zFVSgi3u9r604/v28s5eW3NG
JVIhvZ38o4LhSCoLQQ2XqhSMExrvfCFJn9fOPqW2NMo2TvR7YAVmelIAJUuDOpIbGk3uWlhb6orX
TQcv8mOvjziRe4JAs+Z5ztNoBNbSOAV17uhcMVm06xEseJMhxFxYkF0vUxnNbTTiH5892TiYsh0K
SbevvxN+lhHeF+QhFgvSWd84qRdYB3x61VH4hqUNagZNVDkCT9wDnF5N3vhPjzO0mnkP+hS3pFq5
e2yIigvlGJL8H//6fLU4O5MgFxjf2iynl1Uu0lIi09O2IXoIr4TRbPdbfU6u01/Kl76lmKqdznlZ
BuoAWiusKpKSyyR3RGn3X3J9GnqqGvlxtWNy3S8UKp3uHlfqBYTfMG4Pun86iw+uc50Q7xUDjBIw
OOI3oKg14MfdaPRLE1Es6U+HGfS5+/LykgwKij4gzfhw82OwNHVdn5lH8MhVAbb4ZXaYno2iwbk2
CQvdqv6mFyQX5JKcTGXpmDuREJtkX2bYSS3vDM7Shqcon6iAXe96dQmmNCfDl8rV4Tx4WJtVghZ2
GH2u2tqAzlH2TMHQXTLg0Uup857wtY421D7Rg2krqoZVHHsTF0zR67LA+sCmyqpe2CZV8iTX+soo
vB/ktl0rIDtOXqYneZtpbk1nRk/qUzZieeD6OgCtpO9RAu6jOT1Uy9j/XmlA0D8/im+A3NEdTWvc
ldk4DTw9wtnDHvog/TaEZA+SqNd7R+/4ZJF+JuCHKdRJjBkiha1WQpFpalhVUD9sANmpEBUOGAS4
DZxgonyBMyAOirh6b5/P0rj74R3CzEzjZq2YHZJzThzc4wgtdD8G85DZwPHrv7jRilxGvwyd7piO
3R+wrXIgYcnvZyxfFC+qawlZSouB7SmR+R7uK1IeJf7VwXQUQ07vmji9FlVjS1pY1cy9WxDEBFHp
Osb2uGKL0taQAm12NpJhfCpPy+sZXFCWb0yJfcCPD6xNdYOvYy2ZZhqmkUzm9dbUe9cvBArcDpOT
LIS35+ipR+hol9b943ds0mBI4SxtqXKo4rjrBzpssrJsDhctsY9d1ssRH2MLRwdQoJarJVvnNBnk
84LyVtGQiPSyix+aEEHfzgRjpVrKTtbG+M6IEJ4F7U7BDgYpyYeGqmDtIflyAbiFkpGOvuXvTcZ2
UfJvYE65Y/jWIJogAEzbhnD1EMfrM1K6UvZlwPpyWOhkqbCf2uT7DzOzbzsD1vOC7BinrXcfTQlQ
jUANtXjqs8cLreaLMNPI+9m/KJx29MVBByku+lX0AG+YsovazuuTYKw5oO1V1aOah69Z7knhnh/g
Ma2LiXlxvxqVGuzz/RwHFAP+5sy3dpcdK0d3Ut5UBgLqH/KJ0etSjquuAYYKi5iobC1SHMEqYzaU
AzPqn4HLAaPchEjcxA2C1/HClB2EBhZl8ZdtywFJYoDAvemW3u8PXxYT3gtlZjbF+2YcF/huBnQg
NTiKPqAo/iTqIfEophhpqkvk4yNHvabDDA4pwJrHeDiTG145F5yBBVHKHhLShPdW01KycWqiQzdU
kdTHG0aCk0af/IzS8DftF0dKJowUntLbf1xXKcLD/R2AC1/EAy8p5Txl9luWoYI7hnju827TAfJa
Odo1JcQZtjWYsr23gubnZgRkeGnBmfZwuKvYjCGO43xTwWCuTzu46ItS9A51cbjsf3BcF0+N4FeW
gsO1CoX9b8zlYjlxF0m2s1PxhpuPYVeJ3SNy76NwRNhz6jN23jHhtGQIkgCpD4cMkNJx7czLCVCp
nNwLvh9uoZ1Oc7RKzHevu5ddfSEFYuzSofGmSLGB0ohOD1ZcRKUU0bEpNaaQFqrqixZjRZcVF4Qb
dFyW4ygLsjtPlL0J6CnVMlBOdS40/kVW1R8pMXfp9TznQtARnruObB+UnJUJo4mvDhWxbLm6DdNK
S1LYFCs8JsPGNS0kOdqoXlO1MXt7vZFawJUvqwE3zx7awwtplFFEgDPcdWo1zSSRwjGDH9tjxIyN
3ff/jqivZ5bGnIn+ZDCrlSDXTBKqw/WL8X8OMZDx0oUigdrXMVSoPkSh2Kq1KNCudwNdgbk1LusG
AKFiZsN35jzc+tvXjlia5MV0SFomTUjvdUqTizC0FqeW8rbH08ooqnJ+gOjtcri2+83zI6Zi4Bb7
ISI3RwbTT4AnfxquejFa7SL1OjRQNA2+jL1dhGfaR7JTzwVz3EfwKX7uxP4IwAJgHw6fgcZcks7+
m+Q6qBjB+bykub1I6qbDa9I60HxBzw+CbXvUexadW0GXO4KAECgDZgN5yzkTS2WkS4j3w/lXkkwV
iuiy1wAztgUUFC2oBgyuEfdUEmfN+aLwsVsixaUXbSR7/UZLas2tY0SClQ9VK3/2ouOuJKlDM4hi
IedB299qnf1vWVZ6jCkTaw1eE15Az2dNndjX0r5q7kI27Rv1vna9C0NapGa00sr8ezJ+CN1xQBZk
69h3mP+Gvq+FfvCt+7n684VIU4b7Czrfse+du6muCtVRmQmCkg8TzzdqQU9K/24RtnDt4H9DFlyB
fEVj2TAgJ9fT63Spf3JCh02h+lR7Gb+XTLJWNrZj/bPKdzNgK0cEB89CENro1mPl+tSn9+Ig1eG1
mSPLUsxg+feawFShQPreY2MYy/VIqyY+9+tvIMGcicgVn0iOd1TVtFaQU/n08f/7TmzaqZbAjzcH
/AVTLMKY1e3NjhcFYPBq4Dt5OmcyLX/UpsntCrkA9cvGQ0B0265bg6Z9VBfEa1x7QwrpoWD/Xw2Q
oD18mbcLOlmVQ+6ndQCpHfB5dycBVrBFDIjx4oUm4rtOwPXZ9RupCo40SdEHljMFwvgpfl/dBmGR
NkvBfG+yKuQpzRO5nc93/gOIwxXAanFyDCB2yHcHT1801xJPEH51LV+Matbj9s6Ht15vgim1GRCe
uS0GEaxAZw40ZtmBdNBH6KzszDA0r5F4ba/q9MPyaewr8LWGsApjK5Bw6WbhsVVKuGjF6FjY1uS0
V6IPXkaEBpL9ZJzjo8+Tk4fum2nCk3wPigChaioVLlX4DkBCY5Uo409xxs6exk3X7ng6MjuaRLrI
nJbvIUIvbiVJ6p35ewAugHT3FMyQd16BZD3DYhm64+pdEVo/+AL0I5wy/jqliuJpEn8l5Z2AQLuv
wNIwMaDPbdjB801KvF8TWa8eylEn04ZH04FcWaTEyRJslz+tBVJVG6IIrpsbDsKlzMcAXXblTnW/
6Vqqqb2rKp42yXwCF8gsJDeq3YQjmUBq4KIB6d7yP6I3Hp36Plb64FiJTGmmiCmi6gUyl7ij31y8
SGJLG9SCbSmfsiO5Ad/7ROf9LOUQcvQwSYCkGfCGAFgik6+mrdx/1qdH9qMIerigYnr/lpVCjDZK
mTWk5J8NM9PZWBsF3XtOJv4c6pRZ0YQywQzw/WE4n3O8dKXSQLoiiXOW9h6J5Bg3o5N+pcMdA5Ss
nGzFGBfR9O8s//BD59kIuG9j71D8fxBgl8CV+lkTLUD6ja3Pi4wk6fbB/DyiZ7ZUWruDWPX8VEtp
TZen9cf2nqxdqxCFr09XtKXHQWtdVul7vWnAxPVRknPgfOkw2SJKsqkAxqbsQoDRj99DdtygFT++
en1/404rBogPguRB1/wFlZ5mrD6SO9eMwQwo8eHS8kg08DTXAeseknQz28PGV1rYZgTSGN9JeXEl
ls3kFB9EPSmBpQr/xujI19aoyZ81Fm1zv0m+K2TtqrFaTvO5DbHZNJ4hogrx+gKkcxjN9tU+uhrZ
ISfp2bd+zAWLe1rwaFSADtjVZ5+fJNFIiDYE02EsD1iuPgU5S4RczC5+YIzD0ZRY744M3ml3YfYH
qyKB0YTdP9n9BZHl689MkPK1bq8s9QecQm2ciqiy6ToWBuMQh5eL9tPzsD9QXRCfMHCMyz32lesO
MvXnZUllWvNXskHlVhDR7M/1vbKAfQvYFZiyUeTm356o540ouRZG2xr+sUPN1dSgiCDkwTQEAfzn
GPU3bkZkxDBdOrBL0ttAddEUQ8YMeAR7T/uxPtPrlsOrO3D1Q9lJD6n0X6JhRNSldiH9k9RLXYr1
KsZyPVQKe+oQI0h0Kzjp1jCu+aziVpk/RkmZM40DLXsOpTWPLfCFnPeF2STolOyoQW4kGrvCjoEa
xOGn7+2eDes1JYaWwkgMlgVVNRujz1Nn+Ra5SHw3rX6DbOZgiUhh6BiSz645EacLSLDcfDthN25c
w+1bJNMBp9jIHRt8/GaJHEJihGVSuMbMt28HbNN5rbhSV0y95LTjshFVpkKFTuYCqFIzpCjGUX9t
Ewoz/QCfVb5PzUJTLN8/HEMUfkIVfDXQi6uQC+h9MwIldkOUZtCZdKDkwGQ8gqdl0vsVv/IkTpHE
wxmcgp++mVy6KP9e2kttZWvlX5KdZew1L3kcoRXA9IlJLgGaljKjxTR9WTf93oyPR/y3OULcFqm+
KcLs+J1I5qyB5x8lSK/baijue/F/2kqf/XjEnEUazM6QeJ2EA6GusIlsZfOtng6BghK7Ypfzegex
bYDjDmhHz0ZJNvGRVywHPhP7Ed3mZ5gC0Gu6s5a+Q4fTHEMm0rTsiDdEoDHUFTIEUf3OjUlFOmk3
xPsCGvWH/c2dgLp1wQO2ZWQuQHB8uF6453taPf2oGwy8YQLh/CRzzoy6qIAkpvgrX2K72JONq9T5
w1Uau8rWC/ZdQKgIpe9UXgNN1fc0DXOc36agZVDArYwXfAxj73lUIYyF/CL1TlWWEUCrpkDJoL9r
57B+tm9Xx5nAkjFm2+/WSkbFjX0s1fvPX22ytRjCdcMUIj2jSAF5XOfw3XcocpWvtdS8HUm3qaBv
O3HAi9udgqa7CCifQDe/I3NmfCnIYg+dQnGLBgtOf0f4mcE5hjjHEAQl7b5ig+6a6TPJNflHTK7f
ma3wadl1SPfqS/suftRCFw+UJrttRLvEQ8/PPk7Y8jRIdsIEPzwA5FEJcy3eq6m83bqMvPNy5Dc/
g9/hwNKntBPYmMT60mKwX89d46VO6lYwgvDOD9E62K1Yu+uABQvIbxVqZ5N48VMWJCEUzAsVDxOY
fLxrTKXxJ0fc8o94nUx410hLXxvav/iLLMFbYUa1WEa8RK+GlI0zqU44HdOQxpwNzrC/S3di/Q3l
8DSEgtHfg9MF1Bdjji334lgIUVcnIxshn6jJHUqI6ZRoBiT+K0ZTD52eYHkvmf1FliI9QgmExiKs
IcggKDCnVYPgBxz3uxhM1Ata2TADiyV58qVXpt/wareGEIziAVKIlX3X2rsSBlo4GQnpE6gnxCk7
q7vdEcqHhRJW29uS4iZi7VSEKq7P69QSJYvKUmLQH25gbjJ/YyI9PXb2W6pUYT/lemc3B7DahkWd
vcGtzqO/AyPPB/CFLfbWbc5so4bakwXv30CMXv0XkADc5g7wvdcEIw0o7dL1Hs4Q+XgMLkHRuMjc
33tjXbmOLb9NZPkJTpuS5QtZOgBYZNhEcsTtOsHOk94uE/FnloDoloz0pJ3bCi06KKloWoOoX03B
afBL1nIfEGRQ14aYkJntdKPQkrxMY0Iu5ON8ANDRqmYGJ2/6gAUThDhzamTFIpC/sMm3T/CXgHox
9UFCBvNIrkVOQ1tM+jaC43AbwX5u7UaeiDvmyBDIiqd2BSoyCSO/qQk2z53rf3M+iGrA1x0w7odI
9cANzVO9ir8GrkTS486Bmeows/84n1BibNCSiLn+rIGltsY5J2h83cB6D2YDmPoua9r/oQjwyh3c
tj6CTDerr6FCkdJYeOUjsUwK1HA4+SmCB8Ma2rajPTzfeBBGmxwmubP2Vxh1MyEH2yleKOzfJlVH
zN+9KM3vfLm8KyEb/ZHWW2K+nKB9TP2cmm6jGY2Pxuq8FQ8vfNqTkE4K+oHPTOeEdBTye7SZFEa/
bM/a9ySZG6OtDKYXUEtDNNI6GXWOgyyPWM8+qfpQImUW5gV/4jWipK2v8QROCIu3tMmdNPjNag+1
XyVbRYG2NNLU+Fdf+sAaveV/SSzx1IFpU1wvFe2a8YOWoB+o8ehEnopujRMZnzZoU2Y5UHrGpRlW
yYhBpKJVszRdtLe+FsyOT/kmD8zen1F2CMhQSyDOOemJK6KvbT/IPi4D6JjbSratnPge1p9348mj
cg/sK+cv0AMAUvd9m/dTsRkFhjE85Cv9qRPgdxwBvXZ8Q+PFtel7M2kbdiSfX2+9lPFMTciiGdWH
Yn5cqt289qkdW2lxr23Fio6GdjSJiJKsMNrtXtjNMKSA2P90YuQRxH/F9/W0e667BYYDLqzAes2N
FMPzjUq38vtp67swxxLFg2wDG78jpudTuXi7T/T08UNb+o8FZaMTPvawVMEw0sdYEONLoQI1u10x
S3kHr+7NNEzlFueEYozv2HA9N6UoGMAM1h+GvI18cY4L5TZZhAuBCDUvzl7wxilVD5x0AKygmCEN
vYTE76+Xa2wAtj91Fn+2i+EcDoSp9mMtw1u3cgetM8CjPk68796y1wIZC2un5FKrwbysnBDe/pZs
ODF8+BCrq9rFCUvA8o6p/8X3VyLi7DyD1+AkzQVVhgEQgkSK9mVU+rG00H8r0bbfzr/126e+WyRi
aOl10eNFyLyhPG/RdIdjo/TZexDDM2vFRBI+mBFXKFmJNwgP1u257/Ml/FrUorCOi7vLQtMp4s3w
TMVgDhJZU/0rwY87xVRmXGAZfeg5LECR92fDkWqMjuENuSCfB/hwzcm/lt6V8sULe5q4WFr2Zz+e
bpZUyq3KcEG8Ys6S2gKZVW+FAc6hLAo8AkVvekvQLfXQixuxJFpGYTC9gXcwTn+kVFMcEXBA5EtZ
3szOA58Bq4oQXAVxbDwFNIQtyrY/9yjryVI9KjJAgkDLD67WdFYwI4v2shMQ+MAsPzJwbcoGXnKU
JSam79hdIbsJfBdBP7oBv1FBptn8SMu8XpqYqe2XNV+qRLAXEsfU3ulYrVKSAFnoZ2T+pkudUoI1
cGsnd0wQm34GF48mHQGX1uyNgLw843S1tDnQGG4bhkIZHxWs9Bd/1OecZGH18gAnW/3kZ5Ed4Wxc
+nmUquyWTWWgSFIQY+b1hZOW0M0e8dCNCp3ZXMP6+Q2XA4be0NAkJdcQWZaI0s/WyfEAX0PJe22+
bLrNtac7dqasUglefyW4tm0dj15iCEaATK8OcGqMUn9t93tlNKIuZo4Ze0vhX5fDWuPSY+OClbAD
9KkjgRji2tJt9xy0kO5d1YtffPQ/1WgUna/cp/A43GEVp2U2xFmA1SbVtdQk8qmTlomUWP7t/wEQ
+/gw4qmxyZReAHW7Xh4YDg31VQtxe5uV6lui/yYQ5ZsuDHT8GejeyFJtcKPrpEUpXbD50tDGK4By
KXwrtw0xhW2z0SXgiolvMvHw15aUydhHTsr8DFW20rGuSwfHXcKZg11tIgiM282MeydJBryvbmOF
GhvnE4ZlFoOFhPIYAHVSOyJszkyFQEMm7/WUyJYLSNYvlgo7QSVhjmqaVR9DKLRc3mLN+7Heyi0H
0fTQXx52iJBrJPXk025h8Q09rwCT4BIxX7oglgXMhdgUR/gIuU5TyJ5Nnux9QftlOY9/JbbLZGv2
Pkp/i37f/i2CRoQ9+DHcpzugOEqPsLHfq7v2uKsJ40G78aA++zL7j577NB8RiaOwXvG3MWY46E6M
6GRtJQ3YPzfssvDE06Yip9vGcrlaNW4ip74Gxx0TrdivtAWgxfeNvp3kVwzbaTqrOn5FY2GLKSt7
+AP/Kja4+frgtnygvhq3cFB3UgCA7zDTNJNHAfXH90DoLbdIHAS6x/kQjXgKC8Uy2OA8gMdae7SM
NwQT4H6b5AJrl/YLVpgL2NFMRyrDhWmeVibVZpDnEswvZDxsYmQ/TDUWud3WX7Pt3hd17oHBlVXL
ZYKHbNqxZAwUyQIU/9C695QR4vm6Tc0nNCz05mv/Bk+O/IebH43+Z/4K6+1uNnUrpus962A9YKlK
HaRXr4sqXU+Kh4cwLzBHXy30pBqkNGbUp5gIURcXk06RZXHV7ESOzB/fY5Z2qUpQF+pFpkyO8X7T
obfaahc0yX/hNvXO4O4CMqWCh7sasKcQ2vWYByV4LTgK4pkaxboU/r5uvfNJZ9TZCWnYtYF5Dw9E
mXnRkMt6VpxozLpBtyDgrF9bSVv7L2Grqxy5TbQFJAYBaUD5pCEgtiz478Mj4TrUcZCrgIFzxtzr
ZPISV3+QJtg3DV0Lbu7I+utzSivtPpclp4qLP/1gO+oQvOm8czZ2oQx3+rWxUBzEXGLQaGAVvyO3
bU7XG68ROEYEKeNBHB0pcEQRt2nf81znQn+f/9vnIh0X2aht2L7u7XIbRxQyXcLCcwold9q2fV2o
firOrqrUCEHcpB2YQPxhf1MyKFRJ9kqgMr17dhlkfk+Ds2vQMII8tz5ZDcZLHGAYzjL2msePhrsh
fGmIhkBQU2/ADeECRJuBVNi38fv6Itzkz6Xz7hBenr2aAEKbUbEIJWg0nZMwn6Hj38kCE9DodIXL
JFYDOtZ8RuuWOSG+BHD2ZeqBbdgxVF7zq8qoqBJAGFsa4E9EWkWiDo6MYuxpvoKr/Kuc6ZslNbrX
p1n/MoFHje0Q/2bUfFYr1EHjUzVFel4mAg0DgTIl2JS5Dgb4dOWEWLfLL/vEN9iIXfzRAfC420qg
b4YwxDeqk4mEv7V3TBQXNFs+oxn1j2O8r6c9lfEklpkKjjkelwgnlUchZ7/6caResyq4gj/HB8/t
xFU4et8thn5sJNgEABDF2lRLCktZ64uGM4zgrePm8ORXZueRpK+1FOzMX9MECrbbhX2Pg1Wyjptz
+0UrX2jQI5EtOxvwfGrwXtIDGvmhhd0DibzredTu7DsHXflYndo9FCRv/oxcgeqZ9kPkDwF+NKmU
lO7lNHWOGVVZthjwSFFDbZF8UA1rSUvXTUHcLg7uDmqC6Spy5pJ5PO2rkRlNJgNnZh6FtudVe0lM
UzSQ41cOgvWjdujpV3GlstXVcMF5SBHcrP/tQcIlLhmbYiyqN4MPc36V0uyyzSmvPDE4en6dDUat
YFQNxbHSn2uucnAI4QzFr/s57tAevobcqdJswtt35DscNfIaPG6o3Sl6jjdHkhbtbKa2UJd4y8Or
1Du1UmueSTPeoVtxFZMuhsXqGSUhGNKvKVRS/GCkL1iQYumijAAt8s0y0v9GI37ITe7Fm7qypXxW
xglLx9kdMPf9/aq0Huog9iVZtZu9tIrrdcAqLoj/8JJdgwYTXEgkJ2+CBiU8n4JipCHR5vXu8y3P
Pmz7DQ1aXnTzx+bwhA8h3SyWEHYxzYxuNK/SKh2E6b3X5i4P/FiQucEMpJXs/QvymglXW6Gp2Fzk
GJUYYxXPvT0dwOJ25KJIDC+fxn4XVt4GAhys5Gk7n7tK0UuTKoUg7bZIpKqhn5b5D6iNVBw/6zHi
If8xjDiW4XywKUylKWXikDXM/iqrLg9HifTLJ+J1rQ4P1KRluevQBT7KG4fDwk94nUU7pS9h1WwT
Vj6RVAZlClVS8pxcf+aJlXajTe5eNxlNWLxPsQKqa7raMSOp5Bk22LDsSqjON9m/Vo8rIrToPFu+
rVLPXWEKGfmY+KoSzCPbSNFtXbwUZeeA2Cv1/SAfF/S2lz10D+HTnQufG61wzLAQVgj8rU43bYgl
FS8qN2mVS5dvG7ONtIHPuxIjsxyhuVacumWIHYEwBbX8g6/nvQoVDtbIUfU8z0D1WSznfT6ML64B
lpXNmSNFZqL9o8pBmjoS4cjVHFt+kasUcEDEm0cSxl8skEYASi3ECDPy5sgQFpRuVFuu0QLSkrjp
j+eaHQkMQEZcpwBKhafahA0vtYsn/tLVMqarVdON60/hf1FUBg6d4IIIFiZ8YyRrQhV28vuPeq9Z
JLdUk1l2oj3+KVEw4B5LTeodZiy9dKZdCCKkq3Wt8qcWzxFCC8JurNd2mK6k2oO+4DXmy3KF/Hdg
AfXFd7D63ndHogg6npQf5XKvbeW7ds8PJde+5up/R+AL2wKPtdg4+AlvvBopaqjtCOuADbHPwhuM
zh1yH974yxKgiHmNQ6U2wpzxYkBoIxzVfJ64v8qh8ThcNfXzDvyz15jDIrfp+AHoByR7QEaShAvg
VnpF99SlNuLZHXu7gFZM27L2lRnQZzCK4OADZEkMEIw8P0kA2xa99Q2ZEaddbev02tWstA8xJ23s
AiHDGM6w4Xqv12fvzYdzB6o/JkQ49BID2QX85nEbdm2xEI/SpPeThfm5d5EyObVodvuPqoWGqwyR
DLXsh26y7PGelRHk60DzzIPY2y00xY4pZSnlIcERJ4qewNqUrXt4GjJsXPScNwiXKfsS4jifk0Jq
sSvqRSKHBZ4N4mrpm3BUBQUqgqMeofzqp7HFLM/KARMJpdofs0bHloU54loMrCdwMD+K1LxviYaV
PTt8sR047jMZ9Oz8/USyfIo6NJKhAZGQ5OZw5o4RbsNr86alrYDCFrS/PwK0WqR/Nomus6+knMwK
+hUpLf7wcZRu83K4ALT5GbQ20+LIRsWLT/XGLgcBfICwuGEuPWyumBDeRcJq3KOQnk6Lp6O8lMqw
OPqOzJJQGLrj+vT20FUbxfmPaagtx1rbAuR34fEJp/RHahnfN6CNPrJQStYQA21EPjIhutfaYKZS
77qSMm1vAX3S+9cST97sOTrENSRnoFpLDAM/a6V6bbgPLHYdqc66UFqA9/1bAVjL6l7k7XCLuwvV
jEAn5FLV8QnVymuj6CsKzo82U8wlPTqOX0qJrYXMadMjTitDoXwjQ7t26fguOchCuGcnhYyAXJu8
eNjK8W2zTEcqCuUGv9FwM6YIV42UydbfEOknH1/6bJ6tLj1hLNF6s5ffuFdq0JHZI1iqvS7JFMFo
E2vh7WOPSNpJPHWm5aE6BCmLVY8iu4Oo1gcyzTBwgGvLq+ApVFfUAzGmu0REq8lgL/xgHjlIPqd+
vNVNPZfVNkQMmB15H295nMb+PCY/LynNLp+kW8dGgo+29xhidCCGvZp5eLYQKW2YtqGjDJSRN5w4
BvqpB2XH38emJPb/Qx9Ac7LgdBs9lVWXrOso8WAIvfhBB6Z1Ra3J/CpSeo41dbfTPOAAJ7mladAw
YOYq39/Sm3NXl0DCf/6ecMfQAPybOxngZeT0gNRgXxBpY2SKz1EHfl2xy7hWY0BS1rd7ZOKYMDtY
ZuX/jjtu9l1sW5Z+01Q9OSrHwkwhHgd9CjXfq5lOzT8nRvKolAKHgt3//ozcGhxkiTj4PwLe1BWA
2XB1ReHWTY4zJbLHa2Dn89IqF91IGUTpgc878w8XAV/W5l8kdRWzX5G+8WAz3JU7+8ntPkEcyzQe
0D8kWSUVgfb8L7HFOAvzz/awV10H0fT4DNfQFGjj+lj1JkOEZUTxdFs5T2Jx/zafWnZEtw3k1WH/
9EhTd9IPWJXa7LB6kNWeJuiGSSiWzU9Oa/m7WmjSBJsBJgnyVWArLvDgSe23vTJJeE+g8xEuCpc2
vEqE+lLcwoFKYOpCwXzr6CS2NEqDwQZZAcRKhBF2N6w8kv69EsCV7BaDRDiP4M1V6QNm3ieuoTob
VQQ+quzBhjGzcQ2wIZDb8fOkBhGSPtlWSbkqEY9x11fE4IFnG8++NOU5egvBX4UczooSjoNAUctN
G1vnNJzya1/yDQACFwEUy5sT/V2KUBUmGiAx/CKpZiTPs5yq9mRrplQZsox04JImQcoejxGMwOAC
f+xUz1YYmPz/hqaW8pPunGEtzBeaZoQqPSHBKfwidAF64HjDjx6/iabdlV4oI03E7+DxsZOLkkvl
WZqNTfsuK1spvZORIVvTJ9bYQ6igZhoruwnNPcktJY4FuoKwB8vh0J5eRurTtYT9do9gBNaC1iGW
mqH8Z8h1YrK/ge2TGierC4pe56+sAq1/sR0rDNY2eN+FidMAbXK3d2LeyszXqIcrV8eqlWI3cs4E
ndFzy2t3WjDmMG/eK/AHMwy0vQvllOXKXLFgi5dZY1UshFrvSFm5OqMVntAoNe3J76fcnmGPbk1S
6/tR2tlOpRGouKY0wwiWOhQmZiKFBc8TrBY6BrYXQw/KRGB2FelLcJrLSaUdN1b52Vm3d+9l+NR1
qlXEiB2L7Frs1+GBfzCmR6H2tCuXCxUx2zhI1KlPrA0k0WE+8/SAOekAbJtDvOWo8skyOnbqE+qi
tGc1jG3IGwm2tVDaDKFmJBz6r1lpIfpwHHH+YJAuQEd3KWBuvUqVWQwj8k26zf71iEXEyJHZrJLJ
du68+p0+iIBsbPHUTnyf52ayCzJHdQm7fKNfXzfSivxHG+3aFnwpXkG4MAEvlRc/Aq0RtgGeKiqg
xbegehXQJbnkSauxiKBuBQmfH8iEhyQL8R/BXEImMTj1gh22vTfKqsTx4Too6Wpf6uIs1BqsEQx3
Ixk+1DcuSHHcI4hHx/JVxCcvjP/hsZTw4v+9w9BMun36L6N5kP5JJ2d+d3oWJFuOPQDkf11KTdz5
EndS/aThf+E2PnutAWODpMFDWn3UbcrRMQwSU9uzZtNI2AtFm7gdzW3JLjXJa9FBlEv6KCyqhmHU
BVbZmi8sfQRjBFNNU5QPztYJyzwak5A9SvLcy92FzkTerLX+sq10ubyDt6hoZJu/FO52Z4rppuoQ
j831IZmeB4ynznJ0EYkkiqdJGbft2ne5RHtR9KcXAEBw73bJ9aroZvmhWS/WSssobK/6aMeZgYDC
4Ca8vQ/k+OWk5NB9nV2lC210D6c6rYIsPQ2boL9GdQ7XOnUhCxY8katMiNon/UOOquCXDEOi8gR6
xwoXlC3f0mI/Dkw5dd6X5KuQYC51o/St88xiiKZMLyH17fFdW8IRReFzg/ZRvwO5sh/B5XJYNR+t
KMC4ltOsbnVZHle4gHZr6WLyEWGvRtF3IG7XQsKcUlqZzNYeRXuCxYmW1vIbsHsFLaJbFyAwustY
1/F6yWK+1NtaCdwKJyv5gejkFvlVcT90C++9+qZXD3UFKzrSfHBV/NmkeET5qMR3h76Wsf14porM
7EXJzSAlgAWiMv5W8o5xYudItEUfX//UXVbpjbE3X2WHS98x8LDASs4Apt1BQvMnWtyIFxkN8tec
7cfGHQ/9pyfPuJUkl3pqUsyPbLAGiU2K3jlGBeUsJocZ9znjjI05SUZb3hEoPM6tux8DusGnQZKG
Qg/+8sxpysie4CYplPXwSYSI5B4YZAIwD84clfaA0mygCc08yv20c7RgwoPc76H/RlVDs+i3RVHy
+UiI59abgaMh3SOFQeIRq6knTK5PI+fmqXA15o3muBYDtcnZuxuu0i/VFxxxe4xOePbx9rp7FBxX
dvYmcVKUNrOZIiNLzb7Ba1ehv4Yi4j/DRRC+JNDS+lVbFAsi/kXYVJzAiDhz+8/Dc/EucuAu3oPh
ItKRCONpKtymLMCmyGDOC9WMMj2jgU907rxpOEEVBa8UCBqPW2PddL4k82ijbs/SMpWq0xEgdakK
cf/0YueKrwyxXgIf3k0PcIFx4F7/bWizpmrbuy6IfER2z61DJJ0ErjMAvqY2KZFbCtKrhrHf2atj
nB7QyWNGVJpKRQ5t834Lpo+Ftzb0PpV4O6E+tjgv947U8TdZ0RzPcLNmmHqxcElBNO2AfkWRHnjP
yXZVkMrvyHXJNXV09O41JuKACUVU8U4ApaGAjJRqW3ToKEKrERrKvl80jp5tVihJV8i1MSzqj8xB
l/GDiuOFMU/6j9NrwfvOlE4NXQx4/I0SNzL73G9ZjHu7KjXXpb2iq8qwl1rMLk7xc0y8Ax296beY
6r9IjMgZrAF68jIhZFk6sF9xpGotTBqXocL8tunLjfkEa7MiWO5u3Cqa37++Bhqrnhf9TERJBmuV
R43AyJotXEuYUzXvMXpRHLCtdwv39mEtOAvVelBGuiRl+R2n7rbbMZGDPqcCziffoyxl1exH1SCi
bFN2Bs/wP0N4KaoiJop4wHNAq71keyJStZwlhWaKbS6H1f5VYsONDzhdXKt9HDIdKqm4jeVn1KLx
npJAndEJC+Y1vEXoAQy66+NFZp2JHpi39IcfLZNw+uFtLP7JcurZSs2VyJZGrjlUVZBPQv9BXHxc
qIIsATPaMMvwnl+7xtJ3X4tPxC3AKHXzqqMOWr0d1njoCaX8EwNWSCsyTBDqfF2lAutyDpRu1RBs
9MehUorVbEgjXoPQ7SoNUYoQjWHlyutoIZscdgqaS4y9okKmhU9Kls8fCBT4H90jbmucI9uRBgia
/VDRBrVl7XijFz5Wz0D39GixUyX+vN0tRGvXMm0rjFtuO5pEq8by7hFUTPVc+sL4Z6g80D6hkB/N
OGC+tD7YZy4M5n162fhkEe2qundQlTLMK2+ipBOVC8FQDwpzCNjSxx95HSYAV+rIo/OUP4V9aFb5
3HcyDm2TXWFlw3qahQNfJ4ko81v3rnqtv0nfaJ1Y0kMtp3g5XAsG9ZZvBqS1tQ+eivd4UlCl6QNU
Fhd/aW9ln8GP6EPKndtUkHwh2eC/e8xyDrSRcTZQ3alVSC2ZWMMzN+npsv/Yh4sEULsictPJqOgX
Z0wkYms7yzQNfM5cA/FBrBJgsA4KhMRZr8w7+BH1/+fWTGGAOEma+z4IHuVGDcQVcW4UZzGQ3EBw
tEuUo6hHYegX5HcK/aAZPaOwM4Jh+rFQ9EGrYzbybvWqCQooKsiD94NZnpGeeKzSmr9QM56w9cZg
RrK4tWKXcvGi/qtPQ5KOOMTCx0x0+dVjFIdsfQeMAXy0CHKVhF5C7Vw8AJ/ygG2nBnzD/JNYLV8V
pDSUhJK/mhCNV6sU5kuGB+bDOWf/L55oRqw4qMsYz8FhWDvho0sA1NB1Hy8Z9mQ79LiMrTLKStnA
5Rm6eGP9Vd9STw2tOLFjD7ji2QYHFDBjJX+61oMMICdG1pYVSkuXl1307PILzt/USJ1AjEk9DPIq
Fu4+7FBtlj9hZ+PGaCIGQZA/MAOXt6iOMERX1XpeOJ4MiEWym4Sn/OdgghAg6t0v5gQxLYZzsjfE
P4keJ6tp21DSUUxRwXQBxEX9YSy8oHTcW0Q6fYHQgKG9pzqWjirkXWD4lp/JrCZhphQypLZ1o76F
1p+48hzcSyuaBa/jVGvBZrB5ib+LisvPkCESYUK4c/5i0bFfVdUSis+a3JFzR0XHC7ymy7yThx+i
2Zoy9JoLsNw30oHPmtpFS9jO/MlAb7FXhdRhqkjpmMJ1tGVoAnu7E5Iq79TSTY2BHjtDWVPNJMR7
HVYsfnBJOQmqc23eZ66qDKtB4KZPO4h1JfvmcSKgYwMGn1NSufU5Gns+E/nh0mySNUrWLxETng2O
0jOO6Xk7X+0Gju66qkN3uAZuNm7pS/EjUT5GRMZyHNRnr8fjCkvuXbjTHQdtlFUxDaHDVRt0051w
hgh/ysPeQ+BtiQ7JS3Q514dG4Ao1/G1uYNPaC7SNgxM+lcOT8TZdPDdkcNRn1MkfznLa85DB7vVf
t666/CC9PkrKMxiBs21jr+3QfJvvD++Eijsn9Tl/od1Jqr1JsWR5Zi7gJMdrScfazSMlpC51jUmG
bDNhMgnz00h0rfxbgUwAEDkEBANN4cVScePPeR5P8Cd4yyUb6tBKiwi4WLDGgYBln+4MghsVbBQI
iJzHrDftvKBCUVQpVoo4mQMs6xrodzsZlQmorjYVjsJZyAd1NsIr2Mga0J/JdqwAHfwb8tgXHFto
5yGs433P1xMQo8JWz83+kfgBlevJKW5bkxavvmW4wN3RAgUnBeb8ZDbU4K+LC6qP2DypPKuDXe5R
mjxaUoLb6Pxyy8XnQSQ0txS+BHv+TLEScPzT5AYqmyEcDX9WQW9nTdP9wnw0NDE02CEmk+1/UMzM
K7GFajyxEyjFye8UvrSZ+4bbf/HPiSNa7zsNDNkz5cs7ud/ndhMG4YZ7Z/4lrooTRzIqjJFkvZjL
O5pgoYg4rbE+CgVcObbkIeT0NT1i6K/ysLI89i0VflT8Q987KlIESySj+4IVG6X73czG6TR5Jalj
s7wK2Oq88KC2dyP9MKzvREjz+7rUAKGtr0N1ykPDACAoVdO7iZW64gpHwxxhX6tDDzn+6WUjePOb
lAP5owb7fPZ7uGeKyAPbZZlNRfdPTChLJz+DOP0knBKO/qziIjPq1J26FEpMg6TTTmsQlJGZD4Vk
5TD/diYhtjE+b9MiCp1eyc7GxjJo1cUNh9JQjLHYfwpEitPmPY+SKMn2FTbDS7CTGXo0DoUp3/9d
yHYN3NTiKmXDVbYBKJmbZ8/FJ6pBljjWyhF5mroZIzsWphKfecfOnXT5F25jihZ/Y7IjQT86FGmI
nOIh8jGprgD/4IvC0W9ioHHCKh+seF3w9Ko8w5fjK7xVMZ12SUTFf3NwJ47LMKH11+yQLMoolqCq
i+F/hDQDEExm9xZZzplKZuu/0XXnw4Iw/tJRERItsGGva0bZcVoF7daGoOiojSfUvhUwajTMfyBK
ihHOgP4gRpbMJ4hgVTilZL2cugwy3tPqo+GlrBRBwSqgaKmEPGIsvJs0KRIiSBU6C4Dugz2IKsHg
wUIYpVF6qIDmfSc+mE5wh6wgFV6W0Xf38/u6nHSih2TZMoSBrVyU1/z8oWbDd1S82+XAD4pIY8C9
lbhFcSfoFc/lbHEFyufGAEtS3deimCbwiI54US8Rgzu8sYQMeCzQ6oC0ejGPOncybsZmOBVc8OZr
C2hqBeJMqHBn8KZ3ccM4AUAqbgVdLsAVDoUfrrGxN9mGt1Skabhn0/JlDIc9voX5dR40aMAqf61h
Tn0ADzu0UTOYrDxWmZPs3LQRbAn3+W+ip8WnX0d8NoEvNHPaNSShq21mBmBHRjMQUVTSrU3bsZHi
rk2iFT2kNuEbJBuTreNg0Ahp4v5K8Y+//RO7+x+bIvz/TYGmcA9XcPJ+9fLytV/Cy9YKzNs8Gl5h
IShL7SDGhhlRZFuiLkygBDPav57H/YlMoquB5TJTsxpmRns4wnCT9ZScWV488djVqMmcYsRW5N2V
DXn1b01l+vSWKMM3cH0HUnn5p2Noa+YHax+ZuXw9SHlCRebK7mUVHDHZh4+yw1lArtsdv3t4/7ar
xxUAKTFAIXrq60QndH/XDfoz+LEx5nHb7nreSAsDEHyiLjqUSAtviyXQ643ul1iQrEw+Ix1AJAKC
vkMT5jWQN/YJsWI9LAfM+hZH7+SQ38hIjhymaNw+Ki+7c1gIzvAt+AqRCiquU/Z/l9CUf0RYX7ZG
dhkBExq+ylEFASxh1IT07GEdk8eLg3c8Xa/J7xRooET1ecFQWpCgOltZHcJ5r+2TPR2CMAy76DLz
ZWd8S0RpuND1Xx1rNI7fRw2oKbR5OE4+47ZR0wzeZ01LkXZIElZEp4iZEZyH0tZ0MXlohH6PmwjF
x9hx7gZgnhBxX3p7PCnBOy6Y6/c09NEl1yTJkoShAcSQEp8+aWKx8IdnUJhaZo3oPYimw5M2Q+8Q
92UKnt/kw0mQAEH1gnLAv8F4ZpmrJBxTP+XrMd9SvTgDcLz33tPFrH2b9xnVFU9aXms4XhrjNVf9
iRBh9tfXiKjPcFE/G64P59tg2npuofA3rQYo8NOvz7nmxXK9JzRHmUm+AvlT4375Enji8KXFe1Bu
C9iWqZYvnfyDb2K6y/lfQbA3lnzXYLNNqHaY9T+TZ4Z3zry3q+KybYEg925KRT+wxH6uMQIKgObr
UaTcnIGSkpt2p/ZDMOLFJybVefunFHB4uNwMrR/aKCNE8ILqs4kLF9kib8vLxhkRjORHoWgHqtiP
lUgKefM3a4/ZM8b69Rz9OImz/KIvNLVWlpR9hy1S1mYZwUA1DuaPqa9XYRM2qzek06finnDR3M7p
itHaVdfs67N6LNuaPYO6eEv2M2e/xujgdbQdI+BuB7w6PTRUayCmbetAdPojYJfx6tecF19xVNi4
+AUBAAj5dKxdMiZCh6Jy76MBnJ38qgKc6ea9YcJf35OOp93IuSEoaEaIOk048Uc4USYv/RnfMcyD
aeY2UKRhOVudkCo2P2qg19uPQesq2XXpVOAKaQoPCM3qqIycshkW6n/sjdGKktlyxmYYLaG3k2DI
86Oop/xQGn24mnAefrcmyUjBl6eDEYL7yLobeghFpvNwx+I+aGx9DQ2A93e0fpvR3KJU0rMvWIUT
kjKoE3Jhmcm7pTc/hoYsf7GOv/w40LgeEsnBd/j8oiPOIRbTrTgdablue0gHZMBIc7ZX8ffDb/cQ
DPXEyTxuJPdkyifG70vUiboSgXAKT6AmQCLwi3EJvkSW8sR/8d8ebZGG3WfYdkNQ5FfRG7ll+b93
n/kiWJxTXopehCtG0BgLqnhkVDgz53Smo8zM49mimOZhtLnOqojbIRiC1yG8y1k/wqfxm3v0pdvb
oQch1WiA5YbCPLSRBxfPzP4EUJVh/cTzcI+0JbGitoNOfCmwUDmr1E4ke7DJ3Ccwu7hMvxrxVRCg
szY6X7ImjRLyZ78mrbTalBKzIbg7S2glV8RobPmB2S9WwxJxs8/+IGebbQk8Yu1dPARq00Pw8AXF
gkXlUGR71ZUDVPtIE/bCU6ywU1OvsnLN5Jqxv8MBPUGV/Y/2xGF0MLFW97pw88t8x83DjvRSkSyn
plRVoPtBHXqILxwILRHSep32OWWe2xdV2gMOtQB7PLksVhYHi4ZvlQF24nuj4N/Ihs0/fRxHh3sY
jVRO6lK52dIcK9OkxmOsjJiMOED05VKMK6FoxhkV3QT2HEQatQBKzymVVtFpStNt0Zo2NoCSiZV3
8ceeYOGC17ptv+zuScUFntyKwrkIsHp09NWrtNPZkYIbm+XysvAnRRPpiS+Wb8NRcuqxQLbQyD33
1WJSvMcqLS6b51+3zH539/BQgjUmOtILEg0GrB9/cnOlGLAumVte3a0Ac1lAANRcUBUdt+/NbUZA
k1jKIQ7LHX5HR3rTrd4l0XIsDl+bA+iVQ3EMLHaM4MTX4HinKcFocWbkLdRTR/dXrKrz3YbyOiTK
oDcUmNuS6fNT6YnmgXuJkhNDHjqMfJZN8nk1YwxU+nl291WPSUNvxsR0M2DY8pBDb9U7+SGXizsR
5V5/XyzxsxvEhhhtJ8D15yvLDiSkOjIHfI21/cAixYI3sqxWXlhTjnMa1fAoDvHpqcVxpEqXTo2E
DVfOiZkW47Vfhtq5pifAIvCpbTNJC1HTnJP8de31rSfhyOBvl6Vdqt0MntMEoA9GGkzhx2iHw5zl
1miwvd5NUR1zJC/2SOlaSzRRXC2R0lksT/BtSTO9qZFvtMik5nU6E2J7940XKt4qNf6SDi0hhe7h
skiqe/ZnhpwlQ4G9fgIqoseNSCuDpK+FJWstd8gkL6XEBVE5dsyEmwHqePUR/TkVPwOIWmw4pQ5v
uuEspRS0HR3py0k3vzXQXxmn8mPOWUo4cfbehwEVJC30ZGcNN205O4nx8jy6pQ0h6Bwjn5Y5UVkA
pVLUiQNZQvsa2TN8zoYHYYio+fD/IyGVLGRGdHi6zK+2FZbbQByniBY5fVUnLUzniz45fmTrlHiO
C6NEYgqkxIcOaDuGMBS6h1q/J+0QEuGYzn8PE/G5DxYvO4DybZpepZK87eEuex1ieqJ4tCzsAmRD
oEenNkqwrjlCKTUXDMqxIIY9u6vwAbZtkeWL8z2cUWo2Zlfg5lgh1Khu9ynnQHsqgygo5AUP9QGs
PbyokKu9emLBlJ6qIHA0uZKvVVJYujZSO1riPw59B//9MVyc9qk3M3iSQ+ma6b5YCGnqcTIE2FpP
0sm+lWMzIuOeTFrsgWoRlhowJSmyftqVIceLONCt4zffTBdNEeg13rY/AA9ZEIphu6K9aSxVq/5T
x73sGyOnqOV2SavGGsYPoEUNGWih6u4zDUXYAifKPP5XYVrulN7xsfVACxFbF6lx7k0JcDd+P24C
4mtgKNER5vwkR7nuvcQj2vJTKzZBixXvbZeXw6v1Em4ljyXDLtDUXxli8tZKNXzk7cxtafGV/X44
tNHbXNowDdH9f5Dsugdp8GB2AIQ/bC31qEM14M5RMJajkcYbMfFkip4LWrYvBviNSk8DqoXxfy/f
oxL4l9s/SsukDlN6CxLx8JLSXCU0UHthXThVEYK97+zsRhMv3UpgW/v03qXHq6w0OF2QnXz3Cs30
36YZow/5wWM2FdDRkXEzHJL1kcruS6pI5QRDIsEz1J7YTqVaCvtBOORiVgBLd2L7Qx18a151M/Kq
89RbW0226U7ENUU3+leUXVY67AvuziJ1y9hd8mNsVWmiO2YpoC+av2cQmDqeUsQVjVQ1USEx6Div
io2C5IBeesgolJoA9FEoX7/7nEwl6FcmdYq3xERmv2uqQwNGPqk2FSkdzfVgj+cGCt9IgNB9lJEs
OKKuBbfuvrGNJfEHOLK1zGEt3P7GlVRn5JOjgRblPAYl/MhYBsLe4PEGncUm1VA96PooI3+/xu6e
WQdxj2vzAM0SUSwK9h2DfGO0KnrcLzttUBrTh/NWGJyT6W1IIrd/dZ5HXa+Rey1+Q8eKJbv6k0kp
pfQXRho35vYgib6FkCTT2BpxFzknwalys2GBqZ3WsyqwWpknM2cXht0Y9rHbcLz0qZShRo5dKf4a
HnAjk2YF3W0MrrepmEKmxjTts8rfQZt1et8SkGQlL7B8umzrx/4QGfIq7fdZGdEZDlNJaoFQzVGf
AFoeYMSSN0cKsEOvMCSo+ctKjVLGREfSO/xMHPSOmi2CSsB3rt2h/uiKwxrcql5Y+Nu09OpKZ73Z
Pl+XPhmNqE2DjcaoJuIXVtXxijoYljAgAdNdU84upnNzmcFq9l7FEmvv0r1+9wl9FqFXJhjK+6x0
GKHYl/DyHYWD+g/p/EVaZgdzeGQ51WgKKrr9RqVdziAIGF92z+uMKXq27/JdHhG/LZb6aQl/7RHv
fqAiHJAq7ToqbLRz3ohJxW4o0N17HuH0xAwUMiVyVPYebfgYZ6ILSp3g5ihEXu2IO8FkAxPpFSOB
Rtw6bSYgH3kzBNBGWL30jDuWhlciT8tVF/bDIC1gCwGEu7p8jJJ55NvBBSysebQP2sQyjWtPVi4P
Zgx6qOjqKwhhySd4fCJcmqaWlz7mIxdNR9nxUgIqZ+SmfGyJRuK77yH1rUyBSsFEG5FwcBpPUa9k
aXcrrc/V1bmXrIEAJ8JrhTTXwHmBnoU2LlScuTIiWyYuMQfuRpwC51NLmKytk3ipbQ4f9JfBM/vG
NO/d569egqgyBw5ka+vw+GOsz3cr1uakHokAdUf6Q9iEpDbhwkcQeFNbXALZU1lX2QevSIj7o5tt
UwysjNTaEoLhqAws7g0gUGzUg7ZsRJqjB/qaABuofg27lzHxIRfMeAl4yHKaCbNEI3EAgj8vC6TY
8UbpcfJBR6wIbR4M8jJRNJtkOlH42Ezcjv22sk5nmskXx7vos5L3ZFaDkRw/nxBiF1dqRtQkXFj1
ortmQbJSqcUGXZo87jigw08u56q6v+QJCu4iRBIt7yU8RMQpv0pXeCgnjHqA6aCZrmVAbVh/uwKG
nP69C0DJPPZX3RcyntB2VwtRl4am3IddnonA+c77sBX7OFy71TZD9l82V65XtgXOTV7fzqsGh5Bh
xElFG6/ncoUsVB15ocXhFhMGhNQYZcdLoN24VzBotzcZMnx3sELcvzx6BfGlmyXfpTZNf6RK5CqV
NOmSzQfDy/jsHrYVOCocZr7VREoS+PnCZwg1jt/2xU/Ub2SRpIHOPXivKsmfOz4oU7UaHndWwh9h
cX9h5LtuuatO+26QMnmD08SwLK9CZdcy4z0zxM7At21akB31IVsdEbg/5hpFU4vkuOD24KY/SdCr
IUxYKBNSlFpW4KRcm27MFV59IUBnhCGS+IJDe+eenm8N2yi0H7IYZCtxFt91MyQW7HuCSzDzTYk7
0k0uKXu9/ld3ZhVx1Cxmw1jwROSINas8LzKAbeUaQ/S5Cg9F5g9nsavM7roMyeAHlXTRw2oCNqrn
FYYcBSVnSvDwubFoqvp0J4cHZjG6d1w1SnKkb571KJa+pJ9QZ8fOSMdfwEgi9nO6WyQpqSZjBjga
R89grh2VXoHf/CkB26QQfgMZlLK4duG4jHU0K1gZjNMKjCAAR9nJ5afcvXqbiHzA5RZWIWISg0SD
kF1yxgEkNaoyE3empYJoyNxm2qw0wX5sBnzkTC7lk40W+4IEnKnG70lZbw8P3BYvtobrO5P0dS5Q
EjwBNquD9bjUCooeYTxb7f0VQY5zeDVNFOJ+l23cbC7fVTosP+bw1HDeqdiCYx1eSiT+8grfhNu/
sj1REQtfx6MEEGnKCVBC+x/Ez8JeA7ea5U9n+zuTK3DDlEly63fcfuRJYpp0T7JYBFPTLDKu7lvk
xHb9n9WqVQGFSBSSPg+qrzueHKp+v95vQozwNc2hant44ikSS775b6VSv8WBfVbv+FRHpITlLCi/
Q0KEstW30rIMdJMU5pcSP2p2KmEezqcjuVCXro4bJSNZqULIU8RQ+8TMWTTd5EtC/mN/6CsTc0tj
g+8NGtQyE981nJSpeaO9zBA6c9xa6lUERk5Uc45s8GKBe3ybt0N8hVCKlKB38m23yi3EKihUUZqT
fM/htJ3eDIdt4JnTXfhoGRYYrrXwm//3V4LOjqcz5OR0zL2HpY6cHRLRK+g79ZKfR/oT3FpxcmTt
CwkFPcQToKGGlWu+w3uJLJqlPj1n/wl21zWSVjopq976oDut0gmCwKtizcKYEf3YJJOX+yzHrvMh
5n80a/w56qEs8OXYQps4iOzQJtLM8q5pTz2vx9Lhc9wjwk1twSEVz252dKpyO9POqELLdW6ZPKu6
a0VVkpZneQk8jbU7X6db0eXxVJ3pzuB1sz5nAH+vJg3X+33atefXwduiVxLiT0AK1v3b3Gg+an/T
FEXa7LZPikBbtKylY2xPRvFOoIRSdna71bgB12YWYTXmbejsHtcjttXcdan8IAu0gA3gfAHmRKbT
EOI51D2QdwcWu40eN5pSCiC2GCG5naKhdEun20FUlYIQ8Gjx/RGfxQcu/vNXeMa9lGLdQRA0mw+a
LHnvSIRYezwRc83IuWPAFU53JHIzrObjrmhinVK70VUPNxtik2mD3/qpWVRzzXhZvjecjY4K5eFX
nUQFpwAdRaRYAGY/4+LEZmqD9zDBUoVd0Zv+4Fm4hR5oF1qDbuTYK7psYY+VX2PzeJAv0zirM5m2
9EKbZ4i6JNVFS5JbqUgP5EH9YJ86MMSBnCc5hYCLgubtLRYI+txAUVFbQD9fFGY4f4Ppowd7sTYn
uZRaXQOGn2XND756wmMs4JL/JLs7fLZhx75fzZUyAD5xW34b5xUuvg0JBEsYiioYoDnsEMrNZzXo
RDfWuM47n81Axlvkr6eyS5dN5MCNlvt5qUzN/3BWYDTmieEDc7UNWJz+cQbjPMXLeW3+0mp4llHk
1KhTM1JaLtfKCUhdc8CLB9kvnZo/siLFtmmZ9I1HdgQjD7azpHCvuUVB6M8EZVFo0Cf1KqnItE1h
dZYp8nOXlkE8kJakpIZtABDhBx+IIn4W9+m5KYJnwt3LIkLC6RmwvmYO0nEPHfnZRAsTayX2mN4s
NBNC7rJDIPwL3ksQXNdfGzFBA34Q06Mjaw/vsBltDp7aAW4e7BrI9SYVCoqOUww2iccNLAx/yOdz
/ZnjWTLQzU+iJVS0lBroENg4PSmIRjhUonusMvBCqp8IaKD+tpaxz84qI1AFlYLwjUfpRX/T7gEs
zRw5neCeJ2KKUPu9RRfwkxZydO4Zh4jgb4R3GL7HO50o1gR7M36sWyNECRGdSnwXmMdNv9ZlTbWc
lVLL4oHYVVJCrxtTTCmLo7MzU1ztHeDdx7zxDmLMPJehyGyHa5yArcn3uAUHIxafjpuTMxwCNYE1
280oF6keMq9KudgIsi+Zy7MakivEfbm/3YkVTZKdc81F2CjFJ3k8iO9BQE10Htxec9A+PatJrVqm
KtEsLqs9Ih4CM77v4IZUbzDflgG8cC8goFCj3ZFQC4HTLgbcG12v1SXM292lxkEftwOnywO1MQan
LOxyiwf7Du9vnjeqoxgS5xAN6K2I+GlX+f6ncTu/ZnjryaSqGer+1IdmFx/WNk9SZnhMWXs+uGwM
pZXfmBQAU1zHuGHZ+HdPXNFwPP2e5zDR7mgMTmGW7oDTuyNHgNd+/ycvmhwaVySpYUAkmI24e4hb
g/2TODNnDYocRhzt77XLrUA5YVqLuqKMXfVVJbu+WbGK7y12a1Cjd31569BEEAQrYd0DjT7jdK0B
EodzcvyzgOPO2+6q3xrQsdmOrjclX2BNyRqE9Hs+7iS9Nqd20mAq8wAHoIl19G9YhpaCjDDNgh6Z
Q65vlFZ++bmKMzbxtkjslcyY9QLzFoX+11g9V0FmS+AyG66KCBPFK51irp5hgJFXFZpm6SJxX2pT
iroIbT5p4MMdtOyvENkPqnLucCeLODAcfD4lRCeZNiTqidRjhuZcWBFA1z1iYfs1aLHE1XFBvCK4
iZtqlIsED10KeV9AC4VjqNTN5F5oAwdRhgvHqP866l9NDDYVEjJ/KvzsWPcq7eg5WxJW9qvLDYTw
VvoxEpsgUnfx4W61DVPpv311AFtRimzmKY31nCE6HfZ96D8Iw/GAaudIOMmIftBi5eg7YX1LNH68
9H7cUhBfN6Xb2FqWTuc6E3wzH232CvtzFacFh5mzBuK7Sl4zvLIfFQCJ4Qv/Hepna7lSNbXGHCzX
kfeVG7FRK15iQN+h5KNzcFDLb1UKEjACSbmScXZbjaOVaLU8x5nfieSWLO3In2tbCsefkkTTcsE8
8bERK/xPdLDTG9U99aj9aS1IcjkozLjKiBTIV4esvR+xZm8ixoXgMm2dZO8TQLRB3t+qC9vXG+6R
SzFmNDeCU8xcN4wFvOFlsIpfasaRmJ+mWQtlLQVeQ0n3tc3LgcqrnZMWzNILrjV2g5yEyz1/nN+r
yjE9YPTYPSr/6Gnxo5Izfl34YkhyRkOOz1iqT8oesDqPQsOj1xrQu5ytv/vmLKV30HcTomVkfOuA
i9hDl8Yyw1jMg0+dLD7QrrYLYQYc888vgoyfIW+4vbu1FW+ZbPJTBQ2XnKI2jhnqBzZo+7AGWjFJ
Fq8ZvBN5SQqnrWLx356H82TV5duOxz1BeS8HFoD+NEO32NIbj7TbvOk91l5gWStG5G6Cejbo7ZUh
pDxIsPqAgeVMNfcd5gXBuAR3LsXDTRKlarHpB/7+ZlT4ZJf80Pmdm68I7mjzKeur7UoP3Y3XVzyo
7QCRCgu9eKYK08sZTnuPm8bGUe/AI0k7odCl0UQW18Q6NHeUEdowrO3qipS+C5t7mY5CSK1gBO0/
+H9ervjQ1T1kiv5x1gUi0GfXjTiAxxQzmPYtfM70PFa/J8/w+uPx9wkbLdtJaJ8X8PD8fT5/q8f1
LH3h9y5/JprZRocx70cLXa2ac5tZyxyfwq+eqWmBEKwwd8ybM8H0J4bCXNdVVfMTozWtgujhdfid
vnkjfbJSKGYaHpfdziuckC0v/wCQq7RY8fb561jt/K/CuMSGgSkGaBx3OvUmPVUedwizJcMwZicg
YzhKLaidqivb5sJ07vbaZE4nCx37NUCnJ8iSqc8GBs2f8U0r0qG9A+NUjg6kETz3dLBHQRzsXgcI
/Af85gRZ5pZsni7iE9d6GJw1qO49c1VqOwF/63YXqzRz79a4jFOKdlwKOZ9MrwkSm00IKkKlMY7K
1lazZFDv71apWkggWFTNUTrkNm1PvgkmmLZrsswn3qFmRjLysn9LEQfKFfrVus8UCX01QITwU9H/
P5+YhIfF8vxnzoeftWItA2G5UyOsw+n8BWkSdWC+CIgWisi7N+AUj99P3obK3usgsY3jY17yxcT3
AQd3Yo4hrryvRUdO/rIvxxlU6VZWA5Suc0yz5OvUgLdAXJMkuXw+TrAs1PYy79xH++0OtDq8X7Oz
qEAYTGAD888y3hF2iDVBnFJeDp0gPSBqsj5xs7fE+34MY3NdXy+Dr3bFOT+qhRw/MdJImVCF8xJl
8bu0xgo4cca2LuuislH2z/41KTJkX0A/nVw9v6N3LlY7iMceSMM6Rg5DlY9QMkD7ba4Ox2vwANlq
2KKIXoOIQuyKDaMJHSPYmTMfPRR7N2oHKurnjJIarr782S3F/wcd0Ocz810265QtCJqZo2bY5L/m
nlIM2wg2NJbg69pSy/m23foBC8w1+Kej5gumT92mkcrKpW3NnlkAlv2g6MheQaA6bOVc4UhT3EIW
A4Qnmiv49BuuuQWOnHNdY49Ia27bxVLmXlZqONEOk1flWgAV6eGHLnstFu2vBqySejl6R7eADkNF
gXWnHUo9Gt3hbmrGU9o6lEUNc9I/q0WsL4UruXOMB2a+C3FsmGA6o+1Dc3jBDK7zz3iw3mff6jNE
RUTKnypcyksGIsN68nDZSBVfPaman9PEx/QgT1svHYRIRw6DNQMpqrdSIV7gkZ1Lwpoz3cusWhPe
t03IbRs33FFt6HD3sPleYdu9/GLdc5cKL0nTPkj2uVtmZLtNyM1f8HM3ubpTq16j6AjBBfGJ+sPC
YsN82qDLlM85xrkLiaoKKtV9jOzKD67Pcf1WDURAedrKnu8K77c/3TD2jQjkduPTch7fCTqV5dBj
4Mkmo2uygxQEPh4mYw1v+n5OYaWBBgeIxP8+v0UUZDh4ULbg4sOsUBu/FYMEIvqK64i8aSlsQoHy
G/rEGMB+ohaxMn49SIoIyEzwJywu9/JZJwqGwWV+Z3xeDomE1SUWkA0xN4DGIqFv5x2cHKln0vGS
LbG/D6Pn+cTcmTBeNoD83YmfAxQbdwps3SaOSaPQkm7/yi4VIjqoQ+Twj0reqzonAUTZg+M3i+Il
F/9pqUDkdLeS6e5FoPermtfIyNmBZ74cGckXtv27WJRrxr/6AN5BerAbCGRWbO9vVkH0lFxxzpjj
5EeFqoE1v/v5oLphGaXXXmgRihDnGtiaL8gesx6+7a85mMXVcSyqks/MzPB6Bm92CNJWoH25SONV
fUMsy/xdpTzkiAOA42gUV/UgL0rKdHzfUSlHs9AHEhLLvd7M9sxqWbh18Y+HpJFpV2syK7pfiFWM
r7c1zJLayuPl4CP08L5755QM2Rmf2tJ5Qxvj3ogAh0NumVmSEh4ZFcM+l83Z16aGnd6XqCpJGeSD
4iAFBmdvJIb8ZzD6LS1fIZIOnnIpBCUZTdqBWkixnqg5NnlQrP2rV9gQe7B6uGNdYFpDBNSnY6jq
F0iI+tZQiWy0DG4yWtIlSHqa2S0gY/yeGO4vHiKZo4ff7X7WLjxmEmqKsD4NdslQozlLP1uSNGYU
ORBDcAstTL82K9NcjuWNdWKnx7SS7nk+6Y2XmBOe4m4rucTGF23F8ZGiFhyLWBtY2hy6WnGDEUsM
M9wJmBcoDP7KOAuU3OJ6ISHJaPP4vVHcd8Q9sZwVTMfH5svfiG3ILqSw4XNF7pMTINK8l6AE98GB
t8bJ6EXme2Rm0U8briuUSwaQgQ1L1/pMFzToR+o6oQnjIBXuwhyxlze3oyq9+3XKm+2aCUT8b8v5
q8MDCGgA2gGgvv+SAsCmNX36VI1CeYKekfL5hRFjOVE1zHANto7cXv0yZS22uR3YWqahWQQhUcIZ
KRC3KhP9r55ColuuQUdSyJfFSwdMoUUUhlz8YkghX4xfu3APZQvX/6ztbDcYYSf0YjQWXcAl0ldn
65/1Cg5Xb/OBSuQ+3vJK4lmFxFynOHOcwfFWEFfkLk6NVyyV/nCjqkg0ljX/wugaa0JqsdH2kmjG
lWU879T9GRCbAKATzR6byWyErOBs7tnoGIKkSOZ2W6plAQXJK95PBvxmjcfs9FrWNS2wAJmhQK39
tVOrT46j3Hiu/hdeIl7BI5b/INp7RqPIDZI/xZO52Sz5lJTOKP6k7n6Qefqqk0O1jGE6QzQcz0J5
lqhjygMTG+wp7re+tLwgHCP83YQQ0jKgh9eknmFCFuhzLuXxk2uAy8Z5VM7IT8j4VKgONmgQM3py
73SuOQptP3mvWPke32KaeB36Qo1S+u7y+s+WNGMOkqWraYclBPgcTO1Smt8Y1ulbJ/QLgElIE+1X
6hTKdM81ZpaEvTuOtFc5OqA8Q56b3Ss3tX0Vm6X7tUhLktvtyaTidPQkZd0F68RwG0FVApZw/X5t
TjNsKhIfhMVmW8ggvcWmXdSicIXQlXstSduZDn4AZYvCL0Evxccg7TrSt9EPnS2/6Y1thChITU9Q
WBTDpXUHDTM7w2pvJpKu0Q255qflTQMnVfVUAm8R0ypqEhQ8ESO/a7Uc11aqU1Lk7KJd65of5bG6
8Iw9E9N4aiLWL9NCaAaQHWrlH3iKzXrxWwXFHmOvFGBkOa7Cc5o2Aj767TAzKslzTDOT2d6KMGhX
y9BMEYvDXB5dzjTDXkw1p6IAIboRAM8L7HUYLDSxnaBgQHn6nc45kgyz2wzAyEno1hS1MhjHwauV
5GxPjlOTooCVPB00eL2uhEz6GP1sSrYGZZ+azOW6cYevCLfDWqozRltTcVoQsnu2azBsr3k12Jjm
51TkWTpK1dL67wsh3NAuIsZ5TqSyO6pJYHcS7TeF/lEZmJw8L7Ai7ue/K67R2FC6HHGafJXbH8Rl
bGQ+6W3jqXUGjuKaIojgVAHGdpm/GE4H3rkhOiM8WmyrNA2aro8mM6gJUsSlWNpxSSwkA3xFyZan
yhTEZSRKXeWX7nrns4fDboPlJdvPc8Esq8th4lfeOcPKM7sAHwJyuAF4FfghIDpj87BlJr1Vw6Gs
naliBLe7UvmYeHFNTndpP/12sYPedDzxOvlruOlnQ93zjBaUIUm/P/IoQ0NjYcQpZXr7lObVI2vM
qEzj9WBfCGnx+SM3wg/mixPb6EfQJPohmNfikwOeIgqTrQaq53JFRZx8G7FV4kZ02uhODvBmOhr/
AOPtqPXPvfPf5kq+bv3NQ75SlnmaFb0HT6JEhbO/8zPtph0o/HO283VveYiS0pMJAlxwh5o7ome7
sb16PrzPmXRl1TqjlzwL0ENWpRceViqCVdYrHMOLlzhhvPiEEq8dEJu3MLAp1pFdilIxJ7pCMp9b
FZWPQO5UGeVk1ZUdacExRHOoW884G5py++J9/JyggFOE0907nDypLOUwUyfjFduzlHS7zzs68GlQ
7F/LG08OpuVEUi0kyghEKjkvZXWLKH2CDomBTvuKza9oUlbbc4+IDMXmXuC5m9xVqfRTqGwROQyW
nwtIcjsVTnlHr+9QEVpDl0DNjmtJxsNJ1a57cQFuJjA27f8H+sYJKNHlUzH961gpPuGHmkf+QFDL
clKoYiRg/8s1JTk44b4Gc91mGBMTGk0g9CkdmMQFI7bZz1EWBxQLzqn0P1aPQZIyJpupctXDkeOs
A9d0mYLh8CAdeVFyQWvqfV+5CwFNYmrWjb67JzbOxR+iOjLcosYyEBw8HtRIvmZBUqgaNQe+pPy2
OX1CH8q6fJBzhaU63l4rCvAerphYja8znI0SPnsVoMrSxHid1wocHNz67KpsC7fB/HFkZ8/2aHAu
oy1XczWThyssCCc67Vatm8Unua75EZEcJMTQFidAvOTE1I1t+4iXaSSqcziB75eZ2hu3KcftMsyr
HDpWomyM9/7nyzqgHoggbOv9PVzK+I9EH/+SBTARDRIw6YBVdGHexZbQEq9Wj+Y3u3nhxiDAWYQa
r21PpyPI/R4EO8ri+yRaw+rQbXAPA1wOnWt9P6w1IHoMNak4CaaPc+Vy2YUi1vewkzwT+223xC6m
KZc7+ls6jIlgZDCjhRjPj4PEiXGGkJgatBB+0w2VR4uWZhAKnPCzdM/bRhsHglAQUMDEvnGyWqSe
plXrYQmHqGY0MpEK6UIVtvMhRMYN1v/jSjskOC+VK8QV5n/1IqEOf1DnciFEr03w2x2zCKEr69aC
wA9FCJSr2fUQu1v5Z+j5hrDUViugWAqrk9DjvkTP3pS7FWi0vf3LyCnkjzyoG18jM+NxnsPdMM2z
74huZ3vUpuQGnl6yf8s6kd2WabCHWH8/MqPzmmgSxQz2zE08a0tLfToewfl2ZpmzC6uk5Nwsx4Tc
nsw1eiYJt1A6Ems7n4nrC2lTB9GCnSvgfy2UyEIo9FceKlOV4aKnBhl8HayuuoHBDTtI/bM0Jmr7
J+WCZfeWdRXIwhbCSdGtaE9IlEkcVXXTQOsFZn5VEwhpyNM8BgkE+yt4jO2duNaHVGLMi0GpBzyz
l/SEWVcMX39NHXshPHqFQEFQZ11i8t8Qb/OxtIUHNDxtBjM3mdtLRITJHJ6ltKRXSxNPCPxUHN5P
HDhRx/zFdNKaOuyCz1F+MJZvroqywHMUy9oJumSvMg/hNbC8I08yd8Xh3Lbqga7AuQlljuzWFfAZ
fHNgal4r/d+YzZuwhTTQ0eRbmuazHxGu5L/3TnjD4lf2F3VWOdT+dCCr9IxABSgIVXI5N9gqHGHC
IK4caSB/kpFWjRGHswXOuI6rtD2gjHJ5V70+Kbo5RjRpomarF0agqIOU7Ut6vG1d/LmOG2w1ZDdU
YhuGCzoEtb6UJVuazE6NcWmVYo/OSEzHG5f+UbRUW4W9/mVmcYxlWmE8Ci4GUwkqK5vyDGWXT0HI
385Go/zzsNZrv6sEGnlbONwk4MLhJ3cpzZ3wxnFj+opcVa2cQE/3n3ehO3egk1TW6iGkgAvZi9Ul
uKzCbJ7Wesr4akLzi++oe+0lW/y9xbtfbf9XS6Gc1MBvD0xYH0+mUKVixr1NwrAiJVcgkbSlbKyQ
NNdWqFxD/lcVxchc17xcuC7Rv+h+yulDfLsvfTjDpJGvhzDYjcrNg6scQc9/4UjCKUDBLCh5RIq+
wJyLpNtpNE7A4DNsySqGA7WjRn/Ul1b8mrKY7m63twkZi1IqcW2KIQJOdEiMek00xqV2ib14t6aL
TR8TZiyT5OpuVqRha6UfQJ4UhxLEKy0dfQqJ/+qd06EAmGIcDNQ2tusTCqU0cb2CDYFY0NeieDfn
jk60fRNQzsEgTz2QUCl32sww5qwafcK0cLR4K6qdIJz7o5tWrqJtPOFP18uJJMB96GDMUQFgjcy9
AOSMJmm+s7sUgrKtTDeGuDLFzvKj1iEGRrpr0A9Y90DSSn4mwoqiHF49pgpbV9gTp+qbkv+UTzgW
CDI4MsZQKLhcbBSsGLX+wfl6pHfQhwkWea2sFWXNuKgk/erw5nOgjjzdmmw61u/RS2rcBeUksinM
JHMH2FJizjHuQ2nlgkmMTRb9by850r9Kot47HApzp+CZLG/s///V2ofdRH52DS2pnA4UonH+krqt
DLOnEUZBD1PDueQ8McIF+ONmTfgyUxfog1nwdw+HBk1HCiKLgQL65rH+H7PqAfZ+pavgbC1ZT055
dxHiDe+Oe5H65V5Q7BbbG5IP0WE0xlBi3LaW8LI2XOTSxgyTV8hrqE9xyaZE98c5GXCib0DIttMc
HXB5Dwq2C89B4LJ/nob1LFZC2ot//BC3F+Tec3DZWh+pZw7zAf1HHE4438Dwl6evdMAh0cPkvyrb
kyQvemzPoJUawDQ89EIuDTybHfq3wu+NdLfTNLAdyf+vcozQ1k3zffbq38d0uNPbrC2fZMaG8sXa
+U6Vtu7cbOoZwY3l9jdMfwxKc2YRgKT0rZeXqsfiGJQ447irxu3GmL9bve0G5tk7HJZUfdFPebhm
4iCRH/yAa7QhGbTUfjEJzscbxb08N+1By/ZoSEJOIxw5yBXnjzBHMY8Wc8HvMLYuZidmPZYPFW8q
WdTbg9oa4Z7OolHW2hVDP5Jhwy6u5bDEPCmpTrMg3qSACZVHOhtR/UwvuZ0Jig9T1HcG/9IffD39
rZliBTi2bPIPCqeL70FS21JiTkImRdFNM9lE8qHgShKa02+2h4EIYT6dJI59yVi88GWIsYRPG7+G
qnojxn24tyP0JE3XdJlLQuEfx4Gcx2Bqr0Svd95+5uune8UCeVnx5E99P7fB8jhHjl9RRgnjVCnr
oS2jx5/GnIisF5tlJhJJ4HpvkaE0nVont6P9eBxfnXswXuEp3K6Elifa0W+MTC/zkehCzvL0HewA
dmDj2pqeRdtG8A0cUlPLJjII7UGyR/lK/r/1bvIpiH08cwBHR4q6try+kBrI8gOgMTxgJu1pObDM
PcqYrjrkZU6o6hzr5yMj80oRdflgGP2diBJtrpIk08JZTHxK6QGB3/wv9enF6/cWQMvoaIurLWcE
t5VUUEGlpEAg9Rxxa8tamTX9KolFizvRM+VBf4Q2lYD30Z8u1ZFC2ZD+DGo5U+oDA6XwpDW50p7w
n1ix8pL1fT5aTzWPF9OzoWOuKkpY0Bzbr6Ro2ZSa6fw95ksClxYuJSN0FfEFcscnTYkIs1tiFHtN
UuJdAgQrR+798FPXNI7bEZuy83UmFWdhWO+sGuu8rdcIRGE73bz3R4QRIFDbi7oamlqGu+6S66+U
OCZWRT/dFXcsA3WZMNYm7V0SkpNBGIZSXimf0gXH6PaDNo2mGYNsmHh0O8X5WUXosN2y+jsJGbtj
7niLr1Fn5z2lP1IqHRFDWKVzPLEioRK6o1tP4it2/v06nQ7WTQEZP9sJ/nEUxi34Ck0t1PlFeLH1
FcX2qdxwvD/RlIo05E4/GT+vVzOksmFLaEF2lVY86U26cPVqLZTMUApZ+cV12rfbnqymx7B7dyRn
GfeFCut197I/iX26+4lOaHdx1YZ0Yv4x6ia0+aVN0f7is+R1+01mjulin5RRKZD/NgQmIXHJO+50
UhWZPj3ALer/fPoXjxnMWH90GGJ3TdzJx438freVXELZuy0HMp5g7AcD9l9xtc0cc3xY8jWZySGo
bNCFmdeHFP6c4AuDafxMJIZsIxae0GyoXTme2f1FCh03QlpH3MW+Kp3Jytc9gjXuXQPmEc+we8sq
uxxXrPU/qDS0QvCNVoyuqpAkUSN6yILw/ykgvhfW4gPxEWUznqXphwwganWOCCHPOOoR1dAE3ZiN
TD030aQFoetNo7MApSyFButQ8QViCQzm53ALO1v/prWCCLvJlwJcQATthUpJaWU5ljr8C7NN3Ffu
2c4K5cg9vAA7ZXLtxxV+7KuXLFizgrysbDE9r19srHnDDu0YsxORZ2Wtt5S+mF0yvKX2+xmJxoxn
hhE7Qp3c/TZsKEiPtDZGRwncWgoDUixGKWcGN33hwrdb5y4KSIKjyDQ25d5JK+13iAwCPCwNxiYC
lumJcv+dWdK1RPycmlPGfR9B6O8Nzpl1oo6sGL+8AxH9ZIynGhDD+9S7CB9S55Iz+yzJVlDth2Si
CvfdSkK4xxGeHJ5DakNgwZTfOjCmLZZXTVgu+CiJLq7dY5N3OyC12yWLDnZQIRUdRDZVcfU+4chM
uVguuGVCXJoLEnyi0h29H/opWubZZDJupZw7PYEXa4WeC9p2Xan7vKZxFCjkZdDZ8HAM/BmcBePR
VMkPRjDPaGynGjuatF3BqjQcxXmvhz0ye71TuPbSj1ZfEH9EX4kFezBfWYrPjXidd6dHHkwXwxlg
YX/YOKqGTZ28SnvrZf+NBqE2h8asX+1fJszYlfNajmzFPV+85d18O3FCSo8QqSi4Wxf6i/bpHe/d
Os1paya2t9jegJXvJFs7ogXVuic5DJS+MTSrRzZzcr2RqOWvMn/zvNZlndjMzhUqR7AgiSyeIfGx
VG+6rsT0kuSnhTPUTMJaWkm3cj8P0OlY4lc20tc1tHwpPeeOKcHrSKuFyAkwpJd0mnqmhzPWYl2d
BWvSqdl6sSLtybOUbNmjqJC0pnaeyCaA4BgDkzrmIz46np5QZFBNCxGTacXVvesLxYkpSOZrOY/o
nKtLVBqP/QOOj9coH64E5HD3O5TTabMoP2HBqr+YkvTaEph19EJY2IgqxqGjrXC3kpB9dMtGf+wx
eT9+Yb1lerDuOxchum3b/sR9dgw4x4z3GQ5Rs3nVDlozBNvYrM5ihNTfbJ841VbEcS68JKHML1KZ
A7ErKg4P4ZPRbYZ6XdI6hajpalwvxOjjGrXtxqWpjBfSv5jq1c4ZiTDugFukUb8Z1plfxqiDWAqP
6PjXPLuYDH6RRZqXrs+at29JKshBdm4N2emMYVSerwDSs9GBU8EOITzMtYGdSubU7dCF9iqmxyWo
0elo2KRjCzzHeQHC/Pc+S2Sk/b30Tg5MqjuQ/z5xM0jB0PQMebxNWKr3pdV2b0J/OTPjG0XUvIWd
FwDjy4W+HaXqHKAOg4cJCMtdjRaTkIpu9JC6oeMb0awLf9rFYU/roc1DBnFlePdUyANC36S1R71l
fzu+ZyoIHRophZRB0ucmiDjf/izpMyH1xVBrSf1k1N6c2RJ4ZLl/1psjk9Y3xElISBvCUWEHG2Bm
o4hr7eHwX23qaLLvrYabUYLS4IegZWEuBJbxdFv/0MsbUQu/No79KqZNI5MkCvTGXAujyxV5WeBk
MJjER0ZuY/Qe8WTsMy0C0lAmW9ScAgQDjZfJG/tmWRkg60P3h+PxrHlufW0JxzL77+qm+7X57KwU
cLbfofxENv9Rys5ufpSO2rV8VnbTzlf2MtA9ufZJwBlU1QZPZTV5Syj1viTI6islOmUD0q/3kG3+
8l7fKiiTILCX+FWXNC5G/e+HggHYYxcgCcYXqhknpl7KY7m3NbHpjFRNtfT8kffhwWAIcj7ChAzr
krYiQt9zFrkAOykTPQ8szQLSqFkupj0EvOU5Cws/ro42YlxMRRdF76WL3F3Dkj47NJJPdB1otJlG
TXGX4VSGRL4cESO5fCHhJYAR07Da2z4hcw0i0peRtmQkO3dipTlP5ZdbruPndDLuBlJU1AJJIE0r
PX8WiwEER3hx/mrsIWkwZP9FGQ7x8jJGzAe7ElXhnXHwd9BpiUqGlFW6vm8mhTL04+Z0Lrr2Oxux
5P1v3XpUClv9RI/49FzxtyJOKc3ki4wC1uhthPJiZS8N31zSlCqskWcJcZSd14IjoWRfTIkTGZdN
YV6hL/tJNcOIZnew2cjIop2q5BTvt4yMBcbNKuaIQkePNMAhqPMPQSaPDz06DNvAvEZXfFKu8Z7V
sIAExCRHNTZWG5hmyUZ4o+AZpG7i5GK7j8LMKrgy93t/ThszFeTPJ60uEeBBL89tyK3vACamEIcl
EcZAEWMhcwQ71JqiP9FMeOyDPxnA266F/GVsc9GQebgqKHcwzHXtIRIeEjoApNlkX3YkcNn0RSPY
C3wsDVKu+akeGWQiIC6pXxBoojkItVr5aQKK2Ks/tSW9+Uki6xqVuIvaFwege3ysnSl4KH8x+149
RKdYm12jioaqtpTTLSOQPw6YE+dTiSZXYQNLCYE3CCDJUOnUokUeYDNhvt6iqtByGgAB2+5TUdG3
9GnYGJK55oVKG/9QptmfZo2WHIelMvBJe62vnGUa4xc28zT4ual/wvQnUyhQiZ6eCDYQVlI4zQil
qYLKIQcjFoQd3Qi2ifI2V/tbfnxkHSNhT/zXoU07kL68MDMusJjkGtM5mJIyR85Np0YDlHUZBa4t
ni0TmIU5/Ko1z8AycdFLFezzP9Mf1XUq9jUV8a0AiZLsV6kXOP+ZPhaWkR0kJ8h65354pCbOFyFT
XTOV4UgIETRHGa2MQJz8XjoB3pnhZTWozZ8QEikeqKV/mzHLk5sJjWyNEOH0905eQIWJy7GJmxH7
K0rf1g643W3LX1jjLZHu4etgxITG+xdJucduUZIwdL6gb5H8cPmM3JF29BbUob8UVIPKa0BLltB8
582JcCf/lI+fJIhQAn+gsqGV/Ul9HVWYLnMO4wwOCwLg8bYfZ3Sj3B0SFJGYvN+7uwyoFfvg2hr/
nDlTWMMYs5KIRVseM2Z4U8VIjjYM3FdwWsfQATNEz9JH
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
