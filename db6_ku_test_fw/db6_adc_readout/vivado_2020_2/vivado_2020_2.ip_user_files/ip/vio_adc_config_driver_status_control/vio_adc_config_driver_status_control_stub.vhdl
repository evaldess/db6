-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Fri Mar 26 14:58:30 2021
-- Host        : Piro-Office-PC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/vio_adc_config_driver_status_control/vio_adc_config_driver_status_control_stub.vhdl
-- Design      : vio_adc_config_driver_status_control
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity vio_adc_config_driver_status_control is
  Port ( 
    clk : in STD_LOGIC;
    probe_in0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe_in1 : in STD_LOGIC_VECTOR ( 7 downto 0 );
    probe_in2 : in STD_LOGIC_VECTOR ( 7 downto 0 );
    probe_in3 : in STD_LOGIC_VECTOR ( 7 downto 0 );
    probe_in4 : in STD_LOGIC_VECTOR ( 7 downto 0 );
    probe_in5 : in STD_LOGIC_VECTOR ( 7 downto 0 );
    probe_in6 : in STD_LOGIC_VECTOR ( 7 downto 0 );
    probe_in7 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in8 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in9 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    probe_in10 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    probe_in11 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    probe_in12 : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );

end vio_adc_config_driver_status_control;

architecture stub of vio_adc_config_driver_status_control is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,probe_in0[0:0],probe_in1[7:0],probe_in2[7:0],probe_in3[7:0],probe_in4[7:0],probe_in5[7:0],probe_in6[7:0],probe_in7[2:0],probe_in8[2:0],probe_in9[1:0],probe_in10[2:0],probe_in11[4:0],probe_in12[7:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "vio,Vivado 2020.2";
begin
end;
