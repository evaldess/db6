// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Mar 26 14:58:30 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/vio_adc_config_driver_status_control/vio_adc_config_driver_status_control_stub.v
// Design      : vio_adc_config_driver_status_control
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module vio_adc_config_driver_status_control(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_in5, probe_in6, probe_in7, probe_in8, probe_in9, probe_in10, probe_in11, 
  probe_in12)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[0:0],probe_in1[7:0],probe_in2[7:0],probe_in3[7:0],probe_in4[7:0],probe_in5[7:0],probe_in6[7:0],probe_in7[2:0],probe_in8[2:0],probe_in9[1:0],probe_in10[2:0],probe_in11[4:0],probe_in12[7:0]" */;
  input clk;
  input [0:0]probe_in0;
  input [7:0]probe_in1;
  input [7:0]probe_in2;
  input [7:0]probe_in3;
  input [7:0]probe_in4;
  input [7:0]probe_in5;
  input [7:0]probe_in6;
  input [2:0]probe_in7;
  input [2:0]probe_in8;
  input [1:0]probe_in9;
  input [2:0]probe_in10;
  input [4:0]probe_in11;
  input [7:0]probe_in12;
endmodule
