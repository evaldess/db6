onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib sr_ram_adc_iddr_opt

do {wave.do}

view wave
view structure
view signals

do {sr_ram_adc_iddr.udo}

run -all

quit -force
