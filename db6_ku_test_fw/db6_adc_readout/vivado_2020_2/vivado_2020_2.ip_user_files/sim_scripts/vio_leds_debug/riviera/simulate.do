onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+vio_leds_debug -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.vio_leds_debug xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {vio_leds_debug.udo}

run -all

endsim

quit -force
