onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+mmcm_gbt40_db -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.mmcm_gbt40_db xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {mmcm_gbt40_db.udo}

run -all

endsim

quit -force
