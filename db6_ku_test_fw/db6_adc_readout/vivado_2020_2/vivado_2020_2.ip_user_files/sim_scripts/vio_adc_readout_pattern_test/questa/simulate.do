onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib vio_adc_readout_pattern_test_opt

do {wave.do}

view wave
view structure
view signals

do {vio_adc_readout_pattern_test.udo}

run -all

quit -force
