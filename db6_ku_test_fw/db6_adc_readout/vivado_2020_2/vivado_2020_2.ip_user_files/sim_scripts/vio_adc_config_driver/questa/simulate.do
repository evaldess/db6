onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib vio_adc_config_driver_opt

do {wave.do}

view wave
view structure
view signals

do {vio_adc_config_driver.udo}

run -all

quit -force
