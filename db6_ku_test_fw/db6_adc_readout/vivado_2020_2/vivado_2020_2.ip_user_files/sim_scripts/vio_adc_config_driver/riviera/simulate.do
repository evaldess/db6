onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+vio_adc_config_driver -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.vio_adc_config_driver xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {vio_adc_config_driver.udo}

run -all

endsim

quit -force
