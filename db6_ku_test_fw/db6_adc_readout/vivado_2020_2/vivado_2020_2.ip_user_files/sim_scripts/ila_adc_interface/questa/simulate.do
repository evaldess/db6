onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib ila_adc_interface_opt

do {wave.do}

view wave
view structure
view signals

do {ila_adc_interface.udo}

run -all

quit -force
