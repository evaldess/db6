onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib ila_adc_fifo_opt

do {wave.do}

view wave
view structure
view signals

do {ila_adc_fifo.udo}

run -all

quit -force
