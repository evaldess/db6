// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Apr 14 23:10:52 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.runs/c_addsub_synth_1/c_addsub_sim_netlist.v
// Design      : c_addsub
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "c_addsub,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module c_addsub
   (A,
    B,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire CLK;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "15" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000" *) 
  (* C_B_WIDTH = "15" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_OUT_WIDTH = "15" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  c_addsub_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2624)
`pragma protect data_block
PctwYMMutzA640o01fOmbNFCJ0qiYEA6F+PsNIVOwC/2hLqUzjY9caCBe1diKqa+6/2O06dCi2Bt
9A4SM5WXeypa/BvB4ZY0Ps73vW+CXwtBDC7f0Sd/Dfp3Njn3tB//bCtwbKn7QgKIcKV+aT/4Yx/U
EzvZxx44oKYowqfOfY7IpTZEa8uzsqWhElB44JgEjV4qEQPWlUe3yTPxOF+HgiRze0/iq5Khv4xL
PgUKANHejNPoIb2S65gIZVo7eTtoa5lv/FTnYagxVtkho/fMJduSPLnFYMhfuo4BRxOSCUpzKDMC
Ye5FE+qu6ca18LRW/F4AbLFPX+XIRoiJm88Us+4skV4xrvZl230IdwnQkjiel93zodMxeAaMYu4C
WX3EeOmfIdSWd/48mmXg7ORMq/mmRlMxfWGS6Adpn+2Mo1t89TIl+UTMpQsOvlKQLYeGNqSGj9pC
2rWkcqNuZiBqhwiLmpxMIA9vepzf42DdSmUDCkslsLxlI2cpqS52L/EUfn4nqnfzw2rNdCBEeV82
40L9YEunmcNn+DqDhOd9x2Uxzd3jcHrEZTSCUFf8p45KFr1fc7j0mYjsfX201WryK3D16qLYXYmZ
W1Hj01qbDMe5B2aNEj0PJp2i1S4yufjMW63akMi7YNEIFs8NaphTtYGJzKBEVnNeAtTIVKY1tTkh
F1nJv/U1T2qTyCfqGPUCs3FM1lKtkq01424+Xsz109s3CvrdMwsH3LZ4HTIasVqOe7Q8bTXIxG76
0A1Ys/Jmc3k5/IVR4+uZ9S7FLMjei/HkvcI7SgoWQFMTCFcp+TB9O99mv+iH2uDI2QXq953ETX4A
KL8Fgk/HTPwv7wYV34wtV6/WjL+swCfMOrYp3oKb4/N7rSamoWhk8feioeyruUYf/Q25iP7gdU35
y/FLFsWpz9voqSX38aFLXgSSEa/S5ANdfw05rbuuoXdkypq2fjVFkfOr65cYoCorZ6VbCwq9YLMj
d0WiCB1jDwQSkjyuIKpqNaMO1DkcJjBLPOhNwf512WhCLxFkepazxpR2HK4f25JGCdj51Tdrh0qt
zJ2vcfdf0jPI7ALR0ap2Z+j9zRRaJdhxk/75AdrO/mkmX/NBXiq5iP2tcKtrXJtT3pVibXqMSNN1
2yki/qM0e1aMoMxeS2mhcez+Yqchm/rRPZbYtTeAnBhPX8dtz7FmmARyuXT0O8jQGcuReKB2mxpm
2GK7h0SV8iEInCjqb+NVMuIaOciQ6g6Sz4WDteWxQzyvFcHJxH5hKQutx2gDsly9I52dJ/7Reup+
8C/TsJ2/ipLko2cn8Wz7HaiogUUfmDu6Y8LzxQZlJ3FgT5zDYjDp5RQKe/4iSO2YJ2OpMhBdBrkK
6QsdSANOtiXNMKzxrjw9MD1vhmsnpzsoHM0yDOUrtDKRASzePdT9bNio7dEprYY1LxoYN5II7Dx3
Coc6S/pzCOxY96PNM0FjW/C8EuGEH8RZJHYyqB4MHY5oi1piGOHPL8NP+JWT29oJvJKZtKGFmRkE
fhJR9nVHnH8lNTcYTgLsO/FGdCrzJRVd5xnL/8mZ7toq+Y1QPca46EMh0A6xNgImrYUghl5goMtf
/KSSHt07+gpSl2U2+Zv8OnxwlQXGqaORDqOHHHQSyJ+KhqXF1VQLvyslJ1TN4B+rqQ8yGWqJ+XJP
gGx58phjdO4C436Yt7tR0Vm0OALcgZlravLpCnx6GO6LFe/pSMTq5BuvIcDpshTRe3OreHi1tC3o
LGteDkvZHBoAw061zLhbvJjf79A05SP/KQSK3Ha/LJb4LShtSH92oKibvfxf405OZi5aSwz8YG5F
zGBsD/WUUal3c9FtDRzUkECh9THz9Adh15kFBpB7NSiJZgnw4JZL5NgEizL7kkTBJYG+zvUt36GI
s/LjfosdckrmS+aQ3yBQNoM6sPkcpcwr7sGbwHVh5yyFvll+b9r/efJQhiO5QqbdoU8EfXWkf4Bf
DSj9WyXhVG7SzKw1rRSNjNds+fcb6021HmJuXsI0CV8hWcFfTAJBeb7vrSqK8al36eoK8Xscb+uQ
2Gc5+Gys7Ud4MKJ7IHegGjzFpTnu0ZfUF1xCoBJ8ZtLWMAkwJcpFEeXSbTq38gUF3vLpw8Y9Umtb
3hiNb8iuawA8fgMcYGUjDd4Q72SgHrQfYvLs796iG7ouE3QYSMC/UcR/I4rVuaWJv8SJd1hANsS/
CxpW8QwIunr+OYPYZqrBa0hM0KLL1zm24BZqbmadBkxOOVtC1YdKIxWpviVn6HW5B4kHkS3d7dxL
vmkh3180h544KMumr7FxFSZBmUjj25rSAb9DrnIeslsCVS7fRy20RSlkOr81qSAShoSn4pnR5zYt
O0cH5RJs7RykyucHvZALxmfwIqjRvtR4qZLgXvON6PhmpNvQgWoitphPD72ZJpNPavQ2+9wNMJB/
bvmq3WL/xxBSp6KoD/nGiAoOwHozzxkOv1vfkRjb5mpJMTFg2l7TQtRWV3KeOrxnxX/MxhR0IkgB
0FURvbQ+4cOztagoja9nc4fyKsUaS1DEo5aTHsp26ur6MzptKLhE30dPC6njKIaE9O8yvXaMFbpz
GPG1JQ4jmup9tCQ74/1IrdTY2P1EQVcCMDNbwWTthh0CsqctdzoIRo+TA4Jiz/OcrP+e6F7LW/Oj
oXH3mhUqieAFAaVLSX0nvxaunekw8j68GAM7KgWePM4WnUPgsR/ALnpNFmoDSbEHEn+oGFr2mqwK
ApqSi8oXeXAvkFkCyrWqjs/tqHkBc5YsJjLwfcru3p2yJB8h/OPEZT0bK1qcQvEBIerEQwU4hagw
yyGl7awv3fu7X9MpPwpu8cy2skLUi7fKSTVhTyLgo3ZbgAcf1WvtsxTf0mIhCdspQ+RUsDjFd35/
f7XT+mXeGxLBqIIJOlKtBPd5fd8Ug7/xJxGWAPT7ViPRf9kXipDuFQKeZwvmG/XCVfu7eUqwv/yb
xg0Slyk6GGQpkfP2qP5x2j84+Yaqpp5Aajh/Ll7SBenE9MEikGTGYiiDZSrBIQsuJxrK12DLQH8U
06AgP3l1WRYLNeHkLeGuHUDNUQeLWzcrV+WA2bxw+FsoYaVF+xkXosPfZH4BjlJD/AfkQbeWdJPB
5wgw6axO6VpugKa+I10c+9E/8AJFOQ/lPLDFO/zNrBpscJ2icISi4xwf+8pBI0MOSWGx261cw9p7
Z8Rn98zxc85TeLQb5Q5PFePULrYdiDtyEBHcOitW900ohv7FSoT246nh72EIXGdYP47Tx/fL+eMd
zxKBAB30e8+YxPqAro5pLJwGMzgT5/J7ckW5JlvnezZKts96QmHtc16grDUe2Pdx8PrI/nJCBMV5
lKKoDvLaBRrW7Kl44qOKc5kEkZXZqR0D3VA2SoYU297NBtjgjvfRLAx9eMnyW4/0WN2t0Tv6ct+m
KObsmRHPkBjT6yXTYyIZ65zP5KDZzkNaf+6Yl1IDZPOUdy/2DAD31zbMg1CiutPwNWCcmzP5uJQC
K/I=
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
F7FJBuxNhoHeiJuTbfIYFXN+c14PC7pxa/pxWISlHqFCanagLWoD/SkDJCZLrYKjE+P5ERDqc1wA
NlbnnIuwHpSNhMYod3DO3rOZ4KzV16elceVEkMSpBoZvxjLI+GhbOMes4uB6Cg00wZhb9fzrHhWx
rK0kbG3IqZzadljc03ff7d2Wb3EzfeX87fXRr6hoL+3ZudLPPQVONRPen7rVqgAF6iGLGgTv5UC5
45uC3us3HYdNNB4N2DRW05QS8LFAw1OPwCQkj0ebCqdFS/o1d1eiSUjCgr/+TQsGj3fKRO+DPZCZ
sHJ+FluLOryjeDZtlLLNDN7FbVxuP2SXgIJgPw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
laUnV8P6yU9tr5vVHcPTGcwyvnlZjmteZtZcf/htOo7I9yoWrF8EJikNDWGMeGE45/KtX1Ft2gtJ
LjvtpJzWv77KhZscQrLo4xWMYBKsVHpTL7yGIm0MMyPJFHA/xiZ2ufEAVJQ83levJqmK3d0j+Y0+
lk2NiW6690JJm+4v3npotxAdbJKslKuynvMCzGrmpzMtSIR36GFW3mrzWMc3kCsR0oMrioKRV5Rh
WWNGke/ULz4cvkuECun3VVZj6aUgX+edn5RLTIiT9pGROWRjQSEGqvZh7M00iuQt31llDS59odFG
gzlUS7ZgIMuuxbiOJ8SB3Libs0OhnaN3fH+AjQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12800)
`pragma protect data_block
PctwYMMutzA640o01fOmbNFCJ0qiYEA6F+PsNIVOwC/2hLqUzjY9caCBe1diKqa+6/2O06dCi2Bt
9A4SM5WXeypa/BvB4ZY0Ps73vW+CXwtBDC7f0Sd/Dfp3Njn3tB//bCtwbKn7QgKIcKV+aT/4Yx/U
EzvZxx44oKYowqfOfY7IpTZEa8uzsqWhElB44JgEjV4qEQPWlUe3yTPxOF+HgiRze0/iq5Khv4xL
PgUKANHejNPoIb2S65gIZVo7eTtoa5lv/FTnYagxVtkho/fMJduSPLnFYMhfuo4BRxOSCUpzKDMC
Ye5FE+qu6ca18LRW/F4AbLFPX+XIRoiJm88Us+4skV4xrvZl230IdwnQkjiel93zodMxeAaMYu4C
WX3EeOmfIdSWd/48mmXg7ORMq/mmRlMxfWGS6Adpn+2Mo1t89TIl+UTMpQsOvlKQLYeGNqSGj9pC
2rWkcqNuZiBqhwiLmpxMIA9vepzf42DdSmUDCkslsLxlI2cpqS52L/EUfn4nqnfzw2rNdCBEeV82
40L9YEunmcNn+DqDhOd9x2Uxzd3jcHrEZTSCUFf8p45KFr1fc7j0mYjsfX201WryK3D16qLYXYmZ
W1Hj01qbDMe5B2aNEj0PJp2i1S4yufjMW63akMi7YNEIFs8NaphTtYGJzKBEVnNeAtTIVKY1tTkh
F1nJv/U1T2qTyCfqGPUCs3FM1lKtkq01424+Xsz109s3CvrdMwsH3LZ4HTIasVqOe7Q8bTXIxG76
0A1Ys/Jmc3k5/IVR4+uZ9S7FLMjei/HkvcI7SgoWQFMTCFcp+TB9O99mv+iH2uDI2QXq953ETX4A
KL8Fgk/HTPwv7wYV34wtV6/WjL+swCfMOrYp3oKb4/N7rSamoWhk8feioeyruUYf/Q25iP7gdU35
y/FLFsWpz9voqSX38aFLXgSSEa/S5ANdfw05rbuuoXdkypq2wbDbm+P5ciAGuFNHdyz8Q5MB6pu0
0Tdw8PwC0BEUSXdDOdHvGlFiOrKCKH16jP4gcAi3VlRsWuSBSP+rWp/l/pE0roQIBScs1sIg5gSv
priyyH3DuEwMMLgjc+h0C5aYh9l1pFZ9I6j8KHRZSJIF3rYfYSHSYcNztmdbSGkks52CNQtTo+fu
TQ1tCHIcoh+LDF9tmJV8pAlWMFmsexqUQTcy1CDF5rvhy0JIFmazfylk44buG6yubklRNA6bGCAg
fje4hUVFxQ2wsSmDDlZd/CEAW8XdEag3SlHIQNh6rmmulSKThdfqu5Aj/mLYW8f36wrpAGUlrbTv
eGShb8khyfaJVJfqAqhiys0zHzWVPKNrpdMcvcP0On//Bw4rd6g2klXwUSkIvFryjPeJaZXH7JVr
+bcAcIpqGbHuq9gb5qB3qElJ0uO+R30vukRtthMu4XoBQp+8wPURSDQ0v3HeGI9nzfVgLbJ0HeMT
DKMBo9QCAQaUGosZvcCsc9OzX5XJN4vAZ4qAsUb5CoQ3TCaFRCvdbqoEcWD4uLkQ1SOaxknHPOaX
mpB2JomZO1X2wRB73DnkwnVQkzMq/ukkT6y6OkbmeKMrWmMojv1dFDESFQ/1tWZSVM8DIGdFsj+g
zm14Yx/JUbR6w1jX8UmPtoBEMZlqCKGiq6myF1memVQBqE4W/6sV8n/HKLige2myK+w+NbNh50dO
yPNR2sNb/iD0QzBGxB5TQn6soocQP41FDllDLCZAA0cTpdFk9p/EBKX/8SXr1JWY0IN3ByyCoidv
/0ECczmF1jYrqwWDVl6Pq4Ld2fJQ3URtX8TEmsyv848w6xQ+9u15Y64OzQjSNhBXJ64jps1/rDd7
Yv5WFdqRKi6+YdEbD5pKuFFwWlmCa7dmactsgCls2ttfvgC5ZtRIHuVaggIMVnMsSbFRXdgzMvW2
HU7iggcOfvk5ZknUJAfPDqAR2Fv+ZnCv4W6Jo51EV09/jT0czoACs6LMcsZQ3Kts8CxcejxkBfGL
0dqI+yhixX9Q4d6/X5Qhrr6L8mP1gealKeDfU9jSRy0gqKVtr8jmAsFrFUslga3HquvTud5kVHGb
Scz0ZARuvndPjGj3CjzK2PnFS0b2R3FfxGa25OMlZ8ZmeWjyri2bx39WsKUzKL1uFm2cVpajO4bu
U4sl0utvyzx/gOsE2rAoIoPT0gXyVxcpHLyOsRUynejlgsVr8XZ3FLvYwtH/8mx9XWHRUAHXkvOb
fV888IET8scRPV1cKzGuMLRoooJxdjPHAk9aAfNhVwpTG3Pd/iZ+Rn/8JeS+jDAA5GJSDY1dSloP
qGldYyDM8dN4GAa8u1Ul+mma0ujoucT+PLeH90omQtQE/w24V44r7uY9dR/+E4/z5BKriLYQcBYh
EIWo+vWydmiNhe32H1VXzNAvtHVqOkofUNp8wphDCwyETOfmc+XGFh/XBSDtam0MAFQNh2UXk+nt
HrviABOfT+5/gQgDcI5w3hTK7NmZt8ldjuT7LhGM+HPQQ1Q6q0SGYggF8gumifJKgDhNbJYqNtxg
Bx+GGOTixN+VVOgvJwZNYDlDgjr4xeM5Q0QnDhNwsXoeYC1ThlXy3GWKn8Llb9OLpF3ezDvl6ZFp
/eNf2fTEKZDVNkNps1MWNISpEKgmJ3eiw7soDLkuVVUpHaj5rTPwCp9PdDwzkISDgCFXPnyz1plA
fecBBbwT8ulKyMt2jkvB3f/j2EOtBHV31vQ5WNueOGgwpy8Jj2q/u3G/+kIYNF/lOBY0Q9LWuUp5
cZmKLfr25jMgd6gIRHB/mYyaS+66R1qy1nRAz929qA8WzEUfRj8qnF1C6GhelqcGhOemeZe7PwbA
8FXbBo1IMWUXeghJQ4yoieVddV0QOlq9csZy6wNfBvbRrCDwlnI0pBK1n0t7JCYUBpRkVuyNS56K
ZlVzf/XUncWcgxVMvoZhELiJ+8I2R2IGNou/jJKoqb86P4w+8+Qm6HmqZsbWhko18KTbWxWcZfZG
6OqVM03/ONh2+j/4BbUAAqHtOWdo7nRNiNUm8aso36WaMO2K8AjuB1432qqykHGnJdhWstm5xvSB
jhRo24dPOg7rtZu4jIu3k8haz5ySPYdcPTO6MGG6OhZtXFgrKC23ph0Ca/OOKwbuXmsdndWkQjCJ
OtTjj5PEyLLfx9tzy9scOtrVOTYt7tgJRdUZDomJeDxj9Mok1X/A/CDjTEzhIiqDDxERWtqQf2NZ
crMtCSvEZpHflCSA0h/ijWy8ULgGbQ+FzB+Nde582YcJ9fCMDIHlLT8u52MgWWyag6qDoEATEGeO
bKz3FmO7mm0bAvInA422R1iQrN4W3t8MOYMuMBfTp0c1h+EDYHOdSQdXfV10FbVdLUNwhQT2e51A
L3mYym+HcQHEVmuqvrTuNZYv6/TxcZ+5c3HFxdTArrt24IQHgXaE4uSlAH72KSA8lZIa+Jhp5eN5
flm8FBAQAPE0h2tsNN+VfG9zHGW0DnHhF4rtXkVyJTth40Zlr/PCYecJ2qqHYmru9cfHL6brcsa5
5nRzlw1EVwrDWYvkj6IsE0MN/yP40Ay0vbXPlxUaAklv6QiGDRHXqcztI7RxE5vOz11N0C8Kz/QY
oq/IA5+milklKqHhEuJQ8sfGaV82wtLhRlc62is6ovzzBT4ZQAeSgpo5uZ5jXJebR/ku3DvkibAv
LJoHZGv0kpRB5UyczaUb9SMjel2kTiYBkpksLGobP6jzbaLAiYozt0eMxewmq34AkoeI2uIyXY3N
gqMDITB3kl7TD/wwOlHyHgcRPz0wN7axQbCIAR4WI/3EzqeTl98gUB8lM1ST/Su4uLfldjngrE/j
0EM4ZCKruqNsF56iNem9iVT/LPqFccsfrVxN+FWeeh4Jm4montXZt7plMIl9nxdErpE7Z4OJmEQJ
eqnFzFVMc9a2XDKXNhAiK7DlgUQDQYYKePLm8IaXYtTAw3HfZLK3whKleNiFWNy8NDnYMFhxOB3/
FyBML/pvWcL61JqAIDYOF0sFUiWU11TRDgsBAQuR9t6b5fxAVLoMZ/SNqAe5v2L6WZR/LhjSSng2
CdyV91WH4+M9wq0OJW4mK+5GGExOb7tqVfGQruuN/n3kfLe6rR50T6DedI7mycrV/mjb8vA2rBLc
9MeJ8IvQ0sIRmYZwoVY6BgA908HMFbmC352P+OFutq6M4dUsfA3dPXbJacbqnOpku17NHLE8uRhe
oWatfJCMlrcXqdkktnjbUTlMgthSZ6xt7pOU3EUmiDEzrT10pD5YOtFmhN1cq8tZZ7tOtyPHJe5/
HyevknDTC3CBagHFaR5/KZnz+YbYNiau5LzNtL7LBwtE0/9YV/mL/1iUDxzxjc1Xcnai5MiiouKp
dGeYBJ0xIa4m+qM+c+8E/L0b0bmr6jKMhMxRmybRkXourqOFMQeTjRsQ1g+UDcMNCClYwgcL7cgF
ctyX8cvJhVoOQ6UTWMwYyc8/EVToG/De2zsHK4QYJVUcatJx7OsTbNPwlvYC3VLaSdEepJmI5Ymw
TZA0B1bgZVnmy+WKS+9sVJrS/bbsMuXFykfmhO6BVywNzcg+g0EdQL50GtopzbqUrggQxr96T2pM
JYMd3mO1ssZTZ8ijPq4jqqSkmm7WxMfRIzCl3bU7/56aoaP0f029UGYAVv+ZF+Olmlg51k0Lo9jh
WT2LAWjeWgHgHug4AGi2Ooloh69P4qxwJi4nT/ny+K3yBQ+hCTa2rBogh2ibm7u8vkK496mYlOLk
rvwlil6Ta/bNqi4j5OY4Z6amjtkpr4Hb/URhJ+9WHDRMCeb5y7J+ijImNM/8Drjv6hIzyZIrPEAC
7ZwAyGleWcImHxgTHmSoEn5BHTIr7jPLaH2+3mpGAQ6u71Di+gyjZCBYrX2PhPja46DU7G6kfuSm
hCfWY380GLV8gjd73Z0We/LaZ06egWjNqh1zB3c2/kY2OVb08iID6iS4AP909uFJRMx7JflzeCSS
k+6SkZxwnw1jgq5Q+J+0OvCmiJlenNZVyQb/OqoHaJ454XUjc21PXAQqpVe4JfoztUpQHY4NkDSc
rcCRSGAAprp94IwyL7CR0X0f5RLpmRUz/qBlT6BYxpiV9phI01roN8KXddad6Y5x3v0H/gTAbHB2
pGpcIp9V/T9Qtu6sQAh0Sj7wkG5HtMsso01Vuvq7PXxHKy13Ognsxu/DgxMnNsQcxBOa68ukhAUv
hJKJ5Dy1sOMYKhGLTMwvE7F3WJTxMIXeLCuBKu4v0BhMvAsYCxRS6Hrrgp9mgX0LYD419p6D9oRW
f2xt2eaW4W6Q6p9eI0+m9R97oahsC5WHIiH/qUjPTicCyjIaHrF2RPwzzUF3T9gxxIxXkdsnhnLG
8sK9GnZFCrf0h5fsU4kA8PGxsg7QCyNnR6eunhc8t0IIfeZYb/O24ZTaW/0cppdzypOwRlOo87IG
tGyvtP+a1WZbsQU4SHTMgKhn0iYhjTQ/FVBDiYOc7OTId07I7bgXVN1kaPe9eyzBwYgsDedsfTSE
bMSn/YjG4x0LbKluqeCZt1b0tQ0oegdNqgHTlrH3bQzZQNTrtVUAmq8bKpZ3kybfykK4D1+7Iivh
GRdHQiNsTF+8NZDrbLh7dFBJf4nPGLpfdMVp2ntdXrbFR8zGM4AJ5acB6r6UjAMti3K2gKOEmHiZ
l9r/Y2ALQRR5c42RMWutrGYwPHofNxDRMzuh7EkShUvke4KS3za4oUsUFVp07fmKs1BKscYaEkm0
I9ZkclIvRDE3u9DfzKVxO3IpNx2VuPXT/EK0/R8ts86a35bRyo4/BphB5FTT9yKvfcWFP0UAD2Mx
dz/GRfK0HC7FoS16Rl1TlbSNYsxhtE/y9rCWnbuGO8n2Deg/m9s4B4ucCOIWh0g3ViDt6j0u6pm6
Som6lljuliZS2cidj9yN4zLlYhW2WtzoYX7Ub/5RoXJvDw9mv9sF5zHPqVlQrVzkM3MFEhyiFTBK
vGkvzfLOXqFGa1OsbnmGQJPP+/C4nFD7WBHwOaOFYbLQ/g1mNQty7W/FC7+kH4aN6brlDH/Orb5B
Pp9+3w6NI/a1jfmJOLMD3szRXjCAIRkPJHmkll+6dPYNkTx6DN34vT/Gz/lisYxWw71tzeDXovtw
bK0XwMqzbX4W/KfRhKotxAGTAtj6/VpyLaj+qYg6h7e4xp7WpJZgfPsaUXTOV6dAC1V+HppEoe58
j/uvVS2QmRQLBjORQlVYm2Xl9YuqFSmYvk53RECBV/uzsaJbTZILZWHHF8RrcQ05iGcLDNqLfK2E
ky4Ln/wsxxSE5NCsalO7JZC9reXD7gSlIi8KIvzHqYV3JoMK+HqSwt5+YWiHGLDa3zzDmw8TUdw0
DS6rcPGMqSNovcZPECw8TN4VIThswd1C53TYyNcfemXQDPdZKqDCsfhZBuYnbpCkSkKChsgmxv+K
BRlqa3mS9H4ljmO/Af+M9Gu1wkR4DN+MAyB6+OI/rOf/eKi6L1Zb22tVB2OBonzWg09F4VTKd1rr
FV9xbAvNdCAAEjbIYLlmNDlkEg8saqzNRtZGmjliLk4DSufolPTR9U3U/SKRYMhJ/QlPnr+/8jeU
gkcxV033qISvkVAPN9xL+XHWiYsIlXEfaijadWFG32YlStv2RSGwM5HjydaMzeqcs94wcl9O+r2c
5xN3DW84WwCuxA2EJQY3XKkZ8FBy3lyIOdc0HOZZAiNbI+TmdqHOCLC6wiS6qSnBEdEo+oFKCihY
nilnpyN+glDJ3SBhVEf/dWKBlQqce+Vxoo64L85m27VdMsUpLuPRpnxePzEh7FtTzfYWtCBBkMWT
oNQYgNqtKr+gHSL/yb/WVzKgy9YZJ+9GgjY2UVyXeXkJBkwRaJXA4qWzpdk4lL6vHiWHbQOGujK4
RC738WMlHXiCP5B9qdrUvcguZ1HYDwiVGkCYb+QA6XSQHlgST+jG1AmCxWdfCJW0k3ZIWQNpcRcf
K9j7O71oIGZFwt92oJQAG3U4md/MmKtfHLwsCHGPN5+ot9AgdrrJ+usJMXAdXjwRfEEq5i0RwXVK
wiGkHwUAqGxHZ0gMWoWtLXLmlODGdUmJClnsEN9H31GwdET/I5otPn6CmbkdGZn7/9ZKMX7jCDgq
2uMZ9FA6Dw/oks473U6oMU8wWxOZvCN+63vDcPbO0u9mmdVLL/6JR3qI5qv4oo7968seynpB/Yqf
424Ku7JmwHlIizE7IAyVI1QcvFjCx9wc7cAx09QGnqTeZZ93Csr5+QZYX/u1zu1xwGkpuUrZeumf
hBi34u+lqCzrM/kGMwu9HmIxA+0pOoYzTPFcVrS8hrwMBNgT/gL4Sf5xiYm4xutBNdJIqz2rTpH7
0BuxU9uEGPz6f5u2L4BsjE2UFTqTPA23OJQG4FMTnl8gHIjAqdgBQWJT8Jf+Mif27F9LdOf7OEb5
ltaEvsgI7n8IhXZiFikaUmLktnvokJTtie2qB1nBaupAUaf4PC1sfbxWbN+E6zkKMnqlWaJMDwVt
2veT0lAdb7HQD9A8Uo5kmsX/5zUDD/L/Uzx81XwQoZWs5mduFhdSosgKXRYZSQJo+k7KljCDh1V0
oakNe7/5JUxjN37KHYWIwoJEcZcSaOLy1F/aZ4oJx28ksQnclX0JfxdG1OqG/EsCMkYCliYOL8Jg
bJjYkvCy5A010X1aJ97YG8KooUrjDuL1ncsUxxu9tLbR/GTcCnlMBhQtAu44Oj3y7mstCcifO9Nn
oXOJ2w6Op++7KzXJ0CEJhxIh082M4RtT9nWi75InDdLhxUWPxAtDQk/3QB285nEC9LIXZZyUz8l9
bncQduu1oKeMCs/DIJiUKnWt9POV0igNAkA/HogXaS+5E0QgpxRhm0Q6yehlAbRFxdGlz2vn8BQk
pvoVITC0LDIg50X4AfaZYw8SSfYhJxDxywf73X3Ra0WwG1i3DjJNad8G9IsQWHFjGxedsTj5tMCi
xelXJNRzr0hAsLGfvycxIdUpAKRVj0qlMmeEipmbRO0ih1J2nAFxyandNM6PHO8wW79OfE6T2UbR
110veeiGGFktoMfjEyBDliMxPxzK2Izw4ciNVvQI3PNopbaAe0GTRq7r8Y/bnKUoW9jgCrrYJl4A
qJ3cqU5QVrD07bypqIo1ytcHkAvD0eFLUyvolnbmjNipvyJJXlV/PW0PlpVgQQSoLz71DAIxFRA2
QubCrJG+isw8n/NoH8NBigq5hzDC7CcPI/FLuYrDZfAJoaHq2Mzzukkc42nreMNi+oRFg/2F80b5
48iUO9Dr3HvTjaJJaN7Rt5dYsW/Gm4u0s2n4Djv96U/1E3eo1ikd0yX0p92r+MfcqqD80B3dtn0Q
dSWIflvwvSJLK2Qgv/fING6pVKHHRmN75KzvbQ7VjD94z7Zq8Mo5WJUB0RHUi8L7WSqsqpsH8nKb
aUns8fmkmYtbVvnWtC04Qa5ah7yY8L3Qh2RaDRz3kzFG1TJCKjJt6BjLTMhHSivYonZEX+n52f0j
xj0i8JMObSbWy0+eipZ2ViTu+MES/rJBZ935B6c+MCraAY5OxadLJvQLfs4riImjKuDSvCzqjiL+
Aaygqju5i7c91qin32drejfc7Bq9Mk+ZGCRKa/+HQR0gTSKlD4VVMvXGuto7H/4DHhtd7fQnS2uN
Y5WXjZCm9/7IK1GBl2nVBn941ZS4qs77nOlzuIBJhOGgjbXmwGiPvf6jA9rdRd5Cob0X0KzuN/If
cPiu16XmPvy9q8yDnezrE24NXHy7hvlVFVMuPkyp2dBiFBjjxO1T6RH2ll55+9xr2lVOh4G4hsHJ
PIAXXvKqLYIWIYdCwCOZsG0iFLZ9RUCjxgV77gYA8ybdOJYCdFWmaG2R0dG5BJy9CXONYxiwbJR/
IawWyjZduKgFFAPsIjW7eA13kC3VZ6gwdsyy4mAhB1b0iJPOdNZfUSdFxDQI1r+byNahqNXG7FI2
EFE6uSuO+25lFQJz3UHL75KNsPPgUA7e+ebHMfHkhzC09b5AFQNZ/VjsrYrFHJ0IBULmk89IFQmj
m3WHpdKL73xs8sJ8ueizpz4F2prIKNIrtIthA/LMgHHrU3/EEprQIXVBUQGecvRjDCGPA2sKByav
vcjfBSYWwFqL4eVQYypzHBqdsN4I2W6UaQMhu/BencX2yzE9rR6NiZerUFdvsnn5te6VeDbogcWx
KwcIhUYe9r3ubdqua5PKuLZ+Ak2nVWM1W5La6niV257rAku++N+TgkXuLSyON2JfySJKMO+215mZ
oVrB/GUNqWdF2QuPP9o8kYrqpcWs/8PQ+HvgEJrMP1ROYF1CkMtZbvm3Fcz98QgLNAGxASAsGlk+
WHXloN+937NWIXcWs/8DF5qlIay/+rpXGN0SaN+i0/coVXbA+TkEZ6pa6eUUd3kcK/Ajc/aWiQGE
GEPGOaNJOEfdgfpxqTwvN8PNtrwTvWueT8Buwbs6RbCKgbPnvNZyq3QbbtMb7ivp94WeoumFbFLu
8GXTStXEFQzXpZ7g/6GGYnN67iSL63lrZ8UiR8n81b853SrAD6LzRnFbSOA4KR4WbXdVo1W7YGgN
yQkqmLrBxgLWyTIn+JlXLd3JZ8nhcGMLVqsRR+R/3SuC53GA8vmlTHu9grJSVAxZxk0tsoQeq8J+
S4NeV11xg0M/MarVFUHmHIwX6nCk2xW5ohVfC7D0Uvf1qLTif1Y9WJUNpwPivSubcnDWoOhvT2Vq
QRP97ZVqhaWnNbUb33LEJ7YEImtXFflRG0Ei0umeCH3Zv4ByjdXErLrqtmK+ja1zvhzjR4ppYrTI
fEMuaSsdls9prEB2y4CB7cg+t6Zpvv7AsGvXm3UcqpsAM320KVRWfYD7/A7awOAWCSJdCb7+bsYw
6AS4qywlUxb9vGAO6uimfmSLqwwVPnWJYCtYkDfa9kCw0KMr6RW9Dseg2wDl4Mx+Gp+UFPaRuvy9
9VIhlpiK03TxWlPX4lEjDEnHzdEwfEP17d5w77QMjVmRNBqzITOsvBDHsgFlEYB5DF6AsHe6LJ0l
eqXz1Jru9o5uPXYO1cZaRY33etMrks5RZgkbCETnUCkhM51dNHJiNgJ0DUNyKgNRhH6AHvEwFR2b
XG7UJ3nHhE46RjYj1DfotwWG6SQLU7IaWmvWcVyG6sCvNTb9mlhCyrQKbfyomm3ITQyUQuYTQZI8
Fh8KZ+RQItvY9qU0FkeszrOQsi+/MRfnOdUPDVUPK+33Rc+CHE+Vrd1AMuTn/tDe+dab8S16eaRo
U1Rs/ULdXNDVf/uph5FJyN/Lujo6tS4FcvlgxdcQBkS0ocAKZDrC7VfREx5dJQU2m80qbWdzrdfw
UG5/7wl8BhdI5F4EarmnxtopmE3Xjo2GOueoIxrdW9g/Uss1rbByorY7g4fDE2uhkpB98/hM/ZWk
IZ88IcPPICsqAZYNOu7sHt+FyCt+3/xEjDtSySLWudElfYEKBhHSJWH1jtdOB/IhSsUvmLgo0jTj
nv2UkcmQBdBrRMcDtdDQaOI+tXGQy0abSqqM/m/6yxPTGx/y3cqFq8fZ2mp2nuf4a8JIs26xfK7r
L5VF1esNHgQyZe3w7pY9mENRZ5RvSZoQzGYcFz6VG/FJ2UWklKIAWUAW8pl7UQ/cd+toMPJmBpEi
dZfAIqynwokl9pjrm3SY1g1/a90JteFCN2MrJCC4iNp9GFQAS0KJRXy+a9dJ+n0zlVKGklodCW4Q
ICthbAYj2JnFXcxF98bEOzoZ7yxfFOfqF4lmvKDwWIf65A0aHa7z27pofEzu3IdmnYhXyjKRpOTz
KDKKVePut1atF0iTDKRgtVpnRc/hjQzoD5BGz1xBfryutu4pj2MJDiit1T4QXLrujqKTKowv0DxU
z/8mRQEP3IX2ntqqo2Ako7/RfbguEW7trlwZ6Raiici8jJEiiVVPl7lJx53u5r0vq9RBBcjO6Dp5
2nXTuMPJhNKqZSczQRnNW44XZTy9ixxtsUw1cOJRyVk/+CSM17ARBi/dQIxAYqonNFn2OxV8Ci4t
GzbjZ5JoVI/Frp/ZgEojeTrV3BTvpN8AfuHykgdNrXgv+DO4zka42y61goFVf/gJzanBR25up4f1
PuKW/mkAJomEi0N50qe9bEHQc/OctjqotEW4+0fYTgrkKhsi8iDOS+L5ZkvlrsqfOxrQQck5TgUq
cKrGFLzcPUn6vMLGJpjNKyGqb03qwXFFEmOKHOQQRCRgMYRMEZRKSCOaMBODXfZlvmQ0C74pd+hg
UPsfLgrVUSTbopy2GveoYlpdQH3jHr/SzRgOZENDPCg+icJTZq4E4VRWTA8MDrZo7wbpTpXm9D5y
q1zRkaK1rf+BoAQJF2rMjlnRayON6+Qj4/goUPjg/jFeNrbtOLcX+UhrMXvdqYWgqVKmDdCAZJON
w8n4F+DNMQwBuIa1v4BYjM3ZfQDcMNxZAKn4NN+KVsdeXEwLek6PG0M3tN40ebgF+nvvRA731Y7U
GipZx/azX1C0giFZCuOwo4Yw1Wi8KMWgzCcT2dW6k8QNBkvfDAkSfCH0zNBNVH5J6QS6oqfprKRg
amAA8gWC3SYNSquytBdynCMPgMF1f2bLXoFtvcC78/+LkNSFsc/ViCNv3ejBaBesIVZAcejqZk/G
tR+koxxtGQRbZbo7WZ+jwxgPneqDj/LpcYyE25ODVMnT4p2HZuh31DIiDm8S73U8CvITacyaVgEk
ExPlaYx+AcVzuGdR1O9QOqCbfRefP8E9tdveHASOM7yGuwjvHKWO/hjyb+5TY6+Oz84LUxxHI+kI
Z8t+ryonhll63aKY3qi0KFbgdnaKpLW999ieit4tCy/AT2z5HVzvRFNSFpKdWJqOLY2PIPmdDfBc
QWpchMxVF5QM6XuBcX5hMW0AZ9LQXd49WhYLyBaR5jxP4CATFIEJkjcij1iZ+vkJPJK60h6oJncl
BQ1wp7aN+m/yYXUrQzlEMacsCo41JP5vo3Rj4ke6mXJsiVhhTv3gk+8I2FzlhtgabKqWmlrE148n
8H/xFzGpABar04UImnbzyn9dKEGIr3q5zq6cALE2k6WdhLRi7oYKXq1HQi6wNYaRnoC9IiB1Hqqz
2GNxU+KXNltUrjNr3zjGw4ZQZHqx7+MpRXeSfNjzDkw6+Is72EJUkksy+1aKpJ17k4hbMRha3+JM
Lg96tCVSjxHE2/7qsF2Juo+/SErOCRB+28FThNdnv+lj0sHPi8jlhdVpRW06vVyKFa7LbLglQrl0
g2E4aBZLUdMYZrCL1f0YxuS/gQ15mudO3V0UmOipvCj6BbjeGknfVCtwUByfl73Hc6JDxou61MR5
6sMGC5dSha3+E3XOdX8QOhgr0z6tGdRxTxUp5Z/dbMbQRKQPva7rLsC3NtcFZYLejyJYCQJGGGF3
rzPi8SJnBauWkKciALMJYowhmDRlnr3lWJ5GpgkY3vqcRwLAdb4zXo2QeqTKwd/x7vzYntfSSrWb
rDCfJvbYxGcfWp3bAnjAv97EKHaF69TyIEUwatuYhSp1t5BTHAKH24y5vnx+sK0iDV8i5QWGydPK
NgCLH9HssYSHjia4/kALSA0FZJTAwYMbDWL7D+/F5p5nXau8kgwc9pZiwrYxQPun09VPPz+5MAFr
Ue/aWzoheLVVV3Emkm4tSA3DyeL/wcRY6BKTypiWp8et7OmrsXDG7hJhiFAo7qjI0zFOrv3QInOT
mfDjBG3WYLroPCpajw10sLufY3nrTJ4EID9EvRMS66I0kGRnDU7GwIt+Uuqb3laNTvqxquiDZds2
xmAwp85EfPVymTp3w/wH2eaOD4Vbxk7febiZIVDnymYhSszAWnegZLzz23nO/8CABcV+/bpVy9xq
6Odzpym152LiQgJw7ke3IA5VK3ppCk/jp2ryhoKyF5g/ly5Rf9ByJQO2rjiAm7PJRIfAUmjXE6k/
Crd1iGH6tS2EneAPT6a8RSYI3sMIULlfKejl9vkNqNuy5ai7VuZAvedI9TBqOVsxw6q/DFFRJyRk
e7i1C5kfPnHbceaOCZg3evb5K+ntPOYkwPXY+EUh5iyRaWLWr4xijVLxpW1i7nsf0cyCQqqWv6B0
V7+BrwJpiDCqaeE60IMONgDKER1sPKQhwP3Md/xGB5SgLKSFenR8Xd05tjDwjfFI9L6O0tE2aEIh
RQ8myLCwJsBsMf2t0/5fLbhBnCeq2fjdaKZFpRtvznv7vH1kiCsPGI/iUMetsi5GrH2lAib7PAyP
DwGf3/7rza8XeRoCpxinWjcswm9qJkldDiPkTvZ5AFYC6T+STq1G4Y6APbgQS7WCjRxb3O1HV4DN
a3f7meUdrpKtM33HtMK2YUPu5b446Gkm2QzYrUEo6ioOcDIwpfdBuN4LBiyHLKZHYGasMiF+Tl9a
yHW8QAPt+lwFLvw4Gb7oxbk+lSmaG78p/pLdZxyOMLhN02N5yEu0UtzyLA8bsN6CLSI6sQ7Gjq7R
P9fHSl8vlJdV9RABiRar6BLe9+1XtNPBzSVJ57iaPXL09ORdMkISuaydKTEnxW80vMh0CVMV7RYr
lIkb85DoNtbsJPO5RRVxGO3eWGM1s3oRCvBfcUny+SfQPHzGlXbV0p1JyhjYuxwCzhQDWRMrJguS
fUl0hW/D9/s95SH5RJx0JAnSAp22dVBEEnVYf8+ZTu1Td43MQl+K6HO3YfG8EW1w7FZRDiN1Y7T/
1SiY1Bh4YfViMQtd9rMAbLeD3YdrRcx3S7wX722D8du8jFSE5xz7hJeQ/NVl8sZ8zYnhVR53YYXo
uSrMHuYYkYg6sr0f7NZGlnvOG3UHiAjCXmLWVH9+TofhOqykIji+Am68Ax9Lo37L8NFn3OijsQle
XrpPMVcDpy9IDTqtV5O2TdH62RJ+nRJ+oH0Kp9A6ZrlHt3TgCm/fHKzdlNyaJJ4fP5EdZ8xZBVfM
bpJmzvl3H9TriJQlqupUHlvWCmsZieVOBdMlneNBDUYOCcU4YmQWZSRy1/K6F7Q0OQoi+3SKiTAA
nYIuvB/ziL/Qk2ETf4QkHJ3HxeZg4TF9A63UXE6V45YrYLcmnAb/ERe7H82keyV47XZymewoXCtN
Tx/unbvQqXxlUAYEWpwq6nDpm83aICp9Be1npGd9RqUZ4j+3FPV5KWxkYh5GjMtrtGwcM+sAaxkj
misoUSm4o/l2IBRyNQabdZOHrem+GWRhwQRIgNZZBxoFEKt1S1B5oVq5102acm5n0TQW8+CL2JAx
avAgrKSFoCU1qft4n3vwHDuG2cLYAMc+d0eGhxQ5D9dhimF5AAGtW/3SJ3xwwClboC819YP043cn
4NE7bNLH2L/GY3VL9eZrnEaBYgbvT610eoO318cjFOcfT+Ns1B3KyES+YVC/McptKoncYKB304lA
XqiexDJI39askOZAjjxvDAv2oozJjCSQt8jISYYgpSLams8C7pV3iKcWMfrW5oudtfF/Bd4LT2ry
DPV4OgokTrdc5bbthZgBQf3pEVJc+znAnwqmXFNHz8egFozPSqp+3Hb0IAZ+0Wqw+rdTFp0uXG2f
IpPm7IB8RPrMYudHws4xEzuF4ztIqQI8mlJJ5l+EFO/XQ1RmbLIdTNoEqm2fvABNIvONPWVvQWf0
1eNyM4XSQibPMbupZmYqKH8jGVFfIwJZFmWrVN18NhRaFDB83i+CVMyFDxpEjlJJNtzBYRLlj7db
A+sY8SKZOEjmDbrclOCI7JlZmAEAPVXn08DBcSA54fVSjmkHjlt1yaaaiB8JSCMf295m5VtZUtyR
pQcUNNO//aUQkgbFnHYuCzdGem0ipjFoLPFk+qRj1f6SollXNCs4L1eY2hW8Qzj9HbpUFxBS0ysy
3xbdrx+SfttYfGfAMdffvTyrXPX+U31/hgA0pJuDIy5Ib5ZkGNe5yFPxrsEjbLtX9RfcHZcIigEN
knDsmuh0Ur32UTdEcOiYVlcW8dgDGnB0R+VHnSpm+Hj48o3ix+1/MM4FiVBCBVIJEm6H09CyS1Fw
3usTle4+3irDD9+FI5VfaH4I/HxQuFtt39tFnidiO9quAe9oXQ6H/oUSgD6ixs5aA0iUBIMtD4OT
a7FUr9xcndJ30qcF7MtI1b1vdBLOhhkHiQlR3Z0D2xUAwP4plPSkMDtWaxcsOJ6zXats+7RMsbnA
sOrhYDx+VGSwWoVjNQRJUScuCy64mJ39esVdZVw3pCEFSbLmx9QmPNgBoyLai5+Asdqx+qd501vL
oUUChZjC1+0okAnDJbwLFUrpq2zwE/g74/NgzU6ukhCAjXpwB9KMpe40mav8UBs3j/BZi/iywmu/
ArG/jobdlkwJI0fVl65OhAaA/ErCLvR0f5ce/3cPz8xFmZN3MhM59F4Fohs7VVKSbSdunvCWTwkz
z5KbPoAfzMeDZGXXTBWKPdNTUDiFnhBRdD8TXKcrCbivdiUwdJvpkbMtjA5cbpkDCL89J3jFjKy/
M63c75WMpGEUAJhvLvDkCFksDXgDeTSbPseiggENYwjoivK+vZC0RURKEYWF4TTzwRADVQz8SCXT
1L8JaaU83ansFrAkHLvzEREhqyC2Wsyi9ayxsgh6+AMhBHijrrLRHfCusXgp+rtrcu0XnS2i7yVK
XuzMxalF2XohMxYWteRuvf/SrK5NGypWqJn0F1wt5r5Rml6vLQTciTeCc8fwuMMxdP8RYdgQ252v
d5k1lx+Qg/iqvRe9dIQFUGQ7zhePjgHwMsHGeKFNJXdNFAXakQZ+/gK4jHL2Q+syVtc7S7UCZnu+
Ysn9TwTI5O6ylHZiw1eQb5I5xX+SH4uFl7ee6mOX77BbEi25IcaCTcwNTcJiHZ8TpZ836KqaPp8I
RrZTfPX2aK6OXpxtrpWWJPu4XgOzaEcM8lN9piE94ueF3q7Jhup5KNMLIKOZTcO+o5Lsp5YLVBgf
a4BrRMGQWWbCX1yK3mUowIhd5gGZflll3Yq/ENNJopQGUlM4E77QkfXfXuj4yz9xSVQtyGIhy1I6
QT5yqi9HCOCt7p+qNv1arW79AuNPGdE/hghRDFNHFpzjTkt0P6tGlkrT19b0CazsXgkbCaW/e/dK
WbPxibr0upiHVm5q71gCQz55LbYHLc/igknShhx46ysHQk5j14AG4hFbTNwUiHNanDuT6iC5KnFJ
6LTeRH1+6cRj/UTjAwHgFAy1Y1pMtDa7pC3W/vO4u/hvWhvoZVtnaxs2wX9grlI/2iJuf69eLpVy
YXHPs4TxvLR8h3NH/bzXWwNaQxnOsIX9dVZSNrMN3jHlwk95PZeRZEoX+Lk7XivilSnEEjCLd53x
0RYakyvt1IW+/BF2g7d3Kabh+AGkvQBojILEy4HqPmMIrPg4tODjXgkGm2Lxk6bka4D+05RtWM/h
K7dYSDqqwSNzdCGnYsg1hv2OLwyZRnL7Rgw42gcmKkZSGZkl87y3bFwHXISyXuc7BVrjrHgv44QE
Wxhl5HtGhdzJup0z6Jgqp6GiZcAhHkKs++onyFhBMMF69KB4gCOCA6a3DcPOnbGcwzizlEEjifKj
vr15uLoHBJlKjYyvyDDK/bPMDG1XuYcfGxp2fHxzf79rZmmlHuf/ksbDtVQNcEC72nEOTWIWYYgW
xKNWQfG4hY3R5IBe4LFjHE2flNJMs+JMtW19g3vrnBv0++Jue7XwQOVyemADm+SEcsC28y08LDTl
geWm3419VMbovk0I2+mCjYlrm9OWlnaHEFkShNxYmMU+NcrMJxtNMklCX7nbjAWyWYC3Hns45fEv
lqfGvXb+BLKo3bqAo0wtxwGrUHM8Ic4f53k5otkILfyvMV85k6bGbTE7KgtpeTFsX1RooNk7bxoR
KMxXNK8tL5epC0HdKClAWmdAYW4a2NCCtr8Yg4oXalCZqQhIBI8yAV81Z6J6Pf1V/DZCfA55NTXx
oTF+7NXRPZZRu0pscjOKqnwcICZiy28C7pWBdwlbf4mqtj9KOgC3RGkxK1AS7eoIlylyrDEljiXK
kDTnPyHoMQS/lmqkhBi26VHLcfc2DVtvmI2o8EEHaQUe5UrvWvL0QbjJfa8uPEPFprC7zg5g5ikw
D7ELtwhHose7itZUs+fffCfxv3PcxSEmlYn0XvqMszHSxRh9VfMy8zP6/t1VA8pMHLSIDX7iGEy6
a0bf1+gD+w6nufmgM0sjAQQgBM+v9rtgoh6zdRGkdjY=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
