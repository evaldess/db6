// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Sat Apr  3 01:45:55 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.runs/counter_binary_32bit_synth_1/counter_binary_32bit_sim_netlist.v
// Design      : counter_binary_32bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_32bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module counter_binary_32bit
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [31:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]L;
  wire LOAD;
  wire [31:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  counter_binary_32bit_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
AAg+bGi4G9lXP6TONsjYLxy2DWveMhOF2RHILVKOw0d7Wzh3xCYrwlyD8t6LIpg0AZkn3DGn/d/6
zqnIdM7RW18l1xxDLkrCi/0/G3zJs9Tpo0FyKoaqx45liJ3z6PfWt9AkwctXYHR3eo/2oq60ae20
2SB8VDezcR2CTHTO6iIHxLOzP9M6f7WF8rxjywAcAZKvmIWsknVERdEC9kVHvxssM2MIwwS4xWGz
Er1PGdISRsr6rrlZxaIX0pSLQK+jQzXGDmdcmjGXKPVLLkRra8mJQtfQG3LSnfYHbMoUCSYu8ysF
CSIvMPl3J4o9KydrwUUA2h+plRXd57D2VyntpJSJ8/KIA0DqufcSbZR8VufdwkBnZXAn4tfLycPz
NzPnajqZaIygmfcIIWOpcSv3egjb7r0F4W9sXX+CnujRLDUbzNBkFlTZeOteIHGPpHrZ2Z2kPugt
ZH6KdsL893959B3VzB0M6LGOZ8Ln2z6GOKDyM1o3wv1p61H/Y9PV77ssgE7tx6TO/5He2p8RKZW5
lv5gwnKSTcuMv203Uy5x+mGKrRmF0Zas7vd3vdadWt99KRG5UopQm0AYNHfKWB8O5aJcyngUgeYP
19DBRrUVF/v3aGBO1vKDSqqiVh8PNK3V8iiI5Qq3n5MW5Ly8JeHgFpRQLbfwChpo6mR6JQsoni92
tJ4gsG+j6ovu3ZszayUS7HvQ2tYc5KTaFgc6XWVzAKTwkKLKvrEDT6pEB328/2L8SC0Uz87O3yrq
kF24DqtEre2PHiSRm4luXeKpjgFb4cAPs4RaJlSlqZ7lOS6Tc8Pab+GQmZBrGcnYbCfe1ZW3/Jw7
VJuZklU9TfDWIRzl3TMeRv4L9Km7YES8ePSvqqR9OXy/PiiBTj28UelSes9Xx1CQyP/X/nD/67lj
e/lSWIonXQxITV2F9Q9qFF+pjJHT9sjWnAJR32D0oOzeeIQiX3ePjGuL9Lyi/2h9r2APRKl2s55T
orzi1tjXSkXmr/Pm98l67oXvOlE+6/lthxMOapb61fGUb/7k/9RBrSxS37DlW4GCjQYnv7msIhdQ
vbLYzripYbGayBxC4L0DofA582jDsMOShrK2VwQE06UsfR+rnT+bq0jGYKcIQy1uFPgcY35zdsPG
2/2tHtvNhBNwdhfnTD88GBNLGUiNAXc8X5N0lQvW/RochG5oXKGXLEnYYJOSIpxkRTX78Lfpdots
7RG8nD1rnnU2TVRRaDLGvL0WfMWl7+I6v/RoPlhwKuqGtk0IjZad+3rGmck2B/kGGVIdKUFuckNB
TQUtqtAkqMB4TLl3kpI8trtREAkwVjSkGZGVq/szvHr5I0VzOuCYyA6I07bEKxN75S1TnPXYnBNp
VZC7UhxerPfQxTVSSulJpakre4NKKg/xiD6RC/A8Qi7ynB2bNrw2Xo0EQfKQFRHXSiBLXNh+eaeG
hmgthPH5AJvA49qA404bTBRxGd2QHBUTLHvDuFlZEQEvbpYuGAcbGMsWuYbHEIDP1d7paiNQRlq/
RVH+yOLozJyDHDI/1aJaQoVwpkJeChjHvcYfbgW3VZNHCovXj8guyZF2J1ufYYB3A2JFB/g7NGuy
l4y3CJT0LsvGxFLTyn/6j4vUtJ5mciBQKLhFz2bKsWgxktkZtvIa0I1y7a4po37Hc/pRz66nxU9q
74vEwR8rL0HwetrEa+Q531VStPj9RKWdYsQ3HvPXSUG0ZtpGQ9Hq9C+h/PCYUHJWa0+pPOTZUo6L
HWWfE3Tnj69f057VEVgjGny0de2zC4iTzTd60TaJxpCU7BYAkrDWoAW/4bY4MILGz8/y2H2V48qC
/2pCn8rqfXHvYuy+lT/JWOw1Nw06mMqxqJssg1mGSAxzZIYTpkVjhI5zSUDWyFWDSq+JMsXZiTYF
ltO/Tun+OgqtluBrKJGWsXnAKPEpvo86K0PZPtKRq5MpABecMv64iF5HGUE7J9kjHS4Y+y1Tloc6
qz4Q35U3dw0tBi4jdbbN12uZPig8vyiHDt5w7dDMYG0H6UbS6MSvMnoDQuUoC9waKCpqUC4h1K0t
TBNJQ/GuVbLQPzEyEImvi61UkW5CAX75V/IN3UkfciO99h6xJX/65M80+GEG04kFv/4v4evyFGw/
enHv6hlQ9RiBIVxTEF1vyEmpscomD1pqosb7lSyYim00OH5qGyFgo+5bBgaDMAcvAf4MtbOERwUQ
CH0jXuVzNFRnP97nAuJshhhuLE84aTgMChd0Kj3rXjEx9l/o466Z4MzYSMoOd+zFMYHx1zBEcKgz
xMNse6WCr3j7waQkttvP5JfobrEkKEan39i5cWPGmPfIDJyP21mwhKuhvAfWek6EtnFEk5zZOWQs
PXbwVyPOgw2DRKiXvYlGtIs+oPMVb+fI8ZMu+6wLwmPak0IAHqdxqT7SL8fQZFXUsLUPq0Iiu+/O
XGjwT6WvkNdmX4XdUaE/iAMgAlYqMt/naBSZPBQyewWjccG2fkm22ZhNcUrO9srX2b4E/y5fWujH
f97SqzWHG1GjCbgRZjbzM/iu91+ri8uN/bHtoYgyYyTtSArxwXMxc9XJsqdG75oZcNA4yVXpI55z
ZXCawOIoLjsAlSlnyJS61kITctZ6OQehJiryxjXQQORiu4tY1iGy0sDN5je5eK5JCZwGVoLkMUxv
JRA4+UJYLPdWv2yIzbtvNCAkmKuRJqeBt0l3g3uqlesiRXXh0TkK1bLtQlv8MLIkZoEx4eTKkCkL
DIn/lIeC4iMDAT6iidhzqeC8oMTc86gfOKIdNqwu8nelRVy07oNjm2NQsKFMk9rpAkNN1pP7/w6V
aACELi2IojO7y8WMkmBM4f9zHAAxiNxxewJi8J7JDMMWgvR/+RnQ3ZrPv4a6BGQWF0qx7IjONHFG
xKS1CtQo95jDtkdSxq7fYIt0FSRG97sBF/mhLUIRNCnRCK6ma3XMJCp98fXjBmUrgKd6nXJSuHI+
XFZKYglNDKCjZub4CW3u9pn49wLwcbGEGtQEO3s9ufSUH6Yw/rzn8O7FEDAQPoNcn/uPolu+UiWy
EcwvQ51VztXLyOWOZfIlMxFtUihqhvCaZuOJSfU27hL71WXxEyHcRKQd1o+V246BNbFbSgFFwUy3
dj9dSmL14ZNQJJkAjaVX/qh7bhL/NitnGQKOkIHKRg==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lUnH+IzZZPrHu4zkFktem5n+56tL7/tOE7RKusQKGbGjb/Uhd2R13A+vm18nYyRqeTEGK21NvusN
GgD0aUT5RxjY1eQQNuEYAKl73KA5CNbeSDN0ITVO9vYfFP63ZPqNGESQaBvOjJRG3abzAp4uM7Lg
Cs5o54OCs2+1ZQv1iAj64JqFb9PFkHHkwTGRP8NzKtxEqd6hFpdnRT3QhC904vJtADPPSwxde04q
Odvm/S1NVZkI+o0bLjCupyh56dSuhWoiBzhSSFD3F3hMDZpAO9rbCJjPYkbuAo+kFeRIjs4U0lhl
0CDsZoB5FjZ/iOtOtUt69tkI/R79eb1a+UiKJw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Q9bJ+c0Qp0UMIP/Hjn48cMqnIgie8S4LJaUq6lEcV+30zQRY/hW7/KmFFAOOGL8/yV8bJe1D/OIM
l2ihHfgbCx+7OWSrUG0xx/Olh5MzpTK3CViGAvFjZTRl5voRXEs6dEKJUQ+k0SZtV6zQMv7QsZsn
ZFKC+t5Gvg2GRWz8lecAWBv9djPhhEu5p/cW2zs7RYSdl1SHLTcYgGy7RXVL0AdcfSHWQFnJJwyV
/oFZHgN6rMYIEd9w6dIkB4imjMItb4pShMULrFaEqsibpodp20N25oI6ycT5iaSc70P8rRuPZEIw
74UyiA/khqT3vkBwhx03FlS2pCRddqCuJz4MBQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11408)
`pragma protect data_block
AAg+bGi4G9lXP6TONsjYLxy2DWveMhOF2RHILVKOw0d7Wzh3xCYrwlyD8t6LIpg0AZkn3DGn/d/6
zqnIdM7RW18l1xxDLkrCi/0/G3zJs9Tpo0FyKoaqx45liJ3z6PfWt9AkwctXYHR3eo/2oq60ae20
2SB8VDezcR2CTHTO6iIHxLOzP9M6f7WF8rxjywAcAZKvmIWsknVERdEC9kVHvxssM2MIwwS4xWGz
Er1PGdISRsr6rrlZxaIX0pSLQK+jQzXGDmdcmjGXKPVLLkRra8mJQtfQG3LSnfYHbMoUCSYu8ysF
CSIvMPl3J4o9KydrwUUA2h+plRXd57D2VyntpJSJ8/KIA0DqufcSbZR8VufdwkBnZXAn4tfLycPz
NzPnajqZaIygmfcIIWOpcSv3egjb7r0F4W9sXX+CnujRLDUbzNBkFlTZeOteIHGPpHrZ2Z2kPugt
ZH6KdsL893959B3VzB0M6LGOZ8Ln2z6GOKDyM1o3wv1p61H/Y9PV77ssgE7tx6TO/5He2p8RKZW5
lv5gwnKSTcuMv203Uy5x+mGKrRmF0Zas7vd3vdadWt99KRG5UopQm0AYNHfKWB8O5aJcyngUgeYP
19DBRrUVF/v3aGBO1vKDSqqiVh8PNK3V8iiI5Qq3n5MW5Ly8JeHgFpRQLbfwChpo6mR6JQsoni92
tJ4gsG+j6ovu3ZszayUS7HvQ2tYc5KTaFgc6XWVzAKTwkKLKvrEDT6pEB328/2L8SC0Uz87O3yrq
kF24DqtEre2PHiSRm4luXeKpjgFb4cAPs4RaJlSlqZ7lOS6Tc8Pab+GQmZBrGcnYbCfe1ZW3s7Xb
eZ9FSbuK4V9oEOv+w3bGwFzw46yVv4tgedWVNbOVigttOPQO2+aYg5GE5oNNkHlOjqV7dm6EWo+k
Fn1eZXNxWnM9ZiBhkO7uOQFoPiWAR6Cdga3na6JKuCDFO68ZYIHXw33Pq6+lTdfjvDyLNXilqc3U
opUS/rvIBx3bemvaUi2FahvK9UVBAlvoVpRkG8RCDz4/Ux7hkKomD7/Sfe8Jby2iR29OQAkgGCBK
8o3LrdBfjZ00u4sMP16ps0X24M9i/x8ifdi1/mjB8nYcR8U8XEXPt17R07djfG17ja+geRJmEL47
2bPUYbQDfVCMXYsTk/GInNYwxzs3mHv2qrk/Os8K5HuZVlCB0fOjmRCsMy+hA8o8BJZ8cg3+tRFq
k6hkBfFRZJxY6d1l4rI1Fm9Xuf45g59RdNSDvVIHTCM0C5J2wjmDaR4PHRvyzF9pKC9S9qEeZwKq
QSmfab34llzBqVkxmduahRPQIwX91znS9DRXB4CTOdvlrNpvj9ZFXZf/eawfGiPpKMB3CpBlD0Zo
FrvqLu5g6mu89tjWfCB5VqddJgAK2TH+sQtJWmbA5PfVgCYjSTgoiRH1S344o+T6j09fRsTXaJjh
5gcFvTeGDOwy3N7eAhDuQ/ugN13w09ph3NPiOcmI2a6lq1QyVkA0ucDuctie6Bpxh7PHW8pveGpl
6Bi7rOlN9U7nbC5N9B18Is/o9E4U/qgf2tcbVxp2I0/iENyebbt0EvBTU4qU0aeKJzU70MlJUpND
Pv8/O3ogW7/cwbVO21hSnMU0jIX6XjDC58zVdtCJYstbE6rd937FR+JTRoxVUzyJ/Rdfa+4ESds5
oDLqh8uwrF7UpbnDiyO433cSsiu304VnPEp3WybdYW2xV6GwcrPYRBF8saPDSColc+VPsoCQOGTI
oesDy3yciVJ3LEk2OwqTN42X/aCAEDBdlAVdRFRX0kIeFechQFCfGpvkbiiyLutz09iMGgBdgKuG
xwmxJxvwURXFQJ9lLUVMVCuziHaBcOHzoIjfBbowiBRPlNg6sh1N0mRHAlf4dNYfJI8C05SuQfXJ
vhixmH4nPU0kbhRtE+1ZGmhz4bTXd/bLpYRNDacrVTfVUVOiHuG2AZ1v8VgWUM57gpVrZivR0nJ1
i8yo6TIjPcpa0s1lfsfk3VjYqY0OuSqZIr7nQl0LyJB5cHysKJjmSOxPy9RFyHzdjqQRSq/quElz
ArIGlZ8EBii5yxnY6s6s2rBRjwpkkH1P1W4x9hucmKebjyPMRFa8NMUyKOfu3xWxA902ggLev75w
wBBfAts9SuZF/fsYziu+16EaIOq7JC1MSZLrolc7UEDg6gesMFwU003zq276s6ud5LTkPyqc0mGa
fmRBaxvczyHSuhD22qjbJEBS1B2igPKcbTWWBGktB/3Zbc424TEiZkFnh2p35BMt4L7q/YkHuyAp
W3wDtt24ROePYVSfgO9sqw8ddsg1+OmNHYdOu0pX3hwuwrtRAmg/w2lTyujqNtzqR1aoeNiV8fhb
TA3EfrzRv5MEk/QYZTxT4mmRWte+42AvFlOM/Mu982uRzVag+L4TIUZo7BcV0DKEp890Hwda+lWf
LQimh6KYYMtPQeevWumd/7uZaCKcXtOyq569eNvBxS9NgOuH/Eh/+PVRNghDNY5lZMvIWcKNZznn
GelHsMGN7RRpwW5RAr+tlC7bk8/rWESSZaeuHj4gTcuOXnM6B8Hnv597tT1DjcpEflQo93FZVlOl
vgga8bN0yO2bZMPDcklNGrKtgkjFwtV+lUSV+me8nSQaEuEF0eJPmLkeUsN+9ZW3wg5AypfnynNB
5qe4xO2BofewBTvPWnrLi+B9FpZgz8DwkVAFM1UQqGrZaVYlh774J/NQLPPnNwMF883TRTvkzTzu
BxeXun/aGP3RcRzJ7L/4PgMxCoF22h/IEkeo9a83w679XR2hGkgeCIV4JYQKE5Yp/KkZFJ5TpqME
EhT2pSaI5x4SkrV5MV4ytFUFrv/sNQeRTVazMXt7hB/EANZELI07wcYClWKAHnpS0cdg6NM6BKoJ
i39aOnU76ed8JHlc2A2lnqyObVfwDX+61xY0vC8uNBLuauumcaTeTe+G1Z1JrBcmD0Rg3BMEjwVR
6ra737IO90iK7DzcOBIA5BevzQ6mBkEwFNeMu4cfFwWKUWfdObJtmtoI5YpFaFUxLX/hjAq3R6sp
pucaX8uyBc3dISFEY6F5Qrlqtt57jJ5j1ckWwh5yplK7rM3iBBk3BdXPrU/Lq8VqcZosacdFSL48
RKBw7kSCMcUJXOlHVv8635PH25i30ataN2UMFSwTky2kXOKZc0cNkxmKwwutUHquMf8ax9Da/soO
Zqd1015TbiBOeskiPNgCWdBx8Rmu04HdFcyjN2EH+ZFSDbDulOtaykcOqaUCe6HOPhsJtj7IDmJR
/CLoOGri/uAOrOC0FX+AyBsFypZ3jo4Pvd/QBXI+aSqDfkMGGz9iuDCyfpC+u8AKGat25qzjO/rW
qLc7I3eJYkMFUt1BWY1akh6LVquI4cLwkS4xu115ZK0v6CMKjmAmv/4j+c1Iym/XRjIQiZnW0yma
UtXjWprZEaycIFWO6zEWd0gbzUp5JTQiiZdyCDlET+X3C0SzPLk8/1kh5lo5k4ntI6+qjlTbTnUb
HqRKWH1qP58jVc2/MEphHeWLpsA0GIyBMeeX0vXkqVkj4MBEENWbWlgvFGfYa122TK3/lyoLae2N
cF+DA/9utJvYvrM2eQG0S8LDP9pX1/oj8dIp62YtLLUVKvm1mk6w1clc3ogQLmnG1CePf0dobYd+
sZSr0AmwwYUTSKCMge/+1ZRq7GGFTIRHxY4/9+Pt7Wm9lypxKO9k3czKgvbyJrs4sWgwn8hOb2gS
pXNjH4TgngqQxPe0EuXgxjuToYdygHGi2WLAi9yYp3Fx/hcMN2t9U4hSau885YjYEwi/O+V2+vpj
6b7eBHb809Q4+du+7eQ8UcUmQarsU7KmaTz1b30WsZjpe3wGPUe/w4ODJ9MkzLIL8pc1QOv7au8J
boD76BrlDm4p1lnNUL+esuy1kObRGVQUKTT8R58L6TipYbiZAGgUGEXaWl/jOTcNgWpOcSlkCEu+
wKlD3mSwM0ldC1iSuDWZUVSFV4Kx2DW7pOGK+x27eb+uZLrAdnhRJcl5OHawy9IyWXx91dGaScGI
J0TK7hfVYXQCBTupax4WE9n6rrcIjR9yuLq6y/hDwEYTT9L/W3dKuGG7mJtDIFAl8tiX4Mr3rCMt
9JVNT4nBV/8UIQD0obSGMFUjwbs/K0oMcX6pvQBZROqYTpM8yWQNsaeV9U0IDJkplN4ROhU9LL5M
eWoVPVVqZepDrx5wWRrMPOGvZgVCMJEtU6DDFaDamy1KYuWNfP8Boc8WIts2I5kT/pGrD9s8OES4
OUHlExui+6n9F+NuK1Hoh6I6TDCVsEsFEJpBdNcbD98rrxlqnwr+xN+Cs3aCFjpzPMb55uD9OE/w
sd8BY2xukIPiVtOlJFEpoLFi5TTwZRNIzcT+y1eSEMWzRxFUc7NM38ie2Qm1ZO6C2rEzuvtuubSf
jHplaLx+XIvpEakpgQB9zTmnkwj9EMxqs+YCRmG+C36an5R/wLrXF/B7EjkQ/SC3LTAEtKZbjUJg
nRp0yHGJUSsTW9/e0zMZ/4hVD3FKfTCpWxupEvscmKyuiR02gSyz1LNXoS3cid7PBXnXhXYXthE3
WGMajWR2JVT3ZwPSPr3aa/NCEf8AXrsMpEtDOZKNnqdo17YvR7XFIUsf3wWSwDMx8jOMcvZG95Ng
nLMTfdFf2m2/48eIPpqXXxspdCSC4F52e4lpPdaDopPf22BQyuy0xp8E5xj6vEb4Cp6mj6r2Gm9+
zTNp9GKzF2BRJPCNgfJxArxLUJkir8YJhIEYoFKVjVVhSgxeO+tkZVVJK9Ha4jLjJXk77EkSx7x/
/oqPPXrriXd3hmw022/C9jjNz43W3ow136QgLiyAGIECma02Hh5l/dPKNEDtRgia4ROPpiQWb+BM
sc8s5p1xpHS+4LBmT7weR791b1iMvHv4lh4ecyxmj4vDEHyRXIGFtZLfLVkZHOMm8gplq0fzUN/k
o8Nhr7rynvqy4n00VE0fjy8D475BWqevGfMmehZ5OCcrKmnuGub6AJpPDNN9btac3aQGrOh1ziub
tqkiZzH6PG07YrFwdzKrKM3jYGtJ0zHW4l7NWdCMpWBTc6hk+DtrlQ9QCT+34fDYknlmt2ANxXHi
i4FuO6lFmy28cuFFZDboSstv7xJACXh3MYVFMUYn90xK2sBITwv8SCNMv+fB78+JHkHAzYThnfkH
DTGclWl0qwtG0vOGZLPcd9aPPWL8xj4vq8RES2j+W4u9xXDrfeJjFrG91fjZvzomp48QOHAZSMnZ
2lLagoO2pbvY6vOz2Dt+DfuVcSTEuM5gqSdjvGCevkreMCUwZEI4lowpVz4vW2s47+N/CzKkbyte
nM8wDuACH5myMpibg3MbP7c2faWrIzmE6+zq2ER2oDftE7VF41IZFxtuQvC1l/cypH7tjdpki+wV
I+DTiaklLWJyidu1rnhJ5fwJ+fPQPC9uIm2wpy4ErjhWf/n8WUrqYlZcLBuBIBNS1Ez2NTZpWaWS
Kv6EUF70AKvSbLLrSGqMiLAk9cj5fX8mGs1A7aZOnq8Enik0Sq0+DhX2SUO4d92JiOawFOAqh/i0
Wksi98qFkNwUXX2SsCysHaHCAmYntjn5ehhbzjX30F8M2hTesTfJD6+2e87DMCLo51cg8xBe6fh5
6Plh7EsbvtGM2ZYm6mUN8+TDU3FsZdYy6Gozlk5U4JAdlJPC7mLpdjINoAs/EIt2QbvdhYeHAT04
7Vrst6NdMZCqz/IJdLXKPifeo2tKlAuA0Xzpnmz7TIwxn33MD1W6q1GxH7eh+aKBATk6dX4jzQVg
Tof8xAjOnnbl8VmmuIgUWD4c2XQGrHdbCfNbITwk8KidpnODgkUAVZ2yojfoGU5DzJI5jJLr+pq4
LVNcFUn9Ka3JEVISC6sy0Ti1iDsDZDDXHRxQTf7a0UBLXm/g8gJkyNPyRwzE0XurMgSZpVJLds8K
ygV3OpHYiWLSOYMNAXJKjoq3wXJlc7/OsqAk5v8eUlUlcrVoMpdOv1mrl4cwsmp0ytSKPJLtBkTJ
qsF10sdaXOi4mpTncmg0jknBiND7p9kQObvp2avfV4qPky+Nl0YEAxi4zmNSI93k0JYzmCtU5kis
6fjnoc1acchPMG0h/mq7sa7ZRv0DlJ/bSrg7RfgJH2/iEtm517qeR8+73hKXkwLhaht1ZPeba6Wt
OzpnJzTQNXC15RXyhqfpM2KNVvM7b33+AdAXOr0jtgVEqDxu+tW8IMLS9z5YFy0b3M6tfKIpO2C/
ewVkCKbI8ntZq77/Pkit024xshrqidbjztYZjPevuawliVo7arzwxWNhnAuIoxHS6cSGJKIFOfYx
FJv7nsueKJWaGTxHaU7tsgBQlOzpO84iUXi1/Ti0FyLEPD9bGCPEqz7i/NraEWgAepKfCHx9i6CO
gVHYckQfW3yFm+ZVPNWoqEwR2kK/bTy9/E/fTlLVKDGRh2x/btPGLy9x1scqMVZ2POol9IQyLFTs
d4R4dFeKoYF4HxKDbne6uQRLYqs2Pm5o+a6Rh6KREjVjsnOajmqXcrqlOted1iO8dCLy3jlKt/+V
K8ulNHHBccmx82H/BQYVxqHpMTCJXgCzYY+qnSmV6riRfLtHxVR2O7x9/17l9gNholK1DazxmlKW
72L0eeXH3Vv7qe3K/+YfP+EZO8hR3/V0WNXZKaLspwZfIziq2sgcKxIqz1hHRVcglmmV3geEHPG0
1h1R3JhZ1RdkjzmpAnlXYhMW5IrE9M4A9nFn99oQHzk2fci2Xob7gIUqfiuNhFu3prJb0GxsBJn3
M2hWxwbARnlr5qn/JowYMOZFjc58AZixZqMd8qtVfsjobp6QwGrZRZ+iKY3h2P4SJcDIY9KUgRPx
QOCfpzpOCyk4Z2icDAEDncTXGIYkBS1Hy/bduNJSIYukOFQJRy27g6v/lx59CTNu/PexFfNQuS8D
0c68QSX3CxBocfz9vDPoZCoOnw6Gk49vHYaxefES8Z35Ykl0rWgjR0nQff+xvGgRnjgL+7+sfH+e
GLZVDd6cn2g0pCqHqleBMr1a1LQOR+G6IonFqsxGR1ubcuSaBPgRe/D2vLrAEyEOF1mCQEjUqbcH
jhW0z8b0M621dP4tVpy9wHWXn5UQHJn/oifuo9WmU8jyBUNF9x6g8wb3nc4kKQkZjf9gD5iTYZch
ybXycowCtzBjtxJOui/lDXtpMmfS8jvpCSaDGKDTbv6gZEdE/sA2+QeuNXvE/TzXDLORYrCUmCSv
rE2NjpYx+Jf1Rg7xPPt6Rs+1YG/VIxRMFvXW/nuFiH1YW74tXORhfwBnXcnmkfs/eoKi946s6yM8
ZylUUBN5iEzlnbEsbgep3Wjq6WnkEfvBS+5HZDThP+6UQymVY8o+SmkILGyzHPSFbcR0GJIdoVH4
TMnRhUMs2JPnxUbdn825pjthkh7FtFYzipvXuQrqaampz+UJRXCMef5J0bVtQZBWX2SsbSQHJkYX
wo7N8gzp0CiE70gzgva0K68u1dbFu9wi6mQtVmMw0b2KmoAprcOOcq4qEnAYTFodBGBxRB0gRzqr
ex6WODqzU0GmvM5mqU476k7BFTis59PLBmZoQTFpYdnVltba/ag9/VMQo6DpvmCbTHH7E/BIyWVW
Ded4foFAk9GZqgREPVosuIgjgSOwqD5YzPcTLthycBMKFlDEK68H6y5/S89O29JDYAPlp2md6Dv8
02fZ9MOLPmim/l13sj1xW9TXhSI5Vc7AIc9/zZ50dosleaNNd1UvMr2vZUGx524NU384tOolmxGh
Nise+kJ0jkMMzDq1/AVU71T0RMltot2F6BBo7h5EeNM8kmj0mVipVG/BEI991tGM9H/YX43OwHFR
EcBfSHQzwfh52PXrpVkpdu30qLxSzzjS50VS3L3EzVFAH//+oqfSayQHSeo8TkDM/gZA9JVTAixY
kpebdclJddLtSlMX9o+LI6RuNQq/6UjQMKgakNty3AwLdsVSujbjNWOtgoxO3B5fb7jHzaNnv8eR
5zPu5mosmtFivLjjoVMPiPZbPr/gveBlZkvl2RZY5dbcKiaPlNqPMaatAgb9kbX0B7EriWGaDsdt
DA9E8mBXLLi/wQmFeRMGkE42uKZIyDlKtJ9+JiMb0y3lu8hoJEWGI4RxTm8adxLFIZIIgkR1PDz5
58cx+XeM8C6KySXU3xoPC2oMtLMWjHinEstJc8/G0hHNINzf0YnTjo4IRzzeuqQbX/g1sJ9hdNxx
T1VVhYkYLkxa1cNA6wT9gvUwX5GrhGBxzlU2UVPVkkVuel9qu7qykPERyU3TA0Dr2HZvZ41jfEad
ZAMUIbOpBkzwIeVMqNMjRe8i9WIZSi7xKS46h9YLY7oTwKRgTHKo0v4e8QJkdvLmGDr6wukHNhlZ
JxCV1bvzrt5SfPDgmnWC2yYO4gPUMMcIWTTHFhLjA7wcFCanCWuyEFI0RtYicvWJ6QFUSSGXj15u
/syw1u1rEUOur82St+CLjCLJtjCy1PSQ9rvJ9vmedy0JKAKyrCFpgHTwCvzRTtqze+ldS+ME1tkC
u6lJAUW8O2icCx5dRFjwl1DM3Jv4t3flvoZYPxXQn988wSFTQaG964CWtwwvLkmAgFvMMqa/NoSE
5/J9Qf1LtLYS5rUDIYA18U/gXKRqyaZfovUg54vkIm27iPZ5fxjnukvrPY304dps0fXUvoTubb0b
Fqjy6cwN7/tiVHwS645u0rjMYSRLRa3HhY8pMRTyMrie17WjfB4/sIVehwL0LaGckTCeniJK6Z0m
FPG0eg5FRW2hbggjGqha2faGWa76E6OJYN4ucsw5WUN/R/BxIekNXc2+xBQCM3gDJjrzCiU0boC9
BoIUzR6fAQIUYgIleOSLGITZNFjPmbH7qqWki6dPlPw4PQURfFNlY4bD/yP1Hkm9Dk5h/8Gye+xy
fUWdHJ1zstqMYgWM+UT8V3dpHftQ8KhLi2DHbY4dGDTzpJkU5v+xs0u0aeA6fI0DMn5F7hbEnZgA
2FUhIJrjTaHT3n3fybQsRPr80xccq2Uap8OaX4Lo2u7c8MbTieGEjdiTJwVgROZRes11nZHSJFH2
ltb2+8XLFXiQt0bV11O06JVt+ucqleopLgTaXR5Qu2Ud3hhdz8M4dVtO9Y1O3fGKNFp+ViVeh35H
UqlgB7AMAVOUDv0n/QS5zmFYpBs5vYx5ShbLvg/eSmOL4VSzba5kqyQXZO5EPaIJWHbYsBFWhWD5
gKAgOtVhAxzsVYe+OBJtdE+7b+iqsstN6slUdV2e1ob7AGFhywhZ1Jq2CYGwZ3Ql57628snOA6vr
BuunYQ0fV/+WI/hj/iihR97pPKXZsbtk+RwthQY23AfkbvGw/MP8MF4AXUql5NuQNM+g6qwkt6+S
486gAjfSq5nlAhA/86VhhdO8vHUUPWw6U0diuC7c6FxFlR++2CYpJA49rqKUXUtfl27aXn+rQqty
+cw8XbfbEWRz9KaogeaFyMWeAO40OhzUABKnZy3IF/D96yyRJH16ELYk07gNrkA7v0J/ubtAG5yM
/gqEXXQduzsuIAGR3yHPwRwyV9IsN+QFxiYXwPTdgFKulZy7U/vPhjWO++oHXm8hHmRRLNwLIEUJ
AUSCXysF/6U+xRssDkNDFW0d8qLUoMYEC17q+ws6b+RE99ZLI9LCNoOYN1gAgLSKZdUCgq1wO5Mf
xwDGDaWvA7gHmzDgWv/9g9fTE/4B7eLtLtNdXz8xOz3QKil8HWbfaptDwZT1Eoq3tjNx7pWRZWUQ
gedjLtuX70BZbVRx5bKKzvyRuWUaokNuIe97VTX3x4BJJXEVEaL6eqvSMPCEK5CVm5ZdGBooWIbg
4XnCOSAWM6mv9KgOueSNiZQAYEB3b4anC+B5ocRM/7q7FkoEe8H8SPF6Vt/7blx+RnCJgQVhKhaH
zjT7f6OiIH8PZqUnVx3t+UHUWUpsApqtEUl2FxaEuFvttOH4Qqs4mAh7LY6UWalmkvlo5QEoHdKj
V8a++9w/tzgocUQbG+TvD+/NxXfVc9ReRzUicacAM6ej3JqMVFinWGA0kgik+sUKsoR9N9KtMEd9
Uw0zllQC4P0HB8tRgQYbLAFkfNpuOl9UJWKV3wblbFTQ2+1IITasr4QLtvnPk/4hUgcRob1wsj3H
+ibCVt/8svktZiSvaTaJjSn3y7FOvCbV7QJrWatDQD6VhuhA2S3qBxY1hn01iaUlu0DcunDlcyEr
TvaNCpY9H8jr8+92jlwqKobbAOQb4MRWAlr1h6P2ZM8nHWp34+ub8S37rceeZxGOXrRRgvWh19eR
LNaCLM+V1XQa3CWsMKivTh29JTiVPaQXmMTihZ8kHna7lyawi4aG4DTuyvgaP3IUWet2CIf/Vq57
AIdKxYrzVncnb2UBAaoVxPEr3KHyLJEt4rSpjlnpIy0KeYl0QJPVXjwV3kAbtieHfP9ZAV/yp3Af
EvR6ygrpVIWhAnBeOWuSedOIe2jattpncvpzPhEQjj6a0v71XSUKEIIyZlwIvZiHiqj5fBW7a3UB
kUTWJZTZfPF8O9eUZe1ElTKdBq6Rb5dchBSFC2iTI0rHyQoBjuQ4njQPIIU3sqaWE3gTiaIat7CI
16RQBx2JU5HNnPHWgK+TOMBul4OHUrEWKrLdQo4YHF7XWI9GO0sISN+oKONaVlDqp7Xeu47ovtHo
RVsg1P9wNNoo8bPprkCUQqDi1XSp+BQSZAUlHT/ELD8Vz7FLWzcLswXTNxzJRI6wmpfzviMcLhr8
UQzh/pSohagCmS6sj3oEHbo+45odtuqhz/1BwNHgi8d3J9pS6acYHUZWBYhXiDnHNB3Z91ivHGB5
cpfdZTekscZx6hPDuf+mH4LaecVDpAcU7TWAkmLVd2b+O8g9T5f9ymznouVkMtHwKc00HHHY8ON6
FbvEl59oB6rxxVDVtqGUvPgJXI5AH4/DCQ3IHSNkimnPfiKqtZGR54ThpIJXVRHaowBem2zGm1fD
CTVhMdqJhBhzUfmKOLvBwfIKfAX6aiw3ACVm73zcWWl1WdWsC2drQUCaZDINKSC/hEV+qP8PLVdA
ZaD6qmydbpxxeNp4HS6L6QOuLyOA6iuZPFYqgOpC/rjWaPkreYtnwgYmU6trBD7St043KROdIPtC
Fu0cqayZuRCO4U3a50PdD5OYZtRc+6Uy1xcjjTewPBBW6cgvO53g1xECE4VB+XyPgmfjoe9Ioc96
DuU5c8M7dZ6ObFiklUx0r4BUke2Rk83YvMrPpns1ScVDlLXIKgD4NGfxhJgLpkMpA/4f2QWaI2VK
OyBlCGE9Tq6iXzcR5y22o5og9r4GjbZ1PrnzQCI6q5mxd5olF5UmS1ub5WbbMDZpqkqww4OviLc8
FR3OOg7BmZI8C9VIv3LkeDpYNjwft/KBf4iN47JvSRB1FXqI6qPpawTVpFgcAtx1yzXJixSJRCAs
DMyLpNsLBq0V0fQF2uXlEL5lbCe+IXH5emL2irwsXhzjGJi8EVY+jQmb+XRD3gSOyc4O/VJf/4vf
654nO9MqH8rU/fAsqtXaJ6LI9gSauH2Zrxnmr/MU+Z8uEDrX0pb5F4pOvyLtmE9LjT7J0xsB2T9G
crqt0u3ykTfbotTixoH5h4fvB0FY3xDGfukylqAlFE4a0NRMG5IQSQXEvqiDYEp0WZRLDqQ1m0sD
oL1N4HQepXzgdsg2Kx9o5ytwHJied83jldC1d+31uK2V3KJS3ympn866s9S1N8IaKBPIq8rUuaBh
MZ4Um8XNaecYNUvmwSRgtzqpgo24nyZ+jArH8DsAZGX9Xo18qLnSDvAddxO8J2z5V/lZWaMQJg4a
NYQ0wj/KCsYP4Eop1qDmQgWDUAzxK3vscV5FgZaBT2XEKbCmaPgF0ExOqjGyL6ffMpfWaf2GpEjz
I2ghX/eXCwLpliyphn+rQuAPaOAP7JlkB3W6F/5pX243GNJnX0DOAYpSjDtkYhvMNjVXbIwmfLJI
ScYACwUkBP/mCGBWg89xHSsrj16JPnQ5hI86/yGo40FhLBlBVt1xiAbsGeusxw9sYHIhsGSLvEn3
2GaZHOzUwatRCVk8jfL1ysuhNQNyqVU8mngHwnthDaFPMYgBaYISPFar/t4avLVwYPE1Sx2QxabQ
xH0CBRl47+VF2+quI8cmhB85cxmoe4vzSJazA5h5pVAsDe3h1QY2NWnElT5puk5jZSEtg3MI216w
lpTDJq2f/AS54lZ7dCpoXT4kHZxIfVl77pzdOVhuAPKJW4KsasCtroc5+lFiCQ3yabuvmIm2nz38
ppMPE62W1MIpD6LX5nSRES+fPgZlpUZmYlseLK4SIP6kZaM43beiVGnuTZgPoUz2UQTLXWot+A/l
SsJEp5BpDUPJtl1xXLTwK1jMrZm7wQwQUYuxTXkOw8az9zodumI7R3LoBmR+4oqaNigzkXIVtIGH
Q3UzE9UE0Zl3GGFA+3UxwuEbQWHHHowHSZAnzUomIY+EoKWBCRN5GivL8L5zdWBDy48rPj5xYcbS
BGFebdb26h6/1ZuO75W0GF6IFkYMmG+rz1BXwNahSUvbjRstOYGpLvtWP+/WNPIduoqN34fpi+mw
LNaQwvnrt/CLzTE1J71aQcC31nlqRqJClcYTvZbcyyQZue5Y5ac1sAoeivo75lnxvPiHWXAxfDTD
yDVetJLi7SDf6b1pDKj7CrHtsGTbFzfJwhRdrv2lQp+eD6vDfpvO05d6rhJUETrsjKN8nXgD9CqP
bquMwh3LCCYm+fnQlonI8zcbpRZzEVTN1NhRHfCDbIc4V0eyzST8eJxci0gblf1jO1wDzL0dg0ZM
4kybe+klQCaaEr2tIlB8hb9c/E7zqMXxYxU1Lrnwso4AGNtrQzMYzP7q0jzfYw9BPmfKNtgMfN82
1anxHjFdHiIuTT9+z751VgLJAwpYep0x+iBnuqLNfhhvZeSKeB2wy698s7YDxp1CTO1znZ7DJR7O
dkj6q8ZFQvkt+Rcy10YNRLGc9K/9a7aFdmS7A7jpY5UwjtMUEfR8SKL6tnOXruCyT54dqf9MXdpp
G/CUhJVOt5VCWjJExPAWY4mNeHdAPaSB741nvEePWUlVTeHDEc6/M3MlpLbIKJQgByicn6htdDCr
DawdcgjxFiTiIJ12n123KdEMFdcgUYAYyJ0obpnEHpZ6YKlbVBAwbkiPFatZ9FQtWsESCZNRlqiD
wJzYo5n+ZOnyVTLKmlXNhjPzi3t1V9W+HDOyIscGiaThh9QPZpFTgbXSWHXJORtwar4IdgDzoGcC
tLMQh5PCCbnECmZSda+994M9JCoYP3mJef9La4Ih4a4vCAaOPZS8VQItAZi5SPgU4lvzzFhyPYrv
Biq5X982cokAYb5Am5yMMJZ+isKRAxwPnj8jIX/wIo2+GX4deICioopdLlfz18285izL91lIJ2DS
jxJNMd9SrDUCZ2VORPSohgVuMAk1hvDs9mldvLSQhOzU/wODLVq/iJ1CDqsJUdKs0LDNWs/pLCNC
SEFQK1L+Jz/qiL7C5kIbmiNYfbbzh2pb1itkJXvvxI25xBk2MtqBm4Vjnuv9l7/p6RrHyI3LOpXC
QyvrCFdogQZjRrtQZv26o8aD/YB6zpQ47gkIl53Ll9xBV0e6NqEl2Eq8SyMUfQXC/jxpwAMyZ7AT
gyGwfSaE2l2DeKZ+5HSkH+VrHIO7+KHQ/Sk8C2HXIsD41+5g7l6OfZbJKh6ZViDDKp+KiILM7KvE
47ivUoPUgqVS0PpeBW+gtFJyCfmHDbSP+cbJFrZkqUuk9kykBlvRQRL8VLJsL+B1kD+W+axMp73S
0/GOfP/LqXdsTu3MWQM44YaQEp5zHfMWQtmbMr/KeLayPFSi8K6r091tgRM46qaRVtj5Ky+XVDb3
XL+Ptp1Ae+XDPit7UsrIcu+76ztXmsrdnN6xXmxEJ7IFo++Fll1Z2Rld7TA25DPjnA2ua6pTfJTm
OjAAGx+i5CtBYuX8nf4gkKHhsQYKCY0Avgq5h4TQa6w0Um7na6TOWlFI6Myletr8sFrhYX6LOo/F
FK2gcj51VNpALRYW1Kjg62+lrUgi/iDr0UCpXCvH9ufkCsZ5EkS3/18U2vLym+MRH9vbALy4Fta5
w1M6zrmCW8sa17BxzxNmgJ8SdrYOh5rT7FAeKZn84+1aQkeXl/iYh/dbDkWAFVa7dN7H8ylzuQGJ
ST0KP6INF//9dLlzLn6uYWpdh9/Gv5W2aygSJ14ZwJOFAwpKjLmHxIgP7tsDElVp+1h5UdALHBJ2
7QJqQ2APjgM9iIm9bUx43vdz7q/HmtzvcRkNMJ5+urdQb/WiLZxOFGjshmwhP5484XD4l8a69FyM
zp7DvxT01k5NuwdTTTSsF7uBjo9J4eU4QIFoILOs/H0b5ke1FgXJaASakp3FK4lpr7AN/wUsCmsz
P9mzqlyJBYr7GLXhq6TvRC3eHthOn2bssag1snNfiEWX5MubpVmkkYP/JbStzytLroMIJ/GeN9tQ
b6pbePb4Br38QTkp19I9g4gwzsGal1tkfNfivAbSxvAlK8O7PCAKilHReklQy50OIGhm2nbGPmmn
XFCYWXqSYrzaaePERq9llnrRWIsU6kbHGCYIg8mxuhSuXLyxRiwlnxaRAxgZB2KgUrGNx3MtmkV9
aRnsPzyNiUeXvSBX/lVtqv2/COM/6INJ5Epz2+ngQtCXsIKQ4OLsLTFtEoxI4QEID7KJAchO8Pzy
GJDwhIhTSynOC2tGooYtSXbdMPJb9eC5UOA2cQKA4nO9Ttkr0GRG07HDGEVfKxHvjMKHV5C6fd1H
eetwhZiPJ7UkrjaGaA62evOqdRK1RTRyqTkhYTtx2KZGoyLyeYKPX2m83ItIBmINmQGi3IZbi+/o
GJC+tWq2gresmT1HW6dS4hkiJBO1SrXEZmIZ4z0Ui7JlWfe56DPzJZWBf0tXEabvbrT7ir9wZtg/
44nz6zSL7v/vRl33cbSIhTLV/V4kb4xiINmTZA08MturL3vpLGG3GuHJT+0bn8bBkpyDV9uSh1Bm
R4c41lghUCG17rzJMNXhDGB7hHl4sYVm0wMnEx92w6S0yUEsPkK+DOztvv/4jpw7yFBgEXqQauCH
LFe4mVj7qV5SRbJnoaiBm64973H3oiB4ixg6BJP3rMk9em43v1rs0NVMG7clmiLdmJdqsaTToFLp
mB68x/Pp49LSAiOJ48Y6UzF6I2YB4AyOQfoj0R8LLAbru1jDyLOpQ4YRi0ODT8OARoifDFFZ6aSW
AeuaMWod+EQ=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
