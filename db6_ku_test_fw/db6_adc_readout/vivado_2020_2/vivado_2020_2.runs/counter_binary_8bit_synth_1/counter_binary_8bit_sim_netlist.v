// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Sat Apr  3 01:45:54 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.runs/counter_binary_8bit_synth_1/counter_binary_8bit_sim_netlist.v
// Design      : counter_binary_8bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_8bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module counter_binary_8bit
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [7:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [7:0]Q;

  wire CE;
  wire CLK;
  wire [7:0]L;
  wire LOAD;
  wire [7:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  counter_binary_8bit_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
4Q5o1J7sBpSXyRV2w0uEoRrbDQMjXX8R32ArASMqUX6009WauRZDX/ign859+MXfb9XQVTCDhlsN
0zh4voXNomdW9jIqsDh1r3rZcFHRlOhR4EVLa+LYukrz32N1HPLVb0npvxsECqgzX0Nk7npqinrX
HXcMA956/JerXqK0wHY8vH+MB7AgBqBsvnOix5ef7mDTlq+sU2EkDV3CZeR6ug4vygIkQwPpnnOo
2EJFhYyy+ZN5cfbNLmhjjBqGuwM+n/6P7tgYDR1dOewif4OyEc1QdxJR73QhY+qAKPYxwJjD3IhM
+4DHG3xyg9Bcugoi/PcQBE9DDr+93ypKEM6bQTdjjacYsexa6P6oRqvxVjqwyo9K7aQoJP3+RFXT
LzhPsfGNOrJV6LDJvpfqXYzGBseZhyq0OXMBQvc4IS0w6WN80aEQhm6W2SUV98MdhqBkhLOGBTok
ThTODvDVmfxh0Yws5IE9n3W2a5luArMl7ypPoXzW4/FA4xY+TFIDn77eLECxX5LLgm8a2n7pwL84
g0arSjIJgzAmMAKBDfqy6zrcfw1gZz3QlZ05uCAP0dT3QLoo7ExF8suZMHJ5XPDXC7Ec1Y6igcZG
UGcLDm2riHmjQVRKCjal2Xso7y9uleKmNSRjI6VUL8hogUjQqKp9ZDQTTLN4TrZMx59tDgEzoWaO
HYFCCb0CIXpmA3ypFaeBmX9Po4M3Oij43Ptphzuj7G36tH1DBIwRZuJvmbh9+M+wqfDltO2RaPsw
E4uk9Colni4HEu1WCMDng5p91uKwhcM7FACm7upr56Owk/zZgSs6h8V7wgFh55BhZrgmPLhRNM0w
cIrhZbGsKmwynB/tGx7msebFQYx94106L4kCoY6fOLLI/kEKJHKKWb7Nx/1nFee4WOK018joSqXO
TUqWMUpeS2HBC7qzkSFTMTQOQWpqaAJaIDBl4byUx8gi+/9ASyr14UafqqKZ6vSMjsxI1uyKhOUJ
pAT5Sr9svCwOJtFoXxpR5XXYNQyw/QKMXsWp1FspsTlJGhCLtD2k2bo57xwb/sgXTiJDszKzH+qG
It8cBsxQyk2YhcsnrvUlmDDrLtijeXKeZJMzDBNEH1ejixaGfOwCOuZQZuAnBLuO/QtyfvqgQ3VB
N7tXRmMxbAd/6jGQaq9kanRDrirNhuHpBDdQyA8y5t1/Szb9de8fubiIzyJ7gExK8BAI6Ae0bFT3
vKp0S68c49yGwHI6ecOE4WU0qLf8y4DuMPW6xLS2klrVLbZ6jECcrB+W06rBz4BK9NzT1k741MFb
kRCkPE+1GhgwoSIxQMYzDICIT+wE4sNoVl++Vf7wU73zfMPEFGFa/ZjrZZNBb/UxOcgYycMV8inr
jUtBzg6O1KhqbIuE8QuPsvqAymEb4OACl2D7Y9UdZnFASvkjHvmPRjA7pB6oHkKbbS7UnQqi/qi9
UsIZQcxF0XShS2zS5AYexjtYUuDG5Sh+8RKnaYcg4aB0nPpueKGI3o3DcafWQCcsp+Mv0VuZ4nnm
VAM/3RozP9sdLm6jdBVnAH6ebz58uX8Bek9UN5V8jVWc/YzEwPiQM/Yecbnum0Etvam7LxhHM3Xq
0XVvJ8znITyUomFkComiW6GCwgNoJO7b5nbBfS9MV/77uPi34Sz2/lqAswAzyOSqDGF6C858U5aV
JwwMfhUT28AHSB016APzAWAhrqgAP/zurJ+HYeKOIHTLm2wF9F4+00t4Elw32PwPzKk5JcMzMooV
PffGLclubjnNvuUHEYdoRLStOlBTWo9YiceXMbr3/KuvnxJIaIRRyGNAd8kMgwJX1xdm32JtyCwB
UK4ivrzKmND5NYfMl3G6b00bHe+vEmq3bQXTgRUPMZPNXlOvVt9gqEz3GMfIIlb5lES/qdaVAqsA
57xwQ+ZugXgpaDBcHQpt8HIvBEcSXFs8YudWG9NXvVPH+jYtfTvuYkmdJ1lCbPKUXYLU7nRHFBWo
JzAm2FBaQ2wzbuMGBLBhljWSrSNzZlwH1vBGOs0oM8+aBQX9LJc2J060PnUezxjPcQ/44UVVS3NK
sOyJUZ+sFYWirFyhBtjIStc/XIYzYaT7VXITcEU8PbtpWhub39Zx2PQ9p/Slhz4L9Woi3dFTUFLf
pqeOTcmzJuPQOHi5Fs2n7jv5r8szet2U9ixNsRrjMR2z6jmfAFO7Uglhtz7lXebzwUs+ABJZB83D
eq0F+dfAlQdsSQj12JTo1a5cQcx8KcJZHkc60LIY21bJJqJpXm5QzqjwYhkbPO98yzgcR+z1NuVk
emOeygEPbHOh5l8am+At6/BQsBtZbP+w7hHwd/LiHCRZepv2Ci/oOVpdTBPnblLHt0TZOnOgxbzO
IyrZXAMjoIReFORA80YtXbx/bXNpEy88DUU1n58L0adOcUhbdKXXkaulpwf+cylMKrHPTON1HpOc
z5Mh+rkvOYVmA2PUjuWonJhyWnOVTAw22X1IV6x62MrbHFyafWvcLlxmXKH+fQ6dNb8jZV+G5qlb
m6pdYtd6FOfEMCpLqclqtqRDnS4xgiTkHilhDfqWlJmBF0z15nyDKEztQjxeYQygK7d2fDcqe8E2
My+pkstavtATa69TIDy6sJXEbK5OLTMZRfYXDQGcABERUcugYik2GS5Yo/a0sOUSNJ8u/NaqJ8+O
FVDA2VqehNOB4PPksq3bt7umRxaCd6ZU3a1Fj9bK/v1VvUtur0z27vNavlZ6/LW1mYzyRxsgQAHT
NaXDv504tYjxDBHdZaSitATtTZoXpuVwLyAfoDnkUMpwJOkhJylHtLBUbNF1IxUoTSnP1CKfsG0g
oCpQwhAfwCuTqlt9N9rs/IkAIX0o0hb0Vcw6ZAl3m5SD1gIkOcwCFQqeZWBltXGOLehOyO4Xu1h5
b37/bKZitT9dtK1SYPqVJlpa6S1OIsv9HOyZaCbPNyBTHDSidY2smcSww/ficvqvZlWVO968YRfS
yE6ZBkT3lQ4Wk+03zHEB5M2c5+OZFxKt5brmicbC3Y/g3B5SzoRhEf/1n/WzQAPf9FXWm3aG2dI4
Wly/TxfJohlc5DeqZLtWYkhl/EFkrhqp9mYIIj/IB6zsvZlFxubzj79aUpnMhHsb0MKqhWC7PphZ
1QZ1YUkMzb2a92dCQlW09IZZSzOuXUWe1oHyH+855g==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LKkAoFN06/Ra4ZdV7A4rwLaJD7OV0uCUOcFTe/JSy2WpUmZ1NmmFH+fHmNbt1/CUhylBhB1NfCKP
hjvcS/G42wREBYqS39jIYxkxVLVS1PoJ5mjsj3TdKw+D0RTfVjTJck2irNxrLNWzmhWebslauvpC
6mIlsKMbIDN8WszCk+xVP2vZteCGUFFSWmKlWu0AVEM6XmLuNSaaFXIc3FTkmkcV0FYC/5lVZy/4
22yPR6OSEqTr+HzXvfEGhFIpRFyBfo6Tua/rmYjj9m7FS93CWqsjjVgnHrvVYAZHV9pCi3dDCqEw
enHxaMbuKSBILWitrUJoym0iwBUV5xeDAOyN2Q==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
1Wzp4USbPFxqndtVtyvQ9m015HSLIC59iKgKJz9yJNDOCV2PsQuHi3kwRKLpFPxnZd1FmOhLsbPH
5UYspfQFYKIuk/53LJrg/dWdg/H/kTezYBgpEVGpVeLaTgqlKs81dv3VI6s5tc5kBZsnBdEAPe5E
wfG/2nr3JKPmUOGXTmVHOt33wq76C86O0s9hdDuJ3hISLe0l5ZG53pyM6bTDzEgf57ND9qIyJUmj
nEhZxfuulZedDG/VnkDoEbKWb60A49w2lMikE2RRKKADgjG90k76CM/HiaqLxZ5LdjoKpUDUKBQ9
81b4Df3PlTaZAsdMQohVk9MTJhlvQyZvfi28VQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12512)
`pragma protect data_block
4Q5o1J7sBpSXyRV2w0uEoRrbDQMjXX8R32ArASMqUX6009WauRZDX/ign859+MXfb9XQVTCDhlsN
0zh4voXNomdW9jIqsDh1r3rZcFHRlOhR4EVLa+LYukrz32N1HPLVb0npvxsECqgzX0Nk7npqinrX
HXcMA956/JerXqK0wHY8vH+MB7AgBqBsvnOix5ef7mDTlq+sU2EkDV3CZeR6ug4vygIkQwPpnnOo
2EJFhYyy+ZN5cfbNLmhjjBqGuwM+n/6P7tgYDR1dOewif4OyEc1QdxJR73QhY+qAKPYxwJjD3IhM
+4DHG3xyg9Bcugoi/PcQBE9DDr+93ypKEM6bQTdjjacYsexa6P6oRqvxVjqwyo9K7aQoJP3+RFXT
LzhPsfGNOrJV6LDJvpfqXYzGBseZhyq0OXMBQvc4IS0w6WN80aEQhm6W2SUV98MdhqBkhLOGBTok
ThTODvDVmfxh0Yws5IE9n3W2a5luArMl7ypPoXzW4/FA4xY+TFIDn77eLECxX5LLgm8a2n7pwL84
g0arSjIJgzAmMAKBDfqy6zrcfw1gZz3QlZ05uCAP0dT3QLoo7ExF8suZMHJ5XPDXC7Ec1Y6igcZG
UGcLDm2riHmjQVRKCjal2Xso7y9uleKmNSRjI6VUL8hogUjQqKp9ZDQTTLN4TrZMx59tDgEzoWaO
HYFCCb0CIXpmA3ypFaeBmX9Po4M3Oij43Ptphzuj7G36tH1DBIwRZuJvmbh9+M+wqfDltO2RaPsw
E4uk9Colni4HEu1WCMDng5p91uKwhcM7FACm7upr56Owk/zZgSs6h8V7wgFh55BhZrgmPLhRmiJ5
Qe1UiGkCH22jmlrrZMyd0BYG7nAXQnARyVG4JtT0KOYxcctVmiSEJS2mA4+MjzoujEEcNGaV2DuW
Ff/FXKeuVS1CrAnv7Wu24Pn0rwTALq0otwCguVJ7g5Ps3ouaMvAW3vlEOA23296h4SVdUISBl/Zt
64p3HI3WUFy/L+zKciTiT2JYivGLMmnONdOOACGeyDj9UEid6ZivuSrYTntTSqhEiaKIDMSHRUDb
wt0E41qKIeCPCJrqv/gUFZbGRdSv/ZHUYaDgwYYl27Tn3JIa6ML0lS+r6RtbOw5bI+9xK2E5mpsX
fOIl4A9XL1SXB9cOP/IRZoZO+e/sn5Gc20mVEyvGkcWUUacIwox/KAnWkhiWh8CkFsvRFYuO1jVK
/oYjzR9LMYwRSKOc+Tv47O2Z3dKYAywuRg+IayZMkG5iGIj0RqyuUrAIYhAeXnf+f8QwqPGF61Fk
i6QhQ4wox7d6iRQ+ztjvY+MXzj5K+YqFpDRyTF5kYLmmvsiGgS1xrwJxiHWUoHZq58FIq4fKRq2h
3yRsMq51J6KkzRMjp/G5E0AZwN3YR8QGcKUHoMDFabnWnVsQkadHUUDFUhz63QgLpkMyzZ3xiGBa
YaFjsE341gE7GzwE8/gmGm8VCqrP4bkAxfUKERYmw5XGGueSoiwPEnUCCec8bkTS7ZZHCtBcZlVV
HUUrRfu7BHruqr79MqPYupiZPNpVPgiMoPae+03RKBZvxR/TvQBfHDB2Ua1vTDixIE+yp9C1r74i
3IGoYyTr+tDASPSFHq7uRP9eWWMhaYmMtEZRFkyN6yYats5TEM9F7T48V6cSihMHErHElGDg6lmG
6xIoHcd6eapE9dOSN2pep31jtoC9jjmVe5VD4ooN94nZ5o08BKZxiv6Jd+SxmTS5HkmiiSSPKwca
Sb1Fk9QMe/dDQxtwD926bl/VXTBdiCOdpLPVc2L/TCLTA1z9P5yewyJaEOh7WHuCbiisSW/ncLYT
e1ppL/V2X1c+f+ezXxTLW17Ev6lFxKnn6UlnvPgWM2y6dyG/MEbBGH8lUHelAab/qhYT7n6w7Qi9
ZQ7F8caMe2pFogIAxxKOZ2PTrheZs+lA0ic+Jv/shEOk1+pocO5kfJFRQrTHybrIVPJKeCLSW8Bt
n7GlVJYWEAS+HjINa6Zrn4hWS261vJ0LEZ6vqTo+HHhK/ePVLlBzeHrMGAoemgVTzEMLz4SWgHzK
JFh4tlqO1F1gk8Lkcfd9czs7EnJhYmpCRjLT9ccFjLbyd2m51ujHnRELPZOKzITmoLM8P9qFthD6
/9vNhMV+bl9hyeyHXtYlCqfBEQLbEeVx37fX8JWFJxIN0Bl8I7ck1vlczPl8ZltWpTI9cY+K2OCQ
KPbEAicHNs7jsK+PMCex8nQ0c0YsOE9zvloGjWs5cnclG2tH65bIuRk/XHcD4cDuNND9MWvgm7TV
4biUNDKKDKXMSb1hpcbv1Xj8zEZNH6IZy/jd/6ndklT8cYVR3x/mV1JJDSjZEpcFw4npeYm9j7AD
RgwtQhPmyh/Q2IJt32PVVCdppdLtX7LLEyUBRbLE2CtqGdQz7TW0D1stjCyjK0wo6p5Y/3gGj2JM
bkffptAmfMoj2w86KFuQY6e9CD5KAJiR0uAooKpTBfhDz5eGGjZfjs7As3gsIczPnSDHG0P8l2pZ
FjnK65JrOPRAYfs5Nkan0Zw/LD3GuTklAK+3w+kqRlzvtlQoM3aAhU+EXfQAvzO4kgwfhOSpflLm
cbiHPVwf+EI8vBZfsxyaYsxQcz2Luo0IwB1+9BkqiKL+v9w9WCM5I5+CwA/X3iV7HNtYF4lw/itF
+FL1Y9cfreI1KK+ZWyC+eqaWY2Ol9uxDcRDfZ6gD5TACneulitBSys38jnv24t9/magckwg2DBq3
Psiu29jjF0UDQpPSGnK0S00gWDmajrBiuRYtVlAz5DDelvMXW0WJOUiOxYc7/IEikGV/m50TPPhp
Hurui+n8DbYQ7rbQzBrK0Q42s14v6dl1k58XpA+okOtQm8tmp4ayux00yFBp4C1LVfPGf67UJuMo
Jt8FMA8baxQor8p6yMvs5b1pqQ1gip/5/aNP5PKRzoAhUeakMv9pBays3vN/ywtFZu62tODi9udh
qEB8zc0hbuEJTQfEMcLv3yTyJWFwm77pTfEj6DUUSshXcWoV5OaxdNnsR8qMWr5pxiTbjwXxoTIB
G4Y7HVq494nBDZ07gMOPoZdUhyrjyxABqonQBgLx/wvDBgGNU9PtCQ0BCRkwbxWg8Cni6xWzOVOv
gsYW6q63lhfU4TVQ6ObnTpT+7hx94odlpIhgJGBmSqHvguZ8bnhflaUaC5F60LfOE8pn7g3ZXWPu
rO1gIuWn5emQUGEwV8Fq/LDikiTVOG9zwcBFj18Y/KSutMflEkZLpRt9Bmp2yqHUEhJmOJXmBeU9
RQ1p4uQeAJrrKl4/ZSd2bS4iIkJdviE1DCVVR45QgNZibaDH5EMKZNf0mncO04fPieOJmSWm6Svt
FD/r4nLzy/ggHsFTHknTRnZ/EE9LtoRTxGt/9eBohYa02eJdoFNsHjqltH69TxYEYJA6cLrRwWKa
csFxoQSFjugzE5Lu4wrABUBA0/CEK2dqJXSN+z5j1sLMJr52RGGJzKquYzB17aKvAGP1M/sdSRnB
CcjBX9f/p1hcwAVUBqdBDWykMd5Twh4ZR3K+wDwK4f/VPVSIsrKhdO0WBo14yZxMuI55XiQbzB5b
6EDUFAMz1lyolFfEAOX6CK2M2mlMEjkiaLxmpUZgQtXYTuO98hDPjAHPYzQu1MfVFh3Qx4GKcmhc
fczHvU5McSfZxWG1nD8g5Glq6AKzpynKmIhVSw1AXX+XmKYP1t2hQwUC3wWHpZ79LbIhbZWrE0NA
XsCsgK48jDqKsDlBpbMVHlhb899Ev/UQ8dPYk0rH9ptgeUkYNLcj10Rh5D+RoXuYR0hj38odpLwY
VukdodzA083t1EbpjWigNp54o/12bgQS56coQVPvTAk/IfjKV8p4OZNoYuR0R9fj70rhuFPhrDZQ
n4STzRcH5WMI+f07QOIOJ+9S21lbLBo+jC/SgCViclxNIWkPL+od+hOBWfF+auJQKcGAmOboF093
OeazZiJCYiLS5ODao53x4KhkviFQ+rLXY5iSQ4ujXU9HdzFH3bf1TCDNNdp/Vl+u6fXf1NvniJH5
isQraAE+6iKmCbCiOGw7gGpE+N3ZnxbXSC0PqrdHxdhSPVTHfNg8iIvUP6KEnxBRIN2ICqerAnNR
T05vAm7fDFlPUD7iAL7vip2y1lvcyBzZg3+gTjJALtTwhpXbsw1thEVI4wiAmu55QFoePgU/4CKz
1bQ6IT8138tV9hyCHR1aC+t2mI+BS1N94iQLz3+ENJm5205kZmTfjdPkXTFrWd08wLAOWUdcw5qw
TGqIFamPigXep+2CJA+N2BZ8AXVHoQmN6JVslgAMAu0dsGLdgWRvcoz6NgsCQ3Cj/gXW8VLCn6uK
qpHHk4VM8Nh/YcJiHUxs79PzMNDoIuUiFHVg8d84zU1P6jqvXy+wcwU7jr8Culb/QHZf1JSaOnCY
iTQJjMEQ75F2yV9s+/0wuV3/BWdlyo6fWKS1nM44Gjp6+CIcN3Is1er897KjJ3TJjaumoJq/Bpes
h8VOFuEBcQThoCMBYdMtLjrVZaNKaq65hDlpCJCySfVI6idezlSVxop+pqqXnrxPOpoHeJFZOmVu
wQixxjij6Yc3yz13+x1Tde4osgYH1XqSvDnPNbTVi3t4sKX4MBY+XP/zpDnmRW9GLA/a1ondoVGR
KBGXTNeDj2nAZ4YvBr2Q2aZ0nDBuFfCOfYCTOMsA3xOGhqVbX0XQhY71KNZGDXhZnR7aN+JKRucX
vfFBaJ9FaJaKIi4hfFrkSrFa6B1uFaHUaII11HBA+GGl7PHxWZYswqnuDb4ETDo5UvK+9GElDxNe
EYmKcCj2L611BjkcYfxO9pfkFB6U0V4yTpQyZ5JgWMt2zkfK9UYT0fCREl73xH49tNXu/iT8s8O6
hXlmzlCDacKA6Cbqa+JxB5bg7k5nXNv2DjbZyxszM4dlxy9uQD8CgZ5l2fKeAV4zGoNAEcEb/D4Y
jPJBsmrTLEf5ip/dZzzJ+Z53Ga2MBEVlL69FKretHXzxjcBGWUZqWbuJo9cJpsVkWaXIrdTxcx+M
T9oLPIMhNOvM1bi/aq9zHweGXniNTuK/9w10g8voLR0pNGQq+j9jcLA0kjGDTKstRGWTSFSjPOOB
bc0ct8QTwUUoFn9oIZ8wwpE6JjfJrWLXU2it+5dvdCNxZ2BhZW7EUoUEt+8kkC5hrmHBEH8/NIXY
loHLQm1JgRvvCBF28Jw6pUDH4YSy4gn9+SYYQGJqtY4WjdiBqFtFgIqoaicH6eO2Hx23GVVKZIL5
Hhw9TwJ6J8tZ7v8pP4ZiGC12hhs244ovGBQndVynxJxkVMsLL+l2TVArDcNw4LHJDIFKgCTuPqGN
IM+NWaN087Kp8giFOyoG/N/ckezb9WIbvbypOGWiriuVrmBVfjT1V7ldnP0a8FCLIdlAHVHhjUFw
fJ74mZNknuwnCCFK1Y2W5JYLPFzKjrgi+SUKLOCweNs2UhapwckPiRgECtZKKESwMZE1u2DGKwEZ
nz/QKrVhlD++BExWIGhCigN6ELNWsaGy8Zavbo6BBRWSfJ2TCKJ51+GsT3NglL7ZoXii5sjOdHwG
H+09jUsKP/w5iB5w+zW1izI8s7Twpq1EfIy678VvVGA1CoCv25vY1lHdCAPUFWQcVUkLMjP6h/Rd
dmxZjZgpeFNYG3OfIxJlwQcCFThWhczGNnESC2//c+Kbsnjm3VX+XbxAX2j/ufSCfD6ZqlWLOVzY
whvyezQc3uPmP91qJKwicHEfPx75Ayvz9ztai/WYOE+Ck9BuhcZIsXoZSMbchj1GK9m7n2T4ZUMv
8Y7mlMMg7d3s13iFwCOpJ78pPQqxzileYatahqwuMzibzWPYKnayUbXJHCSdQhDO6BrO+CDfYCvF
qIF5psHKFkgcbSR9oyadsHdjFdtpg+zjfzhKOjRQPSOFwU518R94tv7OFf+MzzvQmJfkhcaR5R9X
vq31gq9LXqPLlGs47aVoWT84hG/DL8jfM+ehVNdGaRDmVYzhaBIAx44l/jf71735x+reqRPay4dL
Mjo1IEmqChUivVIW2zNdkUbzu4arUqYY63VNrZ34R2xh4sYJNl9W1DxIk5mFn9gEnmxyEpYoqfBs
yg3sTFLnRJCtIqbW1JlQoshSxk/Dr/uJNeWEk8uaeykYw7gu+NBpE7Ew/nv1VSDDk+6a79AL2NOO
6hKcELz1d35Ds6CGBdB7DzCrFta60uk9Gf7nOa8OcoJODTrvsv0zDlYt99gsd4GVQ6AEtZoXVw7l
9WYNoiK79VY974tqFU+ohAdZwQ1vXrVVfagI036YgLeAty3dIZJHBN7LMBCVPbsz0iqPH9f72vpP
lxKwe+UZYmtJRcxrLFxTdF4x4k6gq+0rlmBWkycvZRGBA8YI+2Fj/OKnJszkEi8+Js5pJI1cFXqt
cv3WaAbJgwtSEVAB9kfbvPjfiww1P3JDST7/+C6mv4obT82SKOkyh69zEqJyceE/pHGmw7iTus3o
5i6Nej92YnFkXKRAGClFAnc01NPm/kASGB1MXrinE58Po/EEVAzEgtn/BeS8rXp0pM82GuqkcY1z
jccrfV0+U0bbNjdqPoZ/oM8ppi8JphcU6EGE2VEpgHy64CNX7eWIPzskOlk2dbX7S5gYlJelMT3T
htcNSPOJIkq9aBa2EgxMmTVnE6f7Kk8PnceE9TQ1LuQS8tOixiP2eNkO/NvTHaPDQczWm07oWnmr
hBStHnJbnxPZouOrYCKLMTCgh9WeFatuzZJoc19meekaxmZWFj/ushrLP7T3heUqiJxYeootqE44
NUUHrpHcDuRPrGl98FEzudi7unI86AMWNB/0UQM+Hk5a0sNp1MnFCC7Ifqn9ntlpgLng9krvyI2C
2qT/V28wltGvl3FbY/NEghedmaS07g7ySoj6sqVtpRQDm0tWRwNEMDilUIo/yP5KF99zrpMbH5qC
nrKDzR1+hxcH9+vOh9obJ71xGU5udjWHpxLjq/8m5rUhWaVDAXwr0a9iHxfY7tmod2t1Y/y+SfaQ
syiQntH0fMKp1dIIY21YTCjE4OjnnMh76I0xXTTzGJRyKqtnZre3FoK1uLa9URkoJ8hK/l3oo012
W+PUAAFV4Mxup8NYgticOuXeeTd/ezKhJxXRn+hLRLAeAX+08jDJJc6CimBhKE3K4AUm8IY3lwWm
J7Prz+IqJKt2cWIBCXsHHN8OBni7afRHs8MhrNP6dM64Z/Qq384KusNNF7w0W/7L08NHwZgSdOz6
YQhF6qWQS8ppVFAI5kD1kYodR8UHjbJrlCBX1EJ+eMhcKGG8qoxR+eunaNAX3whKwOPppduvR6MR
7akdJ5mZQhl0ldn8PHqTkPPahXj7w9SxmN9b48qe7gILpiioHmVKcq4LXtbjUatsMse5Rhsmo18N
bxPLPnl9Lij0Vvh/11/QyoKwCflXiR+AL8TMWtNZNKxpoCuqCS4H23bBaG4nAhGOmuYOtPLwLWum
lwUgHkCH4s9c4aJHsOSmxG7sfhh92Mng5Q5+FUFTDjY+Fzwi/HoKuaYkJLp+iPF0CC3vx3Lm337A
tTiBrFh48RWneLE8n64lXmwfmVtkO4eAuzVHegP7V4poJ6Y6ie+pPPDHK2d7IUQWYss+TEpYurzL
R+ocTErDsdeH017Ny9bF+JD30y/F07eqDvuyKksjVuRuVfQs7L7WCeiQG1y2ZA8HkW/i3xVJoy9H
ZtkKho0UKezmQPdtmiAM1HO4iHhwN8NIKIj4Cy/SehFIUnaXrUsHssQ0jmfPUh3i+zGy62sUl0dP
ZEtnPqY7Oocmeb3Pp/cVfx6J9vosrI4m8nsqFfZMKKE6uKufUrp/vshrXvmJGFhVFZ15HuGh7u/c
o1KgpQuGI3mY9aPWuC2bBNLGORlkPQXQolYaOqEAmqwjE6c7AwlBygxLtC8L5gISfUVSZ/jH0FJm
/QPBFk2VAFIHT4/PffteJCiGEECdotPIaQGQQvfecbuAK7oUKgZXBG8GdX0TKbrI+qj3x1OzBgD/
zPU4iVANO0Mibz5LdULuHRaVBpp+OtRJpjwayNs8WUN/tlUnQUaTL0RhMaAocHrruajOc3b4ZhFm
jRwlJ3QPejYfE1KQvueheLkrgmM9mM+ho+F3QKbod1nuPjhVjOMMUZ6KhZRPbugsh9uM0s6unsni
9RLorGawqdp6ITo4h6zPVROfhOaP5ZSe/zl4rP8LjwLQBKm6j94Uk6oBgcxxu+jvPg3izsQcoswl
lrVrlc9D8pHVUtUE9sJlbcDyybRpB3uhuAZIftHb2Qs6mPnksyIlA8LDTwKFY1PG7qGueRw+Mz3+
iflcbyfLxDcKQj700dO2YGeMf2mLGei5RmEuOkYw1ttwUWjmElpwYL8Qolm8zWWBQ4niKuTYaRTY
ynJM2ofq4Zwd7+56cD1DSB+E+Lq7XDTIx+uwFXTJh71wYTIfovtCPehYWC5k9Ua1popfaPD5ypez
ddEGf7ofOtMt6oQ9AXm4MM/qg0Gq2qNbV+PHj7yv3MJ7sRVLaK8uRD/oFICTCXc23LFuxsYsTyC/
qcnm2xZbAWZyJGKPaHoNtCDqRAFBpJJHs2un1y+wCX3RRDDk170sAej7+GD+3fwyGyqs6tzzKtrV
OOLTpC/0e9ltjof0VhxEVP+LGWv8TOV+7aIV0+RcOYpc7uJWETjZW132D+jdoF/xG7qTm/kJrAn/
WZXnoG78U1SzvjO4BG9djGY0dKcKYcw9eIwRqnHsc/kHl+4Q+fgJp92uMVwp1S18F65aNfrXykZ+
eOOTdyjoke/3Gmtw/EHjO0VlhNtpR7bfz2PBFy9VPGFppUZZoq1VOivWaLZJVKjuS68Ll6XPHKZA
d0UlDxrW/bitmSLbJyGNsjBVE7d0zW3WCQY2wfbW4UjMffJsPz2MaogUc1v9vPO4G7pUbFQwBRyg
0o89MhiTlzLEy7JR682GH6qWAgAIofYU+9xp6UTEXAv9rgLWQ58AGbqznspAsDPzp6YxJZGBwgWe
WbybpZyT1uDWoeuYAYYquf49YSqbzAqas9jI4lUq4nrn3gSz4eRfg2dZAktt9KIvresH6aT4Mqrm
K3m0RMkZjLS2oDA9YZ1yLU7crdhdmgRFIgS4pMoNTrt8XrH3QEpy6gOrAgsZPw09kUc4AT09fsKA
VF9RwOcLkJJmTcjhUhWkaNlMuL1nyNrvwH6sEn1XPuzcYblGR1mxkGhMOzN12KBqlViWQx9IyXPq
VrLFEqDQOrgHON4iF8TFtjRfmFVsTAfNfx/pE/6Ps0nEdioScHaEwsRFGGP4sce7e78nxgUT2G2N
iVYp0lPNWpWzoes2zbSTrK4/ZCkJaxafyP4q05Jtk29oMI7lJGiaNJwnDAOAaeGinctrazAw71cm
yHWaOEqcwPt/QKLWbvaCsrokOOD1XsoWlga10M+fqyeSxnncsE3Tbbz7VUNZ9LUgrSBQt+eWj3nI
UUTKg33T9o1nAsA1dHKAKG0LHd+3AgvG5KOA3XMPRKZQOumA+1U3ehNX3gLXdeNb0LcUwS7r45RI
PzSLJNESa20jUu1TwYgUvNASJWP5IgUOzBFQizUrEDceURVjpXn4X8zBBYvo1/AcpmgiMKa5RXQu
WGH2HkbygRY0M7r777C+K8zhVGzKv5UNHBxn2228nU5b6rWVmo0FyYgEsax2vT34F7BudUXLpmtn
fz/pHxNdpQ8bVUjUzUWEhWOI15xF1PRhaNNWOUEot7QRcli21IyS8FZqrINCacpHFB1+7gcd8Wdj
k5G7mKrFs2RzL1S3nSWTu+iUNNbAjGM+6TvhHiWpqu//2Btc8QHB5rjx8ZVT4cWHlWUSURY/x7wd
uIDKC7D/1AcMR3iErAv+zqYdQJ2me+Q5z3VhCD1+kyb7GgHzCxmnhYL2Af6YFUVeuKAsuqmC40W8
/0MrxM+qZfRLrZJvtAfbMNL/vbv+NBAZAIb6ByIxZjD6L0cvKzJvT9dLPbU7IrpkAbSQOYnIQwdG
BX+mkfhiFsfn2RcLzx9JXy9YvTc7gKgy9HN8v1LxnjCKSfBvINzWQ4+Fm06O1YfjDVyn8/xdMqbT
/9rBJ5NDSTNr7pAj7N8iDp9ol4Ezx5hG5+riZl1PIREFX65h4623Ji2xuzGHoU3wf/m8lqpUXcv2
u2uGnDYZA1sZa+Gr2IMCfcf2jibx3QcRtigKRVkXPW0b3M98JUnNcljB+YIu/ZtW5ZP42y+nLrKT
mcJuA+VznX0VBsgEyFtod/RS5qHbLrQbY0hEMk9d952Bj0xzSyVLxQCbBhiOlBSZgIfZ9k8FxQ5o
QqcsjLLbqQyMTxbl8FD/jFeQK5wg/pbjn0wa+f94eJ99NLJzJ/k00+hHOx3CYHn8ry0zhK1xdFbI
Ee+/HAnWrOh3tLeGm9phUvVFAJqEQpTENz9w83unhwFaH+VdVeGBOakV9jk+va3N9r/MvMQ1Hem0
QCuC84+oiOdpjuvQdxHHwlfX2bTCMFSE+dpxlzsGdGuFZq1EG3JsVwFVQ7o/74cu2MmJp11Y/mVA
dTY5gBhFmx1erPP7b9otm0ZA2i69crxFB0bWCH1bOiS90IePc2soK5yYgrYhhpkDSisIivw3Ak0s
aQNouSDEB7YssdjpYuswJVRS5iks+egXs+8e8UJtRo3EGR3q1eWKIvfUsZ2G92x8AuAwhV3jhnw0
vWoF0UrBObgLCiw4pQ9QR8ed++bLcHCg6j2r/Q9prWUFqX6ndvVKBUWE3VV8YCRy8D+MmHKWTS+p
XE8hGuXBkzA+tW9HaW00bSOodFPZMfPQ9jB3nE0EBoHNxAoiJphxRbPOmbgF0A7WuPqKAVTCGT68
MWejUCqaQGiwPjdcQ2FZdBpyljZKj/qouHUtDcBGUfjXoEpj4lY95eL9dQi7pH02CyNBmrypZZDH
Jr4FpGnfL1LInz2q2Px2NnULISli4a2t9JVVTw8cB2qdtkWsy9pUUpQXofjFc036wSNZJQsK6YFv
Msi+d5sOdWIIPwi8UPDKuz2LpDM4A16KJFEyEcABlRiR7wTQDTdxtO6lXmXqwUEuUqVF3MnbAYaD
grR9T5AWES9jmP16TxXI1oCl6GBVortNoh2wqJCu4EeGBagq4WVeCfQIDjkmlSWQ2tRe1o1P4Tw2
nmqnLMayqDQFjPUncExguNykCpaaaGKA7n2mDQS3o7zDVkdUH3vrYP9Lpwz9LDX+eT5Z/Bl9jsia
RrPUmNAHEBYg37j/+fqygkdBlJi1ErpxROi+ST806FwR3oiP+1GCQq/+SjDU8ghfmbAOwCfSxJ8x
TF2M7UfeJ9kIAttTT4fPnMthpK3waTmuH+1WuMEavBarD/4wK7n8fV2JeqIYQajPRCvRDaptvGpj
L3Ph9hK8bwrfZqWcs5qQoa0e8gZOMY7It0YcqM3cRKrzsclJyzZviM9Oi29+p2dHXNcmmKMyOIJn
VJJ2nZvJT0sSmhmqlo5F6og6ri5xmNXxzU+Vdj+EzRROt3PSTvxlbdFlwrFCbd8FnbCkw1Ufanjz
OaZfSnDVC0dpPEoVOgQL67sOzczy+AvtOU3pYBIaq1klV1wMBSHRWRGUTfgAK/PKui4y0d7C6Xr6
7LDz3jH6YYdR4ecm7SWTbMexXoJcV8s/PTDCRKp2mN6Br348MxGWBPlZrEniL6ThMDPoElwmMoX0
4dJzqk/5MHoow+Wo/XrOASDLSBfs6JCRVCEqJOKu5y5/NFCWkwBX+d0oFp+iFNG6xWtWXCypdAub
HCDeO9cZXmhpf36W3UFLVbyEmE7ixZYjzDF17EqfQlK7oKzonQdxrxrxe9N2PDGQUyYqOju7SEdn
iaFUgicyC6YyIfOGH+nqky2qY3FYahM/leie/fG/qf7ps2Wp22Fd7+wuLRukj3j9jDTNLBGxZkjH
crTfNlMdv7KqI1flxW+sU9UczFdyy/3aJS3BlB8UUHf6SAzAO+Q3q5107ChVoA/vyYMPQ7cfIWZ5
lgk8DYsxE69vDYsuAidgnn667zES2sUktec2gF6FUrQdR/YyeepD94aIbbYDP5B/JTKDTr2Lu65y
fUV4fJkpMDFFIO1CVuJWYpDhYoshtNb4/xn7/V9fXjmF4FUzInt/ZmH60EbUoL9YuOB9YnKmRZQX
O1hN7gXVXwYnCtX8FV5GzeDSnvECjEdCMe3OAeqIpWOv5wvP9zeFVjJRfdJ35t0fv8r97WyKlgNH
YNBeXaUiwlBt4lcyXvgBZWaGZpBsW+6vG02r6E7ZHMKQNcPzXzI9+8aoQ1Jv8Djd/jrgtbm49pMh
Y8ezjsYBncWTrlZV2m6j9u57Ey24ZKNGngj7ybQiqcWWdHl9RluOiJDMLD3BbKCf1zCcBzcQSU4r
PMTlWG+Kd6lhd0vyUnJnCCzLmeX3fdAU4U5UfDUyMn3K7opKW3+ylDQwS1PivM2/G/Itnhd+g4TC
bspW+nv+o+vWwhZ5MI76BNnFUR9VgacFjmNf39t5avRkQo/TIEFW62SMXDwp2GCPapne616NE27p
ITcYqQetU1f7jCT5MnHogN1RP6VeOD6exLILzUuGL/MQk0FiuVkMANmNXG/d7o3MrvjJZtMmqkMB
7MXfjvkUHXRfyyAu4OigKcULF2VoKG8pn64BH6aiMuUPx13pHa8OaLVikLo1jhYxuWvXEVW08r0j
iQISwjeZxd/2ac1IGzrj3JlE4TmJildUmFfVY/qK5v8uIBw7wwsde/Z9hd1+MJhMukCE6yImgY0w
xqZWmCykddJYYvhUn/CLq13DCBuolYiwloCEapKSSA4cfbJ9ilge5dsDFTLuzNa/2UVZMefZo6tv
3wYcVCFGy5DbZV8qanx0tjB/k1th8SM3yI/j/ipbFsqjjHhCZlybFO9DuPBFDrPCyPN22K1zH/g8
hA5jYu0PV0jzfckgHto4B0h48M0fzWXrM5V1Y+VA1WtiND/K3grE8FjsdsIGG3/0zoMKahXe+5H5
ubySDi1/r07ORuzHxQxhzGZ32IP+rGU/zsO6AYcbSBX7VxV8L0dG5Kd4EimhHlqHWvSw1/MmjAB2
KIGe6G5ovT8nZTj4J5Zgf0A/zQ14RetsJqGowII5XaoVAz3vci1bJqcXRuatwDfvyxE5x6qEOCuv
DIqiTcFJ9tEOl5Ypn3y9GdX/o1wtGu2YJOui1XGFDyAdjDRJo3PpiTtbVtg36WUZmaVWT0BTprEk
A3uMAYBzqhvHpynvTSg+atBjgt2IP9f32hx9BgT4egt27mNr93IQPSFPIyhnvt7xIMo0k3VlxKkY
tJYei31prM0DrrkaNlYhRDW5nlOLolAgQ23lv75W9wcqjS3f1Q3x9IAvYLj2HFAwprtl4qi/f0v6
52p4IVOs83Szjx4B1ApofRKhvXJTTxrXJNGZSp41mBOjcTk37UCWAgljBgRV0Gvsfv/pX2fpKEem
qGQtVUqAtqIqOx2CpBYKZIUUEF9eZ5BdXrT8xHjimuD5Ij54pGxuREw4kPaY9/UDhzu8OOs4R2yE
A9qohvlELzS9lh0E5MJS2z3g2AeSXO2Im2xNfGDuoeYZyiAaNqahuJeI1z+jOAimQXznxTic2V5h
RHN4/mHbXDZyPqDzaCCvGywxLO6CIMNhS4gVEdDNsuUGljKmW8GjixbaeTBWrucWNZaBxaB/d5ri
V6cqmAm/2TdVv7nSbJWx5VvBDz5fCuudk0mlo77pSVUVuBs3dzoBQqrXYL7HPLTGlVLUjN/zxUjx
KNDiEu7IzcwWwE1O3wL1kwnn8ajv38IopF95LT5EoMDNH9/66vaZvva5eu35nHbL7qFxAwtpCuFF
ObzLhsqgAKcZ9dhdFZmFYPB7kXVEJAn1qHFGkBT6JvoboKLJXx3RHjHch2bjTsyDs2VFuFrtJPmd
T9Rz78mNWrl4WkZRXrn551nnxH94xX5sLJB9amtCCdXYPhxnuPXF8iQdJydA4CzfyN380PP6gyCk
uID6mlN4osfYob3E+uJ7iqrUxL4JVwOuGOpIqRRI8HwsFfxx644N50BzqSYQrkOyLyfAgzUcn9KP
O8pOXuYBSZNpbnA9ONsdUdMixIdw0Zk845cgEv/+CNt6jv05dtdGWBdOdMQ0n760wqy98Qx2bkjw
ZSZX+/Au+rcJhRJ6KLNKl+CQG+pp5yg2x4J3JyTrRxCU2oGAL5+K3O32sZla0twcflMEXxJRa016
eROZrrdRS+5BBG4CnnIndo/uqcDT3a6BkVIgosdx8Awk1BA/jb7p0+g3DYhdpT+NjcvnKv/zcWrd
YwKqTUcOuUiT9+bkiXqSj+0bzdxlW/Nca48rxKA38tL/ciN4sRDcIRfoJvc8DjlzmEfTVFRF+AP/
3j2tEPbIO9QHSvPOPasfVn/6KVTTdLk2brYgvZ4u0/giOz8+k1ARWmoDB3H+rArsnElzwwzNp2TI
fHAi3OZTqYlbTHUvazyf7ysjWMgQG3a9wyq65JBImascI9VMWEvl7iG95CXPMH90HTd3ZQEIS5eb
VEkTRCT0dFBoUzwhfDv1KSMC1SuQ9TyVmjWFWCjz1NOtfwz7pbKCCn4WMU8nyJSKwAVIuLh8d1U0
ZxERLpiExvE1GGENXEaib2MoD9CWy5YxwdW8hTIm1sQ0NDKCeMZWRvGuqMyw7+QWWa9cJxScrQ+T
PWH2eNjId9owiH1Mi8I1lzeKt7c7GonrzKGhaNJODe1a3/9BSWcaRv0n7zTBcgAtVffWJvhzLUtR
Is4m/eDxnT2FsyWfcJKIZPn5mObwMm+8IZyk8KNMU62lnTBqpMr57ZOfp9Kx2ZiW0gp1W2cLDOlY
nuOj7ZXWV8ZJ+qEM//XTBJYcUSRuEnfskZAfWLwczB7fDBeK20dzt2DO1SkURD7fLcQ89kWeMYJO
VG3eJ38GE9BNa9nCL+Tn7cDbUe5sc6W4VaDdONTEOFJub117Kq+yWYUHn1uLiGTTHsoewdtIGQGq
+z8pLxAPitJzZy/Aoj0hj90yOWZ9Yf+ynPKNXRPfMVa2ZPNjEYXKyRUnHLYMENXT2Ji654iBYpl6
N8kSdvmWJK4UnW2i7lADa06oVjbWJis0Je9fJS32vkk8+isSnZnYLRlZ6K3dPnp6aX+Dxu2uWT9z
gh2va7Pzyefqi9jYVKaw4kTBdJkz3o2ERrHWR+LxL0SVUst6NmGNrrx36tkks8kQaj7C9vPLZ/lZ
XHFTvXbTc8JI2USB5IwSmnH1uVmbcl8BJhOX2+T2tgpG/NeEO+R6vklr6tr6WizaSiUQCQDuOz94
QNoVp01HZj5oIYZ/ryKIJGQdmPZBlrJGIlftzL6E+XxYUipDBGhZxt+etTQwjx7NSwBoRfBX57+Q
hkhJLYQ/TXoKHwQBQszXAp72zYpd/wpIXFtmNBcQCMbBHtsYx5kB9VSisu9wRnsGzfSy41W0W9Nt
ZA6R17RF8OycXuLexwHrRiBqlt+Q/A8gnMCww72fnUOPrhFr2QLrvMvoo5zo+1hUhfrnE4jPpxLb
GAgnDcRkmR7q4X68t0zAYUlhwfn1IM7osoW1wPYqRzKIVxdIfCDKUgRVD6wq1lE68ybnFC4WY2e+
oEO2IyCdkMu9WWnfwbiyHBEANydPvV+fCR3hdFOZ9RZB2cYFNf8CZLL0Ovb+fm3r1zEa3oZCsqAF
0IAKv3Vxuss2lL1SAWLu6KV4DsodoIH2K3NS551z0a5xQR14qrN26MW5UaGWp0oZhzoceBT49KZf
5RRNEayjIxsccpqXN6md4Rhj3nAzVgfTx9d1lYNNogK5r6HpqA60B3L37zZPDsqFpRrlYNbLulLm
Y15YraTry5f1P15PD/r9DRJbDcbb+K0reFqBp5eOcL+YQihqI5QGGrfCjK47ZtSpjOziKwaKZ6oQ
bpEht6PFQsZ69bj+X96j8Qo8dmSDRZvGJqloK7s7SVU/8ArBY6CSbO9K/ceMNgXvAtf6AO20rmMn
39W2wpQM9kJr0PAgnXIJuX4t5sT5QHPu1nzNW2+bn/qOhpCthM6Ula9Qbo3k981m11LJ3/3C6+ss
EaxHBuCkv75kTQz94JCrEmg5lQZjQWposUzYfB1eEG4Az8XU7b7syqzXHImah8o6HOSWEcI2But/
J64kBL1wPJKoG+3bh+BnMKSKDUP98tvKHdF6C5T28grZbvzUapoGXJgW8nAXTlw6nTiOmnJT85H/
WLnLEOoBC7FT3XyCVYadz7Ba7ImsGJTMlFPw7oiVzMxpaDoI3P0q9Tryqu0IzTUJKvh1/PB57NLt
uHpoelPeoXc4n4CY/QcQ3pB48bOZc+8SwX3SL2Lcq74WJ65tUYF4liGPqTI1saApQZ1rtkHJQDXq
RP9UlcsVRw9+xb000Mt2y2LjCqTzfqfeEH6uynI8qbEBaTZYYH2/xAPvY6bGqAocM5GDN5GpqqO+
mevO7N/eGGTTEyasJfScaXp0tAudDpHzjl7cWZ88dZJ6w7ajfkC9UHTlckQaKipJQavi9rZ3Qiqp
mEmzE4bgzP6reWyiiOKTBQwIv/f5Bj/o+LDtQTqDeNMxavZVgX5PgM6f3sPREwoh2Pj+zs9d6vvz
/SfJVptc7Vc7pkJZCBuvjxC9ELSYwxOZQJoZp7xYnE+V4iyfS3vWCM7ewszbtul1eQSOClbo+4j+
UCAfY2atVFOtzWT/k2Wdx1C9tC5HyKzVtXycHM388UlCT9Qp9RHEDg5VxWjJ4/HS9YBexUwdJAcV
hu2JIpS7wAj06oUK2Rk29Tfzx33+QXt70qgis1c=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
