// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Tue Apr 13 19:47:13 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.runs/vio_adc_readout_pattern_test_synth_1/vio_adc_readout_pattern_test_stub.v
// Design      : vio_adc_readout_pattern_test
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module vio_adc_readout_pattern_test(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_in5, probe_in6, probe_in7, probe_in8, probe_in9, probe_in10, probe_in11, 
  probe_in12, probe_in13, probe_in14, probe_in15, probe_in16, probe_in17, probe_in18, probe_in19, 
  probe_in20, probe_in21, probe_in22, probe_in23, probe_in24, probe_in25, probe_in26, probe_in27, 
  probe_in28, probe_in29, probe_in30, probe_in31, probe_in32, probe_in33, probe_in34, probe_in35, 
  probe_in36, probe_in37, probe_in38, probe_in39, probe_in40, probe_in41, probe_in42, probe_in43, 
  probe_in44, probe_in45, probe_in46, probe_in47, probe_in48, probe_in49, probe_in50, probe_in51, 
  probe_in52, probe_in53, probe_out0, probe_out1, probe_out2, probe_out3, probe_out4, probe_out5, 
  probe_out6, probe_out7, probe_out8, probe_out9, probe_out10, probe_out11, probe_out12, 
  probe_out13)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[13:0],probe_in1[13:0],probe_in2[13:0],probe_in3[13:0],probe_in4[13:0],probe_in5[13:0],probe_in6[13:0],probe_in7[13:0],probe_in8[13:0],probe_in9[13:0],probe_in10[13:0],probe_in11[13:0],probe_in12[13:0],probe_in13[13:0],probe_in14[13:0],probe_in15[13:0],probe_in16[13:0],probe_in17[13:0],probe_in18[47:0],probe_in19[47:0],probe_in20[47:0],probe_in21[47:0],probe_in22[47:0],probe_in23[47:0],probe_in24[47:0],probe_in25[47:0],probe_in26[47:0],probe_in27[47:0],probe_in28[47:0],probe_in29[47:0],probe_in30[47:0],probe_in31[47:0],probe_in32[47:0],probe_in33[47:0],probe_in34[47:0],probe_in35[47:0],probe_in36[47:0],probe_in37[47:0],probe_in38[47:0],probe_in39[47:0],probe_in40[47:0],probe_in41[47:0],probe_in42[47:0],probe_in43[47:0],probe_in44[47:0],probe_in45[47:0],probe_in46[47:0],probe_in47[47:0],probe_in48[47:0],probe_in49[47:0],probe_in50[47:0],probe_in51[47:0],probe_in52[47:0],probe_in53[47:0],probe_out0[0:0],probe_out1[13:0],probe_out2[13:0],probe_out3[13:0],probe_out4[13:0],probe_out5[13:0],probe_out6[13:0],probe_out7[13:0],probe_out8[13:0],probe_out9[13:0],probe_out10[13:0],probe_out11[13:0],probe_out12[13:0],probe_out13[0:0]" */;
  input clk;
  input [13:0]probe_in0;
  input [13:0]probe_in1;
  input [13:0]probe_in2;
  input [13:0]probe_in3;
  input [13:0]probe_in4;
  input [13:0]probe_in5;
  input [13:0]probe_in6;
  input [13:0]probe_in7;
  input [13:0]probe_in8;
  input [13:0]probe_in9;
  input [13:0]probe_in10;
  input [13:0]probe_in11;
  input [13:0]probe_in12;
  input [13:0]probe_in13;
  input [13:0]probe_in14;
  input [13:0]probe_in15;
  input [13:0]probe_in16;
  input [13:0]probe_in17;
  input [47:0]probe_in18;
  input [47:0]probe_in19;
  input [47:0]probe_in20;
  input [47:0]probe_in21;
  input [47:0]probe_in22;
  input [47:0]probe_in23;
  input [47:0]probe_in24;
  input [47:0]probe_in25;
  input [47:0]probe_in26;
  input [47:0]probe_in27;
  input [47:0]probe_in28;
  input [47:0]probe_in29;
  input [47:0]probe_in30;
  input [47:0]probe_in31;
  input [47:0]probe_in32;
  input [47:0]probe_in33;
  input [47:0]probe_in34;
  input [47:0]probe_in35;
  input [47:0]probe_in36;
  input [47:0]probe_in37;
  input [47:0]probe_in38;
  input [47:0]probe_in39;
  input [47:0]probe_in40;
  input [47:0]probe_in41;
  input [47:0]probe_in42;
  input [47:0]probe_in43;
  input [47:0]probe_in44;
  input [47:0]probe_in45;
  input [47:0]probe_in46;
  input [47:0]probe_in47;
  input [47:0]probe_in48;
  input [47:0]probe_in49;
  input [47:0]probe_in50;
  input [47:0]probe_in51;
  input [47:0]probe_in52;
  input [47:0]probe_in53;
  output [0:0]probe_out0;
  output [13:0]probe_out1;
  output [13:0]probe_out2;
  output [13:0]probe_out3;
  output [13:0]probe_out4;
  output [13:0]probe_out5;
  output [13:0]probe_out6;
  output [13:0]probe_out7;
  output [13:0]probe_out8;
  output [13:0]probe_out9;
  output [13:0]probe_out10;
  output [13:0]probe_out11;
  output [13:0]probe_out12;
  output [0:0]probe_out13;
endmodule
