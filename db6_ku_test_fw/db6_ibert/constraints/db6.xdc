#################################################################################--
###                                                                            ##--
### db6 constraints                                                            ##--
### Version: 1.0                                                               ##--
### Creation date: 2019-11-05                                                  ##--
### Created by: : Eduardo Valdes                                               ##--
###                                                                            ##--
### Modification date:                                                         ##--
### Modified by:                         		                               ##--
###                                                                            ##--
#################################################################################--

## bitstream setting constraints

set_property BITSTREAM.GENERAL.COMPRESS true [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 69 [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]
set_property CFGBVS gnd [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR no [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE yes [current_design]

#db side
#set_property PACKAGE_PIN AD15 [get_ports {p_db_side_in[0]}]
#set_property PACKAGE_PIN AE15 [get_ports {p_db_side_in[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {p_db_side_in[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {p_db_side_in[1]}]
set_property PACKAGE_PIN N19 [get_ports {p_db_side_in[0]}]
set_property PACKAGE_PIN M22 [get_ports {p_db_side_in[1]}]

#md number
set_property PACKAGE_PIN AF20 [get_ports {p_md_number_in[3]}]
set_property PACKAGE_PIN AF19 [get_ports {p_md_number_in[2]}]
set_property PACKAGE_PIN AC21 [get_ports {p_md_number_in[1]}]
set_property PACKAGE_PIN AB21 [get_ports {p_md_number_in[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {p_md_number_in[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {p_md_number_in[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {p_md_number_in[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {p_md_number_in[0]}]

#leds
set_property PACKAGE_PIN W11 [get_ports {p_leds_out[0]}]
set_property PACKAGE_PIN Y13 [get_ports {p_leds_out[1]}]
set_property PACKAGE_PIN AA13 [get_ports {p_leds_out[2]}]
set_property PACKAGE_PIN AE13 [get_ports {p_leds_out[3]}]

set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_leds_out[3]}]

#mgt pins
set_property PACKAGE_PIN T5 [get_ports {p_gth_refclk_gbtx_remote_in[1][n]}]
set_property PACKAGE_PIN P5 [get_ports {p_gth_refclk_gbtx_local_in[1][n]}]
set_property PACKAGE_PIN Y5 [get_ports {p_gth_refclk_gbtx_remote_in[0][n]}]
set_property PACKAGE_PIN V5 [get_ports {p_gth_refclk_gbtx_local_in[0][n]}]

set_property PACKAGE_PIN AF1 [get_ports {p_rx_sfp_in[0][n]}]
set_property PACKAGE_PIN AF5 [get_ports {p_tx_sfp_out[0][n]}]
set_property PACKAGE_PIN AC3 [get_ports {p_tx_sfp_out[1][n]}]
set_property PACKAGE_PIN AB1 [get_ports {p_rx_gbtx_tx_in[0][n]}]
set_property PACKAGE_PIN AB5 [get_ports {p_tx_gbtx_to_fpga_out[0][n]}]

set_property PACKAGE_PIN T1 [get_ports {p_commbus_gth_loopback_rx_in[0][n]}]
set_property PACKAGE_PIN AD2 [get_ports {p_commbus_gth_rx_in[0][p]}]
set_property PACKAGE_PIN AD1 [get_ports {p_commbus_gth_rx_in[0][n]}]

#sfp
set_property PACKAGE_PIN AF13 [get_ports {p_sfp_i2c_scl_inout[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_i2c_scl_inout[0]}]
set_property PACKAGE_PIN AE11 [get_ports {p_sfp_i2c_sda_inout[0]}]
set_property PACKAGE_PIN Y12 [get_ports {p_sfp_i2c_scl_inout[1]}]
set_property PACKAGE_PIN W8 [get_ports {p_sfp_i2c_sda_inout[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_i2c_scl_inout[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_i2c_sda_inout[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_i2c_sda_inout[0]}]
set_property PACKAGE_PIN AA12 [get_ports {p_sfp_abs_in[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_abs_in[1]}]
set_property PACKAGE_PIN AC11 [get_ports {p_sfp_tx_fault_in[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_tx_fault_in[0]}]
set_property PACKAGE_PIN W10 [get_ports {p_sfp_tx_fault_in[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_tx_fault_in[1]}]


#commbus_ddr
#set_property PACKAGE_PIN AB17 [get_ports {p_commbus_ddr_rx_in[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_rx_in[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_rx_in[n]}]
#set_property PACKAGE_PIN AE17 [get_ports {p_commbus_ddr_loopback_rx_in[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_loopback_rx_in[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_loopback_rx_in[n]}]
#set_property PACKAGE_PIN AC16 [get_ports {p_commbus_ddr_tx_out[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_tx_out[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_tx_out[n]}]
#set_property PACKAGE_PIN AF17 [get_ports {p_commbus_ddr_loopback_tx_out[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_loopback_tx_out[p]}]
#set_property IOSTANDARD LVDS_25 [get_ports {p_commbus_ddr_loopback_tx_out[n]}]

#set_property PACKAGE_PIN AB12 [get_ports {p_sfp_los_in[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_los_in[1]}]
#set_property PACKAGE_PIN AF15 [get_ports {p_sfp_los_in[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_los_in[0]}]
#set_property PACKAGE_PIN AF14 [get_ports {p_sfp_abs_in[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_abs_in[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {p_sfp_abs_in[1]}]


#cfgbus data inputs

#set_property PACKAGE_PIN B14 [get_ports {p_cfgbus_data_remote_in[0][p]}]
#set_property PACKAGE_PIN A13 [get_ports {p_cfgbus_data_remote_in[1][p]}]
#set_property PACKAGE_PIN B15 [get_ports {p_cfgbus_data_remote_in[2][p]}]
#set_property PACKAGE_PIN H14 [get_ports {p_cfgbus_data_remote_in[3][p]}]
#set_property PACKAGE_PIN F14 [get_ports {p_cfgbus_data_remote_in[4][p]}]
#set_property PACKAGE_PIN D13 [get_ports {p_cfgbus_data_remote_in[5][p]}]
#set_property PACKAGE_PIN C12 [get_ports {p_cfgbus_data_remote_in[6][p]}]
#set_property PACKAGE_PIN J13 [get_ports {p_cfgbus_data_remote_in[7][p]}]

#set_property PACKAGE_PIN C9 [get_ports {p_cfgbus_data_local_in[0][p]}]
#set_property PACKAGE_PIN C11 [get_ports {p_cfgbus_data_local_in[2][p]}]
#set_property PACKAGE_PIN E8 [get_ports {p_cfgbus_data_local_in[6][p]}]
#set_property PACKAGE_PIN B10 [get_ports {p_cfgbus_data_local_in[1][p]}]
#set_property PACKAGE_PIN H9 [get_ports {p_cfgbus_data_local_in[4][p]}]
#set_property PACKAGE_PIN F9 [get_ports {p_cfgbus_data_local_in[5][p]}]
#set_property PACKAGE_PIN J10 [get_ports {p_cfgbus_data_local_in[7][p]}]
#set_property PACKAGE_PIN G10 [get_ports {p_cfgbus_data_local_in[3][p]}]


#adc readout

#set_property PACKAGE_PIN H26 [get_ports {p_adc_hg_data_in[2][p]}]
#set_property PACKAGE_PIN E25 [get_ports {p_adc_lg_data_in[2][p]}]
#set_property PACKAGE_PIN L22 [get_ports {p_adc_hg_data_in[3][p]}]
#set_property PACKAGE_PIN J21 [get_ports {p_adc_lg_data_in[3][p]}]
#set_property PACKAGE_PIN P26 [get_ports {p_adc_hg_data_in[4][p]}]
#set_property PACKAGE_PIN P25 [get_ports {p_adc_lg_data_in[4][p]}]
#set_property PACKAGE_PIN W25 [get_ports {p_adc_lg_data_in[5][p]}]
#set_property PACKAGE_PIN Y25 [get_ports {p_adc_hg_data_in[5][p]}]
#set_property PACKAGE_PIN A22 [get_ports {p_adc_hg_data_in[1][p]}]
#set_property PACKAGE_PIN B20 [get_ports {p_adc_lg_data_in[1][p]}]
#set_property PACKAGE_PIN C21 [get_ports {p_adc_frameclk_in[1][p]}]
#set_property PACKAGE_PIN A17 [get_ports {p_adc_lg_data_in[0][p]}]
#set_property PACKAGE_PIN B19 [get_ports {p_adc_hg_data_in[0][p]}]
#set_property PACKAGE_PIN C17 [get_ports {p_adc_frameclk_in[0][p]}]
#set_property PACKAGE_PIN F22 [get_ports {p_adc_frameclk_in[2][p]}]
#set_property PACKAGE_PIN R22 [get_ports {p_adc_frameclk_in[4][p]}]
#set_property PACKAGE_PIN W23 [get_ports {p_adc_frameclk_in[5][p]}]
#set_property PACKAGE_PIN G21 [get_ports {p_adc_frameclk_in[3][p]}]

##mb interface
#set_property PACKAGE_PIN C26 [get_ports {p_sdata_out[q0][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_out[q0][p]}]
#set_property PACKAGE_PIN AA25 [get_ports {p_ssel_out[q1][p]}]
#set_property PACKAGE_PIN D24 [get_ports {p_ssel_out[q0][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_ssel_out[q1][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_ssel_out[q1][n]}]
#set_property IOSTANDARD LVDS [get_ports {p_ssel_out[q0][p]}]
#set_property PACKAGE_PIN B24 [get_ports {p_sdata_in[q0][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_in[q0][p]}]
#set_property PACKAGE_PIN A24 [get_ports {p_sclk_out[q0][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_sclk_out[q0][p]}]
#set_property PACKAGE_PIN AA24 [get_ports {p_sclk_out[q1][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_sclk_out[q1][p]}]
#set_property PACKAGE_PIN AB26 [get_ports {p_sdata_out[q1][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_out[q1][p]}]
#set_property PACKAGE_PIN AA22 [get_ports {p_sdata_in[q1][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_sdata_in[q1][p]}]

##cis interface
#set_property PACKAGE_PIN D14 [get_ports {p_tph_out[q0][p]}]
#set_property PACKAGE_PIN G15 [get_ports {p_tpl_out[q0][p]}]
#set_property PACKAGE_PIN J11 [get_ports {p_tph_out[q1][p]}]
#set_property PACKAGE_PIN G11 [get_ports {p_tpl_out[q1][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_tph_out[q0][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_tph_out[q0][n]}]
#set_property IOSTANDARD LVDS [get_ports {p_tph_out[q1][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_tph_out[q1][n]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_out[q1][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_out[q1][n]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_out[q0][p]}]
#set_property IOSTANDARD LVDS [get_ports {p_tpl_out[q0][n]}]


###serial id i2c interface
#set_property PACKAGE_PIN N26 [get_ports p_serial_id_scl_inout]
#set_property PACKAGE_PIN M26 [get_ports p_serial_id_sda_inout]
#set_property IOSTANDARD LVCMOS18 [get_ports p_serial_id_scl_inout]
#set_property IOSTANDARD LVCMOS18 [get_ports p_serial_id_sda_inout]

##gbtx interface
set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_rxready_in[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_rxready_in[1]}]
set_property PACKAGE_PIN M24 [get_ports {p_gbtx_rxready_in[1]}]
set_property PACKAGE_PIN J20 [get_ports {p_gbtx_rxready_in[0]}]
set_property PACKAGE_PIN K25 [get_ports {p_gbtx_datavalid_in[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_datavalid_in[0]}]
set_property PACKAGE_PIN F24 [get_ports {p_gbtx_datavalid_in[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_datavalid_in[1]}]

##set_property PACKAGE_PIN AC12 [get_ports p_gbtx_i2c_scl_inout]
##set_property PACKAGE_PIN AC11 [get_ports p_gbtx_i2c_sda_inout]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_i2c_scl_inout[1]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_i2c_scl_inout[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_i2c_sda_inout[1]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {p_gbtx_i2c_sda_inout[0]}]
#set_property PACKAGE_PIN N23 [get_ports {p_gbtx_i2c_scl_inout[0]}]
#set_property PACKAGE_PIN F25 [get_ports {p_gbtx_i2c_scl_inout[1]}]
#set_property PACKAGE_PIN N24 [get_ports {p_gbtx_i2c_sda_inout[0]}]
#set_property PACKAGE_PIN G25 [get_ports {p_gbtx_i2c_sda_inout[1]}]



#system interface


#remote jtag
#set_property PACKAGE_PIN AF25 [get_ports p_tdo_remote_in]
#set_property IOSTANDARD LVCMOS25 [get_ports p_tdo_remote_in]




set_property PACKAGE_PIN AB6 [get_ports {p_commbus_gth_loopback_tx_out[0][p]}]

set_property PACKAGE_PIN Y2 [get_ports {p_rx_gbtx_from_fpga_in[0][p]}]
set_property PACKAGE_PIN P2 [get_ports {p_commbus_gth_rx_in[1][p]}]
