

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Sam Silverstein 
--         : Eduardo Valdes
-- Create Date: 08/28/2018 02:56:16 PM
-- Design Name: 
-- Module Name: db6_top - Behavioral
-- Project Name: tilecal daughterboard rev 5 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:  Top level file for operating the DaughterBoard. 
--                       Intended for use on both sides of the DB
-- 
----------------------------------------------------------------------------------


-- common libraries --
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
-- xilinx libraries --
library unisim;
use unisim.vcomponents.all;
-- user defined libraries --

--library gbt;
--use gbt.all;
--use gbt.gbt_bank_package.all;
--use gbt.vendor_specific_gbt_bank_package.all;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_top is
generic (
    g_priority_side : std_logic_vector(1 downto 0) := "01";                 --! side to priotitize
    g_num_gth_links                 : integer := c_gbt_bank_number_of_links;                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
    g_num_gth_ref_clks             : integer := c_number_of_gth_refclks
    );

  port
    (
      --osc clk
      p_osc_clk_in           : in   t_diff_pair;
      
      --gbtx clks
      --p_gbt_db_clk40_local_in : in   t_diff_pair; 
      --p_gbt_db_clk40_remote_in : in   t_diff_pair;
      p_gbt_cfgbus_clk40_local_in : in   t_diff_pair;
      p_gbt_cfgbus_clk40_remote_in : in   t_diff_pair;
      p_gbt_mb_q0_clk40_local_in : in   t_diff_pair; 
      p_gbt_mb_q1_clk40_local_in : in   t_diff_pair;
      p_gbt_mb_q0_clk40_remote_in : in   t_diff_pair;
      p_gbt_mb_q1_clk40_remote_in : in   t_diff_pair;
      p_gbt_tp_q0_clk40_local_in : in   t_diff_pair; 
      p_gbt_tp_q1_clk40_local_in : in   t_diff_pair;
      p_gbt_tp_q0_clk40_remote_in : in   t_diff_pair;
      p_gbt_tp_q1_clk40_remote_in : in   t_diff_pair;

      
      -- leds
      p_leds_out :  out std_logic_vector(3 downto 0);
      
      --db_side
      p_db_side_in : in std_logic_vector(0 downto 0);
      
      --md_number
      p_md_number_in : in std_logic_vector(3 downto 0);
      
      --configbus
--      p_cfgbus_data_local_in : in t_cfgbus_data_in;
--      p_cfgbus_data_remote_in : in t_cfgbus_data_in;

      --mgt
      --gth_refclk
      p_gth_refclk_gbtx_local_in    : in   t_diff_pair_vector((g_num_gth_ref_clks -1) downto 0);
      p_gth_refclk_gbtx_remote_in    : in   t_diff_pair_vector((g_num_gth_ref_clks -1) downto 0);

      
      --sfp/gth
      p_tx_sfp_out  : out t_diff_pair_vector(1 downto 0);
      p_rx_sfp_in  : in t_diff_pair_vector(0 downto 0);
      
      p_tx_gbtx_to_fpga_out : out t_diff_pair_vector(0 downto 0);
      p_rx_gbtx_from_fpga_in  : in t_diff_pair_vector(0 downto 0);
      p_rx_gbtx_tx_in  : in t_diff_pair_vector(0 downto 0);
      
      --commbus mgt
      p_commbus_gth_tx_out  : out t_diff_pair_vector(1 downto 0);
      p_commbus_gth_rx_in  : in t_diff_pair_vector(1 downto 0);
      p_commbus_gth_loopback_tx_out : out t_diff_pair_vector(0 downto 0);
      p_commbus_gth_loopback_rx_in : in t_diff_pair_vector(0 downto 0);
        

      -- sfp
      p_sfp_abs_in                : in std_logic_vector(1 downto 0);
      p_sfp_los_in                : in std_logic_vector(1 downto 0);
      p_sfp_tx_fault_in                : in std_logic_vector(1 downto 0);
      p_sfp_i2c_scl_inout 				: inout std_logic_vector(1 downto 0);
      p_sfp_i2c_sda_inout                 : inout std_logic_vector(1 downto 0);      

      
      --mb_interface
      p_gbt_mb_q0_clk40_out: out   t_diff_pair;
      p_gbt_mb_q1_clk40_out: out   t_diff_pair;
      
--      p_adc_bitclk_in : in t_adc_clk_in;
--      p_adc_frameclk_in : in t_adc_clk_in;
--      p_adc_lg_data_in : in t_adc_data_in;
--      p_adc_hg_data_in : in t_adc_data_in;
      

---- fmc pinout and mb interface

----      p_fmc_ebus_i2c_sda : inout std_logic_vector(1 downto 0);
----      p_fmc_ebus_i2c_scl : inout std_logic_vector(1 downto 0);
--      p_ttc_clk_p_out      : out   std_logic_vector(1 downto 0);
--      p_ttc_clk_n_out      : out   std_logic_vector(1 downto 0);
--      p_adc_data1_n_in      : in    std_logic_vector(5 downto 0);
--      p_adc_data1_p_in      : in    std_logic_vector(5 downto 0);
--      p_adc_data0_n_in      : in    std_logic_vector(5 downto 0);
--      p_adc_data0_p_in      : in    std_logic_vector(5 downto 0);
--      p_adc_bitclk_n_in     : in    std_logic_vector(5 downto 0);
--      p_adc_bitclk_p_in     : in    std_logic_vector(5 downto 0);
--      p_adc_frameclk_n_in   : in    std_logic_vector(5 downto 0);
--      p_adc_frameclk_p_in   : in    std_logic_vector(5 downto 0);
------      intg_d0_p        : in    std_logic_vector(1 downto 0);
------      intg_d0_n        : in    std_logic_vector(1 downto 0);
--      p_sdata_p_out       : in   std_logic_vector(1 downto 0);
--      p_sdata_n_out       : in   std_logic_vector(1 downto 0);
------      intg_dck_p       : in    std_logic_vector(1 downto 0);
------      intg_dck_n       : in    std_logic_vector(1 downto 0);
--      p_tpl_p_out            : out   std_logic_vector(1 downto 0);
--      p_tpl_n_out            : out   std_logic_vector(1 downto 0);

--mb_driver
--        p_ssel_out         : out t_mb_diff_pair;
--        p_sclk_out         : out t_mb_diff_pair;
--        p_sdata_out     : out t_mb_diff_pair;
--        p_sdata_in    : in  t_mb_diff_pair;

--cis_interface
        --cis interface
--        p_tph_out               : out t_mb_diff_pair;
--        p_tpl_out               : out t_mb_diff_pair;

      
--      --xadc and system management
        p_xadc_analog_in : in t_xadc_analog_in;
        p_pgood_in       : in t_p_pgood_in;
	
	    p_tdo_remote_in	    : in    std_logic;		
	
--      p_leds_out        : out   std_logic_vector(3 downto 0);



---- hvopto header

--      hv_enable         : out std_logic_vector (5 downto 0);
----      spi_mosi_p       	: out std_logic;
----      spi_mosi_n       	: out std_logic;
----      spi_miso_p       	: in  std_logic; 
----      spi_miso_n      	: in  std_logic; 
----      spi_sclk_p       	: out std_logic;
----      spi_sclk_n       	: out std_logic;
----      spi_ssn_p        	: out std_logic_vector (5 downto 0);
----      spi_ssn_n        	: out std_logic_vector (5 downto 0);

----        p_gbtx_config_bus_local_out : out  std_logic_vector(7 downto 0); 
----        p_gbtx_config_bus_remote_out : out  std_logic_vector(7 downto 0);
----        p_gbtx_clks_local_out             : out std_logic_vector(3 downto 0);
----        p_gbtx_clks_remote_out             : out std_logic_vector(3 downto 0);




---- cs interface
----      cs0_p                  : out std_logic;    
----      cs0_n                  : out std_logic;    
----      cs1_p                  : out std_logic;    
----      cs1_n                  : out std_logic;    
----      cs2_p                  : in std_logic;    
----      cs2_n                  : in std_logic;    
----      cs3_p                  : in std_logic;    
----      cs3_n                  : in std_logic;    
--    p_cs_nreq_p_out, p_cs_nreq_n_out : out std_logic;
--    p_cs_miso_p_out, p_cs_miso_n_out : out std_logic;
--    p_cs_mosi_p_in, p_cs_mosi_n_in : in  std_logic;
--    p_cs_sclk_p_in, p_cs_sclk_n_in : in  std_logic;


--      -- gbtx signals
      p_gbtx_rxready_in          : in std_logic_vector(1 downto 0); -- 0 local, 1 remote
      p_gbtx_datavalid_in        : in std_logic_vector(1 downto 0) -- 0 local, 1 remote
--      p_gbtx_i2c_scl_inout 				: inout std_logic_vector(1 downto 0);
--      p_gbtx_i2c_sda_inout                 : inout std_logic_vector(1 downto 0);
--      p_gbtx_loc_reset_inout          : inout std_logic;
--      p_gbtx_rem_reset_inout          : inout std_logic;
        

    
-- --serial id
--      p_serial_id_sda_inout : inout std_logic; 
--      p_serial_id_scl_inout : inout std_logic;
  
--  --mainboard jtag chain
--      p_mb_tms_out : out std_logic;
--      p_mb_tck_out : out std_logic;
--      p_mb_tdi_out : out std_logic;
--      p_mb_tdo_in : in std_logic;
      
--   --mainboard reset fpgas
--      p_mb_fpga_reset_low         : out std_logic_vector(1 downto 0); 
      
--  -- power good monitoring
--    p_pgood_in : in std_logic_vector (3 downto 0);
       
--  --fpga inter communication 
        --commbus ddr
--        p_commbus_ddr_tx_out  : out t_diff_pair;
--        p_commbus_ddr_loopback_tx_out  : out t_diff_pair;
--        p_commbus_ddr_clk_out  : out t_diff_pair;
--        p_commbus_ddr_rx_in  : in t_diff_pair;
--        p_commbus_ddr_loopback_rx_in  : in t_diff_pair;
--        p_commbus_ddr_clk_in  : in t_diff_pair
              	
      );

end db6_top;

architecture rtl of db6_top is
attribute IOB: string;
attribute keep: string;
attribute dont_touch: string;

--signals
--clock signals
signal s_clknet : t_db_clknet;
signal s_clkin : t_db_clkin;
--signal s_clk_sel : std_logic := '0';
--signal s_cpll_clk_sel : std_logic_vector(2 downto 0);
--signal s_qpll_clk_sel : std_logic_vector(2 downto 0);


--reset signals
signal s_master_reset : std_logic := '0';

--led signals
signal s_leds : t_led_regs;--: std_logic_vector(3 downto 0); 

--register signals
signal s_db_reg_rx : t_db_reg_rx;

--interface signals
signal s_mb_interface          : t_mb_interface;
signal s_sem_interface     : t_sem_interface;
signal s_system_management_interface : t_system_management_interface;
signal s_gbtx_interface : t_gbtx_interface;
signal s_gbt_encoder_interface : t_gbt_encoder_interface;
signal s_serial_id_interface : t_serial_id_interface;
signal s_sfp_control : t_sfp_control;
signal s_sfp_interface : t_sfp_interface;

signal s_gbtx_control : t_gbtx_control;

attribute keep of s_sfp_interface, s_sfp_control, s_gbtx_interface, s_mb_interface, s_sem_interface, s_system_management_interface, s_gbtx_control, s_serial_id_interface : signal is "TRUE";
attribute dont_touch of s_sfp_interface, s_sfp_control, s_gbtx_interface, s_mb_interface, s_sem_interface, s_system_management_interface, s_gbtx_control, s_serial_id_interface : signal is "TRUE";



COMPONENT db6_ibert_ultrascale_gth
  PORT (
    txn_o : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    txp_o : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxoutclk_o : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxn_i : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxp_i : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    gtrefclk0_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtrefclk1_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtnorthrefclk0_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtnorthrefclk1_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtsouthrefclk0_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtsouthrefclk1_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtrefclk00_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtrefclk10_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtrefclk01_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtrefclk11_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtnorthrefclk00_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtnorthrefclk10_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtnorthrefclk01_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtnorthrefclk11_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtsouthrefclk00_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtsouthrefclk10_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtsouthrefclk01_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtsouthrefclk11_i : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    clk : IN STD_LOGIC
  );
END COMPONENT;

signal s_db6_ibert_ultrascale_gth : t_db6_ibert_ultrascale_gth;

begin  -- rtl

--top connections
p_gbt_mb_q0_clk40_out <= s_clknet.mb_clk40_q0_dp;
p_gbt_mb_q1_clk40_out <= s_clknet.mb_clk40_q1_dp;

p_leds_out <= s_leds(leds_main_sm);


    
--clock interface
i_db6_clock_interface : entity tilecal.db6_clock_interface
   generic map (   
        g_num_gth_links                => g_num_gth_links,          --! num_links: number of links instantiated by the core (altera: up to 6, xilinx: up to 4)
        g_num_gth_ref_clks             => g_num_gth_ref_clks
        )
    port map(
        --status input
        p_clkin_in.db_side => p_db_side_in,
        p_clkin_in.md_number => p_md_number_in,
        p_clkin_in.gbtx_rxready => p_gbtx_rxready_in,
        p_clkin_in.gbtx_datavalid=> p_gbtx_datavalid_in,
        --clocks
        p_clkin_in.osc_clkin => p_osc_clk_in,
        --p_clkinputs_in.db_clkin_local => p_gbt_db_clk40_local_in,
        --p_clkinputs_in.db_clkin_remote => p_gbt_db_clk40_remote_in,
        p_clkin_in.cfgbus_clkin_local => p_gbt_cfgbus_clk40_local_in,
        p_clkin_in.cfgbus_clkin_remote => p_gbt_cfgbus_clk40_remote_in,
        p_clkin_in.mb_q0_clkin_local => p_gbt_mb_q0_clk40_local_in,
        p_clkin_in.mb_q1_clkin_local => p_gbt_mb_q1_clk40_local_in,
        p_clkin_in.mb_q0_clkin_remote => p_gbt_mb_q0_clk40_remote_in,
        p_clkin_in.mb_q1_clkin_remote => p_gbt_mb_q1_clk40_remote_in,
        p_clkin_in.tp_q0_clkin_local => p_gbt_tp_q0_clk40_local_in,
        p_clkin_in.tp_q1_clkin_local => p_gbt_tp_q1_clk40_local_in,
        p_clkin_in.tp_q0_clkin_remote => p_gbt_tp_q0_clk40_remote_in,
        p_clkin_in.tp_q1_clkin_remote => p_gbt_tp_q1_clk40_remote_in,
        p_clkin_in.gth_refclk_gbtx_local => p_gth_refclk_gbtx_local_in,
        p_clkin_in.gth_refclk_gbtx_remote => p_gth_refclk_gbtx_remote_in,
        p_clkin_in.gth_txwordclk_out => s_clkin.gth_txwordclk_out,
        p_clkin_in.gth_rxwordclk_out => s_clkin.gth_rxwordclk_out,
        p_clkin_in.gth_txoutclkfabric_out => s_clkin.gth_txoutclkfabric_out,
        p_clkin_in.gth_rxoutclkfabric_out => s_clkin.gth_rxoutclkfabric_out,
        p_clkin_in.clksel => s_clkin.clksel,
        p_clkin_in.cpllclksel => s_clkin.cpllclksel,
        p_clkin_in.qpllclksel => s_clkin.qpllclksel,
        p_clkin_in.txsysclksel => s_clkin.txsysclksel,
        p_clkin_in.rxsysclksel => s_clkin.rxsysclksel,
        p_clkin_in.txoutclksel => s_clkin.txoutclksel,
        p_clkin_in.rxoutclksel => s_clkin.rxoutclksel,
        p_clkin_in.bcr => s_clkin.bcr,
        
        
        p_clknet_out => s_clknet,
        
        --control signals
        p_master_reset_in => s_master_reset,
        p_db_reg_rx_in => s_db_reg_rx
        
    );

        --mainboard interface
--i_db6_mainboard_interface : entity tilecal.db6_mainboard_interface
--      Port map(
--        p_master_reset_in => s_master_reset,
--        p_clknet_in  => s_clknet,
--        p_db_reg_rx_in => s_db_reg_rx,
--        -- adc interface
--        p_adc_bitclk_in => p_adc_bitclk_in,
--        p_adc_frameclk_in => p_adc_frameclk_in,
--        p_adc_lg_data_in => p_adc_lg_data_in,
--        p_adc_hg_data_in => p_adc_hg_data_in,
--        -- mb interface
--        p_ssel_out         => p_ssel_out,
--        p_sclk_out         => p_sclk_out,
--        p_sdata_out     => p_sdata_out,
--        p_sdata_in    => p_sdata_in,
--        --cis interface
--        p_tph_out               => p_tph_out,
--        p_tpl_out               => p_tpl_out,
        
--        p_mb_interface_out          => s_mb_interface

    
--  );

s_db6_ibert_ultrascale_gth.clk <= s_clknet.clk100;
s_db6_ibert_ultrascale_gth.gtrefclk0_i(0) <= s_clknet.gth_refclk_remote(0);
s_db6_ibert_ultrascale_gth.gtrefclk0_i(1) <= s_clknet.gth_refclk_remote(1);
s_db6_ibert_ultrascale_gth.gtrefclk1_i(0) <= s_clknet.gth_refclk_local(0);
s_db6_ibert_ultrascale_gth.gtrefclk1_i(1) <= s_clknet.gth_refclk_local(1);


p_tx_sfp_out(0).n <= s_db6_ibert_ultrascale_gth.txn_o(0);
p_tx_sfp_out(0).p <= s_db6_ibert_ultrascale_gth.txp_o(0);

p_tx_sfp_out(1).n <= s_db6_ibert_ultrascale_gth.txn_o(2);
p_tx_sfp_out(1).p <= s_db6_ibert_ultrascale_gth.txp_o(2);

p_commbus_gth_tx_out(0).n <= s_db6_ibert_ultrascale_gth.txn_o(3);
p_commbus_gth_tx_out(0).p <= s_db6_ibert_ultrascale_gth.txp_o(3);

p_commbus_gth_tx_out(1).n <= s_db6_ibert_ultrascale_gth.txn_o(7);
p_commbus_gth_tx_out(1).p <= s_db6_ibert_ultrascale_gth.txp_o(7);

p_tx_gbtx_to_fpga_out(0).n <= s_db6_ibert_ultrascale_gth.txn_o(4);
p_tx_gbtx_to_fpga_out(0).p <= s_db6_ibert_ultrascale_gth.txp_o(4);

p_commbus_gth_loopback_tx_out(0).n <= s_db6_ibert_ultrascale_gth.txn_o(6);
p_commbus_gth_loopback_tx_out(0).p <= s_db6_ibert_ultrascale_gth.txp_o(6);

s_db6_ibert_ultrascale_gth.rxn_i(3) <= p_rx_gbtx_tx_in(0).n;
s_db6_ibert_ultrascale_gth.rxp_i(3) <= p_rx_gbtx_tx_in(0).p;

s_db6_ibert_ultrascale_gth.rxn_i(0) <= p_rx_sfp_in(0).n;
s_db6_ibert_ultrascale_gth.rxp_i(0) <= p_rx_sfp_in(0).p;

s_db6_ibert_ultrascale_gth.rxn_i(4) <= p_rx_gbtx_from_fpga_in(0).n;
s_db6_ibert_ultrascale_gth.rxp_i(4) <= p_rx_gbtx_from_fpga_in(0).p;

s_db6_ibert_ultrascale_gth.rxn_i(2) <= p_commbus_gth_rx_in(0).n;
s_db6_ibert_ultrascale_gth.rxp_i(2) <= p_commbus_gth_rx_in(0).p;

s_db6_ibert_ultrascale_gth.rxn_i(7) <= p_commbus_gth_rx_in(1).n;
s_db6_ibert_ultrascale_gth.rxp_i(7) <= p_commbus_gth_rx_in(1).p;

s_db6_ibert_ultrascale_gth.rxn_i(6) <= p_commbus_gth_loopback_rx_in(0).n;
s_db6_ibert_ultrascale_gth.rxp_i(6) <= p_commbus_gth_loopback_rx_in(0).p;




i_db6_ibert_ultrascale_gth : db6_ibert_ultrascale_gth
  PORT MAP (
    txn_o => s_db6_ibert_ultrascale_gth.txn_o,
    txp_o => s_db6_ibert_ultrascale_gth.txp_o,
    rxoutclk_o => s_db6_ibert_ultrascale_gth.rxoutclk_o,
    rxn_i => s_db6_ibert_ultrascale_gth.rxn_i,
    rxp_i => s_db6_ibert_ultrascale_gth.rxp_i,
    gtrefclk0_i => s_db6_ibert_ultrascale_gth.gtrefclk0_i,
    gtrefclk1_i => s_db6_ibert_ultrascale_gth.gtrefclk1_i,
    gtnorthrefclk0_i => s_db6_ibert_ultrascale_gth.gtnorthrefclk0_i,
    gtnorthrefclk1_i => s_db6_ibert_ultrascale_gth.gtnorthrefclk1_i,
    gtsouthrefclk0_i => s_db6_ibert_ultrascale_gth.gtsouthrefclk0_i,
    gtsouthrefclk1_i => s_db6_ibert_ultrascale_gth.gtsouthrefclk1_i,
    gtrefclk00_i => s_db6_ibert_ultrascale_gth.gtrefclk00_i,
    gtrefclk10_i => s_db6_ibert_ultrascale_gth.gtrefclk10_i,
    gtrefclk01_i => s_db6_ibert_ultrascale_gth.gtrefclk01_i,
    gtrefclk11_i => s_db6_ibert_ultrascale_gth.gtrefclk11_i,
    gtnorthrefclk00_i => s_db6_ibert_ultrascale_gth.gtnorthrefclk00_i,
    gtnorthrefclk10_i => s_db6_ibert_ultrascale_gth.gtnorthrefclk10_i,
    gtnorthrefclk01_i => s_db6_ibert_ultrascale_gth.gtnorthrefclk01_i,
    gtnorthrefclk11_i => s_db6_ibert_ultrascale_gth.gtnorthrefclk11_i,
    gtsouthrefclk00_i => s_db6_ibert_ultrascale_gth.gtsouthrefclk00_i,
    gtsouthrefclk10_i => s_db6_ibert_ultrascale_gth.gtsouthrefclk10_i,
    gtsouthrefclk01_i => s_db6_ibert_ultrascale_gth.gtsouthrefclk01_i,
    gtsouthrefclk11_i => s_db6_ibert_ultrascale_gth.gtsouthrefclk11_i,
    clk => s_db6_ibert_ultrascale_gth.clk
  );




--i_db6_sfp_interface : entity tilecal.db6_sfp_interface
--   generic map (   
--        g_num_gth_links                => g_num_gth_links          --! num_links: number of links instantiated by the core (altera: up to 6, xilinx: up to 4)
--        )
--  port map(
--        p_clknet_in => s_clknet,
--        p_master_reset_in => s_master_reset,
--        p_db_reg_rx_in => s_db_reg_rx,
--        p_gbt_encoder_interface_out => s_gbt_encoder_interface,
        
--        --refclk inputs
--        --p_gth_refclk_gbtx_local_in    => p_gth_refclk_gbtx_local_in,
--        --p_gth_refclk_gbtx_remote_in    => p_gth_refclk_gbtx_remote_in,
--        p_gth_txwordclk_out => s_clkin.gth_txwordclk_out,
--        p_gth_rxwordclk_out => s_clkin.gth_rxwordclk_out,
--        p_gth_txoutclkfabric_out => s_clkin.gth_txoutclkfabric_out,
--        p_gth_rxoutclkfabric_out => s_clkin.gth_rxoutclkfabric_out,
        
--        --sfp/gth
--        --p_tx_sfp_out         => p_tx_sfp_out,
--        --p_rx_sfp_in         => p_rx_sfp_in,
--        p_tx_sfp_out  => p_tx_sfp_out,
--        p_rx_sfp_in  => p_rx_sfp_in,
--        p_sfp_abs_in => p_sfp_abs_in,
--        p_sfp_los_in => p_sfp_los_in,
--        p_sfp_tx_fault_in => p_sfp_tx_fault_in,
        
--        p_tx_gbtx_to_fpga_out => p_tx_gbtx_to_fpga_out,
--        p_rx_gbtx_from_fpga_in => p_rx_gbtx_from_fpga_in,
--        p_rx_gbtx_tx_in => p_rx_gbtx_tx_in,
                
--        p_sfp_i2c_scl_inout => p_sfp_i2c_scl_inout,
--        p_sfp_i2c_sda_inout => p_sfp_i2c_sda_inout, 
--        p_sfp_control_in => s_sfp_control,
--        p_sfp_interface_out => s_sfp_interface,
        
--        --tdo from remote fpga
--        p_tdo_remote_in => p_tdo_remote_in,
        
--        --interfaces
--        p_mb_interface_in => s_mb_interface,
--        p_sem_interface_in => s_sem_interface,
--        p_system_management_interface_in => s_system_management_interface,
--        p_gbtx_interface_in => s_gbtx_interface,
--        p_serial_id_interface_in => s_serial_id_interface
--  );

--inter xilinx fpga communication
--i_db6_commbus_interface : entity tilecal.db6_commbus_interface
--   generic map (   
--        g_num_gth_links                 => g_num_gth_links                            --! NUM_LINKS: number of links instantiated by the core (Altera: up to 6, Xilinx: up to 4)
--   )
--  port map ( 
--        p_clknet_in => s_clknet,
--        p_master_reset_in => s_master_reset,
--        p_db_reg_rx_in => s_db_reg_rx,
        
--        p_commbus_gth_tx_out  => p_commbus_gth_tx_out,
--        p_commbus_gth_rx_in  => p_commbus_gth_rx_in,
--        p_commbus_gth_loopback_tx_out => p_commbus_gth_loopback_tx_out,
--        p_commbus_gth_loopback_rx_in => p_commbus_gth_loopback_rx_in,

--        p_commbus_ddr_tx_out => p_commbus_ddr_tx_out,
--        p_commbus_ddr_loopback_tx_out => p_commbus_ddr_loopback_tx_out,
--        p_commbus_ddr_clk_out => p_commbus_ddr_clk_out,
--        p_commbus_ddr_rx_in => p_commbus_ddr_rx_in,
--        p_commbus_ddr_loopback_rx_in => p_commbus_ddr_loopback_rx_in,
--        p_commbus_ddr_clk_in => p_commbus_ddr_clk_in,
        
--        --tdo from remote fpga
--        p_tdo_remote_in => p_tdo_remote_in,
        
--        --interfaces
--        p_mb_interface_in => s_mb_interface,
--        p_sem_interface_in => s_sem_interface,
--        p_system_management_interface_in => s_system_management_interface,
--        p_gbtx_interface_in => s_gbtx_interface,
--        p_serial_id_interface_in => s_serial_id_interface
        
--      );

-- sem interface
--i_db6_sem_interface : entity tilecal.db6_sem_interface
--  port map (
--    p_clknet_in => s_clknet,
--    p_db_reg_rx_in => s_db_reg_rx,
--    p_sem_interface_out => s_sem_interface
--  );

-- system manager (pgoods, temps, vccint... legacy xadc module)
--i_db6_system_management_interface : entity tilecal.db6_system_management_interface
--    port map ( 
--        p_clknet_in => s_clknet,
--        p_db_reg_rx_in => s_db_reg_rx,
--        p_master_reset_in => s_master_reset,
        
--        --xadc pins to top level
--        p_xadc_analog_in => p_xadc_analog_in,
--        p_pgood_in => p_pgood_in, 
        
--        --output
--        p_system_management_interface_out => s_system_management_interface, 
        
--        --leds and debug_out
--        p_leds_out => open
    
--    );

--gbtx interface (includes configbus) 
--i_db6_gbtx_interface : entity tilecal.db6_gbtx_interface
--  port map(       
--        p_clknet_in => s_clknet,
--        p_master_reset_in  => s_master_reset,
        
--        p_cfgbus_data_local_in => p_cfgbus_data_local_in,
--        p_cfgbus_data_remote_in => p_cfgbus_data_remote_in,
--        p_db_reg_rx_out => s_db_reg_rx,
--        p_bcr_out => s_clkin.bcr,
        
--        p_db_reg_rx_in => s_db_reg_rx,
--        p_gbt_encoder_interface_in => s_gbt_encoder_interface,
        
        
        
--        p_gbtx_control => s_gbtx_control,
--        p_gbtx_interface_out => s_gbtx_interface,
--        p_scl_inout =>p_gbtx_i2c_scl_inout,
--        p_sda_inout =>p_gbtx_i2c_sda_inout,
            
--        p_leds_out => open
--);

-- id serial interface
--i_db6_serial_id_interface : entity tilecal.db6_serial_id_interface
--  port map ( 
--        p_master_reset_in => s_master_reset,
--        p_clknet_in => s_clknet,
--        p_db_reg_rx_in => s_db_reg_rx,
        
--        p_scl_inout 	=> p_serial_id_scl_inout,
--        p_sda_inout    => p_serial_id_sda_inout,
        
--        p_serial_id_interface_out => s_serial_id_interface,
    
--        p_leds_out => open

--  );


-- clock selection signals, state machines and mux

s_clkin.txsysclksel <= "11";
s_clkin.rxsysclksel <= "00";

s_clkin.txoutclksel <= "011";
s_clkin.rxoutclksel <= "011";

proc_refclk_select: process(s_clknet.gbtx_rxready)
begin
    case s_clknet.gbtx_rxready is
        when "01"=>
            s_clkin.clksel <= '0';
            s_clkin.cpllclksel <= "001";
            s_clkin.qpllclksel <= "001";
        when "10"=>
            s_clkin.clksel<= '1';
            s_clkin.cpllclksel <= "010";
            s_clkin.qpllclksel <= "010";
        when others=>
            s_clkin.clksel<= '0';
            s_clkin.cpllclksel <= "001";
            s_clkin.qpllclksel <= "001";
    end case;
end process;


s_leds(leds_main_sm)(0) <= s_clknet.osc_clk40;
s_leds(leds_main_sm)(1) <= s_clknet.db_clk40;
s_leds(leds_main_sm)(2) <= s_clknet.mb_clk40.q0;
s_leds(leds_main_sm)(3) <= s_clknet.mb_clk40.q1;

end rtl;
    

    