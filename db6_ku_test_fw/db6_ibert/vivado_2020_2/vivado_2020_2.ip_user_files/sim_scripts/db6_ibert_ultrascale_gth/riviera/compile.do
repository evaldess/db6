vlib work
vlib riviera

vlib riviera/xpm
vlib riviera/xil_defaultlib

vmap xpm riviera/xpm
vmap xil_defaultlib riviera/xil_defaultlib

vlog -work xpm  -sv2k12 "+incdir+../../../../vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/hdl/verilog" "+incdir+../../../../vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth" \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/hdl/verilog" "+incdir+../../../../vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth" \
"d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/db6_ibert_ultrascale_gth_sim_netlist.v" \

vlog -work xil_defaultlib \
"glbl.v"

