onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib db6_ibert_ultrascale_gth_opt

do {wave.do}

view wave
view structure
view signals

do {db6_ibert_ultrascale_gth.udo}

run -all

quit -force
