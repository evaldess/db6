# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
namespace eval ::optrace {
  variable script "D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.runs/synth_1/db6_top.tcl"
  variable category "vivado_synth"
}

# Try to connect to running dispatch if we haven't done so already.
# This code assumes that the Tcl interpreter is not using threads,
# since the ::dispatch::connected variable isn't mutex protected.
if {![info exists ::dispatch::connected]} {
  namespace eval ::dispatch {
    variable connected false
    if {[llength [array get env XILINX_CD_CONNECT_ID]] > 0} {
      set result "true"
      if {[catch {
        if {[lsearch -exact [package names] DispatchTcl] < 0} {
          set result [load librdi_cd_clienttcl[info sharedlibextension]] 
        }
        if {$result eq "false"} {
          puts "WARNING: Could not load dispatch client library"
        }
        set connect_id [ ::dispatch::init_client -mode EXISTING_SERVER ]
        if { $connect_id eq "" } {
          puts "WARNING: Could not initialize dispatch client"
        } else {
          puts "INFO: Dispatch client connection id - $connect_id"
          set connected true
        }
      } catch_res]} {
        puts "WARNING: failed to connect to dispatch server - $catch_res"
      }
    }
  }
}
if {$::dispatch::connected} {
  # Remove the dummy proc if it exists.
  if { [expr {[llength [info procs ::OPTRACE]] > 0}] } {
    rename ::OPTRACE ""
  }
  proc ::OPTRACE { task action {tags {} } } {
    ::vitis_log::op_trace "$task" $action -tags $tags -script $::optrace::script -category $::optrace::category
  }
  # dispatch is generic. We specifically want to attach logging.
  ::vitis_log::connect_client
} else {
  # Add dummy proc if it doesn't exist.
  if { [expr {[llength [info procs ::OPTRACE]] == 0}] } {
    proc ::OPTRACE {{arg1 \"\" } {arg2 \"\"} {arg3 \"\" } {arg4 \"\"} {arg5 \"\" } {arg6 \"\"}} {
        # Do nothing
    }
  }
}

proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
OPTRACE "synth_1" START { ROLLUP_AUTO }
set_param chipscope.maxJobs 6
set_msg_config -id {Synth 8-256} -limit 10000
set_msg_config -id {Synth 8-638} -limit 10000
OPTRACE "Creating in-memory project" START { }
create_project -in_memory -part xcku035-fbva676-1-c

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.cache/wt [current_project]
set_property parent.project_path D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property ip_output_repo d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
OPTRACE "Creating in-memory project" END { }
OPTRACE "Adding files" START { }
read_vhdl -library tilecal {
  D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/db6_clock_interface.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/db6_top.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/db6_design_package.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/db6_sfp_interface.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/db6_i2c_master.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/db6_sfp_i2c_interface.vhd
}
read_vhdl -library gbt {
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/xilinx_ku/mgt/xlx_ku_mgt_ip_reset_synchronizer.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/xilinx_ku/gbt_tx/xlx_ku_gbt_tx_gearbox_std_dpram.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/xilinx_ku/xlx_ku_gbt_bank_package.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/xilinx_ku/gbt_rx/xlx_ku_gbt_rx_gearbox_std_dpram.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/xilinx_ku/mgt/xlx_ku_mgt.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_std_rdctrl.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/mgt/mgt_framealigner_pattsearch.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_polydiv.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_elpeval.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_syndrom.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_phasemon.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_21bit.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_chnsrch.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_bank_package.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_rsencode.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_deintlver.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_descrambler_16bit.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_latopt.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_bank.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder_gbtframe_intlver.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_std_rdwrctrl.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/mgt/mgt_bitslipctrl.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rs2errcor.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_std.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_rsdec.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_16bit.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_errlcpoly.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_gearbox_std.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_gearbox_latopt.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_scrambler_21bit.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_tx/gbt_tx_encoder.vhd
  D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/src/gbt-fpga/gbt_bank/core_sources/gbt_rx/gbt_rx_decoder_gbtframe_lmbddet.vhd
}
read_ip -quiet D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_mb/mmcm_gbt40_mb.xci
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_mb/mmcm_gbt40_mb_board.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_mb/mmcm_gbt40_mb.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_mb/mmcm_gbt40_mb_late.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_mb/mmcm_gbt40_mb_ooc.xdc]

read_ip -quiet D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_cfgbus/mmcm_gbt40_cfgbus.xci
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_cfgbus/mmcm_gbt40_cfgbus_board.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_cfgbus/mmcm_gbt40_cfgbus.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_cfgbus/mmcm_gbt40_cfgbus_late.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_cfgbus/mmcm_gbt40_cfgbus_ooc.xdc]

read_ip -quiet D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/pll_osc_clk/pll_osc_clk.xci
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/pll_osc_clk/pll_osc_clk_board.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/pll_osc_clk/pll_osc_clk.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/pll_osc_clk/pll_osc_clk_late.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/pll_osc_clk/pll_osc_clk_ooc.xdc]

read_ip -quiet D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/blk_mem_tdpram_1kx18_8b256/blk_mem_tdpram_1kx18_8b256.xci
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/blk_mem_tdpram_1kx18_8b256/blk_mem_tdpram_1kx18_8b256_ooc.xdc]

read_ip -quiet D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_osc_clk/mmcm_osc_clk.xci
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_osc_clk/mmcm_osc_clk_board.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_osc_clk/mmcm_osc_clk.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_osc_clk/mmcm_osc_clk_late.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_osc_clk/mmcm_osc_clk_ooc.xdc]

read_ip -quiet D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_db/mmcm_gbt40_db.xci
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_db/mmcm_gbt40_db_board.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_db/mmcm_gbt40_db.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_db/mmcm_gbt40_db_late.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/src/ip/mmcm_gbt40_db/mmcm_gbt40_db_ooc.xdc]

read_ip -quiet D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/db6_ibert_ultrascale_gth/db6_ibert_ultrascale_gth.xci
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth/db6_ibert_ultrascale_gth_ooc.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth/db6_ibert_ultrascale_gth.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth/sw_mcs_all.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth/attributes.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/data/mb_bootloop_le.elf]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/db6_ibert_ultrascale_gth/mb_bootloop_le.elf]

OPTRACE "Adding files" END { }
# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/db6.xdc
set_property used_in_implementation false [get_files D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/db6.xdc]

read_xdc D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/db6_timing.xdc
set_property used_in_implementation false [get_files D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/db6_timing.xdc]

read_xdc D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/p_blocks.xdc
set_property used_in_implementation false [get_files D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/p_blocks.xdc]

read_xdc D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/xadc_system_managment.xdc
set_property used_in_implementation false [get_files D:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_ibert/constraints/xadc_system_managment.xdc]

read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]
set_param ips.enableIPCacheLiteLoad 1
close [open __synthesis_is_running__ w]

OPTRACE "synth_design" START { }
synth_design -top db6_top -part xcku035-fbva676-1-c
OPTRACE "synth_design" END { }
if { [get_msg_config -count -severity {CRITICAL WARNING}] > 0 } {
 send_msg_id runtcl-6 info "Synthesis results are not added to the cache due to CRITICAL_WARNING"
}


OPTRACE "write_checkpoint" START { CHECKPOINT }
# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef db6_top.dcp
OPTRACE "write_checkpoint" END { }
OPTRACE "synth reports" START { REPORT }
create_report "synth_1_synth_report_utilization_0" "report_utilization -file db6_top_utilization_synth.rpt -pb db6_top_utilization_synth.pb"
OPTRACE "synth reports" END { }
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
OPTRACE "synth_1" END { }
