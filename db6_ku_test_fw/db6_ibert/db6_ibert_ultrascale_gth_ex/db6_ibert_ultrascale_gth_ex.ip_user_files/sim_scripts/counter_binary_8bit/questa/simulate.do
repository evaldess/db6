onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib counter_binary_8bit_opt

do {wave.do}

view wave
view structure
view signals

do {counter_binary_8bit.udo}

run -all

quit -force
