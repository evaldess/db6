vlib work
vlib activehdl

vlib activehdl/xpm
vlib activehdl/xil_defaultlib

vmap xpm activehdl/xpm
vmap xil_defaultlib activehdl/xil_defaultlib

vlog -work xpm  -sv2k12 "+incdir+../../../../db6_ibert_ultrascale_gth_ex.gen/sources_1/ip/db6_ibert_ultrascale_gth/hdl/verilog" "+incdir+../../../../db6_ibert_ultrascale_gth_ex.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth" \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"C:/apps/Xilinx/Vivado/2020.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../db6_ibert_ultrascale_gth_ex.gen/sources_1/ip/db6_ibert_ultrascale_gth/hdl/verilog" "+incdir+../../../../db6_ibert_ultrascale_gth_ex.gen/sources_1/ip/db6_ibert_ultrascale_gth/synth" \
"p:/TileCal/db6/db6_ku_test_fw/db6_ibert/db6_ibert_ultrascale_gth_ex/db6_ibert_ultrascale_gth_ex.gen/sources_1/ip/db6_ibert_ultrascale_gth/db6_ibert_ultrascale_gth_sim_netlist.v" \

vlog -work xil_defaultlib \
"glbl.v"

