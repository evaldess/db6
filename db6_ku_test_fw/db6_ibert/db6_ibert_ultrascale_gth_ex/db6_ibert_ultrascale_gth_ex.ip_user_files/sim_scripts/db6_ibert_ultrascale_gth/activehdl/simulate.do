onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+db6_ibert_ultrascale_gth -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.db6_ibert_ultrascale_gth xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {db6_ibert_ultrascale_gth.udo}

run -all

endsim

quit -force
