onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+jtagCtrl_gbtfpgaTest -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.jtagCtrl_gbtfpgaTest xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {jtagCtrl_gbtfpgaTest.udo}

run -all

endsim

quit -force
