onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib jtagCtrl_gbtfpgaTest_opt

do {wave.do}

view wave
view structure
view signals

do {jtagCtrl_gbtfpgaTest.udo}

run -all

quit -force
