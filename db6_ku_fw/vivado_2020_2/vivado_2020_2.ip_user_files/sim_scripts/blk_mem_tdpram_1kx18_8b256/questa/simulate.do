onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib blk_mem_tdpram_1kx18_8b256_opt

do {wave.do}

view wave
view structure
view signals

do {blk_mem_tdpram_1kx18_8b256.udo}

run -all

quit -force
