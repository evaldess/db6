onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -L xpm -L unisims_ver -L unimacro_ver -L secureip -lib xil_defaultlib xil_defaultlib.rx_frmclk_pll xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {rx_frmclk_pll.udo}

run -all

quit -force
