onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib rx_frmclk_pll_opt

do {wave.do}

view wave
view structure
view signals

do {rx_frmclk_pll.udo}

run -all

quit -force
