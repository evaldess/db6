onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+rx_frmclk_pll -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.rx_frmclk_pll xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {rx_frmclk_pll.udo}

run -all

endsim

quit -force
