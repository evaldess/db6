onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib pll_commbus_opt

do {wave.do}

view wave
view structure
view signals

do {pll_commbus.udo}

run -all

quit -force
