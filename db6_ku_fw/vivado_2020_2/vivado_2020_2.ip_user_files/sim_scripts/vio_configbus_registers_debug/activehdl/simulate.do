onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+vio_configbus_registers_debug -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.vio_configbus_registers_debug xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {vio_configbus_registers_debug.udo}

run -all

endsim

quit -force
