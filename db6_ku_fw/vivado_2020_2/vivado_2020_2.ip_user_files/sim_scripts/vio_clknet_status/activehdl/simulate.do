onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+vio_clknet_status -L xpm -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.vio_clknet_status xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {vio_clknet_status.udo}

run -all

endsim

quit -force
