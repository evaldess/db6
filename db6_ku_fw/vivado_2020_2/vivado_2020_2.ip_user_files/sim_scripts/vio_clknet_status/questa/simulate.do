onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib vio_clknet_status_opt

do {wave.do}

view wave
view structure
view signals

do {vio_clknet_status.udo}

run -all

quit -force
