onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+xlx_k7v7_vio -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.xlx_k7v7_vio xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {xlx_k7v7_vio.udo}

run -all

endsim

quit -force
