onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib fifo_gbt_encoder_opt

do {wave.do}

view wave
view structure
view signals

do {fifo_gbt_encoder.udo}

run -all

quit -force
