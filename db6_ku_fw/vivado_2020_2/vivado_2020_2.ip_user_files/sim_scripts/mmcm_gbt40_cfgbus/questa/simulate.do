onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib mmcm_gbt40_cfgbus_opt

do {wave.do}

view wave
view structure
view signals

do {mmcm_gbt40_cfgbus.udo}

run -all

quit -force
