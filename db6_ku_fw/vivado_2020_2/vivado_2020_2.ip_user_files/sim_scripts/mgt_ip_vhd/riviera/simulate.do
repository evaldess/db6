onbreak {quit -force}
onerror {quit -force}

asim +access +r +m+xlx_k7v7_mgt_ip -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.xlx_k7v7_mgt_ip xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {xlx_k7v7_mgt_ip.udo}

run -all

endsim

quit -force
