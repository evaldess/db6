onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib dsp48_signed_substract_opt

do {wave.do}

view wave
view structure
view signals

do {dsp48_signed_substract.udo}

run -all

quit -force
