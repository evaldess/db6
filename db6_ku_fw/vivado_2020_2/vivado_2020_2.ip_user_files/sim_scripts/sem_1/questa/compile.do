vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xpm
vlib questa_lib/msim/sem_ultra_v3_1_16
vlib questa_lib/msim/xil_defaultlib

vmap xpm questa_lib/msim/xpm
vmap sem_ultra_v3_1_16 questa_lib/msim/sem_ultra_v3_1_16
vmap xil_defaultlib questa_lib/msim/xil_defaultlib

vlog -work xpm  -sv "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_wide_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/diablo_wide_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"C:/apps/xilinx/Vivado/2020.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/apps/xilinx/Vivado/2020.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm  -93 \
"C:/apps/xilinx/Vivado/2020.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work sem_ultra_v3_1_16  "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_wide_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/diablo_wide_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ipstatic/hdl/sem_ultra_v3_1_vl_rfs.v" \

vlog -work xil_defaultlib  "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_wide_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/diablo_wide_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ip/sem_1/synth/sem.v" \

vlog -work xil_defaultlib \
"glbl.v"

