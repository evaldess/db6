onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -L xpm -L sem_ultra_v3_1_16 -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -lib xil_defaultlib xil_defaultlib.sem xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {sem.udo}

run -all

quit -force
