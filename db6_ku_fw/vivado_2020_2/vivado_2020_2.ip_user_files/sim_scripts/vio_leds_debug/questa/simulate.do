onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib vio_leds_debug_opt

do {wave.do}

view wave
view structure
view signals

do {vio_leds_debug.udo}

run -all

quit -force
