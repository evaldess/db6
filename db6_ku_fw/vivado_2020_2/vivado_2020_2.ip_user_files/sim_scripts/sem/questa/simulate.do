onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib sem_opt

do {wave.do}

view wave
view structure
view signals

do {sem.udo}

run -all

quit -force
