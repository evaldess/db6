vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xpm
vlib modelsim_lib/msim/sem_ultra_v3_1_12
vlib modelsim_lib/msim/xil_defaultlib

vmap xpm modelsim_lib/msim/xpm
vmap sem_ultra_v3_1_12 modelsim_lib/msim/sem_ultra_v3_1_12
vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib

vlog -work xpm  -incr -sv "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"C:/apps/xilinx/Vivado/2020.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/apps/xilinx/Vivado/2020.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm  -93 \
"C:/apps/xilinx/Vivado/2020.2/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work sem_ultra_v3_1_12  -incr "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ipstatic/hdl/sem_ultra_v3_1_vl_rfs.v" \

vlog -work xil_defaultlib  -incr "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ip/sem/synth/sem.v" \

vlog -work xil_defaultlib \
"glbl.v"

