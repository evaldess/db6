// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Apr 14 22:51:02 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/dsp48_signed_substract_synth_1/dsp48_signed_substract_stub.v
// Design      : dsp48_signed_substract
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "c_addsub_v12_0_14,Vivado 2020.2" *)
module dsp48_signed_substract(A, B, CLK, S)
/* synthesis syn_black_box black_box_pad_pin="A[14:0],B[14:0],CLK,S[14:0]" */;
  input [14:0]A;
  input [14:0]B;
  input CLK;
  output [14:0]S;
endmodule
