// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 31 23:34:50 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/Documents/PostDoc/TileCal/db6/db6_ku_test_fw/db6_adc_readout/vivado_2020_2/vivado_2020_2.gen/sources_1/ip/counter_binary_32bit/counter_binary_32bit_sim_netlist.v
// Design      : counter_binary_32bit
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary_32bit,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module counter_binary_32bit
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [31:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]L;
  wire LOAD;
  wire [31:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "1" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  counter_binary_32bit_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
Yjjm0e9zGnxh5FWYtU0VCfOdqMcjIq14h/KRcWkoIXBIgeG1k1+PLcXLEp7TI+N0yi/WmYUwgzdF
PtOs47vyAcnzjqT9S2Eb4CBC9MEUbQCx6mFJdztNdKrirAR25D9jyyiULK2iqwWhSBFG0CM+OfCP
LFvhSglpBQXo8DgcdDEua07goPNIWDx44JCO4B2OVIChKTuJZbpAT6/h/ohq0Y2TLjj2tDbBA617
j3FaF62VUOlgbP1fNZ25uVggsK+2uczoKm0G/8LS6M4IOTsqyCsfHZEIH7S6XWEGu+IACMAQanx9
Y9cp+P/CrA77JGNbya2Ak/Yz4dDAxmvx75mFEE4UqEpXvZ4zNJD4X+Ok5ksiJyxQDmLIeRQP4ex4
Y8OkyGqA5SfQO0pkjo21+Exvuy76bJgCds8ptg3UJrcHamWWW9vUsl9keGf6Ze7Gs3KJwI67prnj
/9pcwln9E33qfaZ0LaSaWq+mUV5FWujadVj2rF1MWv+SHZLufOIZvHqeBAJ7eAjWO2h8SqTsTUk0
gIJDKz0V3nwY0Wdk83zXLkWy5yik2zC9P3+7UPEY/cYGC/+T9xhCg4QLT0Y7xUKI3q5BbfmHaDK4
w8uYpRdZ5doKdDhJzVIjHv0e1DOoElawKIHy8o9b81aGjfNHSST5WfxGinQ/cHLQXNx7BWCw2uTq
HdoSw6RJ94DsLDGDN6iZPln4P1cPqN0Q1nIoG83yU4ev0MTgNTNnANjynwy30aS9rIPHZw4kjURA
ATfgoSryNYlY6EQeVRbO17HTOsSTZt8s1dgknAtORHeZG6PsNlRdJw/9bR4MW8JqdaV6jNKZiyR7
/T92bcloKNjdJoZYF1I0Hn7GNJLf0GrRqwl9wKDBHCTFxcfEmfex0442xxsQ7LJLmHtWRco8pB1q
78HfTc+LFN3nXqTUszIvS/e4rJZo3uJkBVuTOrSgs8BTsZps4k+2IexEQu8PKetNXs59ABuCAHay
9jZeL+rAAXAf6YDM9lXs/PUbm1U9y1vksd+hefi98NMZ4ZvqdjZ4cij2F2UJcQB2eL3E1cr57Ag6
kb3Vk3rRqJ1Ystddj5zmuPSz6AVjuLt6twEflYHz7TfsBPBtT7PnBaKObm8G3dx9c2J/mRbk5XYB
humf4lmJl4YYjpu2M18LFQjp9XE7KpedvXzkrEd0baJvlrKkkhA+O73utp2BaQdikm2HK0ugLpYs
5BXUJu0H+tYsRfSPbSnE6+P8xNpYrLP2Q2fGospeHoi563Z5RXZ3r2SGkad0HYOcW6Wu/rGxMkPu
oTROXuf5+9lzLp7JhSMoT6uPkP0qVg8Yf11FTEZIpRet54LKWzIslM5EewH+C3Gdzh5ZqJv6UNLx
wrkuwp2Q2rN+PzUodqqvz62/zhhbm3yIxSkytvjNxr+GSc4eRWQ+q136XA5r5WNep1oTUgXB9SNH
r2U7D2+JOCtJ/zYcgNqj9p3iZSV7+YQixu6SPw3hYGRK4E8TLf3zT5SuXsTADfnjgoIfOIgIm1h+
wWN4qEsROPpvqObavVEud6Nk5orX7lkq66RimovTxEiarhokSefcO9fEnUbDQawCsVcOS2syt+Ca
YiWXujuuQWkxiaywxnrfnbAgfTtprnhxtJGOKXcZTmstTvN2aIXLLTwkOTFf8oUpWIwzV+OXu67z
CetHpzuBSYVVWxdrV0Pa81Cxc9HZpTf3ff6+bv2rcByePT0+XF3jhFx0+0mpnygDR8dzuLilmBwm
45lF3PEw+NO4MfET1BYR/0NnwZAYoWdVK/a5ABy9juz0aZrP57NDna3XkpnSpHilZqcV1J0olNMH
fWU4Y3wJDGuUYVOor4W1Ksfb1H3ILU39NYT8P294/RO4V7HR70j390RN2ywQRyKQToonky6wiCFY
5/WBliapELmrFrRvWtk7HZxXc+KFvKevkVW0sI9mhXs+tq4sISmc+kKxdvdJ0k9c+DE8uLBolKF9
J0y/QyqpOgIzlHKEf/bJKqcd4GWANGvI5JqJdG+tT2hJKfUyL8F9FR6r0dFco5MkzuAc1Xqqo2vc
kRly4G+R0AZ+ZbMf4Ycieesrb3GTriL94rS7a2H3NWrss0ZwI/stY8b01AzJAJ7M3leVOdeffPK3
L9jYjzI0WubqzhPw3AUSCdZmwuKWHbuskherjsDlBlgmZbIF7fyGmQalaODGXbrkXwZ0Ld/doNW5
TSwg9Ol6dD90nppp39c+y+oj20GcUIjSfQYK3yh1c6L05/ObIYpZ5ocq8y+gcif+JNvsBvXeAK0E
13UHXLIc/0lAagiv+P9wFwdpAYwhixY3EBGwAPwEJ6OKoSTLAMRQOIoV5d8hnNfxubzDllgm1hK0
HLofDwSvaGaUVu4EAldyUP4vTGacAhXW7x0Lmayfww22TJ3NnyV2CRpALHwGAMD56wCx6cFcuDqn
j5uqcG7NRzqw7YaxN1lo2zToCAYGyZcKEW4mSmQGkoPMh2WDngIBRRs1fhgtH4dXAEYIiNPlkQTm
ZbOkrmypRGxgCWI70ypaSaJ7FNpVjo/gwJzr1iJmLAR70EIZ3onVxyyfkSxH9yr/4JluqiSFuNIh
ZKSZhZ4L3hRlYD+raWPXTtLDBPVA35g6ZMHilFiV7ZuSMMVpcMwV5wlbG6HMw+r7UBoXYHeRkqT4
o+HsitfzZ02Wv4R5ZR2ZZYffC/PUgVRR+A7Dv29ndK868Hg0rprldioYzHuR3LzMtu3M00PWapUU
uHnWfe8opqvRU1Cw4vP0lmFQaI4I9SdONkp6AP94j4ZvN6+Cilt2OjbwXTDIwrID5UZNv64tT7Ca
zg7M64o2Ks4OYxqd0jGHkGcPzdq4rOVtkgMmWhjPmCuq2OHivRXRnV7ZYIQhMKByckkiRT7HShhD
iToy/MAmPC/s1CJDUs3x9M0Uv2G4A6hg0Dx4i7kdxFRsYQr6Q7fMqFZviWRH2TQ8yNVbP+1o2evw
/8hkyXvmwof9pTV84RQuhj61al2cqAMbp+GDAJrlfnM4fXD1jIG+rxZa0nwB4c3P1nfs2XAEAHeq
YmigWPffa+c/+JjGBh9JjGsaTSZrEeVeF5h/hUe8CrgCoa+2qgvhSkVQd4QdGAVCA1clAghKKdDJ
YtRHj13Kkw4ajKQ1fZ4JJcV+d3Q+yxILgcB90k3XBQ==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KOZNt3EYCLb9bAkT8qHwHIZknENtYwOzqKCKdPc1s9EzkUKtad0XkDz7rncSCgaEfh5beWSmr9zL
g5NMekHUX6XbV6u8bcP3tumc4tGq3LcbYjcK5ZpJjFmi6mUqelzgw3IObt/qZdO3J+X82quaPhp5
PC/pD88ZheM2qIYt16aeS0BygCkB2n/oQ8pjGfMtx9L35sYp3qgZJDH5mpmNzB8MBNNPxlKnuaeb
XkCW1kUsraf9rULXzcWlwyZECmOSPDJkXvqE0JlDnYSkYyZ4P+XgTuBOuRzbCQxOLJHzzyTcvZUa
n0ChOs3SMtXeH4z5L1xcEZwQup5sK691ZfJ1kw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fyNDIamHH4L38sqi9GRCtDQXorFPelO33BYgWnLfdvlLVFB9mIO4rFQfgVGuOvKpzYQsxoBvtDqN
3leH+xi544fsT61rdb4z98pregYr3SebqOtGyxe9F8EJp33V8KsbA8Z7QGmgFbJ0mJ6HotcAQycF
paxaq9lCUhdrsPFf1FXsvoKHTy5tJNxjWLbAnVJ/Jt1I1dHaaO7acR2qFNJGLlXP202RSQ7vvbm4
LuBeVlX1XfwtIzsXb+gKNKLxRl06mqmXAXUECPn5qmQNVfBaNvDOUSwpSAQ7gmREAV/rMnqEfLyf
q1hqES0IJDTAubHF7GUShvwlViUmqlJ7JTFSuw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11536)
`pragma protect data_block
Yjjm0e9zGnxh5FWYtU0VCfOdqMcjIq14h/KRcWkoIXBIgeG1k1+PLcXLEp7TI+N0yi/WmYUwgzdF
PtOs47vyAcnzjqT9S2Eb4CBC9MEUbQCx6mFJdztNdKrirAR25D9jyyiULK2iqwWhSBFG0CM+OfCP
LFvhSglpBQXo8DgcdDEua07goPNIWDx44JCO4B2OVIChKTuJZbpAT6/h/ohq0Y2TLjj2tDbBA617
j3FaF62VUOlgbP1fNZ25uVggsK+2uczoKm0G/8LS6M4IOTsqyCsfHZEIH7S6XWEGu+IACMAQanx9
Y9cp+P/CrA77JGNbya2Ak/Yz4dDAxmvx75mFEE4UqEpXvZ4zNJD4X+Ok5ksiJyxQDmLIeRQP4ex4
Y8OkyGqA5SfQO0pkjo21+Exvuy76bJgCds8ptg3UJrcHamWWW9vUsl9keGf6Ze7Gs3KJwI67prnj
/9pcwln9E33qfaZ0LaSaWq+mUV5FWujadVj2rF1MWv+SHZLufOIZvHqeBAJ7eAjWO2h8SqTsTUk0
gIJDKz0V3nwY0Wdk83zXLkWy5yik2zC9P3+7UPEY/cYGC/+T9xhCg4QLT0Y7xUKI3q5BbfmHaDK4
w8uYpRdZ5doKdDhJzVIjHv0e1DOoElawKIHy8o9b81aGjfNHSST5WfxGinQ/cHLQXNx7BWCw2uTq
HdoSw6RJ94DsLDGDN6iZPln4P1cPqN0Q1nIoG83yU4ev0MTgNTNnANjynwy30aS9rIPHZw4kjURA
ATfgoSryNYlY6EQeVRbO17HTOsSTZt8s1dgknAtORHeZG6PsNlRdJw/9bR4MW8JqdaV6jNKZAY2X
9i6A/pOjjIp/TeWJdDi4xhr183sP0n3qoA/qx1GTmM5NswXtfUeJg5CwLq0kuHr3N322nUEvERz8
arWaAS7my6LSDnAYsouooQqsEW6ens8UdR93B0QFXWo+ExF5E2MXx6bPmbf4/6STPHqdPrhjTQJy
6oPXwBEE5M9eYG+P4DT28V222myT5zNZeT3WoROJicA8CzC4V2dQeTrDsluj3bYcejSXAQj9CWUF
awTZLALR6D/Tbtq/gm9AfZV72LazW3o0O/n93ikva9C8gAh4TVjheX6H1+0kYc5/wbDBFHdw9Dws
OE2XOOhriWr9L3MX4B5Yogf/3kuhyFvxcw/JCg+65IHaJ9f6x29oUYgakehF/+EWQCKUFAZUpiRY
W9qgx2EIUp8MSTaqx0XxjhKuNXXdbiZ10VDC2hjdc6X5J1OUHZAX3/VaE6YSdpvmxN3guxQQog7I
SH1AWR8z90bW70AmfcLwkeJ1VqjItmyDK9KS9pKe+B7fW5gp/+tpPyps5NVo60zxCG2C9i+dswyr
wiyeRJvXfDzOqwUtonTgAZLkRlq/oT1f4rQrduadhbSC9tBgfqqhKNekanaq11TH3UohTEsdsCD1
JEYZ00FgdHlASRQBk8EnbKQTCbaJqvVm1AmXrpkx2xHkBFx33og6zhxQEXeaGXaSD8OlTGItcIw/
AeH3NIBPvJenw4M18AVD1yLQWxPbGaVdDfffl6qL+BUE0YSNmXu+lFc3fN/KyKmJEVEZqN5ORqBE
Ybl9ozYYtkfiGXOcCjqXjzBdEY1EzsMrnHrxymux0GmmBSp7XcK4Q776cInjEdLUZ2BxY1z0orSu
Pfd+OiMZkgdGFhoxhTxMX9s7OP5lJ1RPVtFh7uVPJmWeSHkZGVRFuMnbcwncAR4iWYGNzysNc6tm
LxqbTsKUWEBvO4WzaWr5rFB57qo24VBq6BbFc5F55XR7p+M6LLSvDjP6bDVn3VeYqKdydUi4t932
o6eeB6LRvz67zT5/7cs/SWAYSrBlLBw5t1r+eMbC1t0w5WjGo6Q2WwPYLELourBoA7FICCvF4p4+
JtB0wRpuz309vIzI8sw9QQzpb0IcYASA4lihVoCd693FD2lbhDf5y9SFRuTLBrg5Uay+UNgp/G8T
vTER5ObrkWTPT+9BfAlK9plC+tIrTU6G2xsmmLVDVPhtxxL0qTuxc+/HedUt/obrnmxYTV8aiAUV
lDNuvWrFByWt7r383ixUMY6RbPxF5UtIlVO0D+xeU+NhjOzKlzZ66TwVAZG98YHxowkfX9FkqKW7
lnp2pEztrOm9JbbmLu9xQBlFIPI+nfu5S/RbJSFcw28B0sxu7WQ+VeaMG1503RAzpT+oDRpQcEd4
b1rA8J7wdlky63BMBaj3iINYkLPeO9JaIaAhoDXLG7g5M2P9I6IeskG5kQZSz7XXkz5NJwNJ8Axa
Op8jLZWjUqrG5Y16vjQHcoZUtvUjb8WHZ/IvG7PRHc1UF/sPcRCxy/WPRCVz7A6wFQsOMEmgpGmS
19zpKLE+6xfjiFYyPMIgjPhO2XFshM61PaTrFK41UoANfGI1VV0nQFMMnG+if0DLr9VqSO26cROk
6ZbK1ENJqRAj+Q4qEg/YUBdS4ulVtfj5sm1rYAZEaGgS3DYbhA/gGiaFTY8niZrv/9DGIdv6SbvC
faCfYN7z6niVwPszGEA5XFc8zWUdbftZqRx2tbM2iBMDYyNtzXjqzWhhP6+Eo88j2eh+2EvH8vOP
TAEHH0Kcc86IHnb98z18mY3xL8F4o9bx5FMJAdev0ATUVDZGF08TryqHQJz7p3aJKfuv84QdpiDw
L2ejBg3Nv+rd3n/+RDjy/95Vw8A8TnVtG82xrfPpKVwhONP4HH4lzIBgvF2jzkLKWdRybdy0B1uK
hBQa372OXuPGCZlY69RQhwSsmVwDOg5ZEuxTq2wCeGsksJTshdrdbGOjNwLX25h8uFNKixxgP1jA
0tyHC94tskXzSyexq7bUL+lBTB0PAyCGZI2kC5lsVtRhf4DcRkx/5yu9FPpMedZ4UunZybwlBlCK
y0HB+zMUyMNykSNrJkbfTbKLWV56DBrMnX6nfpBGRNdwdjNMt22uFcrFa75bW377ZzaM1nUsdFtl
xwYOTM7tyQG4JiLPjoNzmF951FockoZ/5H/eX5I93DURGAgIB+B/OLJvkWS5p8rVUfvLbu067wi0
XJPf3BEUR6nyniUIfnP7KsJdspzfKd7sDbS39wckefac6QlTKdWJH1+HcQdGFPK4YrTSG3BS4drK
CBD0uGXYDJGOXbEaMZcOCGU5NoVqktHb6ombz9YxEKkbP72F0vI6uRJ2hK9lEjOHdfXBTchYdcga
ySOl9sKNoM31iw7oKtmS5YgwT9MxcD+frFFohTkEjuoLlg7dLKgEgGpO/nG2nGBrIrFMHNgXungN
ahD8FRfzU4/gRT0W9XLrpExtHw+LUs/uWOtsPrM/n6F4v4VRND44l7lVWemcUrS/5t2vnbsUTz3S
wqa3rKbgdf5YCmjctcjK4sYMw4LLnV+M12bRHWyZqkAcqKp+vxNaWPU9CUv77VGp7RdB+q6aiZW+
OcpC3HJwVOfWqvu4Map03clUlhUZo0AclZWpI3GEErb4Gylc0bny1IyTO13MvpsXWHl9fmCPzKIW
GhUDae3T7EAL4Uy4cfae6KnZktqafuKz3i8NnhM70wnGsOShF18FYfdbsPgeyr7QnpGSQ3+TgXtJ
TWA/04ThY01IG7DLV7Ayn5ARwpw+bPUrOlKpVsX5REW0ZIYEd6AlQLyg5kQTAzqhFnkp0kBAys35
LbyE1sB9KRpv96ArYQGJ3sAthwUds3WTBSXapqalf7FpOUyOkNrPsdJohkpRhE1P2hV6xKr5d9w9
m7jZ0TYewfcVee4eQzPGHcFYvzzn6Wr6DQGJp5gJBYGCgzwLiViLnLBfQOwDbJRiKhD9jFQV0ONf
0borE4nr42cRHV5PWY+rjtxxGzIgsD4int+xnRvpLunOy9pJd8Fqjc+XcXirBBI0DzrvOKFvFQ+6
RrbTpZKGGNnnfJPbSGRWtARE2G1mkmpCuRjchfekbLhRp/jiDrnfYRXsMOf7VY5iw4y5uO3qGjQK
QO7i+BBn7Thf8IMHIB2/hMC5tztbH50yU3ht5kj5zYySK0+kTN4nNheCyoCxK/5l3hu7k6GUXytv
XUapMrK6+eeRCezcuYVC9UVw5b4bcGyOKGi/xrZgf51GNB90CoeTg/sUghqmrEfwSJYUgTkqvXgL
ItNPeOZ8CZd7j4/Oh6fKuIsYLtsAKC2F1xNcMpZQ0SPoDyqbkxTf6lDjwtA6orwwdg5A8VmjhjcF
7IVyXzxN2CiMLPaIb3dQ3Ezz0wJ4THYx7OruMEa6HFqulMnlteNHkBLcZo291IhUETvtgJEQiS2J
s9dBRLrC8/YU0sBaUQFenquDzatBePxFeTJkgd8lXE4bL/QBOFkO3ai8Qusm/bA1Xp0pUFW9x0qH
sVMXDKzXCXISqbAMW9zhNfFkqGqIXqwTgIijMLk/GrBxDu4XuSxGKeRIpdkic5tV1ZIPNTC0uz/m
8QBdm9cdbSJaSBtPX0S3EJyydMuPb5UprQvMth9O8pxdcBLRuPZjv8Xqeh1QehcQLUjRfwNEd8xQ
x73gjfhRiDc8rbbLINwzBuVddFuaqTi2VURxDCZ+ti45Yg6hb1LkCv0dQN2DdBhuWEM1D4e38Hjk
yUNKz/CKms+GttKut2aa4N3opAkzuEIDknY4IgCZdHI+knmktzDO/0M8oFZgD++c2YD/hAZV2/oG
HjRjA0wg1UoS8moyyK2VYQli8SDQF0C69tbaJtvsZDxlgYUNv1XxghpiaairKKH1/Oqbl+9Bm/DU
Z6othhzDBtmC6g82+26UbiMP25wbGCNz0T7xscC8AmC9VnJX5VsGVh9lRa7UfMFFnXATQAUoFDGg
vxckSlUTYgMmykvyuCXioVcJwr86uq/tXD3gLOl4yc75qpNBS/W6Yh8ZwzOtEq4o59gL0W3fFF5Y
JuWK70Q4pet5oZhrZuUrgRLpwlJgnA3H1KkLkSoZhm/m468h+bH+dQDxsnsoHYcGWLov1zt6f2wt
KGUy1WF/8DO79R1rxo4xNjA5PTuxY5J60rk3+Vp7tZhlGzv8dtWMFAMDuTcF7rD4J5bCVRV7g6tz
Wb7QlRR7Q9sBlooFwVtmKC/DhivYP2vaHgr7ZFt/9d8cUCGVSvapVje5EV4maD4qb9QaJu3VU6Yh
Bflrk9VJOkIoMuFRMPiQ8CXEYwtsonS7rIPOZSuW+iFDZcKegyxb4gSQ3avfOKuFxSvSxV6nad6g
7lqrhldSoJ8lJePujqYVXtHsD57N1JNGQKIgSP8WYSmqed9uulo2cmneagMHQHQg146qK/q7mULX
gsrxRDBLceKfaL5XQDWMRibVt8K7LpT/jWw2aIItE67WjBoMuIZsVV7QEgyNZ7nchqXGgcgoUiYN
alZUlHNH3v/feomt7piUcjNFW5+Knm3H0BzoeyDEqvN2rmyVV+oNCSKAKjwqihck5pVcfiP10iGz
3ZUOLBK0Nvk5TTNvPQ1fQ2YagKYhtgPh1aj9eUwxVW8YPtnl71Q5sgfMABU0pmeFoXEE6kLSVMby
c3KHGWifGDUGygKKUQhJk7V070eQgEmXwHFGoqWJzocLIxzAglsu28FhCda8wcyoqeJnbX6sFssW
svFVFiUs6wFsqMHH9kQZtDlV8JWoQuGegZs6j2dTu3P402gTkKR0+UBI6LxhHYNIKymmlAcDmfxk
gT3p++pBTzTDoaco8B0P0RY3Gb8SKPk+VE+bP/12IHYrabX4KbBoFGqSt5lW847LyL3T1LZyTEur
Bymhm+EO0zWns+h6qCrp8EWt12xvWv88F4D4IJlI+lZnqXBIZIkHpMUUmlwMwV5DARToAUFVzwMz
+c+SiseeMAzshc+CS8MuVH52eRqfsfSBnUmeqoj2sdLtgo9zADUdreLg4B8asOZVHJ/SSez3fs29
CoAtjGB18rMxvtUxTEG7yuO/bKTOJJpOuE3QMCi6TW1Ibl3+EdD5P5vXnIWjfmV+ujkw1ATX2nCV
P727vhmcs1Wg0l6SqfMhA9LiVUw2OCR1ILArgzPmzqsVgS7BkE8lEZVNkGtHMsIbvzEhSDzDmdKs
f3EnKRFG5M/1WWDS6eXtVhmVxOkSDk1jozj4rQokeoIDFHDLzgCHkjiohxobWSiaRwgxsAYd1vHT
THy9HX1L9l6vwA94cFj585WSjG8e3aBf0mqHxPSNs274HOp4zPwV0yhM4yPMcN2IDJ0M2Ldpdqhu
0Ymf+cEhwoOy7cSB62ZFMtVbAXHuHIiU08c0PTXTJmJg7JxT3n/9tgcyxDsl7TkYxw+0Qrc/StnA
0i0/qpBi9B89b9IGeIHnGaUPl3Be3qLCLPMxVmevJhZHVMQKoFhcWI7LUp3Ws3QgOvMeAmqph/g1
gWKF003R3da9HecKkGmrNT80nIVk5jmdNU1PH8L/NwrY9Y5YS5ViPB60CNROxBvEkd3WjK+9I/+m
FMt6r7pnt6hSDRCA9GHEoJGh8344+W80zgWtiPYsD3/+tYWLfMtZZk7KMeRZsT7I1H2Sj+0I8h4J
Ftd3aB6YFWlR86iJ25Ti0iDaUFnSTtmqCv+hIyc7yUPhA0c7WGzKuezkSZ/1M7bGeMN79Nm8oP76
5u+m58/YqTVIODtpME5npJFkcYkEINxNOobSrrERTJDX6oozpbltXPj6x7RhF2WKMQBjHYpSPheO
nZZET0kqDnnpBwPTPBum+5HEDp45W9SY/mbiaycSBppbdF6VaOMgjUqHz+3KtXHo4L1ee6DbKUAH
VjVWqjlCoGIgXVZt+KbyNNrnfyjq27uLPfiXEJcc/pf8Gkn5+r1LpIZYMxrVi6Kb4VjUo/LK5yZZ
79DW4FA8TJfFgAAoQzc7ZffYkHTHMQCtx+DNkGEVkA64iotFT0UQF+yhYqg3xKWWkD40iHk4Tl03
/jg/PXg7j6eIU6VTRuRfDgQacdAZBQkX3rHHjbf6CdMPUrJDfYZ1fTFrIUgCds1NCtiZd3JRfam0
FNV9q4sCJhQth6Ts4BPxi7ec5SDRPdyjTwARoKdNA3VTGZ8aHR7qQyP3cbx6P5Cmma+jLtpWiohs
1Ns/GgAV71awJAgWC+bLr0NNWYMvYs58uYedx6dRCyh72V0jrMPJMgkJHl3zQP3GuniSJ52twMrP
8g4BDBwU5xsp6k95Il9tkwpM6ZmZB2z+2WY1ETeIkOaci/uSPOvuAZEOKvTzn2Fg2uIhbCgiL2gV
+6fBFo5Z+ZLyk9LLTuN9eLn5Fn+3noDf8C36NxWqfavO1cUGdq8tPvETFbpqSLKahe31kFxsOBP4
y0NZBysZzKq6EgXXR2YWbvYmZf3hDmRRqyGIVefm92gbwl3AQlSIg55882SOYPWeWwUjaSa4dFby
Zf4ldhHggTucdvi2mfgZopxJrd/UyUvaKpQDpy7uaAiY7zYevIjGCh+g1Kcf6DOhg4+POwmyo72U
n2rBohI//t6H/lceQKtyairAHLAXgZH91lBPLQTAhG4R4fd2rybLZxtCLqWsUO5q5DWRgQGKvscl
QPZhiVeF5GAjJpqefb4PrlL1tq87xdvlJF6jSL9Dc+fG6/Jui+zERUXUV/WD5gY7Jx53X4D7YLIF
Mr1kV5ugUNoYbcYrovgBnDFnzvVAYkqskieTQhnV1Z2AFwxOHsZ3skWwW2n0NwGsjy14XP3GGsGe
miUZUKuHWysCK6PfZpl1BQj/uWTZRzI4oyV3J1b5P4b/Og58wjxEsm0ua0XzZipsG+/Y6B1fHtlr
1sNayHSH4QNR51mqqFcSvgGiW6rE1oYwQtMzqDwQi9O9dclPwUsEQSEF5UyP/DwDMcwj3divErj5
v6DDOP3fH7S0bRY2x4hPlFTtc+CEsathV33E7La+PifqfK9CiOUJtV97Eieg6wGZyR2h74rF4xYs
FbvyLddgC7BoyJKNBoRDXzjqGW1nQ59VjEV0EyYVw/jyUTZQETeZL6BJDZGIZY+Vig1VR5uMfLEm
paOy2VmVkUseMJaHiSv5isPAYVbwHG2dLLywUls2XnAzF01PluU3GliManp6ePiqYLy7ReyfSXfj
SwMBVoxzRVFnDVjikExbIsmGuLmsRm+gBN96NtNurLfC1mN9xuUfUNkffKMD6/GdTBcznsTdcpmJ
QcJkEbtJ9sl/ayduDW2zcNBhr1kVAc3icH6D47W2Df2jejc09fEwMpMJHUsJdS2ISVh34RjyZuMV
8RC+sRaNhCiF+Fo/EpZPNdeCgFd/NgVbTgH/QK4U0FnzwqPngB6//mFORptulbFmiI70fd0alb7x
2Wh1cy3e+DlOKRfE/lK5PFL6pAHiHG/tTS2a4bDnH7kzpCL9WTjbtYW7fVoT92yR8nYHvI395Xnb
pX4pAHqUWPT/yPgzicqKP6xK3y4jIjcocLjy8yAdCdXRQ+H28u0T6tuBOv4hRq0NaxwSGtCdtmSc
C/QuhFub/lS1MMH+EW4c2LgG1SQ/wkupdVMwdNSWv5BRi4IN5eqsMfzm+0n+uh/SqtAucjyZ1Ju3
JQsWE13p4UizGE+aYnZhU3yTbIkTbMrNNNMKXbdNHadw+8pbEIHFUsOOHbJNke4geLJaszVeR63m
W2z0djoJkf4IlVi0biHrkkDAqYRjKbqURlli1X7V7Jm1jukHrpozaLRySFA0PQYQpMYk94qzlZwL
LDwzg0WfYfZLW7tU6DzR7rfI3pMDe+VfWyi0VKjfHr1yBbhFiFu573rg2rfiW02IoX2Gj85JgoFc
grRzEyDFnn6LqTlTTTEtt2feEq+URPRPz5tdWuF2H8uc8XViEROChpPP721pY/ykBhG5I/XqbRml
uuMbkwoTN22bin8G7uH53WxcvexCz3matWqtmubkCwWOLqel5R/gZhW+PUy0dbtwNyWmgtJX2iBL
50L9Z9DnAHGAxFimpcGXBSzXlhYaV1hF08R9rGsgjaHxGb9kMEU9s4HDIily7RF4osu6iXTW7SJR
8xoBDJVRLbLcfuGKQLlLQ5GU6H4/UPYHrCmL2hpaFsZyw1zmlJ3dbxiyUqnaNBZtzmsrrmbB91vI
Mb0suDIMT7ukM84NKNMyRHlCOyMdTQ555WpnfkGBuXTiYCEmeVfxWXzfdBPpLdg0SZ/ZBgF6bxL/
pmdNNSbZmcgWl05cCBjZgeU+x2bB9z+Au5DbNMrGGZ9jdcEvqYJHzgbZEw0rkI4iGM8PVgMsbg0K
n9wbXrthEIzRnWEIVw5IbaM/0RBVT+eXGixhcouDDUos7e7GczvFtq9Hxm/jANoTo1l6IjfwTfvh
UCJnOcsdn5B5g5T8CDb//2EUb7WVIFlbIG4WH6YVsBIVOnI1debCFD2Jc5FxM0aQ59AlK3m50EsJ
k5UtLSXK/HJEMLXz66jm6/phIyh6uB6v+mAKm2Qpp14s2f1tNoWy5yKngy15uJSDIXYyfriweCgO
iHPqyQsdfaP6cY9jT2EmZQr6plYphoI9k0ZhBIyHKkzKPxAswvXsg82NLKGkPCn2mfEsAbEFlj7j
1eOBtaOxHO2h7yVvu/Z0OgU4qqPp5PlGHA4N4vzrDzJT4Pp6hp2hbbx9ra9rYpi8PaWegT7F9qLp
ff9FyuKyMjny58xFw7Fgy5pKmwH8IsUX8eoX8fWfZfYguYdn2GPcZD9A9sSS393kqzRvjLx0VcKd
qZ3zbskAaGg2UK8OLVab7HJ4igskIKaihylPwdB/oBy5zr8VjVi6HkUquoWOi0MQxJyp8R+ieIfS
kqauNllZQ07dy0bvbg8C5Re9pZH7616eriR3ujwqgUPbuLsCtnBUPf2SmCSmGvVdYaZFJL6m89ei
o3WPzmlrPmP8FQgLyvNiZwv8yCm7P0qKYtYw/ozi4OMq8twsvZ3U8RTx5jIce+BYOUJwD8/2iDwB
lC/7gAiQATTn//p27VUvJHP74aVSuXlm5zH1bPR4NjX0EPwCNRKhDj45P/mZi8CJxytJGjDBXTRZ
TYQp9JmysmkYZ1bF2RQRAGBw8w4aA4IxlsUwxblozo0MqVMcY5dvBhzKC7IsPm/duCQsNSkLiO+D
0g6GH0579vz7pEqnuEldPjR2t83A63MeI1nX3RczhRkME77bL5SFZ+ajlefkXHwIadR3BtZe5q6W
0/+UcLu576Ik5KntYaX84xgbD2+j83PQzvcJONxJ/LsajIM0xqUPy8N/emFTHo/HIgvqurADCIOW
2qByPVBlfEfAhHX9n6cJxAeP0SFVs0X7XkwwcNvhcadcGzf5I9zDqCNcKp0xQem4d9mdSd5AD8Qg
1POldVqm5NgmHi2iDNy6uk1nHUlfN/Y8+tUpvCgE4rVWjAMdrqerK6YjlG6mMlPaHtvVO5ac86BX
HqgWdKsINODJBkC9XFW/8wk5CEM/11Ts3wtDf314OAlc4OvWpE7YubElsSEkUvUxitdXd2voAuru
Mr1zT3SfGTLgtXL55LTvjaPlZhexGrXXFKrPqBqlinwHetNz8VaOTsm+iaAnkrPdL9yoFIfFPVeX
Kn6MPAY0UAuJscFlPqVVmaIcoGs7pcvwgYGw4c8ObuyQd7UDlKARdEmYpHg+dpCIQeCUJCLaFPoa
h+ZBM2BQ3Kb86KIcQ6T17migx4rjXHZcRwGZdIgHhrfXhfhW/T0XrocKuvw0iE1h9tl7VOMo7Rrv
ulARA4/nLAvj3l1T2UmG+jMpGMcTQd6Z7/vH3/en6szxomxZToYp20Zl/X7wnq03RdiXH9sK0fCq
7sooJuhTs+MrpR7PHi/BX77isTCu3cH8oqrN/sh+JVVlt4fpiL/98wMuuizeokkwUZu4hVVX6Rqk
DiMEItAEy1j52sDE4fcDKVpwJ+KALvBlQOorBu41Wjl2R17tsrsV/ZvoaELJ3CKSsV2EouwunlwI
j3NQuXpPCb1TWg63QEmEzw+wA6zC8timNJPOPqKGGWMEWuVT//3ayKyrxLc6XrRzZzc1fDlPegKY
PaGycEH9sWKnJ47A/iSHk1pqvHukxnTW5WQSAh+BH6XEBQ0RXgsASTIpr58EZQu+iZ7ytVrl/rSD
HJH179t/hs+YZYMzLI1RqofjgJMLW7YESYJMEm0keC7eKs9DZl2TiOBsxDWGx5gMNLvY6hwlT8Ss
zYNfJMzbsXVksuC7kcuZCkUXcnvUDV6Y2Fu2B2oTuMMlnxSNSe+qYZtCfjj2g+2M3YeLN8tPApVk
wBvX4M2bAIPEjEciWNTqR7hauKMKxtc2x3ST8Uuqan4SHL5VCNcPFT75VoLhCJYyloAqAcgjIjKg
/qFJRl2r71/sooGeujGt12NHhsi+fO/7SrddDxH4yxPnLO26n67QkcgzjlGIvQ6iOrqeV28Vadp2
RGOy6vOwDy8TJa7bJ8howwdsnN+ML0dwoyrtL48eYmEFbiCO5ucOQphEgdEqjL52G/+JJZz4+i+k
3AFegTmJJyraq9fzSBkQzQdLT5zbfJ5thZutczMuqxlQsAVWLuxuwVm98oghxfNI8C3t4T8QczML
80frE7LOcFZN43t1jBRd4SSC958j6UM2gNLzAwbH9k4FBAtsY3OIIskKmMIbeSkaRUxnHew2/dRx
6dbsHTcYsDv4VDVuqmK6wXIVsODRt5G4e+JILiq8deONyxD4JTq7FiG408Z7inPjui3ClARuvlsx
PjhxXk8EdfAOkT99PREIKb+Z8vUiayPcfyfN6x1zIwMGDTWXE9IG8uk9WPk2hs3IXy7SY/wTFw0T
swO2cUUGh+kms/VzxFoTvhxxLo/JE0R0iB+x4LjF8ABsboKlnMX47iVAJFTzQyNRbEbwvdf+H13H
6kwZw27+HFTL/WNuyYoTQL9UvpFKti19usKQuouhH1vrtXUKI4i6Glxx1qAVZ4pCYjVt3jyIL8xm
4jvLowzFwa8kPStxjlFNO5uaZNYwa+HKXh/v9OteZsXKXc+G77VkAy4Gjd7nZBB6VfTSd4wGBt51
woPBPjiuyyW0fMvPDY+5KACjURqDd3M56lWjhlNSpH4TkGEpacx4qbTEHhbMpOMVhp35hUkRb9KU
GKMKbNQCsBHzj79omr/JeOtkdy0YuTtTva0siyETu294c/EE5G8DiqxpaORKzOLpDM9CNsx3FP3m
M/Y4i3biHxnXaPr14YMDSak18t5MXE6BImbVrKSCC+BMy7OrbxUx7n6iSAU/s2uiObD7E30UrbVA
PT0KJwEAR85qOzzQA7QMBfW2IME/l1LVaYoSsGeRbzDMH52Dl2iZ5ENFStYkcqyGbj4ktINI1Y6M
IMUNECteuO2a+BSo9/UV9Y1wJA2MWBXPbSaBtGM6pUPFaW3yAtLE4AbrpmTjZe8mdBlZh0LqzfBT
///o6yjZvdvnl7EYzGAgl3HIBMM7fQcMD4dAACxnEoz1rtyNf5IH4RKTE68rCxCfbApRDYjXJE/H
neLrYwo8bfJtRLRaX3aYmkK/5Dg54TeEU86NpEiJK1JVi5vfcAVKyZhst/wM05E+bKVGXzqEwcWI
zZgOqx75lVSnNhfHtr63Z9FYrN5CTCCbnNoN9OskLNgc2/XRJ7zXBJIP/hJFijKPpGW6jgT2wmYP
K9HHXUm5UWVa+QL7whW7p74+iXYoTU0s5rL8EhFL5V7o9YI0oFcZ4hsik3SlOjZBqI3TWdoTsKh/
21OBgJzoeZFR2rWTKrtsYcP/fLcxfcDfZgWkRrG0K5ZkrrukBqaSD/kfmvPygRX4mFwvimRMrSth
wTU2yvJINA3MP71tsFHl9Svtl+w5krsp6rBLv8yNhAshohB+Ep4WAHRdiuRnuJ7nOJuz6r3Xdk+n
uK6YkQD3vKbMRkXA1e77b+vt1NTguGxeFTsn0uMtYyP9VZhtwPrAsEmrrWZIyuKfm+c7pf1c7NrA
7P3OMr1VAVr+G3wytej2++ugHXeT/O4jpWKhhGMGWODvh7soulp7V9dN/3+lizOQ3Lgx/Jw59ty2
e8IesGnXPXP3WDN/40OJ0RoxpFHpikWoD/gbzd5TzpWHnrwbUi+UHMvoGYt7g/ruFXPMcohkkuAy
FcXTbGo+qIoL0cMkWv6P54rFxfO9XDtgC1pYdBj6kRCIUBU2sgkNXabgfPErpu5xgHHPor3Yx124
H45lJuhPU8HNR4AzPomBXkAiphoCK9qGNJ6QpQQUY3SCJ64PAurddryK86ffZoaPom4WpW9vEyfb
eRdkY7qCQ/nqYK3q1Ly3Gpc7mGkrrO6kHOSv2hcl/R9XbvGdjGrhRMXBNBi03XKWXt2xBqiazLwb
mTGd28as4WtTo6jrwLKDI7MKA8iJeRq1z8DLGe4rhFCS9IIC5i8fi7vDUs2Y2iaSZRddvaLlKP1/
spI3lljuELtLPvS/0NAghvqy5RHvRgjmnt8njuTaLLcz3XmSSqrne/0mhorhqvh3EFA25bM/DeKJ
ffOrEQSzOcDL0EaoYtMN0m+pZfmA/H2N1L4C5eusaeac1AJkiLygWUJ+jEIM7g5HKiK+onk2nC5O
BWIrvDbhc54zQmz/v5fOIqtxMlbVizyiyyRfWv0kVpbSRtMGrr7attxehoc/mhEQtI+utQtE2Kky
BdCKz5IrfA0RdqQG9cuuWJ9D/wlHo9obiaT0NpUf7UrqONNYXANLNyjmsZAFyPlaudcTEAwN0Lv3
95EZKM8awK3m1biFh5TCn/Jgtplfvhx8GMXOrBc9xLWk8B0Yll7QcjllztimP5bk/FlNnO8jOfo1
a+ZLK7yUJVa9+dn3RLaZMjxRMFq9SJogRQyJnsxshI8uDFJzByQHdmTGfMrJXW46WmA7032GdkuH
oc8zbpJcFh2oHGDVusOMfJhmd4uOkO1EMJVzFlJSqwJRNLcOw+dd2DQdLKPg89N5HmuwwsJx3m0o
dSRwWD/Q8H+IqUWujWBvt1FA9zZozgMDUnRbqL8l5uCYbw2zg3NXcTQJp2OwSD8/eyAkmVSaa2Ld
x8qoLcPYw7Flls2hEHGXhxqGxIkL/iG0dV6l8KTrhxabypgxbCwibe/X8W/IM5+lyWCAfd92pUr/
ZEnnTJgUxG4ArjWqj+P0HjCclUterVeYss0/UNO+fOrYZDaFdyg40QuqAO68X+EnPDL3ghl9X4li
hPO75Faggl42FyExrS9JLI69kc5FvAaiKF1E9Kx8267o9vlWPf+kWP6eeKWpKOMs9DwqYyG+6aJR
HJXxlvsiYX+VpNKSBaaxAzJaxkM3n3PqJHoUe8+R81nJojtluuwM8/0yBm2QUF38Z/YesYdRFJgd
dgtQgn4qpd7ulsT5TwNpkpppaSb3UVR4cphmHrpkZiidTCiN5Gj1n1vbgbnf9vbcxh4TtYloRIqU
fbeiaFH+0yBgw0te95TF+PU8lV2S89UiUX/2z5popMP1OpapRLYK9F16NCFxxFNojxwC839QfiB/
/JdhnS3QePrj4k9iZ6BF0YIWSDluy/RaqHqM4q+MhvnGL7cduyEszdaDZkh3txg2p9lpDZMvZu1b
8Rncscd81AUJ0LrYa4kYz7amYc2Z5T+V5mA5hj0+HxBMe2BjzpmJWbnMJKNa3LWdKesIPFGTkjoG
5DYqn4ASEao3nRQKpRFyU6XKdeqfLfZWIFQAvO3TQ1AVdPAh5vvSo7DJ6XMNen7iT4X2J5vDWWmZ
ebrvQKBPsuSFtwIuU5q8v6G6ihmBHspC+HzWRFs+l9/0TYKjWXegPMO2plI5+tWzqGKmRpN6mCUX
CSemj3lGLVBCKQeSkxA56IH1LkemRUrle0TrDfI5BQjWETPnRQJOfrk/YwaqbWxWF7TPTqnHu8/C
Yzxz1muKUxUvKR1Kd1GXlRdlPpQo0ndsa32zgS83OD6yaIMsx6pvmnLDXENf2rWrNeZh9KM1W1yM
tykR2okK5ZlfacTn+wZOB5XIvBmUs1uTVTWRyyizzXmzQGZmhv8ocSsmboBuriu4OgFDRVQXyAkf
8W6aqmDLQiPRNFUtEkmpre56c7nVX9odWTtVSCVVKjx7+miqZ4VcjKRvjiI4t5eq4AvKxrwXFE99
fTDTBDg9dpjtNb2ocTEtnZHUUahXKDEBe4RZOVjQpLBIvpJbpZOIW0TXgAw2BOLP69NSyrwnRL8C
vUuvqiDuoXiXf2fCxEpMo8BWAn7Ae3R4XXOtgJDgDeE27GyuVwCGD8lziXEQt2S+688QjWrn5NUZ
wYMmzINiEGG3hllvFL8pCtDfAYnMjadnvJoF8HqxSLY4Z+CIiuMVlWdyJC23lMi2UV7jjghmlZrp
UT/zvRI6DouHC7dl2yeoRFm7KIcJbyysQClLXH0iYyROdc9JFfjhXFRovyW5nQLLoOLk5wdVzI3u
oiDNq5V241dESIdw3+91SLSpUpFeprsS1L+K63fQrwL7viP8ytBZ6ovJT3piQALJ9S3LC7Qhmq8M
Ce/dac1k1KGE718SOrbeO4m11X4bxXpIkM59mgGwWzFHjlm/JcAFyrXEiKYJ8fYE3K0YG9GoYp0Y
4t4V9UuASvHEfERvjwfYXVLmIP2Nzw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
