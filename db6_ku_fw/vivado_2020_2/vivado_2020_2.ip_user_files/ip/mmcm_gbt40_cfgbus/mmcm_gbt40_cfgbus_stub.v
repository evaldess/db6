// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:17 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/mmcm_gbt40_cfgbus_synth_1/mmcm_gbt40_cfgbus_stub.v
// Design      : mmcm_gbt40_cfgbus
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module mmcm_gbt40_cfgbus(p_clk40_out, p_clk40_90_out, p_clk40_180_out, 
  p_clk40_270_out, p_clk80_out, p_clk160_out, p_clk280_out, p_daddr_in, p_dclk_in, p_den_in, 
  p_din_in, p_dout_out, p_drdy_out, p_dwe_in, p_psclk_in, p_psen_in, p_psincdec_in, p_psdone_out, 
  p_reset_in, p_input_clk_stopped_out, p_clkfb_stopped_out, p_locked_out, p_cddcdone_out, 
  p_cddcreq_in, p_clk_in)
/* synthesis syn_black_box black_box_pad_pin="p_clk40_out,p_clk40_90_out,p_clk40_180_out,p_clk40_270_out,p_clk80_out,p_clk160_out,p_clk280_out,p_daddr_in[6:0],p_dclk_in,p_den_in,p_din_in[15:0],p_dout_out[15:0],p_drdy_out,p_dwe_in,p_psclk_in,p_psen_in,p_psincdec_in,p_psdone_out,p_reset_in,p_input_clk_stopped_out,p_clkfb_stopped_out,p_locked_out,p_cddcdone_out,p_cddcreq_in,p_clk_in" */;
  output p_clk40_out;
  output p_clk40_90_out;
  output p_clk40_180_out;
  output p_clk40_270_out;
  output p_clk80_out;
  output p_clk160_out;
  output p_clk280_out;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_psclk_in;
  input p_psen_in;
  input p_psincdec_in;
  output p_psdone_out;
  input p_reset_in;
  output p_input_clk_stopped_out;
  output p_clkfb_stopped_out;
  output p_locked_out;
  output p_cddcdone_out;
  input p_cddcreq_in;
  input p_clk_in;
endmodule
