-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:ip:system_management_wiz:1.3
-- IP Revision: 13

-- The following code must appear in the VHDL architecture header.

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG
COMPONENT system_management
  PORT (
    di_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    daddr_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    den_in : IN STD_LOGIC;
    dwe_in : IN STD_LOGIC;
    drdy_out : OUT STD_LOGIC;
    do_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    dclk_in : IN STD_LOGIC;
    reset_in : IN STD_LOGIC;
    vp : IN STD_LOGIC;
    vn : IN STD_LOGIC;
    vauxp0 : IN STD_LOGIC;
    vauxn0 : IN STD_LOGIC;
    vauxp1 : IN STD_LOGIC;
    vauxn1 : IN STD_LOGIC;
    vauxp2 : IN STD_LOGIC;
    vauxn2 : IN STD_LOGIC;
    vauxp3 : IN STD_LOGIC;
    vauxn3 : IN STD_LOGIC;
    vauxp4 : IN STD_LOGIC;
    vauxn4 : IN STD_LOGIC;
    vauxp5 : IN STD_LOGIC;
    vauxn5 : IN STD_LOGIC;
    vauxp6 : IN STD_LOGIC;
    vauxn6 : IN STD_LOGIC;
    vauxp7 : IN STD_LOGIC;
    vauxn7 : IN STD_LOGIC;
    vauxp8 : IN STD_LOGIC;
    vauxn8 : IN STD_LOGIC;
    vauxp9 : IN STD_LOGIC;
    vauxn9 : IN STD_LOGIC;
    vauxp10 : IN STD_LOGIC;
    vauxn10 : IN STD_LOGIC;
    vauxp11 : IN STD_LOGIC;
    vauxn11 : IN STD_LOGIC;
    vauxp12 : IN STD_LOGIC;
    vauxn12 : IN STD_LOGIC;
    vauxp13 : IN STD_LOGIC;
    vauxn13 : IN STD_LOGIC;
    vauxp14 : IN STD_LOGIC;
    vauxn14 : IN STD_LOGIC;
    vauxp15 : IN STD_LOGIC;
    vauxn15 : IN STD_LOGIC;
    user_temp_alarm_out : OUT STD_LOGIC;
    vccint_alarm_out : OUT STD_LOGIC;
    vccaux_alarm_out : OUT STD_LOGIC;
    user_supply0_alarm_out : OUT STD_LOGIC;
    user_supply1_alarm_out : OUT STD_LOGIC;
    user_supply2_alarm_out : OUT STD_LOGIC;
    ot_out : OUT STD_LOGIC;
    channel_out : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    muxaddr_out : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    eoc_out : OUT STD_LOGIC;
    vbram_alarm_out : OUT STD_LOGIC;
    alarm_out : OUT STD_LOGIC;
    eos_out : OUT STD_LOGIC;
    busy_out : OUT STD_LOGIC;
    jtaglocked_out : OUT STD_LOGIC;
    jtagmodified_out : OUT STD_LOGIC;
    jtagbusy_out : OUT STD_LOGIC
  );
END COMPONENT;
-- COMP_TAG_END ------ End COMPONENT Declaration ------------

-- The following code must appear in the VHDL architecture
-- body. Substitute your own instance name and net names.

------------- Begin Cut here for INSTANTIATION Template ----- INST_TAG
your_instance_name : system_management
  PORT MAP (
    di_in => di_in,
    daddr_in => daddr_in,
    den_in => den_in,
    dwe_in => dwe_in,
    drdy_out => drdy_out,
    do_out => do_out,
    dclk_in => dclk_in,
    reset_in => reset_in,
    vp => vp,
    vn => vn,
    vauxp0 => vauxp0,
    vauxn0 => vauxn0,
    vauxp1 => vauxp1,
    vauxn1 => vauxn1,
    vauxp2 => vauxp2,
    vauxn2 => vauxn2,
    vauxp3 => vauxp3,
    vauxn3 => vauxn3,
    vauxp4 => vauxp4,
    vauxn4 => vauxn4,
    vauxp5 => vauxp5,
    vauxn5 => vauxn5,
    vauxp6 => vauxp6,
    vauxn6 => vauxn6,
    vauxp7 => vauxp7,
    vauxn7 => vauxn7,
    vauxp8 => vauxp8,
    vauxn8 => vauxn8,
    vauxp9 => vauxp9,
    vauxn9 => vauxn9,
    vauxp10 => vauxp10,
    vauxn10 => vauxn10,
    vauxp11 => vauxp11,
    vauxn11 => vauxn11,
    vauxp12 => vauxp12,
    vauxn12 => vauxn12,
    vauxp13 => vauxp13,
    vauxn13 => vauxn13,
    vauxp14 => vauxp14,
    vauxn14 => vauxn14,
    vauxp15 => vauxp15,
    vauxn15 => vauxn15,
    user_temp_alarm_out => user_temp_alarm_out,
    vccint_alarm_out => vccint_alarm_out,
    vccaux_alarm_out => vccaux_alarm_out,
    user_supply0_alarm_out => user_supply0_alarm_out,
    user_supply1_alarm_out => user_supply1_alarm_out,
    user_supply2_alarm_out => user_supply2_alarm_out,
    ot_out => ot_out,
    channel_out => channel_out,
    muxaddr_out => muxaddr_out,
    eoc_out => eoc_out,
    vbram_alarm_out => vbram_alarm_out,
    alarm_out => alarm_out,
    eos_out => eos_out,
    busy_out => busy_out,
    jtaglocked_out => jtaglocked_out,
    jtagmodified_out => jtagmodified_out,
    jtagbusy_out => jtagbusy_out
  );
-- INST_TAG_END ------ End INSTANTIATION Template ---------

-- You must compile the wrapper file system_management.vhd when simulating
-- the core, system_management. When compiling the wrapper file, be sure to
-- reference the VHDL simulation library.

