-- file: system_management.vhd
-- (c) Copyright 2013 - 2013 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
Library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity system_management is
   port
   (
    daddr_in        : in  STD_LOGIC_VECTOR (7 downto 0);     -- Address bus for the dynamic reconfiguration port
    den_in          : in  STD_LOGIC;                         -- Enable Signal for the dynamic reconfiguration port
    di_in           : in  STD_LOGIC_VECTOR (15 downto 0);    -- Input data bus for the dynamic reconfiguration port
    dwe_in          : in  STD_LOGIC;                         -- Write Enable for the dynamic reconfiguration port
    do_out          : out  STD_LOGIC_VECTOR (15 downto 0);   -- Output data bus for dynamic reconfiguration port
    drdy_out        : out  STD_LOGIC;                        -- Data ready signal for the dynamic reconfiguration port
    dclk_in         : in  STD_LOGIC;                         -- Clock input for the dynamic reconfiguration port
    reset_in        : in  STD_LOGIC;                         -- Reset signal for the System Monitor control logic
    jtagbusy_out    : out  STD_LOGIC;                        -- JTAG DRP transaction is in progress signal
    jtaglocked_out  : out  STD_LOGIC;                        -- DRP port lock request has been made by JTAG
    jtagmodified_out : out  STD_LOGIC;                        -- Indicates JTAG Write to the DRP has occurred
    vauxp0          : in  STD_LOGIC;                         -- Auxiliary Channel 0
    vauxn0          : in  STD_LOGIC;
    vauxp1          : in  STD_LOGIC;                         -- Auxiliary Channel 1
    vauxn1          : in  STD_LOGIC;
    vauxp2          : in  STD_LOGIC;                         -- Auxiliary Channel 2
    vauxn2          : in  STD_LOGIC;
    vauxp3          : in  STD_LOGIC;                         -- Auxiliary Channel 3
    vauxn3          : in  STD_LOGIC;
    vauxp4          : in  STD_LOGIC;                         -- Auxiliary Channel 4
    vauxn4          : in  STD_LOGIC;
    vauxp5          : in  STD_LOGIC;                         -- Auxiliary Channel 5
    vauxn5          : in  STD_LOGIC;
    vauxp6          : in  STD_LOGIC;                         -- Auxiliary Channel 6
    vauxn6          : in  STD_LOGIC;
    vauxp7          : in  STD_LOGIC;                         -- Auxiliary Channel 7
    vauxn7          : in  STD_LOGIC;
    vauxp8          : in  STD_LOGIC;                         -- Auxiliary Channel 8
    vauxn8          : in  STD_LOGIC;
    vauxp9          : in  STD_LOGIC;                         -- Auxiliary Channel 9
    vauxn9          : in  STD_LOGIC;
    vauxp10         : in  STD_LOGIC;                         -- Auxiliary Channel 10
    vauxn10         : in  STD_LOGIC;
    vauxp11         : in  STD_LOGIC;                         -- Auxiliary Channel 11
    vauxn11         : in  STD_LOGIC;
    vauxp12         : in  STD_LOGIC;                         -- Auxiliary Channel 12
    vauxn12         : in  STD_LOGIC;
    vauxp13         : in  STD_LOGIC;                         -- Auxiliary Channel 13
    vauxn13         : in  STD_LOGIC;
    vauxp14         : in  STD_LOGIC;                         -- Auxiliary Channel 14
    vauxn14         : in  STD_LOGIC;
    vauxp15         : in  STD_LOGIC;                         -- Auxiliary Channel 15
    vauxn15         : in  STD_LOGIC;
    vp              : in  STD_LOGIC;
    vn              : in  STD_LOGIC;
    busy_out        : out  STD_LOGIC;                        -- ADC Busy signal
    channel_out     : out  STD_LOGIC_VECTOR (5 downto 0);    -- Channel Selection Outputs
    eoc_out         : out  STD_LOGIC;                        -- End of Conversion Signal
    eos_out         : out  STD_LOGIC;                        -- End of Sequence Signal
    ot_out          : out  STD_LOGIC;                        -- Over-Temperature alarm output
    user_supply2_alarm_out : out  STD_LOGIC;
    user_supply1_alarm_out : out  STD_LOGIC;
    user_supply0_alarm_out : out  STD_LOGIC;
    vccaux_alarm_out : out  STD_LOGIC;                        -- VCCAUX-sensor alarm output
    vccint_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
    user_temp_alarm_out : out  STD_LOGIC;                        -- Temperature-sensor alarm output
    vbram_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
    muxaddr_out     : out STD_LOGIC_VECTOR(4 downto 0); 
    alarm_out       : out STD_LOGIC
);
end system_management;

architecture xilinx of system_management is

  ATTRIBUTE CORE_GENERATION_INFO : string;
  ATTRIBUTE CORE_GENERATION_INFO OF xilinx : ARCHITECTURE IS "system_management,system_management_wiz_v1_3_13,{x_ipProduct=Vivado 2020.2,x_ipVendor=xilinx.com,x_ipLibrary=ip,x_ipName=system_management_wiz,x_ipVersion=1.3,x_ipCoreRevision=13,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED,C_FAMILY=kintexu,C_IS_DIABLO=0,C_DUAL0_REGISTER=0,C_DUAL1_REGISTER=0,C_DUAL2_REGISTER=0,C_DUAL3_REGISTER=0,C_COMMON_N_SOURCE=Null,C_ENABLE_ADC_DATA_OUT_MASTER=0,C_ENABLE_ADC_DATA_OUT_SLAVE0=0,C_ENABLE_ADC_DATA_OUT_SLAVE1=0,C_ENABLE_ADC_DATA_OUT_SLAVE2=0,C_ENABLE_DUAL_SEQUENCE_MODE=0,c_component_name=system_management,C_SIM_FILE_SEL=Default,C_SIM_DEVICE=NULL,C_SIM_FILE_REL_PATH=./,C_SIM_FILE_NAME=design,C_HAS_DCLK=1,C_HAS_DRP=1,C_HAS_RESET=1,C_HAS_CONVST=0,C_HAS_CONVSTCLK=0,C_HAS_VP=1,C_HAS_VN=1,C_HAS_CHANNEL=1,C_HAS_EOC=1,C_HAS_EOS=1,C_HAS_BUSY=1,C_HAS_JTAGLOCKED=1,C_HAS_JTAGMODIFIED=1,C_HAS_JTAGBUSY=1,C_HAS_EXTERNAL_MUX=0,C_EXTERNAL_MUXADDR_ENABLE=1,C_EXTERNAL_MUX_CHANNEL=VP_VN,C_DCLK_FREQUENCY=40,C_SAMPLING_RATE=77076.92307692308,C_HAS_OT_ALARM=1,C_HAS_USER_TEMP_ALARM=1,C_HAS_UNDER_OT_ALARM=0,C_HAS_UNDER_TEMP_ALARM=0,C_HAS_VCCINT_ALARM=1,C_HAS_VCCPSINTFP_ALARM=0,C_HAS_VCCPSAUX_ALARM=0,C_HAS_VCCPSINTLP_ALARM=0,C_HAS_VCCAUX_ALARM=1,C_HAS_VBRAM_ALARM=1,C_HAS_VCCPINT_ALARM=0,C_HAS_VCCPAUX_ALARM=0,C_HAS_VCCDDRO_ALARM=0,C_CONFIGURATION_R0=12288,C_CONFIGURATION_R1=11984,C_CONFIGURATION_R2=5120,C_CONFIGURATION_R4=0,C_ALARM_LIMIT_R0=46883,C_ALARM_LIMIT_R1=20097,C_ALARM_LIMIT_R2=41287,C_ALARM_LIMIT_R3=52115,C_ALARM_LIMIT_R4=43615,C_ALARM_LIMIT_R5=18787,C_ALARM_LIMIT_R6=38229,C_ALARM_LIMIT_R7=44923,C_ALARM_LIMIT_R8=20097,C_ALARM_LIMIT_R9=18787,C_ALARM_LIMIT_R10=18787,C_ALARM_LIMIT_R11=17694,C_ALARM_LIMIT_R12=18787,C_ALARM_LIMIT_R13=17694,C_ALARM_LIMIT_R14=39540,C_ALARM_LIMIT_R15=37355,C_SEQUENCE_R0=32513,C_SEQUENCE_SLAVE0_SSIT_R0=256,C_SEQUENCE_SLAVE1_SSIT_R0=256,C_SEQUENCE_SLAVE2_SSIT_R0=256,C_SEQUENCE_R1=65535,C_SEQUENCE_R2=20224,C_SEQUENCE_R3=65535,C_SEQUENCE_R4=0,C_SEQUENCE_R5=0,C_SEQUENCE_R6=0,C_SEQUENCE_R7=0,C_VPVN=1,C_VAUX0=1,C_VAUX1=1,C_VAUX2=1,C_VAUX3=1,C_VAUX4=1,C_VAUX5=1,C_VAUX6=1,C_VAUX7=1,C_VAUX8=1,C_VAUX9=1,C_VAUX10=1,C_VAUX11=1,C_VAUX12=1,C_VAUX13=1,C_VAUX14=1,C_VAUX15=1,C_HAS_AXI=0,C_HAS_PMC=0,C_HAS_PMC_MASTER=0,C_HAS_AXI4STREAM=0,C_HAS_TEMP_BUS=0,C_HAS_SLAVE0_SSIT_TEMP_CH=1,C_HAS_SLAVE1_SSIT_TEMP_CH=1,C_HAS_SLAVE2_SSIT_TEMP_CH=1,C_FIFO_DEPTH=7,C_INCLUDE_INTR=1,C_IS_SSIT_SLAVE0=0,C_IS_SSIT_SLAVE1=0,C_IS_SSIT_SLAVE2=0,C_ENABLE_SLAVE0=0,C_ENABLE_SLAVE1=0,C_ENABLE_SLAVE2=0,C_IS_SSIT_SLAVE0_ANALOG_BANK=0,C_IS_SSIT_SLAVE1_ANALOG_BANK=0,C_IS_SSIT_SLAVE2_ANALOG_BANK=0,C_HAS_I2C=0,C_HAS_PMBUS=0,C_HAS_I2C_SLAVE=0,C_I2C_SLAVE_ADDRESS=32,C_I2C_SLAVE0_ADDRESS=32,C_I2C_SLAVE1_ADDRESS=32,C_I2C_SLAVE2_ADDRESS=32,C_CONFIGURATION_R3=8,C_CONFIGURATION_SLAVE0_SSIT_R3=15,C_CONFIGURATION_SLAVE1_SSIT_R3=15,C_CONFIGURATION_SLAVE2_SSIT_R3=15,C_CONFIGURATION_R4_1=12,C_CONFIGURATION_R4_2=13,C_CONFIGURATION_R4_3=14,C_CONFIGURATION_R4_4=13,C_CONFIGURATION_SLAVE0_SSIT_R4_1=12,C_CONFIGURATION_SLAVE0_SSIT_R4_2=13,C_CONFIGURATION_SLAVE0_SSIT_R4_3=14,C_CONFIGURATION_SLAVE0_SSIT_R4_4=12,C_CONFIGURATION_SLAVE1_SSIT_R4_1=12,C_CONFIGURATION_SLAVE1_SSIT_R4_2=13,C_CONFIGURATION_SLAVE1_SSIT_R4_3=14,C_CONFIGURATION_SLAVE1_SSIT_R4_4=12,C_CONFIGURATION_SLAVE2_SSIT_R4_1=12,C_CONFIGURATION_SLAVE2_SSIT_R4_2=13,C_CONFIGURATION_SLAVE2_SSIT_R4_3=14,C_CONFIGURATION_SLAVE2_SSIT_R4_4=12,C_ALARM_LIMIT_USL1=39103,C_ALARM_LIMIT_USU1=39540,C_ALARM_LIMIT_USL2=19442,C_ALARM_LIMIT_USU2=19879,C_ALARM_LIMIT_USL3=39103,C_ALARM_LIMIT_USU3=39540,C_ALARM_LIMIT_USL4=19550,C_ALARM_LIMIT_USU4=19769,C_ALARM_LIMIT_SLAVE0_SSIT_USL1=39103,C_ALARM_LIMIT_SLAVE0_SSIT_USU1=39540,C_ALARM_LIMIT_SLAVE0_SSIT_USL2=19442,C_ALARM_LIMIT_SLAVE0_SSIT_USU2=19879,C_ALARM_LIMIT_SLAVE0_SSIT_USL3=39103,C_ALARM_LIMIT_SLAVE0_SSIT_USU3=39540,C_ALARM_LIMIT_SLAVE0_SSIT_USL4=19550,C_ALARM_LIMIT_SLAVE0_SSIT_USU4=19769,C_ALARM_LIMIT_SLAVE1_SSIT_USL1=39103,C_ALARM_LIMIT_SLAVE1_SSIT_USU1=39540,C_ALARM_LIMIT_SLAVE1_SSIT_USL2=19442,C_ALARM_LIMIT_SLAVE1_SSIT_USU2=19879,C_ALARM_LIMIT_SLAVE1_SSIT_USL3=39103,C_ALARM_LIMIT_SLAVE1_SSIT_USU3=39540,C_ALARM_LIMIT_SLAVE1_SSIT_USL4=19550,C_ALARM_LIMIT_SLAVE1_SSIT_USU4=19769,C_ALARM_LIMIT_SLAVE2_SSIT_USL1=39103,C_ALARM_LIMIT_SLAVE2_SSIT_USU1=39540,C_ALARM_LIMIT_SLAVE2_SSIT_USL2=19442,C_ALARM_LIMIT_SLAVE2_SSIT_USU2=19879,C_ALARM_LIMIT_SLAVE2_SSIT_USL3=39103,C_ALARM_LIMIT_SLAVE2_SSIT_USU3=39540,C_ALARM_LIMIT_SLAVE2_SSIT_USL4=19550,C_ALARM_LIMIT_SLAVE2_SSIT_USU4=19769,C_SEQUENCE_R8=7,C_DUAL_SEQUENCE_R2=0,C_DUAL_SEQUENCE_R1=0,C_DUAL_SEQUENCE_R0=0,C_AVG_VUSER=7,C_SEQUENCE_SLAVE0_SSIT_R8=0,C_AVG_SLAVE0_SSIT_VUSER=0,C_SEQUENCE_SLAVE1_SSIT_R8=0,C_AVG_SLAVE1_SSIT_VUSER=0,C_SEQUENCE_SLAVE2_SSIT_R8=0,C_AVG_SLAVE2_SSIT_VUSER=0,C_HAS_USER_SUPPLY0_ALARM=1,C_HAS_USER_SUPPLY1_ALARM=1,C_HAS_USER_SUPPLY2_ALARM=1,C_HAS_USER_SUPPLY3_ALARM=0,C_AVERAGE_EN_VUSER0=1,C_AVERAGE_EN_VUSER1=1,C_AVERAGE_EN_VUSER2=1,C_AVERAGE_EN_VUSER3=0,C_HAS_USER_SUPPLY0_SLAVE0_SSIT_ALARM=0,C_HAS_USER_SUPPLY1_SLAVE0_SSIT_ALARM=0,C_HAS_USER_SUPPLY2_SLAVE0_SSIT_ALARM=0,C_HAS_USER_SUPPLY3_SLAVE0_SSIT_ALARM=0,C_AVERAGE_EN_SLAVE0_SSIT_VUSER0=0,C_AVERAGE_EN_SLAVE0_SSIT_VUSER1=0,C_AVERAGE_EN_SLAVE0_SSIT_VUSER2=0,C_AVERAGE_EN_SLAVE0_SSIT_VUSER3=0,C_HAS_USER_SUPPLY0_SLAVE1_SSIT_ALARM=0,C_HAS_USER_SUPPLY1_SLAVE1_SSIT_ALARM=0,C_HAS_USER_SUPPLY2_SLAVE1_SSIT_ALARM=0,C_HAS_USER_SUPPLY3_SLAVE1_SSIT_ALARM=0,C_AVERAGE_EN_SLAVE1_SSIT_VUSER0=0,C_AVERAGE_EN_SLAVE1_SSIT_VUSER1=0,C_AVERAGE_EN_SLAVE1_SSIT_VUSER2=0,C_AVERAGE_EN_SLAVE1_SSIT_VUSER3=0,C_HAS_USER_SUPPLY0_SLAVE2_SSIT_ALARM=0,C_HAS_USER_SUPPLY1_SLAVE2_SSIT_ALARM=0,C_HAS_USER_SUPPLY2_SLAVE2_SSIT_ALARM=0,C_HAS_USER_SUPPLY3_SLAVE2_SSIT_ALARM=0,C_AVERAGE_EN_SLAVE2_SSIT_VUSER0=0,C_AVERAGE_EN_SLAVE2_SSIT_VUSER1=0,C_AVERAGE_EN_SLAVE2_SSIT_VUSER2=0,C_AVERAGE_EN_SLAVE2_SSIT_VUSER3=0,C_I2C_CLK_PERIOD=2500.000,C_USER_SUPPLY0_SOURCE=VCCO,C_USER_SUPPLY1_SOURCE=VCCINT,C_USER_SUPPLY2_SOURCE=VCCAUX,C_USER_SUPPLY3_SOURCE=VCCO_BOT,C_USER_SUPPLY0_BANK=44,C_USER_SUPPLY1_BANK=45,C_USER_SUPPLY2_BANK=46,C_USER_SUPPLY3_BANK=65,C_USER_SUPPLY0_SLAVE0_SSIT_SOURCE=VCCO,C_USER_SUPPLY1_SLAVE0_SSIT_SOURCE=VCCINT,C_USER_SUPPLY2_SLAVE0_SSIT_SOURCE=VCCAUX,C_USER_SUPPLY3_SLAVE0_SSIT_SOURCE=VCCO_TOP,C_USER_SUPPLY0_SLAVE0_SSIT_BANK=44,C_USER_SUPPLY1_SLAVE0_SSIT_BANK=44,C_USER_SUPPLY2_SLAVE0_SSIT_BANK=44,C_USER_SUPPLY3_SLAVE0_SSIT_BANK=65,C_USER_SUPPLY0_SLAVE1_SSIT_SOURCE=VCCO,C_USER_SUPPLY1_SLAVE1_SSIT_SOURCE=VCCINT,C_USER_SUPPLY2_SLAVE1_SSIT_SOURCE=VCCAUX,C_USER_SUPPLY3_SLAVE1_SSIT_SOURCE=VCCO_TOP,C_USER_SUPPLY0_SLAVE1_SSIT_BANK=44,C_USER_SUPPLY1_SLAVE1_SSIT_BANK=44,C_USER_SUPPLY2_SLAVE1_SSIT_BANK=44,C_USER_SUPPLY3_SLAVE1_SSIT_BANK=65,C_USER_SUPPLY0_SLAVE2_SSIT_SOURCE=VCCO,C_USER_SUPPLY1_SLAVE2_SSIT_SOURCE=VCCINT,C_USER_SUPPLY2_SLAVE2_SSIT_SOURCE=VCCAUX,C_USER_SUPPLY3_SLAVE2_SSIT_SOURCE=VCCO_TOP,C_USER_SUPPLY0_SLAVE2_SSIT_BANK=44,C_USER_SUPPLY1_SLAVE2_SSIT_BANK=44,C_USER_SUPPLY2_SLAVE2_SSIT_BANK=44,C_USER_SUPPLY3_SLAVE2_SSIT_BANK=65,C_HAS_VUSER0=1,C_HAS_VUSER1=1,C_HAS_VUSER2=1,C_HAS_VUSER3=0,C_HAS_SLAVE0_SSIT_VUSER0=0,C_HAS_SLAVE0_SSIT_VUSER1=0,C_HAS_SLAVE0_SSIT_VUSER2=0,C_HAS_SLAVE0_SSIT_VUSER3=0,C_HAS_SLAVE1_SSIT_VUSER0=0,C_HAS_SLAVE1_SSIT_VUSER1=0,C_HAS_SLAVE1_SSIT_VUSER2=0,C_HAS_SLAVE1_SSIT_VUSER3=0,C_HAS_SLAVE2_SSIT_VUSER0=0,C_HAS_SLAVE2_SSIT_VUSER1=0,C_HAS_SLAVE2_SSIT_VUSER2=0,C_HAS_SLAVE2_SSIT_VUSER3=0,C_DUAL_SEQ=0,C_DIV_VUSER0=3,C_DIV_VUSER1=3,C_DIV_VUSER2=3,C_DIV_VUSER3=6,C_DIV_VUSER0_SLAVE0=3,C_DIV_VUSER1_SLAVE0=3,C_DIV_VUSER2_SLAVE0=3,C_DIV_VUSER3_SLAVE0=6,C_DIV_VUSER0_SLAVE1=3,C_DIV_VUSER1_SLAVE1=3,C_DIV_VUSER2_SLAVE1=3,C_DIV_VUSER3_SLAVE1=6,C_DIV_VUSER0_SLAVE2=3,C_DIV_VUSER1_SLAVE2=3,C_DIV_VUSER2_SLAVE2=3,C_DIV_VUSER3_SLAVE2=6,C_CHANNEL_CNT=27,C_VAUXN0_LOC=V11,C_VAUXP0_LOC=V12,C_VAUXN1_LOC=Y10,C_VAUXP1_LOC=Y11,C_VAUXN2_LOC=AB9,C_VAUXP2_LOC=AB10,C_VAUXN3_LOC=AF10,C_VAUXP3_LOC=AE10,C_VAUXN4_LOC=W13,C_VAUXP4_LOC=W14,C_VAUXN5_LOC=Y15,C_VAUXP5_LOC=W15,C_VAUXN6_LOC=AD13,C_VAUXP6_LOC=AD14,C_VAUXN7_LOC=AF12,C_VAUXP7_LOC=AE12,C_VAUXN8_LOC=AA8,C_VAUXP8_LOC=Y8,C_VAUXN9_LOC=AA9,C_VAUXP9_LOC=AA10,C_VAUXN10_LOC=AD8,C_VAUXP10_LOC=AC8,C_VAUXN11_LOC=AD9,C_VAUXP11_LOC=AC9,C_VAUXN12_LOC=AB14,C_VAUXP12_LOC=AA14,C_VAUXN13_LOC=AB15,C_VAUXP13_LOC=AA15,C_VAUXN14_LOC=AC13,C_VAUXP14_LOC=AC14,C_VAUXN15_LOC=AE15,C_VAUXP15_LOC=AD15,C_I2C_SCLK_LOC=AD23,C_I2C_SDA_LOC=AE23}";

component system_management_sysmon 
   port
   (
    daddr_in        : in  STD_LOGIC_VECTOR (7 downto 0);     -- Address bus for the dynamic reconfiguration port
    den_in          : in  STD_LOGIC;                         -- Enable Signal for the dynamic reconfiguration port
    di_in           : in  STD_LOGIC_VECTOR (15 downto 0);    -- Input data bus for the dynamic reconfiguration port
    dwe_in          : in  STD_LOGIC;                         -- Write Enable for the dynamic reconfiguration port
    do_out          : out  STD_LOGIC_VECTOR (15 downto 0);   -- Output data bus for dynamic reconfiguration port
    drdy_out        : out  STD_LOGIC;                        -- Data ready signal for the dynamic reconfiguration port
    dclk_in         : in  STD_LOGIC;                         -- Clock input for the dynamic reconfiguration port
    reset_in        : in  STD_LOGIC;                         -- Reset signal for the System Monitor control logic
    jtagbusy_out    : out  STD_LOGIC;                        -- JTAG DRP transaction is in progress signal
    jtaglocked_out  : out  STD_LOGIC;                        -- DRP port lock request has been made by JTAG
    jtagmodified_out : out  STD_LOGIC;                        -- Indicates JTAG Write to the DRP has occurred
    vauxp0          : in  STD_LOGIC;                         -- Auxiliary Channel 0
    vauxn0          : in  STD_LOGIC;
    vauxp1          : in  STD_LOGIC;                         -- Auxiliary Channel 1
    vauxn1          : in  STD_LOGIC;
    vauxp2          : in  STD_LOGIC;                         -- Auxiliary Channel 2
    vauxn2          : in  STD_LOGIC;
    vauxp3          : in  STD_LOGIC;                         -- Auxiliary Channel 3
    vauxn3          : in  STD_LOGIC;
    vauxp4          : in  STD_LOGIC;                         -- Auxiliary Channel 4
    vauxn4          : in  STD_LOGIC;
    vauxp5          : in  STD_LOGIC;                         -- Auxiliary Channel 5
    vauxn5          : in  STD_LOGIC;
    vauxp6          : in  STD_LOGIC;                         -- Auxiliary Channel 6
    vauxn6          : in  STD_LOGIC;
    vauxp7          : in  STD_LOGIC;                         -- Auxiliary Channel 7
    vauxn7          : in  STD_LOGIC;
    vauxp8          : in  STD_LOGIC;                         -- Auxiliary Channel 8
    vauxn8          : in  STD_LOGIC;
    vauxp9          : in  STD_LOGIC;                         -- Auxiliary Channel 9
    vauxn9          : in  STD_LOGIC;
    vauxp10         : in  STD_LOGIC;                         -- Auxiliary Channel 10
    vauxn10         : in  STD_LOGIC;
    vauxp11         : in  STD_LOGIC;                         -- Auxiliary Channel 11
    vauxn11         : in  STD_LOGIC;
    vauxp12         : in  STD_LOGIC;                         -- Auxiliary Channel 12
    vauxn12         : in  STD_LOGIC;
    vauxp13         : in  STD_LOGIC;                         -- Auxiliary Channel 13
    vauxn13         : in  STD_LOGIC;
    vauxp14         : in  STD_LOGIC;                         -- Auxiliary Channel 14
    vauxn14         : in  STD_LOGIC;
    vauxp15         : in  STD_LOGIC;                         -- Auxiliary Channel 15
    vauxn15         : in  STD_LOGIC;
    vp              : in  STD_LOGIC;
    vn              : in  STD_LOGIC;
    busy_out        : out  STD_LOGIC;                        -- ADC Busy signal
    channel_out     : out  STD_LOGIC_VECTOR (5 downto 0);    -- Channel Selection Outputs
    eoc_out         : out  STD_LOGIC;                        -- End of Conversion Signal
    eos_out         : out  STD_LOGIC;                        -- End of Sequence Signal
    ot_out          : out  STD_LOGIC;                        -- Over-Temperature alarm output
    user_supply2_alarm_out : out  STD_LOGIC;
    user_supply1_alarm_out : out  STD_LOGIC;
    user_supply0_alarm_out : out  STD_LOGIC;
    vccaux_alarm_out : out  STD_LOGIC;                        -- VCCAUX-sensor alarm output
    vccint_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
    user_temp_alarm_out : out  STD_LOGIC;                        -- Temperature-sensor alarm output
    vbram_alarm_out : out  STD_LOGIC;                        -- VCCINT-sensor alarm output
    muxaddr_out     : out STD_LOGIC_VECTOR(4 downto 0); 
    alarm_out       : out STD_LOGIC
);
end component;

begin

   U0 : system_management_sysmon 
   port map 
   (
    daddr_in                => daddr_in,
    den_in                  => den_in,
    di_in(15 downto 0)      => di_in(15 downto 0),
    dwe_in                  => dwe_in,
    do_out(15 downto 0)     => do_out(15 downto 0),
    drdy_out                => drdy_out,
    dclk_in                 => dclk_in,
    reset_in                => reset_in,
    jtagbusy_out            => jtagbusy_out,
    jtaglocked_out          => jtaglocked_out,
    jtagmodified_out        => jtagmodified_out,
    vauxp0                       => vauxp0,
    vauxn0                       => vauxn0,
    vauxp1                       => vauxp1,
    vauxn1                       => vauxn1,
    vauxp2                       => vauxp2,
    vauxn2                       => vauxn2,
    vauxp3                       => vauxp3,
    vauxn3                       => vauxn3,
    vauxp4                       => vauxp4,
    vauxn4                       => vauxn4,
    vauxp5                       => vauxp5,
    vauxn5                       => vauxn5,
    vauxp6                       => vauxp6,
    vauxn6                       => vauxn6,
    vauxp7                       => vauxp7,
    vauxn7                       => vauxn7,
    vauxp8                       => vauxp8,
    vauxn8                       => vauxn8,
    vauxp9                       => vauxp9,
    vauxn9                       => vauxn9,
    vauxp10                      => vauxp10,
    vauxn10                      => vauxn10,
    vauxp11                      => vauxp11,
    vauxn11                      => vauxn11,
    vauxp12                      => vauxp12,
    vauxn12                      => vauxn12,
    vauxp13                      => vauxp13,
    vauxn13                      => vauxn13,
    vauxp14                      => vauxp14,
    vauxn14                      => vauxn14,
    vauxp15                      => vauxp15,
    vauxn15                      => vauxn15,
    vp                      => vp,
    vn                      => vn,
    vbram_alarm_out         => vbram_alarm_out,
    vccaux_alarm_out        => vccaux_alarm_out,
    vccint_alarm_out        => vccint_alarm_out,
    user_temp_alarm_out     => user_temp_alarm_out,
    busy_out                => busy_out,
    channel_out             => channel_out,
    eoc_out                 => eoc_out,
    eos_out                 => eos_out,
    ot_out                  => ot_out,
    user_supply2_alarm_out  => user_supply2_alarm_out,
    user_supply1_alarm_out  => user_supply1_alarm_out,
    user_supply0_alarm_out  => user_supply0_alarm_out,
    muxaddr_out             => muxaddr_out,
    alarm_out               => alarm_out
    );

end xilinx;

