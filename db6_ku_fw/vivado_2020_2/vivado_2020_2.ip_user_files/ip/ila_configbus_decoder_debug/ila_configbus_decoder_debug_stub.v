// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Thu Mar 11 14:27:24 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_stub.v
// Design      : ila_configbus_decoder_debug
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2020.2" *)
module ila_configbus_decoder_debug(clk, probe0, probe1, probe2, probe3, probe4, probe5)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[4:0],probe1[7:0],probe2[15:0],probe3[31:0],probe4[7:0],probe5[7:0]" */;
  input clk;
  input [4:0]probe0;
  input [7:0]probe1;
  input [15:0]probe2;
  input [31:0]probe3;
  input [7:0]probe4;
  input [7:0]probe5;
endmodule
