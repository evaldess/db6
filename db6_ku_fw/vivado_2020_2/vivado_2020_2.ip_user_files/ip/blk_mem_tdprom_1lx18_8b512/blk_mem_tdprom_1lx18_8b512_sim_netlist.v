// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:35 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/blk_mem_tdprom_1lx18_8b512_synth_1/blk_mem_tdprom_1lx18_8b512_sim_netlist.v
// Design      : blk_mem_tdprom_1lx18_8b512
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_tdprom_1lx18_8b512,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module blk_mem_tdprom_1lx18_8b512
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;

  wire [8:0]addra;
  wire clka;
  wire [7:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [7:0]NLW_U0_doutb_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "0" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.287308 mW" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_tdprom_1lx18_8b512.mem" *) 
  (* C_INIT_FILE_NAME = "blk_mem_tdprom_1lx18_8b512.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "4" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  blk_mem_tdprom_1lx18_8b512_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[7:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 20080)
`pragma protect data_block
PvbLZqU+aZuj4i3+y+EH7CBhfVx/Z5gpeAFLLtisGoO0//NPrrSGVvrPKzwcvGV0qsz6qgR4R+Ek
Ja9IF3x+N4HZXSouoUdklZE4xmynU5hpUpv70S4+7fj8VJiezJbEweOMR6Ur4jkhPDszt9pipSw6
pWW+Qxq41jIDmNzNHcWUArBTxf2lbR8D6a4l8wlURdoQtQnKlwAEQ07fLqTc+MFhYJT6zXgB5vf+
YVHGGJ/3jr0SAYaEWq8/h0JTqK0YdmIyv7SJD3Ift4c+icEAtW2IFNddkV1KoTgRfWXE9KJAEsOD
MGChCsWxSVPUg4nzqC+OtgbGS7yXjqe/1Lw1M4/Y2Zmkq8pNLxnAT+8FsbM/16uStuMm1zGBUVK8
GSE9YPL0KmNRT5bM0FS/HAyeriQ+aSuUR/3MZvDf5YMo/tV86yfgcB4E8Wdg6Jb0o/Tvcvwdp101
WOprTUF116E/WP8/PqrzhzK42UrnfDaq0Ht5qRgBzcxmY9pDbpo5FK8GDopyE9VBY0d9D9Sev81B
9JwQwMapiycQi3ELFiX9hiM16BKJ1E9sSWBi5ahIL59BRmYtAupOh0XLS9klZ37vL0JfHT/SeHot
ZWBtmRK5b6toIIl/l6O9DZVoVAzp6jtWL33/1vNwYPFiaCXySiCADLrFVQO7yqkDJjkvPF5j61mw
lItbq1YzvQIxQZlcybjLPe8s5vbrcs7ML4Hh/bfAbHZNk7R79tPhch3X9tmpjIUy9nBnvWhjJLJ8
yiB1W0oTuK/Mw6lV/NkIPKsBvV+zLS8lhdxg4aNHR7sXTs8248VecsRUTuoyCfRUaXZ0eFrmGOMu
ZsnJCvJoutzge9wSaIsuim9CR9K75wNCkQPCkBSQJJnfFoOR/oYJNnK5hvS7Tv3CCx2KnT9T/0cq
s5eKCcR5ddN3iY8Bsjo5k0P5aATb0YLh1GoE3epMc2qVXLEfmjIc7FG7WEG8z05OJzUJC9kur5s6
/80r75j0zlq52I8Nb81TvhiOh5twyfX30Mgi3M8vhkKzbiMaY3j2RLWvUhmbq+V0yJebphF2gIkh
KXx1/OcSYAkVuhOsdW3bATKJA96adOb8Sqs1kkTKomCKt34oJlamXVspeedm2VEoU5RsFsKMwA02
vSZUYJBOOZW3plBJ1uqto7f09ZNsBmx+CQMxyQRTXabivsqb+vGWyP+aBwlyO4KBApM2Rf5m1yKW
rMUg1fcPzU38CEQcbPAU98nBBYR+gYOtQVNjSVKYDPbdR27B0Btc/FO58/UH5sxtHUCzE6693Nar
wUHBXYqfn+GCrtmoSd3/PkjMibhLBS0YXcLk58pBRTMOQ53WFx/jbXZ8qho7HrTdcuEeafT7mu5p
/nDZTGYKmVRgE+dDGRD2kYF+gTqDWr7VcLKMe3/nkWv0v2CJOy8Nz3UU2Gy6sBNWLNUrjbx6I74y
xFoFJ4K+YOiMOTIiVqLALPWxZaONXNNlK7q3Qh0sX4EOMK4wKZBpgFJeQEP3QIWzKc1hFN4JdwK5
KX9uB5jdQIMhHSj1i1IJJHd6WtZZ3rN1LpQzTQbD4Wncpm3FgJIUc9+tjRK8XATYjRLBUJqb4K/1
eabU4DuCTLb4Eta3F3MQ+rRKqiUaUZkW6hT+h4QZ/+WeFBZm+zBD7vs7uB6V1xTYKinUoe2eW3FX
iF9PGOdl0K7Q6QpeDsvS9VrQNx6WPTaYk61aIJp/DRfk57i2yQMwLeyAznczgrOgJ9DOuxJ8aAuh
cFCcsJ9rKpVRLhBbGFKaZohC+6Z8NOBG8dlGX8WueolaGAgI+Wp/y9QbN8BU2rc6pKzwbLuvTjTU
EMMhlFBiUQavVOj7ZTUCzZrRtcWTs0gig5lDgZn55RhGlbtXUxD+0wRnzVKYo2QwGKSbPMknOd5S
/4D426ZLZ7uNcI5e0kYd1BGoSD94jaYqasOGUil20rG+gkSS8jUYIvEK3j+zbBOgqOTD/zDIcD/3
xwJZUPAOgbcfLuUJsnCXO71bDbYuPEy8DcQts4TwjOPhzl8j+7GSoeUybFg3Nc7IEcLhEPjfqRiY
iE9grF+hUZCCp9Dzdf1R2cklPYSKAmw8PjDir0GTiB8PM6bqRMy27Bg/PDNX4QzJo1uE6hv/27Jy
mCxeex0rYXInL2OjC/eFDsy4AAZsYZmEcdfZzeWOjOxoi4LWBsE9Xin+KOONjy6tk1Iw1LRfQL71
AqTd0PyeQRffcRqWJHFR8P0UvJf/GfrO5tmFFmZlA/SomIbIuvg69n8132tqOVzQo9erdpR2bXsF
OXZZJNEmjVsT5rOPhd/VM++Ro+4N+bxZ98ggPMT8G9evTRxwtzfEDH620cPqLPj6CGksTi8spEuf
NYlp4S2CuGzJUmAkp4leskxKxF8GvIv9ksxJ7/cDRF0EcTaDqSysQTj4Y4E9/TUMy1yeabYHlonW
lpn6x+DRn6ydTpC2Y3rno+LePC8hZkBDOt9LCI4ega02ZrOayJVqxJEpanL7w1IvbE5bRrZm7g3/
RewOxb2pGeTFZjgZnbbYHM2XvjbngCbCfxwEXYc6IliN6Ky+Ks/xccMfe0WZFQl7WFnuCFvVJ5Eu
4/VoUJq7lznVy3WESppE0gqF3tNjj1kYraFVvoYXwPBCGUhIof1PDSFIQZFHSuvKig5FYY2osU3n
hQhju8e3xUwOK0VmejfDbe0yP8U9wq7IiIP9Pk5F142Zbz/DQtTRAGNgXUp0PjT8J3Wj3STyyT6Z
40mclmCGU8aqSASY3sjKdsFUo3DFZni8GprJJ3iEl3w59R6W6PuAVnVDIvKu0n2rmj5BKo2Vdbzr
VmGrYBbOxnHzi4sOXLG+qICmZszZjP8/Mp2us9E8BVGvNHQSZ3kGKNRgi5Y7CAFcgVlUvAkSIAiE
mvizVhdOR+3XkPcYoHN0yi0FHA/oXGBug/f/Y7G8gZ07LUSVpwP05nJhxXsadtvrUn91rFS5R+cA
kGMYEziEKMmSLngJmGTNiK7SYa6R+OTIwpA0tal5+Hw+6PXhK9qs4K1I569f5sTfPC4AZibCpiZD
xnI+m+dHw9yimfmaVKiHSYhotmoXn9qSbIjnNiYOUuvr8j/hMBmsu57B15nv2dAwES12SkDUB8pA
hRPjBRDA7sj+GMpJCJVCYLmUYitLvLJDA4omgf0q2YCWsRnDzi0XkRlJTEJIHNnjqzHPk7aPkFHC
W5DWgaGym0PnJW7V7R7SN95fMdQNVMGmECMFoc7r0Ab4+kTFpfohYs18eHfGUJ4N93kjtCQjroAa
JA0QO+lWWnt1lcO+7IvbqTFOLdifd5y3YOi8dY8Nt93wY2LDZ41v9E3tRfXuyFihz/m0M504HQi3
X7BR88tdn48O9T3AF8OJ21p6Bb3mj4PbfmsnBLoRk+jQsVr7s6AJ3ouPO1kcgYrqPu866/LaOsvD
Qd9yMa8Q5fpLMbZhFi6JMObnm35TQv5LmhX2WfAq58/EdPvis8VcnW69POz8n06bJkc3ErPM8Vf5
qiue+rQ/If11p1uwF/s8RN6oWCywjnaus/rz7slLPfyDQtrzMZ7nVOGQZHCBaaXDTHVi8PBzE/uC
/St6u3felPKz4KxqEmh1TP6MbPXPCbCZ3ot+CHN/7II+KQ7CulUjjhF5vWwAEkxKymeLDojMQUhv
k3n2fazSSFF+TFO8jqB7TbIKbg9AMwudA2L8yMXUd4xWuPxXZbbTQNSWnrVAq8XR5J+3tsineuH/
QqLRwq+Spo7H+m/naVs5wqf0C9qtu6NFyx8IAK+Wyof03fDGJJFst0s3hZ03saLKMXvKrHv8aQHv
QEDjUgxKG9/IJlNE1HRwWttwBlwVWiDiDmKmnI94AmdqjurUp+EaYWZ4IDYj7GdpxOxgQXZGRM6h
jiGZ5il/r3bBNVqt09fsSotVMBe1e6U3IYb0+6cP9CEtSYh9X+zgoyjLX5E3pPdRm7Me1ONOSOjW
w8Lz9HYYwAGIpqezs2Mio2fHAiWZuoOKNccaUt8hBDjxuU0rhSKJ28EoH26b8p8Z0siggqKxw/Wh
glftNDHksspzNSl1RJZz8Uu6jRBNfF1POhxy26SqKwEKyVdYJc3GJKg2pG6QiWy+ESFRheG+JgGA
phNZ4b98jdfkfdkgt9+9ZMlkiAZVjP3LEdVbMHkXCyowVcCwwo3+g8hTTrtEOjaZQSHF53RwGHB9
rNLr7fMqUCKihOImXraJCnILB/+tgNaW4DPON/1wr5S1h5uCIoa2Kss5nvha/Hyt0piUbsJWsGp1
/Vodi3tERYepySUrvz/wmeJ7cZYW+pHW6LDJRJMLUAgpUa0egwsM+jxFEyDTGidsqiC4md2/JkxJ
+9A/FkmLC2iX1L27SwQLe7E2jL2utSHgHpEbMqzJ/EzX1nvxA8+xPZmmgWl2/lTojvfj2xQlNL9v
u6UDgGQCOeKC5nUo7Mpt9hiegLHaEtdcyDCuny8XhaeuO1J39jm2Ee+MJe9wDW1g+zfIs4AgOuFI
i4dKyUe0UEuH4U4OHYh87rKfDVwMJJJoKiHpF+QwhP4wijMOIX8EW3DojvBcnnh//ou/Jm9BYtvk
WuyJQaLTchaUuAfnPWCr4cCJigbPOM3c3INpUlClGYMNJ0xdvya9kM32PqbSQgo95D4wphgN1pnY
umWRAhKGMfmfSjX1oYAJ9teHGbJzQT6LZFRxoj1q4dWckbeVIddirLCGdzNItGkRvcefuKKBQKO9
Qv/FPaNMQz1p7DjIbYnggQ2xFKcjKSq8Na3Hy/GZ7fFMrqh+VjPQYHa9da1N1s0VTZ49Zye9R180
2gC4i/FCeQs5f2L4k8glbyQqzZ2kpCzCd/dR/iZ/n7btw4vAn9NYDgyaXqgsUWd6p0mvhfePCwAp
hbbBorSpGRVv4YiK/hs+2tdupu4fhsipyjxqCGMV+z07iARJWmWeVDTG2Bsdt1eMea+gb9kf9Cbf
EZlF00jmxScRr7lGubL62yQaDKfdxhlSPlOWXiQgyaIqnRyG9WO3U+pr3Hry4l1fCL7QsbHfWEne
8+lfLSbXQTi/Ex3Zu33BJr/UNZSXq6zqCWvT2wmbnFLbVER0qrnnfQCK3BRepDAvR/cnX3CcxsP0
+9aWZCkzFoYRvaERdqGWOftvldSTJpx20Aa37VacGS7DEiJXk00jaYEdMubbm4ZRmnh1U8JETmfU
WhDkvHOCEbjzxUMydee+mWQJJoQPQLwSf/a1GZpPLpUcJZOgxLUseOOc72rtrsLswVr+EbyOkz61
FIv7Przyd7eH61hE7yHSvoxygGY0+g2eAKmgeljPPIGH6BQ+Hx8H2ZBA2kC4BcY6ypcldNx4gfpn
G85He7retxTRhgaQBb4gkim44blPQtzk4mA+T7wLFRIioP/bWHsCFSfTvWeWEkjNHzp3H8nqip4X
lu0q4L525sFfojEHh87/B7OHcHlhsjPlZ2NndUkpA+q31yI3TQzl1VP/0BSLcppP+qh0JKtF4Dyz
qCDvmqREehy3gww1l56f+QMBacXKAQ7868d9kFSBIko9aWiqSp/V0OJwha+dIqp83zGdlP05ZxvM
HDEtgqbkXw8ZCqoMm3SzALb7jgGUGTB75lZiqZ3eKMMN5uPr54XeqkkjVcxql/h4lL9PBfGzDq5H
Y4sNRh/5e6m7QWL7ZvXUOoQfE07I/eZZ2U2Wb+wxyHJ22rxomlWBEc/Ozp0o4GZyE647BNHs9fgT
wNHW8/ZAOeZwzGEJ/QZ10ZDH9AKBvViW72LT2DvKWeEab1X4GhcQdBsC/YdXcnkGTjFbSSQt7YOp
jh41Kl5TywfP5+69kYM260G/Zc0tzx2QR4Vkp1PVveMjEdIH5Ch6e0j/vs9e/6XS2e47UkI3JoJ0
QaPQV0sewqBLHtXyV/dw+5cFraYwQBarHmAmC1GgVwI45Uhy6uBS7wLwGrxf+fLp/1yINslo05Uz
2g7hVsMu7y+l/y5L19l1MaGCrQc631dQaLKqumd7a7iEvgSnxZRq7jBTKtGp5qnmcWeK7aWesz3m
qA1dPVVawuEjPUsqj2i8jLJ1xN6B3cqHVKfdCw1CwEp5Khp0HLqoKR8d0dRLkeBS2bJe+zOO+eFg
9uyenfEbxsTYGZi59HPhW066qv6C9Firngi31UWXVSGtxYk5L5L023cC9VP96/9ns9p04ZHkvkYF
kkmhunaKacvxagzNPbuhseQDc0YgUOnZFVm6+rVcErBgHdmgxeNDeZuyOZm5XGNEReooS6JtO0Nj
43x7qOmtxFapdbCcH+Q7DNYhHAyhf6bhiLVxGLNYZunIiI1mGVOvUkUb0kLnwJs2HEufKAVejPHM
pEvCU+unf1vbs/XNotcYI6OC2xjKiZ99QTWMlazaEUpbB7LjMZPvqGvd1s+l2X6xmcwPCXkqBY2C
zLXyIMyVKdArfYxq6h3ke+jUg9tgF4IQWZFZmAzNJVOK69OF8X5nJmLAtz2PTbkUowj+4FNS/n0Z
B+gaS1q7byt0cd8j/ZTKGvj7KkXISLjmOnUtC8qFst7qXV/5NrjmB3dKm4z6KvZFbLO1+6BL+Lf3
NZe/jxA7B2K468RspNIuOcYScnmW+bSV+xWUbSdHe+A0QtnMMok80+tbfxXF9uVmvEFLiONvXRQf
2yuY/rINJJYzhLMafbVGM0cJa+vC1lBPbvvukInyxqxk20zNkNJUbCf3SQjoqdAm6BunN7zS8S88
qdqxoqd1kAFT7oAQmRut0NxEYMcB5RW3N+29AwPy2NW+LtuZrLnlJxd1Y8LRNdJ97+eg4s6cDger
XTFIss0PukwXFBfVDV4cR5GTmx2P2Mon4Gur1E+4nCF7ckfWOEhOXsa3JXx088KEihacC3OmOelh
KTduUkCG3aiksxFn7YSmzsX56iPXYLBX8zE12QAaga0fWFhdAjHwoUVaaFlUljDCLYgAcV2nHYy4
UlJVbQ1+f8sTQ5fHjFem3he+a6R+KyrZixj9CMkLRptqcVcg7niLzLCTAWRrQavUsyhhhKNZq2w4
D0fRrxTiR+GojDeBUBOmMiqRXNYlkgroGTDTSnOF3TQsMFKvql5vRwHPvv1vwSd6lVF05cDeruQp
70ntB7v3sZ+11zrP3cFTX5b9KvmavNeNaBePHOrQU/bXtcduVVRty6bFqPL8xcok5cLJtb6LlAoY
w7nh9RgiLOtdRLf9lrtADdXCEpeDUVgq9VyA4XZM+jeuUC49hcyRv3LAVXUUI2wjoGq8HcN6191s
tJk31jOiPPI8x7ljlh9uuAqShvB78xl/uFbXkUxy03iIXxJUbYzbw1vLwhg6b5J7gfno4Lp6SmcS
GV84v2qYIn8YheY8dC83JkOum73KmLpzebTEHnjD6ZQetqcNApSsYOXpXLr8wM+6+gzUvaihc2Mn
Wa5Mty86bPfc/iyba+/ffkGncFUZjPs/4AtG57MDYrcLwUSHdjwEZbY/4eRnGlAZlYWQUNgHlm4P
VblR1LZj5r22Ma3fmCP+DHsxx3hDXi9w2rDO9g0WUo0BngYS0WhB1XS1oUyjElAFKK4uAB0GndxR
Jc2iaqdWDfNNil96EBIUHmYUZpkJBfnmDlTflKkTSae8JCg44ldB+L47USnJ4lncNHGRQK+zJ+SU
3jXCtuQL6XbOquPw62D0ywga/97UW7nEtU1WkI8ifexXDpqRPh4sIzujcWAEpsaVv+fUPW5Rp5YG
qO8EQrJoSGVvi7sax0IrN0H63AGvJ3o0yUPk8Q2hG8XuknwVfq6oGtydWH2yZPpy095fbW55GKAR
QzmHs2Q0HGYIoBB2WPoKlfa/IZzkioRIZmNE5IDJRyd07ucRLoHAd7Nu//OnGMYUJSLdRpWdp6Xc
fz+e9NILGtxoJhbNCwCmxCcw62moelnZnMxp+UD+krMRDB0E3busgAsZx55EwMHYZc55nE+1hxQw
vuYyP5iaOqL88rFnWGBESkXdfvIqjh5Z9JRXoMXy9oM3X/NQZilluwOfuxBog+LcSPYUv374WCmf
DlL1fH+t/rYm9D3+5+g9y8kF7B4LXuAkuFAWRi6BdU/DzRXJBd/Ppszofk9CFrcPHbQSDl5ZzeTj
dHf+7hlpg+xEy2FlR1dyzD5ekf9znHkMtkMbd7W3+V23a0H+N+5AQyt8fbrCWAnhFPBjuB6qfPZh
/jtRJnr1bfyC2IHdP1uZ9jUmbRx87K85mLHsrwQRLLBIgYHgo71okLB8wius9mvd410r3k+n0Ype
YArUfTKDahLe1qXPrizGsrVUi+F8RpwQryHx2eD+pK7byWCMVmb3VAkSss8m8SvgeNuI4ar3iMhD
gfJIZBRT1dmgDbjVZEZ3FBrAM9J7uvmBdXqddO1Q0GQ6u/hZIxNk6k11eYve6Kls0JDY6+h+twb1
pdsmg6SWFnBgcmxnTPjn02WPulexNGgeBAv4zQxkkAT+NcuPSJ3qmJsI6YrPgl09ZfnHDVV2yAmt
QEtUKT2ICR/DPrj05ytY6ZAUY+KC4NqH5q7jRDhmxlbCwtdcgrqI5qLy2ygiJDOysTobqcdjfZ3L
4uxUyo5tqt7Rt/0IOVxScDXWXxqHwrkHYAEk1wMO+EBaF1ONn9H4fFVEZi3IcCS30fdddWwYohqI
bKzCNNUOmtYk3WyK/SAlKN3Oc8OTPLIGPgKdMhStAXL9kepHQVs0j3Dcd1jBKbvRkSmu0gkrqI/d
d/F5O/sU3QLBleJtL5p2LLcza/IqUzoSaaZ/OJlQIAYMmlCxhPaASAR45oGeiAk7MbxGXKZDVrRi
4xyIalHjxQk9/rJJTBh30776kGtX8QqbV2yM5lYZkFmO4ZQ41HoIM2ICmNtUGp7Rd6dlksP1PqjC
yfA9w68ExZabha73y9OdmXpZNNJYD6FNNh2j1PU5YgBls13xMhzr59yNpFJ3snvj/UvzQuD2XBel
FHNgOSB8RbuDBKqI2qFzw3b0Th6XFqLcfZbXqOlLxzyuSwLJ9eBrn8W75h+e1kVWogcmiF3Hplkk
JHESiofECRTrmr2fATCnSUxKumNGSa8KRJBfjT4BhU4LPdx8moJ/OBBKpNjn5o63Y1u6ksfrGLWZ
sxaMDqt+Q9KHvYSje2fEQPFcziPsMi1IQ8Dkac4vPoej6GS/TGuTyKIpNMioJAvj384ixID7GCam
BmFIGVgb1C8Eic63HMT+1m/Dov9v/fUm68USAfmrp+cPpwocrF8F1ov9VKrdG57sLcTcH6FCOm3X
g59HcFcl9fEwVOVJjmIkmWOcwiLIsqWzxwRHvY5zsY9vwa6C/MEUO4Eyo1oFlAS3i9nyq1eJdAri
YrveitgumqY8AszMFeC39nY8idObqBpDZo4ex0Euw5vSXxvnH3Aty2yCpjE5Dpo6PwjUcfCIpDUG
H2FQNEa04Wq4a2m4bFHcugtkEIZv8nsPBnh5ydv+iAQX2gP76pL3FRhnVDZiwHiffIvRvBauHahL
0UqzA1Noh5gxVB8/v7l0AgBVkSReerEtfrBNn5jYfKzb3z0FulQgKrkBXBiQb+YWawlqxUdIvYTg
VbG/toTXo2W2qIoVFMTwLtmuTcbBXtiuvdq+6sa30fUMt2reDzgW2YRlNWEtOQ+Z9lmKdES5onQO
/ZjNhZF7g0nLMRQ2+JsPsMDJ+wVkmWqINt9vvjGja9h/b3rxDD9WpT5agOhEHDQ3iejg5OfrQTzJ
XCCTKKHScnWMIwEe6B1ooOuupQ/tO46CT8/HiXxoGVuRnJQfoNjiVKbjpFAB2BIaYdDDETT6YHvP
gSwgkKmGqmQ48n+9oYOwVoWbDcops+D4mRBFwwUQMbdz+oWBQ9KyaexSmLnepH14nYYE4hVxC+IR
Tq452vZO6mQg9T6U0Qb+eWjxHB7mTF9BA89322getSeLRj3dGyC/gfDxsobZxCry8aaIUFWjeody
5M8R985XKNS8HPb+G5hz3IR2oBXIBp5yxIBITy8jkj6IHBFozfLQ7zErhrdx69Zktgtte9pasPl1
YW6kQ4fzaZ+LKBkJslPknaQeRUWnJQFtFk/o0XPgoN+JeflkU4HV/06k3zUr7phYYOdpTClKwQRd
ztOMqyEVkcuJoZHU3e21OMf/F8ZsaS/NJHtlUt6PnyF5fG3LmnG3hirInH9Kw5q4qgTMui2yPeiE
K1SRfNAH8rQw/8mbdSgMZF8acG5qG2sKEzMFiD6b8VWaZO+IxGiR0vH02bI20+a2/fF33BgEW8q+
khInD8uYS3zt/eXIwaVl9bi1rxhZSEaPt8knZxQGVvdZuwSf9u17+F1MNle7uay14P0XoOlqHO+O
mmXNRSR3KHC6ht96mlSmpzO1uhSaf3QWLEApjwR3cYFGMX0BI6r2MWiqOTX+SDTPjLkhtZMNqUYn
p4qqtG2aos8XrS52JzTcEEJK5D8ZUasKUf/7f53Vv9049txR7n9Mw8hypj6iQbVgd7COC8yZ1Pky
nbqelG+2oLHRyuHSkULaG7NTA8en2kbLa8PXygBsDvJkQ/6XGT1W2BDlbxVXFpZbHXjFb5uOpD1N
NpsnPxHzn1hXgRtUHgVbeQ9uVoyd6QOS4DI3URneetzr3kJjtxldMQUv1rvFYK2/deJTRqnrR/qF
TyLJ5gVd0QrIA98Q6Ls3Uu3jTblxaXQEuxN75a+XTZ7Vok8r9xsEI8KLopSjk1Uj2eZC/0k6yyuE
bJIJuw2htTZfWS/6arrl9uqXtb2+q404IaEQhiy2HwkH2mQt8wUNfpvQ9xp1SGa3ar87k+PFE7NS
YUZ2S5wyKNHhOaCDiiLPplkPHs0K20ox0/Ti3tPHcZMl2sTLOGyCjfuISUf9T701WEBq/R/W2M/t
syO9S9chZwyypQxumbSkiZYzyTSH90Z9JDHktohSyR7IkCxzmwtXuYWuoSbBy/O9FnE1PGBpSjAu
JkbrWH02aO6iFP91Xj04QMpQYrEsGOuFIJkfmRKm+JqQlKSO9HewydlBkU5GzBOF+saXEamCRYqR
5koE9LEIXRHaFrIZDVvXhIPLLS6BK3MshngXLYCKmlLFj4Le0kUPTx0rHBlIBBorobbD0DpGP4Ls
QQNUiFFSniXZpkLoMR1Nc8L/eddfZkWU/wcZmnnw6G5J+wI4sQP0PaOvwYBPUjz1Ozcru9wyuoDr
RsJwyWW3LEdBvhn8aFxqhl/TbD4OM5RSw/+3u/zXpQagD4PnOriT6X5N2ibuinsxup9YPbP43mPX
0YLo7M9Fe+7D6QmblLJQwegSW7tGyhLK4ex4WXdrhalvQELFJgT8/VX/lkQNWJRjv4oregGaJIj3
ekxYEvmSPovdexoMrZpk8POtj6QzZgtGaKnCkMlC0PpQm4H3FD2rMciEzbAbgq7j+KGWyBTLzFwh
RWz2HOy5z4uh0QeTLVQKoHr6q165Pg66lsndd2Caqw63mMFvV4qR+FEN+juFNulMM/vcbh9Dw2aq
4SnHx454VCcDGL0lNUoSg4PJUbz6k+EaEziZE+T5BEErACE8B1qKELlg2ObfTPjHTFyUeqNaFyGv
PnP1dtai7hKbfOe1C1YNcvOLv8v4RBSwydUTNB2pgJyPAUdkvrWD0kNlxSHoiW5qgz3DJqaoP3JM
4/Wy/k1XQsVQ/JQvFzjSTpfm9mRv3XVNgrgQiZ3kkVaNN+rIdFyNj0mAeimgRXD4irDgh3Rh+5Pb
oQpg8vKD50jqH2fbHlOIsJ3giKrANtRG9oVZQfehEm8fgAG14ruR4Fa1nvFw8YQb/GjnhhyHs/Gv
ZRtt/2OVLULzJPO5JB8Pa8ywpK+iWtgtSXAGFJ6HvAPwJG7LPLeAKS7rz1HQkmIyV0lk2sCE+yhr
skZyAOEJGpA6ICFDsX0SohGcq7o9HQgFidyswLTsfPsgUIpz+1IUNrZ4IE8K65bA9TFTEbE/+c+k
S1f6tInJgNbZOQYMt9ThgstAaTH2zsVqzodTyhdMv4yxfZZlwOUIPGgAvs8oYXUDnCVS4qxVySV0
/A1YKYMPxo/j5lzVFEoooP2VMzMnO59FzGxU98GIXomROanvYldpAk7tbHdJu+iHdLnE9uJS/yPL
0LwzdyGY99IQ3y+Nu8B7a5lmTYHrgz7S3hQ85ePl2/RovScp8l8B0v/nUR/miNd42x6owe8MRg0k
pCumsVFDFlrZTaGvKaiYxA+YazJNTQaM+EcosxG92mY7aRVbl19DwySm2nZ0SuL69EIoBdUDcU7E
KsuTzPuGXp6o7sTPUYkNTZ5pqo2dAAcdT4HSpuErhKL+WWc1MvmsiyaDDKEJ6N2JuRDRrEC6P+6k
p14hmPYnzEWw/cmKPH6P4bEVyF81TinRvpq16hwkfpOKuFd7NHeNvggqSrfimseZ/7/sy9BzD4hE
ako919wY/ITO+l6uDRAOnbxumme4TNxYL66uR44K4gaChMJBwmP4aM4NLvGpXy7BBHX6wevKlxb0
FVXGZfJ2iTw+cu7YucyYe9wG9Jk7FMagSvfzDFSHxsh3KjTRWP7iPntISOzzbvs3eUS1XIF0dJ2G
jyhCveyGk/2lvidSoC55TX2QMsXYGAFWuCnp/4SfrKY8fp4RLij8Cl42aOMkq685ti1Mg1wWNtfP
youTKSEUiRjfDN5CEbFcfIBKw6g38TfiOOjAQVFU7se59oHce+mzRNdNLOpd+VJ/MHna0Hw6MfsI
0EdbH06I5qyOmAmIYvCOchsUwEyFaXsmkATlxq6rfQ+Tj3oIUjDqCnBIQwJLs/niirZ2neQA/5v9
hxiLFpoZTl7k9XxAAAESLU2rRb8WRRprjyuFhF4K/0wW/7nLCY5vFRDSX4XWjper33U0ZAkxt5DU
2CzvmCVJnCStgYAMNjLgUnal4Tk7yh6WB+iiVK+dk3MSP+K1fIkcwLxXpFOL45MFSZyEJBoTO7qA
ICibzv/m78rzWbVmHWFtc8hGwRGuzmLMmrFfEW5PacyAUfbLsmjmF7MB7dVsiRg4SffafWpsQbXG
UHYqL3Xlb5ZbgEg0j7I6QvtrAev8Gm/IntciFgXoyLnR5AvAc4g3iLNOY/JYI5akk2BcsdmSrKSJ
0d2VjMiQm7A8oTfGtoOr/OBvZvheNX9X/BMWJ5FYz6o9q4GFTy2G5uZ14WlxqgJPB15zBWtm1uLv
gf8GWMwaaP/xWZxfcbO5gehFbE+dpRbUoIebw9Of+aNuAK7I/Qic1T0gyeKj/nHq5tvGBRTZ6dmx
5wPjj/wIJVu6lD+fNEwdp2mvn4iw7JGc2bOKHz76Zls0Z4sXNIwp5pVazNOHgvHEpQags49oaCqv
k4VIg2MEPg2MsH3blR1uLJ4T8Yu/PJn8vL8O7zjjA+y3If00y4zEsDyWUoWcQlINeDOIJTgruOL4
es/+fPrgmUCssLd3SR7MN1dCDZP6k3I2pk/yNu+Y2Vq8awRnvTyLGGbXiRe6qXb09iakBHbF85Hk
MhSurOhINWIE+pK9AeIfXvZ6qnbO/oMaZITKQ9IcUYrOGuc8Z4TzP+u2W7akVsU44eYnasdASrAq
zappY3873FpNLXBMPOp8Qaiv/VI6LFRxB/zLZArT/s2bMhIZ5tjiklULQ2Bhw4VHHoatQ5fNU54b
Wiff873lTFoEkUvqqH4B2cu7wzsNuwTYRyyShN8Iq6iyF/ApLQo6/WvZr6q4j774nbbG3DkFLTX/
1Bf+BzJfm2EZTYasS+Jpy1QprP25/AK2RBJWTxoTs0hy2HhT2fSC56KqZ8DF3N5k5AFTU5rWo4HR
IK6WTDLL/LYi2rwAf09CZb5Olknbb9E0Wdys9fVw5+P5DSnYqptMBugnxQ4TWYi7069Ip6ME1Y6b
2CM7YyPWJa9jGRErMSLjTrcvK2vDDexXhF1v1xiMi6g8FP/iSJDYGCFUy96Gu68OPSoSKq7C/Rzc
SEh+IaUG/KGWagEeZRtsbs+hzKVeu+TNU6+OBbp6iK3cm0+nms/CnehjBBZHcFNgfKsa5NmEHWg1
mxXC8/lbaW7VgFeIs/ubQVVF8UoZewz/h4bJ1jeRaP3O46heRaIykGLwxMqfQDYRGyDO9izYuJcm
SnMUMCukE3duUP+N9QT0e5g1MyST7f9PIcbdlMLC7nUJHQ+2LeNze88pf0NMQpBrg5MKVccnk9Mh
fuHF5CMneAny9g3I7G7/TDyLzoh3eawV+cik7RQYbKbTUGvf8Il814ApxpByTPDtCGQuynv4J+QS
0tP65nT1K/lYSsSumgZgS4pewZ5cdzc/XCeqzbJk9wyqxyD4MtySFFhWQ5uMc+ewJgtyvyAJpHTB
Np1vQ0EFhlcYutnR0zyeZYXzOZZw/GC2CyBNIGrUyFP1o95IGPP2SveA1+W1l2yrRjq47f+m2ggy
imE6yTfKk/oazypq8j2pk2+PCmT8vWhH9izh6ZLkErpdW0j75DPCe7sV88RbHD1aNS899VqXysbc
old5H/wuLl9zsI8NLLVcDHWB+852zB6KDKXFPknEzf9I7sv6pAbqiNMPhbRGICMBmv/PLVDiVAzE
aZ1UXbrEbuhUdlMoJiufkTRZr6xmU0GmbP+mcs9HCknNc8KpsRSATPjRdDKHoRzDXS0c4LtupgRv
9kiFr/zN6/LIWWxHj0e3meTZPgAGGC7HtcL5Dd/10ZrSGrhALI30AZXhWOxfgVykuOcYIgu2eE1u
Hv/CvMcIB5f8+dUJ1bNfvcihftD1im+t8cDemh0P7HkmvYqqVwOuP9lX9fzL5oLbbtV4OG7f2IdD
Z5g7L04NNmh4B8bggDPyj/z53YT2NGFUTLOsU0nle5YN9/Wem8R9q0r9jWC9JV6xTvWs/M8lthDX
pCk5fZ+yXJxzWGOwLR2Z7sAuJMeOCVJ/r5mVHQT6At8ebugCwZG+Kbv0lGFMIlA57aRVUv5T5H7+
TzqiIjQTVQfWyGEgjolPKWxWZn23ObPfxw920JSAaqNDPBlTBQB1iYY1rjfr3YovSnKNyVYt1HAf
AFC83nl4AAt5uiVx5K/dDVNEnQG8BDmAxICFRaq18ZoHAOLxLZpeHUc4hPzGe0czphE3xelIi2Gt
RG4DorczM18hzmUe0+UR9HHXb44Akq0CnUr3USGBBRbHvRH6PH/UI3eJbZuIJe2WDwbgN7NNWkJK
5c1DqQgCcOxucT3UlhnOwB+BGOuuJTFyH9a+N159q7PnY4zatRKw9woTjzuG8KuOn29Bu4GIC8YZ
CcS9PaT0N57ZC4u674wHuTpn+TsT4vh7F0onwCGY3y3raMPhdlxwfwawu2CsCKCmdeleDYgV+RO2
9FhrNo/uDP4SFYJdBAnt7ryklauYFZ1wsEwogwt/XErlAZREXxVXr6nIyHhRRfc+JnanQefHJGs8
KNSsHSyd9ZMkESv09Rm7SDWJQrDyRBrNazq5Xjbinon7hTjV7o93igqXC8Bh4lHXmQQ2GGBKYUZd
/FJGoJic+UV2ELZ+nTc918bNZqP8i8TKhk/PhBGc/Z0VBzYAViwmqZ+Kx8DdqeDHDtxPcCZpRGBB
5Oyon5YVBJau+KQX0k9QJ2NTryMD9Yg1CPluDSrziETd+Yvew+FVHRCMFlKYAXEyDJDR8mHlEnk+
e5GjzmrSDFpXG3Dsvl4NLrt3xk+5qbPGWCH7lqLCxM5CE4GgD6FxYPTzkZCvkB0CFV+bu16oIkUi
7QM5cZratnspc1HY/VTdn7Cr8y/x3zXpKZan26iFJIfKzox2J3KI18WLP30oCJ278WmNE44XTMK7
IrY1DIC1e49i9w2v4ZQSc/Tz85NTb9RufN9TPRXbSZu4YeK0tHSxKHxJUW1LH/5KM3I4VUsEmnXB
KCYy3tCpno8P3mWb0w39kn4jMGSEihXqwm6gqxb2nT4GoXaJppGjNmEdSU+y4Ru2xYp/Qv3dAo4p
YzQBQOYHBzG9kk3Enw5uWZLaVTZmlbMPC9nueJ67D5VeckjuLSGrt5xesa4Rn5FEmKWyqTOsjmoX
QVGiYOV7YgmJn4KWNp1phtrHuu6vcReOQxk0gIr69FDwBnJu60XGuLLF1ZvGKRfsLlQHV6bX8UmC
avK8PsuwJZs0Bg2RZNYQWAZUZgHsXzi93oL9kdThOixBRGAYJNWvDr1gtFq7HOnDTEebV5ko6Q2n
HdkkrBhq+Bb0akAEO1xTtUtS5scV7zfaEKXkSO/esNoHbw42qIMKY0S2Y7UUjWdTMjVkaIURpEP1
3m34rRxm72OdRLe65r+5qfI7mAJJgyBJDJ/80gM6aFGFTXuUeFpNO1ncCs7DlZEXZY8EcHzneCKz
ZR3DCrqYDUmlkBkRyIpfVohN34q2R4J+nYGYdv3OUUgsrtjrqgxWfXci31Ov2QbKcqzZQcT2ppzZ
5ZVvGQW1VJ51zsEP/sDSpb4mqh0X5xmhqAXMojMI0hcwrOoPDrUvI2t2NDY+OjamcrgbKLDzd3c1
DjioZvt8q5EtCaxmceGBtam84PqJe2esJ3aKBTPD1B1EnLLMwehl8pJxFkyeW+8KnllStbIq+Maw
kAQjRHCqLdb4iLJlgkmmSK1++htrB+1HBwcSKVOsCZdG74Gy5269TM8GuLy85BmkYXRoXh+zxPP8
UZpk5lymV6fgILPmup+M375mBz7IiR3u8PBFH3EGndcwgQlHYXIBHz43EG73uWgGDY8Qar6IqNHW
4f330eRHbubQfnxeV8Cfi0/UvN0vfs+JmHcowkkIfz7HxLWE+lu8IsnuQ3JKnt6RTcULEBHQgK8s
gHG4S14K/D61qXJkknWSEYOpCdYSgXfVQedsJw+d99uOup2D9sDJhrgadaz1cfGTvO806jm6FmfS
xSM+nw2ZqDLjDz6lZYcGmgmvxGFORepP55sspNuWw2GWPWg69aMcjPsEO1da2artUGMgQKDudufI
57PSD1ApNc6UF2DW+N6RfSL6lHPTtV4NBU2CIotIR29ohxsSsRAVaxiDaTNr7NdPEpBBpjFOqLgR
R2s1ItNgACHt0ZaHAtYBg6YqRROt1IqiLamXyE5LAWpp5/n00oCFoXCz3eGytCs8u09G0pzfh4bi
DRHiBS5JNkteHJVaKQ1fwiAnKwicP5TVePwwBOTgC9SH4xgPW1q3oXiCjsRm8hILGa2MKtQTfeEJ
7/IQWpC7qIWnHEe4UrdacXDu3xmeR4O/x5bpXCxvIsCSX4jIUGtWR0KSEXWqM5aUHFG4950fkCJA
HXo8aKtdYTnzdTSa8kb3+UGhnTunkXg/Lk3ySPqcXfmLP4oNamLFJMN3qjXZ1/581DhYyagD6zT3
gDJg6iKETRkoQpTPIir5HN99LkcLRPwJbYGmu8f04c4CQBagftLX4d9kBpkx0rzTfOZGXqLauUjM
so7X6LyhycL/LnGNwttdAfUOqvGixe0YOoRJVS/EBdhQPyJaiVk2ABfuqMeHjJ+heBdTOHUhuMQQ
adfqK5gNYnacrzxZUHMZC2q8jJ4m0a2lXzIut4+9U/hi90QQJGU3vSjXdV7ZAcrQnMVnJWSqSxNn
yNbPvHMsmVE6YYYTwyQoHOGF3sDrI16fgLeW48GEoyPtjrSwe1Ktw/rr1GP9cgrPPuOxEIBfvXwP
JRFT/W1U811Licfrf180XGaIjutbBupeQWTiyfxFX8GhJrNCMJLXajsJwLmT1NXiHK/XMoqKEsOf
7hYPAEJQdppv7s5otwnkzmU/6/m/pA/RUC2ZlNbhga1HCUF1+kkHJhgA1upuVxNnSnNWcmExWyp8
LDCU9Sfwrf8/nARRcl7ZxYa65jdp3BjAX8ImLWrupHZ4ebMGbGZX4C9S2B3Sn0LZxZeSDTnIIVFo
GjaoPxIqWiBBMaD8Hwmn1PdyQAo77cTYIY17CDJhE034kWn9F1rs2qrx3dF96sERnkFLsE7gwdT6
xVvlTZIXJ0C9tQNvZxFU2gNbHO2j8b9EYK/kCap3mDyUHAJe3mBfvOIKG9gA6o7vgqB513hASLxB
Gr65zV6MjlwxZaWzT52DtLoyJ5Gd9X8Ty7JWJBTIeweGq/68FdFrvTDx0DJvjTfcpFsCJejNIqtH
Tu4X1911cO1Ez579sWJPyWOICv0BnzBQp7tqfK8ynKEf777xgLFZp21r4BAN/aRDDI0R5utdzX17
9TL3r/uhT/ddHlbb9RgZhWBh356thhCbI98I8ccfVlDllzyK320LnWiAID4+Zayt7Jbba7py3q70
HtEsW7/FjQn36gPOElUc4NlMrJ3BM9wJGHESWcmd1mEVA5aq87BtIOtwIB4NyFJvS2WAK5HsoT3X
3DKPqxN/6Ku66if0knJRHIkqRoGeQF11L6cEYDwaO3xlZNha8WrIlitEDvLdJRDQ//rf/yg32WFi
2zpPjF3LScnPkhRJWAqZFZSlz8h1ABhXS9Edm2DpKShWHQl/aIQNTBnDiooMIO1yKQUbqfUKpQeV
u7+rz/7UFZ4XtfzzGa42ySG+SZe3G3KRLL5lpRbfG4IHXmhuz7NJpk7qLQ7zgUzEjkYbiiivP9tV
/OH8zno0M88+K6Do/1Al4BOBsyxblA4JL8qEmg6V5iYNAzZjHy0biB4cHd5NPQbhLg2Rz+Sim5S4
tP/LE270974IFeyKqq53InE7BWPNYFuzABkUMKHwx3byEBiSXJFgaEE2PhH+R9TZ2I9sk2MjvqGy
MZl9Uy992Se+hEIN8vFlEFzBzPovGtoTQGJ4RSqNZZR1BuEnN5SgFFWsPFqD+cck536vGUVXDF2J
MdsLkBQslpXfZrcgIj8i0RjMUFUO6OIoSoaQaKVT7uSBRNGrvqQal6jm4YWWKOStU+1LHrnR2oov
b4Syn68YgyZysXBi2QNOoRsSAl7KABlZxDrGf/fM+L48Uo1EDQW4aRwX/ohVQRa2pWrHVA+UrE/u
bTbfFscLV1HLEfSW5dkIJyN8YyS0H7RtDB46DaLO3g1zh6nviK935iWqi+LGhy7iXAXJIcDHPvI+
h+JX0/0oPc+T+MeGLE6QOejJ4XPC8uKegL3VrWzZK2LFDfpUgSFlo+NnvGZd1QfBjiWGaeUko2gx
6jpc0Fo899FD1zgxAIcabg1KMlJqO8o0VzHa1XwGGYZjq9oKOwYxKmXu8KBd2vbADAYfufH0H75L
vIb8PbZIWKCsZiMyF1jc0VHuFS4HqrlotYWbWU3ktUmb5YcVxWjcqwgO5j7hmud6icGNUA4KLykd
M5jD6JVWUXpnL4VZKMHv3TItYD6QQamOhuigiPBrwLeA45rwxefnne4V4mszdIgrXDHE+OtsC81E
EjwfIdK4Hkn11BNaf0Y/Y18I5eTS7wSQTckcggREzTaak+fMPslFPddzDoYm9b6nMHXN0wU9wYWa
EdMZKhzfOqSpbqwksTDH8L1d42dUCGwNjnoR2PureZcqfNnncItPOCRmX84/05LjyBcexmr/3gCk
+uc9EgFDu+ZvGQEFuv6PLMYucJkgVODvEZ6Nn20yR124aOt6jbRDCDpejK7flCLPjBC7NMTO68/+
iTkhwse359uwIg7Z79BRnvFAzGXshMJl3/PWYbQOwhTkhHvuZSn5aLNAbv786QA6Vb7rTVfceP2T
aFVtVPJooSahnwaWwV0EOzwRT/PRIhYC0/sqL3x11rRynXskrNnFj09CRSbdlvyDF62V3TNMhwN1
R/tBkpSHqtnbCl8GWcYCNi9+RnsSeyG+ZA6GuDK+j6U8KtFe/2VFsNCJnA5K7uYRjGDXtzx8cHMT
jkb54bseWt5SRdyRc5Fiir6CqZXqjvhHUQ5r/cOlN6virFLeBMmwgn/pdeGzCdZlSkR3QLguJcKV
rF1qAEXJwkNhjR6La5zlWd1xnbiy6LyvBKAvOzeLD+cTuTCV7FtC15Cwpa2ca+lITs1G4FXKIP8/
1vi8aLNnn+uP07AYF0uxHYqDWYtx8Qcg54VDmPKoWvivv2V2U9YnHH1AwrV+pXbn/5MKokdJXNgv
xP+jPUIeoAz4SehHc6475POWsBJqR48Y3l7JuLrg+xGGlC5qzMdiRb14nhftb/ftxIVJimhWPeR+
5hFFKVyVjhoxCxJTV6Q3kI9CFoSordzYhVLh2M39s1/MtYerDqMLmh5pklYCXVi2ySTk2Vuaoj82
Ois/gLwFxLkeAQTdN6ntzCM0rYHPNCs8Ge+W2V9eSI+nILp3ju0oXLLZYeUg5WF4IkqQYGJZn/Ak
TEnte5O3JkMI18YG4UPYGrNmE/8tATEUvPTkfAb6+V7nfL6YEjdue3uyHbN67ZynG1kiaVvnb7Ca
1uTgO5BZ5u55PUSy03Illm1D3UMnS8H1jN7ejIdSS6n+42NzzohX0n34iO+9bUDBoz4RfES+N6P0
2gRxYpfNv0ltYrwchnHUvRhJh2AG0BQZtntNrLkuPSM46BOK7wF3czxGnre3ovLvTt9SHeSeql2h
s01GcoghzbVqr3Tl4Tnd2GaemWfcQ7KNumGmc5GiR91fPDcR0kM3P0738JiXRZzV4jcJdOl6SL7o
BJ5TA6T0Uz1CXx/ym7qGVUJTHPZtEFraA9LnSsNCZ9rf5Mm+hP9AbwCgJREMkh748mCSjSgpoExP
a41U2O3irXUYFEsdXPm2itKCfIsS2zwIocrkcR+VQPcT86XEgLKskae54+UvshDLhsBMGEzdS8C0
np+hbji/iz65I8+jTRlJ//8terQkH8K4EE1Uv0cCz+d4PVpcjeYvVdR1r60F/vRQy1Gxodtz+ZAU
DquJvgpGBRMDsGjMUKdl3ZrUL4vZSwDaVvCg4cY3PYBgdtOPp5RrL9kfLf7KvfJc22xBYqn8H9AN
r7FXLN18T4+JeLiz6M8I6w110lcjBoOVhfkR0+TtquZClAvzRkWZKCoQYSJUYogoebTKclKs67Cw
OvwtwLWOwnWLSnw/VamAOXzC+1WZk+KRAQmkDRm7rA0NiR10/AHEjk7yRIgANVsvWH9izPq5D/W+
NIAfY3oNUYaglJ4FnHWY7pD0H3qMH9+uEQGl4JcBcWaGLvPmXDKaPeHNF+KVctOnFCsTauj2CZjU
zIaHq0fEX5TJTXzDqRJUoE2vwpW3tOYOwXux+KVn91817OnyE/4kIaaQrsTbZL11EHlzwIrRGVn4
AlHr+mF1xKJobn7fhQrbB6uwaJ70IYVc0jihbx9oktEwYstqmxFN+O5Y1liYDckBNuvLdrsnfjdm
FfkA0YTOslSid25UgSxHuvbFKw4E6Bcn3zcdF8yHcMmYybWwOFBoSZGgXfac/FCBOYkyAnwFhX8v
cYuw54cjGnMEFcRI6HpcJYxNf2EWatswDdBZ56LOBoq3YCmg7VlvWC2XLpCC9FCWn83ohanonU3l
+LJjuOfC7lVaiV3Dh6r+U+6i4ZDLiZv5ffHpxqI7f39yzU+kv5gdQ/vYxvbSTVH9tOFT7yAcClZ6
tJ9mW896ipbhVJLSWt21OjjGZX4zYaDzMThv+I2BD+UoqM7o8250UiKfPUChhTxBo1QwTZM30eP0
Z0iKI4X4Iihkk4/HpDGR/DtBLXoy85TYpJlgEbrBMRSBeEXuig57Rpwy7v6zXqBlJI1JbbjbEoDy
lvwuIjX6F97mKnbhXYGYLRBXfxr9y51vj9k3mzpbciO8NnNns4To3/N2GyFc+Gh/o0po1ktCppNh
YDi0AVIIEoVvOqStdtnoOcx3eXgTEm/EPBM8EyAjRsd5cjWR5oGBvOSqXYxEwTjpD+EF14XBBlD+
i1FWPmTQS1KoAWuwdx64M/CXhMvbM4lkCHsQRI/Ss5X2tn1fKzmeBv3pdXqxbShc8fr2IH70P1RS
Eei1oMtkYp9petq8iEcL54TxJRVrspJ0v6iRtLkV3XV4UVP4nIB59ib8wHHRjOiTkeuPiL270TtE
8JAT/g/0G3VPFEZnZ/VoEHmN9aPIOQDRGHA3JqsEIMpRkfUdZDdFk1F9Rj2l6oEcRu7tpg3NRpvj
I74NmZWIW+PqHbVxjBio/WrTaYDm2815dO0BN3EIJ/2frCWGuvyEHkmfU74p52lLU4rD4pdQoyTk
uZkFk8ObQyuOHSaD4Xztcl5tbXGQ4bJcLvQ1ecK1qUybANC72mn70gUyXigv0EctVOikCODNj6XQ
FgpuyLvMo8VZJoFUlmd8AQ/q+UxTBdAZjNUS9gId/fcosq5KcZEcfnLPZmGfLAaLlVMCI/U4tYtY
Ve4aPI3wD968oyx0mh7KA8QTNlvHWt3DuM4+pYP7xrXLo3I6l5GVRtmTGmkLlhAYTbHGc0SAU/P1
fDidV74EIZcHBm5QHkkzd3unD4o6TOy7kCNbhrPP/7gU3s2umnBUI04OYN0aNJMIeEOJh/3TO8gv
BuSuY/ibjxuVsHqFoGDQKJikYle/ZvZIgGrfw6l5TYjkzeJ1qW1N2Z+8ZAsoh5A+f4QdJm+L/Ile
QI1kuTlyl1pwNcih67ZaNCpwVk0DyyowyNzCjVM9SdlAXJkWcqI/k+qTbgM3+xz12qeK4IuWaJb+
84gR8o9saMoREsmnPXiRHYCvtOHxym8RsSA1a6PF2kCkhkRaRe5/lXSGcdrDd/i7L8yE+9zK1Hgj
XfC8ipoCmxzQLfHRFYi3z5PO0KiwBZSN7MhEjbU7lWc5uM49sXqu9WwiiAlmQ8FPL3T0uVSHplpj
BeOMb3PwRP4FRvKGWdxi3XfZONJcAz4yVZTu6mmJakF+Yr5nJ8GTKBI+68cWO/lH/6IupmvDDtGV
dOxXgMnRt8NIwITuE9YIcoKM/gw3xtoYhJu5Pv/4dO2NdFG3NT5pAJ9Pcf9zC6aQNfgIJ/WqRy5g
rtuDGK1PVqVgIxIXtMDDSeoWQ+I9y1iME4mWZBkffJH+UizOD0Dk0/PrVdiV3zeLxnYu+ixSmbBR
sDjNw+OGUS32f6ozxpLpyMuOdMfIN6Nd5Epef4Coon8SCbQkPhv8cpI1k0uCpZ6lvNc4T/uq1Zmi
YF5uIRSK2gOkdpjj1zgxYbZp3VMA50VR+2dlAmZu/pHNUnrbPM4BztZmyLXKF9fC7Iiwccrep97C
Ijluyi4KtlMgmQHdaq3/Xr8rP+EKLlsKXhWR6fbHfMPwR4OODa5loweZBwgUe81cUKcpQ0PIJcWk
CBYohFYnRGBCfFuyI+16rduv3UOmw8QxP3ZkCkkQdW/VdF6clu1qdm2VU2+nAGxKD00loZ6sFFSr
DD0sUZ0z5Lorw0QNHOJ6wBYURPpCKHW2Jm2ec+2fDEeTJuK6kq1vqDubp7taOWfKRxdIQagK21J3
Uruo+hOGAmkssPbW0H8xymfScnA+GQJ/pNy5SEghBtoCn30/DMn75BbotRCC7NpoKkZ7Qr+AeFp4
n1y6faT51vr2Eq/i8ZOLC6YcnD5HYZkgKB8M3BT+eE20EV1doPYW0NapeKlMJJLiLX7lrTxUFfdm
pDQueJ/Y1pbBaWEdonGDu0Nh2eK2ILUqSHXfnFfhqC4MsWxdAWk31o5e5l4osGIBgSHZ8/AYA7kb
6onXdqpCKj/osgQmW4rTa4qevM0lROFqTOBcn/P0JYT6KKrFc/MUaTnhU4Vg/kpWr+RvaSFbMUKo
Zvj2XNr1/YeTgOFtu6K7y9DnpShnmlHvTB0J7gfrmTiaKdTn13GPDlSQAMeALQFA3g/haQzZnIM2
sbP3w5QUPNgSZjbhPkaH/i77y9PJJn5HGaSu2mjn3AlnkI15WfncSwwCMBeRjvgdzF9GvFfW685E
Zx9jJpbym/TaRtTG5sjmTaPFPBJByoF/X/pRBWAyXgY/80N190DXkKAiG+3MBwokcV19hA6yOSb8
zK7hHPeqz3Rh72l7a5mk0UbqRiNC5YneRjdqfMbpMScnZxx1pMs8mRQUSEFfT05AViBHJvRlZ15b
F1GfMz4CQVhdsmBEKHSt6vGZ8DiU4CqkH0abzuqbfzb3qmv7lph+fKvAgZysBhQAGltGZZUGLm17
nzKqdub5HDZIxoPoVZWIpdBrMvPsce1dY8GMU2CHsF0rt93rnkoAYIEKdbdhX9/90gVJIygN1Mll
bkLQpfgaJFcb7pO9DWeGRETaR1DHHdFAxMHKDXmNlbC9HUGPRve/xHWtxdM0H+OYsyR37vjtP6y0
IPdmBmmfCk53n99JZKlVcl9WIv4koAHT5PhvGQI/N94G2QgxrV69qquYs6A7MijDbYT4ixEfysZS
4Fur5zsXXNhLqEppAz5hRbWhIek88/zQRFXFgzRXaqj1ca+nPfKI4utWeCo+ej99kNZZ3OUBqMQv
SvcHI/19qjGHogat3Wakb7RFXhYyAvDY4HcUZDSABqbOFMNyZXxuKC1KWcf+Nn1sOHMMaBOIA8W7
OmHX4Q6L9lkc0PSt4PJbufoq0qIOQR8anhdBz6mn/X7PjM+2aX3ZU+nM/E3SMZccd+nUcxQtzeFJ
gbJ2GA562AZs8kY/dg8BUAyo95pug+eRFvoo1LlO2wNhigUjnvPv45fGCRKURxxH6aMfKeuT4T/H
SBwBwwNOK3RWkQtdMHvF7HCkSytQoCNEZFfPH0TJgVNFe5yn9CjwYps+WZDbHwndCOVUQWRIQbD3
ERZNTpZxgb5/8vr4HbElGfriP5I9OMKEuI3TDjcaCZA3GH54YL2lClV/Bmndh0zuipHoyghYD4hJ
0zPLxwZgTC/1CPwVlX96wU0rLKH9j0lORQqYcSTHiLnAgxiVIURZQhAwPTEdLm1mzapFS7fNTZnw
bNCVYpCVE6fACQlOJMyWZh12SOMN0IfhMfS2MHZZyWgbvHF/hUYsxahiozUoVEBxaqFZSeeI6xb1
zc3hbkRLYok2K6XQ7Dqc8y5gb4aZOwNkRkrDr8Eo1dgcYiMQ87KuWzI7uhuuVkCcTBEIn8orvGLi
5q+viix5XkuZqdb7UQhdOFog2Gqj4xVDKc+gXL5Ovq3f01FTayGnhIRgDXmleSYfnB7be7eDgu9r
h+AEe6mESfUm+2tiUAvb2n1JNf/tt2puZEmvGxQfptnyhvbdPrroQMp/Qzv0IvLwHejHFbHVMKrF
bit14SDxOHbSQK1KyutSmGfe2D7+reA68KV+rb8jBBHNdkrCKrSKtSl8ia6UE98EFHn0dZoMxgpo
6RqEav71lmcPWWGToYwf/+eDkNhdjJTkeDwxUcQCQjnY6Mc7sJFoG7tEXOGpTpZqo/cvTsQEoozl
EL1HgDvMiRKNTQCu7WkWS9c2vQDUl+XGGBjP+rCCsPFUTsOkGxciZYYpeitfauIzBn1y63q5NEGx
uVz9Q0pFMmXkDVmf44D8njiCUQLyFBbIWyh8OCiVriur+c9cbMSqywIJmO/t3CuHHqA9wgJCUZ+v
OhZ77Rjxz+Q5VykXkAC3aqXgTcPiiEPkPMjQzDSS2PSgug3LmDYU8epYEhDk4yk9b74qDsaSnwOL
6wMOBLqOEAVWzc/2368H8/RV+l7vFxotCmwLA+vI1wwgFEyLxfU6NeSfFmGkIBKk7npSPbYKTa4W
xdUY8Gh+tgWxPf99madwTK8ZawNuvLFrq57SiTXGjgXjUUursH3LfMDd307pn47jHdv8IoY6tw9E
aOwTrG1nyiEWGd7TdJ6Y1js3Le8ES4qJXySBof6ZxBpy9hHonB2O7Mm2UetMIId+TSVOt6D6WSnw
tzpvz4kvS8gMRmbwIMXeq+ds3CgKqJJlZ6DPqkAlrcOpNqmigvfuPNRdX89manFPE/CLk+u/EPvY
EbQeMWW9t5NMXxkSF7MwZf4KGrcHHmrhflMdF+GgjvLr/GzXshKlB5jB/37CEqsJqaQJAtlN2K+p
lGoQUYE/XGF9W1PZBwEts09D9hZe3QHNek6NkSZjWAC4bpcfpiYSoC6/RAO+Qo5SpI/Hr/cM/Mpq
+yWffgQXSiTCIS7HcYPm+5aR++sTzdUTJkJlm9zA6BN6ek3+1TYYyK6hrtSjTAlq3qacLe6wrvsY
AsDNkxjnzGzdiJQuIAm1Pa+4WCJ9VhNlFrmsUkX3ed3NVaAYDtf9fe82acrru+Eagwmvz3R095Ap
oaVTG52uVCjN5gjE9reu6GDt+0aOd7MzR3x18oC0Bx41svHSe9mmd/ZUO8DwKfKupRZ7SMgf0Rl3
sbeNbb4aa5Mnkv1PRChh5NFPWVIM6L/A9EOUCCgTiUTZbRYUXaEoisDw2hoV/XbwKUL/vLj02ZOu
MjIZOtiJczf2TELkX7F4Fea9EMx4mMfB/QQXP6yHnE8GDRDDIN+Zq7aw8yRSBfYTRD6LUbofdZ4t
Z6qCxj6ELMGtf0KZsSghSv9qWKdwwDZ2yxHMlwVW/b7mmGtsMuSgP210DuFJe5cUakEMeTZquPiI
xLdy3Vyho8Slak8K4WyEWzthl0l3pnvwuDSELEBEuWmNB40LeKLAhgFd4e4HhpdAQXzLY/FSkHpO
aQYsrWS+BOclThfjbA+hxgNRKJRxNDbI6b+urtfMfkueNPlsYeQM+TI+AOAIkWxbgY/WF5uXrS4I
dGszFSrWHRot0uS6Pt2gG8bnLZ6FXabT4u7yOzSajsJJdZO0pCEHT/lLvXNcEZlWKH6+8hfwhjwv
ggR2qaATenLPTTpQh/eJnsa4deB3VcllYnzdJ7k1lo2mZkx59RpawIuhXz35XtJrlMkYXS8IVh4W
vWDT4hGKkaM654sE5Eg7OCe1hzNeFozPq9ebx9DiQ3ZI50yOmuRd1dfeaOzuJtrePsz7a4ECEaJY
OV94KeFGFM3A39bzBN3FDeti4vKyzVXyZ/tivf72UfDVO1hBG+Pj1ULRBiONyp5xSmqAndrvWls1
7uwCrAi3l4uX2cFvessPB1b+OKJkmPIKKm+GMAMozpyxFgk61NqpHCT/IJxgf+IKtYJPcrIpg5rR
79Rw8byCPNZV5LB9qpB4URDQ744dQxQoK0xMVgCLFemBNs5KsFLJ+i7gO0pbj57bFYI268xnyT1M
lTFZv9aBK6EPpcnlILhARQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
