// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 17:00:20 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ blk_mem_tdpram_1kx18_8b256_sim_netlist.v
// Design      : blk_mem_tdpram_1kx18_8b256
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_tdpram_1kx18_8b256,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    rsta,
    ena,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    web,
    addrb,
    dinb,
    doutb,
    sleep,
    rsta_busy,
    rstb_busy);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA RST" *) input rsta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [7:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [7:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB RST" *) input rstb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB EN" *) input enb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [0:0]web;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [7:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [7:0]dinb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [7:0]doutb;
  input sleep;
  output rsta_busy;
  output rstb_busy;

  wire [7:0]addra;
  wire [7:0]addrb;
  wire clka;
  wire clkb;
  wire [7:0]dina;
  wire [7:0]dinb;
  wire [7:0]douta;
  wire [7:0]doutb;
  wire ena;
  wire enb;
  wire rsta;
  wire rsta_busy;
  wire rstb;
  wire rstb_busy;
  wire sleep;
  wire [0:0]wea;
  wire [0:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [7:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "8" *) 
  (* C_ADDRB_WIDTH = "8" *) 
  (* C_ALGORITHM = "0" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "8" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "1" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "1" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     0.32832 mW" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "1" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "1" *) 
  (* C_HAS_RSTB = "1" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_tdpram_1kx18_8b256.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "4" *) 
  (* C_READ_DEPTH_A = "256" *) 
  (* C_READ_DEPTH_B = "256" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "1" *) 
  (* C_USE_BYTE_WEB = "1" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "256" *) 
  (* C_WRITE_DEPTH_B = "256" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(enb),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[7:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(rsta),
        .rsta_busy(rsta_busy),
        .rstb(rstb),
        .rstb_busy(rstb_busy),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[7:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(sleep),
        .wea(wea),
        .web(web));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 30816)
`pragma protect data_block
Im45dUPbfMC/eGeD7kPxTKRcbmRmcnBmfEVqU9z4iKhD5BNFDpTkH1xwvBx6Kyksc0Maw8n23Jk0
N1SB1gcKMc5z0C24BNICRP0zQN/3PLEM2V+PLtES0oMrEKPBaXvYq514EvVsPLqFPnwp9bNstzFf
3kgghETcW/EFH2IGw7fyiVf80JdF+2CejwuaWoeAN5ZLKYZOZMxZfLkBwgOAIVY1V37dkSXlcB2D
3S6n+5ZXp+WxQOeQuZAx/sHj8QPQfnDJCseM6U8fPx2eUGvkRQFkM4sQ0HXNSb5V2YmRsLf8YTsi
tRCCVKiWSzoPjkD4TcZ5tPVXAstmaeijonaGmeTO72jjMrnSn0uv2QEFrDfFT4kNc2SMshLituJU
/uNRfJBjGNj0wuxBJM72x+0BmAxGUrSxZCnzSXAAag87pa1pMap1iKztifbTNr/MjoBq6l86IbXg
cZMuXQa7TVRGJGqHQEpRKidhOEOK8DcBxA1bZnoaK6iQTQ3/dvVWunNWW/hs8UYHCZ4EXRfu09KP
LP0v8JoXZmFxZ0LQY+QJF2sHx0lP+VLF/KkX5O61sOWUjQe+0zudH5/JW5nt3dBZjZSFxeXB0U2M
pC/M0jV0jgacwoQPM0ATuwBV3MVQ69eK1vWxklmpTOObks9lP0ypMp9+1kKJ6XGo69e4Y519DGUY
5nPGed9wHGYKE8SHaYtjm7fc+NbShAqOpcTua9ReayLuxZIylxqkjyvgFLx+7S36A3gpbUsGIhhB
8PYd1z/P0BR6qNZTOvrJVKO6AbaLZLlZnq5hYV49DlOOjpcRXjXJjS+YnK7HP75A8vfuVi4ciRkm
MCdVuazwL5NI+IPG08+Hl+k0N18vKLWqwf48eUuVfiN+b9t2KJvJjqHRNLf6lcfbU1eOsJtMbT/e
YvQ2aaObYgVdu7ciGA0hq9AlFbcubTM5zp8uZI1HwQOMGgP3Y36/Pq03QthRsWEIyxschHRHGR/8
+g1+FkztJWPlLRUr8bP2huS9kaKAyrD+4wlf9mSgILPOjUxjmhIzsqOWjzC1lsVIpQ+dcDOmnDEU
SX1vTl5xIHiFFCoK8+gNP/rC6mpwLBf+2vnEshorenuRHAnkI1evUQ9GoMkWAsMyca55Tgfzyif0
8Z4IU+AiAQeltZmu2iV1p9167j06NhRGx9E7vOH3TllMCasLqJWJlnStOkK86ZsG8Gaz6uTUlkKq
edZ5oLo4TZjk5TyYt6CXGvSvx/7EI5ZpkEo3JiTO696yT0DnSi+XNQ48PisEKa9VvmtM4hINkSY/
2WoRb1xsFfaees5NohCT5Rgz5n6LYUaCQidojWaWB75xg1hDoLOz9kzTwhnacgeF2m5ZFD1/O2AH
jQnstgp01w1mAWigfJ17F5B5jjRqcFZUGiuY/X8nrm6oycx4+pp9ujn4qRCQmhx0wom0yGvpDIP8
nefDiT9v4jOWKS4mix77HTVEO6/hc2jtql0jO3zAzRzEN6T8AUYS9Iocedh2miHEH8rhbJhhumRn
3hEfNB9OQd1M3Q4P6yYLUDgWvMnxoyUuQbGgfCUDFaOpLXra2Zr2gSqCqEn6SdufUlYurFEOdXMv
8lwcOMeHyJvHlI14oK6mwgeFbHk3of5RQvT2cN9nRnPT2gzB0h6uCFFYbrbwVy31+zkhxRdYJRxe
PxyvAvMcF/ddVQ1JMburVsNrlpIDhHd6UYvXtJnMX/fO6fb4rA1OingtQFNEE6rxGekrz+8u3sMD
pXNRXLR6AsN0xc6HIRuIa8RxcYiqpmZ3QppQpuno0FAi7iREnVbCPp0G6vzeA09fZPg8238hWg64
okUBbcJ/1dhwWLzRKzAPNg7oVKjINXXpWPW6EGJIwGnwKydx1paMqOZO0sY0x9idAl30Tzzz03Bd
bPfwqZvqDemWZBP6AfqEdoxUmB/sjUe7jUs/XVueZoBVI1lX3hc6vNkym1Ib9EtzzRH4x4iujz3n
4XDfs9Koq98Nv0yYaQpon4V2oAVvrppjE8vKwJCk3SICDJJyLC2wDipz5wqDoa5b4HtM67WAgSHD
2LwXh/srDZ1bSOd7vOrcwZZMiRj5RlnqgFzpux7z6gLXkXN2v5xj+yM7ePqCJFTeRuIoxWaPCT2t
BD+8ckBYtN6K9u5NI843R9FbjPAYE50/FPGoE4l4UhfLnX1uI/iXj6Z0U1Arkzko38B91Wh1wv36
AaCEQFIhiPADnJF22S5ZLBSS+p8aghymgKlGOIx0eZxJtPnASWCCZLY8rdyhRyxgxU86oZs0mBbg
j+wxyyxqtezN4YG55nZ3bImgG2avMyqWEtQRID+s1mqFrYGRE9qRgRNbJdIABjrjJKC1QrYgkoVJ
iDG/qvusKGhrDAlFr4fMymwEcfm3yvge3k9X2eluopd/WyzR0+Agr6Oev/OOo9C/7qwIjnOhz2ho
wTDrs7vfPe+3IX76C36qkwFOD+WlxnNLUKNuAa+iNI2O9cQcxQ4aX+P+44KI91ZbOgesibki1vuq
e1Ql15D7DXqIzWKVKvUaMYHyUwPC6HtCdpXc/RcvFb03672FA56rRgU7qA/C7xwkTSGuMge8LedJ
jdaiPE0ogGB4DsfIc1qVvmR13QPewcNbYlj4BlBZ1WiHxJVx9s7TvGztU7AMrv9gZfAdEFnCmcvX
OHORpYefX2OGhBPAxzSGQgQHx+MAu3VF65pH/fEQWYBk15kEcUcnmYdVS+CzlPXuZVppr35rS0HG
TxANpUXiEc03DWXF8KAlWGIQksCuki1s6MUl/xfYa0AFP9365oxSK/83sTly29pomBU/VFUobuOr
xIdNuBGs1IMUT2wOF3QuvyLUohjJNgfE0EHoRjlseH1OoqTjP8G7Dp1pL7adDEwhDpmi18sGdEId
5KxDgF498AhyJiLQZCeW2DnVrQNHeNTL7NIFVODY0VlLNuchJb6U3Lwekm0uw7MuhJ+nXVwNF2vk
0uVizWuUOQ/BYnbX2lWZccTbLw6LKQKSUw5HqPsBmYAWOCKBOUNVe/Nv07JrRL7nL/E0mm6S4QqW
jBvOYO2Bo5HHFJxRJVI5+vAkqHenBhyB0hnRCxd+f6vXwmPOCl7ywDOUHjeQQ8TkXCyMIuLQPz4v
+QAGpnSfBpxDDLk15Mcyhnlx6PpJWHGR7xfgxRw5tAXsVFIuK3CDm2fdHC/l4m3d56xSkqy2LQ4c
nfbYPldL/IuwyeG0S/sjjCce5tqBdwCcpaX0UHQtxLPQp0XDf2xHzPzwuYvo1SLkh/lu8SH1TRne
AHwhNJL+dwbxjpUayY1CAQYvcZz6c82vJ3w+rj8dT3qP3p9wF7M4AFz6rGDVOj04pvsSQHN/BsWI
2qp+83z79gl82ACb5CPf4MCyekDGTuTFutQKirVOn7tbznwU01cNLa8LtwSJNhe76IqFfsOWWeMk
iMn+EuDSJRPWJ/t2qsxNFqzUjWHDtHfCc7EhMYCF0iL4i9J/4lxxYdVrGaXBg+PWOXw99wJGkwcW
ANXVSyh46Oi4WsoNuJWYxiCGy4trfQrmauWbB9GC2uw+tSDKPFIz+F08Y0VYsMMgLDPG1L9X86gx
i6uU/bKq3Fje/tANP2bZtK/Uoj/+xZYJsdmjSWjGpvk4VGoG9YLZLeRsvB095mJLAQCnSl4xQK18
Jc5fBLXTJk4zLtnCqk1XgplB5mppfYIjAZuYFYAD2uDM7QHJkbC7xALInaFXWVDHYxhExq1pN2Td
SW0di7dU9HkX+XKB8a4Mltk3dwKKo9FnXlogVTQS6GONM9n4eM5IHgnxo0To1CepOg4LKNr19mFU
60IWCl2GL8zDHQQlZ7FJcO98rtK8ipkHoVboPsPn+MOcHiqaXs/n+ycZaRBvbpXJLXvAM5QdXCrU
yFHkoufRtTfvXmmDjyHAvls0dq/VcyxWz6A9wibyiECv3bpWtq2viSDxTOSPzpyl44FLGXxLUWH0
MFIee1Dk59IWduF1iZ5wan1dpe7huXrbziTTo8uHaeoO6o5N7oH9ApzV5Ocgt2UkWKLatXqSo1Jl
M6tXwDGaC+KBr/nH08HWLCqhBTJOVDbqz1Gxo1b1YCJwEZnaJdFwTlrE6H8qvRfpfzhGkJoi50Pw
+0kxnU0AgUQZMaR/SLjgp+6463KNa5TL82h3iyZnr/3M4biTVn3QmdnfY2lvGtsRszjQlgF1y5pm
surbQDMQioEHhSHrVfBIDMgjNUeTr5p0jBwBmK40Y2L+XaBZX5UJDOdnr/EWmMAfRSNGPDCOnmZ5
VIKGY6VQTQxT27S1R8PFTRbxCI9wglZIUeHGm37Qr1kiAE9dP+7w6wiFlwy0eGqg4jTh2HPV9ElH
8wbPTYk6foYYIow26a3a4yy3vQstdkXQ61XH7AGk71fMpF0l4xD6TPU+dmA/F9X6Kch8NF0+w+ON
opsPTFypKdH4Y6/Lx+WcMTc1vACEQBEjj4WrFOB5Ji13yJsZOLKM+v4M9n5DFu4ZzA3yVm+jBSv+
BBGywxTdLeMPmIgAwZPy9I4c8QWeboIIWZD9HgFBOG8yv7mckQi7iPeFF1j9p7H4+kD3KfF2EC+o
h//0PAC2YxW1LEwsX6xWK3kLjC76LwPfL11x61MXD8BhPhCHnlowsqNVD6wsrJ2BOwOiy7XTfOa+
iCRfM0khNEIMI9C2hBdgA2VZ8pnxHAj/uBFkHp0zYnhJpv5SRp9pO09E2/KMEqvlzYjqTxCrlGTi
XRDLaejG8Uus7L8mfMFrEOnQuyFzOj6kNPgKMimhaGtpaGkeuTcnh/4DIcjC6t5xaxbY9n2+OP3T
R0OozO+mVoPAozD9JfL73CL5Qr96QQCAVSw1kzNrgWLz6DPZDY6796ebAwhHWPzaGQ/ATv9qvnj8
WujsUT/Y+HiNfTmKDRD80NMeg8Usn57vp2idNlT63uPY8IkvqR+kV+m3pi/bp2cQ4rSOgCS8GkFD
9fdz8XteLW0gG6duhSFxrCKXVc7QCZuPyUnprteDp/TIQ9uOgQg7hJ66HWxTeja9y1vENl74TAy0
0Jts3UJQs417ML+Maxhlro2fNS/+TwvfF7dJkPqy2XXhiFa7np2uco4ZbYJgMV4TYdhTO0IfADMt
l4Y+1yhtRwd4R3upyQBaCy5E9xF3NurkOFtnwDtYglitaC2CpU+/cuBJhIxfvEcA913ftqzqiLMw
fGRfpqytGMocItHZqlzCKQUWKKyXygzFZz/lmH/38Zr4XoRPfnkSban1DQ687b/VlkR1mit8H+Zq
YopalpsAWmStgl5nRCgGfdnFNrcYCgZlOqUikSqaFjks0GdelkqQ76vAh+PRbwU7lS19+9BTEL97
hxtOx1EtXUB58eHMcK8gwTsM4yegts98MMMuVxAZXIiXi3WByigBz2+lzBNNywn6mjeccxfAlVUw
N/JsUAbdQMmCAvE1PLrh4N7jiRJlYEYgr+Hg7RivCKEwop1s2SlibUO1tJbgtL21DtQKKy1lFok7
0URfXm1eeLPbYY+5T5CEJociXTwWtVBAfzPUgCJ149Mu7vYH+TIkW2QDP8UtwDdwwocmeZp/xy12
EjXIRPFkUXUejlguYoChQmQMNK2d/0DyjXgNzHyPRcUUJ/p7dYfxr7uBiXpkP8QXcuS+ElhnIGYm
DWgOUIpBTeb3j0O1PPdif2R9AkGcr/VHPPxyQzZfbDetTR1Cz5yjE29Ekf+D95IP+znV/6Yx9ZOH
qsZ6QETdVh7kTPYPPRstn4ACtjI53z/Xyb4tjLjL5Q9qUv9sELu5qbd85+howL6m5YI1SC4nFXh6
N/95T9e8zecinRoF2UQkkAqb5lITOHWuJQ32RYwgplrFGnXvrJft3VfxyPa1wZSpnjPHh4dQd34w
XWfqg2qE9Vq7/wvigswtgMB23hxwdqdyjQF/CXhwhwktKD9hox6ZO/Qpv7v7k0OrPnBstqH/X914
3RLQz0PKJgBgYh4MeN4x6FJSQRKgbIztk9dK44sOFfCfS/v6KhW1S8tRY8CVFGNP63di6+oOraHV
2nIISfGtAT+Y20pzgVu7SvdljGhczG5xCa9+qgZPiO9WgzApEzA5Clv3szm3ClSA5FE2tUPnrSC1
ZpryOvEe6SA24vkQ0cjBkB1p9DZaoA18bOw9iuC/8bWB97ti+ezPvrST6a5MMqxDULAXe5NMaHl0
qZ4suWjF6B0xRUoi3jOKkUNpjkNud3oyegsVEPmeRngeodBifXSQeDQZyObYdEKb8TBshTNTV8gT
pimkRRfonjDPnKygZ/3J+vJ0b5lDtWqTczHxlCVTwZPi1Snfx+J58xaVhxmPz3UeQ1gbc5MJcB8N
7kw9ZWMfPSX94hv03l2cw28/IGjfUAp97HlTiRueZgT8M52Rfz/AKorPKBQWBwSNrGba4z+mzORt
EsRk2zipsa4X1vP4sW/ybeNTm7Hxti1Q3xlORUUjXNls4Ly+zwgAlZ4PjTRGCI9MVD/2UsBKbT3m
kmuFKkqBvko6y3gqSSFBNZZytfZTe7DNo6zGbdZANiEza7eicvtp+3VZvPNtWbFRvFjqUU4ZJvzD
097Ty92zDNztsZxnDz3DdYEOAio5iQuVAthwduC6YzjxXBFzCW9e5EBFrVg6wFwLB64NORgV226M
9xIRnylmL5KYJEzj3v1DvRBoq6EW9q7U5lcEwN4289L1MyUJPh7aGveAR0S7145wBtm8btYaS91r
lz21NJVWnRcjIK2PD+8nq4MWKaQeo09aegknUDtNsFsua1rVqWaqwhwDFsiTtnTz9ZnkBbEFUSpH
n2JbvYug8LA9F/cMQIEAMQI8YTPANDT7IfnIVJKGHvgKwX+P2hyfFMeHIEzMGSLrGlXFo3LqZavi
+sHue0nCPYeQer1SsyZO8xWMZ1b/QJGRX8afAFYI54qm4caJ3el/EgAVXkZdK/El9eijDcIljLTd
26UlnGrz/3++owt8dFsEUB6JvsnlxBxR0tv2qmqojVplsn8VJctMMUrx7VjDGt0FEu5q3o+Xu0yw
ZEPbfL9lq684YiNLcAXLO/bFJp2A14qtnREeSUVRIWi+mawxCGYSfo/g0ymCaElr2faGLg8pZ6f3
LMYoi1AwNEb1E6+2UMQ6FRU0Hz2VAjFUxASekIrzlaouBRVeBKjlp+CKr12ZwBSmrzObVbejbxoB
3my59YkZS0DkTUjGbAajEQsMpGwN3NEFFCeZ78O3vGNpLy05kr+y1h/DaeFQxg8BWyPYIBMl7Ldn
YEhHkGNWQTNu+6xy25df+kdS31wcOIaqWkgzog7HA/+6C453VyG1RVdnmRm3+ojVjF9/EOqLZBkX
CfiqI2Q28bCY+inhSMh779rxmQ/ZcIAfeO3i6riKQgUr5WWGrzuU5Rglvx6WjVxkymYus+o1VL75
GZbiuKagXMDH00F0OlPqUGx48BIcnUgWFq42J3nU+rVbuM+TdTSKaufrIUzfYfaaHT6CMKOMpKj/
I/Tkit26elsLCZuo8N0D6Dhy8Az9PiaoquHp2R/ybbACPdLsB90vY8rd/a6uPTgIqVgwjekdQh22
sWzlqQTNmEVS2mTk251i7D8tYeuNcT150zI9DhH9IcsEJYMYZD93/vU6qe2+2VyCdZBkAa8jFfly
6Q4kQTyg2Y4cMbswlkHarthCYDLbvN5gPYmeCNFZzemI2vc3TcuPFwIkfBpEPOngxPo8Dq0rgfpj
N5Nt+mEqDHpZthlomAV6fxbGzDXXU4qmh8Hid3g2UUUh+JNmgFpjR5ZPrdwcJX+STi0sN7h/Ph5g
qHG9LqrdXZgAbhHT1JRmGdqGXaVg/Pv/toMZ1CJs60WWanYKWosQJEycVSrznz9cAk31jkwlzyxe
7dPhdYmsOmlVmNut28fp2VeArg5EhLCdARsH3fzZO1W4BmNRNfr+/c0nga09Utuv+VRZSr2so7dx
EXmmqWlUcaVyufrERLLtMCyvfOPglN/aCPJYiEHpj+I8ziJlyAtif8cCgZCa+qaRfX4hMcsD/8fp
l39fMBOLicGUDvbFII1NY1Bu+HdxD07sNEBDkbFnfGEdpayyvXNk9X6OnjM0z21yiMXa1sEE6vqZ
2vpeD4XjSBMXl/VSRwU+GQRguPFpKfqQ1w2Xy4C3pERO5B//GQOD8al7gnrWova2ik/8UB+LLyi9
RuB5raUkkD3gcKnLJC26PCRg7a0caaw2U+4XhPquNejaDtCXrKr4V48O8SqelYS/kkaHF2HUm8Oz
gAiD2ZqsFCCD0JQngoE2s87mZqEC+k8KgNEFf/c8nUM6m7ZZzJqwGOmM0FXaBzc8U1ynNczTKZKb
rRsdC8E6Qf0Q+PLBBQkVL/3g8vfj18Pd7jaLMdqKFn0zeCxl6YWDoLDJ2LEB5AHBNCJ69rynP+AP
Xou9iARoDAZSn5j5QaFv5ziSR5ha3jT3Wg1gUpzFj6dDdngIy80ygOQ1YjV0Jrhoj8xp3HuGrkhz
A0IUJ+HAv1m56GoTTEbmdZwTzR3LCY3WTDWgrewLCp9De8HCjt7FI+UCgyWxiqCNOAuiT7LkdmQk
XLwBBtAeo4+a0uBrtzSBYytvlFTTzgI5plmbnA4QcwvoOWOMU3qBStKVdFR78a7csrpY4JMlJp3I
GO0kX/T/77t1iKhKs+xSDqoFMVsDTmsENRDOyYDobaKR9IGsENqz1T8tznf82JraDIR9c/HU0xlc
KECqNkcaX0wscelki6J9o3G5oAcn9OnApAqSkX6jxvIzj/Xx/mK3qxFGjAuGtqpWF7nHIrDp9uzb
Az2iT1GoSnYX3BLlhqLyz+51VpDZKUUvo681C1tH4mgfQr7s6vXp/ZObP5YZuau1Sag+OWF1JmG3
6Bx577d7NEmePmze0Nk1hXAvYwzY6izfR4BR9McuGaACcyBpllRijYzujpxv/AVdHe6swfxNV3eI
Wayy1mwLgS0xDA+eH9/3lsj1Izd6nwMpvIao/s9jp4C1lrD/CWEajIXCgK7npKVhFgsehrFCt52i
3R6hgDreGmtc9L28sTsRR9cW/ARqIP1WdX5t1iBinx255guM/zDHu317zhzfJYU6Ut1gJmjT4nlm
jL3hSwpED5ul1nM+th0i3Xt6HH9jHRw8Z6EGVJU6btL6Nb9acGt9VcZds99gzxNVQFNXvEkQabfi
NJtTK9ZBaqzJq43tBjY/sRGrQSFIWBbER6Lez9i/IEYEo+PpJRSCJNGQBhvgzS8Nc4xqowWMd9C6
BHvgJ5wrG39aIpyAojACsGlXzttn1ziIdZqV/7ytWVd/u3PdXtUqaJ9hJy9tK9D7PgxPRH3wd5Yr
52d5mWKORDSZdAe1YRgoEnGZf8qWDZ0CpOPGb/lNhbUaik23GoiKI7XI6lxUtnj5P2XyMHVCgO8Q
KUd7SJ0TG7DlgrBds83JnNZXBIcb4Eb42aqcvgmBijgVIWkGi6teeN74s5bc/wyn2GDslghKV0Q3
F2bWQNf1tJ7qmR4PDXGiMS/FM19SFFP0UdUKG6AYvSw4ABZ2Y9XncPRTqnSyoXA3EjQzFLFWSegJ
xLsV4KlV8Y7GhFf2LS4VNvZQdS6cKglGKSXliu6Lb3QuL0parbLLMVHg8dJ75zZyqJtdQV4B8+89
EzNbaMDK3i0vu8nCFGJf/7SizSkiAlEWTkyMLRb89Glih5UZb1eogeHF2iqp/mdmf/gDS0SDDCc2
apOGW9vEirZEAFPELQbwBItF3H9SO8THkYhJwLuJWF4VFeTgZgnuazhYWP5zCA+mv00ni0wrCpJX
OKsevVkpl8I7JThUy2chKuA4Yht4IobAP9qlDOZ+QqNJQGV/6AVl+QKoUJcA1sW0l0T9sG9YpnZ6
OKPQZHjsiHwxmOmbx4sqYf8a7AHjKidgoBp2MnFIhrEP413QwhpUbt9sNMCrQD1TeOrsOtTlIKqf
zB//u1VaSzZpF8Uz9b6Lc9gsa4cpe5+EF67JA1646htkpJCNeINpRFcEa9fhnGWh+vGV7qyVQDRh
cIT+eemPVnBGqxdkZpdKuOT1XU6vOGdkyeWuELhve/kwY4NiUq2+dvmuvOb3ZlYewv1b+ci2yZeP
JBgqzKH0cEpZxGk+1OdGMpQ/aO/A8f6g/WgT5TFBecekSFjbrusAnLjgaS9mAc83m8lMV6dUPYW2
8ttMFizbfpE5krflJ9yTBAjbst4s/O74rfyE3Raya2MMwfFmGTkRUCnCEotLis7YIdFc+q/FwZtO
r9I/lfz+2hDb7MWAsLPDIGja0Oi1Iz2MW6VYULyVu7JCFZk7wPoqUilUkQhceNSDuyM6x2grpXpF
rlGaR/HxYhUHNqrQs9MOj3bVNcNmHzGm49K8xvpui9oVcHhgPaJi3/KQ1/nz6BEw0VLrtRozvj6y
FICel57Z9wXKjTjOtD4RewKv1m5WmnxK1Kz9kz7bwq/abXqPP8u1jXsR3ndG2VTcWkDbuBbQPDD9
l+zAIrgLZjsv88ygYUtLa66CbAazHWygqsJooKZKaRJHWt/tU3zZVcH9DmH07bb8xTA+gryIVL6W
jyZI/tR2z3t1zaBjpiw1ilWKvpkrSZQ6wfdcKWi/pwd7HcrYhXao/woamKT58FAHp+yxmL92JZr9
a+jVuwU5lDeVM4ePxEOnhHxIWVh+/oToi8tRKHulRWorfqTlhrprknTN5vGK2Wf5Nt29kTzGG5UY
W32RJpSkHKJxjGlikkaE6gBeYsHUBYjYhq1qRYg7aA+a/mD3dlY7gK8lZPY+jzMliyCrUWwzutRb
YfsjUbF4S1KTOlScbWgerSVI7/yA0PECyKty+IxqpTKpsIK1kGtU5XuWqtzehI46hCzBX6jmi2L8
xEy1AIB6PCG2+77UVEaHRMAdeLY9HVGJ5BjyMfWBcuxYmhtxDFfcMJ8V8gRzQ0XgZ/upjAZr2zQ4
qri4QsAfoMBa6hWUJ21v9JQtR9Pxw2ajTKQK6jj+yWTcXHgLbk+xGKcNin+dd39jRWTxDtG8tpqE
IZrcewiVfgfC/uWNqzhu5AreQnEGKBZunBXnya/lpurN+xcJ3Rf4hdt7/jqXRpudkuTu7s5ZIAPE
8TCbTjm8FYlH9g+7KabRBcxYjye/OJv6CYMgdkoxPqpoelQtezXdSeAuYNWE6ZaDPovCeHOLqAQZ
qEsA3Hftjq7R9/70mx5RpnL7fKCEVFTih7y3hTx0eCo948tbYYRptP8X/VJOVb8gPtTX0B8TpxMg
AqoihMvhLbA+WdA95dphGdOmSzesqC9mrHV8YkML/uRXEgo/mOx0gGUGBsm7ZITLe6dumRdlI23H
E8hrCIiFKF7cBMLxGjJM0KKDtYPWU3a9EKmHLTx43nIhB1uklM9TXlhBKt9e+YEK8dhM5Ix+U/YX
GSWe1P7MyQZF9ewX5NwTnhrKbsNzZUEtkK1EwrJPiO7TFloz6mR0dwNxelkJJ7XgD6DZnvZdNZJw
jlqN0CE0kv7f9NxShqGXjHASUfqUWXYDqDpmCXdwmq+CKHCrbmaVhvn/tbq5hW7yTSiCD6T/KnfP
90u7Yw7yY738y0RFmde929v8ZdVJ+mjDhOX5Gp2qH+k/WVZkwvWc16Z2axhzUvak36aix/4d5/Dw
7CPLfbLH2Ea7yVFT6eFHz8NfZ7pLDaDd06cFtkPtTW8ilH5GM6a4SHJTVZ7cTRKhy+J3ANKchKqe
deR4l2RPBfBPDMy6Rx41MG9893eDU/fRpX7AaKBxNlr15Y68H8m/XEos/SLonKdzC3ebAg8gY/s9
B5DLciwf6GrOL4aCMY81YAk8vyUwhzXosONiZx3jLtshcwThac/XPMtZRWjKj146w3G1PEVzwLhu
6yZCntwlE2va9Uce5vssv+6OsdIanBWlopSODuyIyx0oIIEhwFxRZE9R3SMrKdtlXjAztrJajpk2
kuqY+xiQODqqzX8aGE/FLAwirQHgYWiGBMvKOY57RrYUUcez8D7jdk5mUxEtfpYPTkAhw/dwHfTF
ceoPx+FAymMkbUvIbX47b/MMMO/mG2p563s1IN4CAyW2BRXy6hKfCUMeoIEeNa0qF+PknkiZtdhz
Muw5F/5uPromze4HXGqaPzjpM0JCjYJdMTHynpRm2xh/JusFcWUfdQxw3KHezlYUii0/VJSnCB7Z
B/nLxBa5iT4P/Yjl2PMynveIWKsLTOpR9p7wDML1uu9rLeYuvCoF6+fmw+JlM9RCPIEi/bkQuq5g
irXMSf3Gyfh5PUotdItKxn4YrdNKloZiPnhVeMYMi5Zv88PsEQ4sCy1mV8FZjD3jjf1UXd3hktsE
uCszSXxrrt5rvV414TWqFYpsYOD1XHFix6BlY7VxlmLivTtrpZ4fevVOf8591QuOGzFK3Bihyr5T
Hc/q0l+kbTcTRUkoiJLSZih2pUpX5j0Ul2i05bs3B958Mre55kR+CY10KIlUVBLvFRizz5wvWkx3
05C/RQnJ50OM8ETyAmEOiqNq5Wn6cxnumpIxMvcag94dT7kMUK1zvdu9O9N6Gqtp9TWFO/JOgRUj
ahNt+DI6ISNdBDIohPRvt9urVzPZ6eaAtYXWljA0OVX9apjY0tNgkcmZaAImpSBjJQmOIJBnF/K5
bq9haN/fJ3zyFOhyy74vbgSMwqeRK7+cW8riwDbiChI69diBk4JeImaQSHdtfa4y3jNCsxdlWh0/
gO4dEjEpowWKavh1McRV6eDnuXOZuVeREL6KXXB62AvHetyYDcu6Dh5GpHcXBpc/eVozY3yW8Oys
7QtE/ZqejQnGEfhjq87D6puAQvwT6T9sauNnOYz8Uozd55KHylZ+ZeA/sZo6SQJfABWPcecAoi9J
wZ0PdIxxunM2W0rDiZYWLCHcyojUTdMlwySTeNlEyJEU995kmldKz4AavQD78H9kCcUp8QkeiMQ+
POn9l7KmnO/olulsZUw/w0mW4XYZPx70DeB8aACP9gNSXx0dlaOirw3bkH78g0+iL0o/Ufo7z68X
rN2cE4zLTO9aaHjwzGeDhrWQNcBl9K+gPSU6NZ4GMVbMOkLSDr+a5Ot/+IGilkaaKmF2gZDR2uyG
6ggz+haCD4ANwjgaWdvnaPObBlXBTP2qLe8GhXar7zX5Skq2x9uZNkeBe8FIsgzPO7O4ZMbtDUc9
N5W5zg360D0mx6bX5zGfK3D4Y7P/KBWUdkm3HzkSvaGrWPPuNgANCfYHD+ZHVU18Wvf3bcTKfqOd
vMOnJIfYBty4gkpQNUTvPPDceZ7YVkeJPqhljNsUdt/LY23FjNafylkgGfoH5NvuDEGD0opYGkcq
yjZmlJ8aUTJM8LQZMKOHM8iWwU7kLhJWuitAi0H29poQvJW5BYzDiPezgRr3SQlbE6LhjwvXSTQE
L7NSDU80kJaTvUgD8YxfGNHZbHocmHs0gDYePd7RlX3aiMZn3JrVTlS5073lhwkjBFVCQT5Fr4N1
olXpWElgPffJjxGMumUfR6un7P1crhgxM7LRi1Of8I//3T351Fz37B2BC7CWcepUWSiPZRIRZlay
Cc0mGETJEskO6FG26MpCwb1A+u6NuYRdXPx6LrhN2OSCGfqkhwwn67pFEpTvw0OfhHVjIeYjeu8j
FkB6zXykQrOKAbb8IJkxUwfcpZVFB2touS9HtK2JFBZqQbvW99VVwTDcVlNuKImzK5MxQLwxXRvx
82zaD+WfroQKQiCD4ZHdltGNxbPf3u5afM8ipNzjGnUyQ7EmzulG9wyeX3UZOawu7uvQxUejFE3k
A1stAeLSu0bR/Jsv8YY6t2xkC4P7dAvA8RSFkFJ6i5ij761u3PqkRQdg2ocjLCntzlS53IxBB44W
Z2BxLjEHXiPAFXQa2QNWn5ra+GJ+m330Qh6b8PX30yFlHZpOymJs5gN8zzh5RMs9pgnYfI1xGGz/
4fRZtAviqVaXO8+qK6Mo9EiXDcIkSfKIqFCRm9E6rtAWU8Ed6U8suRhYtIM0s4OXiD95exxzyYh2
CtrnngCzfp14mUbQ1mGa5iRb7V1KAPLmzyyFTKJpIWlOa+ORD+292F25iPTWZMf7nM2kB6p4xYM7
lMoCQBpvdF0i7Z11CgWzIDlZ3zykVBo+nKbgTh2CZ6bzS08cSpmnJy9G/YKLJqpm8XNfKUbgfcAi
L5wZ8iEjq+Y6J9Kv9JacobacviXdXxviPsFp3Vm8ivhcqMyOYd946FPUrhvyEAP/CesVBqG1JUfc
7jknQylwkiKNkH/qtF5Tq3wLasENSAmrhcYIv6IrcFw6lY9I8W/mjNEADOrHLdb4aC7k666hV2O5
dWzp3Wu9WF3sOFNpDJOhyUzfg9lUFIVcWzyTrjJGbuw7IMz8WcGaUs0eMk16cYgCyKPih+xZC7iA
jg8ixkIew1LxZlVkv5GzwtQ32E4XewbFYp5DLAXzMcBpLYM1lGJo7aZKaJwXJmIFCA8L2W5jASyv
rUf4o/GutqzGi6YnDu5utNMr3fqS8AaoJumI56LN89gn4l33Oavz8lgaGiD8iVeBakOdOKnVHc85
dQnnh/hkT0Z+ccKeRzCu1zuGSwg6ql+B5h9x40tNB0jb4Haed+xlWgnDJsAM8031qygopWZcXKLl
uVZ2QNCzmiR9hrH3lTQ0CA/OcLaobB+R5XGhXbocC5+F4cE4SMAsvwAC16yJVBYHSXcGWCbGLVjy
d079ZiyDqKU5HKAEsnieHYzfZkVeqs77IUKttRHAgVupAZEdnVf3oFjGmGgqiLlr9Dc035WgIt/3
UvCP6MTS7NMJE1QNWeqGke5hxV1K9XOBx4ApneHCLvdJGMB1DzsP1Wje7KDWhEa9VKzHzS5QzlOg
AxuJHiCBcIReaDJHG9D1s/edDsBQoPXYqqyrKUtW+wG5g1uMCtBxp/EBrJljzzXaR5Tpr8ynSR4v
+Oo9LXggRdgzPlN+iRmTeM+2EihUklYStIuQLkPWQyNKCvQz0Mx2u6YM3UB6xgBeRP6UfW6F/K4V
y6jUkOfEdv5F9qAkKmMROI74yRsNpJ6ycCPa7uu/fpaHYkFEZTPgGZA9KMOEeXRRInBv+1wgVw9K
GbnQOzyQlTm3sp8NazuW6Vww5MJVqpiRvvunN2+hN6eRQ3FpSakg0yHd/4yKJDTdGTmzHj0F01O3
2XwWxam2tRiKd+hVjUhpPjTN142c2cOHnuOdq5r8KOoQ5YaMExAInWZtQPxs2QppAP0FNQBgE7i7
s0sfLk6TX3m5hVjKky4ec+0FWIoHjm0J3K4N1pA2X0aCxSKVcOnCs1PA5HOdxmFL/gnqKRE50MWy
e+UmxLl8wQzzZrswkKx5OfwYP28p6ovtCig2FeJw2GQ+pXcbidkMZmmx6OHYgiwCT9rYAotE4nMh
OYA6kHBwoh5OzNTxKnRKtN+YZk8pMIoOMl5R9xmqIQWn0tpb1o4h6MIG6CS3p48F0rKNggNP51uw
ryCW3td6C6GhP/D3A8RD57McmEM6jvF8LqSp6rEPvd+qB7OKlcdF/581ctQWgXGMtPcy8Wymdm7f
9OJdf2g9f90RxNm5NN7lh8k1pFhkQDOai9ftfseN2LWoDry3CbCarSsVyz/1iVz6ckWPJ7O+5Pup
JAQCe3wK1ZsSLaTfnOjj+UPulz9/MwUXieIEPuo2ym+l867j7hxsqpwIV44tJjld41ozLvjDuUZs
zh5M6yR2eAHNk3NOX+IaXGTLMw9JGXoLFTFW8gEQLXw4gOFtuluH65+8DC7hAhnXDfoBHsFmHmAk
ysbxovsNkGt9JyuPEskOPNngw33oqyQMqwS1pujp2Ug99miD0+jNKdJMQ0dV7dHO33cs9RpEUOJI
Mx8pyUyj9V1xskWptOg7iW2fQrEjdjtegixz426JvvTpeLzjl2ao4hB+nXckhBO1dnidbthSwIWQ
7NcNjMMPLE3kQ8+R32tU6hJnS0JxAHDye3cGp5T3o4rY0hbhaAaeoKXKrGtZO25HX2pGdqiXAukQ
vhqD3xwXbm6YwLb6JrpGdVmkz64Z+bvqrXoNl/owo57ZmQj/sdYuWMZ6DxD9wQzgBrHj7gYI1DlL
Y4pXtywba9bee7XmbTsthNk9uyGtnvWKKMz4ImNIClcg1y7hQ9No/NZbmufXwDDmuNZK30XcqOs+
2C08/LAa3ezKcVmlG+JVaWUw9qJ6HkJHxa0ZMZ2iJo0h4BHeCOMgmljk08PGmjB95eCqqkSj08iK
wKA+W/R0jOIIGP9fW8DIahrWvsB2CZ19QSzKOlO3dGBiBTIzG1PwSqm9sMjQoYxgloWmGgYW/F3p
RZwADN1dQtCOTcmSV3jb+LBVvKIN6JEyMi21ykGlRSDt6CfpApxwG7EKgJkKc9936T1xYRkfZC3C
7w7tJKT5RKMJgPF5TEgEtx+ELlJq7UDOvP7rDr6Bcmc9ujvtSH4WgrOvfYXJPoS2vF3nsVyiD8uX
5rFGoLJnPCpL4+scgqRF1j8X7ohksOgtyLowmSGEp0tbr1uTtCsv6SK5C7TRnXJascD0yjRuKjUN
hgoXAcXgtldy26QxnMXPJt6m39BZUn3Q+oJ22Hp3yEH4qBRG5VDDXo1nl6b9p+OVb+pCc1qlRufZ
40dIa+S36w5RaEKy6x3VpKSGMxJoWvKdJskPmlGtr6811HX346dcmwHRhxsmw7KogV8U0/WKtiER
gDyXPlLuIi3R9GOUZg1RbxZk+WjOEun5Uk8m+YLc6C3O8RPqKb2jic+L2KcyXadjmjwYPM7N4e33
4P1SI+pEEZ7bNVZIWUjm7ePhvV2y7USSRHKYVZ92MIgYqNRKno/HJnUodjUSAMcnptKwSiGcfQHu
U8Ersb7eTQe+qCdFYGvENWnWEsanutH722M0ojYnnxCDTEPZ/3gR/LbV3sIgUBACJMEvv4iPPXLn
sjKoijCXkaTpTMiWsNFJCLnwDqAUDBasm+/Ya2bDSSjTKPPGiiTwCrHvw2b8NS5bhtrmqrVGty4J
dvnXlJ1QuugfWcc+aHxDtIg9XNnafPJvGtr9U8XX91dI86Dm40vNwmWlVH39f5nFfR0EJTz9OzJ9
s9xahvWeuckALBDWRR+CgfeZS6uNOXIXo1bV1cUaq5AjJEVtTsbol1qmdgWoI4Iq7+YTIYMxBBaq
TJ+xCj21Z2iysgHzeMCxskpbarWHp/bgf1grFrW3qOigU5pnjx1x07ut1abkQBIa6tlzN7aKVvOC
rFyxDG45wRHtlrn0o/wTmraVovI3eMreKMbANy/sryAxVD1MK80h602GEbwoV/PitIpZc/3JeLk6
opSiTSpyCUbdnXE3Dh/XkfFdpWAKYcpaZCMcgksEyaBvCw2jDix5UrzGyELoXZ0XH7C2oZVH3oGd
gbR3KHBoGokZxID7fNf9zqIEf9e3FHHSSuLF4hRQwOAG9cMMemrCt67GEL7hs73/ADX5t3+QzOHS
uzC2vP3CnWsab7C6GTP6O2d6UP7+cu0aKOA8PljZdtjtt+eSLvJacbdlxYbnz/ll+t+akyZ+WJcW
GIPqvr6TiLjW5E95tD2DVOZAbugSXZ0A1Vcn/O2QOwjWkS6skSu2OsTZ1xLgWR0bZeccHY1d5G5N
KrVwAWeM5W0NjESi5ftdZRtqu9Fg1kdJV3fPkZr8TuiFK6ZHhe1xPPgXu5ufjDjlVAMdpCVglHuZ
8pfmekATYChqAnXcm+p2mO/B8sNJh8Gm6U5/HMPCdx01LtP1cF+0x7N4Op0gBVkscS9ZnITzHYqo
7BO0bPFxxxQEc4OaiOgYK0913ZP50bE/ump8M7ZAL7yLBXwkweDMIXDUmqlIZR+5X+D7GQrvXE2v
EC/lz6cZZOJHsDRLn7FkAXbtB9FB7xdVVAj+uJSpQ4j3pMm48Hb0/jiBGoYj+9PZq0ibX08lxz22
e3sR4qc/g9dMSOJuZhw6G6A663zvnZyuWYXcFGgffQxUWn/sV8CukO7D+6DufVIiip6vBAlfh3Ct
9gF8Z4veC++zvyW9TvcM4jAp2SeHyO7Fo/kX/T1aac7KrBymJNOrEOQqA3zTlf75pUIO7Br3DqpV
vgduBkp25of48aneV1ccSH+e1Cnd+iBawUOzQrmrYCNs2ReJmwmgTUPTuFI7u1qXEJsL5ZEZqiW4
cSyWSgHiUFUdQj3q8IwBcK+VQdrUvZU6gE613ZUqJdx+bdESYOS/zWfWZX2zVuwYMNWLNnmBgZEe
7piiHpvCgFhZPORne72GbkWpxMeiPjUsOBiNakc03YhuipWoMZGpFzJ8HAOwpHNnf2l8OTQ4LWG0
V4UjwRlkh6ytk+xiLZFt7vZKUptNnVsjpSz1W3E7GZYq4q3kuLK7YgXdvgzJgNtgEWQOx8OVxwTT
UhVS61dunC/bZ4s2IJWpIu8oC/Zm35G7E5YbdTSzBSumpAqML12AGApSgvq8yNY31QAA0oFULe1y
b1FctL3iN3nR8b+Vl1oJ0Y149pD0cFAEMTLxJmKyM6S9j3jN1bElXYRpnLtuWVr9tml6qE1IGxlS
t0VcyA3vjN53JWJ1OSDzEYPCukvu+1a+3nsmd6cmHd25S2t7PfKmy9KG7I2hA/T60l6ewaACiYiv
HnjcqoVhQAwEUqbUrl5kKg1bhg60oAOuL382VjpNxk3k0NKxCaKQX3xXkFGqyo0m3JQaBTSggU9A
TLtZYICIATac4fTob1vf9WsiaYb6/lfCPnrHrPa5ni1cZzou47C1PTZoVJhBTVt/8njIgdgQa6gc
33gjgl8jYwMw/TW8kcZefF7pfrwervR23efNwLTn+eINVU/4zHGy6LSFqLC+33lQir0jpn1wIG7v
+0JTv9OA9ooqCqItSmokrb0i5SRjmFF4RIuT4c78wJU4Snh2Zgs7oKENBBO5M9Twm2HqeTRVHz2w
QIHjL5Mfp3favZ9SsuBL4xKF5gegFg9BDZfH3Igl8LHOE8LjfMANKpAP3oBKxEfGBEobYgutah9I
WOKAs1uN8JbkHWZDbfmKNzf9fG5ezeA5b6fruDpT/IM3ODlGSi8ZvrRyIiseKscuvliJjyqsYrLm
MZcqqt41jXzKCalwsFO+BJNwkFGDoaEUuw7T8YqkoHbtNUV+aEZUbVnFk20Dr6dpIlu0FbnLs3s2
uP8WfQSnqe33Hh09qiEBI7dSN3fhi3X6erMzXDP0ZGpuViHmKJ7YMZu33IDR42tXR/+qn7+RcS9h
JSJgyUut3FpMAgEW4wkOSFVfpDUFdIpIcrASwI3kqONTWAPlE56b3vtr4H8OIAtCVK7FkWYPcTTw
F1XYpWivbkwkhB17kOODRSvHq0yaCUYrXRYbtyC9tIF/EA0hbG8hpCY4+JEgZjJAkSM3y4upkVqh
V8xV1KIAp8l8NllIACYmbx/kPVPzLqPC7g1hoUOPtKqy0L46N/itbJ4IiIhyGPZ5nJounnZ9sk63
wHYsx6YdCErLEMIqEzwK6uAmwWJKpLVBUWQDRKESpgjWpK2orbREmOmSgx4kkmaJOsTdYjPvi7r/
wgGlBwYtoCJFQUOS/HCmFVx3akEvPJFTOyh+RjzQrvoX9+JXtyfrvxnvDGX+K0hG8g5MsSKIQTH5
9kWvnICw4KrxTdsWv4n8BvUywoXCmcHn4Pgg1NC1qRH/5w/iK38u1bck0PmMkK+1NZTwFuN47q0V
/RZivrcDQgP0aRKs1foVLVeZQNrAsrV1yjGzRC+XLLe9BcdW6sMTKpvJXULIaR0YPcTJfYleIiAm
uzKQsWcSyJMG8mZ4ebaWv1viHmFNfPmQUQJc673GjNE9Tjif7S8dtPe4b6hnqb/VbOrwCVsmTWyt
7907cMT0rTpUNda8TorjkFUbDnWnS5qXRpMuotsvFD5c5z10CEhmYtC8x/Y1kctKks4WZgketFwX
lvULkq7KdYdvdj9o1TTgM/E41rsHE44dOX4HqU8x3wxx4GUSOhnubLLEXyERcElTZif+f+rXULs9
iaPXYAKmTD015kSgAxW+uwWSIKfYw6G9KblWJkXZfEKBbmTb40ICXt2kuALQeM60HShKLlSFa/TQ
KYja0kjHKpIcMKL+7BzlZPuaF0aBbP8krPK4Z+tiN5n/MAUTn0hVmU8w3vc/074HHa13MeyOmnvf
lS+ctkKCDlof8MNAGODplW8RIp3AOiM2VDibYZq3hr4ZVQ7WqnwF9EmxZ9r/NldOU0Od+0ddm1oO
rIZpUGMoBU36VaN4AS2Bjxw8tgHNI2lmvuCWoga4e+iT1BD+l3tSIu805tjyJOGJjgLiktl8p63G
aMRcGoQrwibgJnc7iwxjziFfI2k/iuQ25VdC4LRFaeU6LW4jRvTU+Wdm/lA+2MXNqfsurqBJZkWT
MbyQvCqjb583FtBDKJGIaMR2jsF8chv2htPBalhAJ1X9hrtSrsZUJnhVL2mIQzDVUS9efJly1P8Z
ho+UUjZgDg56bak3Mi5j09mkDgqoLDQjJXU3IWFBCBk2MYwQnMENEt4jmeLq4ZEek0pfZuqxAFSp
6hHrZmrUgXiXIfaiJTof4mkdzFFSHg7L+yKsZin1uTTq2etRkhKh7jRiot5ocH/vyxwc4o7ypjKe
umWGpcFVXV6hxtNzWkHqkAtltzrH7CSJed6vneRwsj0CqyzvFULpqLCld0VbRJ/sleLY8A7jQR00
PJwQdhlamAq+FkijXALvnhqlF9QaEI5xqhNZT8RJCruIvpd0HQ2zPex6XRFe5IBdFYSDFV3W85w9
4kWekHIDdarxnkygeaDXdyP9qvN6D8AzWfmqaNGslp4jIGrZ7+PHd7UowAmsbMudHxFD9tfk7Jkh
yGMOn6MQlIQvJSmnSKpk5s8IN0hVZzY67bmxipLfAus9MkjCT+Q694PFAXMkN1iNG75O8eYSur4P
LfPhDBBLWm4AFe9ZinO+S/b4UaussUTvq2hEAxV7xmZOKlJzUsULSrb9vec5GDvEFli5sHnPyiED
8nL7TX9FLzkLjYLFGrxbgywgje48v0Z/df4LRxqTBwCqaAiWwn/hqCVseXVbH8vpA0lcr00lY6+2
87+/c9NLujBO0GsZi20HBRYvZea89Cbx5iE5OwdxxjFyRudkHvqswjBG/u28LREpxiVXVYSMH8Ij
9icE/FuHSyXDKy4xFqLdu0HTSqY/cYO95DfB6e03hSEQ8i9u+9Uf6FBZpO4KJwxAvTR/rhOoXG2T
+N2tGSz1OgYhIAHUCKDpHAWtpC/CaYzeM9oC7Uu/Ln7YQTme0ozxWIkpTdD4T5rrdcQfXuIiuPdh
qupaMCFjUw3Ph+shp/XXNtE7AfBr7ven9ixALDSBvjPUfMk9u5Oxe56vQ1EJVuF/UWi61SD+oIj9
X1OPIOhy8vk+oNDcdEGQE1TKzm7UE30DdBw3Jcoa9f0b9Qei/ZJTg6a7kgDPqi6eE5r1vINJIGqo
zD46QYTd6rfRtxrx9GMxFqvAtOU9cUUfN6+SH1szm/Yz691keDo7/+WXxFrSIjakXIPn/k6GrosR
hSoFC/URAFWyZnz+lcDFPqYXg8Ai9L5mN/DYtXbIvFjspuoO9rYiHkk8scqXpoHSScj1aERZIetu
xP01B6ObuAl4KDTXtg/OkO7k1QsiPlDsxHfXrOWF2EG83dIOP6aoYyxubK/nIKu9sfjLlFhQelxV
QTW0qg1S1b1cIbnWhjVM/5DQETMsgadJk5Ea7HWUMZrfdwy/wLMjpDZiZwcIis1KAjNKTullhmMV
PCoZJp7/61ieod65I6G7HfXjyyCuf64S80d5fCVEFR5pkxOJ0rXSOvfRe4qTUZLZtyuQz5/jyRDU
qIfB+tfsn3rz9eti0uZXbs5rY/gaWTw3VFYInm3cxRxk8odWIYhm+/48pQajsLANN6tcDv4jA+Bi
BdxSYZkNPl7Qyudkccw2Uai4bqgx4ZIzTGgKZQ9rIhZFISvQnJ8Co+hS0VfQvYoFaQsO+bCq4aCb
rpmcJiySy7EUie4cI0pM/oCyzPQd22Nh2lySACpf1vQzomaEevEsjyTOZeVdI/GC19H7tIZpfs0c
47xt939Wqr+caP+tBWUhx0ZhVucxV09STuvfBXK5/nqjnIEP7sTnFJsvLoTHW8OC31lMENZsdIl4
ZEe8oiwi9xnJfWP/J+1wHCUpCNBqemuRzHQWU5T5DoqwqsLUd1s9MNB8Kt5tHj2HweCdKJqbcZhn
X2dWuM18OPJHCaTw8OUZopD8zww3AvPKnES3fWCKMENYxuDnuOwIAuSt7F3GoJy3W1HoX5xYr/2u
1EGpE/L3o7L9sTDqr4nYmjCIfpCXfmPSPG2p+IKK2XkHsTXeMAjOtobXiS3qBeaSnv76PIQRUNBM
4bB6aXlUK1gyCJ9Hhf5UhY4jbGIhMC79YOjduSb3N8N8Kl25Z5MtNQGEIpPeo2UeCbz9LkX7/iv4
ee0KimnKwLIJB3z1dmPt5gvVKQVHCyjy9+i4m5f1RbQUW1tTgGPRCK/j9thMYurOruNBsR1lJDs3
Kb5hsvLAFe/nsVPW7A72HGlfGXhw7lXAzQtKLIetvqY/PWgGjm7elSDP2wezpKdNLbuQtACaWKa0
3TJFKPkbWJt4BxfO1fugAwdjDUVZ9c/yr0SBGXCdvzxPvy5ArY3XBt1AtfHgoph5MM01oAScku9C
IaOI7tw9CRmBwF90j+/syDKegDepY/APkBxxEesNebuucpyAUdJtWNCZB7QdSjJtYYPM1EqbklXH
o91Y/AbFEhoiiudUvHLyjR0pzXC+ARoWL6HvSm+DhmdBhNXR7FN9Atu6lA0EB/7GRQoB/feTrv5z
Arm4ypQHSXZLYi6gct3Rvv8Tki9xr3sq3acxLbx1EfFxyTOpH7Z+2TtD71V7OLk4hlPxC3pUhlzN
rajPwXazKui3iAVQoHMCK38NRt958wj0mGkmQDl+kaxDHOVll+31UZQjIpovJFpmXyTSoUJ9s9MC
lWw+94ZQnEhdxPj3i1OnH7pQa2+atXrx2d/5vJYgeiNy7PN/4TmbMDgKDw6ZdA5fiRYPAk39WaWd
8E/XWtmijvmrtPvQUkHJyEwSP2rgqNPVBQx5U4jdo8IvgRCAs9yGVjNZKyBoeUBHDBOVyWmr8AQJ
SJzdQJZf+bDyRIstTwqEBA/zWfOTAJNMd9Ilf31F5n3yvBEezlpPPHDNhsH49ikNwu1curfcLUW6
Yrq9b5rUqeH7yvOovicfzmBMDjjQYTZc/0+LbWdS3ij7Y6bLAGZwkyuMABKEOVimqHTDCLnv5I6e
DOM7+z7QLBKtoTQx59AfywWnAF//nppvqlK+eM1Aqp/8/DeS/eP9BKAR5JXi9L9ONk5yN3OVjdbL
UvbepZ5Enc0tXbv+rmj8UM5CcYPXq/vM6ggX6XRdNIyELU29OGkL9n1vTTSBL+Z/yu156kmOVgPG
qs0UFS8bYu9KvZs2cnBydNCk12ohwF+KSY36nHNFXBhs9EXh1xRMvjb5nbm6uhrqh1V36pxhkQEF
8yGcKO5WK6xXRhN2iJ3Qwla/d12YrRW7v0j4Yp3rilSaJfgAKBL3iz+w1fJwV5bfjKwPt/sxjBlc
NhzNfvhtRek6S3HRc5NTDl54jIC7Q8LP93L9HvkiI3rKxvdMhVcblKfDtVtlWo1k/fOghdsM823I
ESddLzCAaXFa3B/+pRFld2FJMzDalpEBLdA7e6tH9UYdKDp02nratf0IyEov19feOtpfJsZrNvl8
SFWTzgjC0TbMn8Ml2e0yGWS99Hr/cC3Wv1aXLMm/o7RMERGzlqBAN8chbbZCtVFm1x/h9gEBonA1
FY+YKwJqhSltZrJHDTu77OhgwIzUOb3ZAhoRlEXCeVNVsGj28qN9zbK3M4ceEjgYF3M4ChNOQdoI
cVO0J+Nxw6PddGXgnGiaNPkpplpxG4sc4TLEqOuCtyiG82za2nYID9isanePrFZxNrfl2e59YuCc
A1BzuigYZsRZcN6VsYDyYbg7KJBR/FFiVYKctGzwIiQqtv35kGPMu32lBknUV25W/MgHHRDJrYY9
l3RjAGacC4qzpCapXPjVkHUBjNR0tMO6TfeKR7wrtReWQg3ZLMRyD5wwG2T+OBgZXDSBDnVowj8h
POjfi+2rSF4kH63IMHwvfcpBdGMAfcpmhI8HwRaL3+oGq7TCvBYgT6xkGXhXyysX34T0k+xfCoce
d4mXXwnBQwGHcfSyXGt/SYSabXh5ToqoVWkuF3qjw9fm50Ms/oxk8wCeNPFoaphfMhIALAlDvmg5
KE+dgS+p0tno27jIpAdpBXQfynSKILib1RJCOjBgqzjQy26+WdFIoRDaYjkJtR8v6nGr3zgs/upa
q3jNJfVGPxBhWm0xdTOU3VTzcfOus5rhEXWlfuEG0UIlkYxgID2hmQMz1AJzkevbCrvDYk2sO0Gm
+/b2iWuXSscHQgcQ6BxjqP7Hc+L9UC9nLQljWBn2DrydEhJjcSD4XQyf0ImMWx9cf6T+aFxP9Qn4
8XPP0MnHRBL4VOYUbaj84x4jmj0rIywcfRA6E+ct26/+xX70G4ZsSevtC6zQwaQfP/hPI90Gth7y
H2dloOAtCtSO96A+kY5cNUXzD4Emv9HxDODugNSFuBNfoktnYKU+YdZnB4XEIBGaAma4sWNWWGnM
GC/fK9DYRtaIY+j5MFOBebKp3Mwgtg2xX/RGGH1aqFaUszzi64sciII8w/xDBbGp73/ji/HtqqNT
fLfSfYYKr6YPkkm9rul5KQvvykKQGJiNojC9sVndEOi3W4gRkQU6xEna7tJPK64ZX+PpmrSNB2v9
G9ZVoobe1FFrtVl/OdMODQMIqSOIjwE1Pj2MFS/zgp63UzFuo6n55PmG206dktZk1WRhS94LCcdu
ecCAh8g7KrakpkSZLur4azjszhNnnTAX0qwVDrZ/07mmrAbh6P0FNJDY68ue3wzruk8dRpWfr/+m
S6ayTT8UAPZhGAFihGgsdPsr9IHqd0A4ZcQMsJXSj1puiMEh9vBFP39MSbIWavfYrnmxBNrLRAXH
rdktjt58iwqBSH+LPXwbb5xHOw/ZKdVbqyCZ/TmapKYsXYUlrcTfzKTdVTVo+7xC1aFML3tFG2L1
Hl+smTVS+SKaUBGDPjwfkDrdgQS7f4V3iGCgSYt4f2ZWX+o4qjHdL5sUPkWrcE9hUDHFV9hDfU+d
zKSru6VU8pzaEeQQjt918eiRsdsEVShv9uS7yD++JYU6AeEbNhEOMZOOHAfNFb9kcEYqCs2tyTfM
NvUiY/lLQyeWsR4JH67YQuduwr90CmEPu1/itLZP3WEXAXsbdHjB2yDOf2AQSUKfmOXhshO8CN8/
44RZAkx4LQ6kjsXeLyNBORO7ngQKMO9I3VPsFWae34zbDxrakdlgY262F261yoQ8xgbDm3Jvoi4z
wCbNC21lXV3xCSomWM9t8mJYIkr+OpeOkoYfFTRRjBhNF37wV5SvsOPNELWuXv14TOYG1zsshmht
x1W3F4xmr84/P2WtgX9bePViYPTa23XUH79C5uacRfol6YuN3zesx69Q06izscXrIHPMRlxEtjI9
1yEKe8q63LgiL4jl6ag4wjAanhMbBbSVdUiwRPrq2Se+06NjUreyON3NdEGi5ZGZ7AxXIrfnskrN
0TyOoM9XWr9kyi5Zo22WSVTAcbyhU5MMDV+9zPihdnVQO73lNb/fJ/9M8gjOyRy6C/yOor87FIeM
uf8K+YGtZzGB8gTghWDc2Jm7SGI7es1eLh/sWnz3hu5DQWYRHBeilhZ36qklag5BeJ0sbJoEUqTp
/o5BYZRYz7CBXglDLmHr4g670muSui5m7jPEAWVgW+SOEdooJhTzilIj8Rg7GS3VOuCkLcgZPG84
b1M2XgjRkuVCvVVdaYIeXjcNTm0ggXcDP9CBaBuSqIp+zss+zprOyjrxNYidXgELy9t7O7Sp52c6
CCX8fSTeJBXLQPMHtAy+NuQqECBIx80wqByntI2Tl9vARGC1XhB7rTnvtIKKUD1nvA7cfL/uUU3w
zCk19KDqeqYWRDukKuqcn+OtIqQswe+us9EkELLhQ90SzwZwE9G0w7pOaeCeZrDfjGt525TBZYPw
f+PQMfsZug4AAcKwMGwOpHIFS4zJ/c2dMG+GjV5FFyADxfzwoyS1jwu4rDeRuIg41KUWUAS250GW
c6ejMs5eTPf6c357WVg7kENkRMdIMLdAvzcFiu840CKJDEkLo6YC2hc4uuJi+C+glRFuSrCiF+Oj
SwuPDmdK0x8QuaJV5ZimKdDPRNWExJKG6kgRCzQW8V6bYzSeeUOgJC3t8/ygEQ3K7N8tXEgF227d
jh/e1Gyys5/RAeEO2Q1SpiCmqEDcrB7hBO6ocbzpFTbCFiIbvmaOJ/guDxRl1mwgVNLDW4PZsq7u
IHEp+ziXtFVBgbi2EPKQ6GNWjeEnLqgRLRaVzPIYzyciORBPN1Hc1C2AqvFk7nG9AFqEGKhQwgeQ
ph5laJxVfXuDk55kRTGx6xuDNuItNHZmRd1kbhacSajJzt+Del07duJkoca29nJNXo4Xsp02vvtd
gZrV+qN0touxD6Xdgix8qWxQwHBmIe/UKc4xO8xm9CcS5F8xBX2wnfVKon6VfZp+Pw2x7EiDnRjf
ba0n0lxoXbsSh4746TLJDeiIEdMpxVOcILIaibseAGQxPIsOdwikeqD+si3kSgnRNuiYWYNFB1Xr
DMi9g0zyz150Y0zGmANdGy7irUqts50aEwBTdQeo7vGpDoUVQ4ezhQJW0CemZHXa5JvKKtwDW+zw
5UV3B9A+KJWE1V06zNXwwbHuXtol/4b224IED4bpDQCQJXWKKWVJWLFpr5qI98Ceu79PLtFPRtqr
RYVFgQa+lTEghAgb/cQPPc6tRgcafaAchgWnK9twlLJJNumzblZYYRrHG1WZkeFl+eyyKQrxnqbK
5eqtWOWTwuVCCeuQ3oen/3H9Rpu0j3/SnHj2+js2Wkvxp2kbRL6J5u50GWJEGCR0raVWG8Vzvk2M
ZpTVLV9lAdoUGIWcL4osc6viMvtHuOL/UmFWttfndfA4SFtDDhXC1BGjtf8UJ8uZrsE7O+o1UbqU
E8U8kldfFkEEfhnkZx2GoVCatGT0Z43nfzTqZzI0+fmg41Mq8pW0UMi/mCeVlvFiDHnxEsOxesUJ
4N1E5VQJDl3Gtk318e4kN5kIL0BqzTYsnb1hIhfWYk4oIkzL1BsM7NHHxngw5NShRHo0lIqxO0dm
/n1quDaxcp0ps2owoT73fVxRSf3DzcW/ji7MXsfsbP8HNXwOQw2GK0R4W7cEK7Onzz93m3hdR00y
plXbKtcpJrIr6J5+wrksuhy91ApnHvNJNT38gugM2xWr0GGvosfJdR4GzoAe3ruRUy6KODppesSA
J6M4d9GLq1dSNYTHxrhmIueFz25kSE0MXshIFVpVSOfI+C7SwLNra0urdvB5L+PdVwYqMfI2vd8j
vAe9AFuirIDltuejZTica5yYFG5vhGEVk2UEaWBpU8zEv4s0RgAiQR/c3HVeNyem//nLP+IA3kVc
FrJS5rW4/Z/hK7dcaJXiBgKBSeK7UuahNBCAb827icr7Pmt078vlHmCldqvo4yiWhjxhDg+046Cb
C0wdhJ53yOh0YauaSQLU84jG46vfjnxrCBz6xA28rH3GEwRfZb2reOcTJSa1kR+Q1nNGx5v1Zyqb
14I0Yk5lkNLdTcqdbKo1uCJ3Sgzwkam0erJER8D6fWNKJJFGVf1sLsWHsNPq01i4Ji4Q0TaHA2p/
/BoEZ9SRu2R0v8COx1guF1rLYeUSYA+9IGH0QDuktXf2durVJOD0eUoKTCFHBQJBkyWMK3/FrI9/
hVTKDCoIHefaAESKqezGoSIRgxxPbK+W/dEMiw3F/3F5lpC9NTbFMPy6DF2RjY7rMH5LxbkwtSki
QT7XF25TDKuWbx+r4sx6F64buJcsj+/fkogxSbsCg6u8xg7haFGHpAiAkv7goG+DDD0IywDQpgYr
z6b+0ZjMLiyFVXrK2kcmFgHGJIe3kn5q7fTtWepCUrKq+ciW6YaOW4eSckJ3op5SMy4eesupoI+6
BYD78YPbWYz/WsI1H7+odVy1SncI6OboN/WGXfjwyIUaEfcoeFTBSxePHW+lU7e6B/JnK4qCMTFe
nJrY+Rh7foLPIBUh5wYlYqN3T5K7SPaaXMGnLCGLPtXIfO0JbmK1IrPRrazCPL/JTbBAitSC/aXh
mWIn15OnLsbkYMZHycbgKL95zCA9swuXFybxEOcDm1EpiQylmzJ4kTCn9Z6niFyZaP72/7KJuvv9
f+/R2Hm/iWC8ULuH2Y0fX0/qUBiYkINYS+fy9bSQ0pVPktHq9Ectb5+UtG9JngH5S2iPs7VXO++Z
29eOunkLxcieqI+VEmhVbttBa9xBbC3k4ESQriwEX+hMlqHwqpLSEwDH5dn5V1FCN1tPAMGmlbQG
cTebX25jjbc/5TYiONGOxS3ldUyDtxL+5e915Rn07dNy7LLQqmOXnKFfUsnS2u2D0lh8fEpA7HrS
1ybzuaFjhl63Rb2grlRShQXp3U8Vv+5aYEKXqvLxjQg7HmssDNB3KoRnafxy/i6Vjz6lxxvgPDhE
D+qE4/dpTku/9EQu15e1ebCGJTcBny6IRaNv2wLr106SeBRv40XXvL9Ukq5iBxrU9dHiFj5E9s2p
lzIn50THoL/fleSfv9L1xMhgnKl6uGNGYGcscpf0f6F82qONoV+6cSONSPFGjF9i1zbmGguZsrrr
ujWoQVHrHyxydb+ks3y3VC6gcROzv+KVJ/2cTRSC3pKQ9DkoJGUGZpTQcK+8jAuYhEzErzLqu+oL
WqfXz5UQmVlthsfgPMHb30iNSHDdJLwQxdOMcQnSGUaYtpwWRemNJTrLNY41aYp6HNC9T48t7COO
dnoc/74jaZs6/nd7clMsfMrx17YO3IgjWxofdENN2kBcQlZeTEYRk6DHr2n58ylxmx6m7yvQ4Fde
RQikYluLReHSsUAxZHN2cPUDOaUHPnraBWvWEvx/k8VyCES0XMQUvlWu/adPzfPtQhfxtHJkUv/+
KanLrAHmfT7+jabEPyMQU8gPO4GOYPeOEkXr5tTk8SrKdnNeH+PVSc/k+jYTFJSG7GAUhWIf5C7z
7s/Pdj7hfy5ssFsDrM/MJBkYZLP5GE35lVzj3aadTc1S8krPVNZDcTi9v5gEKfBZGpc9Vk+3fyrL
cCfHR2nZroKzVOQf9t++QlRUUpF5o8Dj+TZChnv+J1159BovA7AvXugYC99GK5kgzl0+Bt54R7a0
iCLRcbSl6g4vQ/Om/bL2snvW6U9dQGj6IK3Gi03z/qoAsfkUr516rFNSV0YTu8pvVW8Mo/PIX6li
PKvAq1Z6AVHH186BOoBBdKxU6mVrxiIKFBYK/qoQRcVamnrMW/6HOW2ZdbHcDEcwUAUxPKOmSAur
wGOb9ZtYJ4S/OqDsPrPONI93xqKqn7E0lCijr1Amh5QC0y6U4LGWwG0eTgfde7adLZvf21XN7THL
8RBa2WzJOvtp35GaRgK0ifSVKG+mQ+fXamjuyyhOm9jZ+0alym6sbNfnwux1bkG6nomCuIaIWqKr
H55/rzZvtWknthVSeR2NjGRQtAk9xbOjXCLSCb9uEJk/sj3C9I2J7tIfBeXwNEkrHFEn0Xe14rcG
RS/5ty94Vzbru0qS6EexFYBnefMBWTk8KqmuhdJxqYKs9R2dV3Fg+prO7QDhr/INF496JqgrXGeB
XJcBuIoNwtX7jf34ks9/cBUAjftVlcAxNlxAE4LSOJ5RQY5ng9XOkym6zU0qrcrT3fC0Im55sNhk
9EwKpTHMy4WRgpc7icwU931j0LNSsyxxKG5NICArLvW0LNkZ9R0z4r37dXyFxQ/cpyECumvtPOND
O71mEGBOFu+ufi+xX4fGKK1s5bp50aLUePc9L33jnJlMJ6j4yXK8s5cX/L9U6W4sScEyY2R2hXwN
O9qwCIF1NwmPtcnzphpcQ7DodcLmdvMYoq1r6TLVCcu5nmB5GX6OdDwjb9ycb680RRTuWP9R9vpJ
HUXK6naRD6Mlto60dkFxZE5uJXkHc5rf2iO8FK4Kv9RppkhdysoHhcaESOD3+3wnZu1Z6GjYWUzN
QG+Y5Mr4Odg9RcniYP29156ScPd7XzNgk+FTgsZpunO9DsBJFmniWbgipHfTD6/G2EIakDOYhpgi
ZPcWtmRYpLJS9bB5n+1ZF9pBP3wg0t+kNHrgGZkx3qN6YBwQ9RTrmhRu7mcZpqE51efjhi34i7d8
/u39KTP/TFYr8pMV5ViiuHuCC/p9T1CyU7HyOxxgW+Hat1PEyshxYZVp/OHChSjgIkfgZcVbcYkn
QUaKuWya/YPmz+3wF2wDY51SNcCbZkGdr3jQ7bzWMp1lWghOfW7uuVxI9sb9OO0a2+PvMe2dIygo
iO4e29/ZS9VoStG5K4ppUnRn2aPk5AMKdbRM8u1VhWI1eRmijQ4zadEkiM3eS69EQGtT/gyG4CyX
JKqOfsdl2IlFutbAXWHkhiM4Vl0Z9t+YZHUL5yMvpyqLax5cdUjHGD4+Y1rETxhHXXMaDCIk+Qot
HFthjBOrMNyNh/EC3wEnAgL2KdNzSHHLFFjorSHxKn5ZdAQOof1tS8SZsM1fmSfk79CVdHa1OBCV
IXEm3TvX2W5S2CvRrXLtlnvw3KyzqO7fUsy9ezSh6RbUwmu85+mp3zmdyk2K7DGxpxzD2KUlf/jg
g0EJx5REWKj1QLRnXMo2aYcO0OsPEnG7OR/VkN5j9racz9Cses4PGF6mo+y4T+GPPsoPQ7MhoeIX
ikc7DzpkG5P/L6LlmJLu6GU1bhDQVHhs6Qo7mZCIBeJtQvB8TWejbMmF/PXQQZRe/e5a93mgW/d8
VLccljTYhaf+tXYhUQCQP8ZlJ6XXMgNq27MN2eNe+j7ckdz3LFipks5yDOZJ1x8J25tX9wwDWj3y
HEPzzbXFQm3r8O2sDH2PFU0xdiI+BxtHkQIII/DEixXwOZVBEwQMFbgtohNt3Vy1cMfeYoHncGM0
jT0SKnTI51fJzWqUBqr0CATQ7kFx9krqTxmNbARLZIrG35Ibnx8t8mOuebLYkLtljsoLRKiFhnoC
xyD+OA/oAu5ZVQyZHceMoNZsYEVnAGVnyut3ty0H+zo/Q+WhjgJPNC8RTzxvsWfVySAHHhe0rhtk
VNWWwpXIC7P526EBel1uF47EJ8hoyJM6QhmMovfKNnH5dTwq9LNLMVOFsjd1MnTYlDWwh2umThaT
QVc5RoI6ISsuf2mwi9mO3TyL5+QXdgss6HPuSdhsczIYpVm9zVgb6M9SNDS9fS2fI8GZlRNsTVCx
LXSo+W0AklZ2/7OxIiMkHansbzcWafaKYkGy8S+khJgJ/7XSpi4eymya+303gv4l6ZydhkrpWhz4
8LREn5Nt2Su0xDrXWAedG21XyShB2PYLXG07woGH5xl/FFoPstgVNcRl7a3vQ6rbwv83rDWgw6AW
X25hgSD9ZLkAjPdKl8P58GKUmPvmudyA/6wCAIDft61yQTWfjGq5rXBiIV72YtWASQdVDGkpy1Sh
JBKFfeSF//c18vbbj/Yk4pbCsATJpaiGwr93sjHJ55Vj5dAUYO3TxyNp6/uKpVre4ksFU5wOJwza
2M09AE2yj6A78cLGs2HISYDF9mzsvpTHwfQQCKge4GuSKNrXc+tC103MPgUykfV1njMu5gauVQ/u
eydMRv4HnaeO/itMzsBJMgCYOYZD75o6Bxptf6qJeCsjvSugwhREa1O7AyQm2njkGeSXTs/GeUz1
GkINLNUWrIB2ax554r0CumdlVFFAgwoEFXfMMecb1zJGX90ww6kI7YxJTWqlGE4btoExocK6CC3J
PQFgfVK3JJjRc985GEUpHWvt5Pqvxc76DPEMRG2VoIQprxVOP8nxv8+UK+YJppl0JvBTSY1q+dd2
CHLNt6j6wz3EKIefBBtf3Wf9kbcSzI8t+iRibcrsZlMW0qS2YJ+daC24EBGmDE9hWYAVvuXaVyao
67fpS5qA+kzF7zvjDmXY7VRjE2gGwJyxVTamzMAznY2u4ZUZUURykBHYWKhC5DIYVaG/TwsY6Gz/
/57sj5wM+JQ5kjSCVHEHxJbNdyfRs9iLtIp6wBNz34mClaRgYm+byclPV4zrh6hxmGzFrs1v6enW
UAs1wFP0TTOdnwF6eRAbjhjefF0APz0Aw4qU3tVsyXDkNBNrHc81NLOk/qbKoDtwsA37LbcAXOYy
4n+HLam/m+BxPytvT49Y2d1jf0Z10LKaXnv2+g0/gT9pWi+vjDaWe61sdHKogXn7IuMFOT95LcaP
OXfdajOXaLg2QFeBfTcRF4TVhXWOVlE6taFUuG58VeW3xl4bG4dBXKjUHi/T4NJ7c/81ZIQR2E4c
w4q0xoJh8nOrRqFXwyxV+2FD/oegpSt/PQPhecYX6b8Kon/wAJFhq7BXTk0GhyqnWz9P8Ph2yxjq
A7LzDVSdchBxPYWbh9lH0BcFhMVvwwDVibqu8KpalnhdRhqxYaBPiDzfD3vtbCwiSHrBLQp0ukU6
6gsYT1TMWhsw+hXeP/UkVwH05Pq/GbkSoZ2ulUf6SE9ug+TYXtdFHoF2DLrY/9Mo9D0D63cGHrKU
DCYQUAg4eRKSSJZC4xsqMkHqlGMgAcIoEpFO3p9bjGrANezwZmSLRtlETeC64U6WUoemY6Q4DI0/
aNlfANT5ziVqyAlDVXO60L32qcl2H9c6SZwL30oveKVgiVhdtajOEiw09xKcNHL/JFuqztUp06XD
W4kJTWpyOnXIJ8SlfEUY9B07RaknYHkH6lUJnlWQ2aVWCeHynPE5BNK1stTemL5Xzkc/91iy5reV
Z8jMGMeuvt31Jwuz7ZE1v4CspAxpVkSoSvKDvWazYMcLxMN0BxF2QcmNrHG7uef0xXkPHNLmAbjG
snULyXcTvHfVtGKXFZsGMwx6f4QIaMhBnAtYkB1j9h4bAdEAWYe1b1+FBtqIKv1IRfzFSIN6spXr
ZTbWnnKGWLfzXcq+TZcM+9io3jS4tTwyqdeTZu+mFsj9XsFtjfVTLw9hSm5UWqzZFCuccrVOYoFy
GfmRekm9O7r/C0DYBsHDGxmydiRJu/oH0oO4DoF3QrG0h7X+aeZEymdAnIabtm0ef4uCULccbiV0
yVvv+GfG+zEPnPoKoEjlGXJUnaqTsxgaX76xbZyHDj9quvY5FkPKxB8nA37ZvDlSc1XHPZm01Muq
JOaDgAnp54tKox7i0IAQFIm6OedYnWQG85pQ/6bJFXQZ+/w4CDqekJpO7tVmAd7wI2KDfKhlXGPv
zn8WvD1CtCXjb1+loljjzhhgPpzlQxmESt7t0PklX8rLwqQ1b1w1Y0BIRV5wagFy1I4igZrD3VOE
gz+2a9Bd1+TKZxEY5cbcsoOx5GLqcfv4EHARhtZxTCytkoL/alnSJFDSfCeeH2QMfFseU7bF4kYG
JLyWktvTEurl3bAVT7MxKQzq1lizUQFiIpqsY2r8Vkn5OahvzjxXB1alIUSUJHcMXYg09pZDk9V6
sfLHZ7rdQY2jIpL3f9iDfvSK1iLOygHxmbnbUt9ujpAolVwRRqynfbqcJa7pgkUQmCm8TRNgoPFw
WQuLifAtTI7sHmIJi1XlZUohAEdbiDRuEvv4PIO3ajFtcW3LDWFMoEACN4MNrIFeDP4CKa73WlJ5
GWdMDHBFE3XGhLZEKZTc1rmxi1IBqWph0f3ILiuAPVnh3DpDNQuptWgFZp/Hzk4UyGDHvI8HhvFG
r4FSbVr6ClkHZg6xNqSYdL/vZORVO0/SJREodyi5n3nTMlH1v4Ve65bwnkXZOSrI0CnlTVcbL1DW
SFWsfubmKbfV1Jw3klNflawR8QfWcRnuMvLNaa9SchwpCmjkun9YQrMEYfPRdByGjycOwsAq+Her
p86Hzq3Fj9o4qmIo/AazgXChyMzXL5yYV/xjFrRyhsbP1YuNEguxKJG2HbqJKkgM/sGEa0gt2H22
cg8y9uEem5sCBMliJQ3LJFCASTvaNaMQalfWxqe7YvGvmA7kT//bwaYBWreaeiIlVJzi34WaJIeQ
7OpCKPRZkYBIoKZKGGh4G+my0NbjY9IVnQU6zwJwiyliTwmj1QHKCc2A/IsKSjUowOkTpJ/CVhZo
14tQFhCVoQLaDObaCJeNVsjdMaYrXLY6OVnY3MLMAJgsHz+ZV4yCQRANLDeilnHyrZ4+KB0OWp1j
kWemJ1q6JCRBDiu+iQtxh8pu1DyxiwWrLYi0DyAdi904i/MGgSPVdy1Jd+4OWjfWYfOAeIQRihvo
lHoCj0dIi+ay3JsKE95o6d9CHMhMlB5qwWhke9mN20r4lI8GeLGFu/muIu5f5kDBtJsgJMQlrdM3
1HVDwuF0hx0ZQbwMWmsdQ1R/os8UQ0nbFCiC0h6sVgLelzb5LEPfUHDDPSyy0LSdEymZT0DP5adY
xs0AkTTAkyANfT/a5qIMpyATkgsIZMyyVmynKrHLQUc217axl1MSi1Uz2Y9zBrZ8wBPYOia3m7Wx
LWiRm//n+fCIZ+ZhOiRUv+o7J96ocbvdEPYD/bA6cmwaTBYquxJTyuDcc2Ov9sWyu8Z+jHc6Ltbp
gkz4DOH0Ve5FTYkiUYhKbDm0Wlg3y/0dcXomkVk0XR5b3mHDfWz4erFWJVnWKC/maTVok2sN3/Dw
DKdAwZ6HcOrL5PKl2okd04He0//Zl/wPItL5ZcD9BjCDKBnePKQdhLUG0VBeYCeWy6VQJwTagxWZ
jZxpnXDPwn8OcR3hxN632ul9jJ5OHK14NgG9Wx2HCbMiYIce/lIsDkADqxNzMFnyOT+vP81PewL9
ESOYwMNH1K3O1usxFuF7bspzd6J8zDwAOuwJ1D+ckhjVyFJ6BNbm0ZHcgdZll/+t7gSKPXKOJIg7
0OxhKOb9oC9F9w1IU/QZVr73T3RmwwSsP1Q41CB290HfFRJwa+kD7xu6uIap1WcYhmJ90ie71U3h
yWDhBU9loHKaS5zI+JvOEr8bv22LbuBVgz+6lC/Qo73cm1GH9F7jOY0gUUYL6cLrruZbB+ZKOPTt
0Cm4BossZ77s8QmOQzf0HpbyEoCmLy9BEKT90qRMvnOuKvuPXYLt8bzeaWBvnh0MQbdvrGPtfjOz
KaTZlzVuHY24JoGJ14xUgJf6qc9hZXpSCidYxOHAz8E2hGIDU0dkSriduLUhhFXVlIZPShaUr7t7
Lu2FA4/hWccuJ1zSI7yUo5uFDSYohN60lK/QgnTPwJlwzE5aMCF3UIt5FBs3lTp0xQt7hMmt0lsV
zZwN40A9nMWwewQ9r3AOKxx7rHO2FTP/03/aWPDAytjTcO8sPxJ+Oi84i9Za5yms6fsh6mhNwnto
U1g9/pbELITeBKsVsOJoDKEHXBcMbR3543on7eUkeyoOUILGRMqNoA/ReRsKU5DiTmgTqtllnpLC
WsmHcN+nQmFHkRJeqyOxdf78hIYrLh9cHHllT8uECCeYFOtPn5OkhyrH1V9XXsipQkXywFvNpQRH
aTWrGg7+USQNjVNeVegFzNL+82WY75xfEAFL69JCsb2R+y8QBPO7TpQCgjFec1InOdFl2rsBrSCu
YrjcuI19zwe0R9oYXnfx5LebCqhFnKH/fOZWpABFfw/yC3au0znWBigegjYrYsW3/3qGqS1JI0wV
hFAxzffjeobA+F9OBkDH4F3OvQdeI+9rgRrKt8U4bV2VYO9iZm6rXc+FmIjq1X6oJoh0UCmHSLhC
tuD4zWTfx1BrBeHqJsTp55lirzPtaMgA6ZGNTw7e9HgqNMlzHVAtObnk2ZUYCNnDxquCllqW7uU3
+F0HJSCYSemAEp3q/Io9EEI5Xtk0/AOxU6hGx5sb/Y2u34n1duch9HfoZjaIOW6kbdDJxx/fnCQx
jAzDTBnM+FPtJpe8zd1Fz9yNka1JZ73/kjU2GAjMoj4ltjSdyNmzqNbfop8q8LOzBEVuPjE63Q/1
67aRq9dihSb1M/376Q1dX6OL4z1QRw5p7/enqBZ1K5/0WPkWpOpjPOI1G+pFtIFyJppSV9tnq8do
x0uvbCobP1swV4Z42pZTP3CJoX61nh6spw15S/u8uKVFkQCvcCQLRcRPgjnIKY1mrnnuOHiMmDEt
YENLlOrfT5sMBwzJglm79mLhAbf56zAqI+fY+zGHCBVJ2GVtPqdUB9Sr9+yn+CNw+LNXSjipy8iO
oif9KYZnFnuvkX4e2nmq/URjjU/GzFH4EukFMluGunma7GUTt6MzCORRnRp3Q4CxrZVGqnTAzWnw
yci1RanFkQHgK56fqoxXVgtG/SIghRjq5KiL1pUQfXBjr7Sc8K/mVwuQa4RRfj5LwB1BCmi53Fq2
niuTJZ94L+4SnuE1HUs1a0nagaVZeiflLT4lWQCGnUhhSPpUg1ac2x2+CLW/b0h8RRTVlIIj7R/p
icddeEXSc7WHiF9OVpPBYT4xFahjA+8pjA4dD/okHZ+IpO9nHQqtGHlWJUqY9b8GOD7ngdy8M9eS
Xk/o1uhK89h3nSQIXCKNpDI1zPtSWOSCVmEqAiH7UqgJ6Wfegb7274pqmfQ+6k7tZ1RUH9ez9woW
4ab/wlgMsgnDiEGDnWvUns9vgk1Cs2EoatvRgTiMtkFn/oWBapV4KxVRCqkR9cGrVFeETKNTVXvG
tNfbXwZdV3xYvLIZpjoJ9E5V08LyGd6Clqdm3GqZZ5qblA2pxI0DGx4faVpTOvOATYVgxVGHq+87
0vHxRSP2gaM11w6IzIR6AlE0YHbsHAF01hGq/rZHU+6Iyw02iJ5ai5btJeYZC8NikpVwc+MLz+Ds
B++GJnv2qAE4GNqGGQx2EZe559s8tiWik7NtIr+ER8YykHSwPIHgS0ldZGMhCzS5iKbnmvwymceq
jaA/kYssBKAPcvNwhWkYIs04VgjaFh3HQUVYe0qN670uP6N/HibszcpF3jT/3M3NSNdX0kYMvmCy
lta4LkYQI41sandUE6jO0PIjs5Y6ZOtNDencphUaHw1VPpwh2yG1FMrm8dv7ZlDr4w4UbhfpXAa2
T4ByVTxkShERJOY54b3SQ418SoY4/ojEQOTfZBTlR4XpjmtYC3LRwcvFFHNlipczZOI1nbgpTVKG
bcAWWadDdQunAxxBpfk+bPDPlkvRW3gfOlmrSlpsxQe9q31R4kiMpMxSJwYVWpKW3dqzWIWU3ZEf
0AWYevQ5zAg0k2vCtCwpbaeWNl07EAgz2DDojOgqdkknQAfeIHPgub7AJoLGwsIpnwaN9A1YMX6+
mRZnkNw/Ae+C0Jy/V69Wtp3fP1jwSaQl27828lCMrdS+D43sO8ghLyMaTmoWvTZdL8ldnDU22pme
Iz2Hnxv7NBjEcQLfjLW6dKgNcUmbRnc+5lHWpMTjwMYdvjXlPJAko7sHksLQ8dWcpxZVszWqcoJq
DfZg4HCnjfM5ZOiAjOfNXYcbSMgTD1xJuh5kplHCRJB0uWliUUSLspUDh+hehCB68RSzCQpf8ddc
blAFeLtkm+HYNpD6KLhnQWd+vt3NgiU9kHPMx3078ZNrOp5H/W+HHFHv1FftXXo6e1YIAZMx35mK
nEuUgXdi1ybX44f/OvgSNcgeG1wWrLIbD8j3fceGY5oCRXBIpWKKRWkCCVUO+jcIcjwri31eR+2y
YeAEnb63YdcJZRd3sCW//86zfVMNBwjSERDWc6WDWL7CzMoLQs9MHIBSFeAZ7g73x5DKyL+ZlBdz
i4Av/FXB7eE04c+rzZIpfMfg2wbChESzbpvFPNHecBuub0wSnH1dVS+DVq6c3GMG2TVZURIHbNnO
GzDzcgtuFcyPN/o4jEP8HW/kNgED1PU85ION7LUcidYMLm9cafSJmUsLAmYRYUS9phu7IqNmgLic
vjzXbq1aKpFBNDQUaZhmOQ4wObcSjrHw90B+jbyWibz2RIslxirhSsnGSUOeP3KQjjSHnnkW0nOA
SVdjRbBOTOlQhf1bGLjndKnvTDgnfryAolSkkSW8RFwe/Lu64VhAB141u0FEvzsRNrLkG8rSEI9r
ZTNvAXmuK1STz1H/rjaKhGNGG6zwXomws6L3w9PBTYoabqYgGfb5jK3FTSrL1VDVtBq6PDZn6ucD
mas71GJTOu7wJUOYtI7GWbQFGlMrwx6K3SFQ6FWNRAM2nSbOI1I/xUcbxBt7EobD0VULeRZAxC8r
VNnngUJqkho726k6P3O5Tj5+FLbCkucl/bD5cAviC5hbjEVgVhJnwO9hS8mRLTW8yPJ7SOYAqlyP
kAnORGArflYL/GTVOWajPY0Ahd5Vv2VHLmb4kTgtprjysmlA7yhKEMW1XWfQncBa3hDgzRJSHuAS
e5Qa9yN5Tnqljxz1i3teU503ybWpQsfGaD8owtHANFGQ6ivRx8NTa/oGSeWiIlCSWmSn+Z+tcqJK
RH3nx2niPDUCwt9u3ph7p0Pf5SNJ3taSjjTkRLaPKyZuXQfzx37cRw2ovFpp/TcNHrdqdyAyFigz
6p+7Inr7ZN669B0eaL+zjrAwqkia71hOD8IuHLRGDtACrO6mG2TqzTLV7/Mq/sGeXb5MOUUVw9+x
5NyK7PUwOEtweCnZyyzHy/zcS1oAXHn7TH2VQ5W1+8Kb67T85A6cjznwxHI4B2k7T364PreFj89A
RAy0cMOEA5wAVg3i0oKxAwzgzSgNGMqiyAyoxOigpzAZMuAfqyAnMnRJ8trv/Sy4acfctGmJnw/C
AXAxgaBMf1i9Sy6YRYNDtCUpWcuejSZDZZ7xSRSmiLLS9+1X75xQNuyDiO6ayMr/AKkc+Bx7ghuA
TheJRT3IdM1SUuX1iCulKqGtxsi1wRbrprldoOnJBs+nTmnGqvFdNW1pBriXdM0S3XS16yOdJ9DB
1alvGArogQGjb1aZLYZSjcDZ/tu137PlDp/xXABKEVqSAFALh/NVdBWkgFOUhhm6xU58huWatDMe
wMuvZM1qBm4UB1lnxpLTtqyKJ9QZ2TpgnsfAOsIsGMxjUbH1TRkhAWKq0+N0bo+FmESefS5ragyH
21aIi8gunFOXtzGTdacDhRT/ipjctcbKt95aF39fSMdqKi00w0+lj4rR3a3gYCElfxxHWUSLuJ7U
KuZUKcxyOSWowdh2PTm+Ni8XdcDB1XX4QOXDbuSyGHDdkZboIkrG5tMAUjnQkJWqOdIlXXWcI2C7
0GwzsNnZnru895tZdRVM37fApeBaCgv4ZYinU6YuJCvlxuH8Vkq0aRP/hFfYaocO5tJH+oUC9WMo
v2t4FFOXQh6b5HI+W/hARb/n5PtWN07VHMQcYOQbVUauid2/zyhrSY3vqH5rVICQUUUNpy0MDFRo
eXHk8zm/qLDxSxcpJZFY+sJwOBS0IJS/ArJ3wfAzYH6gwegN6kLh6aHLMB1Yhyyy6Djyky0Bsgym
naG3Uu4pkY/kQNPC/6Ql9oauqXFy7QHmNxzHS409TTBS+rjPW+D44Gq7WRqcvdH4sfW3oCauNPD6
xfCdohJW72/yPtnAgssJC9qYl283frRJBag7Bztz/qqgF3U81dahNQvSUzq0lh9LO6J8o3BUbLNM
/PtwTTjEns2Kere0Rx/pJIvgPfLEk4L7a6/ncRjg5EH3mVGhR5UgDc7NXM1j09Rl7HVbcU2hzWrw
2OQisPtVB9VEaw5s7blvWYPlxU0/szwG/80C/1PHNjrzvb58l501KzxHZYDNxuroQHX6Fl+yFHDw
zbr/jR3Gc976rxrlNn7JBSoTAGYPcP98ItogNm2EM+OIlhys8qE8T6lnMCZcEazAEuVNg9G8gc4H
ovFuF8u1FI1TO4UkXsAU5sGZJ/Z2yKdXPsHQg2hqvCizsoROw5Y+hlbf+u3xbocp64hMvxud1DPF
cPB/DpJ1wCeowpezR3VlxaEs4bNiuSFmaAGwZiqT5D7vqDfPS4zSZ9fTKsInw9rliSLjL0Dv4crn
u/jHz//MGalSUCuH1zwBC67uC5UUiAwLPsxCLyAjaN9IS3UHmlwAabGX68SZMpNH6Vs9XLGNAFl2
eifHQK0cr45qdZOdNgddC5pcs6LlFgmcXfzlywfHGalkFGs+4QLJ+NalmA+tBAaG1qTF2LnS+GdA
j9q/1O9C2Ze/2AG6uJpwW4F+/iwF+XXKXl07Rc+tCKDSuF3kf6Ahxr6i8H6XEUs/trWOCz/0PpJc
10yLVP+t52TSRVDA2/lNRknx1WjwND473Q0alpep9KnOthS5RuILlaYR8wbfSw0tbAK51AnC+lVH
X871XCZYV68SE7DfriXyILYZn6r2xdGa7x+2UT5NgaZDuj6YwyA7EHQHFqMIyS9GZRDhTbrXzh/Z
fXhHLUdhvl99VKOkdFiLv+K9kQrbg9bfoaS5G6Ct4CaBxpehs7OVzmyMGeJfxi6c7ZiqfzttLzlp
Pu5Dh6ywobLMKU8gKAff5EVXxxUEtUCDEx+Av2g1+mwR0GZO5D3HuBxuq1RqSV1LJz4qsgedkVrM
2L6KK3zAqHFI7Kienklv97vPu0Pg+DqR8rj2nNGEiIp1jp/Z3YhLziKxXhQjLKxI6yrtof8e5goj
5YU6PwK6uPaqS84S6xcJ9LsOQgPYj8uwxbLYm7zh5+GVmm9TbqEQBnTd3RjHvd0LXRQmfLTo1za8
k72Bts2FAVucclsEcPF3AqG6KYaWXdLMst1BO1qOu5FpqxeqE7Od6/bVejT+s+7Ae3kGxDdR9n0c
4RECGCg8Ee/p/iMVaPwtq8JSCz1n63jecpDQKKpegGESyxlzt5gm3l7zm3BL8l2c9XnRZm4MXkMj
LRaExVuWUVSliaudCN4IBi/b8a5gqWgM1CAYFs1kdRPqIH/WjCjkhCXO925EmGV0sBa2mTb5Vocv
m0RbURg9roRLdpXzG/hYPlgHrckpWNbdUW4KCpYE9afISX2+yfbR6DkBTb3cYi/P6FMib+E3shnf
4g8F7+ZFk32gp+IJ7H+aoYk2AFXza4gdHqrFVOBOLa7o7md103ia2tXcS8DlXSMG1xUDHiryOlU8
Qlus1XvSOio0GnIZRxdmSi5JpArOmR6qVjFqXUkgI6KPx49dQKSQaauTa1nz5OfXSBgNaoDrxDFg
pGHeuCNP5coIRT5mstVccUiij7FePOb0C6bNr5k+ugzDaN2mBGNb1Wfryg1kedMO8LHuEnSrJ8Er
fLTaruWA1R53ZIhbmLlz/B9J2EgOi2W06JrG7ynk7I5//E1El9yIhB5f/p5VSniyBQWa6I4i6+fv
V5iSEhmPPSVs55EKstx5egjO10ysyFq2ugmCpavNYn0mmeIk
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
