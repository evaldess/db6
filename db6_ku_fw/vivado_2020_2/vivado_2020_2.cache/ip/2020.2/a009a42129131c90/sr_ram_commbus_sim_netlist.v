// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:17 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ sr_ram_commbus_sim_netlist.v
// Design      : sr_ram_commbus
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_commbus,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
XHE3IrNUR0rAgOSs7TaneZOCem+xKOaVUndAgQMQ6fiqQ7sNz2l5jVXfMEx0J1E5drsp/vFpyBfK
us9s0XKVnQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
iNP9Rj01ArmVzHoVSW7lElSGoWnbQe/FKLklfFiFiJRRgWHkBTgJfwNby6KYAgA4XLe1eWz88cQS
FukoZ18JES1Zuf+KwL8zwISn6iD7iixfZNEwpWFYjyj8XUfUUjAVZiCjZg8f5vwPfWs79Kh7gZBj
vgDcYNXjxLehTwCVO1I=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
nmobDEi1pust/app0GNcoN+V8y2mMEri09/oF7dQ5ZiEiG2p7rMxs0iS5vx/JpQ6fiI0X0AJUPZb
worjx3dSanWZxlmpvUQW1C+LK9h5RA4c6zjOdaM5qZ/K+NCauMad2OY8ZgcddQsrreoTh1nJ2DWa
TaZPLvv5pf3U+x90B55qP2fEPiqbYkbzpATAH9u4NTH7sLWgjc2AhgaoW5eC8oXtXFv8D/e6aVTG
z+0zADy8vVe9/EfQm/dJ7Jg0DqAR5qYWGcVn7yVF+tPiL3kEf6FJZBjo3JgKIu+qAthsglm8Cx+j
2KVIa2CX5Gw0SJbZkMW71N8rkZU8FopYgshYqg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
sQodddsOwbYSlSsSDMNCYLeaJ51uv4v/ftdtzRqygsJNUO74ZhxTo7+viqM/zY+gFJjqy+vyVh6/
lpYCCvOfPW9ohlsyigMit+d9OfUAHtHOnSwar6P7DvEbD+534I8OBinFHuDcHnDIFirvT7RdkfNd
uCfMWv1oGIMacpnu8DitSYvvt8DCB+bHlF3ijp/IC+P6O1hD15eQnQpsDwpKg6nnVcZHA+6NbT95
rwOncIqFR4E+wPstj6ayfvxsin9AXJ/L3hE0nmxedSpKDKOwBjtiGDED3rRIS/N2OZSt7dsYgyAa
MHSfsznlBT9CuauHVihH/u5MN1losnUyYm2/QA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PcTPY1NzlVv/1miCbWVLH41v6m5uRKf5NQUVNklgE08sx21KGWF+V/ICQGqfMvIC5eom8kSFM2HQ
dFf8l+zO8zFaHEcwmOu/VP5gnGydh7qelqNx+0jPz05q2jp495ez4dMFlOZ8sQGQEzx0VockI9xn
YjRJ00trguEtLmc6trk=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lmC9ahCx71j1/ZSeKA8Rkt1tIlMKGNu+RHHj5Xtwh0bt4FfcPDS17km8+8ppXi7OUTyBXSIFrdK0
NooakhmRZCmMYOTdKwnxgk20HqIlahm9Iu+bxjgvH97W6T5jJcYvFslglttPbZrvLoRpnSfUfQT6
o0EtaHvsEFdvL9+ScRUKPku8EqkOu2Bw/VZKo9IMnl0FoU5KXba9O59tKh2rkrbNw5L2gwOiI4hj
K6KuGhkZNMCIC23+bh94VLvhhAbeZ4zYdMXlsjm/BFrp9rW2/KEFj1X0Rlmh/dk5PzuDb5p8oOdz
YKZejj1J0rHlMYssmi6qnwXn/kI09IersaxdRw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
anbwWs0l97JVYhigoT6et3H8TOlASkW/Y/8eTKUdRC9TcUSfTU88XxtY8yyw1fQpzUYR2pxNi2ri
ijWnRd5cdXyd57zrFR97a5gvOC1uBQO+VwZqLcjkcD+uCBspFim6ZUmqCQtPaJptG7SMYEatmSeu
5AOckCi1UQBo3bcklZM89hRwua0b9rPBtFacTvBkGGMEj+3Kb+3nEBjrhaIJyprIebvMvsj2unDq
NZN5AyhAJSQgoJgaiptXgMjTKV1UKRQ+AUYG3Il2upp7ugSL5p+QJ/8P9M8v4jzmg6XOd+GGtyl5
iWC6yFcF9Yjeui98q9M6xYivbpBmKndva6F27A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SEfonlyNG8YAcVnPx91iCPk8borIGPaWiJLZAjQ4ei/rFpUclmCrmdDaAEKl2C6egNjlAS0+sjPS
Y+zDUbgB1zmvlc/tdhSobfHENw4E7nVpOiO3LpH0RNW+vE5gVHIgH14HjipI+MnMpA0WPM1yKTc6
9vNke9I8uopfYKPwA83sQD58OW6+jvJsOUI+g8qfuRMbZKYy/Y+NS2tS4ypXR8KfAWW6gdUxjrnw
P6T3WgTbG/zxJarG4sORWn96Yc1NAiD44AkpnonzeL86+briHkw7CsuzAVLHENNjRtcIeC4zYXDr
LMlHg9gcMiK++n43ZX6hfeV9cJnsZRPwcJdMvA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lo9lKufC+4lUbxCisEYQ3GipTP95COa6tmahcp8LSG8DdAWaHT60LT7lpmYwIBAutlJSIqVJnIHn
qUrADSaI85BggWmFFPiBJ9l8F429HJ2/9X1wD1vQmQTxvt/NBuo22uXQ/9tVB5jGm66HwdD7M91B
vQ/PxfdS7joZd4HlMEsJLq/DbvxI8yuhcPiR9juvFHiU66JL+blx5ETQSQ7BUFQg9UthtE/ZNgFO
J3eLiChOF77wzbPzU9J9Ypvm/Py5gy7KUuzfP0RlH7s+PK7XKwdoCXUWxfvIJ8LKfFQP+lp1RpWV
4tEypdUV2MqqFIbhXuNGlk4AdOtkcO7Vh1IvXw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2128)
`pragma protect data_block
A+KJ0qXDaHyjIpMq8HxJXut8ZddcgAQwRmR2fJ0TveJ1L+o/cDfUJvuJ3yATS+687lmRK/ZGIdAM
oCUGOGB4xYct46kvL6GTT8jEipTmtRU0m+IEMAkH+R6A306A1HfXDwPf1j3C9UsxSwp3ou61V0Kc
mWklItn340ujgyoXqR6o48fqQjHxjotc3u2tuq/xjCBiATZYg629n2o96fnslMHk/InK75mydqfe
/yJLBQYiSeWKlNC2w6NWhO1TE+YhdF//lm91OGsFFo8UG1bxRBlcOt3e7A8Yu8RpkEoN28OyhAFD
azjlGoI7f/uPHNLgKgN5YITSltdp0uf0amBIrIbE2gWMjOUD/ewO4YdCjg7cFoI8JWZQQVQibvTM
Qatyy7rTVhfngDhjP/bY7mljK2hM19zqKwg5bg45kcPXqHGrveoUwsYcY4I6aj/drr0zleeKds57
F9JtS/y7OC9dWssXrt9VBD6Re9GGU6FV9RWAOZGZr8HFTxA/BqILC3K0Lu6ySn17vseMjA3NeKLf
+6o+coyGrAgTW73eUU1W1kBNHtD4xJfrFf+Pm6NN5apR5yE6MOrralRJM61i8S7kVE37vWwFG8Ld
ECGZ20+GMrkv0YSJDok5HJAA8GhNwXN2/n3EwaDzSQrEQJnTtYJEkK09jEFPHiD1ttkrG3m01TQS
eeuE8qgMWrMJLsGAq1V+2rcq9KxPiZEHokFNcPVTtEASTfwLGbHSuy5WzA87Rq+ZA9JsPmA9fdBp
XjiISnT42qvlkc+b34qb54PP0ixCgXIuQuvFGaYNg51FGMEvkvA2jGQaBvuPHZtIC1KGmw8VLFtj
oU3mIdFVxb+8Jz1I6dXM4QC+gquww/X7zzvLETpgT+urZZDq0s1dmHhhb/qJWXc52U//euMwr8ns
gp8lhTBBFVyYDkjloTOT5iWohFjjhArvLqvItu+g43RzsywpqeN9/dAqZJxmoKfqhKlvWAzyQNDo
Hte1jDyeeGnVHSsYHGtNkLl5N89RZCt9JySyop1UpGuIebUc0mBgoHxrGycTk6YnSgOvRevNKaah
eb38kqgoI+Z8+w8kkN2axR81QE6aB2Ku2LmYD21wP8+Tvo9JgaojSencCp1K/xpbPiM+LAVGqzKz
AdgY6VT1mOQxrfu+IESv7fafFxjZlcRB0h1/pHy9l1TGj9gaGg8i4cwpAnhkRMLQsEUgcrN+Nz/L
3eGz9fB7ibummNxMAnn7ysQxF+dhWruieAE34z9ewNWlJidfzor6BYJgLQfmT+ldKev67E8Zf726
6ZiRLLfzqheSlKzmVjR9jzP10JTqQ//ivkRT5oEb04k4raiA97kajb6H/lNq/FEQNI8PGNcx9L5n
N/r0Bk+R/CDgjbQK8hu65wT88D7jYHmUTFId6zdfOZ/oQVqSDUGGs0jlLDjXL4rlfApcS2u/2jzX
9AIEw4QKQI1EI9pxpKEw1cJBSX4fGUjPLpuTYh6/9n1fiJe59n7hHs3GyXF+87RUh2Ww/sRcN+4X
UAxYVzGjlAC0/2Jg1w25l9a4FFm2HQsV8c0CsztZ5daGf/CX3EtnBkZREsNihZjtsD20llAQ9Sl+
Bo9/aoSyt0zPizOJwmjpnLZI//tX6THtxNtxRWGJenTUpxUMlbxdbQswvKS6Ev+rZCxFjxKU5Avs
liIQIyR5bkAkZl7NCn0jO0TLG346b/wDVfLiGqSDYx85+Ee9gaKdjsALXRfgkS+NBUJASOyWicT4
dymG15LfS8Xj2ieM40rYVXw1sFMkQ28XkWnN/kKp4YisaKyA11U3shmfzXqlRO1jdnxyFiyqK99z
VRdp9WGhfycckPgk2u8Tyj+AB6DfljDzlpSKNU2dAT+iDhBQ9aT9wqeWhYGvR+l2nfcKZVkL0I1M
i3GjJNwW9yU3WqTQxh7xRvAI73OFkMuCOn6OgHJK8msyNENz8NEdDNZ/yShOoqsKycFVZfTlRLXb
QyVvf0ELPPjqPsxqsYRed3l6hjPbwwr70O/Yw/ZCVWs7KO85q+GMkmL4VukkQuYv1KCKbo4mqMPw
E79wWnwbGbtpCTmglALy07GGlaU4KqyCN4ntGZvV14PNAAvduRxuo9GUzcq1YqOmQzHRduNqOG1b
l6GUgFkD/wGqMrrhFaSrCklE8p4iqmMeMHp4GlYYL7EHauacTVKMuCuj6bHOvTJN6k9MJEyonbC5
91CZ4DWor4VKLFPiYsOR5d5J1B667DtAr9RTyAZXyXfdHL8/6FRndefgszTwqbzdmcpIMifSfAXJ
sclhKbtuDIJonyzEbVVzUlWrPW1/llGasrEHMu9rD31Uq+iBoVz2Xprn/ZxagUErH0aXSAkVPAp4
jJiaIpMVFRRVJ+yqMoVUzIZBh/XrSi5FS1G5MAXWMb1o69A/IZ5ho4hvMdeZNXY9Z+6OX6DUvBnk
mhwh4PJMNCkJ3W4jxH/v+uiXYsC1fuSgWxfAA4lrgkR92dp6cfuNyjo3DejsR2sE6baOFpkhslHP
W5CKZP0JUwXNKY4c4hzqYmkl/+d2aHekM46WmHHdq+/48npN4UEbJHTkU8bM5Bpgj5JhJKUUnBHT
6Ip8caNUO/QE8pMS51DPytusXi8KDaNVOkv14tFZdKhhh2kJXdDBNp7a00xz3qk1FdEXWAWSqqIK
U+HoFuvQFa3rcseseR/cPCUUNkWj6nBBLhd6xGD78uf8qd80AM4U/JZ8MK5EIfhwFq4wmavL4K9o
z3V6ZJuLBVIAW58wNec77dtvBg14kOTuS4YoCA7B3pGNmDIsaUf0Du952hoXozEQOS75+6DRNpnJ
81+3KJZdOOUb0YJ9CZssvjhaAQ==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nwI9apodsxWnt8/qZ84l2L5r2ru1rYRvzH+cIiU2LZ7ZFrYGVhrKUku8GacxvPmk04mNLHGAUf3D
0KN1yrZ0UA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Sm1hR/bXnEX5hSLJC+m0q+qTo+GE1jW/bGh9GYODVR1B61WO0x3DI91rmMkLB3jXabqZYmZaVRnk
N8AiDf+w3tD5cTm9k3UfnHfkmqEgj8LBJAWCYHciLWzjmW7DKTQG5Copg5YaoAmLrkH/R11p2QBq
US3uTE+2f5z8QlQwimE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
y/EngzI5VWuiEHV+TKhmZG2qH1QkzhsLqS3InhpMlNY6l/FsFenjJYgIcwfRB5cHNIe7FLSQt6Ne
y3HMmpsqF6xetN1AMKtt7yIa7k99d/5TC5vyU4dMYs9g27cqHYJzk93esgZCvjIZLHpcXw/tu9/b
4U5FbTjst4GUWQQ7e+FOVWa1BC4H7jo6ZOE8mZ1oMeTUDMRBFFBQWv4xUZFg+dKul2euXKFScShR
h6tknaycBcdNbA+6dQJo+VgrTTewvfrkpNyifPBwk9vIitRhFkJJJVGsR6T+AF/UJfY5dEYYFuu5
J288ggKjbjEUNQnIyNWOpZiuhpClTTay3laNkw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
htKUMvAlzdN4BbAAeNmEM6Yr1UUCORwvd6+1cV737AnX/e5QyMGFY1ZuaVzrrzfIKK+VWd/bFDYR
WeL3jKvGUsyl0cMQ9jcxLrsCI3RnUD8yDbbqyDu9KMj34D7UA/k879CbEg7mJQsE/OUuwmk5Rusa
S2E+UVp+HrYNnNymuLmmn6wOTCKRZjZEMW81xyRvJrDTTqf12SjMprM/ubdETBwwiEzoIwLeibWv
EE77NEiYVwYpzXElBkB+JN+riXCrervjpMbAzHbeomW24pwXmffMMvkj1nRzaEI2QRT19Hpc4iqq
tT7PSLFxC6iyyFn2bd5a57kSCEK5ZaaxszxEVg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
ST+OORnrF+3QguD7AuqTgC907V9FPxT3xpP2TfPbwAQB2+m85/czQ7xrlMYLNRNl2qldRPC2JAtf
yRLJmvKEgyRtR6tv/9gg66CdnvMVGbBmprZnmsgKpHGXcIGIVm6FR+ifL/5pZcFZyTQCKYlbE6bz
YNrIQ8EskAk5YXNHRZU=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Zz8HkbKk2BMn9pYqHdEWEMFHnKjJed8tZnBzajqsks1G6q0CzbV0FSYoWS1nKj84tIU1JkBaGDIt
9sdF4TFidxOJyhtrmpNfTChKxpMr41K8vo0yCOwdi29v/VShuI/rkIBCSgrdlmTBWBEgiBS9aabp
Jqqjo1ol263k6jlcp9rOjaoU+lcQMEXCkHoZu/V2+VWtTqhoSiWKgDQ0jJptGQig3wemEM16ctGQ
xX4urrzlEYCVTlr9g3mn6x8NgAjEFjJqmg1uE21AWGXfsNowkj2dYZLCXuVTF108ULXlOgx8TBHk
tPYc56T7eylPXV3Y05Z7agtvOLTYldGNSnm7qQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VHzNHo3jyVixjpbjlcbNuO7IrIjCuYoXTAjRb06/SIYnbUS1pXATLQwryf5S2ETq0CYvThlIAGS0
xbNOLpEIhHMaY4VNrUdhUPBHXcXHWUCHudYKaUCB/Pk28QZKLuHYt3FqZh6wdzI6AFJdP/pykVJb
M/Pyyc+uLtqsAqyWqtJ0puNrBSpFPSM5259v7Gum4dwYGluRNUyJPq0CnQOQDcjaKw42cmf2DAtX
CSJb79mvoLdsFiW5ePQbcfrrcT/FhIkNj4/DqMVl2EB85zQgcPJw5Up3lLGw0Qd2Cd1jeq3A4qcf
LraHhfdfhy6tS33yDqFUeXlzvLfkicvxivScIw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ir7vg+6icGbLB3CLLO2WEVH7p5OyaYzRs27g9ktjlLGEA8UZWJVD/LEebYJEdrotzhB8SWmHZMDV
/tU66bmEBeBvDhzPDFffP8JEne90WI2d4WsOz8gc/qUmQrWkWWpKaGeRzRKobk6HEaC+nXg3PqfM
0b03fbE0S205+4xE/rEnuHBIRBfZd3xmeVaB0HKBt0SGPD5SSQQZpPD38QOtCELjuuuA4RtmpS90
kaKEHc7Je6wpd85YQOJtbSfSfwms8QmBrV2vuYX5vgvFoWdrKhFu6ei5xOtYRK3gX3JKdEXLebbV
49uISo0iQ96Wfdc+51UDQD4Z2sSmPF/BKuQ5nQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LpdRmMYH4gdKs52wqPlK6TsP8t36Rz9etYG+uFXIxoYPOw77GvCpHTnPEq4wgKvtHfjSBYM58T8o
VFR+rx+dgG80Vv61h2/ALXu7WMVNRnj432YN7jUfiNGlmdGjYf7j5bb6jDSZd9SGg9hOG322ua8w
FL0iNhZ1+8bqOC5DHZhVoYhtH7wentMTqEBB4I+Xy3zK2H07hbY20A+hZ5iviyCzHMtmQ5LCJzAb
8LeBnGRdOv8ntIJz3n1voQKFpamiYGRWqDwIHC+A3vf0VlEiw8M53hPC9SjoIQqQxSnkzTditbkH
fDStRcfPfMIOJ9yoREe7QoWlh0XCwpflnMvnNg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lVj6BIGA4HbU/emQ9heAO8QUQkFZDGJIssRtP6G86bTgSRa88O5ptZ4DZUgHBTgSn1h7hUaEv8+a
tNMAldSZ0xOYsVz9BzKaXlNxns3kB+Rpds/SSPADg1ubjsV1ik3hSsyFsZLEFJdM/USvuJx6tlqO
lcvOhJoE25XQMSQhHw5G+Y3yndIsRoZXoEA1xWNbxtqIWuS4RWrbllSdNtegha0g0dtcdwyv29ZF
+2IXIX2w2HWcCsaK/8nqc0KCPYrKu09h4sB5NP9dretssPLHTt9OyBm372Jqp26BBHaIyCAkz8H3
ZE1GQphTk2LTGkkP+We/+N1N1QcRoBrkbdSuEg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
G9Z6MsHT2SjuZxrNLogMpUFzsiVa4wbLJY5S1Ytfi5iMVl0+jefC2h89jUHygHvM/g20CLVI4r8z
iKwGq6+vAoAqLDfQGNxWPZK500V8zp9+8AZD0ck3A+sCVWDyxwimogIW30qZTpmZKm17SaranCXF
JSdGq3svKmZM4RU/UaH/QG2cNwMwX/O1Xk+cCOklb0cKWq8Tp1FRQZGHgdBXrBKwXhgHwroI0wLn
vwRsZni83d8y8pau7lejqu89bp81D+gvBJE0MhuTwdESyjwe0wtJU3RGs75+d3ko9MOP9lJ2ok+D
9hYJFC4rQYsw4IiYBzY7ooeFA5rQdywXzEGQTA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9344)
`pragma protect data_block
ydnLYDFV1zKlSHIfciNKYF4/xOhOjYa9ruVBGwZtBY51WkxkWIdHxapaQnnlY0Go20Nvgts8qpzY
tLR6nK5Rik7m63nVb8lbkH9Tw9rEY3Cv7/V4t2x3iS/Ex8c/n24QbOyyVpMcNcP0ZU53NkUaSQt2
1/LLYiBXQg0y34Uqare10EvI7QKNCg/7FwNuMuEj9nG2rJ7snPVtNssqzfIL9gn1CgbiB/ybKIO5
lUbhTP70Pfmdu/M14tk/CQNRiIg+FDt0pbo1XwyVOZ5qdwjNQl24WahGypkOA9h90SK8HBRSUw0m
aZ7cncMILbDSLl3PC27Us9j3oQiExGqSq1o+kLvC8CEkB+NAbAOyWcMWWeic9jR8rkYzW1srAT/K
n+Z1gLawu2+mAckJc+6cV8jh6vkyYPiFgMMUMDcn7YcYnjKc2ATGTmNmVCW+k9LxVOH7OX3gUkzC
pMeJX5lNH+OuHxJmzEpZ1u1LEczOvDSiiBSyj3JdCaGTP4OYEBBiSeR2rwOqjq6jL242hS1bhHm0
F1nlp/QOGqp5X1ydvbbmhXhA5cDguJTu++n1sTZirxNy4yxulOozCIy5x2cWeJ9Dm30+NQNkydSQ
OI94NBNIcT0QCx6CN6aILDgOi7zb8YlinZxjA8T/bUp1JaKn1c73pa136bnRu3OrwUGqb8CniCuN
m7rRyOtjsJISJTSl0T7oLpFWEsRAr9oQkDy/EfSa/mwqBDkrELo/9b+anpXWcjVp0CcwUcv0SYsc
+QWmsAx4lFYE6fv7md3YOO+0hB6KlEsbVihIQjuQ2Pti7ed81GoJqUq62lkP08ELxC852n695C3W
7ygwetgc9yy0EAaeZDB5JDycazf6KUy3IJr8Pe9f90BKvISMy9Mw9Ml8m4mOgzwmIHPlG1GIQB5T
MXv6GVQKnu3H6oiPqBh/HICAohnUgDdUmNyzpvWb7UTMl+udw5kwmsQcTwC8/WdALK9ckKiktcPe
XK2v29NMcqI2lf8CkspBnfaQkpENB1whphJvn7eYu7QPMS4iAKu9GLUl6pKZmlVWmSmJQ3I0vTaH
T7+6TyeRxaTRnnVFbNyWECxN5IcNbBNf8zFyayMihNhootR+2qXhV2+se2k6CKtBI5M0z2+aQN0n
JqF7pORiBpOyoSPma+6vL4/88yHzvzqWxo2+KEk6bYuYvCqWjj4JT2gCIltJ/39hOJSRa3qJlocP
yqqG3CsQQCUjC9J2Y/7YDsiK+SBskd9oteICCYVj8JPbBBbYww8VzPOSsAcLrfEBiFpd7wWs5JEf
lK6/JM27TbfcjxBXzWVHT+Lv/cMUHe6LbCRrlWW7pu099mtTXgOd3xthLHrZhHEJoKnHxX/5KZHe
6FeGsCz2JsXsaJtjfW4Kt/dG7r5nKxWxwY8oBG78Lrcx84OuDJhXmT8ZxZMXINOqXIxEga8K86wR
n1r+nuKZ17EDMqnLjG9BkoqdUHxY8PoVc9d6brbTfCjiCaFCuJ/F/6iTEdP+jNpTfHR64ANyKLgq
HNT0jTknF4jztalx34jBgWg9cSUacM4st/XLzdMwTX84CbqE0HvoL0aIzUovza7UYCBd9UC/SJdW
0drmzHWSvoY9IJ9T4qvQKRPwN5AtJ1mxll70bnmES7LesZlKaww2KoTyD0CUOyeP7rETxzzDX3m4
t/HlXYTcBCQPlO8xdR/JbL9QZhVMz/PtldodDbNTcboUjru0hpw0Y1V0XNId8UZR609p9g5huYST
NzvtrplCfrsLz1DgIOZLSJ8E37v0+ReaeXIRbwar9n72OHI8qBU5GMm6TMcJx/5sX4xk8FWBCcLe
Wcs0R7py7tPWWcjtByhmDwhcbbhlcGaJDIKTP+q9xU9scfrR1VRS8Z+yo1MdIJbeuCD6S1g7uJ/p
Go5zl9Du5hi7Z6dsQ193/1wLoasGUvXICh/XfCPiA3lSloZeMT2u17qgP/KqV8Xrnd2gklHai3IH
oUt565X7hul861VojbCI2mKLnvT0xIqVmaVzHuPKQcUCVr372LLEHAIJDpaghedA6/wzn1+2v5Pw
L/KTYfXqZTk4gpsMNZd2n6z4GNA08ZbGsnIdSd262GnUHdBj67eezibcgteRkCDl6DZz4wPQxVwj
BQnBks40GqAhzSuWeRz64BlD93xLUI3g/2Oazly7EWdYrKZ25m1ZlpNnaBjna8kMfcQBu7UCkFEX
fmjo/t0Uq3UT5/r4hyoCSBCo01IzDfdT1m/Pc1a/XQqINi4RzWLiX4DnJ0TjicSIPEvErKzQY0kQ
TvmCGp9Z464ubw3GdROd/YfNWDztbtTcizCEpTaRWslCpB/QA6p4mv58bfzhvAD7LXRD1St3bBzy
JnUp3cHml82fsMov6TcdaC+x9Hh83aYbd/K9xEy42IKX8A614xZ735K7S26+gTobEFKdvEF6svb3
JK5AuzYfYCQV3evrYlZpPTFrTva5cZ5k1pDvyQG8f+X/9tLRGtHZcx7KUsZkFHFMYhMIlzcEBlzm
tF3E5TCPWFfXBN4LVVTzHFJ4+3XZkkXzkNAFNoUKT1wupjveojTnhZt6/slTQvVeagUcbxKPwtiJ
xuvihRBIYTLFjkphgL3s9Tyee6AxwU48obH5uJMYrnMyJ0Lcqik2YWfkidEm5gX5Vak8qaLSPZqB
iqypoSyiSvOAhTGvKWWST9qa05w5Nn2x78TIW6/VI42Ol7dYxnqQQtgWklTDiOsgTaMbZgXnEwAu
MMexBaCFvxBwjSl+RZlKgwczYsg+ra3h/u6pSDyw1DJ/KmuImYhZNgmqajT9C7vprRqx6pV9yv7O
F0moFcv0jqA/SoIZp+vCcqMdLZz6BtjPMCeWZm1Y+3p49crlRvPU32rpAfJh0fTRUKYO053Rmsrj
u8H3HURv5QExfb9NqGxvd2tdA5PdbeT0si9TGqHejLpe1sjrPzYVnEokUq7pKq8mIhT5szlaxoI/
9SJskDl30+6Vn5WIRcFhxKmrqWfVC63GevQ3quLUH+724XCFdRTBAjRmsxZwE8NguL3nI3lLe0kZ
JdVnKyphDkOg/A+xmHr5zhP3Ma9G3QVaGN1SPWe64Okww6jKIkdrRxdxZhZ7U1yC1gma77GtNk3g
VqDCGtswmVFfjJAP6KPPQynKt4frOA/5KfkjAl8TmSYAgFsxtOmMhRSgMPpLBWdHRNt2XCE/o7eu
o+IjZCREIJs652syXZMs623ySAYXwECfGF8lWRGV+arIQnvrL5WWbAdumz/GjuGXwkWiHh3sHKpB
K36j1Cq2XJlo99+lCVK8btAjslbTd8NvUloU0cEbh4T2m+je/iknJWN80Akn5zXNQ4svidGTfIlv
iQjCKJ8j9T3L9d8YcS4reZgXyJ8G5ZrZJ9+FgoOq0OobO0IOHRxuHRna01IuVwVAQ/vx9u1Boa73
Wdrsc6C9ZcAM+SNdeCblX6Cnj+1zerDgUsZRLBArkM/RElaFI2it1mS0EYkeGBQ9T1k4zT6L5dS7
bkPH4nBGSXEB08Z8JTQygQhPfEfE+pdiBWp8+dZXsTnC+fvfBw37YxF+PBhUoIyPkQ0TCsx/cS7D
UKZ8uUaDUEva5Sif358/KSDyr9f//Xf65xjEvXakdPNx2xsbDdCCBsNiLvKIJ1uzputR1qm8Vl+F
kk64OCgLGi2N9XLWbVqRSaFzWRvm5JbtPP9WT+qTwWGnuSYVU/u9lecXENPo/HqrwLyDXwJWXQp0
cwk6OFbDIa5U16ULKPfgROfyynyvTg/o0oGpO2aWWNRL0fpx9e5hVqqinBLAU5K+5gaosr/bhYyL
6yoAoIxeSDGm8TXRt+H3fMbAI86yPmSWgVO/TL9YyPc3HTsQt0sCnIEpyXSi4H6iPJKkudtLMy58
8k+86qtXglAYAsl7a1cfDuolsS+EXIpv45kR11xJzCo5Qm/1nqxg0bjRO+xMLQiq5lOix4n9uayS
4zvjxcL4A0KCCxM0Jyrdt6QMii0/FKkXFrVHeC3i4OVMdhO1rHccMabiu4mn5x7pMQIn8irDLoJP
DvfLR8sTXUf9i+TDEhre29HzpFkwosGtZOX3Mv6Oy4MMwkEtveVA1Y7x3yVKXP8htMqjbloeFraX
kuwZ8eMIJGidsInKheZCawHTnNCN/8VEE4KFCbrFnryC65plnFMDI2UJ7Fm5icEaJ6IqWPju5Lss
0cRUx+W5/QN8lxqnETOBFUqQ7YKVWQh9gtTnv5LAbUBsF6e5gDA8BGG/wfors7FK+KLfFQAYIHwh
PKWRjzt1fuRWPh+RKFPU8k6EKv0J3cNAfeJisstTSwhtSD9L0t3PIV/O/ObfzYEffR7XBjzdMwWK
566/0Ld0gHNFA6kyUvWH6KLKVSFP0HuxGdF/yf42G//M6CYLbqIWObrrXy0s4nfalq6gJ3tpVpFw
XcfIdvKazW+8emczGtjBiX2277rLAziR9Y5LGU8bZn1a3RuHY+fVoTzdbQP7iQL2ucjQCBjtM3RE
JM6d9arvpt9v2uYi9TFho++Z8t2wqqz4uJIVRx+2dkmT3iVzQ8iI44m/oyMAYpcrlYH2db+Xmq5S
KRLJMOo6TGHdb1qer8fL9vd+qtKuHTUaWs1qnPBZ/m2Hss9Y5bMmOamWqC38sTBe7Ci6CANkSeb2
8P4ASGt2tFzgeJBvUlauY/hH012qKJKjLCIfH8z/gIsv8pCa1Y3C1sDAY/S49VSXLOrXI+CjKLP5
F3ePZQhBkmFb4eO0Yurc4RihWwelleBOW0MGCatTKjvMHpjVZU0wCvAloz0/01NhZg5cGweg9xLL
iqeSMe26/mmJ4wOlW9VoG5ye9FghU0965dfxbpN7nnzcA26THYV8Cqva1p/PugkbGS6mUs4lL+FW
8yKQcRUemj8Gxfvh9lfaRP2CaU3jmUXgXaxPz+DB/ARVDzPfVh2x0ML5hraxP6gVEOxeUZPV5JGx
nAViHdfh4QjvrdGM/wep5XgTBq7KpB/6uX8iLZi+rrYWkGe2f9FKB00Ejnzc0NySmdBk1WaOANyV
89nM+nspALAMk0ZnxuOR1sk5yLxjpS5WHJiAuVPG66okC0/nqRYtWmir2llXpqc8B8BaGXegYUxe
05DkudNRxjbwAy/HhY3zcGgzf+vU68v6/4/rlKNDyQz3UfVHVLZYNPUOSlyef4O/CFmT43eVCdCa
Y8r+TzPYz99hxPXft+18R2QnTSQGlmgaat3I8qS+VCo+3sQe3XW/aUaDKE913QPOeH/BQpRttmd8
1HcB7sq0QJQ/M193sYICE/zEgExUvLfdLt8f5p3PbOb6gcqPnsnTqluo8Z6WSJyaGDveLZ0nslcG
VoDt/myv2PoFosDqFOGksjlO0fc2ow5en0jNz0xN/xxIXasu1qFjA9jZ1W5XgsFCSFd6tZ5RT+8L
xS+6lHwcIiiyPaHcu3SJpNQAT2KCh30VUwSiTaDw9gf0w+FbEJxwrnAsEENyHgxaV+uwH+qcX+2I
ZnFTK5u3O0ibccG4FmNnGT0X/YjDGo71JniUxCWkIfmrNrsBQV2QfVNp3BUiyxb3S5BMP7bN/mqC
7W0ahzFu6F+C4vyu25u9IHkD6hstXO9e2PNyzWbeD4LV9CM2rWiIE6DD7cwdoh+eFpv5Pe9DUj5p
UB8jDdSSOBOB4i3ms8VaIuitoy9Ie2KJqLRtUe9p+B1ewC6Mm4beqFvlopBGHKw8/SQ9NtLHxyh2
EhBDXVBr12BXz/EucQyRFZsBFv+8WMEHXZuQnUFHmockzy8kdWJ1dk/qx6s11v4y3kS9sDd4T+Tm
ZlOQxddxRAvoCqxSy+sNVijHpkUqmCNiJKsWQaUtsAwlhRW8YGlA3PdEPb5hJpRJJAE52DhVuj6b
NYFK1arVoyD7VwABSnfm9Dn5gC+OFX0YFE0Bsq1f/269+vpjfuL/yM3f4ulrXZlNeu1ItIlZz6C9
bkGwQj8GgnWqvVZKSRZ4iG7cxLxvTZD1j2Nu0eMaeZ+6bjcPfPU5J39bZeJMeUtXEGJFhQNq7Mgj
2Sh5/YXB0wBQvWkGE4gxl69yaE1vD0c5WULOYSCE+7rJW0l+N/xeNTrtgWHDq0Cq/QvNRqMQpJaB
+Qy9D/4BDofTlyQrg23RvP6a2fsttOsV2KhjctZyATsyqAIiyLl3MqDmR9HxPoi03GKrTOqQhxbt
SfSLLDooYeyK9Mx1EuLKPJaOBTE0hOI+pj1wJtvPvwnhnpLt6dkyAvbnspLbZ8lIuZM0Xzjab07J
9H1FNmczfcmroe/uIwJNa3cCyHenIeFLainvSTMMUYbQGgkQ5RKvKqQhpgfI35gQfCqCUJdHy09k
Y8GvJAn8Lck8dvAYfQjBD3ukswMeLpOPuiIIr+fQtMAiLNZVpYQuwzXYMsUmGk5maCsUn4Qdvbdt
+YO0jsYlhIss97B/n2Kzw1RH/VmyaHYmUFedH5TNoziycL3sdo7LdjgtZfmPzeOwK3swVZhYZn5w
6l90aJ4B7ksHXN+PFVin4cAA8NlmFYGr4wfYLUP9YyJ4kQzFN3jAh2hP0+czvK71tyKrr6QIBeld
qIupkcw37XMXC4BzDEsZZwNHV28q+3nNI5GPkRhS15e7qIBqpEC9uEQbG1i/r/7iCiRjsH7LrXTc
Re6G2mYGtpjTJltlCubfh5Kr2BRsm/jRdqUCMkKqVxfZVi+gXF5Ln9xvhms9C6kp2VH+xzaGqOb3
xvFHOLgSV4EDcZTGDug9UjLYxprtMOSnQcQ7nL3VHwFGH9ZNA/6wWyF2PoZz9Le/crRY83aTmGpz
usUWv9CzBt6CK84tNo5p0ivsP+gdWwVG+YmB+dy5cdhak99zHVVJjjVDSD5DlO68MgPmx6ppkc3h
JgkpeQcCUQCji0E1wMzNghiHam9V/giDJUoJNm7TyDHu0P2ItOhGjX0tsc9X/3p+IxHzHi5LZwP2
L+bBQL7BVmMBuKUb+mC6uRgMp/5oMbm4qRx4wSjEOvUZvH+wr0HKpK85lCfRtF1v9W4BGFwTQBQQ
pdaHOVAfXyXdaE9Q9wVlc66tTKv7pVu5u5JBPh1YpG+K7JGhpKuwKTJEIWRbXDJUq79kos7ifoIR
Dj5rQHLiTfMFWdGy6/SPF0C1Y2lUVmmeLsW0F+TvVg0PLrVejhNqzMZG3W4d/6RZY/38JUEwbYCC
knw6ZQ1ooWEzOf0/tpUhNNpDVdyBPv0s9UOn/fz8h7JhjQwHfFLZ6q2rHiWagtdPU4ePocErpnYI
657T3GOT/x//WpvbMujvkrbxNvrr7iiUgdHXeIq4/SOEx65aFF6kA16+qrl9mUbbEz1E5PJkRtm6
FYn8wR0gn6gKql0HAmNeS619s2TNaoymUy6Z+oBJ5BfgN3T7nGRjTDvS65akTRVcV0da5lMKTm4+
Q2ihpDr0mwvHbr9rWntv3rb3MPVlGELS5hjIqFcghiAeSQiJIto6mBD1K719EFedJfX2a35K87yd
RX5s/5bbZQv/QIh4Y8o118ZSZfCoMHBRxm7SdPL/ocNlgPPqy/2Ip9+2E6RMsHHNJmWLyZyo9IiX
cMQNwwqESFY/byCRL5ceLDY+pDqOidpFLKGBgwHEKbsHShzvIUNvyGtiv8nMtnmPcXzVlRIWYKlP
dSu9wFeAVNyaAyS4LJIP0+6Kg/T80TEzwefN+DLvkUpNNq0VD/Sg/yh1BrSg0ZLpvtqkSKusl1C9
gH65GltgSgPb97MVg/NWVPDkdnfnx60hfA7zYR2aKWH/wIbmer4PkWsF/APKvStjz8jBieAbskOH
IxfHXBl1NXjNyjfJxKRWXmX3HNSHQ06oxdTXCgQa+4kgFEzvpByZrvJ+IkluQ0fndPhWAcJjsr2C
O3xB13s5Q6R598RU1tI1gSa/udBV7Pl1yqkSY0CQ21hofS824+OWu1sszBokNxGcuMJv+m3lPmo3
dn4Ac2w5tDhRyMe2y924c8kiRYCKttPfsNzf3IuQsuYlYckH6Sg28ZzEETPY9eImByNBswlC/6KL
yLzEuYfE2C+Tn/znhHEMaOyccBw3JrX0Rhnmx41h7+fRWMwRVAQFsjPWLaSI5ltOY3yPkvXWGj5C
TwBO05pI2+GQRRxDyx2sQKGxMLtQ5cEQlR9W/5AFdd0knOYjm8qV8k4XzbgWKQyZ12Qqwgroh4Xf
b+tPw8uu0co4D0/2iMUWKhguAz/cnRbMuZiAxCyQJ58pQ4ejTZ1iYpbCatLZKJxk3CD+nm9OSuOO
eUepDdw/Y8bW+8ULCDQu25hX1aRrKeHcN2RmGj45XOwJEPwOBjWdkAOeD3B4GvV3SBdfOZe5ZDXo
peVU52d19jYpCH/gGE3sTrvcghzZaW149/EG7woI6Y4GGnlFhOs2z2CPWXvFQlRm/K5wzN9yDOB5
WVqUaq7bfPvxd5eT6ZHQ+yU/mF/M4wXfeKXEb03tEhH9Ev0d2I7CatoSg5jq8I2R9EqJWvk+Q2IN
pyNSEAgLQc+8Fe2ltGtALSFHhwqZCB5w7qdqh32kIk3zW1WPxG2i35sezRtkd6jQYaEGGDaxbMpq
v7iwyDJCpOvI8KZ5oyGQD7OrW3GgVqPTyzaowKMnc9bpUU3kb2uqcX28nlxKv2+b3gjGksPaUa4L
w/gmB42JF7BCTPunG4vCCXwKgDHJsNN6hQiS7mYlHBYw6PLdJZ0Xmhrt6O+htMv8XvG5LQLHHNFR
BTUez46AZaI7aBGber9DNnx2IKPB5NB0glKI5AAWa7zkLsB/k8/kkt1YjtxLVtfrXaPngRhwHPLI
D27Ilf7k7sWyU4QJyfE9hocWCbc1mKq+doccWiL9EkH0y1oD7ntEAiwyZR75yr5LfRXrQo7Q4Gxu
ZICnJ+esVcMEQGdW5ZnhkqYQZt4MHCWsCyVTSVAQDRZ08q8BZxxVblNcczUxU2BjhsYv+QCWORIZ
StU29yUgM8ceQE6j1bXj+qVhZ1TTkp1uoXxBIDLy5kffkgG4OQf1bW4iRPcjO85eJDpBUB4tmzLv
RzuXKcV8VwGQApUaZ+o8lIO2zj1SCqI9cvdRf6nqMG/kdaHxL6dXy3VYSvlwsCiXQARpVpQOFD8c
x6kRzr32+TH3Bj5Pb9UvyJV28+1FWBW08xzdz6bckXtyD7iadRcoaIxyReeaMungWuyKdOtrH8Kh
VDfLYkmmpiRtEDeFMvT5D0M4m9pcH27G1WAmau+sObnjLULD3fX2bfR/CJzraiTah/Cp45cesMp2
ehyzq02R8heouEbKAWys7Ic6IrTHEXEBod/XlPsqBC88Y4k22LylycS200+vwlGDMFAPsJczRqTZ
UzK6zL4V3Wg/y9sSOgH3Mw3wh+Xk3wGTezcrg3wB3YL4Mn4EjH5/z3tHGguEbceMbF5X1RpEML4s
SRJe4pLz0QrfCBnaxmERyyDlmzQ1A0D1GnRw80YDHuOkXjRk+hgRxiChEa89Sakr1ueEYUqXCaJ+
nPuIAnyj9SuIosPEc3V9FPQRCeVsQ+sxZckOuarAW6Zo+5YU0T306fYPzU9a+MKteHsfzTbDCbmR
WMq1zMi4GYI/Vvqu69kckCZe/pvUxtbtqLtBiLXousJucpoRdqscPAa8duWY2IeuMOjZJ/jpNH7x
FyZZmYcPKkHUwtGUdGDVofyzvjD2lRTePAwkYRy30guQrwVl9ijK1X9TLtyTm8NMeZ0kX4OWkhNp
/Wd8wAyLEegkdvpSPNBgqr4q5mKkaCsZtOD69Un95yWQr6yJvj4MDiMRwiZgNSEZzrJNYei+1ZY1
A3vOdvr1u+x61wfzrX4+GFjaXgaKruMOi0e2vDNpYSsL6p2zE0CqbSCTq77iYsKYC8z7aVcToH0n
luoM03vyq3ooYOZDmhG+HawFmZkEl3/WPJ5Skw67rNvBS2/TtCn0l0dMP+lL9sjIhKOlRNfdDJOm
E5A2SfA0pMmKnLZlA9yCgAYtcjKHSXj13M3wftnO0IVecb3ydg6r2crqw/Sx25E5F2f3POY3wUxC
8f4IUlWU6+Lab109jNCb4/XTJxXiS9iFxoOng3P2/FqDOPUmJ5BRbewAe/K94HAYoduOLTZ8eDsM
UuGMFanBVLBRXy5C0OhGXiimtcctA9zhyA5LKMf4JRRGqE2nc7fW4BgRei7CukagJiAv2PgeWdTR
zvI03Tq/PrJPaQM8/PxnfZXRMEHEp3E7SxOP5unp9N4oZvwvu3O0T+Twj95INnINHMZKM7S3HPDm
AjogovLfEi1oX7va4LgEt/gnDqMR3LiycynGWvPoq7bMSED0D1IL3Sfi5djZY4FH0gzGqQQ4uNwQ
4A1sjtDuKWN2EsQfKNqvuOXv6+YPkXVRjlqpuqZDI794sDyWnhZ6iadyFJDBdaX4nMwk66ghKgDW
2D4pcfvYdkdrDh9H5e0mDgNIkZPd1bDILqMPeR6nZCSGsy0A3zhEoYcWxw9Abh5mF5ZUmledduKG
zTX9H3ekDnCvhJIdYFnkSf62cKFcYcAuGKg64AB8bDFzac9S+wnW9wRc2sdtHp8QzKC594or9bOs
uaJltJ6UAGKQtqXwZOUAFzsUS28Y2GgXx95LoWNEvBMHdRbdLKs3Sw9unaugN5pHy1BGmpZaRDQ9
twOxaACwI0mVKiJxTmfIv5kZ8Ogo4/d/mAUY9IwN+jW4A3Ibl937JYFT6OXwWSSKaicnTQmqx3DC
Aj97gIADG+R0B2p4yfJ0FzgFIQ2oA6vMypfENho5YfpXbVPo2374rk64Si9e3wt/+W16ocdrpSbL
nM59KpVun9OoGzdnFMUZ46VelAMqlmxsUinx0ugmfKihpWOzvkU8OqbyYraJFM69thEaJ2uJLzOS
+eqVjRclA3y9cfw1hBLQ0CFEgV4bvGPBp+tb6dRM7Bw4+PLvpLnzfJnUj7mYbgi7mrIvKmVx/4/Z
0gbNoe5LklHby/Wpei1ASnoMF/Kx1unvQ6oJVLisbOStYXjwHiO58S8+6ryQ8YjWQlWn60l4mRdv
ToIu/SHtsqtuaQQc7T9hJQ6ZQS4dqByry3XUicNsM+Fi2RQc/p46lCll9Xvkih8watpk0BvDNBZz
EmgpnRV68U+frnUoHLluM/gf1BW1MkqtN9w7fiFujBW20NjydWqY5E9uNZdgDFI3U1xXwPM7MDwd
xztQy40u+YXhuU/8n1nldRHLehhCTiARCxpOfiWr/MPyg021xGixqXjPxJl4mRaYtA9h++50h70i
OX+ay7MUoeU+cjcuVpEd0G9IUkbxhQYjRVZIRdKMRhhe5iIIoj0EyP151ZhqO1Hwlqw1QrpuLdk0
48IVaw1YU/3ojo+HNIdk5MNjpz+btlgbLrS0Wk57lw1M0TmXJvKCchC7mzPZbklna6EtxECRNsRa
oNcSO65I2lMticrwgC9PPtiNlO7HFfWKgjjvaw7YB7adkswmX3Rqlef4B4mpTuiqHPA8KwkIfpFA
mIhCAcERNqnH5auGFvmXijyeXbfp4UqOqQpK8zI/W1vx03EGi+XzfJIg3oyKqCPOxkWneTABt4r6
VbEFTC7ahSW3cz035wxe76lUQLoRtFv15ckY3TuHgQb4GN62ttJjwZAxw5JkxQ3q4k6f/z05Ke4R
B550955KXFcSb1EpZU7giYa5C00N4LacloNMemW/CPcfpBJ4GINUTaz90GtYnmPwzDLWIadCrSOZ
QRc1RMbxVnDDTPN9gR/xaxExe04rJwrECMEGDfP6ihNfFN6P1YoShxbgbmQjRXATXSAdhdwibBhF
kTznLi8k0lFa75JiVcipNhGo/p4uuaC971epRCtrpdssfkSTVJs++ZqMry09s1ExvcroavJ1a4v3
on1msFYyMXnE6J0mDSUA0VAzNX+t0o8hu0rIpZA5uEI/A28W07DceJEujRSuCfn5fiSlrZaJpG1u
YsTz6I8hLu46ByI5nMhe3AczYO0Of0HFmyg+RzE9GzMhBSO2jymAHFt4XAyD7988+P4fjkS31hq1
8Xv+PnpMrzVvoOqbd7AbLU17ZhFSgupVYNxsNab/q0hgFgLo3Abju23ZWkRnTlaMdHqOnhDzQfUq
Pahm4cqgNDzcFV+8Uc5rg7Lx52tMDFbWKP52v4tyXSqrpTjHvPL+mys+fEVLwP32CDx6T3sTaHWx
+rlNCgL7pGSxu8zpyz+tZk/krI5Erz+5sn6VKqgMPsZJpOLILhoF8BRJUReWEKhsyi2TXuoZd5vj
CdOTQ4KtSmSV6wTjxfoi8zMI5F+03BvbDzwQL99f12BBdPCZQugJMFwsdJa5JilussDr+F26e2fK
DFlMMjdRsysdyiY7utIpQ+i4Tg55czjS2HsJjjMpl9FqECLbnB1TQ6SNyXvHnJKVXkGcZki37+1y
AbajDcMugVnwpsaPG8FnEjnHWh6yhW8HMdC0rTxtKfU/srfW438bRllw+7d4xItVkjQEdOReCdFQ
oGtkQDNjOopa5See8nPCN6bh5P0YJGf650CoHT4/R0W8fZ/T7JUl2LerhcvffiKQJ25Dirc=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
