-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Thu Mar 11 01:22:56 2021
-- Host        : Piro-Office-PC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vio_configbus_registers_stub.vhdl
-- Design      : vio_configbus_registers
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk : in STD_LOGIC;
    probe_in0 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in1 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in2 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in3 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in4 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in5 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in6 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in7 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in8 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in9 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in10 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in11 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in12 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in13 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in14 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in15 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in16 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in17 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in18 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in19 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in20 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in21 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in22 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in23 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in24 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in25 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in26 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in27 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in28 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in29 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in30 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in31 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in32 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in33 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in34 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in35 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in36 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in37 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in38 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in39 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in40 : in STD_LOGIC_VECTOR ( 30 downto 0 );
    probe_in41 : in STD_LOGIC_VECTOR ( 30 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,probe_in0[30:0],probe_in1[30:0],probe_in2[30:0],probe_in3[30:0],probe_in4[30:0],probe_in5[30:0],probe_in6[30:0],probe_in7[30:0],probe_in8[30:0],probe_in9[30:0],probe_in10[30:0],probe_in11[30:0],probe_in12[30:0],probe_in13[30:0],probe_in14[30:0],probe_in15[30:0],probe_in16[30:0],probe_in17[30:0],probe_in18[30:0],probe_in19[30:0],probe_in20[30:0],probe_in21[30:0],probe_in22[30:0],probe_in23[30:0],probe_in24[30:0],probe_in25[30:0],probe_in26[30:0],probe_in27[30:0],probe_in28[30:0],probe_in29[30:0],probe_in30[30:0],probe_in31[30:0],probe_in32[30:0],probe_in33[30:0],probe_in34[30:0],probe_in35[30:0],probe_in36[30:0],probe_in37[30:0],probe_in38[30:0],probe_in39[30:0],probe_in40[30:0],probe_in41[30:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "vio,Vivado 2020.2";
begin
end;
