// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 10 17:19:25 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ counter_binary_sim_netlist.v
// Design      : counter_binary
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [31:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]L;
  wire LOAD;
  wire [31:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "1" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
6FcBDPD655CGvP5V4pn7avsV97xfSTo6DO8N+uJCMzqQ8IaAYGh60fudUJ5FLyell46pRsrSsgkm
Up8RFF5brKbLSWgegaHwGuYkMFq9GqxhPsLeyIbt2IISSxU2YcmFbMZQ+mzp90K1dApci83G1S1O
OzyVD0h39HKQn0FcBkfb/A3zdOgFvmQsVDyT9CSbk2EtUcXfoqZm+mKY8GdWUqpZ9jly7Qr/hqlT
edD7qjd0CYdI7JN1YPCqD8yQ6aGZbXlfluIno1hkt/LdKpHISDgGUqe10qSVjRiqtDKIXt1FLOe3
BO3WONLceObmr/xQuzeNSkf5lfVSq5PNZ0BFAhCHNU8K95tfYnXpX3stvcCxOwF1ZAaK478JIYli
WNN5/a/7/++WQqvuVZtD/IJL+Z8GPW5l7ONvW4/O8gp43h0eT8DYkPO2g5tZ2kQy3pNQREPSZKtL
5Xt/2pC+XyGkmYEa2xyEHqdi3by2qM0MEOTI+/wiL9H+hdTdFhkQePZcFfMnPe6jDYLOqhaKqvvE
gvMnh9A65h8uOenpcZAjLDDTdDzNw7t0hL3dtPRlU+ypSJfvg0HgP3hDfwYX4xSK1qCeFo9W5OIf
ohHSE93wJxT7li5P4BsFmDuJodxKExfTdfZPHPP7cJoNDTFuRXsT256AvcihvD7jYm09vrJQ5HPr
pXewbadKsu8l07dwMgmKER3zROgUodbECKNIOQvjboZWLq+7xXs7dReaDBP7NmKZLqMGluAHjg/W
NCmZpTFnIfUhwj2p22+CYGAn12zleer0va0kEuw406U0w9W76ZWi0Yf9X5coAaJsNZ3Z27/nmfxb
9I1XJN83zE2IsUzKtt3xc4The9/nuXkGqHn0fMOwadt7hcgjkddC0s3jFRRWhTZJREaEyWrtIk1o
PLr54dF8hM4wpaBSOwiVh3jjGFoZidov31XkvrcTRhHqHFg4zZhk4AjWYzQIAsYNE8OuB2b8Vfau
HPN2N7zu3M5IhTMYhzMqdyFfshXliN7P1djcP8+J7kI2HspgMwu0iLLKmgLXZPmz0HxwNam5H/ds
idy9ZxM+QNKt0a8WzMw21lLtGBtbKN3EzvLdWc2LjyDCPRZxBFXSlU7t6DgqEPBvhza6G2ofKImo
PdtlH0fXh8qMqiQJHd/c/L0aoRgukC3rz0FljWhS5h4b7c1L92XtTW2r0joGMloyWlmaxF53sl5+
FDoPJLcM/CtWt6zsWNWdTwR9LEJsJHnb96rdc7ohISWIxlp96OIBgyjunc2ghPdpm3GaJxCGH7Vz
hw+EY3OcrRM8+Qbr/+i3N8aBzt8FLyG0pJq26pEHiFdeM7oJzEZ66P1spj59ljuyn0/wCnEoYOYz
8ZqFcXz0tNTUVmmmTaBucBKJkj4WazdEEaIN6izGVnfRPVm6jRzh1jKF33IRyf0cWznDum1LWgom
JUuq7Y5FotV//MVPbiWOg3LIZnlqkIEGfZS4nDfMfAgJ28IkL5cujJdBtvxu9QOsg+Qv91o9/+kY
c4hCSp1NCinvyuumxQ9IREEDFcnKhNGmdnJTqIt/FqWeMDufTzDhIkQFMm/rw0/Jw5WOx5dz2Dn+
RPZ3w/mVwyw7vxVt2IeA4yjLrHL272SARPlenVjh/6wmtiqyMMy7X2so5WXU69npQqy+QNw9sicx
id+k9fZl1c/4ByRBbRxc8+9zB6UROW8x252/ybDjIigGjSccf8naUgCXtdHvarltyK1SILEbYhSv
YEYd9pEim/TCFULawGuJgUs/y1eU4CWROnzeQDJ3kXZs37U4KQ3pLe8HrM1y44eq6IxOCZHOUEUa
ZhPbDTTzqTLLNXJsFRy4NcWoCSarIpXmGrUIz4Izui4t9i27MOllBfTEZZHZw8Dww8rPH/3r4GL6
Ki9l7Dj6XZ2Cjae48Tic6wnsq6sMe8HmSEWiNSK2AXmuS41AqTn7dJ0f6OKIlq/isuHkUSGCiyJ/
neUk/LIrPtygGKwmu9U1KQV3LqWg30QctnRabiu40HHux12bQhV7gYFh1CRLjFa0+R49Yg6adPQv
aX4MwpsZhPMNcVlULfvWtcwqvE6F4A26FoyV9lSel68hd6xZJJ1553umUAoKc0ze//h2TpDYhrWv
2/pSlIh0b7n72a560f3ZWdvN7CtD5AsaYXh8/L9duAvEB8+CUICGP4lCiDIxDQ/Kl5HZHrQA9Ibm
g9PKPQLoG/Fn4GvIj+Yip+9AcXUpLRjuw1B9/KkvbLf4F8LuCdzPZloxzdMTVDIG8W181fP8lJhY
EuosgvA8p3RcEg5h0jVXWL55lz1SprG0L7XdeAAhXkqbBzWrgnkSOwNG0L51SrMYQr2kSZ6VQNMY
Nivw1V4Ycv6crDGJU4ozlFT2uTZITXKh0cP7IWj/Ye6gIQVwQYiUr7qb9HntmjcO9kfjLldPUSX9
sha/tKVScXe2bAxmUm5tzLQWZg8tNT+iBX6Gnaa9mai1xKBaeh9SkufeHGyeAOxBCnZBO53SlJJl
UJ2j+HYkiNAeaa8MrOuP+PNFcYYdqyyN5cd4C9VR65FtJ/n/KMsh32ksYghRrDXt9/FAYbw4DgwP
EJJUUUAvuNMc1C6SqpS88+cGSQZgV7OjKE9pRno5fX51UvTRsHwd6EYbPrAxhwPFCyyEipP9x9mD
b7N/L4FCVLEu+hj//Yb3geTrbZeJPj4F3CzmKPcfoALjc0kjOc5OpX93NuAv0ZvGL2kyzWGQwp+i
9oW2YAXPpCjMnw5y87mReQTVljAxdJTuqo2HSVsliSH+hCFG060P672EOwdJTYPgQ3lDBqdF3C+I
rT+yT/9/2XvORJtEK4fym1t40zYkKJD+lvYzjglnRo4iFJnm2pNAWuzmMcybolpUA1PuCPgm7YyD
vrDfMUsdrXm87xKLx6nM2m9XnbAbXNdqkmWmvNIE8fdfMFdq0hoic+vzYrWB1Hu6+BDyy3e8gaR3
hLy4BwmO549WVqOmyvhizMo0EMLaqwJjqwQ/uHEdDaN/Lig/5tCkyUa+xQzfXoCZ0/9wMNz13xO1
pD0mfPT7IgsXki/DYrkDXTrkiBQbc4JPxA/kvDqf2FAK2ZEd6V0ucNhLRoIbUvCahLz0D/pF4FLV
MniIJ5LIL6yU7tVihlk0QFBg3YaUgTdtjXHhpx6VCA==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Iv+AHela1zsjiYRHtaHN5shmNRY97pviZXEGHtpSDgz+MyQH3pfg3D2apzmElt53al6vUMViczdZ
uETqlrIJulc47Rp8pfelW5RA7xgR7jLheEc5wIYhwAH4RXrrWLHH3C51rOOoqxYHcFdkvpYXhTAo
04dDan5F+EzClqR0Mqfce9x1rheh+UTknh1IvzifLK2B0swWNahu0jG3tKin27RkluEWlsx07BDT
BTFFxBIXOvj8XiqCSqSlALPc9lEZFURHLt1+Iunl8W8TCI8wB/SZanEKg42SNPpQO+/MyjF9BG3a
AaN4A0lm1hgtP9aRzkMMo2PcAi7R0HWJGNsw8Q==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
aLPuW9GLbZ4umxwNvxNidG2z7+U3TZ9RlSmv84H9JkDeL+NOwaLyD+gNlvL+1ui6TXim16/W1oSb
+TkJsGeeZwz1o7RSzbA1GBsMr+BRjuwwsb9GV8A4WmjhE4cQfg7qsFI8zdPd4sNXXFkhMGpsp59v
qpk3KHP3YrkOZebaSQFuiIYcsRKpkczpucVnDjlykLayJeyKYssEbkdmCa3DiMjcvtx1W9/wXRMT
xtGMgB17Aj4XufneRg2EicX6mB/KZuqilsevdLuIU6duALRRisaAvZRvIa/kipR7j7LwAcCJ/2Pg
vq97JGdAXPYYKl6enhChd5ePjyjNZTouWqTLig==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11440)
`pragma protect data_block
6FcBDPD655CGvP5V4pn7avsV97xfSTo6DO8N+uJCMzqQ8IaAYGh60fudUJ5FLyell46pRsrSsgkm
Up8RFF5brKbLSWgegaHwGuYkMFq9GqxhPsLeyIbt2IISSxU2YcmFbMZQ+mzp90K1dApci83G1S1O
OzyVD0h39HKQn0FcBkfb/A3zdOgFvmQsVDyT9CSbk2EtUcXfoqZm+mKY8GdWUqpZ9jly7Qr/hqlT
edD7qjd0CYdI7JN1YPCqD8yQ6aGZbXlfluIno1hkt/LdKpHISDgGUqe10qSVjRiqtDKIXt1FLOe3
BO3WONLceObmr/xQuzeNSkf5lfVSq5PNZ0BFAhCHNU8K95tfYnXpX3stvcCxOwF1ZAaK478JIYli
WNN5/a/7/++WQqvuVZtD/IJL+Z8GPW5l7ONvW4/O8gp43h0eT8DYkPO2g5tZ2kQy3pNQREPSZKtL
5Xt/2pC+XyGkmYEa2xyEHqdi3by2qM0MEOTI+/wiL9H+hdTdFhkQePZcFfMnPe6jDYLOqhaKqvvE
gvMnh9A65h8uOenpcZAjLDDTdDzNw7t0hL3dtPRlU+ypSJfvg0HgP3hDfwYX4xSK1qCeFo9W5OIf
ohHSE93wJxT7li5P4BsFmDuJodxKExfTdfZPHPP7cJoNDTFuRXsT256AvcihvD7jYm09vrJQ5HPr
pXewbadKsu8l07dwMgmKER3zROgUodbECKNIOQvjboZWLq+7xXs7dReaDBP7NmKZLqMGluAHjg/W
NCmZpTFnHMmv0ITiAU8etEWPLTKvtx83nW4j4xDUlACuHWvc0zTuOSL/tjUTAyGc3j+94H2jREE7
xBUmnIXwsrSBvbTKoWg7+IpDHGjN1py5Dpy4GGNnskO0Xrs+xSJoFdfdWKvkK1cqh7DAoZyNqj6U
Xjwq4TEPekHY4arMXfptYqweI0XZXLJfmr/BzVIuAGf/i/ndpUfD7TyoDsSL2mDp61utBHs5xcjy
vJZHqBNT/misCPJebEqdub4lkucoWevFDJiLpiUqVQyY+znLYVjIQ2jR5uAIJWMLJ6wc8Nm60uWt
gFQTB5uP1gWVgJGrrir5mpy6y6F2Q+fLEaiD9lC1dAxF6Ba1R6m4FhdI+wCVxXD1Nm6mb3mgK6pE
1MdnV9qy8z8vey6wJxctQvPkrMI0E9fUoGww5FuozedX0ahD1xEd4Yo9Y6ILGsp9ab91F3U/Le6V
Q5Q6nKSuDzS4tH2HeUZJ6gWEj2w/nQVcBBGF8TLeL58yHMYGbcohN+zXvl93ff2BHOAEFEYdIh4G
pnSE0CgX254p67ZeIdx98NWyDaVCdOTGGSPZCBJZivPum1rBtWm/WFJo4QPi4MxGBfZtDY4i1ENo
VInjHw3oQTEarQLpPb5/iVm+DPCremIdiHm2Vpi/RvAGqvZw06ZGNpZZfWtvdw1eH+OOKXT++yyW
YkQKy1HmpLdjxurYqhn3Eyitf4MNqxUzO86TTg07vcDuE3SMPWubIX+dZDqeC+schrw6zdcpnSMV
daPpYyI/YXZhY0sMSky4yqvuOB0zRhpJEy0FF5db+UNmmGD0PrE9Lwd1W0mcgEAm6rdPu3ZzJlBh
YyCEiZLPx0sL46Yyn8q2h3cTL459DF3wgryAR7JrgcBtgCzxS/HFsO4zIGHC3ZqI9TS11gBfghQM
Sy3rwIj9pwrFn4ar5FmvfmD9V6NeaAKGcUINQM7Iz7vLCAlD8ZJlffS8vbZYDboNjUYwza+gT8+o
PP2DOgays1hMEY/zu2omg39YEBZ2ABZ17s2Y2SRPHRKk5A6TJHTWyhOXq5zWodyH8cp+xPt0NFgl
Jj1a7mekNTRkbTCGvW5IrpUnCnkrTfAduUm6Bo3bk2eApkGkxCjQcXZpJeTl2woRT3Dqr4TJLk2O
evbkGiG9HPv3Z02ZizBMseTMfa/zGEmAIfs6x20dHLhD97S6lJvrQkKoAGB0dCfVOxJ6PVuUsett
TfXHSJLhoJ/JnsszzC2MOu2ig7Ab1cZs0ZAIqXI6PMcyDw2gS2pgcRt8GWfnk2D10mhY7ExxNUWJ
sohzk9C7mqiFnEkc2d/FWAD183E/5T5uz7nDjMjzA9TOaFeKu4BIBLP0hfzRSoz1Q2E7iEKC0SL6
eA93ZnmA1imPthx2vdwmQbC0mdjEjqkXhV9Wq6liegf+i7vdXWOeZDe2L+72VXIvPFQ21T32tA4V
qmwD805cX/v62OLiahaU8yd/7/YgIieg+62Ko1bLs+xrKz8WfrNXQd+yTbYd1I7VFnpi4I1CFTjR
Xhzfl0K2jVh2DkT5Z6YnfAVnB4nWGBPU5uK3cPWebtpUntXa2+CnVPDGQCm4I6Ck4AaNtCgjvqiD
3aWSP6fdV4q+NWbsRKN/mvix0hRxo7F2StK22fo4AaOi4w6kCrfCWsqGj8L7ICAdrfSqE+vNkQYS
62U/qrX9NPmj6hwRDFaR0bwKlJW7n+H+TU65pCWbaCV3z9M0mqKc3dwJfhfxqGfuv0sf+p+vnIh1
S6gHkGpAp/yWUKU+TzZ2cI7LsYnuPiP9T6t8XM1vTOVsqD9Du4H9tmbjXSTCoYs8LG9i62tK9Ei6
LeMRIFgEPP4O3pOLSY0ieKSNawTUmXAx30+psiGClxF51vuTt7bRWAOP2xf/DBbWtBys7NIfXqIv
GIC79YPKuqhICabeUz3H0YS11reIgH6jU8XLVhuf/ZtP/DafkIiFW2thxldbh57BpZbVrRHKleZc
Yo0P5K+JwuziIxp4i7pqtIn4i6ItB+3QbZSO3CICXlycWp6t0HnlLUz51VwGr3TBVETYzPoMoJzP
rPJDr+MCped5RnkBAZpLds6ydEK6r28kOYVptV/K4BCV/VtwuDxM10Mw2F9SQKzhGgOtJmclJUMA
+NIGFhFV3vSS35GI/qrIBbwLNmwtHfC/Yh8Eze9ugJ5jhXFUenmBmm4E997RBN021XH4c0wPERu2
s1FJ6SrL3pNCsRdRZmVO0Yvv2FF0oSHVsVD9r4CrhQ0tIcw05mbBtXkpI2OzdBJXyGTJyHmIIpFD
2KIwqexWL9yZQM/s9jNA2Rg+2GxXfTDvhapAxqpuy1MbmTDDrnuMUVTOOqUo3yfSkwZbeufP28Qx
VjJ9+4tIBnKilrcDyWiLeEI+SSeLNLQPCsRlcENaCQDMXr23rzrRWCDAYlUiFqRMg1fqnoLfkzyI
mo6lOZs/mXHWuoWiH0yaGOKthdK535+iUAXen75mEzg5XVm1gSfhyiSz3/UJ1EA6ReEpEQYRO3af
6b7fK7rAOn08vpunFVAL5QmDjEYmbH0X+cVv8PWJcHpKlyN2TzJWZ4l1Rjt8U7XD7obmHqePS7Ue
NrhT5qlUMFy2fuOxaWi+HkisozPRHfT28HzIqh4jV1X67Y27brHKhvQg9llaKzje/MSU+1KEHrWK
M6nUVBhoBW2Pxtf9OPYYZ0+epeYBa6l+EmrnQSeRjGqDdqL0BdfIFTbfEw9P7ywOn0xcpmIrSfHO
r1Zkx+6M9GFO625aa7X7cL9ToWVbz2LZ4aE5OVj9L0rLzli5MMXMmuzm07epBvLezVxBGYD+dgWe
ud05e80RVgRJ+2m7SpqlxA6rRzsyunc/oh/YmFl5rvhU7qSQmcLo6VYl//3AL0qCqYDNjyfGb7Wh
RVcoVQWBOH/RYVdXtArr9ZYINpZqHxd9k9//yP/iRXz5zYIXjbBQ7Gos6HZlVVC3AA9B9QnCZcGU
yd1Kw3txKXxN+x3DgNaSqHk9rJk0ZbCcr+1lzDQTWozUdCmvRdEC+iwELe0/ClFA5C2nL4ZWzIk7
lt5mR4sj2X2a/fBSmF2UUe5WLbePPCkGJVloRHDCi6AqB0Kf77GVfrjYQeQQscjRt3n/fQKteGBz
QG2OBC8wpY+SIxOiiw2f82XYRKNHsyIbfQ10KuvMIZ1zQOiH0L1Cya2hA2T8x2SaBoCIU1U3FbAQ
hBGqolRG5Zfq1vJF9BhsmQAOHh/+FzI0oUoEIum8DXt6vRTFJmzKLfLYYyh12FNIYiC067HW+lVK
OAiap6tfI0LS3x5J4WDmAcQaSFz1/zosDPrVgDYjnrSCTcgNiODltvpZ0qZ98lXpB0W61cBfNCKu
DHyfiedfgPAMwSDzq9CeOqgtwArGZA7Ifpm8/wvYqEXHXmrOtGNz11wW5X4KwnTmprNlQjzxxx/C
7cO6+bA0ggKz+/ZdmQ4WEL0zdrDQosTRYMSjwhGXpR9govBJeIKxa1aeJWsGj4GEl5teRrxu6YuJ
YDMIXl1Biw0iJyVmryil+iKM6s/VpvSCL3R9vOv09Bn+Ylt4VSGHtoPRUAqMfXhbS0OYpQVeaBRp
6u5vsmPJ1N5ymvUx4nyChYkBxUxQKduUvVMJHS/TVq77rrWQx0a9FplkiVb21i8y3cXmalolg5+5
ApLVosNfPlt3fonzMZPW+iwi6VY6UCIGczaR33ho1fsdr1IRcVijTV3p7MEvtwNboPXilIlPHFgq
eQkCzwI9xeeXH2+2HPzTeL8PTL+xgUdUDaqKYbQvo3F3ybfvYrzgvE0ONCs/PW2JfaCL7sQJ4IVC
lx+mt5nD/Wxz02bb72bErJT3foheILEw0Mqp5BqIH1KRY7EpQQKE4hR2/PXVWfEoMHEBecvHfvqK
nm/26O0KK8tz3n4P2TBrZ4FT0YlXmK/IKo19JXwTz9tm/2C1yQyuDWSvn21ij9Y/Nl4xCSWYCYC/
vCr7SXcWivhNfUJx6Px4myoVy8U7pcAhnCiqwEWKnDec4R0tObINaX/26xG+4c4tdChRgLtL9Nhr
lQfcl5FtZ8uzXXIhZY5YaYlbOneoJ98AccRMeQeD91iXGfOw+gyS2KhCEMsrJU2QRtYyqnFXwJ5K
NDw7T5FLSqLEPq7slKJjJl3zQxKs6/2asY4gqBszeTp59SN+4dX6UxHAzlbPWltarVL4q2adDjUl
1lMrbtbl++6z5psLFQPw9n3hIeVZGpyndpOtIWuUQyb3DKoWG+U7gWsY67NoKvZjgR2DlnkJYYFD
ANeSTc4sCpuCXk5zc4OEHaIwp+7/qMq62CnWvD1Rqdk0yDPLyEQaohlkLo+XipfA45JTXu/fcsfW
ialbtimoiitmcW1H5x0jekvd7riPTUFhdlWOknnTTlRqnZRDgh3oOKAhK9EFY8TB/Jzkk5Ut36ud
DU/0pgJb+akh9iPbC8Ts0oT6jakJJTeay/C7ubD+uvUhnZ+Nev/YOzQ9badW8GXS0d0oq5mRp6wt
kz12R2RZCYdURqMeOUF3f4ggcgkHDyqgNsgeveUnU2Pek7YafG6ficfPssjQtXAsXkalEWU6pgZq
4WI1CeGlLi6a/6CU4waeyKlKMGP5/RcRHbrMtzEFFeI+yCOHOpij56k+pY3ts+gK3VBaLDkrYoL+
3cDEMiwaQ3ZZUpK3Kqd9r0erHDUOm3lDvbWb1v8Z3rU9X4DuVkRb/SJ/BRchL7bffr8Ga+fGy/vD
ZI7/VW8Y9TCVQmHUITnygFUlfRcxLyRPEe4JObqTTuoNrGOaQgPR3dclnAUw1MH9IPhvIbH8xUjw
AZNoBuHAh6Ptsli3xlyTNEIjlbI0/q/zc0gPExPkBnzIaDU5ET4n9ak0IaU7BgToomMrtJVU6hEw
IZS6Xv6Ljfync89V5QXyFpa4zmTQxPdmKAHhP9R2BLp6jiUHta7+Ji+Po28gHigYJLfP5qJqro21
kFw1Q1ce2pjYpb+X6c63o6nAeOztY7u9yyhi8u/Dm5eZ311qjzNwgRok6MUIJa95/sidyZYofXu3
bTmDXoe5uQW8iIrY98Y/lFpX8k+SXCbiAHGqB6mnJ+cWZ+PqDmj6uRWFKgiOQTbUeXzcRMVFmLeB
OV7fqIHk8+8i0bGAGAWvllq7y+lQXysDBjr8hQKE3fWdWOO7Vi7vWQIeKE+WHLyOP1yr4ooCw+Ov
QywmI29lkP/DKTmRjjIkjlKAu9gkizPgsXEeaw/UXgvbZNDlYweEeke2JwnL/2747xkdB1ZRkhNF
kzmpemHuy7ECLSVpR7N4X8vxjQerVrgdRMG7gTye5ulMtzMmlzqF8uBTfu4K3Y09oF6YkbNwBOIW
aMFEh0YIxXvMzyOlT4EiERYDLjUUTW5vWWTU1zpRwYjK46JQP/MxW1k6Hef4IBFZmb0ySLRy+D+f
N7s0nIK8Y0A4MgCU4VgVz0b2B5uOboMuQTdGcoT3afDvMdlSTHFqBu3qjQn3sx4dLNzIEi6ofCrI
BTRnXqu1QGoWwTqHMgnDcTS11RAkWhuzIdbdIi09dtDYRc34cYSM38ex5GDe5CXDD3CgaXUq0lYq
25lzY1cP/mgHw82cVo98Cnq0WtuFyXP2c1aIXeFVyX5zAClwe6T7GoTHxagc9YkDsvvfsaTL2eIu
ONP4dy7A1Lf5djWeRYvkjzTvZMD+S3N6dMEVVfMbEqaeUbmmgkum30GG5AZvS4W59qXNb0OE4k8V
6b6ICSM2aqwkbcx8LhpYXDny3iBWkpTJ5k1+PQDUTTz3lRUrebZCGQB4vp0EeWooRsrfJi0NMPkv
2FL+0OI8j5OUZirmlzSonLA+NEjFUllMQTSlB+FJBKyRq9m4qCfnF2Lz88/zOh6XWqi2RvOJz1ZB
i49sutZEWIrFnRti6m9z9vA2hMRY76lKPRSB9MoGPfaD3mmxDI2gJhxQsVJKEicRlR6F6iz+rsHK
GkXAa/xqTcP/yi6kFFIIvvxkDpFAZoDqa8VHxWvhkt5VJxzLCopYGEmie9jYI/AWgju67+IoE1I5
8IWfme4hZ5n+O3EefSbOSCJkaYgeoBnB/49pRxbrTR+fGEt6WBoeePVIOD6yJH2jqos5jZchhfal
/MWuaUFlMDdaKIhtWmzxt4G18vKFDn7noW37lpxwB6r3oILF8X2GbA988bK+x6diA3NVsJ62xZHV
7Lcu4iHOzQ8u61XKjEA5gsE1khMNaf/pZs3cyfy6aoGk5ZDRrQLPFDTWQ1BNFosQR9RvkLyFyMqQ
KReV+qbgRxrPX+ZfTdbWPhFzC+l3TqQvqp5ydw+1UN6Z7Tt/Ooy9XpgKUZhw+HjR5gdCjFmRWLbH
SPXt7UodUxHpIz7FYB9sahlRBf2Od7YiClP37fCNB0hWTiZN7spoZoe+Ibkh+itbNOb7MnyrQ+Am
OZFJLm7GTfxx/kexMNmhQ4GGvdu9uBcN0id3LNSUU/ixb0pSqfULG+VIJ6E4nQ9DaGfzfrIIc2iE
xRLyy/NAGdHu6hmCnf4DQsjO1QMOu/9szujW09l5sbEu9Qvnn6iOw3Ji3Ymq//4KkpeLn6Nl0QfN
Ix4coXs2t10XrnBjuLaYHvQGmDRxSog6ZawxvOllD6aPj3uVi4NGSIGy/qcQ2WpRtgBlZ6pvNrg3
HC3Ds0kV2vRPqRdjCYt830TpaCJ7kchqwoZ41VZaBNOzg90OWagCuJ3qdEHXaCZ48qimw2946yqO
Tqb74xXdqQ+mRQXFFK8PHm1Ewa3x3VI2W14whneifSZmW1SapyZcSpbkdAPhST++fnOs4xJO6Sr8
xLovGaAz0fDjH9WQTL89bp7Kv9TQfAGpdfnCIs5W502hLINteXAAXEjsv+DCiduy4mqXsQc48dem
n8IENiZiFENSrUw0Vxz/nRhPJDU0ie0RcbQnRQCzvchSuIQ+o7u5b6SSts/TRWVEsIi2TyAf0Ph/
HxMeTLv2DvseHmx44w8BPFpZTAuakzZfsLPJgD71gHDJHgzUGidXDstSJjNv2rSwKmQly7A3TIeH
92baBPGvpi5LW5i8Hu3LSoERph+d15NCgdgFxCCVUIZZPGXYDkg5fepN6jm7R9SwbKLPKpZMtKv3
F1JV93KXbTclqM5TaCaVy99UN1MznMmX55WEHczFhzKhUUmymvbcvYJHtQ1zylyYN0AwD5kMG1SJ
eGqopBmicRTjhpjxC+xKLQQ8w15PAFt9rKpYODz22VxfVKf/AP0CMWbpsbdC0nFVRsSsqtjxZRsh
Ns2vj7Ntfe/fHDY1z1sIlJQuVIjgGkWIxGIjKn3iRt6E8H1+1WUtIEENAu28Raz+QTDf+TJb5/5h
pQiltG93gIHuyMwCbA3eoYzkLZN2jXRp9ej5WZIwUL78npMhqCVCB8Pkvd6LcoLHn6CTpWVNWsnO
xhNFt37HbZq466b1rQ29xwOh+Z2D+edJ/adVZnk9qe7w4I3VYuhkGZjsXJfRJJFV4clxpWpnYBvt
bjG52e8TGt6n4UnVYe7/Jp+du6u6OC2MKf58AcE3E7glLptH/ArwHsfPbsSLTxFISiNTzZjBfqZR
YiMtuvx0ifjeKZvz/l34Px9UA1hG0A+6O4zrkjH+k3D367AgSzrSfaCbfKeU7FetQbitaV4eFhv5
1Z90Due4n2+RUHH9/SyL+BkZ9KIiqn48GI0jjTpoE8Tyz0T2TNOdRVim2nGHW+05/zJmjoakhEnv
SeTLxOnrXWYA/XEh4HOAV+3LQCWT0jZovftOJuXaG5hmtuuQe/tCoVTxQbbSoqjtpeMU13yF6Q4Z
8onRO1HCkgvp+WJ05wr6+Vy9VQUXm5Umq7T0J22Jd1sU4vDTQ8M4nlyUxrrUK6gONt9EUmeblYN6
Ux8LciI5vJdBWnJb0KVbfhk/mZx4+Qlx234uJFvaO1vCFxpw0jUiu3ktphiTqeWVRPESjisVLkSp
SRiG2pCWnkFw4wnyj7kCLBnbmf/6oCoc6nobszx/mMxGffUh2b5AwfdhrnGwVAcfPN6k5+PVo8GF
F6VEcaJsMDnt0PvjOmC99V0ideQFehxpr3zNhMcyWUlo7mRR3He1iltIlXCjM/OtZ9XjKgDiWiww
xmpw/p0m+VvBuxRRviNCOJABzNU8hDMZd/Rz7Qqv2ftS+1r3IKKlAnj5ZoSAiaAxu+RsCpcjhF4W
M9GiyvuXgqHL+sa/80xAvQ4g3OhChLaf8cmlnDJr3vsn+dej7r/gX/MeHvbEDze7BEz/aKWH/Bru
TXS6qcWNhc2YrFb6BlJUTNjN7bG8/rTNSWo82Odfqvs2xZ3TLEtUyF8mgxCfcpm67sD08465hhr0
YKcsx5MhU9XincK2jY9ikGP4WxavQcsdQZJEgUO02NyfcUnkewKr/XfXMe5sxtKkuyCfx+tszc3p
Bp0rak43Ax8rFRes/txrxFbI2dDNbjLiE9lYvqmvMehPHQLv72e+hHEkPvX5RqIfeddgHzBbJwOI
GBNCNqAeqSGlyq6BdPS9ndNdmbm+hucnPe6hxZy7bmZijnmDsBc+TGIFOGbPzT0bYMKa2vLkbXMP
DEdHuZ0KpKweRWMn3vj+osroYxCW0KHcPmNWG8M9+Ea9p8uectRk8KOSbrv9molsqUNTlQ6uhmYM
B8K2lRGJE6xyjYt6DOJ7Ie5cWcNk4YdNO0gmsR69s6gPuBPHThvgRSoc8u8rxQT04mjH2SNGmucc
dOMx7tRYLHcHVnGtvhvFFAz9FHvAbVD3rxbxnNnNqq4HEjaVCOCj4UrH5aYxA6xNR+VRjyK7ofOC
w7+UsAjVCASM3oNWKJJw6XNn6xBd2QPejdH+RU/pj98Ea2i41tKsaqzgC3L46hDoUICUkE6XTVwb
YN34y/DNCqiEJwir09N4rK8fuPNYr3izyh3hX4lBhLi9FWZM1EdiTWqp+3I1Ml0EHxpXv2lh9ODg
ocUA0QPcpDQqYpEs68ZPpVvTCIzqZpa6me9ey3tHNJI/4cckK3y0nBf4LORw7bJAWKeJJDm2QbnS
S4TTTEE9hQ7vCHtnN9j+bHv9v2jGpWwv2/U2aXylXSAyYTT0WmohWru1FoNXHwjz2yph46MzUVNO
vF+pSs9DcCcJyHD+xsZ9TxSqZBqbWxT7E+Cu2qSWOc7FcUwLoFXKjiIwkWbrmhz4FdoWt2n/h0d1
a4foapnRhGB5Ql09S1fg4u1mDzPP5lsyhJGVg1IPNhXDX+RPV151SfNZD4K5NmKw63xBmM1SzwC7
krIl9i50YnvlLfUgRzYWzq15c+ISSdQm4OpIr/Kml+8pEgPO5ixeqIX6xAxTMCeve4E0FntySUOv
jt1NHKJoZwcmBrfHR6o5XfPFVBe0tsicRqUUiitIPhzJvOb29UBoMLbdNQzFkiHT1KcctjzJ40ld
K1d0w+Gs39Pbe3Jo44X46ptXfs67ZrB3wRXA/m7ssXhKqwp6L7geXrh/1JsT76brbExMC9BZgW5Y
D+5zjXEZp6QGyzZSFP1+0ALbiNTMg1MSDmQKQlDT01vL7KKiio56wtXzVDXjIv0s7xnpQ1X+k1DH
iofKtnznjjfqFB6unbCBtqzTlyA0nix9TVV5dimPC5YBQFOKOsrQWp0eAcT8iJo/YlDVf6IAZDAG
reeZKC37lemktM9dADuR64nDQW+40u0PEibdjxRt0QJG9ikImemO6TxIDSlj2MUAGfxyUzTQlpqf
bn+QuCjfDOxaQbbt0vYioQYOapF0flM5UsOdWKjwE6O0Ugf3coDqDks+Z9XCggcM/bKkHw0OTBEP
adzr+MRO4yBK9iBoI/MDocmGG7AUpCHCSm3X1eX+8DU+Le1KUtTUs8+aPpWJaTpUNlRLMNHDN9wg
hTLBPrU5DxyP7+Y3lu6t6piMBlxHM7vGD3HE8sVjxmAftkhqI/X1Cm9x3RhAXyQPdwUxPLFs0lZU
ZeIlfEWIWdLwtiMAgKwHruMtExDVgPJsm0P8RIL5muY+R0yrZBrzjV2SUkY8LsC8Vtzqvr3z7euI
4WBO3aOfiFnR6YFgtdVS83v1Puj+sAJF4y9bY36RzajTDOv0aYCrFB39av3B6UKzjH0TTKVjFEnA
adeoM2wsmeznWWbbO/owhXZ/i2HmIUjsnb526e89WzX1pHq8pZZUMj1Sxsmt+G/8bHNjIpQKb27k
sI68xlB1wZ7uFwaRNqfX+qmGod9qdDGQJa15oky8/h1hWUE0GuM2jcEv/dRcG19Lq2UtaT6Xq7vh
iDrLIvG8rNWW/+GqTqQCItw3IMQ7gcGKT6Jj2Fl4N1u6iOar0EJzFut7KxjIMGEx8XVAKMjw7aIM
7S6KQtiCC9KdJOVdi7ykxnZo85lfkKaMDy261PnIFQetkB/DTMvjS8YrpSYNRPIXnkRbbqMgoFI/
HIKqfGloh0rELmfF29TCb4zsiVitas8S4zLZJgH8ZsowS4xhDrEKeSMCPNWtPzqe3YtqLfWpyk3I
Zh69RK2mNhM8QUOPAyqCtTHWBg8brgrSk6mjA8puiE5g9xhekxD0rCmMSPyb8kBNhd5hll/JHpfl
Gy3GAjpvgNtzs/JNFW0l3qcydnvWGatUjKjiFTGrUnqCyy/RJpciFktY589Q3I213ZfC0fO5z/eL
SWW55GyubtKvkLLbDKWlPYAidz7SZojMquZYXiZ5N9H1Kf+mX+4pGE1xrXWGJ7zbzRAyo3X8Sp2Z
NQ6R2lluyAli0QT0VEDlUYepEymKxFaaBY37ykVwUNZvF6IzJCXqc5Mo7NiFBY3qQag1ntRhrgua
qpf/fa4rlbFdBOZHSJ28XEIQu3nsdVXBXZkQqMMrMnE4/al/Q+wEtefKOeoBTwqa5/HFxUfwax5W
3UFCKtARz+9+v394IUApFdS0E4nLvSiY+ggGJ0wLAJavqtBCIcmL2ZlTTbnrDcNrdUi5VtFZBpcS
FL+MvEqJd5sLJRBN7dN7iGOFx57km3Ppd0azXtvRBVW+6mT4Q21eVtvLvdxigCt77MokuTuBsy3A
yrPYNGAMJH2zW271YySwHt5x9XuDL/6/AEPNRwhyi3/Bo55cUWYjhvs6ADctfNmb2fVUqfIcz8zy
9ZHVGZNFYAvN7F6saJHEQUjISZG+4E9/tk3Zftn5V1wkDSN1ws3FBfvfCvJA47bkEoVapVltSdkV
hHpINqxWSe7cFUYjn6AUaWfeliT1jwjXiZjfXhbuiyBLPUfC2wzezWlDbrNK6z+wzVPYzxoOhRsR
z9/pDpVzFPrUzqyCmf6Utv+grb/93idJ5H9bmZyRB4FicJ0g1MjTypdtz6lb9dYjiRLJL3Nwx64Q
FWb4vc/NkwXVNbjCSifyEx4liPNW0PKpxN3VGvWKY0bF8sPBNg81WIh25+RlcKxryrEbTfHgOXEC
6XlusL5CiOmyOJkzqp+SBXRyHaT6K+aWOHw8QNuYVkVsBX0UinOlszXM4LAER2qmqzrcSUL2D2as
KWDdkqJJhsZAeuT7erQO1ric7EFKCgim9NxvXken0T0Bi263LWhb2Zm013fr5bulCrHCG7bLa+87
TW9zHv0TR28yF/ceJ/7CZnyuZNOfxAlkhT7u1kJxAdD33Z0GrhO+kbyO8BgvYGU4Q0wnTzDy2XF5
1CzIHnvXfPjJGp9wPhZIMe/0v22druy/kKFX0KWn49r4+Ku/CMquW42opzGxvn3jM0n2oIvSwZR2
dhNPmslZYN8cBkzGRqpN9s3qXxjz1Iq+xBd+jEQ84uU3AzaG9VNoyOwIuoebNc6JQhXqHLa5JlBy
Yj+suLNUxEeO1LJrMbnNYlE1M8K/uQugJd0Hkw5rwbj0/CULsocjXXhNuJfpR48aaEKT0pRfo/gW
HOmn5XragPQsoVX3Oe2USSiXfkiDQGIV58MdvceNshW/oWs5jUWgKtF765hpBQuXcf75Obb11UH3
C2VtGnRsoTtQFUXuO9R+N4SMxJpzaK5gzhFk4XVVOmnZVdly+X8zC+RBernmty5lzT0n9o11q/LX
n0OMIHdzMpIoUitakq3GqZyzP5Brep3dHZNDjf6lMhRv/J3ZUvZb3nVd64CG8b9OOvzWwicj/IJC
bGjuhqD2avHQuEfdVgPqvbNm/c4nbIjHGDMvjWe57EMSoHVJRDoUlJbYiWEQAzj7c326QyuoGd3Y
zlzpTEcq0UQ4TjizzLl7mhwUPWDrQTiqzff0npyiwG5iqFaQewt0SjNKLOVAdHd1oYyzKM9YNrFH
NwSZ9YiMuvv5WYbbFNYVOkBRWa47z5ZqoRCyu5tX+jxr57SJzGrTkpbpRz1RfuIo2NROitunpcFv
CCGa+xrL25FcsiAoyLg7wmqFtwNJo18C/Z4bNoYJigMtlSTycuKadSRcuSgKmqthxPudyUzF4LXk
I7eHe94acRtSZjHpdJeRMHOc5Ma6OABnsgfMtE8ew4tf8yTiVmVgUR7BZiZNFr0HvkRpp+BNvYfm
OnWJSTNXfH78HR8eV2QdshJBhYCbO1/5ScrIxo7/rKs9KFsYaSpy301Fynbfutt91ku/6+IqWxAb
QUnfQBeiJFRajSdbYMnbVkguxCh+B2CS7akq4Oh1kBx/KPm30hIEcKDRsA963xYu3vPVz0ylcP4z
S2hHp1PdFVbXunphxed/lWoCmz3lUajlV4o/qQ4EXnxh+yi6ortzSpuBvcoLqjMrFpuTimWI+VnF
Utu5Z11XvfgQ2X4czbSZWNOOXtLKjzmRalVdtUsKV7mdkcTOlLQXALPN9C/HPpPh7iIW2QE6CvYa
vaqiviLrYcJZ37/JaxkZOApM4JvTnhNYZxRD5/4CQSrYztcfDnPSMpxmBRYYOwKFQzKIbjGAON3M
Xc2DUsBMeginxM2rTml7dtQJEBJsXATf8jr6hjPHQSNT7iRNOL9bjbFenUyco/lsVyHVMaVcXamd
dxDm7Ar+BcRiYdK+9Lh2nXjNfddUn+hUBsQ+0Rh2X6ovnFEG9QL45Qn+3zR4EyBoLP0LfiWb344S
NGYJB1PaQxcEBpT/UBGObEyRRN/INe+ZD7mpYCh3ZraZ+iSi3Xo+wKAxpUpcQr3Bx8ZlJFgifysc
Vsu0+3XLIWUeTrWpK/Q+7Q2LGPTzmAHtRmPYmwoCgkIi93+NPT1JIt2ORvQybrnQ5K7pNs/f1hsO
/mOinoxKQUFDUGAM7md45iyFPasQ8Bd1mFhFNRoHOgpEewmUIo3fErBGFiMbB7SZ6xZ/7ildsB61
g7SDO25Qom64NpAzS4r+xIHK2IMwR6no15eHYkKOta35nqJyViBxqEhHJq8b9YMaQo7rKBSmkbAp
RzhL6J9KQlem9n6gUvmJbGIEJCGjVgpHR2p6R3QEBn1M5TR6ZeX8h+qQyY6jgNwjbil8M9eCRPXF
xginHsOr4M7cvIvQ2owE4XlY/3GaOrxWO4YVBQ05AbR9KyNL8uQJgU7UfBj53j58NJswEHXvthYC
2yRM15cbYZ+razKbjyuxhjZx7ahPfWs9dEFsObvKiv5oO12ZrDFlZMcfzkhhS+dzj4hCGg4Lp5r0
XoK1KNGjWGOE6Ry+qpR6sk8ZUZcu91xNuLy/ciP8t878HFdyemL/yOUiMCrgwhV16cEnyxvyGLsl
XQj1PoCR+TF3It00uxyGtQway4l47u1ymm9mXjFvyZUieh/AL07gv5gPy+NmJy+PGVn/6Sqiefif
o0bLPuujDSaT9d5cGgkWCMDMaem5DMaspXKe+rlsIablVLSX1aBz/6P46tGeLu4Dqp9bkMCQ2xJN
iB5ai/wNyqB/S+otBULqLQ05IHe0yK7RLTXf1WdFg3ra+X4qIfySOqSFi07gcLjCks8tfT8GfEyb
B0C8aHSFx/1G/5frABYFKEcFd4oR4BJk8U2LxkzMGtKSekLGEgPTEoCZzJN7CV7P1QOfBL2K2EnW
x6e5J9mGkfoyNn394vcpBgb7+eYFpNDLSgqtdcEe2V/ChpUsGjkKf3l+guZi851p8AsVQA4R36Hu
uenqGA3ngGqEwGVSOBdoysqu4lWedq1IBD/FYUxLug8xjw/qzsRypMY4SWOgSqyPqBen6IMjtMHf
yjln9Gp7c0foeYlj10+0j82cZXtnjcm0Q96nfZSdfD03thT+Cy4getpAB1Snugd1QhcBqQtA9r5o
1NT6E5UhWlua9FoeUOdaLOtLn4gHai3ZXiI7Khg7shph5QQlDT4PSBrwM2MAHjtQGRIiV+cjSCPf
z0UBnpKRdz0/uqGa3caYTRzg79B4z4a9jfNInK4qEp2IMTI62G1Ph/hbB90F5or79YQ4OJ9GiZsF
cbakxZysii3RnrcXw5hKCH5v15r5Bq3Kh4ydgjhLeKq8i2fMjY16AIqNTzXYcjRZtNWyPRipPMP1
8Di2j9kkmsCFZjAl1cRascDnb47/ybAHlOd8N1u8283gNNsljck0Db5iNtB4q0z6jpw39tXHmU0S
i7CDCOTdbvF6eC7vQYZ+C4QaWLH1T0QNGpmnrULVZ5OOehol6DVzphVeGFpN2rasDz5/VK0oFetD
xBaQxuGqzaSM03Di9wYrz8zKTGvV06T+CbDEdlBeZ4AVbOwK0XbuNg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
