// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:35 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ blk_mem_tdprom_1lx18_8b512_sim_netlist.v
// Design      : blk_mem_tdprom_1lx18_8b512
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_tdprom_1lx18_8b512,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    addra,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;

  wire [8:0]addra;
  wire clka;
  wire [7:0]douta;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [7:0]NLW_U0_doutb_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "0" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     1.287308 mW" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_tdprom_1lx18_8b512.mem" *) 
  (* C_INIT_FILE_NAME = "blk_mem_tdprom_1lx18_8b512.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "3" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "4" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[7:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(1'b0),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 19952)
`pragma protect data_block
OouckzC+pyF4l3gP3s7WFmBfDJl38tJ5Abuxg8DWbntGeQOGF4czp5ShCvwzUTKTKCSGPvTamdot
53JLHPkK1Cet9dB/wgI1Eu5jCTNggYtEvzmxSr/QshY1YE8FXnJH7M0JKsDZvddRzgF+/ZRq2mob
kydpPvsRaNpdad1t4cvisqud8JkL5Bg7y8ubkt4pwaPTY4itV5bpczcF6aREZmokGMhNIr3V0PLH
KOdI4VTtLkdMoarXR4UXjxZa9gURYQoEd/2VK+12z3EuVN5/BfWH4sG3C7gpW1KlHpe6ynJuiScq
co30r9LHLTKKWt9y9TUApIriawVkduJAsQwLqUg58cHBhBY6/+O9M1k+DaHS58r9g47B659qJT8W
s6cvm1oD7Wud/4p/ptZRy4x+sxv7SmNqLEuH7976cmxot+7AItHCzpFI16tV4qoc7ZOovPImbJza
u0p6ZaVqj7aKQpZ7CFBPalOvlK39xsO79JhL55+IKMJyk8dDBRP4hATs3m+IqPL8HYKEkKss+Xnv
0d7ceZigYW7nkdfJaPlIT4KvHdyJIyn+5O305Z9RsbdxdJykcoCszihhEN3vhM/2+MrcwxkSgcPs
vYnTiGIh+JME7GbkFxutzu83onq3tyxkyfk8CZytCuQ8PLMQG/KWUQyoAuAz/XY2/0zD+VSYTxK4
rEwqoecBksSMmj0fnlQkQGlGKK0hl6i92Cd2spArZ9WsqJ7H0psc+qOu6NMtnJfQ16F8WeWxYEhF
O4q8J5rA41tXxCkvjXVaNNoWIjAg0hsbNo5p3qQ4KoFqWzawiJJQ9HMg6RL5nmHfw5w5rDQN/Lix
qcWMWQFw9Kk+r2RP55Cpoc1zhPtPWHqhf28hfP1Y/Fvgw+duGNJmBtyFBsFvLi6y8R9vXc0//bSs
ud2WXacq2ZlQUqzsCqNX6ILq1PW/99o5VP8mLCltSdKvD8hVuz2ZL7mEGA9BIax1w0lTAgD2VhUz
U33V1+6z98QrGNPzTHk9Rj/axSZf1lUqRgrn8piitTGqp9SAXRzCs3GhPcC8pCRV/58FqhTHtzrf
RLVrhT2SGGotgKqktEaBBq4U65woRGzNmPP4KIkdDChb4BLU+lYQSeEjuwNYHxENpyJX2UwjFwVV
XpUB2lRHq9ioZChCe3BqsG7MA6ervlcCLfzeeMsy8Yv3r8LDiL5b2YBxpD7yxwX7k0dg5/bj98j6
vSZk3GrarOCO/yg20Nm461s4+yMGL0XrKGjRRbkHnvy5gQz99Puh6H4uaCApkSMW9Iq1TS0jIm/4
Q59cjbCrkIvQ3UqD3WkC+PCLc4BiXyOXvrHR0+WUoCxaF3fp4741Ve/aimcAzI3qrLP9PfRdwb9J
gNxVY/xxW5DGeZ9HvFY9LIAtim0ID42oChAm0on2riPlfyFoKkG5FqLzR9Lym7cY+m57oEsd0Rw4
iFsrQyMyLvkLWbPA8cJQBfS9EJ00izEv0/dIcpo3DCWHr5+ObSkV8lE/RrgeXnB4XCwLiv6xnLol
kTJd70sGs25TfAr233Zfz0GYa3d0jxb1OgPIv500CpoCHYN9XfVLeH177z5ycgRfSlfzCtKmDWWk
nxgnJEc2QpJQ0KvPX+6a1EvdEaP/M9E+S024VdFYUpjW/iFdTWyNRhKn3xAtbnBvge9NzknOPSv8
cgX3NCFKmJlzj3oKqfi5wyJZxEkaBOb1rs/Geq7Xjw18qwjVvg6VPqPHAcbrOjdowtIqI+STcGnT
1EsgbdsjogsLi7QM84rDHvfTFHOdfa2VaysOvypuouMa0EH8N6sp3uTXmjaq109WQK/3t1CAd0r/
IYaVTCjBollgW97djzXGcO/jaaffEWsEC0QrmCqhxMonUNr9h9uflmPgoA+4mqjIqj0N6NQ4+RO9
v3z9iLssjr+EAHT4JVXBfThkyBMxO6aHakqMmo/sKWgwvyj5hlWp4nDg04pInHM8XvVEFH3uWM68
/kwK57JTVM9+Akw/adVbl+7h3k6B4ju+oDRzl0Lbsh6R6DZRrTsSiO+QRwL+pQvDbIH7V5XlsBLR
9iz2m0FlsK2b/u0DX1NPScIKPrJK19jNZsHF+qkQqsERW7xMkZUL+1L/cCvk+BjXlcQrrhbXrY2u
19r5iNk3UzMFvFYOP/uLfxDOwkbbuRJd80e9WbSUTOHvLoUAOUliYx0N2guiPUtceaT/kKoG2V5s
9GwBhmJbVoIpTuyNhgbiv+XLaILl5RvEqRvG+EYcu31tjAFH3Wyb5D0KHEFIt/nc2s9l+YtE4QDS
OVzhyZSy9WpXmlxyuoei/dcBIw8W3Fvww5ax9uK/9zB+eueoc1ohr2HmvnpDUR/aSmvM5xCsTp2d
XDEWqrU8GEebh+Kug1FyrH+E8leUTVD1LaXMydVSUZjpnFoZWfXRiFRL/KOSyltJfByie74Lz8iN
r92kQ9jA4uk3eKJOziuN6jr14DMKBzxgNjEAmkWpTCBIdePbcR+ruBMnNtUIfRVLjCFjzcn0V2aM
QouGDTv3je9wiilynhkDwJ80B+hbWQpPeGLAvf5Fp7UD9yxYW2M43HuYWi7nzkivPeFN3abFAMh5
y50nx3d4ApksoX2ap7696htrrjTm7d/xAJckPOmsAHtHGLmB4VKjuaUH/Di2Wy+wzY/GYN3RGeeI
tVBB/pRUm5rPEBYrpYf57EGkxtDBKtrYFTu2BHYNPsw5NRiOwWnEgcqYOk+53O4JameLe/vPC0Ez
Ur/5LhXQzy0Gn9Tu0Ukg9O9DCMlaD0alYgw58j6E9Xjumy8LXcQEEFX2N/iyR7rrccvQJ5jUiU/Y
15lk+UIuYvVEnUkC5Asa4zpBwlDjMcsfDnLpog15/J1S5wVFNLZ9bJR3BzeBfPX6FOElNB2kk5/e
p7OfFRCG/eHOYubVE21ptM3CKiwO2xOGo/KSqjNJIOmoPPiiSZriXfz0lmK/njJJOurx5808VCql
Oz5MxIU+J04E5h9FAOSzeevtUKOJMFoEsWQu042uOpIg5KbYEMX7BGi2T7CLjb9nb4e3iNAgJ0fQ
+ejLfo1/0YcAikEBQZdd/B3j2W6TKebynD7mc7NHOYd7uE+wNx51FLk/l9VTGVFquTQ4UDSbUnuk
v3SxEfdCK8ooza4/AIE7P08NGMhtKLvgy5DzvKhrQKfnpdVBWGNksNa+GPNqCATkQx46VZM30J8W
6PXJy6FHrJ79XQRJKZTWQ5wFOQL68+rejCkAKz1oJb1x+8mXR6SIj9xXgoN65B6w8aZKoXOjK4if
X4dz9WBGDq6uniUsPmulrIW0d/QW7BVVFGvkXwKwaUPqoxAQYip8f5GE9GDfA1Hx5fS71qJKdMtp
SHIrpPipaTuRoCBjbEURiq16mYDlMYB8ToHmc6ZiYU4+eDgmMAWhBV3ghEUbIWs8I91zYUVsazF3
T03bsbOXgeYCdjKDS2FrVfbxYsRIn8CaWJAfvwzhgftoT9JE/lpUYYyDNUuVCdjd7Xi5oYdCgSA3
EO6Bp9Vr2d5BlLB2HBUqHXr4l+FjZIQiQe0TXCeJ3LwvdC6y6MtIYXbU9BpZLU0nDotm0HSjck7F
qbrXohH38wswx8pQIoTTUabmICTzQKaQhpMCM0GW6V1+L8nQw7wZj+qbpRX9d1PVF7qDFaqfBIqw
evmBx74sF6H54nxGK50y667wWThD7Wlmp/6qBor7poOyXugpmUUfFihHx4hXNVrz/jBAGwnZqAxN
n4JCc7IOaZjICae9fr2lpuj+1xT4zjXlLPUJj4pGGTZlk23Q2FC59cghxhXzG4XHH7QToruAkzYt
ohAa6s9fELDUYaNFGds8/GOEUT+Jo9QurMSIplX86+cAyOKTyndsiC62s0d95zRAXXNWP7MmAB8D
yxvxxy6I1M4Oroa+IdbKSAA+kPR0QBlrSjissNpdwX99FUgNPib0iGRXyhnPYPIQRTJJCcsoCdXk
xXLQqZmGp6bTe1NWsuvLTnZ5MUBTJHaB6I3HERTT5nsLeXIkmMjmeKs4W/K3A5IWNjb327usZYsI
k0BgA9DzQKltOcvFxpLPXYqwRnRHYN13i2S/Urwhlsf6ONEwZAtY1Ck8DkUKaypA6Q0q2Pr9bX6N
iDT9LKQub18pM47pg8+HiP9NWirDY6DMVFyieAk6g9/Lq/KbMd5KjKD0LIQlK2iSWqJgI0j0wS/q
v+RxCDzMRwkIBDUe42EtnOCZXy3d0OhIdSF6OMxWsyJc3703U2flP0n6LzZ42zPyCnp9IpMPjGhb
YjZleQqsxxtwX2U+oWrKoTIttIh4fLyd6Nt7XWbp7A3y7dnBeCLx/1U2tv3uzeHpCe8pYilRbWJr
m6tImSuqk5pQYaHPJEKaTqNRHfLb5vQogrB/J9mU95G+rMoNEZS51G0xakwBQQZKqY6TizLwQxPA
yN4HdmTO8KxTx9vtwmpwcgWP4Gm3qNBuhSvyF2gUK+zp9XKTGu8Sd+9gyMODpCorVFmPAU3qnrX+
LOUbC2ewwfvEpQzkFB8qyElqrnS3P2hoGDXOJZ29g9h8YsISNh1C03cGrxz69BnlCZkhs5QpcpKu
IhqU1TJ1AcjNiPcSOfaHqducAkol7b8qS1s+BqYf3h9LyWq5P9K4OnZpeCzbeb5S2qyLYOF2Z2Qs
sddkG29eDJZ7nZAjIsNTZa1DICj9p+b7U66fhOsp7lFqTmykLoGSuDSMn8vIedmXN6EsYh554TFC
IXlrHk+l8A8of4oGPhoacRD7p0288UV++Yos+5r7l3JS3zGW5Ri0WWODEEiTG05uhqSh1ijEKbFj
y6LTw3XrNoe13+5iLZ4KnnUutQYE9Zv77vKI4xX5iRCVa4+AfxXy/vGWhUMqlvY+5mauD5IQi9LZ
2pZsC+4jY4oet3kDbQ3Pq+Pd2A5EiVA2D9QsP2fWagF1CcJDkT10K0fWID3Ajybf+Bi1MvnSr1/n
EJ/lCok/N54ipdkdm3Kxnl3uvtS6cf5Cm/ECIPHyQGGzLOdc9c6nwb5wp+2y7eLogoXLk8EhvHL2
6p+KjKiFyfVGlhkJi2cVbiAbVPMU8ZN119+HDFxgJ0Py/xbXG/+THZAbgABmtJhEgWwXRbkVxJNd
TI3vQ/af4zYp8wiVnOuMoYiGpjOFom5RaIElsT6bGiRj8s5eowOLY5K2mg7rbyl2CC8iYVTyuzpv
D8vu3ylYVrDLcRVUFzgXcxhjYIXQ7O0M8PiBRTTgP21TeU3TDRnnK9mudfzpYR762JMF0anHo8yc
e48JML5KISu68gqV/E7o5xgmLrAV4MpVR7aGswPFOTDmIx6rgFFRsYlbrfLyh5Zh/QzCRdcWs/Bp
+zN533oeQU0N99INBasdfAXkCMtO0ggXJj6p7OxsjPCcGa2OiDtTXz72AlGAoBED8qqQ4SCB5evH
EdTqdWFLNAqcVCAU4RPGNEaUW/8ew/92Gxj1S2lLYDQWes+xr5/Oz5/2OqJ1U/ILMKzKiIIhZoI0
qlqInWFnEMwlmLOzg4kCPY2YNizuvZC1zhzs100wHf0zX9pMFCwBAi39cxuLZuZbVGMnmSWpXg+u
53LRumQrvaM/NN+XJQ+ef0p6ieoiKTn9V2Ytb/eUAYToJ4T4S1BEQAt4GR66bl4xMJtTXZ64e3Z8
JwvpNvvMiJZfc/w9GiTh6MgeWbKAH6CUBa2triWgdwcDuDkytyOYHryImu9Gvl1Z2Zot3IEGVG7b
eLTBlbmaZj909xW9ggKRNVUZmiF3Qs5Ls4gzDLss8j8Avz0/qihZYXaSVqrLuuOCbDrFv7g0SvOU
89R6LNawBV+H3h4R5DNMI5adeUl3Wta/1LVK9F/iUqDX5t/dUF329gCSfoIH+LgYFe7EjSdWGFHI
1AmAVqd41lGarocpgPtG4FojVOu9El2+Ke7aLoJD2ElDctkTLfw+Zjkpx635UWQA/k/M6S/NWHCc
7BaOmACGbyMGY6kJTJYC5ULyev73GY0Dwpzn6M6Dpa48N4gKjeymFx8RAq+asVOD/KGr9hpyVejt
hVYvDqhCSqdXdKftytOA2zrpeEJ3OaElH1MWQoQe4+4/UXC78G9witMTDeg2Nqiwg2pyEBMpMEJy
3ably1Nc3I0IeOvOQ22XDMn34n37317Ma5cjCmNBejVwpsCA0ngQOUEpHqFT4lmkQgIvZNVrUzxl
6M2p3RoFysRVrVKilbcNpGh0aEIhvnw84LaT/0wRbHY5xNXq/gdGHzLtKmyi4MMadK4U5yBRpqqY
CPG8jHkhpCgZgPtU6q9M4Wf3lQCtC45BEiKb16hl82iztPspk7Uc91t3lGeUAOARQSjT7Xdbg+0R
iX6eSW/YJB0IK90I6YStsTu8Qg1IjVMrcwV2MOBdAT23PAoRtQKAB9Hfbh8P57fY17vUJT1vIyis
Oabce243wDuMbyZ055GiwRgr0pSTUocSMz3P+mIqy9iMdBSYJRaZOrXG1NuHsEPmLmAYXNyz8Avd
420WHASWq1ImZVFBZSt625KoC5toqHW8Ol7203ujNmnF60UDsXT2pKBgjcZOpUnp1sOLkHjNKbH1
yZ+QE7Q7X30fUxa5nd//vTXUfDgR81/GE3T6Cv7cxsj/fqOe/80Rb1rFz6F9Udvhzmqpzr2tGWov
jEbMZepPDQVF5/devKNK/COmzx4VnRjtIcSlr7eWX89EyX0QnYvaI9bnl7mhDUblFg6FBbJ4lTwS
kDv9SDSKsnY6pXeP0uNKq80R+UiCWYJDjJTe0j3CZLL0yyJNuZfJ2WD2LA4OkMi9fFNvvfTsCSOJ
goN873Dt7ocMAgF7dQfCjvmmlxvUEr006n1EiLs82qFewb+8ScF00RaD0c5cjikkq1EYwvIJAcr8
kYb5JtptQRC7R2dvlB7ZwKQEenSGvT0a3Jj+1Lx4WdMGHR+Q82t91Ajx7iVsDwT/xnS2PGMEfLUM
R+Cr9J5o8EVh7wzcSol7iQ8UkwPsYvNHT0k3AbCWz/3hgJP9nmTZwv6G+akZ3AI+IP3LxaYyPr+u
+DQCs6kpQVDQfJf69QNEfEMhvyr++0Wh8zdfjcFp/Ju8728l+vvxb0ya8H3A08d9w7pni8dd95EO
2yUKCQVA94CWGJFIgoKkv5GD4P/kspcb5QvANxytT4lg19E3+STJSFRtr/pK5N+h1rdV9ZrO127q
qOyqXgVxlauuZ/h4d+Di02GkYsurT4USeHWeK/ktGalK9MLzbXxWkBjgsTGCkf5n4Ie0RJfY3Rkx
ir5Ms/+iF6keglCpOqXio4amyxVwxkMq7dqzjCrAl3Ex128fB/BadYhessfII0NKjybsIFEOotlL
xzShmBTQhUgxt828oERDcpG/E21r1pppcS7gynum40W5B1Qn16rEQGNNycSlK5omd3NTlL3gIl2h
T21JA1mM//0ZnzD6zX5PfPIKhppy1IxEH09nKe5pNZ+p2BU6YZFPc/ySdFA++LY4x/LbX8yhD+Wh
nwUsPm9aJtOaU4Zn9M4wzxnI2GkSiatSQFYjmqq5Ef3Fpgp8yWSkVr/hCMA3neKeY50qD+uPsAaV
XkeFGDI9Ha61eR0R3gCokcAA0a+3pCCKcap0Aoe99mt5Yri6HLQ4Mt3DUcX3cwtPtt5fPH01N/vQ
rL5oOW+u3NCTwMRLsowbg5mMbZWf2iP2AQ4hhWTq2w4iTqR3j72m1DQNW+QA/DI0D3VBzWBBgo50
vTmeB1gFk5fbl0kumebMuj4KdeInBgNt9qjpu23mfq6ipPK7u0cKPngxiRfAn56u/AFutrUIbHGf
FQbLJM3O1jJ1bbJN4KhiHfYQY6TAIEjy6jgcRCd0OagI/rCCfxUXhWD5D1hakdRXFKr2ojEniCrp
CdJGvgMgt6TuQHdA92KRKNS47u0XHHeRUPP8G92n9SAkEZwxH3UX99z9opEhM5FE7gjLw5MrNwOD
Z8ACirnjPZ8YaOYTNIYUEZdKwHA3SyZP3KxSU7Mo7B9nCSqEGfi3vONP9iGBejisaDlNiteTF97l
uYbLz0HYezWbYk43NQK1VRr0cgWo2Em/P6AVuBg/Sbe3lwgBVL+SrMdou3s8jeLU4LLf95VEsX/8
HtAQHUmgZ8M5rJ1RjVnn49I+eAdzhs5PT7B2Gh0FeqV3wHK7BcpqUdts44SR1JfyhAzXj6xRJdGs
FV7oHN3TGdNAeWOsVWB6CV0nxDaJx5CF7eoYAMb+p/3BUKzFdAinXT/ZsqWDMkZui8yzpLer2cD6
c1cdCP228kk8Ng0hqTBzyngYOIUCq5dbc96pbQDH5M6dbSyVCE/D5Cqyvtbj8+e8+sVJ3qaBZWbO
RnUrxP2df9VoCWzaGi+QuxdvIo1erpEqcdbi3//7XHf5xZqrMY69ym3D4JxgNoCkPFSbB4KDGU4D
1DVU1Nybp0MtOjt8evY1CaFU4o/ghcDpi+RMCXPdV4QVSwMFTihl0UJQS+DDJHLbY5sAG8eIzgcy
YeWIkgvUvAKdiYVgPA05o78x9dqbZutlzlgXiPtiFT5uxfHmAUESgWxoKVys/6452KBrFJh136jd
KA9MmHCiHRVCrm9aozsMZEPz/WP9IRe+b99VC340KqXaRadDPkyxQF0UxmbLE4XRH0aS6f89K3Hr
2BmlUfe8WhrAudyA09fPBP6vQkSRJxtSbfmXpxV7iIiohsUzQp5hKokMEDkQGfLqi5fB9Tyl5uY7
U/EhcABUHKrEND+ihlU+CKWMpOLIhZh1zWFRYIarrWXtqBuT1ycsssOFDru86IM2vJuv/vP/a3cO
p3DKa3sP8n/q+t+PFxZsZnXRy56ZJzbGIcGvzDahXWmnj4K474ZDFKTbfX6KJ5+4vHLiekY3mD+F
UK+uke0Czg0CSxaiqYSu4hslpAzMs0sOM+HYuH0GaFLYsMWmTLeOb6BD1usbH+GP/E+c0d/1Zp2T
QJnotrF1ID+GF5xpdw3QtCo8Xxpmeb7vl5ObCn2kt2u4KvI53YFzJV/EGSnltcH5b5YB3PhWGFY0
PmGcjEo7LgOICVy9wWl1eoal1tdfbzBOgd0WbT8cTkFJVlraRaPH8t0pUBM9R72Wi9tGxkaVFYOm
37ayZpkuyI3jwS8wAZE0vkOUIkxUzgCwvizc+AKcalUrh8tt5ThFqG+N2ItdnkWWnAn5hTMVWqYr
z6E/EovKyCpY6XSA/KxPDtAO3S/CC0188XseTxixnhDh+UptSZMN5dFGwc5x7Obj7Nfb45TvT9D5
CpUJAwq6/EgqNgnuwedGfviMg/oLMVEPod02orw5QFun22Rv41tqy4qG53vlnsNSCzBpcdb46qWU
h7nD+XusexB1DKvG6TzjZkaUTtZKu/UimIwA0Y5pfTxZKeuk8/9Y2+cp6hXPd9jwM5aeUoI3VdYu
5jaEllGoKGnDnLNrxt3n61e/V0RLsl17HQo15f0+ud+j0v3hvbo8bZOZNZXr8QFOBzaikbc+k53h
evsVlPVrptF8eSNe81BvU86lH4Kg4f4FpjhFZ/xZ5G4+snFBsgs9OfAgl7xxJTDz5q8s1xr3bqbP
JwiWfKYlhMKl9uybBaaZWb52FjfQHP0W7goYnY51U5qDMJKruqF8Sb46CbsCURl8cb+6j5sq7p43
HOfKukM1dT1aiejix/5+zvCTcDV0y2S0r9Qmnk36gxcNDLXkMxPQtxhbDHkB7E1CWHkHdKYLB127
CneFSmiZZMEP6d3c8jzY0KE44iIFxMyhe2az1ZsIeqlkiiSC/jxY0lEbdSCyCdSQmV8/6QoL82ep
SmwFKXJ1EwENtcakI8KZjLUKeLuRsxz9+iHBuqs7yN7jxfv7wavBUSD1cN9GUKCTwODcIXmjIKQO
DR9qBQxecUEmI0kKoSdcTDaNg7mWJyCo36mJcm7hDRJg812qtXyGNODfcshQIv5R0hqjHihp5Rsw
vNRwPifHKCjexq6LW0akfW3eySaAT+PsaPUuiv32K5VzfhVb/fI4NKHYq+mbpq1E6PI3/UwYkpDn
HAsoIxDPw3CbGaNTJpyJZZk/eSZZPAc5eXglofCo1Sz0pCw4PdXYn8EKCfZk6JluJsX4kTE+x4fA
3wrxJk1UjZ/PjY16mPOsuDebYlKsptI9sM9tpDmG5Gdboh+larlpMh1iqkNi/l6amLRn/ss4NkVE
NRsx+BZ310juPy4C3gdB1hdKSkxuobP8QI+Iot6BjhmuuEMORsKQYsR1VpOVSR4Uym9t5mirAzIT
QAyg/UIg9GtWEJuxNH7ETW8tAST+SZKPwyPOy0BIrdWoh7dtVxFVMQi4y+C8LysKuSwBqQkHVgpB
/UKa9+kNSdhj9H9f12UD+PcQuiTg9xuViBn4JIw7Pr3Cb2uTYOXyQAl6lGgIv2F6caB7rzMn3IGY
tasiUCt0H1nknPh7pUyT2SM0yJvZ+DYjY9z5JABAVCh7czaCMVouRjhNVlfzC8Iu7GZujX3RUt9t
OrRuskA08zRAXOE7tCqT2fW9f7yrTzC7DRtf+tGmsaYI494iX+XCmT/NitKLuOrjDtYPiY5YwsDI
IYEapmEW9eD3qagL/1udSvLxrOSPbTFohE6YqogQUidF03D6sIx2OAw7SL9O98Tq5RV5kPko6Fx2
XmeS9SvIC24K82hTz5LciehyhGhOfeETT9Ge02x49R8QVUdJUEgX5h7ruQsHsnP0/LSPlBFJj4a0
4fLLfGEOPPb1tIlUBzTXRoYwl4R7UZny1zQLSE8rizcZWbHNAAEMOFMyHtEXoNYz4RJebKMlzt6G
HZ94hYVfZpyl/0K4nk+X7+ifWvsVQE+ZTc+OSh/08WIccslCIBjLqTKuefD5WUAVIZKOc8nhn/yM
vNQvHt4+/J1ejnmrujJDINYhGKg67E2HZce/CbL8eLOiSuadAOunh2PsorE5AMmG6yjUny1A0FW7
kY7PkEWY4f62mXFog0Sszwl8SqyEz3oGqB33n/4pna7nprrQBl7s+WdmNKftlGmT6PpTep0hnLOc
ZE7IIiCIGUhFxlSSPN0hGvYSPYnnJpWXhSawvOuwrhzeXjE1BECBv1CbM7+ZDhJ3SoezDzueDihX
KdZsBzu8UKCMqpMJudo3Z1tuDBPSs1WtzC/A9UH5bGDJk3qOv3MzkNV7nTmSzTxcFUkbE77HKZGF
AfjbeaU+aWU6kvF4TpYCTJmWL7Cr1uxkwECnlr9mTcIltKnkEVluQHWSRYCi+PXy39LG3/VC55kr
hvkrkjXovs+2Fhps2BrIDadhkhS3GwupZDocoJQn8etp3pFuh8Rmtc2mV/yEg43spAEtrbYTO+fu
IbMEFZjF2AyWB8gJzvHUopSMCftKy2xhSRsf2Pp4wSbOt8OZrFfbFMu5ksY7rvZoHuK16conCdXA
YjEfsm1OGLeuNe3U507oMor9/J14GxwpwsogcG7d0oJcElxtksfTMN6hk7cLu195noH3IL5aj7ga
lZoMAunetZZsgd8604Zq/3Uadq0cKUnNng5NgI5MzkRykzOpxA5zO/a28QpkoXd8a4GS0N4Cb+xB
aUTvnWl7c65w6h3K8tC/qdqG3Hf9B2O8+1ZyJoCUpzMhEhaZxdThvxX7NsI9bbBlRaNLnOkdjorK
YO4ZOJVqzGkh3BEm1zOlxsAL/8ycJkXZy6ONvTzpd9p37vsStrzHgbEEnoBbj/fvedsC7nSCeacw
Q+1TvZHeXuWVkYtf18GTITrwyHy54+6yxoaBmnNoziPypM/cdK445vk2cNiLP25gO4kjlq34aksu
RCqtV643CCniF0qilsj9/VKUMkwzc4QQGhz+kOTU3+7fMdezQUTtaCHb7pVWjZcpO4j6Z9r674JH
SIq5ilphw7rd9cL/91UaZwqrOoCLzXFSZlr0HH4mcQltkP80er238sYQZbyciwC8K2/tzhn1HPFT
4dyyy1pWT8PbKoCoSnyZ+kD0SbBvFTRWa5dSScgV2hPoO/JbwEXA8WPzHadISogAr98fBloK+Wer
vAt72H//9AtDwYrhwLgkNoC8hjVUIRnyVeRKzXslIIzGHkGJG8Y3LtwktyK9XSJr6fBD6W9PP2/l
hi1IW2cXjBVGAvPjfLwJLG/OQUkwcWPo8Pytml7Bv8G18M1NmNdbf7tdWFLM3Q8eCxbD5k36eJds
3PIZBZTSWEb34QttdcFN4kTY74fCbbKe9skCb4R+WkKP7sMQaKWT17hF+VtaqD7Wfw70k9MHYcDd
Wz7fJiEISEXwWaNzZhgUgH0BEOL4pmbztYQgdSvEtFpGDzwEaSz5+iMW49NOFQbhgJ7E43WkW67T
aiGaoD+eRnwaBTVtqJ+p0bPXOrIQJeOT1IMA002mbH1XLAF1Ej4AV6fTeeu3Q8lIgtoZwajxI2JZ
qiDHy2TbF4MC5Oy5FlXuHeK8dwou6knZEIDP28EmgGTQtiGnBeO1r7vkjoJZq1f9E7AF5tLYKau+
Vtral6s3NUETNFBz9+FnhCApnoS5GM+QCssetGzgF/gHqx3Ly5kSnBZLlTMFr+dWBIHFLuUfV70F
82fgnpksTGQRj0BC5NLeMGuClFkwtbw7gFwqEHcQcld9d/26gnd/bGUq2UYPwDunfeXF85ehBd1u
XfOy4VCoABl9n5Pexf73/yV4ha/XPU4cysvmtY8OsUqbX8aQeB1ZKG0wdWQCPBrY9HmNQA/pDYUg
mOCA9IY663UZoq+fjw7NPJJMJVZbMIpr2qYVtr11gSTSm4/1vSdZ7a8kAUtIyKefVEUJoZaJB+pQ
fjpCCz1ljJA0kKhSNU0OAi/q0jnDhDF/Nhlh1Y6NLiCkX+qqu5ecsbzPBr5ItjE3oTddRyJnSyxy
0mGtXWSFMzxW0TBGcG3xBHoxeftF6bY3UkO173MEu/fIhRE0jc3+NRJJBcOIWDxW8TAHDAN84M4q
Xw+x3Bqcf5KstW3vEw9yYjn8tv6mMr2a8frgMutZTB3CSRTlkKQ2dG9mVLdvhzVtjJf6+YKgo6cT
Z/HzFOhANWc3F+8OGaxrqhtZD57suB6a2j8AXGcsKzoMwWbytWJdCoUTXrOBGWv6sqtzXx89uq3y
lBg3G3tevfuH+78mCoaUW/DWF9TK+ZQOlygUaFfEWq8Eh/7LV7vndT0T+JhviTwGR3xwmpGgdT3/
3Re7lYkxqrFL4c8irQ4lgNLFx7jBq2xoF9mNAu0COaq0r6dFoJoQeGKr8EQppb1LXky08UIRaQKZ
dyGUxdseMtEt+5SbPhXaMDaQKN3YmAg/w0NruUbjIk0KyoLOahfTGQa8+Z7kWgLLlyLHo1Y3O/S4
8UIIXjR7rS1/daPgZQjh8RqwbyAEpgqVjnQ4V9X9UahE2fi43Ke7IeJw3CKvh+hGnPMH2nJJ3aN9
v3NPs/2UsaCqb5ik/LspLNkvmqbSl5xs0Mo4/6t1tHucUFCj5SvQG02HRH2yEkyCOk/7IA8RyUdd
Yao+UpMPJusDMAoqWA31H6a2RTlpMt2hRxAStOnwNBB3uj9vuDHZW8qyzDiTWQejBy7fitTqUsIf
mZ9jS9+2vCZ/C99HXxgdDfOHpK0+hQNl0srwLh6WmDjRO0ud5RfAiyvG5FFxMhLDcIu4aiqEpLcA
B3eXBVsxMl8Zv/Wa1uhyZ8GK47N3tOgp/9A8t0QSpUBnTGxQdPi6TvzDJ0jRyKnArbo4TX6PFXnX
Aig7qWsiDYAyYNbfOeOcAGvAJDbXL8DeecaDALSRgdmxbhrvMS2yc/m2WQU1w6nUl0eZodTxFQ//
DJqdxji/Awnsx6T4wZgcJhjUJRsGrEtR1aH/5d6HWineUPRCsz9BtkOb/fStqCDqmtI8hkV5JxxN
LXWEOcdfjaRjXz9VGcs+tFqbmt4y1KM8WeY2Vsa4JRD0UvDpIKpWYJENsxh8/507zN0o9zf99djW
4a6892kLqTA8QNJlOe2YhYObS6K2f+mYFTaeCAkULD+TItz/VFWQMQI0mnqaUkN7ZoLytYMJ2hNE
Plqvr1N5SmQSytvXO5yGwjR21q9AVk3o2gBdhm1HpdJQFGVl7W3uEGuDAeLj4Otpp3cU3aXkRrTQ
+BtIaIFYAK4to5rGl7zpd/xJUL/r4gDDSewh3Vrit+3t6h3PunhziPBV50GsvTl5YcqQ15ZMbQKo
aWoDDvDvEy9sFGshJVLk+Hlf+jDlG5oiU+FzGh4iBlsg1Wne6pNTtN6/356pwjHB3R9KGvrGEAjF
kKK7pBm7kfuud8quA+1azFo5OWlwQwBLJZ6wemFbOBdJ6FHrGdCR6xYKAFbvQimjv8WbdhB4GqZ/
bvPUP/CEbp4lrsD8EIt5cuBhyJyLN6SMxBVTmmkSMcAAIF+3zEg+KUc+8Agn8C7IkLWeSU1sculs
dFieeKN8GRqYmeqEN+MllNUwzOmGEfLz69RC6o2pOE9KSfPI+st1wFU+Sct6VkIusUf64Lzy/xRW
jmv7kfZCmi5vfDl0EU+9uRTRTESvCdQ+60cwdcuyvzzgK5dvLxSxj6lQFzWV5oK4yOPfjzY2glVQ
38XEqVOZFEKjybCVjZbTe3cD5r+akwLS6bvX6G132hgcnn/iCN1NhgcHW5iE+F8iYSplhRCA5t2a
HToQcxi1/UDnD4bjUCNIXYfuikZsjnfmRX7cAROstdPiMg+dyBtk7snUJBo6CfdrJrhvQfnzUZ4v
1/JGofbktwhRKPQJa20C3ONgUadhK1ZVXs/E9OqqexUeGwDjKtxZFXpz9Y7Q09v9TbMxkUozE0Rc
gKNcnsb16M2x376HqUwoK3X3JNuVfXItMYu5W0a2kHkaXYn925wk1PM57lDsK5XhFTgXc3Di39hU
936SCNq15QViSw7IB9chjagslaDEB8Yw9LildZLALvt7/CULUUkWaUobsxfgSsAG/LrDDsTWbjAt
1CedrHcmcjivGFGptBHIfKxhmqxf1Iwn5u/iOUtlTxsNZqp6k/LVYqfZQFDi8Cfx2wqonw9xvXJm
+OwL8QfGAXn9sDzsXzGQD7bzq7POYvfLN9+fw+iO7ioB94zVAvH3TsgY1XaBBajytErnpy9wwCSk
VFutQTtTr+I7OykBMSo0yi1XSqAzqZWZ4AR6fi7VGW78Gjgv/gNqiY+dW8xwTUW1TdEY5JtYOhMC
76h4J83Y4twb0MPq6gqZiM0oZ73Wnokn9Zkd/QRwulvT2NF9CFA/gpFv4wsTkQsyDSqj/MT3j4KF
auYMes+4yySPzREaaCUa9NVLrJfDRzuIe4b9NDcCwBBt6M8SUfGUtME7gJTmyn+TwJPiCElwesGd
wEEydldk1/HZ8jry+6t+Gd+n3EmARh7LZr5VJDuUQEK9XukNPmaE++1rWWaJBWCb4zV4DFwInxvb
sxvpeCeN9O2cNhqsFmnk2GvSMzP420tOehnfT2+2tVRkCoAamnGh7l6yHGLBhoqiLDvR9LPcrKvr
U9eEBpYpQjndeVkgvGKdtDfI/+SuAVzDk4bcOZp5s8IIZM2eHKNYTbQweY5U7TUWuPtT5pCU1coE
CuMAoyQwOnyFSoGeU4kyoD1PHiT9jqlpOZ1hCyiykdGo7DcKYMRYvPgR6MZFd6ByltR4nx5WkZ2L
2XXHrAlVelpRsePpZypWEC5VmAgcWyTLgRj0B8l86tQqFtZnGf0nzj3zCKSrc/IZm/VP9j3LOiFj
I5P6CRPOc9BKMG6OqMm6YJ6NCICWai1cLJ9cpkqPMvNJkKQKY1nPHjVup5l8exZZ001eboWsxmrn
cuu7322gAJuzdiSn7ASEua8VUawKGAWU4rFMTdUFZcbS3Qlqwb76rCzD55ZXLIaeSQkG0r/OLj23
aLap5Iu+P64PDbW11DlKQgdQsigtiLD8XY3kCFsIskbwMl8vdtQUdmBZ2ATRLoyLKOD4QSVJmO01
a9lCqPWTE4ciX80kKTf6yZi9jqc1k6Xt6jAN7KVy4odpt5x75spENgESNonSkgeHfPuMMrSyZCvG
xTTTzqYR6vnyoJh/o1e8pdmvKrX/S1ovrwVOWqIgDnZUlYvA9L7EJB7SnW0Vv3p5QQDpAaM5NGEN
eLdJnaTAYdZXZh8vosScj6fkYEAiLwIA5dVOulhqf2xG0VX/rD1EBTYlv/UQetwLIzumJ0O3pKMp
ef/DWKaNIlxGd3GSE7jtu8/gGXifMsCn3JGlez82kL55v1feM4SKeURoHmUjMWNrumWsJuLJ6JX7
TxOuDH2xKonrFcwRI9nn5bE9DfqIuOaHu+3JxBzayqk+ey+l6vH0p1GHCitAa8YvmpPp1gJL4bjW
wFyZRy5ufQ/QVC4tvzl5GnpUDXxYzXGxplo+BOpEhc/9hBAEMf2b9xHshu5LEQVa4waHtdnnbI+6
Zn7TcOM12SI4C70c4EHogTuv2APpBbsPFpwH8gmMB2TnkVWQGE+0xvYKlh5uQMv2vS6dQCsl06a9
j0nj+bl6gq0mTQz2DQgUCeFnlzj6z8VvkcDTKxuoqzcTNg8+DXmTZZXK/Oacjih5m4EXx1JBT13U
lK4GInL7TPDjL+7iriJxEdAdXcHLz4ncAf1+uvSARXrYcuLAcG5tHbzpuKneNHQvklcjXTL6tfxb
NMuOKnKiJu8jrECrraeFSeGIk9fD/DjOlGwBD4Gv/DinyBuycrqObhXjwoCrFDxKTRQy5DUm6baO
k+H+7G7pNRxcFHwhxdIRKscaLgZp+aNfo+1JWJgifLH7m+2JzAW1l386Lye1H6LzlnadjPacG4D5
pjl3MoGPxbbq6CWazGldPISC0xkC3yNoC9bWf0/mrlQKt59ZO5kK3L5NxfiA5ny5K90hionyTVTT
/VX1wAEwmC0+MDVjqmyB/GCqQ2wnPUmGkwLgpjasbFTwvei886fQmfzj4ZonP9U7m7IXy+6N8qmQ
U1dS2CJLetjibN6pZr/vJTd2UeYS45LsB0Y3kskTa7AlvUCKabDZfeVCNGuDuYi3j9EAnb1hIke3
JhQoxNCc4BQZblFMNgE7rPx2hEhwkVuh+bHIne1XTr7z7LX/UGYsNHOJbN/qhi6rMQETS68M5DPE
vf707lniDUKArd3OMNwLhFF27Ldiw11BZt66Etu8pldUCl23kRCBhgMbrNfidS4sqkdojnj93Tux
wYCkhoiu7FLNPO6a+OsaodGZVX5av63vUdposotLWR49pOY4hyV5sy40JNIie+AXR/qP/aPUA2eV
dMm6M2oTYVPSX/UAL+1zDTF9E/u/zXTLcTJ9LMwe05aTzH9+Q0R9/i3/u7bqXQGVXK8CwP8O5Ysm
Od3mE5/FM6/wgGIEC4NFa13LJYueQfrNN3gmDABWx5jtHIQwRlh9cV170qAGfWXH7ygbYjG5f8fA
6GPOoNznMoAWerfVgYNTpibh4AsleAfhhKRsebioXoRh4nt6tHDOiAGmYkQuDM8Z+Bh2Soa3SH1Z
EC0NPNFW+vi7rovuMAL+fzrV0rYUwL+sBbvKSvxWxHzrNYIFPCwXX4oofFkrNCLKAUg+lsa3o3mK
BDRsJ5BPupY7URcPFXcnb8KmGaDredbbZWmke6qYonxf1/l/8gv7h+6b5kiyKWaXkEjWLZKyOJKO
wXn3HBnJz0WLoCm5bRPXKlyBJt9iQetc2ccPyQKyN8oCuI4blKdk5CslFNmuiZAEUya0C5OPpN1O
+CnAuzFyBW9m0V3olMFwB803YYBvuw1yJRHIWrmpPXpOwTE9g4itxzIQOYvw9mRHfUIcjPdWdgzo
tQFYlug1vWTpOJBryNjRtoGSYmE+adkBqJVYEDBaEQESly2X0FAHIRzbluPkWtMcIIFZb1zFKLGl
UoKgHtCVqEy4E9X4UYTLyqCgHyRLihhqnHJJiC4oRK8al9WoYp7zswXT4pnTfHMlwYmswIAKLaPs
66aI43kHPKOuB+6PygFfC3DkPmJvM6jNN6NVpugWepiSrKOXbfovgOf8ZlbFl0c0FrQLhI5RD7i2
J7n4aC4oMv8WJRDlDH1f7PmeojjsjKYW+ky9C//MmNGys4E44233NsfvYhVacdk/AjsFNJjnz/bM
yxI73ficaZsUFSE80SWesPbi/+MTvJLpyxUndq0FmRrYQY57jEwhZK14kQRm8QB5Tq/8l4pdU4Ce
gUApGbkDJqxYrKO2f5UDA+/TtBVLGKdev+6QBeze2o87660i1X1SNqzyrDwmx7ikbE6hVjYHPn+O
GcqUyTHAM2bY3zSHR9boCcGEZOPLCMJqCo1zPOkZ7SXvGjeuVLbKX1e99Fgp7LDMQrVkkwV23HCP
NJm9MVG4CQ//1082v3nr5PkV2arCswVGH74i1v4Jv4UMzj8NVdIfkFpy15xcKNZq+HM2XuYJdGnu
Jn6s0/rYK463kR+whGfchmhasjh6IhbTyeFxGMVSxdgcub+PMjGkSwN9Rap/mtBKqTcvWOInfA/s
v9HZYNRRjkflMBaGVWL7zW7g6kFb7ptxW4LXwGEpOn4z+b+t2VW1CVPW1O0d4+QcHconIEPvWK7Z
HskLKQ2e0EOf9KSbQLGfrUx7rTW/wdfJ4n215KPGcv4rlUikdVbn9EkIB0rO9t46Af4g9e4g59qQ
uDu99Ck8kYn7V/0HWB16BHzuVuKSqA5/be0ICsj5Jc5wtllTLClwgpH6XOJw9A8G6XrLJmqWy++d
GtBNJcCHZTUVIVdjapI22nffrF3CLsXwTIQbscPc66nnNDuVZbLZYdZJ7+reWz4VjkPYqazarEEb
ExnYKRdbRbq9w+QDE7l/xJe+VWhX6RoPtXU3Mb/VmaDX6MH7/cI0BGGYolEt/duoze9HKMnDj/iB
2YWcFD2BdiUSQumqv7IbEEKkKemc9J7ZegWHbLz3FWiPvV284U4RlfZOS4wm3N+nvaxM0n6gx3Rb
wmP47zFsmDnW5+zrJK2jJXQBChZ+QrOU6TPy1DrxKfsECuVZvNy1RJC0urwzU7XPTPx6z9/0W6Tm
P1xDEx1bWnDPoMN+/k2Rr1kyBgczFID5BhZ6xLwZLG8XseJgVA0YEfElagjfJ4HtvKTikYXR361L
0ikSjr8vhtLHTNVn+kWseO+A3aZ9QyxIqQCdKYviQSLmG4FeFIA7Fem5Ehb7zvLc3xc18kT+6+Tr
4tX4nefeKp2oKA6jWI7B48zQzf0nwY2cHh3nxcFrxS06mGTZgDYG/72afyMowzYkXk0GK6170uDS
39wC7LeWqAzSx9U9b+MLL2BJMMb/LW8wlN1SZBksiyEWokz+eihi52Wf1/Fj7LSsZPg0Ej3B+WgW
pFKp7/vr4wPbKKY+giuKu7SW2anqyIHykBGoBs8bZ1GDYr/wugOZ+oRHAQBuJnYDWyTol3eC0wyO
qJmMuh+oHeOxM3QhHrs7jDscp22uLz+E+SxN1b7DlSeZGmtSFTHE6AHUko7gj0dC6QUg17f+cvYJ
pmNQLatojssv+NIrHSVvp0HO/MC8/FhxIiPma0oWDjCp4qruWmaYPrPp66hnZecxniNpZ6QuX25h
PlAKTNxSq7l/ldViLa35dALUzZJjR1FSnHcNALy9PUN2p+NT6g44t/tm5fed+gjPMZPaAKKvG/27
8PrpIZ9uKULBQs8MC6zACiDbSIPTej9yqqUzcCP2p8jYOPWdVlgHh1oSKUjDRTcov/CbaaUFvOjt
2411m3NED6W/2iF47vZaVNR7CVY7lDsaMfGrWOi6z3tsuCSRtwHlHHiQlZJZgBytArOpM6ofyt6I
rAcOCEt8g7vw8yUleVttyaIr90KG4P8oe3Uq59LmSLDwRHAYbBiwPTOaBe7AKN9WxFN4wh+rqp8m
MqWNJXf9Vg2cWcrJS4/w+FIcf8QzoX2VvHiTAkIrQffUbZjVyAe7/Ao6du4lY4xV1yjjyNLafkid
LEw3FUJMUX2AJscToNBTs4bCDIvHpz3DAMHDrSrJJV6v7WhDJBVLYaP3G1pnZ2tUcfE1Kjxt+KL6
WaTzF8QLxQT4tw3eUS0EmGHR0/NrP0Lgw6LQpvd+w5vehJjzqoXIjV72I9m/yQwi++OhaTrL79ir
moIBxWbJGN4NWwrSklhft4QKJR0Q/BMsemIsp4faNF1nbjshNZsemXeamFs9xawNJbUF8KTyJOHi
zcGYZBzdsydpyimllolZQ7yALqS82lcODhn9/8fouEyOlJap6nZeTwn/8Jtpp6hWLDOq97jHeHFC
HRWYVArwR4EHHrKKs9ZVZW0Yz6ZCpBCyRNPhj59P6Y5JRyqRX9rg4/FAYCFl34xUF7g7GHeUy4VR
wOderRgPSGTQ/PrAoMGJ8JcflRQFBEMFrkRhXgmkiZY+kZ4859lOS8gBOi0tfuWp/fDcEjrp9s3r
yrDj9j2DDlQJR48WTzpkzwBivwVAMFI4apRsPLmCci4oZyN5et0LcRl08NMg9gINfJHNq9DxKeaD
LMb0kua+oqoStavjSUi80W6PJVY5hRN4IbK6EqXlZm2qr+5KCwm6qfXhqF2iJT5ccHIyUozUmrD6
Z11SJdeKw6KSQ8eFoHmCl1HplHult+1fp0mt7W/vdBBd1sNXfBo/o6pP0CpW9k12595/pGW1yn6r
rwUZBBW5Ea0EXrWvdBGiDPitIbHzRLwJI7YV8yghHkZxvPt8JA2KEnhpdnhXrlngJUOgmVzDeA1S
QdAvC9Z4xBKNXyP7CJ+jnFIwvFW3gYMSMIuRINPVImDG9yc4zWEN/czzXUrKyC2z9UciEo5zsHux
w6rBHkm+rUSYP95w6lTE5yW1JH4DUAwdCZDvBtLKOeTAqJnShQmE2Or8MvcqTPPvo1pAV81QBexC
UwrygpHI9p79bJY9gtt2f5h1Tu8oxLmxjX4sxza/xRsh448BB/5d7LKs7TXil7jKX8Gmaxc6juO5
p/lfaccbJqrI+jiJKS/E/bRKRJJUdIsZQf/eXmpzCEOh435SqxpqBN5hXfEr+Kg3kDmPveSMTPfO
cE4Czvfbaqtt2DMJpO/a4K6nYuFm0cXu2drazte4mqH7BHwjWqPc6jgwCxrOPW+utN2+bb0NWvkx
xkHE6w0YduT6y5nhuQgRwdPnRntyIUFLE05AYiXyiXsiXh4svmLBFkThqzsSZRQGHQGCsIXFi+Cr
5YSOKLgBvbwJijOZAZAzfG4VnjiwftlIAx9mOmtHov76XQi3prCakcaWH9xpbXbDspSbV5kWwGwM
7bQPjAIhIS7ZHRrRviizsQZbueV+vX+pVbO0HgmL997tZLrVkHFmcEn2qnPwyTlc+rlHucc3Qoon
0weHvzttunEg9UdvqfDSWxdqBOl6Q/Oo9BCd8/nGYipXkchEOi4k9wljoebsVmKotj6gEBVwgVgt
5yZhJ4j2J/wlRK12LpQ9lFAO0PF3ruw0lJl88oJdchoQrK5nJqyqaGndByXZqEfJty1YbvsPqbhA
r01QO0OI24N71WFNyQzXZRDYrEScqdJClRxqeIjxtoW3GKunVaa+5KXXR1BNmMauVaXUO6ZzDdWR
5s8WxpekzeWnY0fRwggaTLp4lkWdKwV8cu9AoTgQjoL+s9KoT0j5XmTksXW/bExcuWXVOrXOX+Ya
yOvL+1Nk1DYjoVfTMBJQE4VWZifCIbEo7NfU8sNTd/hNSyECFJcG8V683FS5RuMUzmRu3/tbqUUq
xp74Sckbhk85j/ezjrEtqdzMGE1TNtTWnmbyD/vO4OAooqcRBAH2+7bON2/0n3bR9S6k4jVWwtRx
bKtciPrsELECgmvn2QGOIusDZuv1ROYEHk9/0An+e2zXDZy5QIzOw0U7z6sk5xuYLbYJzjECklOJ
BuP+ksXJPXPhDANCtGQbuBGhoX4BWMPC3qv9Jc/KaIVkym9nDtiH4JpaJ1+NFCNSTvRqkjt87Hzh
2jTZGwjIbfcGs0W0DX0Cet/+yAXVYyHeCKrm8nTNVUflReRtoL7teM8iGVVchiCmCrZY9/PoWo4Q
Bwxm/Jh2nTwoa/gXjqX171UagpJAs/yqckUaFb/ODTLVpC2znFGd5bMI7vAIMZNHPp9ZuFPTzVil
L86UW2opTzkdogMkeVl8NL+iSVEWutOnGRVHqLObRgormBje8WwVSVhUbJ+dayyFbMxIb/UQ8tx0
b1q+1lG2rkFia2P26GyNATmh5QFGJWBbqqrNP8Xhnu9+AHnlfmmCGd+jpgLDuHmHuDRJ03yaTiht
yumPGX4+dxKbn2cua8+3TcGDVtk57iodHk4mym1BOAomJRm9MbygJFYhfXEsqOYhNm32gPFR2M8g
s/MH2Bqi+uXnZOfAuUOR5NjdIAH8B22meYxpp/x0tTIS4yWWKD1LqKQ+S8G5IJik3j4W07nW0Y1K
E4fdPQ3mxED0DtF967sBoL7iVAaTfLWtY4NOaRX0Ght9yvFIdBDoWu3TZMFHzr1XSU7tK1LmdtBr
JFv8eKdsohnnYwK0o2LRZ0qSmja6pufnJgETvvOvO7wcQVUKpBuButpAXMwMNQhaPzz237C/Ojnl
4pUO/+sLDOENXudCzG4zkQJ9dpaWyTZjAGsTayOH+OjieOp9eXgQkzHrGB2iWZqqKFQlkn14F4Uv
u2mXNXGpcMAjOrX5BHwf1OfQLO4Zft35Y31m3kjOOlZsuib+B2DpwerG/x3mQkk2fFW6LaB/sEzf
xcCoQdGVdvMY2vWdUaT0jd8jx9MPojLtepEZWTYBXXPJSm1EPBSQHqqFL2qTCkhkJA8FDIzLOqf0
/heDLZtFDIfLl+8yYhkw3wBQdm8bSPx2CI97vFwfj6Ldf8h6jUlKENsrcVOM//xehu8KpQt9/K4U
jnGIMrRfbYJEbkyUUF2mtLEudyzQA7XX7soFI+D+i/umEYzSML8RlHGjULMKCsOjGu9CaI4mZEbQ
p+5jq+4Vf4sCCi9SdCIZ8d958rGSWxLBCeCUJrziCWpvSEGf6b2CSDXSJXxenHM6ElOrL+b6XZ/z
UpMZiIBsnBtamp3DRGQxVtxdXdIYRafBKSmSpJouCRm/7RnFiap6KPZzTcwOT3frQ5RR2jQqNnwT
truwlXtsMGilkNEYbRkFjGENyzJ76O0s/NQ4bH2K8GFSoT6WD7TdgBlzkXmh99vmTefll/Sf+Hpj
ObnrdLkEuWfXcRGyAz7VCu9laXmY9r+fHAMFOyWVDHpS+yHWoVyzfKpL+TzQ+i43vv+/m7b8pirq
tqJSnr+E8AKKLgrj7zuVQV/gAed0M/nGxX2a7I0hjnykONz8vxPdUeo69uf3VkjNuTa2gP7Rujtl
ZIBh79ZXOgKWRCWXIucnAarKTO/MyyMJRAQuZMDUodm1EYkF4ppbtypcKlX2SoMRzfdUwDPUhEVE
AzRMJPdwTwvlwmXSMC+n19v+IB5LWK6x4bQwV7pCgWTdRwqJbqicOCxnCqlwzyr6H1a6w8T2gJsm
DbzLecEF7tVdXJZpkc+qmLPvQYXQdt+VTsORCU2N0M8C3xDDA2qQ8Co67ke+bpjUGT9Q+nvNurg1
ScYPqumUxH6z60XBkPo2v6cS2C6Ql9wQBnep42lIxykljveUIWc0As1CwYmPCZTHvDct2PIrHzy/
SUzoBYuDtX51KZloo6o/NrWxMnn24ZNx8f/SV+38TUr++xPsLXei/ZoxZRddU/WTnRCTYapnSXxL
b2VEjOUVQ7SJPqWNdbGLEl49eXcbsPI7Ye6dmux/3LdE+V2VLPfO3xn3PsrWXfimdNIIMoFeTmdr
IGSvDcS8a/S8u8XHsLujjMQ3LJWuDxbWqKtv0qQy/dSwSLZX0TERB1E0mhO79DMvFOaukcYPxsyh
kHW9+YAUTvFLj8zCsZoBz8gzuCd+dAer7PbhLKW8UpM79SzHhqsZNT3JRWCNGrpFmMY+u1wUdpxe
qq4RD5f3g7GbgiupX8nw9wLhvcMwlaCrd63HXtlPG3FheUmzZ3y+Q6qb40RkTnlNHSQi3ZtmNTRH
NgEi/VR9mGaTZHvssvICu7DOwg4mXCfKVGxT3rr7jnAY/j2uY7yxtkwvuI6rSuAa4mMhPRJUW9sE
t4kl/dI+2H0olMLENiDO/zEwYtCkxzk6IKhOujne2ocZmNmMsDOnBhrEevZsGPUzqS8yOF5jE5jI
2eHLjUYG5KnAfbuFdThgEJXh7rD2qDjqU9Ad+J21Hia57+aXhSgv7al3g3gaRI4QVDPX3tZBNz2k
r/VSHfOJ5wLap3ZoOT4FrRuyIXAFVJ3i8PN4N6BvmmlOYOPhl+RMB1X6qxV7YVCrmQYl4YGAlnHg
Zycsisp6dcEInNsOxMmx5TQne2XpRpbTFpyJKhC4Um8zueIi2NHLBbJOLgqgqtXVFiFJHT9CFrtm
3IiWQIlWwzd1DuKkmte/Sj7HoBGGoRQ9laJ7SCp4h0NsiinMIXFG+GOc4pxWIKYvTMboUWOjiYXW
m44VwWZ8PnPuTzQxk1VFaUyBDJ2rsZ4yZDiQ3o+Vn7edrOeztlbZWiKb2q5Uu+0Xnr1P0zaXKJrC
0sCx4tpy08tmFtJBKKExgvpdi0XqF+PRs0RTuFmfkM66eViTJ4RvMA4c/x3h8xWl+mqx6BdkA5Mh
Fy1kKaqGiAJ1dZ+PFBigym4wNI+pz1jL3K6dUob95FT8jWTKELuu2mQOkYN132zX38Y4O5bulO9T
Fq/AFAO8HZ2foROwQBAi2oPUxuHihn91rZhkSnrbgIP5sOIUqmirgUnDJeJlvO4++b8YGsZTD3/E
xry8c7uDPV/dOJ+ANUzoyUmS4NLRIovnF/aQPbO1hOL6GXNwuBlmg4jfpa018YfZoPtaJm9HAQ4a
wUfc3p3xH1Ub57xACzTO4A0hm4WjpkpU0TAegzMO5IHz5QcqwlT6g3UKz5dPX4iokIwYxLgnlFBn
Q4xVGbqM2YkYc5Nzqtg38UwrdUQpnwuR1qNCQyRlgJO3sOfHE18NP42nB7Rc+NuQxoGKEwIZ7Ntu
1SvPDhaN7TQhrxuVbDxcfrjjacfRUspefz+KQ59VqRbJ5zcLux+M9odztjWYn59BxlnCSOvA22RA
BEFLC5ZtAKHIQ4hb+46k88nRo35i7TiYu/xZRLqU25YjydrOsH5/vZ/V+9njQySxe7mLegldTCoL
hoOJ6keZTIJALtn3Ii6lxCxwCXg0JDVzZGr3rTIVKktdgAl0g6EYQ00h0t+UAhuJ3ZkpFD+icxZf
OFqv7kvOAJwOQ11EnA6YSghV31RYTRWd65DvLOhi//TBrKrp0aZYXY52NYZGw2ZtXs8vUT9eGlx+
TGMB+q89P8X0/tCcT6lan9w+pDGZV9un5PqCKka2Rvdc/BAihJvNbqN9g5MHnN7ylIIljZQMfu1Y
/5unxuNPY4ltAdNNChmAot/wyrnAitw3G0qT5phcKERWvBRNFeuOajdjwlqeQ669T0kmoza4X0pA
icdmGtojm/VpEUJ/XC4LCloq8lJql1q1ZGyQFoF90zCyvfZT2EuYoBBETZ+FYkEpAoTdY6XFXnOm
WT2J/W2ThE7H739Y0zFzpjSZAxdC3QaOHA2yvb0B3T/yizVazT4H5BNme+tPnq25dVjxElwc7qZA
q3qnvvaq2BxFW8jpesrIvlWDGWHHmtmlRvqihFSULWGQJVmEQXjPXHNX5XaAK2xIK9vB25MoLAZ9
SPy4Tx7axxJFZy7qwXcwdiFBBLQe3eZxFWwDGwItx4VTX+qGxLFZT+FyUkz2fHu6OVe1hl61dwp4
dfLLviErnk1GijxVi82Cs1f8GhfYWuPuAVEtU2AbtMVcHgKMYuq+++9QBHYhcYm7i0J+p9NYTeeZ
e54VzbPyhQSuZPdobP8M7qjuJMJeMPoT2+MO1H4KK+V968sg1PWvKuu9jQ+37cObKQlMBXKpp5lv
78UYYOvn1NJNNF3E77cePSXyU+rrZUezsWGUBR706oipkixvAKoKJ4x1IUFC81etFPawHcfw2fM6
3oorMrD/RWzB3JkpHl4GX72zyl0nsNN3fiJbxnUQ9Ny13Ys8qL8sOytpmgrWACVZfVGRu2yN+FQb
QZ6duXpRRQdY1H7/OwphZhouxeOTk0zVWEVat70Z6kITyLMseBNOSznHI8U5OtEgaoQTqrfPp93o
Refk6l2S3WghMUU7dDZxO8h9xtYWy+ukdLJPmOl/6eNfvNlzJfhI9AXME8LcMOaVIWBz7soGX4B/
6kltkxqpfMa2kAaXYN0x8r0N8rU9SN3TkEbIFYdI25O7hadzNuEI9j+cEXjhYxorIOdMzdrJmE9p
CzksODscAbdET/ZHMPy6izWZmjm5o5ztjIYmXwOE2b+l4tQctG6117ZrusAMQjY5w3jLI+jXEAmQ
HgE48kCn7+rDfMf8rs+SMKM1fJ3v6zLxsJ69lmJuBP44p5TxCaXN2RHBhYIQpisycGtoz/TvtLgF
i3uUIiFbmJesbfpKqmP2nHZmsFF5Tangdf7RHzU3lkd2m70bJMKCPcE3E7xzBHymdxE8XnUcuILM
uF+RJlIK59ZK0gbc38tuyjI9qa79NuQhXRbadp7n4CbSsCdGjzLRYgbaQ6P8snHGjgqmXT6nD1aN
zhejsqwWzyj/cwhKpngACBPwvKFPGDpFnb3wehfFfM6PTG6zyn38H2BURYEyUbM+2sCSk3vDC5Wu
04ZnhyKUKUii8Dst8D/0iU6eVhB9F8DYIPYPqMjH2x+Xy+evrICsy9K3QxHJuPTe2IryXUQcbO7i
gOF5Il+7qR5mDTBeMqhhnUk7Vys6Qt4EV+kejBVJyMJG+bSB0sy69TBBSYaa9hNbJIkxYtn84KFP
uq8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
