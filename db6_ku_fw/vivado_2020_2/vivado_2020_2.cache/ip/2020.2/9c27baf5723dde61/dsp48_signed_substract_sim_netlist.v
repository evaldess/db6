// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Apr 14 22:51:02 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ dsp48_signed_substract_sim_netlist.v
// Design      : dsp48_signed_substract
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dsp48_signed_substract,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    B,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire CLK;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "15" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000" *) 
  (* C_B_WIDTH = "15" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_OUT_WIDTH = "15" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2656)
`pragma protect data_block
9ZWZFTcSju3tut87WIUI4kaoayb27szI6tDbVPxWPsfVxZSV5ZncdZZF6bREvTZsxYUPMX0Q8usO
7fyzGATfTLQIPTX8CTYRgEiKIvB9xmwcbRO/ZcTmalaQgUN+CQOo0Bs1MgV3vFdEzo9WNmip1H77
ADIl6SPkAFxfn4XFttEp8+BwuaklDAfJiOWMmha6lc+7NNIbl45px2M3uGAK8u/l99d5hHa5OV/6
oLRGl1KxIRuf7TnkEMEVOSNml53sFvhsHFM6Ea6jDINH69SwjR9Y1UkhumStQQS9cW9UnulHFZvq
oOlWdb9BTK8FvdqGHPXrcZDsYOU0e0uzGpVnJJzgFxXv6BNa3V4VxdLOR8PY2sR6BFU/gerrPAl8
AnCB/4j3RGoKZ/rnGV31TpThoelSHXzJ6lIP1SjSEG4HfFF3AubzJKlUF1ret+Ze4FecAfNXGmbA
My3b0YKi7ekma/XRbTz9EY5DlE9MSk6XFID1dKWAkXi6Yah0ts3NnvK8UfNhtEoKDFGudZpeUEL/
Ne/wueAcl39YHGdaB2nI5FS9TN7vOvI/t9RhNP8H7e/yg2X55JjQfdJOCP8vjHPXHOkQ75C2LAX3
dCxWmmDGDxelUeMixHsmO4KM5YjRhUY4UWHsWmJXK5Ga1RpMIPoz7WvmnAzHY+JIHsTqER70q6MD
6MN9fl/io9757DtT+uqjK6i+XaJW8yKm6C3qMaNH0+nX4HlViyfK/GrvtGdRvVbTFckAfGnvT84B
2nwwGrJ4Ke0EahR8ncMp22+eBwgNMXO+Uu4j0VeXxjkYs1Kxx5KaZZzgnrbxtrKeY7rmtzIib1Jb
XR2LVxmj3XXlFA0AhErpx/GJUORvpyTJqx8GUe6ifdr4/bJQq5WBvwx5BuG6qyU+nrCbpdtlirwq
jZK1qWymjmfw7Bx3VXxByFR4+gQ3LbK86Iz8xZv1XqNgXC1U6TWRUZ6DsJZNIaVxUFGM/jHVQmRW
I5pMYZXmdfV7+PtHJVBT/mT7KCCYAEr15aUyokWvgtxRWGal5t1UsVFpdg70Hqxp9SIjc0qJK9be
jUzqonHikIMODqkfW5TLJp+U7NhcSn1y6HAjdUvu3D4+B8slRGuT27Wz53ih5tXCKNDLbAdEPtB/
fQYnlfiihNreVKENdPZ2FTB5msNMiqQjv1soedqlGilBjZ8Ki2Q+tEG67Ulxb3Ls8GLhA60lyfCf
spicscMUc38zdJDYdtzvoO+jEoqrNC/I10JKhlv8H4hVJfT6Gjr7x++GzOC3L+D29vYUT+Fn2TWz
cKCHhyjg9T0b/jOjxNvZ2iTdQYHOmI46kxmtn7jTNW2UdGhpRSBSe6DqzjTOB8WRWOK2kUkntKwY
abJDLzATJwB9FA7ene/OrtY1Z2g2pT1yxzYES0o5Rom14LIYK75nP9GM6i2ddzUN6fadHLyt05rt
0k+zfjWl/wtW/pVUnSsHgVOYcM+uXT4or9yYw7/Vsa5GpudK1LHKK4R58A5fNW7NE5rEP4woEvmj
Rm2xT3FTCx+bCOiPUjoOKk7H+taDdF+X0e1qKyG9BIwmQEJNjcdmUmkN0U4KZ/0FRghmuQlNbr3n
jVmIizA38db8QhVFKdHVqybZE3cEXLzZ/dCgBkyOO2fdanybg4u2IXNeSUbs6NPhtL0C2BiKcx+n
MxoNai1Kc9PeNiGAdfcUMYOxAVbzCjCfib9fXGYMHlK7gbgCMGy6PJXSxbtpvpbnS8QAXI6lyP9l
ucRsFZwsDnvhdfGy8XPDCMgjkYM6qeuvVHj5tlHpNggX/18n7uP3hFwedPoq7nsXHqpULOZ0NRQ9
LoADaTvUDAck7BTMEuySdUbrL6qi2PQiz+wwrYSSJrFtO5jC8DWJkFMh+EdHsivc/n0tCQiL2neD
Yo7inDTxB657S7AKsMiedbCm86WbI0cLhkfDDchlxeD0k9npAyKgYBES/7fSTApKwh9U2Wf6JMn4
LXn6ZQv2QDq9xA3wEAhFvPkeE0iBGP0Jjh9EDXxSxp1nG0qtdr/9XXNaqTQFbI755r/3XpdXfTDT
rkm2tQlTI6hZRClBPUI7By7qFkjsSO4VPBm+D6bwMMYt9xlNEQgzhAXT4a4TrgMwW+tdj8L9hIKP
fmX4HpXWplvtNIV4YzjabxNMBIiYW8tre/RTjSSIMAjTKVwYhij1y62POdEHsuO++L7Lk6INnI3e
F0otgx+rplSKk/PzhLb3VoA2t2OHsQAKpbW4I8hgiYrBF734iJEpB3cXnxhyC1IDpaby5pbvetYV
oZGjnLQ5iPEycuGVniCrrBMJJ7P5Lr6/peI9/jHAEdLPNqnuofZgvOpwam9EGLuB6G2/b9uAojZT
EsCJnTRam1uZPA/cBX+YlG5fJDKXEf3hDN7H5U37Et64fXApVKjWTui40oK3T1RgNQwf3zSax0bN
zGd2KgQeF2sehyhR+IIxF6cp98JPltdDXQmDGAjycohepg9xXs8sHC4RSIqPGT20LyypU8xeG/do
P0qk1VXvAXJ7ncFFojRZuRz6v+UoI62FqIUXGo6P9ZbJ6g1Jox2g+SQUJQ8NyVjoVG4ZSkmL4dss
AwRS8h8MRHSmBOBmpAa2FUB9usiE9tXLzr9rIf0NHWHxOEejliTeyvVO7V3L2yyQWMYSyS8vH7ee
IfkMKMtlsDgUWFypVu/1jFSTTUR9rTN+wM9d0QWaWe2ulZOTzmGvTt072nDaOUCZKTLOickEfOpj
XipDPpPDqYyb1AVb6y+JWE1AcPKaWthrDJU1UWKMYdb1/kOw5uHex+c5V6jIicAh76WwbNfWrx3L
Vt1zGkaL/SCdjo0uTKqwt8F1idepugXLC5a4LM4ehw9s+qTTCF8Lc4m8YGJk4t/VgBuZW2hfV1E+
Bt1pYTtEKkLRVph1rEjnvq+3iM8uhqPp55yLaZ/SZ5IXE0zGAK+Us1ktAHY4BXlyef60ui9L3C14
EUrEBx5VbpzDX4dLxp9QQ/nrf1klxPTn3hlupBndG43eyL2ZoD4teGmAs3hm1Ltm7dDW4F95Dcsv
VvINkalVYHud1YyAU23AherX++uZpkOm/xsEWmf6BdDBM2d+Yv/k5z/oFi+Z0SzTuz+dLCBLMMVR
b5NuN8iPEIi8qUYJ8yt9FKdAUlK3ZiIju7cp6YmsDpS0f/QMfUHFgl1oVdhVD/tD8NNM1pkbMDTH
KV1HJylNC7aQzX7NuCRg//RfIOhAu0NT8Rl61FlNPgV7iTIhXwbdHghfWFl9r/NbLCbtrCgcf0LE
Awp7SBdTf2/RXsRtRUzEET7YQIpgJqVhajCArZr5KWgy9R3B8CWaWPs5qwGndcMkTtUf7QHRNm9V
OfY70S6ctus1HskhovZw3sC/eqZ0AQqwTBAvb0bpiacwllhQTq0ObRHyqebJ9rlqjFng96oeZnQa
Dg/TJxr8Tmxyu00rpnGX5WihHUlrXcHkb8MayDucwrtXrABCD7Ol0OLJcy0lzTvHcQWhPH5UnRzW
EqUq9JbPbBba7oXDE6DWc6U+oqri4VclRK6wuFhjYdksyg==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KaB5PKlLKT6wTY0bgyj644prKx208smW5qSBgP8eekJtezZ65SZeXmqh+sBLBeIIougnYvAL2bsC
rLpY+EAdOy+4PkeWHh5EBsupy+xmCfRR+mOAr0l3nBle0OZfm3ZOHwRdkvoGJMyaokeqc/yQxAs3
7mxGRVjAPteZiLH95yFfKdClgYwke7tniQ5RSTUulZpHVE+kBqnOjt8mPE0xWx06SjMnLNBLjqxB
ZPm/2dZBSTvFNUcAWc9qr8Se7aWrwT2DcQRWbrji6yD8DvMkcwJSkuS4gyo4uJhniwlEYVdtr2Oh
aSH7LGITNypPjPkUZLtlRhkrysUB+bIVOhibkg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
6lFP0xmTTVNTrvD/6aECaXEzzqOEw9AnbEQXH1asKpt+DlfxkZOYO4k5EaYHRqTnUA1QE6YoInEZ
33UrK8K7oA9kpUUdGwdoLKG7ohnHEdS3rBCUA/pvTdwfZT/jrvnUPxr5PgZrM3qlHJYkRG+yCXGZ
4w6uwX2BMFxPKv4c8GZXGqoiLRAq8eXeAh96C+Qi0a8BqAEcV24aHpVw8u8q5chh3Lh4npS19JEz
d4Kp/6Pus+xO1HuyIFe9qYvFMt6VvdWrbDM/gGBhlZ9Px1xpggjfjV2t+teD0fizqHlkoqXWd1qF
pLCOOC07ax3mVIius0iG4z8oypjdDXyV7OJA/A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12864)
`pragma protect data_block
9ZWZFTcSju3tut87WIUI4kaoayb27szI6tDbVPxWPsfVxZSV5ZncdZZF6bREvTZsxYUPMX0Q8usO
7fyzGATfTLQIPTX8CTYRgEiKIvB9xmwcbRO/ZcTmalaQgUN+CQOo0Bs1MgV3vFdEzo9WNmip1H77
ADIl6SPkAFxfn4XFttEp8+BwuaklDAfJiOWMmha6lc+7NNIbl45px2M3uGAK8u/l99d5hHa5OV/6
oLRGl1KxIRuf7TnkEMEVOSNml53sFvhsHFM6Ea6jDINH69SwjR9Y1UkhumStQQS9cW9UnulHFZvq
oOlWdb9BTK8FvdqGHPXrcZDsYOU0e0uzGpVnJJzgFxXv6BNa3V4VxdLOR8PY2sR6BFU/gerrPAl8
AnCB/4j3RGoKZ/rnGV31TpThoelSHXzJ6lIP1SjSEG4HfFF3AubzJKlUF1ret+Ze4FecAfNXGmbA
My3b0YKi7ekma/XRbTz9EY5DlE9MSk6XFID1dKWAkXi6Yah0ts3NnvK8UfNhtEoKDFGudZpeUEL/
Ne/wueAcl39YHGdaB2nI5FS9TN7vOvI/t9RhNP8H7e/yg2X55JjQfdJOCP8vjHPXHOkQ75C2LAX3
dCxWmmDGDxelUeMixHsmO4KM5YjRhUY4UWHsWmJXK5Ga1RpMIPoz7WvmnAzHY+JIHsTqER70q6MD
6MN9fl/io9757DtT+uqjK6i+XaJW8yKm6C3qMaNH0+nX4HlViyfK/GrvtGdRvVbTFckAfGnvT84B
2nwwGrJ4Ke0EahR8ncMp22+eBwgNMXO+Uu4j0VeXxjkYs1Kxx5KaZZzgnrbxtrKeY7rmtzIib1Jb
XR2LVxmj3XXlFA0AhErpx/GJUORvpyTJqx8GUe6ifdr4/bJQq5WBvwx5BuG6qyU+nrCbpdtlirwq
jZK1qaEoUOgQAbSMWGub8XIOJCabnLYwWSqCRhLspcFjFoTm/GvKuIOE9lPgnHXOR6fKX8BzGXCw
36SOYQoBWe16UnsnZr2EsQI55Uwm1FMBkBkjnkkFVRB8H9VDNojKHWsrs68WwF9WKbPHt0sqqPH6
xvZPVt+jqCmtTNXVDhaOGE+s2/URzLXwkae9wl7sXeMpBqexN+NhhVIBK58lK0GB/l9bQkf18NCR
DTy+lycov1hocmpyJwwcpu5hCSb/HhNEdadUA3pwVzahK4UxErqo4qq6jnTdrs3Uv8cI47WM/Nwt
SV4bhgGvOKnKEK7YIBDJljTCNMJdnGQJMcVXYCrDG+ZAlD/6CKBS2UMTpKNEgf7H30/376lJi1zO
oyQJWFi26FzJPOgZSz7nDk0hhoe6CN86IjGCo4i9j9ZEJDn+U/lUyVQZ4oEHbJsLrBbh8+EAby3q
Ah4h3zqjnahZA9rp8NIMJ0IwGYiwkZGlI/F7hQwBO5l0jQUfAXSOXM3xZlb/UrwhaAAbO2frRUD9
Num6qy37XfJj3/jS7TehA7J+HMFGZYb2QWoKCJLejXc1kfbwA0F/Ox/wSF7u2T5Ogic3JzXk8WFR
Er6MyQj4Ki7i9HXCZ/D4fVPb7ZT7dGQHFU6yjB6xoIMgYvd6aH8vpY7WSh5GoRyqhmTbvsAp/nEJ
YpkFFmdlmvJ//wrT9Q2ArICE7BFzBGOrOhAHZRVH/R/qjHKmKi01PuvYMVnsNZ7XTtN/UF00/JhA
bfydNWnV5BB7VSKEQ8vwX9si96HFZAec/eqoXppiihNF/TwlrDtwYCz49r29kxBvp22aEFWQP2oa
FcJJ+3i35xZfeQr1sFeZ6CItYg+lxgseSP79iteoL+wVnfW6+ySEDCEfUHV+UYqOG1owgD19puug
iAp/L12LZrdyZmh93tGaURK0/cNt9W0f3tNwqdXKWK1/hIP3lJTjqYka2EEM8DE+iNRpj++/9B3f
dzN6jUTdNZwWHFfQjAGIReRTvvGInJ0b1hZhkmWmIQKKdqJN+VyTWB0MLFgHOtw4lF+hyuZhpyGO
u/O1pEVBTaR7lqocc0QSoBUaoPqsAUdFQai/cSMARKfRKcs/p0rSNyrKwHsbNXRATP1UDawsVavD
nXN2psIgBS+RnIWOTo8rPEXsvd75I/joKwwiRwkR+MW//PQLZOjI3VbYt45Qi81NwiaHi5ATZcFq
Ray+gROo/3XQXCzalML9f8pyFdO8n9qhTBhiVPETBZFLBX8Jt9eVy4Mnp2FBZcMstCqe5OF6Vwjx
pfsdQEFG1HPEYiPr1nr/PIbR26FzGF7pdS1itKcY+OSsqOb/EOwNNfCT4TbMo+MYFjIb5Q5t4l7q
0p6a5NzKb3xCIiA3/dYZu/p2xfEL7wx3GnNEdXpBF2QhdkQS21DItk7mHMCKG4q4+bqvkar7MbjR
unwA1TKtiOophYb7k7BzJL8e1XYr0aBa7q7W4awaDoXVWLfh31kFtgLjLY+7eT/RWQvPtbJpTNtJ
YzVdCJhMcErYaO19WhgP7XKw2D2wvGMAeEnMfVQUr1XS2eZsJBjhY2vQkDkFv/CtwWfk1eEVHMgK
EKqzzAVRjSpBiuvsORCjeWhXXR/sAWr8Kjy7TNt7D/H1Pl0lYt00EVFufBk7mNyBlRK77NdaBA8h
NeA6JL7EFkQJPFahtppmJ3h9Cz3x7OwrHv2DBVmlupxNMdvImjeDhFnlYJa00BLx5yuhv4F3aOU1
Y+xid0/BfsvHxJUOxTXYs4CPVtsAY+UzToQk9xgdjA5qO+WqqWPqtZ5ONLfH0RRVXQbkWJ0o6ANk
CiOCH+G9yUyzK9yYKZt4U+P6vA6g2Nv+cngvEPDjyvGFqQQRMIUI15vPGED6qRakVbKHuQTVqtqL
91Zd8O6+vUFt36lN+fGhYearLq388P9E29nSHkiwx1TT06jhGzqZkU7wttQY/HKK7F5uhz8RWRzI
NInXng+eowYMOVdEYLI6hpNGl0IRocTCxhVmXvH4Qld4OS4TvO1Fl1aripN60konz0YPjXmkx/yF
g70rhUhuS68kmHYTWLRfwVpeDHH6fvYhILRrwjHAE0d/sjT0xz3iC3RQSAZTVX+7Cyg3Xkz+YEcG
Z+k/BkSIqha8XI75UN/GnFWBrNSmcegzlW1tx+YYLnfWd3SwNR90q+st563XXqwOZJPw67zXitcK
gjZ+Wv6kEZW7OQAHOOdAbDzXd/GC5yad8/AGWkOTfPjL4M+1dEt/4cq1i0pcbcgPSCg6xv19OSmA
QvOyiu+HLAsD02/7NyHRt/dS/RrailhxlGtWeyrfVUXpAtcXyVXpw7RS5hPSRnniU9IjbIArSQqh
sGO5v9m85T7qDY8inYzMAk+Bmce2k0ApjbnLkgy3gOHnVEZC9H67HhBcsUQPp/DVWJvmhU3IwWCt
safuWuBdJSkS5jrswqlL8L6bk0GXizC5lgra5ORLqpV0Zz1FBPQyjKKZaEb11hbKADAUgXPu3GNR
OyaDHtp31CjlHOTarZdL0GzPASWpByw4t/TSPnC3XNtxf3EhwfsqIfZR/D0hDczSpxFOoLYaU7nh
VXAE01mikK9D6vh+KQZkj4gYWxj/iH0vvcbvcMK+yShMVTVU2deYZOvezEvYK2Yu4RemsQ21nqhv
HLbBGyWNxhQZbMg7voyQ6qxHr7hmnVQw2zo1z/gTIbjLEzme8XySeAr0br3uCosXEUww0le5VXoI
1TJu0jnNU2m6Kon98mkdeuGPZzFMUPeBPt+IpS+L7TfLpUHG4k3gamhQnRuBVf8HnxTQHVBkE0S8
z/P2TDD8nyLs7BOTsjakEJ+22PbNNnLBz4cDCqBFKxR+OkEla/Guda+ev0jvsV41iv3GYZqjloLN
wpn+bNOjO285kPX0qB139DbTA5LdN4iRght28VakkjoUC46Un5GDvNqWPcmZa+gisWx34TDG2GJM
7CRXMVLPFuNAbzfgfTisVxDElS0g+urxgdBJo8vXag4jUgT36i/VC3+kPBb09NkyzNfptStpF22m
JbhHypMGPjXGNnqej0Ik3eMIqFIWyCxkh+gaCdWzL58H8659tD0CBCnHPvHef+B6incTUBfpKfMZ
ryDHhLnIQC82cOGk4zX2TX5+Z2vk8mXJrrIw3lmm2eHs/10izdEaYGcS+WgKUZ6z9bAcvL7v94r8
6aQBvIEPs3PVI8w//Y6YQ/yQ7e6QUV4nrl6Vu9qBkANUT5h2amWbDjpPjHyVInipsgnP6HjCs+81
V/m+4QNxY1QDs7PAaFq7BcFip8PBOmHz6/qBLSCYyp5axbYhhxTBdiqjT3jbebhiu+XotjZpBAKP
p8PTJYYmF5DFM9M7on18MwOvPteLQXya/SIOqQ96Qzp9/LG39GcHnLKAaamOz8Ewuk8KoUOCk4E2
mtcJ9H3wAzMHzFvmdq5MrK81YBmtiamhBvopnJt2VE7F9efOcqPPQtGUcvDoEEJ/3vv5d1zjvO6p
NTEAbNAvGjffDQFcfB8FFex+Gz6Z3jj6TQmBk5a8e/mkB1qVX6fmZwZxN9N6BMJQoH9BUCTjeWeY
jJRPASmm3FJAZo9LPM/JZ4aXSARuM1aDNVRwylDCKYPCfi8pXNPHK3PbPF9lyEWVTOji6wkRAIhX
Xtqewfgn7/GFEQqFqUuP0156NwmG/1mWsolmb0lsKy9cmD1NfTCfeci/r6U69poEjNPIxV0Xj5J3
wzyNVcmSNQe9kvHSyPCpQvMucBvDosut9TaklQv9xR275B7aW9HI1uav9BpH7E4yUGufzy9LDk5h
eG43lIO9ceibOCNPqB9rH0EppeeQIMNaLqNDZNpHADAtS9GQ5kGtISl33Rx13cBq+9lplat087Z+
y1wDlMHU4zpj7seQfDPrWOCE6ZEvIZRgsIr2v27ASEZp4VJ4ip2K2R1LDwGn/h8AD8AWHtNnywHb
GI1hlxq0jbrvKWL+32nLNvePVHs46BxJvaKsRbQhMH+3Eskv3lgIxmeZ2Rlx/clxpbP7+gan9H2Q
cshdngRb1LIfyL2NoXyDeP4Ast9bFHVohGQvptJKUI2uUbwYqOtDnQV4cm2mi5pNVAnI1IQyuFfL
AIlhTTPIWwxaLxUWKWkREhukm5swYTo2HJT5loY5hm7jTFTsKCQnJq/R1JvozGNdcUR3H5Rf8+t8
0+tpw0RSlLRG6qWpprFT5zgI2RL3sGT3qvDH9CHfEkk+mzkLQZou6AYzfwfPStspIsfO8dCVGzrs
ZmThie05leOYIor5HCHU+1NOnfYkBe56qI6XIb1BmxLYiUzBxe4uIqu8QKct5GnlIPUNdvr5jnax
lGoGUypoE7IGmRAlwIZxdsS5witWmsNOV+X2e7K74a3HfScn9gJ/buc7srTPBMRmlOIjWZio0+N8
Q7TnkeIUK7JcxmLDICr7Z8hH0EMoKmm8mXChcoN4PC76kyZCpbdb11JVhrSu9d8vVpMpoJzJBvJs
DT7xlnk8LFqVbfG3oWaU4NCQ5R3/vWVhiY2cofs5I3SD+SpYM30zyclcI9cXZKDOOg+YvRcksC85
Lmi4iqrEXT0xQifkIAOZVUYGahVz5ZGD97Gu95xPiJIeh75S+natPbimJqe++u3EZaomTWj6W23k
ao8lq0X8DUZNl5VwZijD8LYdqW2qWrdnerBrJdA8F9OoOhup/wMDbxmBu+HRdpCIgk5cf7asOzUY
icVeP2Q+o/xEc573plu31Jp4maEU56dwGXMrM3LMQCAktmLIdYnGK/FFBZTSR8qbCHFyLO5FFtHK
OkVBxIeSgCcWJclfWs9qt/KdCbm49Z3GKgqNXMwx6FtJW/Sg+UnI+hpDxefWdMZ7Kt1F/glHr7qK
ac0HWsZAtoqwjkUXkHIP0lBm58Pv1GGPuJj3DMgLuWzlJm99WRm51MUQqln3iYFKI3lul0Ao8g/i
+CmMXt1CAdGkDXK6q4e7wX9LwMTsRkv6qNcwHSyH4CV8DXwHWLeQyDkO/CBO5rtqy6yvf1+QI1sP
jOGbob2DFMX5o7Ow4Bgz40DqLycLjgQi5wXe1PQu2vIrIfu8V3+xYzApqCuF3otC4eYl/IAjmS4d
fXbFAvNkVtjGMbuSxbQYmqJbzwOX9MXGi3oIVX4dGxYURBhuR9nIw9wssA9wiv5yy4TFNOg5efG0
/SR90x9s+atLGfCAPFyTos6kGGzQMouwoEKuxcFKWTc/teTjMkz9Yfg8wl0t4v2Sbe9yVvp1vmok
Vl98+cXsqoAHmts6kId9H53cLLY/LSODains9EON7h7a6BtXx1ERqdXb1TEdDgn8aZ0dOW9vWrjh
zy7QPfXlQgrNxsKDbMSV4MIDJPGiAeZq11wDxxUbupoS5NOgX093vlLBvaxDQYjiJV8NwwjGWA9X
KIC2eW7D8OOlK3Trfyei0LLMqIyZ1HmhOujOaNWnOcq/WlOjrJuuiLi5QKEVqnbhxfvRgyt5iYwZ
FbLGA+mXcE/OAF57gMj6bMpclfp4rd1kB8TrBP5sZhSqBQJ0WAzFxMCcwDMLMPn1W352ZVP6UKlJ
5//CmXFMJVRPcUQz3zRmggGcDBQ7jYjcOfQamdzYqWNA70y+0Da7Mlq+UFY8m+P5XjWMvYRAQD13
XSRPrwwV3PuvRNHa3EUaf4g73CcRWbkSreVU7K++cZMlXQNJk5pf41d2g8M/ldDTtlLs9QPFWs+6
b+d74xCSsxpY0gRftbmdJ6dXKzHpQ0fClJMOEQn9ckkeM02y3u4y2e9famOnS+6AXOVUufR8Cde5
YMGfnK0WHMQbJ5IkXO95BI1F6kET/evdnZdTjdHGBRIMbDO7KEPeETsoBWM9c30wb5CSFaaJVU9X
VZ/fdRuLP5Ox9Glx8BwSlfZIesjv76HfLFaHvzpuRbMCI2OO3F1qZnw5OIjBFaZ4S0ed1L50nWPK
kUpGD4vebyYxfyOcsR5gwF2SD9sjR77f7IG/YcBgTLgfyA2QrkxCVvK1JX+Y3NxN5CPQjGHGODGg
JSkm26ol9eqphY3cf7Z7CSapdUHhJ0bV+cpvmB3At+DOxLBh8O53Fif+C3a+GiQRvyowQ5lIk1+l
Muem3iqfGNouFfTOz4qDnG5DfC4tVq2HgVhnJ/zuwAYQCHAJ6oAYy5LT0PmfuolVHXp8+gpjFBUN
Xb+S7y7a/FB11j9qE3XMlgPZMSl6aCg9FnCRu8ivVsG1NVBkrEXe3zWX9m12cSaoh+BQqkHUk8gf
wWBfbKhyjtA63OPIDSahr20w4iGjG36C3wIIfOAqZpl12L10nJUb3cZU+C9WAE3xIhXcs0tdRdlU
ZSJfuBTPtMX8ywNiAjodGl5b7PNMx6tGXMyLcXzcKOQxHEGI0XDjo+jbVsATBfiEGTPC3LBPv06r
8eOKpblaJ0DZUKQdQTLhjuximPf929ayvVD6ZOnxaipYtTyWPXeh4MSkcR/glmD88oUwcefv4PdI
3f5kdGd/KVvfKaqPJ5EIKhHcSJ12Q+RDqZ57Zk8XzjsXmnU/CCv1zPVdx/WytLNgUsFear5SbedJ
TP+3gkofHARtBabQh9LpZkG7VORMt002+u96GU4gAii2k1xNHAYTjKuONe9KzZmYz8b4v8atFi5k
gwbGVkK6cjMf9JBZXif0cuNcF5DVZyIZPD7ZYRPxS9ldDIMpMgixisVsWlGYX84PauadmaYmqL57
kk42quswAhOkvVjwCVAWl9fup7dKq0kr/5dbuLOn75K5DtnvQrtVydMikOJA9tIVP9FuPPamW83U
vl5zLlJGKAwONaEmmO9I9bq2GwbYaoDzpEHjYcn6ZMSz3zbmFZXeOdZxrlxliZBxvd+6wug1awL4
U7wo2TwKejxroHidZDKft9rmq1lKSO2deltmqJLdRR5K335cel+VDlISnSzGpuGCdqCb3Is6DwFK
/2lwpq3Id6E7EpBbPOKBQOZMzZZMt+ufC4FH675wKyXLTSHiILTxUPGluJxJLEZt2Y0OntXdPW/8
Zf2HEF03UvIlyJs3znRXGzJ9dlQtF/H2mEtVo9weD7COmd3T1eXciSDIBZ2Zj4m6hvu5f2HlPsV4
BTusz2m6E6Z9r25NXZJeALq5QC46gxoYSPpE0B4OPc/ltf1yxqg8gpVd9I4Y5EXr53Lfs2Sof5Aa
1gLGLN2enqEkmVWSTLInH5g1/B4ohoDqbkBepfkSFLT8fSPVhdsyAeQcFDq0QocOhLjivMcChxw4
klwRJMCMn8REOnFjYpxFkVGV1EHs1DaXxiRKkQPIQrek98+JSwIyAK7dEMCtR21Ar4SQgqCtPaCd
8to3I0VUOrvRJ36pZo1y26tzI/B/e66m6IgElL4wL4zXCNlwQyJ4y+HEjimWbDxVAQg84JFTbhKV
veZFNqYd00v/kiDBOqEDKQ1UNd7fXkV90MQuRbW7r1mS0qfA+5LTgrUOevlkjXaD6QhNyy8N0vZu
cYLx3mUdov7pAt1MCUPdDTnnMxnoJZ9ECF59LdwZ4TW0EgPxnddsY3FvslLUOT+m/0Ppc9WN65kN
ZDjI/QzrwTD9UNiFpzR/CJlSdo0VSaB/SyodfOY4F9no6G/OJQoEoi62c414W6FEroxTvPZnr5r+
kOqkXvPtC/Z86As6NpTo1GL3aXboW+nluEMAedv/SJCsxX+T99bEvsmJvCFcr7z/8hkPBD7d3Aqx
c22B49vXAS/woBNGy0K0up2fTiaw4VJL24R5LU0udQJg/2NatpNCmt7XCbakTMLtSafomjXiWvw3
EvkcblVOXfOoZf/2bjjCZny8M+llUYABM1XJRLzz1lgnP0vR5fypZC0DBjr37yCCJ3dVRuNzezei
SVFuDUU591Trt5zvAPeiSzWhb3q7XG/BO+lX9wEaVq6Fl9zZ5TnUMgnRNKEQwyryBR71hy4n7ZUX
H6xTCJj9Ja4KaGyJ+3G3uZ+hXdF9Frbago8SeeqboCyMB3HJgYkJO+LJXeNqATR16bKGOm/ZurCn
TOv0/lUmGDN9Bp7b5CPkLr9CxKiVhjysbjZ16u6FT4fC6gC0inM/crBlVbRnwOdiaBByQlw9RKlz
89fGe3INTbdd2fgYvokilHbZZmgdnHfmwXZTFjgogMCCiUj2izdu85HNedAMXsavxUD8UXUbWetJ
PwEQRjx/gVjm59EAL9ebwZWsb9F3ZLuS4OeQJteQKHeRe2uvIGDjGgTlUHB52sWZvztkYKpudMeU
O0ocmV514lgD8Iw3jmIpTBpC3B5MomtltjhfROU/5EJAxnatV2FsZRm+iOyTMRqabuQlL6ztD4T9
7P5cgndU0VIUIqTukYkeXJ/W01cRjGx1RgrC3k8PDS2knEDag2pK+9qaNCzZR2dUJS78TckH5WeD
B/J0YF7ouQzVDcUTyTx+mGHBU9+MxlNKx+U2zJKImV5+GWx7oZEslac0p0UjqbzGA2gYcMWbfpwj
/evPurkLKBUcs0DGX4h5wkqCHeHs0ngs57X+mh+v7rxPoS5RZH7zpwUiIJL9+9UIfh7nlPEaYVIs
DuRidvrXXby4M0PsGtNxig4+TUOINPVbUQRGdDPhM9FSRWJtJFgwhVgHsemcmDZvIo7IgKu9Hn3P
MMTW3/lmt6ZZoIC9y3JzQaNOKebc8GmJAl3p0H5js6jH5JOOxWb2JTcMTy1+96mADOSeRaAdwuV/
w6DqXVBoeNBsQrWJqFNRK4Hy7GLMvXkUw9AxKNIq6BsGM0xp0V00fqkK+bghIYOP+n6VitXdUEP0
lHkX7z0s5uwc91pGqNH370elOWrNqNOyjLewWap7w1hskT6hFQ7FWqloTlBVvf1xMyJodi19hf7g
e7G5x9nbLK5yivtFo/ZNvfe2l1H8mtpdYY1D/QaQNDyKsK6wCkeiREjnZQ9Le8XfOOTI1uSSIQ8H
S3HUhMg2m/vDR5fEBMzr4b2Umi1WucXHSXeJfsQRj/pqRTAeqiPF7JBQBxYZuecai+jP+e5iMiJ8
0DFa+ypZbluHFSEwWVa52PcS/tLrcqFmUl12VTenFIJdmeMAbK5Wq9YNGlp9v9cAKnaAa7ZeuH/5
DrbQDuAaQM6SVHjoXEpksg/sLqCuYXbHuEyHdDmZqvKc3GcbOweghxzKF5NKvagf2uN6blb8Y4UK
CJWvVgKZO2Dpk6sQlYw59MH4NhSXKY3jSiIzf2Gp7ihUuSkWt8Le15BBgtLe+cMk3ZtRpGJhZO9a
tt7UYkVyI45kylA43SrCzZLegAki9P+DWbJnMQ0cbiPAK4AJrK1MxhXMfRQcbmyF984iQAIXXRUW
O/JH94ZMudPjNYoC0TbK8Hcy+ZFOZA1W1PV+ModzkN6ErXjXnIR3qAVXJaU8F2VeHpopoEE/zJow
RYnJZQXjmc4EtCf6qaM/zPGp9lRLvDPqK3b5KeYHcd3cLj5/d7OSdrmQja9ADA7pYjD3fN3L85JA
cT0U+spjDyZOu6q/YewfZdciPl1rrpPU7YTCOHHUK8z+zrHStMZr4kddLKq4v/rvL4DZv3PKU3uf
luUFyDLy1XFapbXeNBiKLm1QirwaRg0xHhvzwaY+RVKfAfHN2goaGd8j54n8BzTJL9Fi0rXF7Und
RQyG1EDGHIvd5QiUAJWeSmmX6XoHp8asV7UtPcHDiJHuUz7vAptLb3z/U5/faGGtACVIyew9Y7Oa
MeHid7XmujgtFNpLjq6QaoJLoqz9DHqV8v4RNGB8QBde8hWVRm9T6JdvGpCsorNcUcx8Shk/5Qj9
aVQQ4oz0FJUndxRRn7motqZCh0qqsoqjIFTfytc8ap3oP+xKh68MaP32RrGOKv274d6nF3+X5oaO
OU2A6vC/GnrYdxdSD8j0FVP1aYLzA49KPkfqKTRpY7A5W55TAgEKma+eGkIeCMBPD1k+1Jjx/igv
3gaHPkzycYDt8zwUbAjScl396vkdUl+Sgl2et/HJ+4ixXeY6h8JHgLTBjZvmQ4aKGerfnULQZ1Q9
JdQTkrnbnjpocDtZK5sIwVAiOXI8h59iJ6rjYfUOP1pLw4HZ3fkhEMyCXe3NnzIkPNNgRbVeSGwe
DVLhfdNlsIgNQt8650mZ1J9p9T9K9lvG1a6KaDMtjVcgMVuPV3TWIXaDwQFCdNXmjUl0ANRFtfXA
xJ37SfOoTKcvHB93eHfb+o0o7zmDJvKq8GCgZ1D2THX2UH0KCyz/f1BAb9vc9TTSmHPFZuh/JE2n
61jUbCNoiQBq0zYTBuE1ap7X374FlmJPb074yH0+B8NLNHwHBUjnH81zyj6bWXFJBEHJKurUjS60
iwBQKX0zyydWKwToSJpiaR5bL3lrcjELpg6Ffl5NNJUQtCpm7BKs+PMDi6knnAxvAabRWKSNlXrC
VDsBE/wfiRMidEOcfQF1HOvm75VBGpSJbBFDXTBiCtWRguIdJnlV15zpRZbqVn7uHXFx/DLVOPCd
GVilu545nDfBJoXx/qzMPysEPWvtFS5RjpMWYDfHCFR4UQyDegPexvoKRMREFBRcq+7/nHO7mXXR
PJLnD/wljnRVjaMsmpRvjSsTRGo29zxMnJPZfIhiVCVjdW8hmhFcp5WPPrz1iAQVbtqKuFJ4B9Yx
3hv1o88iEbFzwiT4+jlP+EjJtSgF7vIKBBiM9Ph6NcgnTY94FzE2TKYtXmDF4fDzgIc1PZzskNH4
6/U49HN9nYDzJYRa0s3y0AZomwc72CR2Z+pNtroyyPV1w3v8EtlVGkaSY54ku87MjsKQTpziAXnW
mJkLxnCaViXazP5uW3qdnn+WdBnxP4d7CUNGz5DUx2BbSefdW58C/edShjT6cwCaX9J9D2pDEBmF
FnYzBKkwhADjdGOf+czWdNZFNRyRAsK/ID0QUtqnMXYcy/2JnmdVwiaCASKC2E/mQaaU6v6cqeH+
ZwTJspenZaSKrhbRgjjqcY1athh2hAyjdo3xQxvH371LwJsFQ0+vvimkWQ11CRB7U7cuiupGhgy1
YbTFxICKNxRzW/H4ICRTgCYf/94ntcaOEq2iBC+AQDUeWDCoDg5O+OGeFqTY5IZ7dyXT8GHmInUi
o/uPHkp+mxUhwcWfjCadpPgu+sZMKVTa9k2ygZ0OqjpnoT7crBZQiNIymz4DBBLGVowxrIFEmPZA
MX834spjm9i8h5qEeO5fwScWaR67TfKOaDYHCQpxf/sLshWd/p4K9ZfNjmjwuMU66iSYmlOwZu3z
OYzBq+YbI4MzLtYi+gsnu4/yBoc+dfEAxqH9ptKhXgCLfaMKs2ALggleXeSNy6oMTW6hsJrMppqC
cc2Q0lQMrI9j9pZRtgD14fJWbAKCdyAzhqYNs55nJOGeZ3LjQSgYbLC1Rusmgqscyvl1pPnECcEV
+bjMLWFN0w0+3Bczy03ThctV4skKrxGTzb0ZfY7XhTsbnb2rzIYHj4cWu1zzaxUe0dp9MNLtrQV3
dQOOpYSGwbkd+bSxn70eqlzEd8KSaydyJop4pEE9M7pGiSVzZL9smdrSF1E4SnTyM9aJjDiadGEx
pMfnj5CzbTzEwC4Z+zmNDDxsGdOBG7UQOaq6YfL+JjhyYA886GTOE21MpO0voeAvY8m+30Hl+OuY
0rLwn9fiDwYN4dADB71wUBMDh0ZuT1sghayqy8KXqtefXo0YUZVqdA9+4AJkfQnlFiz/QeC20Ibd
vHOE1IVWzUh4D0SmZYhuURItegRCoqI0yqTZrjW5rmK9a13Cw8dLsvb4ZO+4BhRM8i7lzoe9nCH/
cMDBixdjR7cw/KLq4jImDDMospjWia8MnISp4+iptQPVgX2iF53GT6XZI4CK3uxejZLq73U+p/U6
JkR5/FD6ctnMSWidf9FW7UuzL63WxMKcLaa45LL7fWDBp70wtoPZCTb2yNRtvNGg4B10NOstE9zw
z0qfN8LpxmJHli48y3IDIp4Fle514YEb/vXFYfBDZz8uR6al244klWLgm5Riq/dhJx2CjBqeWtn7
h8py2ymODa+7c5lWVW7X8gevjQJTSHvGuuoYBJ6uyKrUXDcPfQDonKega+e2KMxJTZs+bQxiuqoy
350hZHjhTXhElCJp2lWWA59DimQ/3rV21BjqDuUJmorYYPd8Fo6itBrgBgqixuKyE02JQXd4/u4Y
pVqW8s4ALDuiv56mSxgTxjkCCYRuEDwUtJin9+aS73/+flv2To59JwdHGnFe/xaAzGrwKI0w5EHc
dWEA80+GQRjjl7znZle3HFoaAMI1MLmLe521Qh5Rm/P64bdKiUCMGxpvJltbBBzdhM+LTsGKY23c
KeR/u45A7WNgDGpCNx29OLnCg8eqG39RQSVV4cklaDdH2fii4y/ZEtp6jPRj3O50JKFRpqxNvYFa
99xDJPDgzeOS9FApZH257CeocmbsfixZvNQpzw4lJunQ3T9py+GcZ+3/mK9e2Fd7J9ZpBK120cKT
PK6e+ZMVe2vKVPwoVEOBRwwBVuqg/2a1Zbaannuj07YbcDzDy4Wae+k9/1xL+WRPzS1WMZS5MTCW
CDi4EW3xotozwzb4roeaLjyRjG2pVgo8pvcMWQ/88YDfi7c8ReKK9KFIoD/lfNWuBwnVDYf5rh4c
48Ktx8pG3ab3OpCh/hXxO0WrCZWG8QLYl0ASnx8vPyeEeFd6THbkusr6mUYEnxDwtn5n6OTLF4Rt
uyg3JfQCA3sAjSBTClZHbdWBYrXiTjqZ+TekbLrlIS7DVEDJZ311xWJqWny1NBvwrlEKrOSnYfZN
peNd+Ek6MNA6zVSeq7lxSHkS7UG0+WhkjYa1zOGK47TY94A/Fa/dC4aDJ3W0TuoGaO8bcdDESrVi
wgjHtZgu8u7y8iDzwP/7JaE02IiOXY+Ea/sQl5if/P/I7Tc1zIMJuUkdKpC3W1smO0l/Ed2vngM8
XIUQLwrA6yQTNN+4wn1+3s3AIP4mHBgUbjaqWzCgqg6db4j/gk8oRrktgwpHHoI16up7GDIx7puQ
4CcEe+voc0EnJUVvPM3OlBGl2d+Lkx9HOEinXYDu9dhSL5/MxfH+ATRTCWpKfeMJ/dKdAk4Xy4Tf
DvW+zpf1xUa84V/FoM8FqskdI5kPZTNIfKSzerjyr59ysN7+1rgrx+6CGtpv1/EsdD7JsMn69XPX
bmZOnmhxJF/cAXkc1Y5pmrNiK+2unDj3quXB+6ZnUmB8UQ4eMKNq8bh0qUqrs/V71IiuLJWn3KWt
qx9D6HFATy1NzvnyTw7oigFBPVmRig0svSbZq1+De0jvFWPPb22bswW2xHOyG22QchwFAaB1Wt4C
p+9xb1ZaLMX/9/MWSrso5fTQIPUrfR1uscIuyB91QAA8LhhiED6xZj6ZgA+deC1IuzWavlujudHU
uMwHB88ydc+946K8Db6+Vm20KKNwH6mteD8JCPsLimiZqkOcZinCtu2xFGXX/y3kJ66udWDeMxnp
sC0ypRhthkIRYgW6gGPRG6Nr+rzkugfkqYze4mRPSBmjn73EJBhcx+6esOyJXIOSJLS5OZ2blWD2
PdrUKOxLCpAhyHpb3R7ACOoEnHEt84Xil3gPdGwW9rHgYE/de5ST/64AoJttHeRAK6mAhNf6SAiT
sJWzEIv9F2V0O+/XIsq3QzdAZuDEFucZHXPbMMW7t+QX0uQtCp9iu/6CLzFY/2FKphgzku45X9JM
M+LU3moVnajpuVTp9AS2JyioB+e/COL6QPaI+y9lN1BOwMcY0kqTGPyE/e5mzqgAJ4+D3gETGnpK
rWohkGWNfX4Kk6ejoTJR5f0Dz3d9RPHWmZ+3OjSqqFOFilacmSWslO2XjWpraUqh7D/9hkJj0qyJ
5r9WsEHSSI489BEk1DCVjdUcy9Qj3oOP2ptMzl67cc+mdJ8pt4kvsp7uBtITQIyFux1qlTOwJywh
B5x/N2QMzH65+2yNpvbX5PxCSLPyTlNeKXnw2I6x1NAbIAvHdMQvtLhzSpSUg9tcg7+4XJsSOW3b
Hjq+TWem7Wj4swnX8LfGzKquwqB2u/yGTtoxPPtaoF/Gj/JLcP2lY5e8Tk7CnPGzxe/nX/6Gz3Es
VC4ZWOMHnNWjm5UGtu6b83WED6uzM8wYrrNIAJfcNaJu5mEzAcYGYI+TteHSzF/wlkhVHq5G2qBY
TgSYBNUyJ6thPoGAC4Pmips1GkS76fm3tNHDlbBl3DndYCrNFGN5ZbzYNx9f/n1g5vMX3WND1NSQ
B2hzMIU9ObdJx0VM+IEXQHtKqaGQtVFECUMjYvZg2c2lFLZ3wiDianGYRIWylQF8gS7+Uenz3Xd3
9kiWtsh5u/63CQ3lMaySOCywSXbwSkAYxZh4+dMy2V16rFj3BUfYL/T7o7fxN9sGEdX2v8LM9JEa
akGFz0gDFudTjk91deLnT9TqETJ/5XQWPY8dOr9uwDbCAhxaV1C7BLCkFgrq5nGJEXE11rxpAaaj
R6zz4hsUrLBo+jfKo6zuamXurKdeaM+C7wcKyhxnh9W1JjSu6wSJsdx4mOGDfLEfK4Y6Ia+XXKDs
+WIoEermQf61ilHESKNGFCO05zdVNg5IlC3ZSBnUxl9qc4SYCHt3+msd0Cdt/l6EfUlB06Rck6QA
XhCLxGJGAI5EGW1U3i27EKS97lcVlQKxxH2ZpylQJvXbKr97CoxFODjErk5W1MBFLR4p6iElVaJT
bfSkxfpVUa585grepDHSTHPXmHUZvi1hhGuH3DZwFhpdlYWgX1KcKc2yGgUWmiXsiOGw5FlW4zgR
Asb7OUHhlDO5blM8MmJknSs6Yc5Tu/6QNEiFhq5mn78raMNp+PNb0dfYSaxPQPG/+IBWsnQT8m0G
5Xs9ZQy5jbciQ/kTQhzlx4Cx2mZ7Sn2L+Hr3hY+pbEs/Le0ELIGPh67YRHbyNknK3eD+alz2yyus
eIYaA+qj1PGvyr8jD4QNKRBjNI70ahu2Sbs7X+9LdEEqFzK5KAl2Obo/2rwbrrozSmCq9b9IzjL/
ofTer+mxtQ5wq7TzeSqgvXNXkeBMg1val2YfHLW2HwOGzBEl5oFN/osM9P0KIIEweX9HhQm5RDQV
jBvmT8VTLtUv1eIOvD3V+LqqNWPWbm+Ay7BhjINBVimamVvIHU3qzy13qb16nxoYNF7FsSjjjI6t
r8BQ+h3exxZZ3h8cMBubA6U36FIvIwDcjG+8VbIGTisQajaDWxpaFd1abT5a+Mb2gU2MUP5dEmfn
s84xUHCWbbCrJiu0hOcz24Mv+rRcaOLSVu0XZjkE99mw/c3jZu1tdwYy7P4BcdvPVNjf1Ruyj4hI
KBaY3aw0CL6h7BmFpaIj1OlZgBU1PjMsoqoaSxxPrFg3KanxU+6K3yHqjNZaKF5uze/qpqsa9MpQ
zD4dkT+QmsyX1gkxZfkdSTQ/0vP4WwRe263ogN1jSLj+5ODrrk74wANrXm0gNJscskVRCp3LI3dz
uOnMg1LqXu9wwVGIIWFjp4TBQTorxJNp8tLQc5Yh4Pj5r5oPwWUuH5GK3/kQ5VgpXAD+oNyI7GuP
eCYtVInR0+eYTNNsBzsmaffgePx67UT/+xgTXM9a+ZnXqNkv76DNvBQDO7+j+Ewa8ZrnqkQlpv4r
3RO1QEzOFauq+W4PDCbl9KfUcKhWCTjQ9X6M6w3qm1OsLt+eESAkbEqTvq7w66clq5dvUmre9C4J
2/HcBnS1hvOfXOjGT4xJxJFZA2gu7HkRSvEgxDJ+uVQq8MCtHhUGWBvFHRGwX0oK5JOxVJaWOu/n
CJ8WtJX2B0nKGeAIqfkLvAId668itxWIFb9nIwzoUvwC66ZKs7xadbIxeh1PvyxcZ7ak3kTBi0uo
/1wIVFQ3/SlrXXYG2RtZ1Cfceo9xNAFVNKc7+kuFT/vMtfUROsq8n+sS+scIM0QCRw6l2woXn/Cm
XkMzPhAV5t/B5gihQxi03B2P51j05KAllT28SKogKAZ/12/CRDv76tp/wyhE4AD2mMkhgk7dC6iZ
aFR+J4Es6VDMSDXVXzHSZHLSwPaW51xOfu6k0ghowUNzYWfA3EGfKaJgmlWlst6GXt7HvKRj5GZE
bDNFwcN7AjxNa9I/xbVGaeiD4LB1nJ/SRI0opanax784UQ4a2A3mp95oGzporTuqq3AQTHnoB3gC
slmD67bMMf/eB3+aOQPXhA/6O0N/rUE6jELVmErpPDgPjlDrwEdNIGObz8VeQtfQHIeH7uQOl5+Y
11doUkspuH+f2otWxWhjlCYhOvYUrzSa8ZE2qx0bQJJHurXZyFdexWGtMj7qa9FFfl+1YtY2atpZ
B3JBWVK71JKzEA03xlG83MItieh7VfqdVKIWBDP4hRrGMMWqdL/P
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
