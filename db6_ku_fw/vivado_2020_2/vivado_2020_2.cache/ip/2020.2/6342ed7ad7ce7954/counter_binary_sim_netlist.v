// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Mar 10 17:31:59 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ counter_binary_sim_netlist.v
// Design      : counter_binary
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "counter_binary,c_counter_binary_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (CLK,
    CE,
    SCLR,
    LOAD,
    L,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 10000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 load_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME load_intf, LAYERED_METADATA undef" *) input LOAD;
  (* x_interface_info = "xilinx.com:signal:data:1.0 l_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME l_intf, LAYERED_METADATA undef" *) input [31:0]L;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [31:0]Q;

  wire CE;
  wire CLK;
  wire [31:0]L;
  wire LOAD;
  wire [31:0]Q;
  wire SCLR;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_COUNT_BY = "1" *) 
  (* C_COUNT_MODE = "0" *) 
  (* C_COUNT_TO = "1" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_LOAD = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_HAS_THRESH0 = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_LOAD_LOW = "0" *) 
  (* C_RESTRICT_COUNT = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_THRESH0_VALUE = "1" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_counter_binary_v12_0_14 U0
       (.CE(CE),
        .CLK(CLK),
        .L(L),
        .LOAD(LOAD),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2368)
`pragma protect data_block
05SPcSoDoT8aaOIOkXEKdgWy1UdCRj8YTPgy3MdIwCW97bCgB22daQ9SO7Nh17EM2K/d7D8gU+2T
FkG0NxDZhLBdV7Wmr4/SpG+92tIhzMNel6OxRpvv/MPWRJ/KVw9pjqkpwFeBlk1NOxpfropKpShS
3yEbxsD2pbpgpEs8pzPpOgYQYE51r3PXroqdDKQbw3pdug9+mXpKKo7/BITl5QindT7A9tj0AHkG
FxJdXW4Rn6qHoBvDVLCy2BVBY2an11ahWfgbocvSkuTw+F4M7OWEzdLMTxJ528sA2I1s0Wr22hrf
AyJpwdqLYUTJEwy2m2v2BppkahwimFwBjLb4eU5u/OeqPPsNlhik8TRPCnotX1ZfZ+72KkyST/5M
vOSWcMpUokwEhHXQtzBVKVXBfWPQjiFVseFNnHH3wBeLKyTKQt8a7NJ3vd3eY153lo/SpJH/LI4c
l7e+yk/+BQZ9iTafbecPaUHsk5DCB1+V72Vfr6Efmsyv8/wEwqmEOr+5CEwe9tzoeau4IYdlv1In
HUMoZxZBdAusmsWJjBAC/tQUBUSMwiVNZG/P39rF/RpfqUfZiJ4mAd9ioXkfge03YlnntnQt57cc
+7SXmCH7Ukim5K2qLjDBiD2hjELWP46OINGDuHkXme25VSyt3QOepviX/sIwDdAnYGlG/X+0iKz9
KirPrZwB5K1qZiUqS2/BlUBU0Fz5fBteg3mADDB6thvUvUjo7ZlQSo0ciGTyVY263fneoA37NtuD
eUfBYjasP4yTOLRX4ULgl00INRomsF23uF/bW3C6UQWSMVZwdX2TUWOqHUtosogdUqxA/BCVjZFB
ylFWD2e8UgeaA2kH/aqp7ICfkw6Gqtg3posGXAexdAptDEGPQOqmVTCmgWrzft4EApZFvH13RTsI
OXlzS2DPTYP13E+I7uE7r0xcmfLWLZ7qkKjR4zxUesSRmKUHE2xqBLIjd5CAarCRuvg6qiamnX/0
PCp5svMe/oEW5tdSww/1E3TK9t7Nbvxp/oPf6yD+Ba5wjFK8VqQf4hR0EqaSj/XaoP5xghau1v3F
AiIBHP72mUX5+GzuzVoINKkWWlY2bL9jV0zpUOJM3umRBoWa/CYbMzqWKltH9udRne70Z3jMM7RK
cwvHeC3FLEU8O4safBf/IY/oyGV8T9nPYfiFQonqsyDnjSkX2D5jrznEjuBezU1kkJ6IbZPVqxqB
biqoDK37eZrmTTfq8O2e76g3rW7sqA3fj9GULg1NabDdDOFeCauj8368oetLChhkgoyR+UxsxBJt
6D15YXqIfNfn28EvsIvp5+4pLy/PyUN67oC7qO5wcZPHSouS0WJEtbflAgVS1MvlVWgoKIJpk5eS
iqmN39hj/K3IxlyTpPgu58T7zUBnp0ppg+iDiqscjyH8ze8rWGYtUGYHvrk/dUvzm5RluNLdpOgA
wsVvsJP1ZOr8IGDQIadA8eER2TXwPivrhdD2w9o7hdBrVsRkUAHZgUTBdZwn5DYP9Ldr9nXZqmof
BBkdgbmGhAV0NONsfF7KT+Z8iL5h+XC9YZE5IZbvdoaH1X1krEsv8tR0j8b9mQsIxTSv5JzwdzLU
Lm+hH5u61mA3Ie9/RZYXqCQjT0TEuMUE9vpYL8hYTdqfj4xickpKSFKkKxblWCc7vrevQXxmz9DR
aXdGV77j7ngBBG9iBt5wLg9o/01ZgwdxNbJsLMQZrMae9kG1eoMkHSGzvBy6cHQVBAJxXoCdKqDc
K8oei8DkTZgFoBghUlLaojt0uLC9qmFvOT5gS280tC91Nm8hpq80YmHyn7Mklb0+KzTDvOy7ThcZ
YUSqGAOk2WaIdbHC5CB7qrHHy5FqUMqr282IWfc1CdmNtFVrq4f7kMVhxsyVyHvAusc5eRsXqk+E
0zUT+v9GhZlRkDfQ2TO6cGSreXAe4bqQdM8Yfw4qGsdxqxnnzIdLoyA9IkVDbNtrXVkBwle2FS16
s4V3O9dWSsE/Dg7qVhmXXFs4/i7eAN21/8TAWsPyBGZ2iP+/GgYnBHIvj2610eUgaTVm7EllEf/f
tfwbXk6yhjfHhBQRulKbZsSJaqfqlY00NNIhs3BLVbHc7p7oNWXwhNUwV+ZE7pgVH9Ew/VuIcWpy
aUXVEP1xE6TeI+els4jpfYlphKMor6ZrKmThrFBDQnjZ1qokc5lqmaCaJzGtFnDXpSe/K6AreY+R
uxwsVNmdI6DxIMOPNNYo8o4RwXfZUZ3JrzKsyKN1nIRKeRZnXS2LiovD4kAWKiREDYboXf9a9FIl
HfXHPgUGBX0sotdhCl1oYfQCyDB8Ql+y2yCVqCW9nxq4yMQe590cP+bjpJSyNJsinpF4vzDpn7vK
gKmExCDivNgOuRz0bXL/PnT0gRvrbVN2fWMwsDfayvrljO589LGeMfs2d+DU7tACjwtOQezW8uHQ
IuvtgA5tzRXcR+Kw8p9BVXOXzVzp1dTsifueUDwP7lKGXuZYEhM5V0IS/l5N1ZK94BcESTPwBA3z
wLDiKMMwNFM72E9P2UttYR4Lecuoy3o2FoF0VwMwS9rXqd0oWRpSzDg/Sfiaiwx93BuryGQPQCX7
uCNkk0vVBykg0gqQmTbF4XXINmOrgmJ1+4nIHBWTXZodswRaDDIvUBFpFWsZxZ6Rj6FbwhuMIKGF
mQ+SntHWojn7yfSvmBq4XwqTRXlGRUs8bXMXr2pvkoZsxLceNzELuWiNhkdPDTmGJ732e6ZXNUwU
QwNcZZxbYzw/lJhcxXj1wuMcQ1+T9Cj/kDF7WxD7GenTyPk/Lb1VC0WLWEGhMimoyHCagP8O17R+
3C0qF5vXqE0hcZJWBvLsdweHNoPmYZLPdg4FMeB4UrUJMDAOk8wi9+exeDIN3tarX8kuDPu//+nS
fWaa1dJxY68FPqDM0W5YeIJdS4UiVSKY0rKmzKo0nIE2MOw5RKFL5BXfP1Md4Ja9FXysoHxTLj0A
7oq287p81dvINw+OiMqHbZ4eA4sxfUNGUuUE5Jckscl9R4pV7NgKr+6TllpvXlL8VE7k2mnGrmxq
PxoTVSDg79WwTZRB9JAfDc6GQCcG4tcsTma/TJR5QxPc+DgpDliVzL8uF1W0mbkInoqDgvwqqH4U
X++E6vIOVFxIHus1SJPOfLFCa4HQ/xSqWvifoxexBw==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
WpplON9gajPqZwUKldyuoeqmBpIPSBxYcr5JWxrDlqNhqbxliKwmPwmbmeArplvGzrWaKVJ8yMLk
xTgTAsmnRg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PywlUwtIgAXcje485P53GElPqY0h5tEj5ZDYGG4C1L/pCl1vhbCpI4Lfv1uBUhTCUgt0vUUApdRs
K2IImoVdVbz1EI11gNNCxuGNEsj4QbnWfiiRUf8TsfVO4gWgHDJkD4RJc+jcEVx/ZrSadMs2mHy7
KNZCnUFKCidfdRy/hkw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
unxmOFx/kGsfl24PCNNEZkygDDk8LvPrdhRZmBKwU8hEl0IYKnNbmVzy0GX33C+cHqleOLdJYv/h
wKQu75v68Cl8qlEV1Vqfa7UnK7q4w6bLjBa9BHtnG7S/H0Ywr54xnAXnSKvxTDfYX2sDgkcwSXoh
X0q3YhQRNlz6nKs2p675XjlEojeW92VNoWv8pHj8MG/qmJ8VohHbQpf0YxozMcZpH0CF/Ozm/fua
Vyb99q8DdEkMUxP21j9+F/I46Pbkcvq9zC2FY4Mv+gYZfH461p3qA1P0UNBQRmRRkOCCOAxz3PHk
qsrTTWDzAK0GxdzwQ7cbJFKBbdBVaV6+4memyA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
pA50PpjJaJ8uV4EV7d4QCm5ucA0irsAJKsW/2yhM7uxfdfY6+ycy5Dlu6AXQj787AwSOkZjihqnA
0ZuEvQsnWN+aN5ZJgO/zI+HLHFGLXVZBK4YXwqHRk9mh8mtXkERd+D/Ig8IyNAjqeNFZtCo2lzge
AowqsmCoC67eYhNG5p9fzPjDy5k+MEVGOvXR621zFn4wRLcANXbLLaqTgDI902JfKeuW3HE+NVjz
0kcqt1g2MHeO7vwLhiZFHoP5uU7phxW1PW5Y7GQhQXmnbxXYl2WKNQoAt9enH/W7IaH1Se4RY/MA
HR2SD6NxDpfgAqD/XrFGW0hzhzJlI6XWA2wiLw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z2C5b5Vf7eNgxsVgM+blog4oljuJGPE5amBDDw4IFWKEcJNxmK8iNsR1/nSU618rRzWshK/Fg8uY
H1Fs2nnnxOsbeSPfDz60zapynorXwzsi0dI/KtefB5PI8A9PzP9LZmPF5GoKgCyeO5RXGRNhstIX
p1ezoG0hvuiDRGjlMKc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
et3u9Nh2LCj8dZdn07LM2qUls9+keyt7JsISbFOsxR6cpH5B16zv97Rzwn74yMYiUBGAvUZ1T1v0
O4vr5rGCW0AQjy4M5nemZ9M6vuyPMPAob/tFs+R7Jb9fpt8qHPEH64ni3rOSEVPe07L1FARbFVCK
LUHHDuIaqTmTbQ20cYPgWi7rOJGYZaRI6TwujcBF5oJDmg+gry6t509xfzd/HPgX+tLX6NJuYBCP
ePAG3UjlqodSXw8U64081MNLzzmsSrNe2EnZfEXP2ODfphEFJ/9pYKdR8lyWMJQ6+Pu3vdvO+IIy
c0Cghu/ZzVtvJ7/zrgoR8hCFeuBzbeRvdhR5Fg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m2Nc/hOcqeBJFQqyL9SEkYAeLvPo2q4UIb79AxfyebsFVgipkPXe9Fr2Ly0oEBcpASNJVxE/qNX1
ncav7fcSQJ3AUai6lNvLIkrtdkVBATFfCbWr3T9gTPaXD1ZY1pnli57FrU8DixIaFRoeIg2lfWgX
Ejddks6fcCByoDETUKwOz1fhlUulegwij55Z9od8zC/RPnW2JzX7L7mQWAla4j7M4VzHtS/8AzAP
IcrhT+J0DDWfBDrYcYDo/5IL9X+cSnPrj3CzqrbyEBZ9J0tyVT8g9z9bEph9htiA9EuYQVcpbIB1
qmVC7LtsXr7t9qeijbb4dFcovnX3H5CRc3Xybg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UPKDuDrUpCqZq5As9ryjcITL7qO0/Aj65ai6MGkRJ5fsdrAIoRtKd/gZdMexAxpHxy5h8KvNWciR
45oibPZHqHo46BRzAtonK7cDtSPx2RaIzOvjoexdDjwbvwPqiCJhCul2J8EsDU1WPbSUWx7vpKn+
MYAq9BJrKBfkewHr8CqWmQugmrAbTxft49DV5mIiIEOhVCOTMV21e+pl1SODhXcx/d88X1XTvMY+
OkEL+ZPfyhoGAg9Tj5WjHVoAT0XcCjHObI3kOJqt3hPr2RYm1+yghuhT5ntdvMHa6iEBG/En+ah4
sN9yhdXkV5VsiSpxp/EsAX5tQkOiDZCtXXHNeQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rBmtHpx5e1XZPx6PBIEZ/58/PYTolg3kUSJ5yidwAgHM+vcWKSyMd/LXtLj20j7EpJVceIapdGYF
4nkL9OaJnw2p3gO+zvHk44FY2WlPcGjJ9qy4Z8049p1vFldJbTCwn8j2kMzXfA1XD0ll2p+WVUVI
EDJhvfyMnZOPwoecUCmOKjFhw7Oe93CtOZTTQI+gL+gADbsYMQ4cpMYr3spVh2jDfyhZRzb4Bm5h
ZlvJFfItmnW4/YJNUbQXoE22pLPLOaoAtOONuU5fFYk7jrQlcGNSRbnIf7aS7oW0kJmbes5lzfoD
QmLyp2jy+Pig+uTYrKUU4x0GRLNhdkoO25o5ew==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
TgmiIGNUr/yrN35s6FW940ceOPV2CVBKRXdgUm4aopH/EJBt+bSf9arEGUu42VCKqt8L0tIK3z7T
eoapIof30N6hcWPiExmvna61f4HNX1LNpfMROdaE1Q2r+dxMyaWTD4kgNH0+AGkusUdt1N+U0oiY
TglGf/n/aYw7f/OKMqyam+wInzn7FPcP2Jzq/fX0OIRD9UI3fRJHJ3wO0gcHHr7ATzTTSEKfiXhz
vxjEQ1+fC7IHQ50xjn8jbPwogPbRZ/9+W09iIqxoIOJLKTJB2kFOtLIldkpo3yVlX3AQVj3+OvtG
C/8zocpwfThEPB/gLluiB3dOIsWGqZrvPpK+sw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Fuccn+j6xUn1JnNddHRaJlmLbdVMEkYMEEkcOZGhVQKl4jjJjtSfb3uTUQAOd+zUzwb/8qMGUFK+
EftlfWGdQBU0uTQyUvmowB724MA91Ba3hEjZRna+R8F+vUSKEKZamtYMTDG9QRrt/jjpJPaaYbWq
R+XHj57SK6FqIcZkj3tIP3nC8NKskSBxCRgpff9/Uge4VKQirw+2bsSfjhfj7O752dVo1eg1FVG4
DPUmlrVBEgvF22qASh8kje/KZ20Gb4PkPIcTv3u5DIwFrrPvLH2GSakiLq3i6G0h9VNs9JIXLWhi
s8ZidVHLM5cpQ6ztA5XW//6PG4CeFUdh3eBiaQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 11360)
`pragma protect data_block
05SPcSoDoT8aaOIOkXEKdgWy1UdCRj8YTPgy3MdIwCW97bCgB22daQ9SO7Nh17EM2K/d7D8gU+2T
FkG0NxDZhLBdV7Wmr4/SpG+92tIhzMNel6OxRpvv/MPWRJ/KVw9pjqkpwFeBlk1NOxpfropKpShS
3yEbxsD2pbpgpEs8pzPpOgYQYE51r3PXroqdDKQbw3pdug9+mXpKKo7/BITl5QindT7A9tj0AHkG
FxJdXW4Rn6qHoBvDVLCy2BVBY2an11ahWfgbocvSkuTw+F4M7OWEzdLMTxJ528sA2I1s0Wr22hrf
AyJpwdqLYUTJEwy2m2v2BppkahwimFwBjLb4eU5u/OeqPPsNlhik8TRPCnotX1ZfZ+72KkyST/5M
vOSWcMpUokwEhHXQtzBVKVXBfWPQjiFVseFNnHH3wBeLKyTKQt8a7NJ3vd3eY153lo/SpJH/LI4c
l7e+yk/+BQZ9iTafbecPaUHsk5DCB1+V72Vfr6Efmsyv8/wEwqmEOr+5CEwe9tzoeau4IYdlv1In
HUMoZxZBdAusmsWJjBAC/tQUBUSMwiVNZG/P39rF/RpfqUfZiJ4mAd9ioXkfge03YlnntnQt57cc
+7SXmCH7Ukim5K2qLjDBiD2hjELWP46OINGDuHkXme25VSyt3QOepviX/sIwDdAnYGlG/X+0iKz9
KirPrZwB5K1qZiUqS2/BlUBU0Fz5fBteg3mADDB6thvUvUjo7ZlQSo0ciGTyVY263fneoA37NtuD
eUfBYjasmwhoPTB6adbRHztRPq6gzesYh880b5NZ0vOei3cD2wnBjIjgEtlZypLE33z+VtecAAE+
GkuCvNTDg6N4lZ6lD+56/a8qszaKxKyk133EXmcqGE2UjuFBfPRCaOploVz3B3fwvbAWYrixheVo
LvUFTQF1crFUUkri6XQHqv/Ukw6yIPCeMfiWw5pqMY9MmPTqSOJlEFFfZc0Bg0xJmXsnQe7+AT1U
0xQYWvEfFs6xocndcvLRO/nM6NliH4aIONSZOJKNhQga6/Ap2Bdqo0/sraCVWK1Jue7oUJ47r70b
vma+WOTNkGdU5m3n1OewS5i+b/mmLG3LoR2Y9JQ7LfNenaIalYFR+9nyoLYE26trRvavP55NWeuE
ZjzmCbrvUunmiPLRmkNHIzgeiqivI2qyPr6ec6EynYEx4A2gji+OZoPXjaVk3CMm6IewuK6aa0On
7uOP1lV3MAi1WUO0vcfi4qxI4o2HXSvSDza5z7K4OC1K5PDma/lB2t6svLA7QtaZtH3KlnzF7e23
2IYU2y++GWcW8KWOaZUEMuEBQc8DykgwghOigBID7xoQdWPkogBvn84Aox8MEGD3rU8GYoUvmFfN
LN0APwURJCco5LbwpTuIapMi5S0EixhejhgdhzDgwcykySHsC+w1igBZSBTrrIbwls/xwkF1Ba7p
sir+lRQ/4bCIeS23RtNQyqqloJeendsn9DlZXYCnOdTz5zclEfYYqKXFvBms70YvouCb5jZ2NyyW
4YrdzYKmINkd74epYMhvlMT3lDEhX440rD7IdxkVjoj8Gn/LTVfMapxNDlHNwEz+lHfZ3es1Wbk9
4HtKuyFVo/JlCy8j/iY9MMxeq6eE8/FjYQPzkrirIrS36QsAeNirjpFn6Neih86+p34xP2up1H9v
X5yHcAGUICpqZiw6dRQSuZaCFQjiYpHT2mXrRU8fw/qRZ6GDMoBsuGXDzGYBMlVjZkBoM2Bdyqxm
VOe0FNKCVZaxo9fg21mruNcVqqZkdirKL8sf1W6NRqgipyE5tOoLRgvd/Jr7dakOuUi+oHLLvp5U
tJNCuFlnUljojO+lKvH6XmpeesNRn1cU5oWzUZQ8dctSzTya5OSk8ddTMM0/m/0XKykK2N+mKp/a
MJh3X6OaE1pO95mg6qzwaPjb4Lr9mG53gR8UtIROgcpmIMgMJ45Ajajb3sXm8ovcRwlX8fpoLrKh
V9HwNpqVqU1dsjar//dfoBIq6ytZpP0nLC1guUih4awpZrA21J8LzJOgkyZiZHGINI+p7sH+aVdD
Ij7Yz1SYtcIgGy93WNY69WOvB+lDGo9oYDKiiCddEUGL78cgLkGUGGybNElpiE1HqbGI+06OIHJW
NvRUnP7nn3y0zbNanpc2/hXqj1mNHjs4LbMGbCwIaNKdFUDN7E/sSGhGqEgW4ckZrvCy0JA7YcQr
dcMPdqPBEEMxl5TpvTZMmy1qXFN9lg0Dra1DnMLQ74Qq7WJWXpreTATZAkxKyi1PlmbrkPrbXi4+
WCC8kWQDnKF235ZZNq671OBviOzAwb/KvE59QhIQSxGWkkjI53ssVSKhe1SzdoX6nXtsQJpjb4r7
FGg6b/yOHrTAHwuRH6BINl0c0TrD8ln3sJ6ndeaNU3oIaQDhRlqCGf1Bb75sdv7F47Rg5EvuMojA
/OBtH8LFFOoMHaGeM35O247CrmDTKGhF+w4fQLyrxqqyAje/QrLB2KnCZa0QWHdcNvVRPNi19KIS
0tAn2QYzOIiaFFgNuw+bJlnuKUwUne8Cr7S5Mm7ighXEmNWujbeh54Z/bsCCs2B5d+U0bOrsY5Jq
7QVG7cc/9pmtX6mHGWvIrRGhaJkKAPXrx5Rbe0fNJ7Xp2JcQXB/uq8+MmnQWZoiW1B8pX6au7suu
XGXmDZDknPdqGFHBBjXm+6xqAiEj+OnGJoEGEFnw9nBbszVVxSe6V85BtxzLMkMgQFtxOkJ9wnGB
fmw/fL8oQmKTHJMAJbb5X09kT6mJ3SwL1LsrC06lv+ZrgjtmHSwoKI8V0sxdknWqA/v09GMNSeAE
j9lt1IkmwhSVRcPk6/wVLwMHtbonTx2CLHA4P1bErsWFatEX1/rpjYSO7uYBDZxVPAI3fe+5Aa1k
tssJD+9/0enB+7PLlTuEWcUb/Ccml0BbRmyoSGxEdMpCJqRm9OTvzx4xzo35U2uVbLghKzpam/B7
zYQJ14ux9UKLPYOGDyo6GdQnAYnsFqB4Mm9j4LyUXfMvrOOAjsjTwYz29X24vpW6eGzvcWgEYNuJ
2NiAhCdBjDgmwQwCRA+W0jQ/sF6cqBFAlvxUkvNPc2GzbeEhTE9I3BbTqjzWj0ofUWJ0urf4MVzB
x6DlK3s5o6ucb2xflXPdnsNl1/+GDSwKksqU50DHO0CM/JACENl4PHvpYL8nLfwSxuo3jz/UyeUp
X0lIB4osQWMqTAK4HfGFGMJIJCc+m9CPQ4PEwYUz7x3rTkfLJ28aNY7ewf7Ggd/P0YKpcgGNML4m
XRSkD9dWhAS/I3RJcFie+HIpC7bG8oM5BBwlssKw/rafmLITnHUEex9AD8gvMZEPTGCDRoYmeEno
4YQg54631dDNW2a6fpBmRyDJNMZeVAeTYCAY80U2N8kochlqKzYdlNxgUJvl+uUlM+zkshf+SKdL
28CoI2vMGvezy40O69mrUqdChOjZsEGEkIF+Qeowg/cpHzNFcIJtwi0E8TZbUCjZR0PMizheBR7P
Nc/1mWaZybLSNuGl2jcHuO+p3AoPgmEgR95f7y08frY7V0nbQa3jpM0IkEm4Bg1auXbgRKq1rdxE
OYyuSNd2FkH1KPVIQcebb6cwK5C5vFJgUL0ub47nu1edhsViCyWjKlnLpmxLGdFkMr+qS0hElPX1
MRCXtudk5aoOZUYMfWrspPUrhelktGGvsFA5IGdje3ogbPVhWZYx+gZzFz2uB3ZlldCHy2uwvzjh
V7cNGydZfHPzSdwTntB12gjhp5nXuvXYvVT34WHwrxOG+1OV8zT8tAs4Z2nOktC3DNEFdrBBvYTV
WJfnes8k5o3h1987y/Vrxo05RAhrkOcfZC6G9Ruy2MWe7ICGrRlpleJx0uAtGskXkrvNnn6GDu/e
iBoRM0vGwoSeUad52FW64wuWeglog61SdtcG9IQq3P6i/AGIDNa/UwnEXEVTPkYUuROiYwGx5+Ii
KDzkNrIoIeHCRPEJJF+9tlPeYnsdb+C21MyPyjNT5E8GZPkb656A/lLiSCUskAqe86z/z3td20o7
XYhHaGo8m2LooUA5N1moMkDU90A5bMW/PP+6uy/3SMt2ncSphxiUvxwrzhwr4FQwba8PRR0UELVH
qoLsnUwBiXCubdlImThN0EY6AAoCUQglUuI6t1qqMb0t/cuVN68VghLlLIkcEkPaPGsCq0UAhVAi
ztEDFTLgH/x4LOmSI9BCCCNI5zz/AAyogKKv8CCu3uOc3VsDc7i5VGH812b0wNnG6JIyBoeYW1EQ
EJRsdo5s9zbeGTMtdSgmqMs8qe0RMhiXJMLXT4tWuiNzpAC98vsM6mYVJc9+5NzAnj5Y6QEK/Ymv
JM7tG7d2N1/6YKex7otST+jdUo4hIRBpDeKXlWx2QEpkmUHrvSAD+1CaCcg8AwvCzTvVGarw+zYz
bgCQ2hlKuXvV2DpidjuZBqn00uvKVfr06e5MAma/hxekxEcB4I7vLFY7ooI3sbJeVKsu7QuKMtpA
ofLXHMXjzeY+9opYaH+sYNlEOt8WXYTfLq7l6Goa+IqV2yk8tC7M1F7e4c8bvG+BulyYEeizcQcJ
/UTF1Fm4IogvkrPr79WFhfTh1Halt6b9omGVDXqfiJMjbiGGirPstgqy6gHVBCiB7GQgkJ8NZ0MD
IL5wsOGQYljVISZWoI6C0iEwIF0j9tu8VmgSTwQ3BjTEjzBx3db2m3sebRn9PxKOV1s1bKBKJjNI
Z+AZTM0v3qm6ry9Zt41NdHMCCZFIprSRuRxSDcqOo5f/cY6DE4iRD4qpEFEMMSXiEZozLb45mVKk
25Z8oCumSS6DfFj0zmHua8LWRL30hy7vpIe+ijUd3dvE9FlmnfZ/j5C71+GtbAUcO6yFPHeXE7W1
FuWY1MB/g3DcIRhv3i/g3OMiULEkSegTe+v443SEazJTpbzT8DENZL8bfoNIKhTMW3h9E2iuLi5e
vjxLw55wzj6Ig0KOwXj1/Y/hb65JdHar3PscMZsSrS3dEItbXjy6qFKOngKdRjdWjGePm9KJXyed
LZPrvEjcAamOCZjPTxbzsPkH0t1WeluCkpRwUgim/rl5a6CDyTul7OmBLU0fuh6XRAIuxHkgOGlA
gtk1ouCOjggd2sqSxNtqWb6WrSiQ1ypdQROflsu0/82TyQW2L2jlBwQJ4B7rKJmQwuJgk7/8Mlz9
97fTyD6MWOhvZpjoBEBIucQ11QHiNf4W0wBWhKM14PmpPXxsyy3btIxpPSSLcluaWDNnCQ/ghF9A
7ssItcB2dIk7Us9OjRRBMpSc6ByXo87975MN5OctZsMQhqPQlMaXAgqPM/j/7cr7TnRRG+UZoU6g
wFyJiuPWpFLIfXTWNLApuJ0MGH95e3O1LWa0nc9oj4bq9Xfbnc1GMfOBbuOSuQR+gln14eMK2tvk
GRY4/3C2ojAg19Cul9hPGGxo4+am9BHWfkFFLEGHNWT++qdHmeHEeZJgKDmOCxkwwr3eMHUyV4Jg
aGQAFKMBZfO9xr9RZ4nlismzpqsywL30ONjUYWA3xcmehBIy6Rv+DC4PLNCI/apqvg57E7Ja9JJn
++TRyQZ2HMFbHUsmq0+yUW/YQh8153SjVBo4HyATC+ku50+3JRuIX9YmmMhD++AyRCGtMzJUHmzl
T4ud/zAQrMytQtBjVF5fOdyE33TUfZrWvw1Kr3wTAWdsCQk9VEGLA5lv/nshcTMTnjl9Fv57QOiI
3D/ReXPWVkfmRS56l9wXo4Nr7MTJLnYEDKxYMu02kHWL4wRPZ/q1q5bwjWkw6ogXoNRUuoXGXfJb
907EpwAOqPMB5+phgJQR/4j0ZNf3gPAbI2xqpmQp22e8lrcBqMYQhFRpi1HQOq9HBtcaC6lzPyAj
ZWS/NmcwQZbiLoMNC1c5KK2Jl+29KgB6qxa+rSpclBuUDzfNz7GtcWi5nXbIX4TLwjAHSci8Q7Pu
aFr4nGFp8542rsk8hIIqWvxSN8AjzevlwCnak1ZpUfJxQBe2K5pdoj4SzydgyO5b2yN79mNWNpdU
xAesC90rD6Z809crsg3zeiPKpR0XzaemiKNQnr4a/6DptZ64izQ5fpYA8xL2iYjqR2JkjEprgBs0
PFhFkFjqpW5SrTnKqnBvVcQp2pqK+WyX5QMhD+zKT4Vpvl/K490KrTiPMLF80KXGPwvJQ8P0xO3v
YJn6wCZqO6WKYyG1LLIRvrt2ymYBX8y0N/CnhjujLAkRVQsTsUGVuQBdwxNp5gKKQVBP95zYUEAA
m5IQYXYcT2BD3r7TQbbTmbT6mkBPEN+FlxvU5G30bpSvaRb1G/QGIwzjlpZOYbpdmGrKVpgn4BT+
k2emEWSdKSgI6AbaQM7TP/glS1X1IC58Eh5/7AECOp80HbNLiEiOm6Ozum06ldk73eWV+D0GWbij
OQe6R2FMWUMe22bcmEA1uaNZrzn5PQEADrdn8J5rJQQzRW9S3Yz8TocopXk91Nup6u2Ilnm21XfB
LMxGEcQkWK1xyodY/eW9ZfHg+B0UbkHWGNMw8hbCtNLkGC+pZTRZlGmYST2h91WKu/ZjELXZtbWH
zn8zG9yrWfHga35DA9emvJjJpCaXpnOAEKV99F61ELCM8pbVFbkdXnZCfugvAvaVIgvRJNW0eW6L
hwyvamN6VgUT4emGGH7SNEmCaXjxOsg39Kc3sjE5E1xPetwukVw26XxQBrzN1SJsmvKfviLlB4Un
cnVQ7pMyA+VVhbDjC19qvT9VgSKr/m8PEUQ9yIiuOTmHlCQdkUrI2oapptb5wv6JNfSMR3OK4e8m
ef1IRXBJWvcT1r/yoxsnBMyB+KwW0Zf8Y8m4MzZq098G/cMgLO4Dcj4GjSgtj1V6COXuZtdNWbyM
UedWaYhcqLqmV71qa9M/IjoerivE4D7iD6IOcNuWHAKJQqIH/dLVkxmrsLT3t3yOeYKv2msq1mrI
QqFllUr5Ofcz67PG3yBZQ+7jHhTAVzY4B14ZMz3Zgvz1q15VTC8++3fdAnpskCgkA2zmakpJrrm6
9O83dKxavAVtZL0f85XOcQtjg7X/CnxmtMVQZ38g3VZK9K8mrNTlMgcZDhpzTbCLAzlZLwUteo0w
Yhbv+XKVPANz4DmkeQYElrj6CcYRl7I8wx4hN6wjg6qLAEuySsaImbgwunUJ57xKQdH9BRbN5+Jo
pNEc1twIKaRb/s/VZA9B7wTP3i/s3FsjVKm3DABvVjKE8ZMps9ij4JMuy2vZ+UYUDwGw7QwOKB7g
rqfrXUrcyraJkd1sf9CnRZoszTjQBop7TUc8Jwd88G6BjEk42nm11VbPF7K5dK0v/NZC8FUMKAtA
D5jcpQzRp4pzlcNpcyLw7+VVQL8Hb+Mok22/nnr2HW2JIIVWxqFo4Gsef1c38KWPI5rwhZAP6wac
0Z53AC4VJpLVO9yYjCyIKBaN45u8CvWsdafKpifvXVticvn1QvsBl9kiBpRqIWTSHs48jS/ytfgg
DJmLpJCgAwH0KgS3LVlztl0ynqVBkINtQgyYkG1Wga2LYeF58WVRw6q228wdrofetC4fRdIWWxIi
1SodtyAMcsfzu3FouB2Op9Q6OzBh2/HEdpUD2/CtJXiPSNwm9YkwXo8ZMRREIM8QJ84TfxtXdXsE
9qq3QUJFszmmdvDSAdBe/IBu1oydntOQt6nkkQgpGuzu9MLcWusrmAZDEz01EN2mrBdNWlWQmwCL
lSIQTc7w3vx6PvleBgO3ervENmJrdMqg4DaVid6k7yge8ugbxnTOn27cJAgvh+gUQWUQmFNSUdjw
asKVQiaXeSJoI/TjcQEx4A76jDWnDsNGg5I94i9bUsoEenPvrzDtwKX0ddWOtmrPQExu34fknYwQ
q4F6E0WEX34qem7TYjuo0n6mD1C9hlTZ4UOTaSBXRkz26GuwigTj5SRNGbUa3/WmqbYE2Taw9f8q
KIlTK7rvvu/Mk3sofXk5SmY/8qZ81pCbyibF+47PSJz+NoaqCRe7zbaAZ7XdAftEhxz2P969OqPM
kEJ1oeouGnfK04qu+y2YLW+IGec3n8haqsCtzKyTyUltZLnTuJKrMNqD5qICIbBSbUjBb86EuhGZ
OAYpC64lgUz67wjS9yGHfdJDWjs1DNd/a00nZXivMYapYBqJmOWeIVTM/RqY3A9alufXpRx0wB87
I+aYb+kVs5kAEkWaaXpC7r7CwmjS1IVSrzxCSit9Ibxs/RrD6o+kyei1rF9hS3NYYtXrfxJ1iOxW
efmVvbixmgIF2QAkZX5Enfz+uJYv3yIy1cn/elh+thxcMv1ox/857MZ2Y8+R3YxQIPYuBD4uGSAb
nwdSu39HvQ41Uu+3GZJJ5QMIKPgFr112vF17XIKinWzVAbcMfynZGtBCS9ciMyUncC4CJgWx0Pqw
BVqn02MZw//Ttmh8TWKkRRQtSjE3EgJwgAIZAQ/E5DXoOzvhcN2gljlQi0vgjnODEzLLxsIkAjoh
pL8vsmKM7sXeqhhadrhsUA9ypHtbtpwMexy+YCRyKw4BHaTFhGJmnNTj7tpaM9KJcxgvv1tFuxZI
KJu8jV94xy/edNvUTSstoX4bzS8xM5pOaaLl8ZkCfHG4yYtDw1W1m25rvHcdgGzLXtOebmgIDnfV
3G6l1pqTXyDnPRzaRt2sD+wI6t62++mJNQ2Nq+n1ftxTEnxWQb52ECSeTRZQoGBpEKEFUN0Nuk5t
Ju56Q4zOOwxFZnd6gT5EfqXIqHkSceW/SMZ0hh1F5B6I/n/fWmvmL8QInnDs1rxoED9vA0DX25/O
X/Whu6e/KY/6/ulerxmAYIFuHioHWEyxi1UG0pRzIKnRFPkzjEWaNVA13J1/plpuC0xfO3/m1D9O
wBZBXtHnHzLIoGO6QS4hQQP6zxrAcPnXgF16MMVPBxgvykM3wn+TDq0YzGWR8AT6VOsTgKCEP0OI
tiqB8zzL0K5CzMsGmIgWAbQc2cJ5HrhndqAJXvFi3rPcZC8Jq6FcGH9Ry2zHvIWUVjQTKLRr0dGN
P5cptzPulS2PIAjfcUG4Jt0Pr0ip59GHfU8FfMICEwe0rAiT6zd4r/AjbN8W/47kjLAtg2QoD3c7
TLrvcV4CYH7xs9AEg9KUxRAk8WvQpI3AnKNCOXl3SXsJRKwKulmKmp6rrnTH+UVK6869PpCyTIom
7wns19guCzsstjbz+4FHFjO/78eB94kxiaWsDX/CBNRUqUNTEmZvHUBPwdgieBhDt0vN1qg59ZPR
r9iKTmx8aGXrvy7YOj8qGOLcVo233Lq0MqG7QVOs982hyE9uqCGaY1rTLkUxNxDA/eEaIXFeu0Ws
GzdozwRSGGGP8DlAPuQ6G5Ahop7UUDwZyUArhfsAiGpt/XKhfey10tH+og/+Pq5TM68QzfC0TOB6
G0JM++yjNum6GLNZpSok9nQNHS+2wODLI3vyBP1oH8TiIfzkqsLguO+xKRcZw+0u/Rx6DLGklUQa
bIDoqDiXvr4ilbNfbht8YAggkHi2/NJl1ZLPMtUFccK4d3sax9sZz8SYY6YRQ8guYhBRwpm+hrHr
C4u0DB3Q5yhZFjA42UgLvb8bBbjudXRdMulVII8IfXKPEYH6f55TuCD0Y1i+LnIgsM67B9TNh7C5
8nlay7aQ2uEbDcSzmDK5JrC7mPLO3/hn1vfIDlx/46hmLna8qj7WlLzbdFAkZuVkBwToQ1fk4UpF
pikd2b5mJDsq8rb/AsHmWQUy5tyMloT8B8H/EThJc+BwLMNMb1hi3JwjDIEx3UYds96VhbjbrUWc
gzNspYD9beY9zaJW6ueIPhqw62jN8cYaF370+HmtTvY73jkziuGI8dCyTocmtJSwCGZx+3CIpWPS
dPByjC3GnHup2GC2b+aRVj/BcP4V5Zr1NOPu4xWwdvC1NWpQZqY/ZuILB3G85B8EqEZ2yfIzf8Tq
8T8+LwriKlJ+9qaiT2Lxd/7Y7l894oTVPWTp4dl/YcN3c7Yji/wX5wKRvxtGIsi/Sh+/yd9NNqjw
nUEN8NhRNsHglv5FJBhFb1Q78Lu6m3Su7F6BlgzsFTaFTe78q58Dtvip+rX077SmgISYG+/tvgKx
wrcKBxjhv9w1d20wZdQ1Ukt7MoYPZ7J8OAo6LA+K28FhzjBhC241ZpzINnOmn24LFyWbcNbxAusk
5exRmfeP1FMFA2U7MaiYLCbvpOz1JEGMFpOPMSYWsMRtolzDDrE5vCBluL9/laNvusiNz5wuf2XG
gtS/BJSLbtn1GhpxkkUKe85qvSt4hAJTwf9z/aonwMUYvWgOQ6hxhNTuibK8C/i6WZN+uXGIaS6b
+tzyDOgXvzi9+m0N3+czL1UIGwE1HgsCT2ycQQMQEHK/pbUBD6lIg/60M3D++7qY9AU2TL79nRg/
slcDSxW56KK4RJt2y97ZMmRbpIMhfSd4aHmT+zs6opJlGq4ftqRWzI8yijMCYHxc5tE/pLIG/H0Y
0sRmuFq7WmbgODTOH/LIhTHcwYJz/bTjCOZXENKWGqkZ+vjRXG8J17eGG8GUdZuXZT02PoB63cTQ
IgyrDWg9lQ+/b6Yt7lPWrsCOxdKD6jrpGqYzy3dZDQas155XROM314z9xrjTUpRL4rKYPvJvjPJh
fpox/Wiq+X1etyOMCCziSKgS5TfZjnKkYEuM2tGDsX3PRvvSzR19HwEcEfX75w3QMXiLs0/OdHNK
n9pNmhWxF5lO6VUwJj/Xk47cjjJfl5+D+p6QH0FRMCRtQMZJGlru/o5P6kURP+YOXlExFNzakdrr
CMNU8sgWkg5StopCqsUyYBRp2Ka8csuKwTaO75ih6LCA/pXutUdzTW7BAAEJhkncHUsP3/9ylLx6
d5ocQ2cU+pH9gk42QL74htvziq6HGQxEWd94oW7BCaFsJwzmOv96HGu/Px+a+shpluUv44nWGhO7
wm1h95Cc4gMw8lAkgAM3amhmclfW26tLxm5HdZoQWqIin5mhuCG/nktj19+Cc5wwnoPceztfsKO4
mdqb+rMG6goe3XFq/cZUCTi4HSKMzaz4agF5Zd05GjqT1uJhL12maOns0kuhu0RHeoq46kSZHfA0
IfvFh2fZlW1/ENTKUGceWt1Iwtdr8KLfc69sUC1OwglMBM3PZJ0OcfveX+Q1CDnbfGVUX4+ZG5cL
bvrNR13UQ3Cq1O9a9GQjp34P1+V/taDpQ8iCSmFB6qnI5pxnAEUYb0qWdxlkBgGada8Q0fC1sqos
gtWYu30+LXTTc/q7wwnVpOH28WFUc9XjhU26L/7wDhSW53Uczx0GJBIXDRJCC4A2BHwAHVgk6beI
t41FvQnWzoB3aIc22iUoNgnMjw5iYgWsSZVSpNL6rroD0PL2yEjB8ze2ihpEw3CTv0bPMdXndaUy
2dEeZ0YGgCH2Y4Y8nB+Z+sdo8MS5cTg51K0LtDPFUusENKqDVJC89AfuZ/MEIixcSJzjKw09N1/I
LxPbXoErnx3T5EsbaaLd2MqD9ynlmcD5fUwfE+cuO7UrjIcXL5cRsNZwuSbcncqJVvZPy9pbrnLr
HVki3ggXqzn2jiZ2IEM/mhKLdwE7hCDiZbvmX2MD2JDyXJuoI3eaA99ZA7iK0mYlpMAErtHrbfa1
YvHG2uUKy/CpiuL4bhN0Pw7UI1F6vI7yEWhqQbBQv2g40v8RUvkaVP6DTN/NCLlp5fB3Upht9cXU
pEzex4Tt752g90MTh0ThQlK7lzSACS1hOmlbxbRBkMvECG37ZJOGSQzN0Rdbn1vheNP9d/aqUS3v
uvBw8LnGkYzkWPy7eED4qcgxdePxR4zAsiOv4EsP1qzKbTAdjsqreAGtyQ51jJu0rWCybuj27ceD
bYbQNHFcbYc+3/wlC6hCb7BQdiysrIdAqnIBYWcgW0I/iyeTYJxFU4/Q1mV3ZPEfsI4imiXkNOpi
b+Epe46kMc9M8BSET19pIuuDioUeYomB6ak88lXcFLEgSGzzuJ9oc0WLq+sdPfTI7EWuad1CSqRn
GoPQRO6RhguH0b+XOvsaVIM98R5yclo2cr42szdrThfcBbPNvcLJyKqcoljqE9RA9O+MkdyoVQB+
qE5kjuDDnBmrccIIAjD0TnV9gjFBnAMwKpHShn09COQhzHtotOE5mFrKA+TUnnK/ZKruZmFDSI2o
Jutitg1+we7vCIe2rQf7N8xDvPTSaygyZB9LxpvwKYNMwh8wD6dMutyxVqRIVc0OOX+ihYzSwtUt
D9Hj5ahV0+8UIiskIRlE2Y867s8GaMNjqiCQQ3h1OiTS8mML+1Ox79c68Nn3oW5Hj6V7L3ubqsjb
qAn3kyTjG0/XYn9d8yzocTGFeLM1HhauBpG8zFwecMpQ6P8Af2ADf9+OEzSOIS84Qen13x4dM8dH
Hm27eWAjYULm/9qU0yCps6OLFec8gUPG06fPfYz1M3YDuecLpmPcmk7VJ25xcMEXO/jwR+EZe9ek
s2Y25J8K+Gby/bGkTCqDkl1JDBG4m3Nm6Fj1GprjyJuwzm2oVMko6lxnpcYvobAWVz/u8OlcYFS2
IJPbhPnem4wMQq0IUMY+pWdpPKEY4owdNy1Z2gAs3D0s5IauSXfLc7PwCJJRZYB75UMReygL2Brm
ozKvl2bjK0Ac4LId5yVAllg9BzKAn3+FC+zVSQUitGqlA9Dup3GtLMFVFiqH7GeyCS3hOocpzrb8
3Hg4qVmTIE2n3dbYktv+/d787JrwVYVLWjGv9CnuET99LwfxdXgb2LxNBAXBcqLMeDzD6/d+fd5S
/+GAD/0tHeq5fEfzCcGcyzC97Jm5nZ/j0pR8fLZtuex8CKauPEld0EnnptCOUEJ5aXiZ9A3S36Xi
TshL6bs6RPnlebi48Bpe4gynsuxYqYz+iljS6YDle+ovmvTqwQh5zYBGvI4kxcYzIVU36ZdFleHT
fig/SWhI2sY8kC4g99/dLYRP05t1dvrmbb10QcRlZNrwuI9xy9uLw21Invm4oEdwIbUrjYOwwvct
PP4g/kVYzYBJb4gVHEHymIfl93ZPsjhcGz60p2nq4gc3MwTS04vDXWDUOIsFmt7RPDOYyzjmIXH/
996iqRXA16ceXuoQ4Qhqk/OHjqAq8PZn45oCU3YmCAGosl1Z4SL7zpD7bgOkz7Gj3ZPZMmCv5THw
z7E1J+q2scOShWy/Da0AgnYWLVVKhEJdf95fmdFz+eMf2tHka42pqVpf0K3PUB7B1NgyYWLiaQUz
GFtgZpu1PdeEYiXaIINdRdOAE7E1Cqyk8oSPhGHrt0KuTeSqTYK+37/QiDm26mWzEdxFgK6jlB1L
p0xODbUXliynyXbYepS84BDzNpkJz6cSyNpRdi5IVrc6tb31TWahdex8rskfnwG2lYVaDga8hFtY
+QdHen9G/6eu8/Nt/FS39hM9A1MDaoGJk/K54oNP5w+JjGM5R9bnH2BO1nyrsKtWsM9VEjGxx23H
ha5xlNJWLyTbKBJAtr4GQogc7kh1I9o3LnEJcR6mAdaNPaF4G3O1y2t8SvGEZrGKqQeNJjj+OFeQ
bwX2NdFeUPaV5WV2zVhBIWYGiJtCHbr0IRO8C23vhvX/epLAQrwt1PT3M9QIYzQupuBkbIeaNVzZ
HW8+WDUaVljUnaob0FIPKB8CpdbT/1wcdxksJQzyzWt0C3gYzUw5d4+AksIZocYrPUyZAjNPIFxx
OSiLph8Ohm1/MNehWMfUClhguuYzi3sanU1Eah5EHrFyDEh83/jfzPBPYJaDVO2p2Pd8R7++4fsy
PLD5FDBHmMjBsMDxh15tvaHM1benM0BPwpS8Rz4kXLFZYFgsQeCIVxoTDENo7aOllMDhFvSysw7t
YHW4C0rScrJkTJAfE8Dw60++mIfoXzqEtgPpnLr/XcbmmTXg+eyGe9rXylZr7L9DXt/JE70kl6oF
rD/vAL7fy1cXoBYrIaZp5xlXBsoz6aeA9MJ28wP97CA13nWaITj75xDP6ApDjv0VvPvflTP2GHTF
CenXnWdK3usaE5dHXTIMnAT+nA9Yzz5SyQn6+0Q7pBNWbsh88f9fmoznh47fdOrgJCbIpFTVApFh
0vhFYiTn29BHjB52K4Wm1qqmOmzsaar9hbC5wcN2ykrJ+RxyTJac2CBTjwulsIh1+hAiIJBEoSQu
FoB1w34h5eNcDO4dmSB11NmIni87u1IPWGRQ9oHzGWwLSMpi2Aaww1XQ+S7Gq8NM+pxJ+cGXFILF
A6/0FwRSn4QA95N8b5dTv6KMEbSpre/APrMJvzf2WS71zwtdeeTiXvMfW4GiHeJaqd1gjfwgZwNo
3fCGXfX1C3u3jCmGzHjhFQ3q0mVck3ygfRrvVx5jHFSk81pzOiDKHJvvMdyFiZtW01Z6yeobuK1c
Gi0hpMbr+XzPTcWkaveMRQ2AaacgY5mbYLSsmasW424ZHv//Dh6Bx6gvz+u3KyFMrMY5oUrCUcNT
3CXVe1KqqB2JiQANy4ytXiyO2lCwIXma9p06NE91jv7NI3EV+n6/21fgWwrTbFvplgVX+tFc4C/f
GZqC+7isWxUJ8NB4jOxvpK8lJJKOQZBHHKzuSh+vG5NHaKxId6qHBX0B9I9Wf0aLOHsOzjd85Ki6
vXzpw6/AXEX4fl59zxVZcwDrEbVEQdK+//TkyBVVECht3AiVvM/qa1XI2u3OrAS7GJvblrsTcmY6
p4sq3hPJAyDDueEKPR4gXq1AlBoiW5LpuiL0Sk5w0Oi8N05vS7Dit/7X+yiBCELA6xsD+vszCIrb
RuKg3bNd/YpC5chF3dv8WsmgmCW1dXJAH+AfY2/Whr9TMb8PBIJ/gDnimV207daFIVUh/8ji0ga2
EkXg8qnvsy1k9y+bU+alumYooHoO5Povo4kTK41N3yetrglG+nuhvt51bGZhjyZ+Z2X759TY940a
zJTYkxwOTJ+9NuJ4SgSv5Qy3fTPENCkblegrrdJHc70MDVzzN1IS443W89lpaoeMmLVtaYOVw2ZM
7AC6yfCX3h2oOisZVR0Qy5/Gcu5qJpSe0LM73zEh+V5C5nM1GXyr56JHLBaDqDKN62e3IDtgL8ER
Zan0W/nv0mwoUh7ozxrndFHCeAu7XtERBF6zx5lXr4Cx7DFj4bmrO+byivXP26toj6hzTb37dNhE
CaAMYe4yhGBGH6EQ2A/B2nQPg4vPBuEAlB9++fMd6vJ9XqSQZbirVh7jkLFt0yvrdl/qVdkrEl+w
bF9wcLUF1fDkF3OqApXzauM=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
