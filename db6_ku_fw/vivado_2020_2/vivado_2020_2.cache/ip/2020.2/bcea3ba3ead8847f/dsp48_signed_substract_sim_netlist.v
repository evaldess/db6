// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Tue Apr 13 22:46:02 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ dsp48_signed_substract_sim_netlist.v
// Design      : dsp48_signed_substract
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dsp48_signed_substract,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    B,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "15" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000" *) 
  (* C_B_WIDTH = "15" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "0" *) 
  (* C_OUT_WIDTH = "15" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* KEEP_HIERARCHY = "soft" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(1'b0),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2624)
`pragma protect data_block
qi/5V/7RMnwPs36oeqdy2phcJwmFruHRUeydzxmD9AzWpj5mshq6apy07vUWDkXSsTJwVYZB0sYa
MhT3n2N8SsLv8NtDKuCqCWaNvrhzrApc3ilf3TyYFQPiA0IkZLdiOOLLZjmTeY7JU/MTnq+10HUV
CP83dbLMYECSRoudlSYeKV1L915nt+HXDEG9emn9A3nMG+ry7vLz1GLJEMo4I4OQOG5clhn8l9Gs
ZKuiSiHREbJcZr872D1b9wDo1kUZxkC9K7upvkFgWK7X5nuRhmC9svDIw81NstNQciNh3EuulSRX
z/EGijB2n9kGMrX2/wAWO6ZFFx0AVhW99JRfXs1mr0PKmkea8yrjAfux8MCWYfnDhABPW+llyHDg
3QQaT66IoigRP5TzuyA1NLPADoVKK+VhRS3NlTZrOws07UCjQ3ybIcMkoSJ7A4swVlcN0yy6KPaL
VNo+PJDSjb3mm+HHWxdGkEcfntFq8fAmci3C54uBH/RaT+d9+rG4VRcdxXeRBB8xJ2SzBfBr6DHO
G+KYfCNz6y4gW4X0iJ/fbnsScz0ZmvHqTYm5cRx0X1eF/8oW8gdqmKrViJEygXjkP9dW5NFB2t/r
MGePhrwVSsQri8jZF82jLOdKvmCngVUdlkhSD6FT0fWaUFmfgqCT4W14xvjO3FPpMyXl/KNQdRuf
1N2C5R+56GMeawKafARu1qN3Bhk7XivddAylXkwljGrg4T+nsod/mOBgcqXwKpKngQ+IlO8+VXNC
j6ZFWjYrqdqF2ItMDFlHAtAbwBx/+aL/I7HDVezNRETnuQBiZ4HzFUf4fBf1FrR1Xa2xCSiIuMNZ
V+GHScwDn8HRAj7nlAb596ersD6ILK/ApCr5Jjceit3Ag7WNwei4tgYymNT81YMLxeFcL/ppTkry
qLCtg5LY9OuhQviHFpH0BBGLA/M4JsbA0uOgMCND/xpa3+rvCpAObeQEJYwb1flI74gxYVBGpaRl
C4YWsiMlOJyMCpn0w2DV7i1ahtBsQgG5UGXgKl1qguoWQOSJxpquCaPf6ctJqunlWTHE1CnyU5JW
/pP1sIihEJvNoZ28g8gOHmci+fkdfoF/adjT+YjuUNyxY2vcvsLPmm2rvV9+NKbuljb6QAetgKqK
I0ZXQje4fN4unTRN1bazCcRwz2B1A5YJcHa5AtpXKn3RM+UkV6GkSNdYH3aPtfmTXCpbM20/iWx4
zwzeggKgaVxWVeeQDzNiObX8grnpd5jz/TwE4zRfFd3LdJpmbUhtkOd0NEzBRCltmZS0lJM2ShYT
J4K4Qz3cns5D0gGS7W9G2+nYGvWRML42o7NpqVCefHnxeyLbsClxeL4gaEdbGo+sgXu89IgP2tLC
bNtfxnb9yg41OAv5bs+/82MkmbbLabWvEctFGGE8+0HDNGQGma23vMtOqxuimbzty+VguvxDH8mn
Ym7EHoqZ/mhpFpYsY4DXqfAxxIQVqxlAKFLNonjwUmPUosxQixzLeS0xpKEai6rJZlSYYws1sfTh
TguuvLwqZCmwhU85NOr4VE+rJQkKgtm8ZELs8PZFiKIH0ipOaBP7PWg1bzNrq+Ou+55QQ6QCP5Bl
RgLYUbg/GW492d+nE5ckFimkeLt3YvdJu7MbgD0VqXWoYyvIL+uWwEIwR2VykdMtkjO3SNAV+QB4
wmccXRtvpiEa6CAhed8iE5Ux2LMVCi98AuoaqMo6ZpLhSoH+9l66EAAN7BJLQB3k7bJZZONcdG0c
bIbgqJEWJZupx56LAS1UDmCXCKbzGgWUHkrdodlrix0V9bOC0CzqDCSWmLSofzzBeDxs5hWCKGWC
Ra98+IGs2Dpc8g774p+PKC0MwDoP81frVGl5pyBqhSanRP05gcvlhWL6sIKg1HSOtFxXyVIkye3I
eJUxn8fr67idFVPSXSwuP5tIefg/5EwsoiSeeGxirUX9ae09v6SOr6W50SIVBlUlZ8dcJfVrA1+0
2e5n/7SA60jVvTuHRTnUxrAEyhauOXOHTEYWVJWxxtCToz5xuRscavn66l/+tEmE1dLFeVo7uspL
uevqzJC55pNFUo7haAM6zxrsKtIzjLMjN0jlVJ4xxptEIG5mYWFy/9aD/il3BAn0fEeyi85zv99d
9lp0JYM1Rk4iIQ/tFwKrVkcH4S0uPpNCOt+KGGyTkIVu0FydlsM+OBvArJSueHLoTKZ7h+PLhkho
nRPn7ySZKX40xrDWvEj22sBbMkC8i+B8ijDoL2r27q3jzJJokLSCeq4dRogtvZWD9+/uUHXR5Exx
ZXtl9Wee+nrhf1Nv8TcjgKP3SRhYJgOYOWqEIVYBdTOGxzNB8xULMPBJcIu5SQbYJkE2AeCU3pBH
aEqHx+pLy8J/EHuhAXCO9TDjOsnC/kV5wFBz2Sz9qLGBHrk+uLe0s/NZ9ZVcSyVBkhGFmzpvT1Bn
VPWWKJDGlOt1BPMCOCfjY7ZaMWDCn8rkkjVXBGwWGAJpsT0HCfhJIWpFe098YPTXXGK8ZQmHK2F0
Rc6Ii7vVhzVIpMNIi66PuB7HZPBAjzDqHx/gK8KjSSbP27CZsf/q1V6YFW63CcKd8kedtYjUvY2v
ujeC1T5sekU5+B6mvh2/Qdg5ugT5EfX7ty4rQMEyJRDunquWVj8XyVZrxtrCQqhJSq0d3SA5Zk/J
nuZOdD1s/UOTm3VnQqXjel6GHxPfwAtxdZvbjJ4HaRS3ObHbwX/IrrVYtcXVfo8XX9/FyY3NmKpf
CfVg/uxUTEM42mvmduF4Z4gJUJoAjFYyZr1FB31/+tB9k+gVnrc735iJDDOzq2B1AyEImQfT9l2N
8EzXNvsTMoTY0nk7GlW4/rWpVvOXWiGpxUcft/9buiQyCgo/hcWui3TNnu7OcAQmpWoJzvA40m9u
mq7WDNG80St3r3VOUB9CwUQXmXs1Grj2Oza64/z/0tf7T3lvkqOKmGNTbhUnxw8fI3t+L8vOwd4H
cNqmQl+hDaI44HWaVDkveQD5xqH7GQpKa2VCwM4bBufLZraqTzSNIPyE9W/06uSpGMt7jj/15r6K
2T0Kf0mc97uYU0ATi7e9bLfEbuh88FaasOAEH37hrdmpFbDLeDyBFUMBy1vKy74AlSbE9s8itq/J
c+kZsSsCVCPJ7/4ZWdKWsMOlgbD1BGDVcbOpkxXtKunbQGZLf3BTBg1t9fbnAk1ImUxsOURX9hxX
gpIuAUiv4nKOPxqzud5mjEhpE5jTzfaIFRe05OwXOurkDoL1BM8bnQacat2nSFbfnxuXIxR8KyJr
BdA8eNduEaieI4I83QrMfUvj4Ja/6jbH4NX+hCjeqihOMUXj2GQ7cLArmzWhn1YkDZin/cZ2+t7o
XCcwi81nI6CDZdLDsWWBsC0MPIpvFsqsYitaeR7cS+sQt1meSin3n6Egc6QI14LwIJBZvdPqqQV1
9uD2K/qe9qOSQ0k8GgPiGmYbI2fhapvJXFdQmM4m3MPuEHCAmbT8VpK++niqYBgJ35Fl94LvTeKO
HFI=
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bQC79m/9TVQN57q4d5B9WKxWrmwNRrysZMRUkRxJmPs8MuZPBbCWtQat2zc/YeVHWlO5t2unWcvd
c+LcE4MPxovGREUl5RzIHyPuX5cbMXEB1eOc3B6ec6we5Hfk6NI3Ka/WfWAA1NuuU7HoQBZuURR8
8iE8cgJ8WMxsgLnuFrKtiTIcWgwrzYSGq1cP30y+SmoXDpmSMLbYpHcEHNK5OYkW4CW8pGBCbBDD
aTvKzvyRnJ+ir9vYHrrgai5KASoF7l0DrtlXMBcKspRXzmvWb53sxKfmS/11vf7xFKjv3DvzCZ/U
6ZU6DSUBS98Ut2Gqm3sSfNMi8aXEBmiuQ8QfHA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
2UbjjTxxXl/NU8JtU2/+xrqBk8Ad0SdrdhT5incLtbIehm+jF/yMpz5nshNhbwYGoZ6ZzEjftC1j
NET3YhmxUEDv48aQtsu8czv1gfzae8esLzxE5JTM+HuZhhgE6ojCc4ocO/0xLYLx0qUi+fYwge+B
1C2WCwH5bA7llJnjEXJuDJLuulFrZamGqzM1xbQT6j874xi8esB8Dq1W6vV/e0kLc8f8Z3cYOyvn
3xSp1hnuEznkVskUmkAwvISIsuuduGCW6sce6qx0ObXe9SIUrNIP0mpa6WWtxG7tkjQsWxne0Mpf
FJTM0lkQ7WPYhXXsmER2gMKdLdbvhlyMKrdCxA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12176)
`pragma protect data_block
qi/5V/7RMnwPs36oeqdy2phcJwmFruHRUeydzxmD9AzWpj5mshq6apy07vUWDkXSsTJwVYZB0sYa
MhT3n2N8SsLv8NtDKuCqCWaNvrhzrApc3ilf3TyYFQPiA0IkZLdiOOLLZjmTeY7JU/MTnq+10HUV
CP83dbLMYECSRoudlSYeKV1L915nt+HXDEG9emn9A3nMG+ry7vLz1GLJEMo4I4OQOG5clhn8l9Gs
ZKuiSiHREbJcZr872D1b9wDo1kUZxkC9K7upvkFgWK7X5nuRhmC9svDIw81NstNQciNh3EuulSRX
z/EGijB2n9kGMrX2/wAWO6ZFFx0AVhW99JRfXs1mr0PKmkea8yrjAfux8MCWYfnDhABPW+llyHDg
3QQaT66IoigRP5TzuyA1NLPADoVKK+VhRS3NlTZrOws07UCjQ3ybIcMkoSJ7A4swVlcN0yy6KPaL
VNo+PJDSjb3mm+HHWxdGkEcfntFq8fAmci3C54uBH/RaT+d9+rG4VRcdxXeRBB8xJ2SzBfBr6DHO
G+KYfCNz6y4gW4X0iJ/fbnsScz0ZmvHqTYm5cRx0X1eF/8oW8gdqmKrViJEygXjkP9dW5NFB2t/r
MGePhrwVSsQri8jZF82jLOdKvmCngVUdlkhSD6FT0fWaUFmfgqCT4W14xvjO3FPpMyXl/KNQdRuf
1N2C5R+56GMeawKafARu1qN3Bhk7XivddAylXkwljGrg4T+nsod/mOBgcqXwKpKngQ+IlO8+VXNC
j6ZFWjYrqdqF2ItMDFlHAtAbwBx/+aL/I7HDVezNRETnuQBiZ4HzFUf4fBf1FrR1Xa2xCSiIuMNZ
V+GHScwDn8HRAj7nlAb596ersD6ILK/ApCr5Jjceit3Ag7WNwei4tgYymNT81YMLxeFcL/ppTkry
qLCtg29gB4H7KAsa1f4O3rxW8Y83B1t/FU4FldlJXCSQ8D6YCITK2E3dJQ4brdBbb3R2FiXXBzQr
b8X8G2NEThifi7K7cfJK5GyFZpqca0lvSFK5m7J0vE5dget+YsKstL29HtNw424iW90nhIgc8HBZ
GqrHRm5zfJuMIawg05LGloWBKB4faNj/XG3Nzn2Y9uUK4XhH/wFBs+BsrMofgvHyzE1sFmvPaJdG
bftLBMn9R+eBX1S04UUNzQRSGexxAWPl5i6gbphYlP29rp5qgqR3NGzKrBnDupasqDBMKL4adCeH
dVHXyRrwziFRawwBjxR5sn/HiRr4w/48ExK2h4Yq5GQJZOAG0nmbkMwJKOVKo+l+avyV0MQnlhuR
0/b5q/PfjRPpkY6itdUr+cNkTV5ybeFyBuTPE97gCA7iMMpJxHy9lmsCW/x6mE99XRG1FvcGKxp0
Zyov6UBhu/A8nlDwMepz3tuDZUpk4WsTgiC3V2zCDmu0yBOSWCELz16AZSP2hYSvGqVQ7mT+FkoE
HN6C0mte/QRdRcQR6ekaNYllmd6RA9hcro3lQFcrUv6PK3gyVG5YEf3nzzAkA9BMPLlA8zSvu5qj
6+UWdfyXSAmrDYkincPfhXGij0sdaxzWYOTIcRQmZJxvZhbH2/DRurV63B8Pysk94sSVMSdpEepo
rEgznozzW2fwjI3PiaR8pUc8cHjU9uJhk+d+AdY2xEwLAjm3y/yxfbmLnN0P/cEqghOjquxcDgnh
UF3DYKaMxzBF77VK8j0sKJeJv8eRx8sh7Eix4tX++u/iEarGpxS4VnUFIqNaHVOxRVyQNNiaiyd2
b2urUa3Tg5P/xM7mCwRteFcz94IY2PPq6ttsQNrTrUJqq200QOqkQy4AhbPtjY4dO9Y1/Ai8Q7Qd
5FuQCQBmpzwxChU59+4A2izlsja56mI9cKn3J72yO+CEmmCLmU0IjBoui2ymMfjVFj2Gbk76A8bR
G/QyxGn5jSrw9LQI/rzmpo/GuWtlE5yT4SEaLoSLPibZHFpJYmi8zsaW2I6QMZRDDOnyerEqnyMf
pgdmHD9ttV1HPfNCbH8AzSxMzAopkcXDPaxxmoGgYFREV+vG2fFRaUJdS7aeDchZhP64dpFFSyF0
2diuAQAeUnrxd7rEWhHDEFaV2EqfRFbZaA8csk8MZjUAieTEOrRz/qUy7/DNMmaIJpkyIViBV052
TuVdp1P3mvY1y4sepxdcvs39vwcrFrBgz0cl5GFn5JIHH4u0O08tWsQHW64YARC55G5FUgPI+3L6
wUW2Sy6bf4jW4SSqXIRDc/VUmyC9SzR0Y1vhl2cn/am2cNmdwvmp8Wmn7hKSyHgEsrhPl2VEPnEo
Q/cu3cr9wCfnHlhfhbeR01X0suzA6jS8j+/E0wTSRZl6G7qHuITfuCJR3Pjkp9bbsPdJMu6AZMJH
2eqdsMSHV1B7nc+Uf5GWKR+Fm5uSU4J2J4S6I3WhhnqE8J5T7LsIdisdJGMoVWbsTK/Qvy6Q28gB
XYwGdGRtO9XYy0NiybmwvDy+cc5Plc6bp8mgiIfLfXnnTIublkA2pL/ZfFlkf1dk3uymKdf5Jt1Z
TXM5br3bTHgyPg1lF6ddK25/uzIKi17/KYPZ/R4T0U1gIFxdsZnTDP4Vj48Ln5P0QN8n+gcTFRmP
6DC8prbD3qs7B68XSZSV+/8x8Q3grybaRaKKJmuMmMd4nMybx59/IDFMLZQEATHjtHG/IqHtlmDp
n+Uib9VPTuIoPhgv2PSeu7qvAhq7fJgDb4Tp9+qu0Z6pV6s62oUJJVzDwFzXT0SKr1X9yrWHs1El
QNUiUlDFOuaOAPU3JZacnhTY6mrHnlvL2/dtKJSjqlVCvDHqYibUJLpMhJIksupVYSFUF6W5IyJl
Ul7pyCdu/FClcXHx67r2pXjs08wB1A6IRItTVWwqLcDoH4TGuo5h+UR8w8OXNzW9fRbZ5ok6aEdz
3SejHVvLkrBL5wCXvQkGUr9KpvL7Ixkh6vPPggyRL2KevaQQraNOiwc8hoKh06Em0GdmaBzDdJtW
DtNRlNLFv3X7+TZ1x7cagv+8cgx8OrndY4hgPmY/fk6i+A+PStgtYXVyyfm+i2H5IUtWgL5UQQSC
M7lAmCGIqdZ5vDD/WxXWnrr8rrpI8+bdxEHr3mZjRFhnNMR4FI6BrmCHi5qptR91zVGzVic5PiDv
K1WqtJEo1EgPi+kg+rtO0VuJM6Mu5ViWy7nxCZ8dfmr5/Zn/88Jf4I3Y2ItI5WG/lAVE/XLNl/q/
ixS/pAmwgHxYgOeqSqEI14j5hvn4Afn/FUJT0KbtPItoXXHBd6qFmR8xxH0jJS/poNszrpIm5YWF
f51goV0iDpWn8QaZCBnxq1U7B7kSTwzO6uE9llm2FcCwhkvkjEQnbcvswErXq3PZFcjZhw8HP3fL
d3PNfH4nUc0dOCKz6wJXjHEYxYUFMeWz91H9AlmZeoipGgP2nQTra6hRf8ZQSBPgkd/sa+IXmF3D
oLvTp93SEdaTTA+QBxpSBUHpR2SvQ90tkSb5MfQ3i260T/CzUd8e+e4ExkjP8O6Hx+K8Hf4pEon4
g6q1K5O3WauzCw7yiSZ+LD7h2EhxTdtrIWCCCOZx++A28N3+PeZfMNljFFan3aQiMAFTuzlmdhqE
evJ4Opl4Vf9sH5UAHzIKVioLvVtdxMM/tqEC+O99vO5RBopAiDIrHPLYpAUqJIg0WMN965p6Qk9U
VAVxcacyhBuqFZpVw7jxATdeK52k+3eagVhBxnXrlXxTnzoe+PvUXEop2giSv6AAmyO2c6UxBFLB
PUzAvq7zZhQDJTmMn6nytIQziVI+w8zCEbonCbEmI5U0vF4LrA+mxSD4qFsSZcuEEoF7l0OUxh55
M6FHqi+AX2Gh2KNSYnWjdd4lPeodP8b5gqN05U2Y8TO7owzRvmHc1dydPfiQrAYKinrzHImkGbKq
5KiaTBOSlmfSEZlCn+SSZlpG/0WQQt56qzWEsfByBD6584M9qV31jM9E32FXMRqxS/PslG+lRlg7
/+5/tQwVkIljdMxg0BmWoYssCxWz1/NanRmrJURh0xyki5tn03Znbc9shHmsWVfKVMke8j3zZDfw
R73o7IHyTSZrf0ODW6nKVS2DG7Qb5iA5t8VsAkiBqYV4EC4DEh9OGlfewx3WqdZADYhxYa8xTrwN
/IN5c0A02YTlhDngjANvx0YcTDLzR8CpcO69oAXWFzI3nnEzLKYJ8zpbBmbpQZ2O4/byx/cRGegy
AAB842MKxN2G1oCCP9j5NmEeZBLStTzy56KR98uxO6eeaTmMx3BhKYz8550dj8l3z43gZS2Lra7X
eMhszF4i5dp10NiDYM/SsrGu9ssGSnsOCIUeHW9+Pjxrt+usJjRR1stf9al9PJgWCTOOA8JqxRmi
rtRMkXkqmsqOUtpM9P9rxYiTHX6GT+6kru1BsrUQyUUQyz3SSW81dcPmIhSzg1BV0ltsCBLHG6CO
5VgY9yACroRpFwCBsDKSAMGUa1/hx9SlCgaSEFMx8+qa1z4qm918EuGQdh5rqMSfm1I7d+rrz85e
8NOMjq4MmSrs1tlXAyPfq5e+EDofUt11MQPezIfXqeRZqxtyYoDnD8NAMoKPzF4aZYtffUjH/kJz
M3xTlGqplaBbcIbsOdfnFpBYDSr1Rn6vbTj0xWuQ5ztr92aWI0U8d3yQ0nZ9xt0BNGtXuOqQW1w/
vr6jPzkrG62zya9W9IBIEheOLt+PczroA1lspF+hXBJv1biJKCLVyhqLlppl7k+JpRXDuo8h2qze
7V0SvLitJtXgySmpJj+y8v10RPUI0SrioJOSe+jKZz4hJZs0KhLxkgyvqDVD7OwaqOj3cAuG4Yg4
cA7yMWFWlWlgguRfvDoO2bwePCkFTymuu8l7Ns4LUMNO+PkPX8ju3+FVAamzSdY0rDXxW1OpNIIg
3aLflBa2Et6NAvtRxxuj5DcOMFCsKJrlqF3LRak6wwMZK6rYRR/KoxxC2PjP6N8VWiJRqphZriAi
YkRDJJu+P60a8iiVQ02SIj2WvlByAF70Vo2BtCDzH4vrANS0QSkuwHUZnh724AGhMx+QHz/US6dt
jD/dgqnylbymsBpNmyh6MRRC/XpbHK0WZbHflSz5TNVVa5yH1KxHxtNM5CJrgsruHxDtoyR75B+/
ckkWLPX76djLLHiVroE8AhM5vSbpU2h1ePQ2Dg5bXy4+KQ+CEtjr2ELp0JIZDGyW+zCj5kFB3FNv
YkcEnSScJvkFkQraX3J+ObrhmCEBFNP8undOzGqRT8OpV6oP1oFu/XwNmzYt8KAvZ5Tm0mX0huZO
Ex52hpUcysAmke4eZ1yAWIr62/IowI5SqUCZcGx+rlztQig0xP7coZ+ec9h/gmmiZpMZ+XUP01KA
VRknCZWVFaCMvI6JpSEPvWHcf/92UiS/rVTETqcQCxWZ+zgMdEIVSRtBM2zaJAbZ/WQ28IKHwL1a
0hDvqzTHU0sfg17brp0FjVBK0uf5P8j6rM/VvYUjbgxx+BvqDl/PZ86zksMNLsuP2EET5olONyIz
cs0ph0haOZxV4Yz1OjbpXwN25jkGFMDjO2g1RZ2D9E1CdgISSbmCjmGyqk7ZsEOV5zcmNPII/BWJ
c/XRZpP/Ni62d7jg2Bd84Qjh7szWrZm3nlv4ogiAIjRCuwpoZt/G8LDdwjmzt4CZD+kODiq9itH9
22pTjwLBqwyMwXW8qjoj8Ip1qszJwFgdhrwQFc3ei6hoy1c1KM0RZDFEKpXwWPXyv/TVpOgZm7N4
DQYeIRmsloQatQfYSEgoqmBdudsJ7KDoUn4vcBRKrDevcKw/2AeaOhND6jYS10rtkcc+hMk94m6q
JJyaKbeUbgqTsLUpOkG5Wv8IERscd7x2EJgdRm75gJzSejNYo1Z6kco45KDp7DfywIt/k2IKTNQv
SMz+Gu+gvdhO2ybpmIiE2sbv8BFBPu5PWcIe9wHkxjpoyrqOkPP8ICW9cwCZeXxZprvYEAFjo2ig
3nl9amh/noWtBE/sJX5DNACe4FX90K4UblcHOqDDBYxBDOo2oFtIKLERyFuJiGu5CofWQQhhsDXi
mk6/yPd3B5MGimOXp4e1yZtS4nneemPm72XnduFRZgUW6vGXgN6I0R/Yz++3CZNtYn5zLbGXm3ib
vHrUnLQQc5mv7j54jPrbFQqg7idIgSInKkC8RTpaxQ8BMN+17y9B6rl+uBMvPD4Aw9v02PKAspOL
DzJygeIQ75sQG5Gm6/pGFfWw8mwgULMI/UPus/TF5Oef2XZH/9kD0LdfUz7GfTs9zWIb+M0sIbt7
kbzcEO7+t0bxgz1TgD2bDVO3qxdx6Ecng0JnqGbTBNPVd96Sqd/akh4R8gNY5GHQ8g1cg/yRXSYK
JMIqN//k03soAc0vlqaQwJMsEWTwYIsXuk5c7spcrg1RkGA9/YNwCR0MJsUtQkCPlOeHlUeL6xIJ
S8v5eqv/QFGilMgHTEhX2h2nPJlzgLBka6cE76S2mTTCt3VGrngeJoieMHZb+B0xfhwmZH0bIiij
3xj/FnS/tBU4yUpmsQ8WpVErhzG0hZoQyDCJo9Lq3LY98W9hMsFpmMDHSVjdOpX93FXITGouwl92
O+ByU85nIEJ+DQsO9NFTe9JEb/gmJH18+lLUvvL+0hz3efwxnTtdStFurixDYCialSOkeNv1ryn/
gvzPtUHzE5dvKy3huWg40GYyDZH+Kd6vZEM0jOXQJdjl/ik4DfFhH7u2jTYwdI29NaVHeqryGN2O
7rzHyETjxZeCPynHBHp3FydMUDKRwBPy2zH54wMsQgaryCxd8Ybt0cmt8RSMg2oEATem00EClmVT
+R6TO++xGuceDdFA023ZDeAag0qJTgbZ21zsT/SMoofIpbecvrzPxHPV8quKHnQ0YI9zBrbJYo/K
ZKk5OO8pSHKJ76xpZjuVD/KwFY+ynOWgErv/IcCK+cVJA8MeHK8O31jXTGRizOdltDAY+wkWRbZX
NBzOj6SLC5Xzdo2hujkObssifCFRKu5zNJWPZrglioJdDTpJEj7/AmfDdAqgTb+9BGnrHOWoTImY
WOblouSDhMMtUqrSPECWQc96X5osq7VeIXfLDhOtTy5BZY+4YNV/ONj2mtDPjRX6krL5nebSDhWD
eFs3WYq2EylUTgBjyvmXO89dejFbn1/FzLnTYDaykMrzFTGXTs94RDrGV/rLPj+9HHZ48Rz2+ON9
VcxaUtyG1HO/71prRgFENF65DpdHXIITYH31+ouicLpxRUzZ3s98UTMUBiO4J4srrrXmrJ2dl856
wBsJstlNo2YQV08vlM//XdOaX0lmaA1HZx1J931VZsUB2RMEh05E4hqsaEN7xTJIb0PCogSZPt32
e3yXCkOSNj5HY2+VoCyXLJ54m3uXRKUhWHejm2Aph9fCWiB3QhooE/Jlg84548ab1SW/K8MATYec
dknUzskTlWEJXo0eaPNc4YbG79Qaja51WNQPqLNOL4twKRoZR3dX8OXGoib5qBUZqhzWfkxTYB84
WRWZVrhmOuQ6vA7FjyZzZR0ha7ePy3Vl6MUg8R280Cv0pERJO4VuVck1dQdZfBmqOqbN5f1cNW0v
3Cu0ejjSAs4c1EZw1RmtiCYy0bOaNYc/zzpeyNrelx8tCHhDJZ5ZM2xsyVRsmr6aLD3ubG/qGjHx
5cvKTGTVdJye3vI0BsXRA3CGB/R1/xOb2oqeKD/l2RBOUpehZqjanV09y4boG9sS6oLdOVX6A9bA
ST/fe51/bTxwePN7vV1zP9jyjbMhJtnKDwju0huWOWwb/AIiqmd4/1oSJEQ6/4HPtTjDi+Xh8foy
QSTyrcRrPcAbKCzDzBObSB8dnCILfumfNE20gw7v2nc7ZKUJ2/hqr/T1q/FTygSQE7hJ0FIjEBwC
0cOeBPukh9Cq0c/KUYipTVrZ3jd/VGowH7fWVz8EQjiTmOdISCf5UXj6AwwxTUO2qqpwUGbm2tXR
1w1F/oJB2ym5j2j3z/PWEiZvgru/xXRpseVPzG6Oxxo8qHZFAvDOsA4AY86tgdYMDZj2UlPowurU
ItbDI6Uu4f4T0aHs2tKsppxCNdUMVASNiiSEkoHRyjYaA+/1UZT7vQaKeyzqlP+lyYeCoKFLH1kF
U6LLGP0Z9SRZtInN3B2ckdgvbIRr1NeNXztm6uQvJphe1oua0YN+Rbvj/PKjGXgPdjlqA3HCX2mq
JrwOa932ta5BnjT0aSEfcsHf0hEK7379HPskJYVME2tJLbdBj89dWqKaGytWGgyhyfRvy3wULtkt
CoL7dk7oso34UoORLF4J9MNcR6pyjz1/jEgl2dMX38oB24dV8EI6pLMr+wiD3V52eV51E4K9vrKL
ATrtpK7M5QxK/tv21GQ/K5Fj7EZJ619gTow0WOUoOHMQcJL2VoymYOxoO3Jjyzd03tt+O7LnSeyH
CZvADdMG86UH2sIDByguOcDArdUIsFugX9n5JYL8lINDIIhB0Cekt4DDq76TUyoFjMjRhahiO4RA
CKzEzzcP9KWBRbkFc7f9k3y0PpvtzUFAhgQ/loR86fdwxr9n4QK/0NN/zaOgeAVu7N+nWhHL4iNi
wjYvHkbYgRFP5s4Nqd4HhUP+Y16RiP8rE66oyqWaF4q+bEc0qRXWE/+IFPBTZKTxZcnIxYsiepA5
ll4wXhgf0epFJHgbjP1WlronDvU+4s9gUrhLC9CFfLEPmgeP1x1HIIZWgAqmPhrcJ9MnQ4FugfeM
um0Fy5QnfARzAxnwqq59uh7t3k56iWSpFSwcSZkKO9w6mjxKwbATv82fMljqiM9GvXpvBcwhmB1Q
GqmOuDmeFkrC03PDI+579MCRCPb64p8uqm2DhA4eg5dzK+m1DJUhIs/KO0W2DdSXGEheM6IIV6Rx
h/HYGHSKmm7os/Ajt9JvmxAhwAsBAg3uRu+SiQMhTehfNE7fICXlsZ8rYr2/aVi+AeAoSA6BMQNB
G0eiUYWLshn2s7FGIjOK8ze27HDBvGCYu5DManHainOn97ZraC++w02UDKXv7Z+d2ffrRocasyqW
6NOXsw5GBB62BxN3aX8IBTAAXkjcb1q5DqW4lN8DYucr9VgRFRhliQWq5qakWVkFtSx7beXqha/M
ybSIAybPSMZCfuB4Ghy1VKe9Eo6sWym3Jp4dsZSt+PdY5OVTj9RJdamYgE7kBjX9Wggmrt7nSmzc
fbCrjrBMGlDYRp1f7EDhL+8W8jZEZ6NUqwMG1Dk//4RReZRbQvHGBjYnvDWxLV8V4f7vDhJt+6ow
w4z893LQ2i4HWb8HQSswfv7Dw9+MMz7t61X6ZOdwCSYQmlU3sZQ8accis6xA4CrMLUewIBEVH+OS
IB0i0e+TUNTcFZ+MeBe1aqib7ue+DOo74Rdsj5RtWZ/o3095SpavU/6L+hYSkk6veg6xE4DKCUyw
Dm2s3WNF+h2XAvdkbbIeGgu1QM1VlKCyxMRql6rqVjrCGqebpCrZm4qxXL6HyIS3jqpjG+QvaWUv
po/d0pofeNQ0V4B6MVYRbDaGwAVH0nTgWcIuLgMzf8idwkBqelwH8S5+4UsC3xg/WmRHVZl2a0H4
iXORk9oa9rF230vXJxHf/8GDB9RBQYjWU8Gf6MfFV69/4OkN0LTjEpIn43x8DCUwyy/bS7hzev+h
nu18Luz7nelQGpt0JfszsOE6HYm4vqa6VWKrC+kT+QSnEN/vXqJsDWJnG6XWxZCyAzkum5z6kfyf
NZCjy4H0jFclvj8Mt9FU4GWK9M7tWbMBD7Yr9W6e4ColtIp8PO+nEGxuoRbk2qCODW2zxZ6dqfqz
qSzPYonf3saOY2LwQzk9GLd+1Nxt5J4nQ5Dlo316N0EeuHAjzeM5UqJNepC9XECYyaFipiQTUA9H
kRHxFOKciyuAb+/Mw1LPa+ai5QAvLZLJwIShMpSr8KvOqWcB8mvzss6XWJQhmmI2nOcrWlE81C4/
u3jMOOSvDVr0RJFY4GZW3NeHdzKVmSGPFGGoojsTlmpr2oZYUAzpo9Cvliy/+UK5GBE9HCHt496d
xqX/eklqmDLsGA+qyUpsm0rnVFe3o0aW8zHQaGXqh45CMLQI0g0+XnDimjn68ute5e/t69prAf6d
f8ZAAhumvcjne6JrHjCZn45RiAUSyoOQzn2VC1mp2OfxLw/YQlY//YyREpuQuueVQqjhVMl+pRSo
mK7c9WP3VHcHuws+K13D5CYFUeZ0bi5/eBi+tfE1PmFyiuEjMYyv3k9Xf8SuH8a5QyeA4t96sVo6
NXTJ8JQbUWywLk03RkKAOozSv5A8dpOfVTifrQHF86EW3s82t+P83eqr9hflx4DYRG8Ti7//znfP
sMaxzMSoiTd/NeTW7C9brttyAJIc7346nVmXsYKZi5JlHzTxGQpX7xptdUGAof1ljdZQUcK+0DEJ
J+0sU2t4uRciotc5mblyInW2DGJpTQ6VqcWydHq98HiupQHileolLFTxxgKviDEOsWOhOi7tFiOp
MTGaRKIru18baaxWNQ4rffRIyEQE9hcPBUOP+UnzXPsvj/Y2mqqHGjNrb3Q+m3jSC4oWGnGRuMfI
mhDL+F/+9Q1PjuicKw9ZKpEsnZ5LonT+afE8vJYR98HsmFua6mrQq79gZhZkR8mIBsLaAifgT9B2
V3fGzm5wmA7fymyiQYlwNmZH4vRirW9CR/DqZSfSRvhbmYF5geWyxeowCpAiE8tRm8Ad7Loc+DRG
icFagaPIH4OZMXWEuonZ+6OOcrT7wZDELlRnyrauh9YG8nnQ6kbQ4Fw3ysmaqJ2Fz2JbnAfB5DOd
J4kykbsCfJah7teHE8U/G6jiYysOhjssFn9gx4+zlBpr2Wqn/RDGNk9CrfyUJVOjN3rN9MwJlhSr
QWjvX7ehzUIlRWUYROdPAMUwOgmsg0nBnsNC8gVEYZuLVfKfROe1HNSl2RulhtzTg3LWu5JHU88d
oJKT1XV0czs8w3e5xMJiET3nxA5M8pzrCV2/RtoikMSgoXNCiC8hGvN9U4SWF6iaHWYeh2WGdmZp
6TsnKwPOEAjRbU7t0CJwOdm/1hdNSC84ZmIkoLwLhcFbZReFvKpR2V4oL0Ujyy3josJiSdpgUMHj
qeura4bGXdQ01Gyz/qRiK8ijR6hgbvkbdQc8gGSNSYiHhdJIU8L5hJF3qQgdYwl+JRyqyAZF7BBk
mS8dpX0SK7lmKHTodCrGCkzwScOkQXbdFa6JvF8UGdmhE4SPrRQUZFUZOfVWmzZ3xa5M7zKA/6Tz
wg1hC0uYOUGIYXD2lcsTt+ofEAbIT2JuEnpVIb7tTrVJ/3pzOYN/iaawGWQIdamCY2I+dk/pxFbn
OtKYR8w2ROghB/GRrPR8bKGZgiYBGAcORO2UnQAQFircSaOoJu+KnLH+1Hc+p4GHsbHv5RrIvSbU
+lSyCgdp4LmQQwgmGu+wiy4kEPQSgsdcWpbUnqtEVUXxcWiYM/2YdzHgCYdpgZnPC5I0nn90QuFH
s+pvelQ4FwR6vaqnGt5NcJ88IsMHiF1MbZ2CPIGJPtfnRBrleg1yX+HBwawCqm9gK/Hu/YkI2erQ
08lgjpdRinIi6Eg0L3pPppyZt3aRELzv14n65Tu2wnC8OBPeJ6UQCR8/5Odl2ztV2AVDYqyUzOdJ
B3SmBtu+DVKshfRmDOzrWQMvfjY22PdygsGKU3H6Zab+/BqXETM7nFPOO/ccit86iNtHptL/8hM7
YUn/D4f5xxZOIjxmB9Nkjgq45N+mg4zriscPB8J3Fe3GHQfyo8higTLgYFAlRr6AMklsvBzqIjW8
GZgSMsk1kczg2HqfFZTU7sVGZOVKCKslTwvZFtT2WIGWQOlbxnSqXCpdGBdcaOffj/KaqGceFkIo
FlZRvz0rAu773Q4wVLXGL5yG+VOtzJSJkuhxMHsUJwtmgcbVZvxPmIImhRhJe30t61rNF3/y25Rv
HEppQsLfO66Hp9jZErMm8jv/N6DT/EXUk3Y1F/+LN6wnFur3d5L44TXrShb2x8OrE8AgeLUQETIe
HpM3IWM48N5wbPN81voUCSmmoWf17JXQy80mDLIizeHcnWvn9fpJuJtRoLINbCRswad+4IhcjmGd
Ppj3E3bxQhxAlo+DAQ6Zh8Z+nXZ5xBseJJfdF20Ez/0aqWXznYvKa3dlcR5SX4latByYZc2AJq3S
eEPW0rGJQW7AZkOUFMC1RxueRLC7k+RUyQSObp39u4sApKeinv9kmXONaUHk653HJ9d3Rj41EWw3
9/SVBcsX5jse1i9tJbJDR1quWnmJeBX6G0cCj0ptIQc3aORzJZCn6FGWUXQfmqGElaT9kFfSTgG0
NU24dMhZ2QaiAlJWi+qGVeLnJjFdJetiL+VjTfMt3eLOQcRteVFdVcqy/ad2jaiAriiHwLggy2hr
v2OMAmvJTg5F/j2UT8tt4R+wKrBRf/qKs512lG5IM27nwbMn/OJi5PGLaFa8ohfsFYNcfBB1w9lt
Mv/jrjBf1GlcI+QA6/+BiiC2na87/PRnllFBVa1ZiFckDtRCpNM7B/4YjKHHFkz+vuyUwbsNOLf6
e3qTdq+Kb8DaYqHouVt7uR9tcecld9yvcX6cBGbYHoqvv8zn/Z8nvNuwxeJo/qngu8XMm7QFKc9F
fBYsCjErZ0pvqBUPzv7nkn/9bOD+tcN7U4skiFvyZi+VsWAVBQPCN9K1YBPvX+S6yB++dRMsC2JI
yZQpdtzVKHehm/qioRvCFCouhaGO6NXH7vs0UOzDKxSm+wZTcWXZgfojyt2aME8yK0RnRkLSxEqr
Uc9CjKZezP+A8Rny5FUh39y+PEg4ph5SQbSydBgtte03zOJUrDi7fsrwpLDLAroYRn7dtFJ72VeG
A0PNn1kcEKkoVEeZIUffQhgMOMHhybBDK7LLWjprT1CUcQ56KyHuh5zIOhWCPbqsu8uP2xm8zi1r
Kdv2R0GVEpLiKb8HQFiBRk5wGcsB8ztD0VS0c52gl89fDDmDPeo1K8i6DhQqKDH6KOx5thWD+aOH
qTRFsPfMGOpOVv98IdXyZlXx/plgRpcGb/wwuCBDi/NwbHN4/ybO76eCC4/Id11UZcLBopJUpNgo
qI6P5f7OAuEEgUEyB+XgrDXCeoIdH9qNl4a06lhstXQ8jBe25Bx0wO2yN/Idj8YxquB89Oac7J/M
3XbmEIuvFYc1k/8cf5S2kK9ALyvpbLByeTu2EmmOi8F0kkTnwxtwnF+z6dKHDHlVMTGIC3/fnR+w
CF2/aHEe2/V++eMlJtApfsyC23WjymMabnrHnfovj7A+0Q0Jo+fYakCWrJ9g7ZqGgScXpWT1muCU
2WwAkVAN/mK4KDN0a9p6LbPy+MDeDAOjLkyv+2oDYWH29JVrce6mGUTcdmlJw7hC1fWN3AqM47mR
fvW5s3ymvxvW0D8XJpNlaa6u0u4Nrw/h4ptwJtaWteNc2jkddbyvwui1ID6GCRsN+vN6kP0XB9tL
kxq0iXondP66AKxj0+H6ACFayrhhl50+z1bozLLu9eeyYrIMbYuI4m9dAmArrqhtFch9GEWKDyxr
LiC4S0Kpkg8m8BeaHygmeMP/phQYZEw3J7NkBe9hCBHUrI0QkDt//K/90uAnaAByjK0irfXC1kep
uNJhJpHvhFXQ1HUimJ6IBVkBI8qCW9+EnSilC5SFpewUK62bsqv8YA+p7Q52nLQkkN9RO8DN8R7d
M6BtS5DP3vTvNVzH8rWBibt3E92Uihz8Fn1Q59yUZMBCsPtCy7VymT+j2kDoxOfxl+dmlQz3YnSy
JNmjxNg4CPwujt7YqANyn2GLBvZnQvGHrhHxQdo5MFqOvf9LZcaaH64FOl6Z0BP66bbTgjZPODt+
PlnSjFybBugyyLZisZpIZ0VNrWUO4WvrB3lRqk3BB8jpHyxUwLZmF9HRJt9zs5iIYEfncrYvXcFB
QcESJ5YWmH8Yrw0snd+JAqW0chnYQu9ewScupZfQDOxA8V31wNLbjrVC5FVNgVlZTqYzN18HgQ6R
haatp1mWGDiegIoiT+tI84dyHdf7jrrfaTM/j1AE3vavLf9k3C8NZS60WgKVPOxT+HoFz9WgA+B9
KyoJYM4gfDqq8NdBvd8CcFvpG87q11BQ5jD61oHHVtlq3RTJ9jKMQM/JYnKrGwBg6KelwO2Z54j+
1EJ+ByPRtTC5/GZgtLhcvXZKxCchtcs6Co7C4LB2vYNwhCSfUbm4gAiIGZZdBtyk24Z2AGXsT8zx
8efGDhe8+abtYCpsfKqsPgB5MBm+m0lBvAaE8gc5VJN8T3ZP5bYnjJxWKQVSdEtdbJrVxUOzX8p4
adtV/uS6QxL/ijSkV/7bjRf4S1nsMJqE9aqshJJFrrwyA2RVNQtn/LhaEzVdmqdr8yesn+ZU42kg
wF7m4miqKwvOvShHqknkKX7LaKHqpY81yGBmsid61zaOBbrXrsFywf/UDKRb6OfiOrNqLyXVjfsW
1KAv0qZLbMiiypPA96bPFSuPuUhrSRkWgz5wQuKR3aZMLqnAYTP+XsMwEeQ4XQfSD9I0IcHjOZM7
DGU0LooEg69lpyi/9kGjGH4uDAF0285XAGii6bNCQaOXQ8YI1k06syRmt+iLjG0WrRWsuv1YkDc7
kPaMzRYeZuKQCQ4wRsjjF8syALddCHcp2qwG/Y5QTO1i4TgypG/W+eEo6w5nBmCOedpyHUyZeXyP
aynyUoDo9T849Ho3yyc0kMBjtI3JTy6o2cocyWUyjdSE05Bcoo5SWr7pPkwIFCdAUFTEv/Ctfr5b
24MDuKPFgZMvLY0iC/jnjMBf6OOsYdA7gMvzVk9x498G2lYr7FSTZF7TdBJlZShh59vro+F2KWuM
+pC9QdSPVlaiN5PLw3k2QxZRWaUKjBWr5oujNELRmFzVWxD3SGVsJt/p6r7/gZ4VZ2r4tPXbyUbb
NzJ8YQhEv6eVBFQMJODFo8WMQ45ZwUdf95ja3TNxt+wlTiGexUeHl9GGTaWKSvC3lBVwjEBosfID
aPHmrrWvHZIzz3pYD0n1oP2jS16lVXZHxgh8h4mVQ7MFWUQHZABx895a6p5TgINvNyugC9jtMc6y
NiMyckN0o2Dxv4gchfcvHbL0jGFDcAbAr8BWUoA05lyTcF/4AfKqYx4gUZIp3zARsCGTLsLQCZUm
amLvPswjrA6Aec/tLDS6EiT5y/E1tSr5mhkUSWTJSDW/Qnod8GP8nCcLbo1RFuADhHJ9yF6CYK+/
TU6QSg89Dbrzl1dYwYd1DEZqFBIyKYbwzsoH1FMycKG7uviGLCMVhHxXrkAWsFHmCqs1sA09nw2Z
por8UeQ2lwKHEIRbdbW3tijoJecXoMrAiwD/uGcwyKEgDppVwqVm2dBBys1qvKVk2E76zrdjMs/7
Rn9qbi9FpnCswwIDF3ND0GEu3ssPS9qBDm3nFuWrVa4abwkkF7EgNO6qQGrYbpHDkCeAsXAsvx2O
53kR3A7kcIN8NY4j6s8TTNBfclPIUm/wfcey1nPjsXRKmrLfXNf0XuALVNPrpd4avlDuoD5lRPLs
z85fJLIK9zsVYnnox2oX2miEjTJ5xuwnWEQNjnVy3Nafed74p/ioElBFlUqvwASxZF6ZSqvPJ6l2
mYLJJslXycP03TNufb71iaf0LkLNYcaJ6m/uZUZhN0Kcj6n5Rte7kMvBVbj/YauxWq4Fd5C+n+8x
gYk6mcZkrYVe1bRBbsmRJ++Ce2emBnd3h2LhpdMFYQE/CHU2DL7NxtMxcb4tzwhixyW6LbEpql+g
1usbWu40Std1PAq4TfShWmfUK1hVBopdsBMJcqNTyG/N4LoZe1mo0qQ9m+5Q3mf8cu6qcv3jaTw5
pMN3jt3umx1Zp+XZffiwcYSBPv7wjDXUiN4QiXt9whQ2gBOpGoUWAE1bCXoX/O4fMX4d031HJ5IK
CZWVcWzRGDCcPhLbkQ2xuh5qy4F0wRwRQOQJxD5CigSSpJdrl0Oj1RdEVOg49g34awo/xQAg29Vo
nEi+RJA1KzG5ZuWBUzzzglh+ZqfyyVnxGCv8EdolhPP32WB4XFk9ZyF0Vuc5n46pu+0MV7WKwCXN
08ec/Ztsn81Hto3lJgYXu/TDPMvLyp6HDHD8C+zQZB15xxYFFRa/o+DSg6z33I024hbG+ldovj5u
4WtMWF7j9omqGZJdw2qO1ou59/xpPDnvlzEuavPwq//SMRs7TDwUplh4KewB5ykSXPnsKIv1vGz6
nzEI+lfCvMaKiZJWyYFcPylu6/zx+zpOhDeDpZH9J6B/GPOIhx4FTU832ehCWQYEMsfcF6ZnGS6B
r2rXxgGs73boZhnHX4wPsucmJPAGNuw7D97LPUYCGJowTmw=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
