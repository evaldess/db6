// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:27 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ fifo_adc_readout_sim_netlist.v
// Design      : fifo_adc_readout
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "fifo_adc_readout,fifo_generator_v13_2_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "fifo_generator_v13_2_5,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (srst,
    wr_clk,
    rd_clk,
    din,
    wr_en,
    rd_en,
    injectdbiterr,
    injectsbiterr,
    dout,
    full,
    wr_ack,
    overflow,
    empty,
    valid,
    underflow,
    sbiterr,
    dbiterr,
    wr_rst_busy,
    rd_rst_busy);
  input srst;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 write_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME write_clk, FREQ_HZ 40000000, PHASE 0.000, INSERT_VIP 0" *) input wr_clk;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 read_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME read_clk, FREQ_HZ 40000000, PHASE 0.000, INSERT_VIP 0" *) input rd_clk;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_DATA" *) input [13:0]din;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_EN" *) input wr_en;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_EN" *) input rd_en;
  input injectdbiterr;
  input injectsbiterr;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_DATA" *) output [13:0]dout;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE FULL" *) output full;
  output wr_ack;
  output overflow;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ EMPTY" *) output empty;
  output valid;
  output underflow;
  output sbiterr;
  output dbiterr;
  output wr_rst_busy;
  output rd_rst_busy;

  wire dbiterr;
  wire [13:0]din;
  wire [13:0]dout;
  wire empty;
  wire full;
  wire injectdbiterr;
  wire injectsbiterr;
  wire overflow;
  wire rd_clk;
  wire rd_en;
  wire rd_rst_busy;
  wire sbiterr;
  wire srst;
  wire underflow;
  wire valid;
  wire wr_ack;
  wire wr_clk;
  wire wr_en;
  wire wr_rst_busy;
  wire NLW_U0_almost_empty_UNCONNECTED;
  wire NLW_U0_almost_full_UNCONNECTED;
  wire NLW_U0_axi_ar_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_ar_overflow_UNCONNECTED;
  wire NLW_U0_axi_ar_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_ar_prog_full_UNCONNECTED;
  wire NLW_U0_axi_ar_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_ar_underflow_UNCONNECTED;
  wire NLW_U0_axi_aw_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_aw_overflow_UNCONNECTED;
  wire NLW_U0_axi_aw_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_aw_prog_full_UNCONNECTED;
  wire NLW_U0_axi_aw_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_aw_underflow_UNCONNECTED;
  wire NLW_U0_axi_b_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_b_overflow_UNCONNECTED;
  wire NLW_U0_axi_b_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_b_prog_full_UNCONNECTED;
  wire NLW_U0_axi_b_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_b_underflow_UNCONNECTED;
  wire NLW_U0_axi_r_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_r_overflow_UNCONNECTED;
  wire NLW_U0_axi_r_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_r_prog_full_UNCONNECTED;
  wire NLW_U0_axi_r_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_r_underflow_UNCONNECTED;
  wire NLW_U0_axi_w_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_w_overflow_UNCONNECTED;
  wire NLW_U0_axi_w_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_w_prog_full_UNCONNECTED;
  wire NLW_U0_axi_w_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_w_underflow_UNCONNECTED;
  wire NLW_U0_axis_dbiterr_UNCONNECTED;
  wire NLW_U0_axis_overflow_UNCONNECTED;
  wire NLW_U0_axis_prog_empty_UNCONNECTED;
  wire NLW_U0_axis_prog_full_UNCONNECTED;
  wire NLW_U0_axis_sbiterr_UNCONNECTED;
  wire NLW_U0_axis_underflow_UNCONNECTED;
  wire NLW_U0_m_axi_arvalid_UNCONNECTED;
  wire NLW_U0_m_axi_awvalid_UNCONNECTED;
  wire NLW_U0_m_axi_bready_UNCONNECTED;
  wire NLW_U0_m_axi_rready_UNCONNECTED;
  wire NLW_U0_m_axi_wlast_UNCONNECTED;
  wire NLW_U0_m_axi_wvalid_UNCONNECTED;
  wire NLW_U0_m_axis_tlast_UNCONNECTED;
  wire NLW_U0_m_axis_tvalid_UNCONNECTED;
  wire NLW_U0_prog_empty_UNCONNECTED;
  wire NLW_U0_prog_full_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_s_axis_tready_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_wr_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_wr_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_wr_data_count_UNCONNECTED;
  wire [8:0]NLW_U0_data_count_UNCONNECTED;
  wire [31:0]NLW_U0_m_axi_araddr_UNCONNECTED;
  wire [1:0]NLW_U0_m_axi_arburst_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arcache_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_arid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_arlen_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_arlock_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_arprot_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arqos_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arregion_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_arsize_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_aruser_UNCONNECTED;
  wire [31:0]NLW_U0_m_axi_awaddr_UNCONNECTED;
  wire [1:0]NLW_U0_m_axi_awburst_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awcache_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_awlen_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awlock_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_awprot_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awqos_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awregion_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_awsize_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awuser_UNCONNECTED;
  wire [63:0]NLW_U0_m_axi_wdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_wid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_wstrb_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_wuser_UNCONNECTED;
  wire [7:0]NLW_U0_m_axis_tdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tdest_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tid_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tkeep_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tstrb_UNCONNECTED;
  wire [3:0]NLW_U0_m_axis_tuser_UNCONNECTED;
  wire [8:0]NLW_U0_rd_data_count_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_buser_UNCONNECTED;
  wire [63:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_ruser_UNCONNECTED;
  wire [8:0]NLW_U0_wr_data_count_UNCONNECTED;

  (* C_ADD_NGC_CONSTRAINT = "0" *) 
  (* C_APPLICATION_TYPE_AXIS = "0" *) 
  (* C_APPLICATION_TYPE_RACH = "0" *) 
  (* C_APPLICATION_TYPE_RDCH = "0" *) 
  (* C_APPLICATION_TYPE_WACH = "0" *) 
  (* C_APPLICATION_TYPE_WDCH = "0" *) 
  (* C_APPLICATION_TYPE_WRCH = "0" *) 
  (* C_AXIS_TDATA_WIDTH = "8" *) 
  (* C_AXIS_TDEST_WIDTH = "1" *) 
  (* C_AXIS_TID_WIDTH = "1" *) 
  (* C_AXIS_TKEEP_WIDTH = "1" *) 
  (* C_AXIS_TSTRB_WIDTH = "1" *) 
  (* C_AXIS_TUSER_WIDTH = "4" *) 
  (* C_AXIS_TYPE = "0" *) 
  (* C_AXI_ADDR_WIDTH = "32" *) 
  (* C_AXI_ARUSER_WIDTH = "1" *) 
  (* C_AXI_AWUSER_WIDTH = "1" *) 
  (* C_AXI_BUSER_WIDTH = "1" *) 
  (* C_AXI_DATA_WIDTH = "64" *) 
  (* C_AXI_ID_WIDTH = "1" *) 
  (* C_AXI_LEN_WIDTH = "8" *) 
  (* C_AXI_LOCK_WIDTH = "1" *) 
  (* C_AXI_RUSER_WIDTH = "1" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_AXI_WUSER_WIDTH = "1" *) 
  (* C_COMMON_CLOCK = "0" *) 
  (* C_COUNT_TYPE = "0" *) 
  (* C_DATA_COUNT_WIDTH = "9" *) 
  (* C_DEFAULT_VALUE = "BlankString" *) 
  (* C_DIN_WIDTH = "14" *) 
  (* C_DIN_WIDTH_AXIS = "1" *) 
  (* C_DIN_WIDTH_RACH = "32" *) 
  (* C_DIN_WIDTH_RDCH = "64" *) 
  (* C_DIN_WIDTH_WACH = "1" *) 
  (* C_DIN_WIDTH_WDCH = "64" *) 
  (* C_DIN_WIDTH_WRCH = "2" *) 
  (* C_DOUT_RST_VAL = "0" *) 
  (* C_DOUT_WIDTH = "14" *) 
  (* C_ENABLE_RLOCS = "0" *) 
  (* C_ENABLE_RST_SYNC = "1" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_ERROR_INJECTION_TYPE = "3" *) 
  (* C_ERROR_INJECTION_TYPE_AXIS = "0" *) 
  (* C_ERROR_INJECTION_TYPE_RACH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_RDCH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WACH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WDCH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WRCH = "0" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_FULL_FLAGS_RST_VAL = "0" *) 
  (* C_HAS_ALMOST_EMPTY = "0" *) 
  (* C_HAS_ALMOST_FULL = "0" *) 
  (* C_HAS_AXIS_TDATA = "1" *) 
  (* C_HAS_AXIS_TDEST = "0" *) 
  (* C_HAS_AXIS_TID = "0" *) 
  (* C_HAS_AXIS_TKEEP = "0" *) 
  (* C_HAS_AXIS_TLAST = "0" *) 
  (* C_HAS_AXIS_TREADY = "1" *) 
  (* C_HAS_AXIS_TSTRB = "0" *) 
  (* C_HAS_AXIS_TUSER = "1" *) 
  (* C_HAS_AXI_ARUSER = "0" *) 
  (* C_HAS_AXI_AWUSER = "0" *) 
  (* C_HAS_AXI_BUSER = "0" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_AXI_RD_CHANNEL = "1" *) 
  (* C_HAS_AXI_RUSER = "0" *) 
  (* C_HAS_AXI_WR_CHANNEL = "1" *) 
  (* C_HAS_AXI_WUSER = "0" *) 
  (* C_HAS_BACKUP = "0" *) 
  (* C_HAS_DATA_COUNT = "0" *) 
  (* C_HAS_DATA_COUNTS_AXIS = "0" *) 
  (* C_HAS_DATA_COUNTS_RACH = "0" *) 
  (* C_HAS_DATA_COUNTS_RDCH = "0" *) 
  (* C_HAS_DATA_COUNTS_WACH = "0" *) 
  (* C_HAS_DATA_COUNTS_WDCH = "0" *) 
  (* C_HAS_DATA_COUNTS_WRCH = "0" *) 
  (* C_HAS_INT_CLK = "0" *) 
  (* C_HAS_MASTER_CE = "0" *) 
  (* C_HAS_MEMINIT_FILE = "0" *) 
  (* C_HAS_OVERFLOW = "1" *) 
  (* C_HAS_PROG_FLAGS_AXIS = "0" *) 
  (* C_HAS_PROG_FLAGS_RACH = "0" *) 
  (* C_HAS_PROG_FLAGS_RDCH = "0" *) 
  (* C_HAS_PROG_FLAGS_WACH = "0" *) 
  (* C_HAS_PROG_FLAGS_WDCH = "0" *) 
  (* C_HAS_PROG_FLAGS_WRCH = "0" *) 
  (* C_HAS_RD_DATA_COUNT = "0" *) 
  (* C_HAS_RD_RST = "0" *) 
  (* C_HAS_RST = "0" *) 
  (* C_HAS_SLAVE_CE = "0" *) 
  (* C_HAS_SRST = "1" *) 
  (* C_HAS_UNDERFLOW = "1" *) 
  (* C_HAS_VALID = "1" *) 
  (* C_HAS_WR_ACK = "1" *) 
  (* C_HAS_WR_DATA_COUNT = "0" *) 
  (* C_HAS_WR_RST = "0" *) 
  (* C_IMPLEMENTATION_TYPE = "6" *) 
  (* C_IMPLEMENTATION_TYPE_AXIS = "1" *) 
  (* C_IMPLEMENTATION_TYPE_RACH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_RDCH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WACH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WDCH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WRCH = "1" *) 
  (* C_INIT_WR_PNTR_VAL = "0" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_MEMORY_TYPE = "4" *) 
  (* C_MIF_FILE_NAME = "BlankString" *) 
  (* C_MSGON_VAL = "1" *) 
  (* C_OPTIMIZATION_MODE = "0" *) 
  (* C_OVERFLOW_LOW = "0" *) 
  (* C_POWER_SAVING_MODE = "0" *) 
  (* C_PRELOAD_LATENCY = "0" *) 
  (* C_PRELOAD_REGS = "1" *) 
  (* C_PRIM_FIFO_TYPE = "512x72" *) 
  (* C_PRIM_FIFO_TYPE_AXIS = "1kx18" *) 
  (* C_PRIM_FIFO_TYPE_RACH = "512x36" *) 
  (* C_PRIM_FIFO_TYPE_RDCH = "512x72" *) 
  (* C_PRIM_FIFO_TYPE_WACH = "512x36" *) 
  (* C_PRIM_FIFO_TYPE_WDCH = "512x72" *) 
  (* C_PRIM_FIFO_TYPE_WRCH = "512x36" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL = "6" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_NEGATE_VAL = "7" *) 
  (* C_PROG_EMPTY_TYPE = "0" *) 
  (* C_PROG_EMPTY_TYPE_AXIS = "0" *) 
  (* C_PROG_EMPTY_TYPE_RACH = "0" *) 
  (* C_PROG_EMPTY_TYPE_RDCH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WACH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WDCH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WRCH = "0" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL = "511" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_AXIS = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_RACH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_RDCH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WACH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WDCH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WRCH = "1023" *) 
  (* C_PROG_FULL_THRESH_NEGATE_VAL = "510" *) 
  (* C_PROG_FULL_TYPE = "0" *) 
  (* C_PROG_FULL_TYPE_AXIS = "0" *) 
  (* C_PROG_FULL_TYPE_RACH = "0" *) 
  (* C_PROG_FULL_TYPE_RDCH = "0" *) 
  (* C_PROG_FULL_TYPE_WACH = "0" *) 
  (* C_PROG_FULL_TYPE_WDCH = "0" *) 
  (* C_PROG_FULL_TYPE_WRCH = "0" *) 
  (* C_RACH_TYPE = "0" *) 
  (* C_RDCH_TYPE = "0" *) 
  (* C_RD_DATA_COUNT_WIDTH = "9" *) 
  (* C_RD_DEPTH = "512" *) 
  (* C_RD_FREQ = "40" *) 
  (* C_RD_PNTR_WIDTH = "9" *) 
  (* C_REG_SLICE_MODE_AXIS = "0" *) 
  (* C_REG_SLICE_MODE_RACH = "0" *) 
  (* C_REG_SLICE_MODE_RDCH = "0" *) 
  (* C_REG_SLICE_MODE_WACH = "0" *) 
  (* C_REG_SLICE_MODE_WDCH = "0" *) 
  (* C_REG_SLICE_MODE_WRCH = "0" *) 
  (* C_SELECT_XPM = "0" *) 
  (* C_SYNCHRONIZER_STAGE = "2" *) 
  (* C_UNDERFLOW_LOW = "0" *) 
  (* C_USE_COMMON_OVERFLOW = "0" *) 
  (* C_USE_COMMON_UNDERFLOW = "0" *) 
  (* C_USE_DEFAULT_SETTINGS = "0" *) 
  (* C_USE_DOUT_RST = "1" *) 
  (* C_USE_ECC = "1" *) 
  (* C_USE_ECC_AXIS = "0" *) 
  (* C_USE_ECC_RACH = "0" *) 
  (* C_USE_ECC_RDCH = "0" *) 
  (* C_USE_ECC_WACH = "0" *) 
  (* C_USE_ECC_WDCH = "0" *) 
  (* C_USE_ECC_WRCH = "0" *) 
  (* C_USE_EMBEDDED_REG = "1" *) 
  (* C_USE_FIFO16_FLAGS = "0" *) 
  (* C_USE_FWFT_DATA_COUNT = "0" *) 
  (* C_USE_PIPELINE_REG = "1" *) 
  (* C_VALID_LOW = "0" *) 
  (* C_WACH_TYPE = "0" *) 
  (* C_WDCH_TYPE = "0" *) 
  (* C_WRCH_TYPE = "0" *) 
  (* C_WR_ACK_LOW = "0" *) 
  (* C_WR_DATA_COUNT_WIDTH = "9" *) 
  (* C_WR_DEPTH = "512" *) 
  (* C_WR_DEPTH_AXIS = "1024" *) 
  (* C_WR_DEPTH_RACH = "16" *) 
  (* C_WR_DEPTH_RDCH = "1024" *) 
  (* C_WR_DEPTH_WACH = "16" *) 
  (* C_WR_DEPTH_WDCH = "1024" *) 
  (* C_WR_DEPTH_WRCH = "16" *) 
  (* C_WR_FREQ = "40" *) 
  (* C_WR_PNTR_WIDTH = "9" *) 
  (* C_WR_PNTR_WIDTH_AXIS = "10" *) 
  (* C_WR_PNTR_WIDTH_RACH = "4" *) 
  (* C_WR_PNTR_WIDTH_RDCH = "10" *) 
  (* C_WR_PNTR_WIDTH_WACH = "4" *) 
  (* C_WR_PNTR_WIDTH_WDCH = "10" *) 
  (* C_WR_PNTR_WIDTH_WRCH = "4" *) 
  (* C_WR_RESPONSE_LATENCY = "1" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fifo_generator_v13_2_5 U0
       (.almost_empty(NLW_U0_almost_empty_UNCONNECTED),
        .almost_full(NLW_U0_almost_full_UNCONNECTED),
        .axi_ar_data_count(NLW_U0_axi_ar_data_count_UNCONNECTED[4:0]),
        .axi_ar_dbiterr(NLW_U0_axi_ar_dbiterr_UNCONNECTED),
        .axi_ar_injectdbiterr(1'b0),
        .axi_ar_injectsbiterr(1'b0),
        .axi_ar_overflow(NLW_U0_axi_ar_overflow_UNCONNECTED),
        .axi_ar_prog_empty(NLW_U0_axi_ar_prog_empty_UNCONNECTED),
        .axi_ar_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_ar_prog_full(NLW_U0_axi_ar_prog_full_UNCONNECTED),
        .axi_ar_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_ar_rd_data_count(NLW_U0_axi_ar_rd_data_count_UNCONNECTED[4:0]),
        .axi_ar_sbiterr(NLW_U0_axi_ar_sbiterr_UNCONNECTED),
        .axi_ar_underflow(NLW_U0_axi_ar_underflow_UNCONNECTED),
        .axi_ar_wr_data_count(NLW_U0_axi_ar_wr_data_count_UNCONNECTED[4:0]),
        .axi_aw_data_count(NLW_U0_axi_aw_data_count_UNCONNECTED[4:0]),
        .axi_aw_dbiterr(NLW_U0_axi_aw_dbiterr_UNCONNECTED),
        .axi_aw_injectdbiterr(1'b0),
        .axi_aw_injectsbiterr(1'b0),
        .axi_aw_overflow(NLW_U0_axi_aw_overflow_UNCONNECTED),
        .axi_aw_prog_empty(NLW_U0_axi_aw_prog_empty_UNCONNECTED),
        .axi_aw_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_aw_prog_full(NLW_U0_axi_aw_prog_full_UNCONNECTED),
        .axi_aw_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_aw_rd_data_count(NLW_U0_axi_aw_rd_data_count_UNCONNECTED[4:0]),
        .axi_aw_sbiterr(NLW_U0_axi_aw_sbiterr_UNCONNECTED),
        .axi_aw_underflow(NLW_U0_axi_aw_underflow_UNCONNECTED),
        .axi_aw_wr_data_count(NLW_U0_axi_aw_wr_data_count_UNCONNECTED[4:0]),
        .axi_b_data_count(NLW_U0_axi_b_data_count_UNCONNECTED[4:0]),
        .axi_b_dbiterr(NLW_U0_axi_b_dbiterr_UNCONNECTED),
        .axi_b_injectdbiterr(1'b0),
        .axi_b_injectsbiterr(1'b0),
        .axi_b_overflow(NLW_U0_axi_b_overflow_UNCONNECTED),
        .axi_b_prog_empty(NLW_U0_axi_b_prog_empty_UNCONNECTED),
        .axi_b_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_b_prog_full(NLW_U0_axi_b_prog_full_UNCONNECTED),
        .axi_b_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_b_rd_data_count(NLW_U0_axi_b_rd_data_count_UNCONNECTED[4:0]),
        .axi_b_sbiterr(NLW_U0_axi_b_sbiterr_UNCONNECTED),
        .axi_b_underflow(NLW_U0_axi_b_underflow_UNCONNECTED),
        .axi_b_wr_data_count(NLW_U0_axi_b_wr_data_count_UNCONNECTED[4:0]),
        .axi_r_data_count(NLW_U0_axi_r_data_count_UNCONNECTED[10:0]),
        .axi_r_dbiterr(NLW_U0_axi_r_dbiterr_UNCONNECTED),
        .axi_r_injectdbiterr(1'b0),
        .axi_r_injectsbiterr(1'b0),
        .axi_r_overflow(NLW_U0_axi_r_overflow_UNCONNECTED),
        .axi_r_prog_empty(NLW_U0_axi_r_prog_empty_UNCONNECTED),
        .axi_r_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_r_prog_full(NLW_U0_axi_r_prog_full_UNCONNECTED),
        .axi_r_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_r_rd_data_count(NLW_U0_axi_r_rd_data_count_UNCONNECTED[10:0]),
        .axi_r_sbiterr(NLW_U0_axi_r_sbiterr_UNCONNECTED),
        .axi_r_underflow(NLW_U0_axi_r_underflow_UNCONNECTED),
        .axi_r_wr_data_count(NLW_U0_axi_r_wr_data_count_UNCONNECTED[10:0]),
        .axi_w_data_count(NLW_U0_axi_w_data_count_UNCONNECTED[10:0]),
        .axi_w_dbiterr(NLW_U0_axi_w_dbiterr_UNCONNECTED),
        .axi_w_injectdbiterr(1'b0),
        .axi_w_injectsbiterr(1'b0),
        .axi_w_overflow(NLW_U0_axi_w_overflow_UNCONNECTED),
        .axi_w_prog_empty(NLW_U0_axi_w_prog_empty_UNCONNECTED),
        .axi_w_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_w_prog_full(NLW_U0_axi_w_prog_full_UNCONNECTED),
        .axi_w_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_w_rd_data_count(NLW_U0_axi_w_rd_data_count_UNCONNECTED[10:0]),
        .axi_w_sbiterr(NLW_U0_axi_w_sbiterr_UNCONNECTED),
        .axi_w_underflow(NLW_U0_axi_w_underflow_UNCONNECTED),
        .axi_w_wr_data_count(NLW_U0_axi_w_wr_data_count_UNCONNECTED[10:0]),
        .axis_data_count(NLW_U0_axis_data_count_UNCONNECTED[10:0]),
        .axis_dbiterr(NLW_U0_axis_dbiterr_UNCONNECTED),
        .axis_injectdbiterr(1'b0),
        .axis_injectsbiterr(1'b0),
        .axis_overflow(NLW_U0_axis_overflow_UNCONNECTED),
        .axis_prog_empty(NLW_U0_axis_prog_empty_UNCONNECTED),
        .axis_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axis_prog_full(NLW_U0_axis_prog_full_UNCONNECTED),
        .axis_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axis_rd_data_count(NLW_U0_axis_rd_data_count_UNCONNECTED[10:0]),
        .axis_sbiterr(NLW_U0_axis_sbiterr_UNCONNECTED),
        .axis_underflow(NLW_U0_axis_underflow_UNCONNECTED),
        .axis_wr_data_count(NLW_U0_axis_wr_data_count_UNCONNECTED[10:0]),
        .backup(1'b0),
        .backup_marker(1'b0),
        .clk(1'b0),
        .data_count(NLW_U0_data_count_UNCONNECTED[8:0]),
        .dbiterr(dbiterr),
        .din(din),
        .dout(dout),
        .empty(empty),
        .full(full),
        .injectdbiterr(injectdbiterr),
        .injectsbiterr(injectsbiterr),
        .int_clk(1'b0),
        .m_aclk(1'b0),
        .m_aclk_en(1'b0),
        .m_axi_araddr(NLW_U0_m_axi_araddr_UNCONNECTED[31:0]),
        .m_axi_arburst(NLW_U0_m_axi_arburst_UNCONNECTED[1:0]),
        .m_axi_arcache(NLW_U0_m_axi_arcache_UNCONNECTED[3:0]),
        .m_axi_arid(NLW_U0_m_axi_arid_UNCONNECTED[0]),
        .m_axi_arlen(NLW_U0_m_axi_arlen_UNCONNECTED[7:0]),
        .m_axi_arlock(NLW_U0_m_axi_arlock_UNCONNECTED[0]),
        .m_axi_arprot(NLW_U0_m_axi_arprot_UNCONNECTED[2:0]),
        .m_axi_arqos(NLW_U0_m_axi_arqos_UNCONNECTED[3:0]),
        .m_axi_arready(1'b0),
        .m_axi_arregion(NLW_U0_m_axi_arregion_UNCONNECTED[3:0]),
        .m_axi_arsize(NLW_U0_m_axi_arsize_UNCONNECTED[2:0]),
        .m_axi_aruser(NLW_U0_m_axi_aruser_UNCONNECTED[0]),
        .m_axi_arvalid(NLW_U0_m_axi_arvalid_UNCONNECTED),
        .m_axi_awaddr(NLW_U0_m_axi_awaddr_UNCONNECTED[31:0]),
        .m_axi_awburst(NLW_U0_m_axi_awburst_UNCONNECTED[1:0]),
        .m_axi_awcache(NLW_U0_m_axi_awcache_UNCONNECTED[3:0]),
        .m_axi_awid(NLW_U0_m_axi_awid_UNCONNECTED[0]),
        .m_axi_awlen(NLW_U0_m_axi_awlen_UNCONNECTED[7:0]),
        .m_axi_awlock(NLW_U0_m_axi_awlock_UNCONNECTED[0]),
        .m_axi_awprot(NLW_U0_m_axi_awprot_UNCONNECTED[2:0]),
        .m_axi_awqos(NLW_U0_m_axi_awqos_UNCONNECTED[3:0]),
        .m_axi_awready(1'b0),
        .m_axi_awregion(NLW_U0_m_axi_awregion_UNCONNECTED[3:0]),
        .m_axi_awsize(NLW_U0_m_axi_awsize_UNCONNECTED[2:0]),
        .m_axi_awuser(NLW_U0_m_axi_awuser_UNCONNECTED[0]),
        .m_axi_awvalid(NLW_U0_m_axi_awvalid_UNCONNECTED),
        .m_axi_bid(1'b0),
        .m_axi_bready(NLW_U0_m_axi_bready_UNCONNECTED),
        .m_axi_bresp({1'b0,1'b0}),
        .m_axi_buser(1'b0),
        .m_axi_bvalid(1'b0),
        .m_axi_rdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .m_axi_rid(1'b0),
        .m_axi_rlast(1'b0),
        .m_axi_rready(NLW_U0_m_axi_rready_UNCONNECTED),
        .m_axi_rresp({1'b0,1'b0}),
        .m_axi_ruser(1'b0),
        .m_axi_rvalid(1'b0),
        .m_axi_wdata(NLW_U0_m_axi_wdata_UNCONNECTED[63:0]),
        .m_axi_wid(NLW_U0_m_axi_wid_UNCONNECTED[0]),
        .m_axi_wlast(NLW_U0_m_axi_wlast_UNCONNECTED),
        .m_axi_wready(1'b0),
        .m_axi_wstrb(NLW_U0_m_axi_wstrb_UNCONNECTED[7:0]),
        .m_axi_wuser(NLW_U0_m_axi_wuser_UNCONNECTED[0]),
        .m_axi_wvalid(NLW_U0_m_axi_wvalid_UNCONNECTED),
        .m_axis_tdata(NLW_U0_m_axis_tdata_UNCONNECTED[7:0]),
        .m_axis_tdest(NLW_U0_m_axis_tdest_UNCONNECTED[0]),
        .m_axis_tid(NLW_U0_m_axis_tid_UNCONNECTED[0]),
        .m_axis_tkeep(NLW_U0_m_axis_tkeep_UNCONNECTED[0]),
        .m_axis_tlast(NLW_U0_m_axis_tlast_UNCONNECTED),
        .m_axis_tready(1'b0),
        .m_axis_tstrb(NLW_U0_m_axis_tstrb_UNCONNECTED[0]),
        .m_axis_tuser(NLW_U0_m_axis_tuser_UNCONNECTED[3:0]),
        .m_axis_tvalid(NLW_U0_m_axis_tvalid_UNCONNECTED),
        .overflow(overflow),
        .prog_empty(NLW_U0_prog_empty_UNCONNECTED),
        .prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_empty_thresh_assert({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_empty_thresh_negate({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full(NLW_U0_prog_full_UNCONNECTED),
        .prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full_thresh_assert({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full_thresh_negate({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rd_clk(rd_clk),
        .rd_data_count(NLW_U0_rd_data_count_UNCONNECTED[8:0]),
        .rd_en(rd_en),
        .rd_rst(1'b0),
        .rd_rst_busy(rd_rst_busy),
        .rst(1'b0),
        .s_aclk(1'b0),
        .s_aclk_en(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arcache({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arid(1'b0),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlock(1'b0),
        .s_axi_arprot({1'b0,1'b0,1'b0}),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_aruser(1'b0),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awcache({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awid(1'b0),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlock(1'b0),
        .s_axi_awprot({1'b0,1'b0,1'b0}),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awuser(1'b0),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_buser(NLW_U0_s_axi_buser_UNCONNECTED[0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[63:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_ruser(NLW_U0_s_axi_ruser_UNCONNECTED[0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wid(1'b0),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wuser(1'b0),
        .s_axi_wvalid(1'b0),
        .s_axis_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tdest(1'b0),
        .s_axis_tid(1'b0),
        .s_axis_tkeep(1'b0),
        .s_axis_tlast(1'b0),
        .s_axis_tready(NLW_U0_s_axis_tready_UNCONNECTED),
        .s_axis_tstrb(1'b0),
        .s_axis_tuser({1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tvalid(1'b0),
        .sbiterr(sbiterr),
        .sleep(1'b0),
        .srst(srst),
        .underflow(underflow),
        .valid(valid),
        .wr_ack(wr_ack),
        .wr_clk(wr_clk),
        .wr_data_count(NLW_U0_wr_data_count_UNCONNECTED[8:0]),
        .wr_en(wr_en),
        .wr_rst(1'b0),
        .wr_rst_busy(wr_rst_busy));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
SFoQ2tXDMrL2nCJbfpmHXuteJlKaWDWl3o9OY1miFvmYb8EDywmDpLUHQktJ/VoW+17fK5WHgFVI
FZV1B91GDQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
mxGWDRjEAsKmBqldxevT1RKZvqK7vn0KlTODVXNGlRcGf9zOAmj0Z7Ppu79POBDb8oNQyCY+2q1q
BddzhQfh5WLIVX9BNUMIF6M6IF0elM4GMSLHGeYEwqSaMPC+thuR8FGj1J7z6rH+43gDYhtIeyY+
ZuZUz/Pqg8Lu63Xwe+0=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HLwPjQzkuqv5FEDBriEJS2DikBeIHB/bWuVWooHY5ChdoHatcmqCHpSvnGxVzLwObZWHFys2nR9y
P3zxywjtgtOWq/n3cYVa5li6eyiUmGXv2OE8nw1nLnAY1kzBvGd6VwQ45t6l4Hx5+oqpIfuU2KI2
7/Qpj2atiTN3Y+q5He/BMXLIxF9vWuU6XL/+HsxriGAumcZDuESdidlxOztbW1bFhYr1/qWwou2q
wynnRVKYHL41aWycgFdkDoDEFFxv8ft8+F5Ux+J5Hg5XdgRULJc6uUQE/lDG3zOqzPftlODB52zU
d0cm8gFOvSZ2nO8ZB8THnxoAGe33iIZJfMcefA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
jlR0iZ4fp9QXiFgaT07DMAK1YFLyBpsOGOOR9j2PWImFEh8oTBt4cvmGo+2z1Umbt9OMQwOhyepO
QIsKLFzUXYUba+SFFLBoCiaww24KICecbUfd3VV5sg2bEJjAdtYTT6mJqyc3vQRvBlONeBFdIGy2
AXqdK7QtXGLsLAIF/z4FG8cfG6nSD6e16gccBC6+kl5MoShdnmebKLyoo6UKFdMbDK88sHvTcD9S
LNCau6RK7FkTZg23FV0tf6cTP9Rray9YEcowm2AAh51Wldo2lGJ2W5iiDatRKH/W1bu7FGWZG+OT
+VZE+Ckiuf4T6cuu+G5IbrtMv6a4U93R0gtxXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
p/kq+JjPPJbOTWT2SRiPJ99/iH6kkVGEiluRRXpuRN+j+cVPgJD1v4QVjw3zMWLlvTGB7OOqC+JG
Lc62Wiizd/BFfGj2JYkTZMatcOWok7A87HK+vRTjr4nZMApD2jKaneJdU1279KsIEeRfImCQ2uRl
QRNMH3PPdNGYCnOGgNk=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kyyI/O29YYc5VBwhz19i7AV7MC75r43hHVKAOTBiGBhRu8zZxCwGGcNFqc2HgHcWC6nq4jCIbIXf
S3FDzPdasegnERlWvoob9/SXM88zKsyeTbUf+DRu5lB8SPROBMaIhnj375C5XLowL17MXZdmB6fV
X5ukCg7cNhCjssKt/bIJibWkfna7hvj4ye+CLWmi3LdEiix8KTwRoBS3ZJrjM4/N6FfZkXerVxs+
txkhdsmG9ga1g/xErhTRilhqrV2WetlpX86qH/64sRGVxrWeEfNoHhMZsqEK0jWDx4WavKt8XY7W
NDzMXLZ2m5Dv5HMiJWgFG+ntPwgiYYtBuwu7Eg==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
tv6UL1ZWqo3dAIlhN5UTNGzJyqzdHpCqh217JPvIvHiWJgcFh2tw1n7HWnOPcK3VhCt31AGnCEFe
HpTiinXvHna65L2X2HhtNUrsgvZlUuh/oQR273wp5JPFDPD97NQ4ELkGI+w26HTYLgZ70K5rQo87
D4AkQNRuzTRS5G12yb4RU7ZYgmkYLuq1UyqjlxyN62Del4XoqZyivOGw5H+7wlfkNRu98iQwqq12
jthZbH/ue5wxZJUcb7NmEwL+3abpyDNmWs1qORHOFoE3t97/9XMmeSCpM2+KnSKJvsV5VbuoTCOT
964fsEh7ey4IVb4aum095gQjLCqTmDm8DWFmaw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Oxo3AgNmVWgrXtMKDIThYfXr0YJfyFr7Bsjn2ge/G72mb25MA8Dbkd9ZZPtwqU1poazNnTng5Cx5
s8C1zMNEoo38jNY8zEUBjCCuasJgeMo5xsiha+3ZIBiuHS0KLrjLaPFIQZdsYevb44fg6J5YQLn5
jd1M6YdNMd1VwSezDxtbk9sN8ExPrmtwum/6L1ia9j9UlIzPTEaJ60Xz7tloPsgsbkborO2JLiIk
kIAY2q1b8tuhHzJ5DoXlvIo49wSDj75ncLrkwbAd26huob7aOmX1bS34pJLF17JzqYH0MoPJbHxb
RPdD+qUawXFsMSs2fOLnZrNxeG8L+TyAT0N8tQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CIR/vwxo0IBrPr5+bMp2YuBCQTNBRIIbqgEB18Oewkc8CuHzGCAgPyQUBUKaUG3bBy+KDOPVxBP5
cE/d3QYZAT11fyB1OMMTrjmEIZcr0Vk3nVTAnivoxxxkmdzPjkj0OcGcU9fMArPi3dfTgIsKdtCq
94+mV/70WeprgijzuZFWD7uH+gVioY/+rq/Wc1O6x1n949w8YGgSCTurUvhsobx2bonoC317J0Wm
IX17XRkSBIFgzqA8iC+GV5oCfxIGkihKmXxjIJbMamlOdCOycEkjkh3JYmm7TLNxmI65iffsabR0
t5+iI0l8eJxFhElzWeREqE43cnJYLaKZBUA+DA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 63360)
`pragma protect data_block
Q3sXJx2pK5nCQnESq9BKCKB1JFXVfbSMKIKFpxNAJApI1qd5iItDV3atBOREI3bE1rzXPVJCK/x9
a0GnJdusWNmyi6sxPsguu/czN42QC/1MUmCyYtIjIAft0w1V6jD83fcoYwUrtLKQG5AOerRn3W88
T4SYyoV+IIdoI0ffGMO734gyQGQh9/TH/hbtAf0wRGXizmGZfe/MS+fkHYK13uRoAMtBC/Si+naP
LQ5MviGotnLIXaXwFVuNyZ0spm9XE2pnvo7h1XdHVPZ11nCEB0BFrHV1gXANxSxh/XQNdQyYs1fJ
HJiQMifq+0b+aPXt/C8NVkJPyV1/wweCQv7oq3Iw+0EhIt8Xz7X4LpKpwww5R/T3eJBfEirBHtFY
k/EvFUqqrumvzH2/Igg7xueu1uz94WvQPRADPO/7hWyxqS2ATJXWGuJsId8rLCBx3Eov8DLko5OE
kItmCRnIkmdViINknRTbcesNyLwzBL/dOxm3mf1Uz1TAlaBLv1wXnPnOSsJChdiNn9tDf80jj+27
0uBfdse0N7tksDSiMN0V3l0aTxJhRF4T79iV5ZWu0y8OVTJ7Py8pXMjvepi4IOBxRI5ElbhzB/ID
xDarrwziEGA4kMPX72p4ousy9DkwQVKV+9EDMfm6Bz1cBeL+hVvIL49VaZxMoxHtyaCeNGtDwPcZ
WZCAZSEVPxjPoPvEqCGSP3sHAL5rZRsw9/sAetQwql8fRfvmNx9ZKYttX+qpc1vTwEasd/XNBLhS
6bdX+wHbX3enSBsRgmIgsHv0rUkVN4vZJ0/mX5JrI7W9DzoXTuFbpSVfCfEOODeCVxLDaLdVnuVH
xNIte7i06dvPPukf8r05NN/CtlGlGiIQktXoR2465wJNVqssr1Sx4nmFcamlprWHnT1FSCNkG82m
CnnI9kVglhobdvmp95XP4pZSjzX4TZGmBc3qXIzk6WqfByJtAMZkZf4jq5P8jeOBM6810WSSB9H1
1ufN43zQaHtQnHlHi9BQ378+L7zFtkiXVm2ac7IVGtQG91HM2CXaPewJJ1SEm43w4EPCalbNnDG2
f5qZzTFONTxQaBnRBatIK2+geQ9DC0TVnPyHteFQ2Km2d7vKPvMXEa9F23/gRGw6x40I5acVj+nS
zx3Li7AWYe8Q9+EcJomL10S+Ge6MBPcnbQmAi5Lf6n7NVnlxSKLWHdMEWqYj4xYTQZMwFiyZOlkw
gpUHGYi7UkZ3WFrdajfeBW0VIGGa0akjoH+FfIbsSzcAnXYGVvI+w/6DHKqLrmybOknTSf+9UFIg
VtKvTIHMxytIKS5TtqGXhNn+NWmobRjDZupuGzeGSBggZEhZvYoKYgYHtOMDgHBIb9J33KntchMT
pDeJ9hHs5eNm7R/p9MrO/TshfLdHYH2Im3V/4LKftRPWtHxn42qhctaXz+XM3V4D5mFJ1lxzfExp
P7vYnU8nejcl5mVKXDfZeMDgZNI8o2nM7zKzrwoddS4RqcGiZDhTwbE9ikNy1z3YRe+TOeqaJrn/
nagzewltBIPc4dKaihrjYmw/ctW4JMdREzhIc0rsKr5UOd0URRG7YyOLuni4eo6d/NeVmmmxSDL7
3j8RKiGG21dGpP6KP01W8kUNCCHD8MFnDi2Ebn0CIvyVFxPGCuskeOWrH6QYbqHyEMFJtTMFGlkk
SJBtK1oRHJN/We59sqeeeMpBWIgvO0dqcQSVKqT3cpXR1R8TfhA2etjfcsTL3vWaD2MvkOx0MhZA
XzgIWUPNHIYK3H7lBnxnl6qxazL802G+93o/F8EDf1Ytnzyv7EMIqKr1Vkvc34zfzh/ebi/wW26Q
LHtMaW42+1wFII7yHShIa8UMnOpzXaznT7sQu9iN9QG0AOma8eKJ0ZxLleZZ9248qBUGSF07EZj2
a2T89ikDSCJZGTDWNgzcURBvQZgLjErHzJD9sVexN8JKXDE2zjZ58jjEEOXX40tBUd0QcnqfOSyC
tiX6LctUW/N4SytaCvyBaz4kf+BVOTtm72bITsAwSdWe0o68o+8YGstqxpBaRRUlQBuit5l1jx6Q
3cVgauRvHVfFYyTwOO5EnuqSeghplMlKElFoPTp/iZyXIAuBpcK9iSZyaDBVUpnc9BtUT7eskBjp
uy7zUWav5/zvssWr5WVljzKu21blJoAvfvG0djPG9S71KLLcbiLrOKat/bSVi5f8QM6MNDYXLFhL
lz11miETnmGHXB0hVyH96mZzIpsbqV2z+ZjtO+mrQyMlSf8HHoSWTOCEJvH0ldHX/d6HwCFSxLBa
ABEAQV2aZhVYb2yHVelVXYKTtCuUGrNU5UYgRoUgh+mdZZuuKR/D+fT36bBzRjC5qq+QXBDr4NH7
WQagcbv1OfpsRpUyRwC/KCFSDCRBbJcKAi+rNXWrYTiwk2hDwgpuQrNS7owbK2oj5V0XZ5Arsjqj
CGfjoGuPB73U6twQ4mpdT22Mhc953gp78bTUNhjcXHJga7SM4qUDaEptFmq7q7RBvc4tFVebyqHU
iP902ByC3rSvX7dZeTYxHaEwHos3+lCR/2S2yYHu1iXQ+Novjvm08JPYvdTnHb+WWz88la1T/iXD
YOlVCNAhDYGqCse6Kf3zkkmk6Aq4nGl/nNwOHDejQUMU41cMJ2/aFhDK/OWIR2+3uBSNGAzrrA3E
vyBF5BSUdgkqv5bXr+/w4UF7eJ1HKKJG4aOjMZ+joVv01wICnT9oScoYfuS6mF3PMfUsGZG5eu2D
TPEXPelg4a4x2IC9wUFf+Ozso54lH8y5DlwzqTipYny5CZMAEDkrr4IRSuTd6AhhNjTKcWRpUQvA
8PCpphzVBzj3IyMdb7mMzC5k2obaDUh2l5VRZBvghIsTtewa+b41gmrJEnk76ddLKtba6txQxn2q
5N9Wnxai/R9UvPXlqKeMCD96QxDayyU5ckkPFDiKGb//eOG/xm9Z6sYPShwuQYPrNFAm0EjNrw/y
1IFlaAKSfc/XT/WOQzwL7pupLQ8rl/yOolv1HSLxGTlrgkuSMPMZP3xo7UE6Qk6SpYQRg03+W5IX
IOs4BfBV+jVzCauvztUPl9guXdiMm0CFrhbPyBCdnpkQ1DqfPTcWD7uuHJckVUydnbNYnQWvixdv
Y31IPU00x1ic/YBSMJWNNMhfuR5c9cKAUX9XkEn5G6NfSmDPSyZXv/SG7mErOWT+E0vmq5PK9TxJ
k9UnuNvLkLR4EMSU3spF16cKsXfQ1ZGppk5HZFnmVdPQZgNWU4BXxguOM2XTH1ARQ0mqHJwlyiER
RrYKPdH90n3QD8EHVCVIydK3Gpi0+uXivagu+umm5cOzDekwWzz94JEMRq2bOkVsxdHhG2Ls2x95
sAvebJumyZUJqspNun36m4CWLPmR+vhuSqpZuQsJM9rt60RVm7XNUCP3Moe/3ciOKhV0k384WwbP
OPDHfhIekHd+CI2eSrMBXI4qWGX0AT5iBNbCPdlkMA1BqpEasFHCHfIrqRI3/2woDgFtMUZvtjtX
U0qON7GrFL7eEhliPQzqzX7pWEPj79kZ8aALTNLjbQCI471ZBWxhPx9Wzuxt7VU1C6jPt9nQCGC3
/IvxoR5ni9k5ZTFVTqA+fLKft08rG6/EvVvfLDTj06HKKu6LIGnsdt4qDAOo843jfPbPT+T1lGhd
Whg5qM6RwuWA1Ri/Ai0ql3JU6GtLiqGmdrY8xYHuF59yUz0+600uy4lRbvWelqIirrWV33SbM1tg
jBMmj2CfjnJDFt6JtXpWRQAGqDwo6kmjSxBvnVwLHkEO02/0UFl54N7ycpsOUR35IfcChVuJ9210
N8RR6a5ovVUEvcABsRj521ld1ejjijv0zCd+DDVQShHT4BUl76T37SVq7heaikdCieDSs446/iyK
Vw8aHIsmAePmFvEFBqiDY+p3jLCdla8vn17IQ+PYbcfvBASgVZ1AfCq0WIB9JG2Tc9ozGL9oXU3U
CTegDJNzTJgKaQDZp6gG622EJjZtd+SAugLGmx+plS64eo05OvYOcG+1N9D9Pq1byp/rZAaJenLn
Bz+40gd+XAxFYPACFKE0RQNiSok5Tfth9pDi11/pITEDCCwH5enl4d245WaApHUyXVyTECTQM0Ya
HfMbRPf7PrL9KaTV8xd7Q+0qZ6gdcGKChinlik/eNrULL81FhErjOhFvxYA/dNE8oXwvyrcHyben
67W6ERCCNBtdEdJRrh1KphQ0wbTglHbd2wSgoAYGGY4y64Cm11A9tTbbrkK3yKk+7QKfzvSzbKXr
4q6RZ3umC16ZuErDChZk6lkfW1RY5O4m5u6LByhgfOasHXjSO9NmjLmcRrlyDtScYOxqKInj6T5b
jqqu2wsaNGvB3GYE3mZchGZ60IzKsbRIZDyT+/syR/ig1XHEWP/E2gy3+Dw46iFBLpyF2M9fsRsl
tpJTg3zEe+yVQUAiqOK0VcFrXX8meagSWwNP2cys7S0FwG5zGzagv4+qec90N8XW5h3rB2l8+ukU
ThLzSx9VDkybyKJ+Mggxh+mV4xIX2aAo3h+mtR5hawPtFOfstT4LwKNsWTZB2S3+t4Lz7gcp7Jj9
vjRtw6hLfxXvGt9DqbUgGlIb8evZzlj1ivE7CuFVQbXdOUaZv2/X64W2DXpDOS3WAGYafUDnM64b
nbMcsg/Rqu0PzdMbzlyKeHLn6Y2e0n5IwGlmXDidx2cnAkz1pPcC6subFp1XCyPldlFJ/RcD2cBb
ECS6ePAgKgepKT7PTj81T46ejxSYUPDDCX/5iQswBZT9uvmZpoyCJ6ml0wvEf2wCeEBxZvbhxY1X
j9p/62044fqS4mTOQhSJGrWRnoeO5ckUAihH1o65b+dSdvM5hNZKxNPhSy/jzRokpmBMSDbghl4X
mRXhPf01awEQE1m4bGkjXFwP9YkuXPBLC0w+4NA4rya8FoDQgT9FyfYKPirGQSmq3wm07qMS5Kpv
2PYos9/PNWdmid8bJzU9kkm9SKMOLx0lz8P0Sai+IhPOJ7h7rozFV4r+VseUBvDomsIzaypNzOJT
0nMBaO58owLq9+a87NJmZIeO+KHDM4zR5P2NvYTbItFDwB6GDs8e/HhRWoSVAMzIlLYPEjnEAces
+7mtmn6ghxfWpkUfaVQQtDqQWZ0/GeYMNvr+iawPuW/RLBD9zabhJl1MVw2qZAjedOuu2gwW5FKg
33iedaZiEZsWunsJTgU6NOjJdI9+iKu0mt+3eTd0VbuYadbrs6VtuLhkN9WkYOwWl7on6N59s+8c
Ko6mYrePyZG62b03/3q+5RJogX6Gv6OAK2URGeys45v6Rm85/cvwPU1poK2SVwvLBa4nu6zJXTqq
ptYUvfLCJmVtnxRvUr36X4S+yyPkqbb/Gojs2i86NSe9gYHKmHMb8mxkO+lRkrd2M2m9G6gw8Ck0
Ba0o8FZRdLeTsKegtzYr0EBso3RTLhzvQFgsXfkA5hNq5oeMor4yDwFsvDPK2Lm2Md1JpiUEjZll
zY9d01iVxqnZZNtQw3rge9sHSHEvIyzrNNTIjF37TnbXH3lijqJQ1o4Dn21Vg1fdSwIn7ZGn+W8N
knk2dhK/qncLoKseOVgwmtlNN8KCDP00TPb/bGSuspxs0Qjrmh3eoi0B78ZdIfB4vvTxriSPM1g5
dnGyZ+E6iJQ73jXYeEUJR4lCXUv6jJPQgMt+Yq0XvXAEsh6YoCRWSWPoP/JkMHehn5rMhXwPtj/o
2RHXLp+bYIBUrnK2i/o2f61/gB6N8FSZhBxZ6uUxBy2oTvknxAuqVKPwGdWIuhjo1Dz3M23wJx+0
1Xj5AD2NRSe2c0JGcSgi80R6kPNvCFyQltTKcKEZmPZokIxoF2IMS1cIm2oOu9/Ih4IEFLTiECgu
8Hd9Bk5x+24VixszkQfXCOD5pp/1LdqZGbR1wmjKWvFAJ6ReQAXoygFhP+gC9Z4tcC2OGltnATLp
DW/BxF03B5gbBvfGHHFcJMl3qYnM2oqct2SkV/Q+F+oTOGSaKLq0tFOOFPobAEsesB9uP9noCbDn
YnOdvDhC7x5BY3Se3SQQUE82mWRgFuYhv9lMztBMPca9eMnPBIeEC2aFzp2cCJP+un7reo3iQ1+9
2F7VUrxSezjEoQlS13pUn5TEFBcmAmAT+k/2gAidSgBqqzdE8xI+FV4HkBlUaFbw6VlABvTKAe5n
cnvnE1cHHlDc/YqfFcXANK/blBolfYXu4pePOgdK5MRFWCHz8Wmb2HoUGCtdrMZEHyDktgdWPx0j
9wdhZrtvWHj0s4Uh8DyrpX+BI0KU4qm2HO2Jh1Lk/SgqjT97H1piK+kya4z2NBXRTjd7gbmutaU0
yZiXWAkCj3XKdh2ejmAFo4r9mfBr4FL4ezD2ruK/VSxj8jNM3GYoLeqQ+HrOZ8fi2o8sil8ARMYF
NZRn20KemIadH9FD0VdPLD71SZXtw9fXRF28Rhwrf25oS3Ro2PCCfDAB9Bczxoo6X/gj1iP16F6G
jSG/tmX2xoX1Ydddro4ruMIr9m3VzEQevRizZ5kuQD8JRpp3yrTtitOTCHbobW0Wsn3JEq0xHrak
KDXe+x6k45trdIafgJkl8s5531w4SxdTYmdR+4UGoGWru4wumz3EFOqWdkYNW1Jly4crNoyPTZZr
Lzn3olpb8Q9l6n6ZbRj9VJfxFyv/vIxhwVMVTpZ1p2S/IiqhFgArmxNphRpgOICEbDaWizD+I1vP
eUEzQIds9nMDM2Gth+7I023uaMBctS1+krxyc4A4uLUs4yGcx32VGZTbwIFsDXNHPWcwWuy2TjGG
Nm7FAH7+7ms4978SYXOViD8dpHGKR9Ql1DqZdCbYhA6zWYlfJEWdYKKZh+I35zNh12UrmpaeA+sD
eJswRRQz9MXa1TljG8nHvlL8jC88sTVB/JTywAtl+z8ydvhbLdZi6G/Stf5XZff7FOxdccnc12I6
2knaajaR8wOEg0lfhjavfTxreIj+lafGrtHbeJwwu9Xuc0TuQN5CPl97owoKK5RlbFrdXJSxyGA7
3XGcqNe7VvwKnUbzZTzSv0vVaUQ1MAaaxnl6UDdgS7KljJVjagAv2sTJQXQWYw4pshmxbfeDyIp7
L46KeZSnStAZGaA93qXI3DU2Z7oe6ZcBr0/b3S7+eFBo7kYrPbNuQvHPPMX83dDlVIkTO/ymp8I4
Rr0bAqvDTQd/w64C3iQN5WPx5BSwGJ4ypK0rg6VVP6/hkj3Xw7yCKEvCKCkyCZ7Le5wPCGQA+sA8
fDvRMz7mkrKFL8TDw6cJzNoPL9jUSpvdhdSYNFFbcN7h2PAMbfKr6ymDNpF7kTWAFlM5jCFl2DoP
C0N3a8HmYS3ey7Hefp6INv2usnyXhUovqLeEULNLgqIvxsWZpPSGiLKVIJvtxHA/N3+aL1Ala92m
IG8+G6nsq6awtxRXCHsHKtXTqisovjvu5dc2FToqj9Pio7DV2/Hbnqj6XZr8Nv4gavO07jyoPjMu
LeoMwjlroK/xXf8EBgJmwIkTcJTht3uTxLf78ywhba6d625SIQ95KligHpOH8l+AoMwz6rs3RkZg
E7T4dcTlOBMDvgwLSwuIFjARj+KpcSl/UByNnr92472DMAKG4SIKMEeqxGu+p+SIe7zCz6ysWTvi
scsqDeGd4TSlNydXwXHprocB6NL8gn01RZSlMErRXbFcXKT4xsVAYKqftG2uSP/RyTUj5d6EmFh2
BKnwr+We6rBR+uqixtiPbvkgPS0JMuPRll+sHfMYP53VVLf7oG5stKpaudiKNW8PNBP2Dnwv4O0R
qn1vfznz0fm8Sq6VTjN6kCRpEDmCr7Uk719duYspNywFsnfnpL3iVqPrj1DyMWHRTt4ijYeOe/o8
9ZsMPCJF72T38jSfIBs5s1TrFPeGQ17Q+594/iL3Xvw5L6hjGUTvATOWmVl5z7kOisZtnxTD62dM
kwrXH8RMbnQXwLCxtTIGvSadaRCzH5y1C5eaX0gEvFx1r6WNoiHnyBqXJM6i5lkJC6FvviFd+8Av
6xtPXt9fPprGdG3ATMsDooB0Eq+kcm512bZK8vCBTrjBoIJbMebWMT+FPC+LzvLVzA3IYJf1W1xF
GHHNrXQqa2++XhJZ8A4PVtuJKjsZJsnQrQHPgJjqXLv0dgQf3FcnQacblBCPPWpk3vmJe+eTDU7x
0O0IlHyDigdyzJEZz3iRTclJhXKqBvi9e1aIgxNUEC4JXG5A5Q6JzRBckfL2l3sj24RxPADf/pVy
Vf1M4nJ57C2nMU8BUmdqQ41rUQtcDnlJryY2mtZRI4pKpyM98NHM5hkhyFdfY0s9P2HM+cL7fAe2
3q8i72L0hB0S2EpFVhdR5Svk0ms2Kvze7qFacnSP9lnNy3dDijFNaOLTqiTICXbBkCXoYkRFXqGx
F4DgDEcQ6AdJsV/131mwJIsOzv4lQTHPAHR6cUkq3AArhNo2p6KzzWHKqH65b0rBnJoD1IkVo2dn
sduAG7aeVkU8PxqDihUrDFerXiaFL2oFbRJ13dne6qQJ4vkJHjD5IsWgJL/qpOGZ95pGdBOcJxdB
95FBEr3FdC4pcCaXDKQVKrZuLf5OqzLKpDPypotjOyh4HSYv7IPGL79b40gqVHBhtarUezGujHJB
I/mCgFCgqgvMJiFtfUaXH9cLgm39jPUiQ1ZROrH/v1imh/fGuYlTbWiovocvPgiTsVliZGUZzDym
nQBE61d5nN3Y1NRzcvP1jj0xwhyqfkRLOUnvibud/exe2keDVbC4Sg/OZRFXIAi1ReCbcOl3uaUu
y5l0FQ6zR9Kl/HIc2MNA7FjWVsVlncGUopC4/u5PHV12PLL98OKollNLHDbQ5ogxMwjC29g3+9zu
AYV6uXroqp/e7aiK6CEi9b4LjbsQQ9e6lmDoWZP5e4SGC2QveKCsAGXdMCV1VUYNigkkNGZa/dH3
VyDS5YysTt5inXwajPze0F1R/WkEXRMjAlE9izim1iyO/XEz0guXn4bejeJYIG9oCSNXS8rIhb/u
uXlWAB6Oj+U2a5B0U+sWq/FkgmoKvhvscsGsCZ5DRWYtsgFG193GQn3pz9c4S6VbYNgVmPwLnn3m
L7pbM73I4TCDcxvhjGEhj23IO6f+VcoZqERniC9qIAfF1ygkv9gorvBzcyN2xHFoIk4/GQyajla7
4TyX6vn8sPaSiRaoi07m69zhOfaGo/nI5MrU1snnXS+oXx9O1oJeZk7QDZY+oS88ir1J+KD0OP94
zoyj4yfK8Ll4MlTBv5to032U6rWdDHIWfPQbLZzaLL/k+n+zDX1wP53ey9/Ke+cUdesuHmxTx5no
lZFU573aEb1kxj8z3/F+dg//nZbh6PRtp7fpV647x2OoyVZfaPEysvnvDV9poQGdfIXVhsz3iDOL
8bJVcnvBsEOrYZ4lryeD4c7vYU+S+cg+TINtfMdfJoMQ/vFpxblBxFnp7rQ9ndtH6yokFgnjxAcN
ua7vcULfnoFz1gC0hhvTsvvPqP14GuxSnXU0P+R068LeA0eL3zczl1ie8TUt4oznYxJhe/vLJK71
W2/qiisHawcwAEh7suQc1zZ7CyFhGgtOdBAyEkX1VU5XKmfOvjnzc/xXHfL/1ra0k2RK72zK/7ID
61YlzDWJAVALkxaXPkreuNAxTWiKbyQRFuC4s7KaLUJ6LJZAyX3CFEh7jkTd/9Itduv+fU4yYLtj
NVMx/Ilc9dxD0hi7tylCR6LoZyc7ygNN6MpIoP7Px0jr+nWgPQrQhiV4Dm70yeIDjYPhW6Si3nlt
44QSeJM4mb4qlhieH/EftwFDstHK/c4y/M38qO9X4cafKiXLZ9f5/2It77Ri+XCrY37QpM8xkKDu
cjACTC7XAGWGLtStkNCYzs9Rgc65rJmqwjBbNq7i6do14aPRogPSBoiLbXCGNcUhauUw00R/qmVP
ByEFTDa0+pJfLZ68u35dFFPaoeVRpan84/6jVvD6PWeh0XRY2djSYQIya7Kt4OviKZkgLTPgzP0f
WJbY+cPcshCz8Kpwcrymy36E0B5CAYuZlqzx2grIWsFmDH8ByAV9+8rPqsEJmOQS8pcGPdzxWjND
EfdSj3KSGarFwstpayAzyt5UvUUfwPBFh4joSPsaBjOcps4mPkACfgjXqWTpbIqOtFyVZNfi7mVn
FzgaLzeukHAT3j8/FECqT694dy81mO1oUsFKffvQI9+caeNy//XzXBl9y6+CQXRjruL3HBPc9IEw
0BEVX+2NqCXrKeDylQ2a1zc3VJQ9RbPX8aS5EH4iWso5zK5e04B7FyYHqLUeAiEpWz6tpNEBj7vn
qoW5w7f71wKS/GThKYm49Y47j3A+7hhl7aFc/JKOtZpc0D350QWeD7WMs/6qGxtqNXt2hhaBkVAc
h6ef3Fnqf1a/A008Kcp2NR5KHrJwROUusFzUblFA8WvE8+gFR7A7hM3MkwiUKNhJrk7S8KPpFLTZ
Uw3mVGr+MfJsPPMPaI1A0RvNxf4g3ZlXTHPkR5lMIYG4Q3C5cO8Wk9HF70NmmfA8bQzxOY5bnMgU
yBKGPp2MZKJW2ez/MEHuZF5nRVSHTZYEDvUxVVl4dpcYwaoLt0SxIt7mVJIQQ4O+U3837CZuIOlo
vISVDk2Uw+QnXbJYdHOaNN7PQS/5nbH+VjiHL7I9O1wY1O181P/9eJF9wUqJ0oqvYcbFOU3/5G1R
LA85ne+hrhNtguoLRwukAvMe1VYwgOYSwIMvewGZOejUtZboth37gWAMklt01+uJ4HVQVeWCcIUf
EIlJ0sed468bJW/bUzhpwb6d5ViEiacNjRiccvuYI0FYQaM4TXG3h1xBDMMuApW1xM2Xs5g1loTA
mMQxgxBwOm+jbyP8bjrIRsctGgUiB4u0vtQ4QNqpd3fdW8z7aiAILtPgBVtE4x399DwOeuKWTIx6
A4EXmNPYNgGmNCseq5W7XLHh6iqWQD9sJjHlgLdWrfoQtpt7PC1pwM5eb9OIXUb82Of39Wxhci0b
Cy9XJ5fpI+l+yxnvI2byY8xV0zxt71fsWlsQ7Ye0Vqk4WM+xdvi08i1lgimB78+r2rW6taGRL0p5
t1P7uZvmMow8l0lTBvYj18K4KtjzzZ4KdVwasQjIRtI7TvbyzBhYAmBfvEUysIxWD2sD5Qqahry3
7rANXh2HG+DrmLIkAwRKeTR8hao2qqO5jmBQrOSrkDXieM80dhUAyFFLsrxh4eU4ZfeV40Z09Xi7
paGh0WqUb8P2XMAVDRPDVUsv6dSzizUkKnOYMhkhYoU+vCcAJ1dQKzAxyqWi65ZcReN8xCc8/UMY
5+jAlLeezuO/VLAvrMQmCfCQQwGZTTkzkMWQZZfLOjZm/RkJvTV3iTtEseV8e+r5KntytW5Hkfff
I7uWvWjtS/i5xDQGs3vVIxsbky7EJSA3o0CyB52AzBk1UOSTDByiX+4g/EP5nACcIxKxS/yD7+ze
AOaOWO51G2K3qcDEZIdiu20BMaDYTQct08tSIB+CR/DNR3ItgcK4x4b2FqC0gCHyGY7RUxhFcatn
2FCVlYH8OVIaCd71BO9Ql4UqlgOERYfAmriedMXb+vsdmwazEDgvBNLqHUwmo+ygf1HB2uc0IXSX
VPDGEw0s8gjg7oqEAAXRiASP3eEq0dAvmvM0qGkjz6/sY39vZ0ixECgn4TcpeUucP146+QcrdsJ9
MZXj+xa6OibijS7UnLJyeacTHQglUUfEND30YOCXfkG2ZbPZuq+WegEghqx9Lz/BslIuF29ntyaG
7LIF3CeD0eKhYJqZdcNE9ok9qHt7qkEcMd+zY0vO5Q1J5EccKMm9el/vBsofN5FWhdylV4Cv4OjZ
PK79OUQ1fR3Q93regtP5Pn2P/JLi81eyalq5PuwQ6ZIZGcsZqlgGJW39PIHqr93MgEckxWGxjZ09
CLouzeExFush0H8CY0QiHCniVZHpHdNW1ofd2Fz/+kzexkQLuteaQJiHKH8L4V9vYzLPdMF7A6xY
aVOqob8vgXfK4eDHhKfLepuoC+XTqGju9CqV1SU0lXrUat9E0VpQMmjXD2zYxjWaOptPeQCt6PgQ
09yvB4c6v1xHFIZou92Ui5b0jrvWCJne+qnrZCoCG/AlU7cYnIZ0ZoVdwBR3lLLpt722XowujWDs
38FNWVI/jFT9kNoJQEq7YO0BAjb8dtz3O8Py8fhtTGLiF5WSr1YhybQ6bnYDFCj0db9bJuIhEVBb
FkKjRL49ofM5PgYuuJhOodpPSdVKzSMIywpYYvhqtB2MGm3ekukrW0NPIxeZ/yg/wuRvy7pZVkDL
cQiau4EjnM43dqRjaBcmsMfWbyzeuw1iVIRCxkDWAj4/fc4U2cg++hbAlFIT9fH0wWmSTAv40A38
pbQ+EfvUTFLj7G5q+fK+n3JqtQC/CPsOZOrfHqXncIja2AUQGc2LYD0o4Y+oa5DAwGg12Shn3ip7
iq/vil+RDTlElZihGdTIk7OyjQCjn2dj+H73rAm6AflvzXqU4j3hCtdqHpL8Q3UBi6uJe79CLer6
3mIWGddjPpu8kv+yOq5ydcBpWz+zUOO4p3lxdGD0qPSJblTuKjbL66MIlnKjtiUW4u8yZ6I6el2m
qzlUzV/v7B5H+rD7OyDydGiVf5ZHMcba4NJXPuBzoj7xDC3femvGmip+jTRvNiIjUX7WonaJuLPP
me6Pc0UIu7uO+wc30Yqm6/eWUoBui2LMgAZrN6zrEGKNBrJ2KUTl35ecSE0pAqYy10aUO63o+zuL
KCzHM8p8TiH4DbjmotpgRVjlmWM1gmKC0GM5xmh6o0Gvvab99s2BUhb3WzKnCIA9rMVbKOh50gJJ
0+2KiICdCMUJP4LoEsHjJl+OxYfydUidVVlqva4ECa+/5/2AXCKiic//qcdGJbFM6TX9mL3+Y6sG
mGuNTjP1AUSA1+iY6Wc4XVMr5ZBS9Y14md/UzgME92KdTjvOyZUY145IWtXMPrIrPZvmLu+mHuQB
gNz1u6lr7mMc+YKr+VdVikbV+VFwizQsmD+n85+gF35okOkhTNUWB5q6zsFzeH4W4RsoffrB43l3
z2/GuscA5Xir5rB+gTOAKrNRgCj5yRv+JAaD/MW5MyrLJCom2l6J4eEVGoR5qNOXKFla39SjPvyQ
ds2rpCUOmYZEzqz5juHXxO8AmkhdzuI25gIFpOcW8QP+Tc6otelUCdlICACweMUJwiyy70hdwkP3
KV2YxTPO0DOwnT/QJN9ZIMJ26Z2+mHzCPZELinW3dbGHq5UTHjV7xrUiZWcoC5PJBAzmR9fV4PjL
f7HtpnwDnFJ0X5JOu70Y6zmEVjZgOJyfiEmFF9vpjHewj25jXNZTaDjdPwC8y/1dFd/lanFpwB/J
1fy/IDivVBhUAo+s+Xz2A67+bAlT4tPEWKHBg2b974w8HT9CgdaEH7jq2YAL+pV2ezsra+H7Gogd
xEOdFFaeSADIpyYTEZhF6MFvy21i92oxuP8wabFUpwnAtAju5Z7NKV2tdHBHwjysS4MZhWNzl/s+
uiPdLGMFiKQ3OLOD9NLYdjNOVyBqFK29Y2yPDNngRrW21DAJ84/YSDJ/cf4o5izGzsIg8tWOmAVL
UCJyWr0fevanZwOF1Rw/ol1+KZe+Zfe8W4qFOGm8IcpO8sVq3g2HtxlS3mtclOYYMwfLmVIRlrG2
D2NqBm0ioMobzswX+USEosvfrgxYOcg1vZBEw5ShMhG8ZCd/D2CV3iAhcWe8VkeRbKU43Bmx/xR+
QxrKrwaiA37iVUZh3nS/DVi8kWMu8WW29RNAsP3eP+TFYkrphevrPFo3BtH6XkYym03m+cxExdkh
Hpe4fgCHxymzFp4567xFFsGS4pCbahs10RzXaFIHIYmLlwd702x6Dy/EkRGkVx2ca1Jbdrfkyg1O
77w9yHqAQE5hS9njI+unUxvUGnPOCeOKFjOw3UGA1dr1ARfcFm5A8R5xYqtNKNeCQmtvL61Cg/0d
+KS1k954+37aWDPERn2DKaDpYkafPHctaNb+6E+8UT2r4xu3nrkTx211UmaNTbg9DJVlZC1sfjrQ
+rW61ay0ALFbSzC/d5zJ+uQORZT2jHTjpcay2JBZkNl9VMLKwIQpzdZFWKIHK8hEZzhPzeQj7t0i
PdVYJnXzg2jiMPrPt9YeZwAnfW/okLDCuTqzcueqhgCT/dIIIjTKuqP2LOe1wropTkrmH16fXtVZ
bmmmE7vxFrWfulf6dqEJj+kPe1s3XT0ZaF0hco/SLUvaAEEFGhyKWPqY9NB3hsMiVSo50V3TbZQj
DQ8kWEawZ8lzMM3kMFatbg0SPyRe/ynxLDi5fvPul8c8UqON7Qv8iqj71QQKFGzlq8KzY8gU3hcm
h1Q7b0EnCD44olUx63O0VFhQZQfCbxzBUjsMnkMBPerAtSNRJMkCK3zrTFnEGumB+pHrMNaQxSF8
lz2K2AYW1UyJuNeOzDZVqN2gJmUtIDdnH8SNQUsj/I74s5kn+0bBYY2aLvm24lSTY8f/W2s2Ssyf
LEdXkPFqNqvu5OzYW+ZWBDA8ebumztbk1yMh+3ODgwHavVQ/QVBDPNPKeHdhXm4q3ZyuCJVJ2YCL
Tqf7tbqMdPIzTQDESd0hi+DKT6oq4ClNLRWGZFqX6hx9Ojp2I+1jBor1tazv5iHHJmiEJFBFUKNM
/Hryh+w6qxN4S4qY/HKUtOUXYFdRLDtIupovE0Pdc/cVv3uWOBJGy2zjoIBu7wJoLkC60GYYX+hc
yT3EKVTdOSbJPPch7aVQNX7s55RjZj9BN6keTiJEzzZnHFr3pA5h+Q5jrfWkfjEVH+X/CRa4g7tR
Zs3+OLbTKIq92SEc2Asx71km+ZG+m6vaiTlmGdljs/BU8IjfJgxm1WoftxooMcu22scirCjei05d
70QK3nOp/lxznypZL3acDANUq9wl4ktexN1j2PKcW8Bl/kCAgcC21hwto9EjI+ckg2OEGeaeJsuL
LgJaAXK8IYfkhw7fGySzq6Lqi9d25nLppTfmQ9NylDAyGaOhz4SxsIIQP6pmHPDwAO9QZvKXVDTD
91maw6eSa/PbDFcVcLJeTD+x3pjTfhIdJ7B6yB9zsn7ir+V/fKr+ej9wrj6xDD4/7MBReGaixiPM
mmI0pR6CcCGnQo+EOnPb4x+cL4qP7/iPzciPyGyCMStkDUJhgXZ0hRKoxyZlLOKSVIw+1hkJAjFq
cl5XeAlHlbejWmAJITnCeN7AmRxWSTa08PDZAKffYuSW92dlmOaxOP3TWIPnYS1LgXG83Ghfi/qp
GXVIDvadOG/g1HoOgiVpKxU3DgAccW8bxYjjRPnh5AD6PDNO9tInSYWumwTTutHWMQGuTtxqwzJ9
H8lKmvWhX14IgGrCGHYrhrEWEbKG23G7RaLzg1C2yFAOzOyxVBFDkGywXGUw9DxzvO25RxmdV1Xp
VZdpVN1Xrq7JfMhnuZy8XoHtsNZuVC0AXjh8uBwsz7bC74RhE3P1LHgwDLLPa/2FYkcp1khwfMmC
yhf6QbNXRxdcZmwJXNxvhdIxjEOrUvM7JzCjr0+NjlBmU+wpzWZ25l4wG1H42lgXBxpOyohdn8RF
OzGdmnbLy9WjsZ1EzUYFTbch34xEYNp+CUsg8yKoUF/L6LmCR/G1BuXppTA2fIT8whQhBjBByhhf
LFe9Kz/HEX5nJ9ZQnD9D+kZTTPXNJlky8gi/BZL6oDdYCPVIdM/sI+2kvlksDXN1BhWKMCHRi4Oq
N324uY2r8ZPU60JfThJ2k0ZUYorhnwLs6iOSzwy9UcdDKEXbmpVok4qAXHB7Vub+Z/NmezXqqiJj
znHfT/l7BsNRS6bCa9D7yxKclVwoExdVbseKIhbuC7S99StkSkelm7UraJrJBRwGZmbeOpkI1f7w
IsboUZEugwdONDanSIysvSxYLx4zRv/bUhOLtRKrLhLmzHBjVJB2KqAruR3S/7OwS2nPTami12KI
cVcQNSxdbu8zjqoIIqsPbdJ7r4i3G0p7K5MH7/APWN+H++leJhnek6Wh+DuF6q3alCA2/tPYXB0I
n3OK8IKlkUK0nXWSqMQSxORTFdYPvJKmRhlE2XjBJrHtBRpf7X+V4VXRojwzHKUVx03bmAkXyaZI
rdK3KFi43CcvzA1jSW4J7NZ+Ui5xGYucMt73vQTcPhCvKlKZueMydRvpGfyvoZMc+oSOWEbPehBO
3NN5m1YSaXmhFx8eUc+6bfIGazDD9GvJRdpVCcZ24vEk46FYlrhzeZMe+ozuvaoBPH3fcnGG/8TH
eZmCrJHLTauwCTr+lbqwg9sxgCRgUlwCotfMwosJ48NyWGG1QIvllICQ45mIv6s3xSgAFJKeldeQ
LgdWkaJOP4uxoScK1y1cyJuJuexnY3XY0uDE+qVuvZT7Icie29yYnANCMy0/wgCn5s7/RPmOfBld
BwIC3rA116XjDdsNoLaYBbf8j+cPDhmUKTqk5VzZCuJ6BuIw49ZShdTM6N8eI2gOaiEd9AFXp1qV
CfUtLhKPYBM7Sie3AvkCCMRPH9ixY09st1voT3OvGlvRx9qT0kpFemoYaBHONMZZ6pMq/ys4XBN1
ls+J9a6ggGj2T6ildXVBHyds7VqS2zdqVrOTiae7gTI/XbtUmp0mPouVQS2GveAgt6ZZPd7cRkkM
hy3ESSprqmv66B6FPm/ZxPKjDgA8uws1X3wzOpiU3P2CegLchmsgl34/p/bJ6eoZV1IijK2eM7fA
AzaBiBQQF9Hhh+1tDeoG8ZqEba90rwX5imUohawgvpi5ncF6CikeiWavR87yX0IYhhZrida7wCMf
3yvbVmVbV1uu1Q+7x0fasIEvpMBLh91T7NFAp8rko8uDZJzThYCL98qWU1dqxDLlKx4QhHXIhaW/
eVHO4bK48Onbz8g7c3HPGjqqzxteS6s4NtxMJA5grDTDGOsLDW3A8BuMMWIBnUSKft0cNUeuLAW8
Ftn+nXqaqeN7Ie3iCHbdgQaTczKbRONmS6u7LcspMpcN+HOU4XuSKp2pBXMVw6NRSP/mQNIZ61dm
v4UwLCZwSFJpe6uDPEuoFVRmoL7kKOoN8bC9Cmfs+6qU4hN42oVnqn3vi6rmyyKeTL+HvVnX0A1D
v0gTVHMKf8t5s6cvoyJvZg3bp3HjE7VAP+qZ3mPGO2gI9aBDQn1RykCtK782gaA2/HAzDdadMO/b
BSaO03TFWn9Qf3kgusBLql7inWsdHTQK1prUpBltlf7hSTENta56O6EgThPCpFKrQVH9nuUREXXH
ZeWfvCEJ6QJbGTVO26j8nPlkjwI7Peie524/ReQLozL//RSsev8+OkpeAH7xHsvx8V7SanILBsb0
+iISezNcW8QNiHcpRHruUc719177wgT9J2hzA17J+xd3S33Ze6RpvSRay9ybgSjhVzlo2r92xbvI
8xwGp1mEGw9lMGLKTXUFc0KQNHI6/aKRWb8+nGdoSQD6MR3n/HJBYNuh/S3sOWwFQ3bCYrqEzi8t
6+/DKMYBvz/q5lP0M3MgGReL0TBPOmNQB6cZbIlvDJ/JWjbXtmpvyu0xgWJJCN0FURDBXEO22hS7
Q0TsZw3qLNlQ5+q9UDGKbsNVbr97qh/0e9A/C/6YtJX84Lc5/ELDIBiqG8arAuLjwJaU/IDgxn53
LUDThW9A93RaH3+XIPYRJblQJJzxThNdtt8HBcGRnX5anp645XvFqb/t7E5grHiIm3tyCgDQf34N
uRLUlPDuQSD8E0dVz+5AO8gm3InnBiBOmf1i7JNlkWfpsUoQvDMn22nGTJ59MkHmU3VxF9YhUV+V
2yKezAMv4PSytvoYC3gb2unRiDShdTXPB1MPyBghyeTC/DspWItzPVitvnqccvtme4sMk8x7eqSF
r2PSGB0SqoBsmi5ELvrNl8qxkdrL8/owxX9M/q7EoaydHl9N9Eu2tlxq0L2eHzSrr3dNg/9+4iVF
+/+9TcD6fkvnH0jTnfrr7Xd5lgl1oXSj5mCzB5qhzFJbtY0z5pRCayMcAjRrvA1EoRN6LP1Lhnxy
2H3dq4aZ6zjYsJcXKd0uVHbDYhwp1Ss4PrLY3Mx+iSAyUvlwJzFFdZlpJluwE5qRvEBgwNqrNsgb
ofyCB+Ty10Z/xwzaHVetr29r3I1k5wBue7MgGwIR4SZzmbrtGzi6knjA29xYcvg0C9BrbmM5RFWx
AThUcosOVJAd8+HadL7hDDnxwjqNuPoJUPDE79cXsatDHxr4riSB22jrqdl141O5UIQMiNZjBSLk
og7X1xgvM/UbZjBcuLpIsYNEk3ZE64vdJ6DxyEi6jiGImK5mmst7CwjJkfgE0mu5W8BBwA2u+nt/
Ck2SxyMPu/kCL+4L9i8gdY3vn/+xS4YMp4G20AWa9Phsn18OpVQNw4RmOxLhqqyWpME3GTy00V9C
x2NuOm43D7WXO4nY9OR+hqUa5mVPELkDRIgVPPOrNllCUZ216tzJcmoAf08TVb3KfVjzclPuNFSf
yWjFEKTsfqaU1C2ty1H2XjG/kOsoFU4SMKNHb6G1fXZ66R786rXCeo0ly6TysYb1zqdCBAs09SE7
a7RINzKeTsBJvLmbYZala86fDyJCBl38nulKY4ctVYqronW4Mk2YlQRPzTL7BO0y1yuhpJZ6hkZw
DC2NVeZiBlP8+VSNknii7XFmsOMd2bs5GT+s8uwuR7qvYLjhKuE3q1lpu4wCdsoIFG21BRnrmYTp
P5IxggLe2xQlzphTUg7OilcpecLqqTu41VJsly+4XIIh+Fopk+TQcJz/Vk0w8egrA8cUzfhg9Ini
NaxgjhmQlIqWZ+6WvTBWKGNoLB2EphNN9cYg8mWjY2cWvVItybisM8YD6vXCi6hGo3Awk5Vd2qpI
+hi162qUjSY6B3n82Mj9JK3RRgjpASzg9M1TQAyay9mD37xlRgtEubr3iBbgl7m/aLw6gG22gFLO
rZ/uD1Me5AhtaThXuDRTLxq7rqg8NnxS2ut/Fm8Q0wAKUYU58XW73bcNXNj9KWEODYUnErb4w3af
dViD/RyLC6BbB+25h6x/GrSCmuafB6jDRpd5XDdZHmJ7FXJVaTTROUgpAljatd/JehD3Q2Sj6bOR
kQGZ+2D//g5YmvTUJDvdoHgA7aTdCqeAlMuyHLFf6Iop3oFg9qoUwSsSq4m8zyBNQKfibkR42yoP
hifagRs40XnZpmt9dffRfvQDZWO4Ar/Bb0RAf5N0CSmF3j/OL8h6f7pt3aFvmMGnmCNvdJbau342
mA6MnXrwXh48rBXkgiVg4TLZ7W/bw2Y8rlFEqYxG5pwwc3zh/OlrBmSSoQRI3RMo1QbvraltxZ+G
nGjMyRBlbam3nZDLEcNnSluidLTRldsih4dpTDBNLVJhZ412OisCgZocHeoAjHjWAHJ1p3thjbzH
DuvnfgO8edk930rLjyEHz2m9Z4G0tTGYHxE1GGrm4oVpFS2dhtALksSAZ0RHmch4r6ZppgSOmBqU
9y/UrKH0PeyI2vwWQQRnWjDcpH++IEpsSJzExj+o+YJTpitjFJL41BL5ABb0T9sKq//0u+z7ctLQ
ZM1U4+OSUHxh4gvyV/YQ7lV2SHQxDJcGieMqORgT/+sCPFwbPSt/hNYycayotiN4BpxUOX7pV9IK
tFI9oYA/zAs4nAPAbovtU5x6+FKO5d7nbNLqySI6q1Ak0lZBuw4K5oxI68uWmZsCE2sEvBBnzzbO
K9E+dGNwqxP1BBj8+iAZEVgk8OtY66/n/gGbHCqSunnXTVvcnGDPDEkccZgeDxSp3ecIwhc6qXhF
wlKARXxinG3RlWgOkap6lOmMrRMXThI3qsVXjKgsEayG+rThBEi0fV0B1EaeSE2yOc2CQiTuaMgk
PZfpXuyDCInYeg6g+pRfxZ9zgBQ2TNByfZjPHhsgknSfVEPQ9zoDAQ+3OJkkOfMl//ZRge3KEWes
2QK+pajXebcb8MZHwg8+JQHmtxnr6aZKXJTxNq33IwBff1mjYnWpUkpUAslq3bi/l3AQjdQXxUBY
AsYWErmdVfacQXY9NzEpe4vk1CYOY6gXG51uzDZHyXBhHLZGAcy/9zXNzeYBOJNInuN65cKvIAV/
Ox2BG8gfioyt/QrLs9CclEwNrI5OKC8b5zg+LPTWv+nKHqoItar8ceXNYcXB1WaW3mB+DjmvF6g3
n3nRIsEDc48AHPd/dyEa+10/01G89CZx/xWsYP4Fm3A3fx7Dgl/m0ZVQszCEO9gJvwl9650cOCkZ
I8CuBSQsPzRDlUsTfeNiE5wMdp1g61Bq7JoKCeEYXNScu8BM5C6MfL3XHdkVeZRg5ZhgqYe/I5aO
cNnR0fJURwgljAavmm/AYMGm8c+wOTahfsfkAPIYsaBT6B47bIF2IqqeJhsgwndbdvwBmH3TkT+z
mW8QUpKK5ieQ0OIIiXottptGFnbcGG/oGfvt7A6SEXkK7oX35Ds8VfM0uIIxxDa2iTCXwoIquLQ1
qKkkW/trjaXT3WQBr2SG9F0EYl6/KyVHyVYkGuYKyXrt0TErvYOHxkURaWr//MNPFSYG5Im3B6aq
TVMUhgxHvxZ4su8z0VPWCriysgUuzMBKCMsFcns9QHT8j84s2Pkz5z0n8tzKmkGMBU6DXjnSjdSk
6gK9ZvYO+O8zmChpupx0J+nRurvmd5SypcPZIrrSo8Bd/ivj5zr15Yrjbziq0zMHJ3SZuWOZzKhj
3ZLfBItMl4V0KzaDRfw5bF/YzdEqTQeaRH14Eafkz/98fYzFobcVZg/FUIVP4h4rEvloIRSAErys
s6Xg9tkI8SdAzpXdRIW1L+e6CR3KFJuNFnYjawhj3fsPLlu7N84wlCX+3sPSLdOrGsLhtzjES6mS
O7X0jO7i0AoBmRkeKxWn04gVeae2uHlUOH4mtW225v/i/kl9xhifkTCA8KqFkETgu5PZs1Ijgl1O
AlyBQAtdFUOSyz8Ua4r79uv/9OK1kJhR5mseWLBurhhLxC1XTCRnay1HyK2ijgXs9SswYvru79ip
/DxqEHpWkNUoey/xD0B49Bv93vAvfqo8TbcNyQE9txr7IvJD5jdCR0ig3q+BzF5pDpxmJpzCtNyl
cTaV3r7YKShkg2g+vx15tPZ0QRFHAFbVRbZEkRQEhmF0KdZUNQEBQctLzZjlqJLYzt4pu2UGe90z
2K+H3o/Fy6gG3GEnR8SaFpwX55L4Od8IVrbkqpmrIRR9M+sEh0Fda123tP0XJ+GKlaZWTQzXW6c7
Y4LUCKQvN+x4LiG8XTB/J8y5NVAX8nXbJRA3GRWo8/RVO9k8rXiPkSynDh4wIGquDqwiYyRueJzo
tMSAcJd1/y2wXFfd3gSqnmrkttFSpWho6OFQY0XFuvM62Mq4x02ANCNs+Eg0XvyRT4y+3vvYpfIO
2BFINRMUxTGQnVkYWjIZyVV/Yx/BS594lSGk7uIB7sEgGkDtkZbYwzNmnDgm9pN5kYFV/35EpxAb
Ufos57UMHIiZjt01WCJemi4xeR9vcX3ZEH+/QY4g/hulUjj0rd9ilIFRMxmcPKovLjczrDxyq35Y
4qUnylPSj6tgI2tFeQf/5alk0ioHrPA7G6v0MmCaXeyhmsL7/YaDkXQIvU7jQjp7R1/sR4mv5dUt
1SeMwMYsisddspNp8vBdll7cQ7Hee2JzFYKDdOc0KLvlR0fk2F4e13aU16NVCSQAdpkzR9lioJxg
R3a5ErJ11DoRQTO6EW09wuZpuIwKsOwZ++fsJ5q7o+7BEDm+dHWHm5YxCMVVp4UGPcmd/mKK/t7y
gtd9a1lKYQYZqP+MgrnZX5nZ4eK4eJGz/QwlQTdRAsP6hKjAzPleuH143NgAW/RrZ3dovS6L2tJv
C8oydxoJ8dYlT2LxfNpIrUcMIKIs2dUtY2kZ6Dmt/4UMgCOUq85XrlF/IxDOfB9ILqDJxIHZy2mL
j/IjD7YAFHKMqb1c9e2sCyOuZxInBsITQHCiysxUD+LW1x5ZOHhYCGgdC3JP389U0Eg0o/32BSbM
KZ0YeuCsd34NrgIwZwkEk7nqviFiWAWhQRWiFY1X+3IvlnWDh4Lw+SzPeyjBlTLtvGLHRFUGNAzI
EmHfo4rS2ut3irNc/z5FI03INL1oL2jGFpSbUifbn75Vm4roWZJox85FYTrIy+0eVsyH/rOjbFHF
rE/uEdigzuHksybc1xfDQpARlVcpH2esxjUPqCexflX/hMC3b+Lj54/yPNLeLGbt7uRVJNwNSL7q
Ha84a4Jj7QAP59gLFct7IC8rNxBoMfWurK/GBuh5AAGSHK8UoxF+/dy53UYGO3n4AdoLauDEDD6c
WOi8rNzqC9x1W39YdQdiTsV2JLIrzfY04BFh8yhXUBKiwlCJnz0AQB324m4o/Y0Kutt1ugtcohFo
8L/8/6kK0sVols0nEoQjakD5Z/vGqR++1AGkI08IBwzZCdJ0flU1d6KLhME2mO9PMMERRJ+ERRlH
xMA/wfF8WO7V84t4ydLVI9j9qwfeXb5ZkAgUzeIBkx0rxJVWIx9bOOhVvhIPfCKIRIvpVa6ZjbAO
/ggYpGWUYDmVuYbWRc5KSafhzYVQ/NALMNrQ3ONWMPnc8FUBwfuVA4VVX2H2Wy3bz84Xn9qRz8JU
rNw+9uW7kk0T4E8K/2FAKb1ioQi73872l6jHGw1ioe9HkN2bvssZPgY2YqwLvRxx9pODdNOhVVDL
NUovXOQpsBVCYukOBnggVBar5bAJzLP2RjceU//hm+4A1+0HADPpeT/PLbKuhJmoi91KI9kJPFcU
8stK/6JaEYN42++9L6PigIIjLIw3jboGRbpDJn6e+I/2qX5tu+D4mTGVsK7hrOz5xcfDInsI/4ME
t3cb1NM5Ptq9tIJxCqNatzq0v0n9w9XIXEI4NbPlIde1DyyQ+ZCKqmtFKLS/PIjoC0BWjpSe1ZcV
Ji1tcZTv7SiDaYTKmE26r0XQprfYm5lqYU5ZDBi7SVlVmfGI/H/BkqEHcqn0Kd9q83TOL7xySdBq
zHglZk+fRiabbjjoh7RpK49wNkfOXu5XZ6/NE+fhJoV4Khf1+/Zgmmqn2/FcyRaxYCX8UOemREZX
W5Q2kPNYiiIC40HRuFxzTSBdLOPA/UaHjEVxLZs3P+gVZM8GLhN+xjaKZFY22luH19bS2Ip362kT
OpzPEh09AowFk0XvKiqjZpszrQY3CZEbeBXZNCn8XenP+hb1nwmwdtCJWv6LxtlqDyPtyFEvneez
ck8YnZrInNbXwGlLMwokpbNWrzaCoyij3w/SOVE3H6GOgSuk113NzOm/WHQSjsP5guMd38vAY9Rw
lMVbanEzwKTFi6j5UgqrYcNQLB1nWJDz7QNlKxf8c2Momp78I7B/BfQHtz7/PjeHB9mRsi+3Jahi
l2KeNrMu4vCILf0P+ySJ2wJ2tnKq6/BsVb1TL8PdjNG2iEf9zTtWSM4udyi0d07xk+qyYuFEuQ10
1QFgxafFetTCUj797oSAkyKc1EDEL+HN3QHJWCub7CrEmpk+bABFe/BdmazAZ/NvQ1b5o+W/zXf4
jk6447VwplubL/18+eN2lX8BjaVQJc+0GtR79mTdJAmn5qjacAejcaAguSKelF3eCUpaZ90dECyI
4IYtNY4yMJkkpETHCzjCcolx1eECIgLCmGoBjHzQ5PgdqvsYSGRmix2wJpFfA6O94Omk3gR+L1XF
Lk/XlaNSozwXtoksEy+VNPihn9kiYyskv6AxMWOR91Sbw032Dkeg6FEagvVupdW59ECjlvYtNXaZ
DkA3LUDuZIjCIqNBbNkSBmdvknm4htPYs5ouR2MvwbMvWa6bojEnObAqNkiEO88mKelzQMfsn602
GylSVAQpAqHffouvkDm4Amw/0uSNZTNgw0KpB4LxJwt7HpbGZ3x6QP/vUcGOrf7XuTS+Hd4atPzC
V2fbP/cPvMsfG3gWTKmxmLLEpLHYPmjWIgD5uUezmhQ4eV5IvJM8rNA70Wu67MVl4FTuIjtlWQhC
JNtO7wWQ7suhprJ9g9z/KcPPTsBjrH1Y69pd8CZL0nMCldpWhmxV/cXReFe2gWnOvkCBVV93Hvs8
xWg7QZpMg9LKFutPMv5wIrWsqstls2qF3i5EtHhd9YTxmVSGVXny5fd58FCQ+KH79obwXXQHsIbe
5+3zzAB7AkwPH0vuKNt40+MmVta51GtM9smvmAt6z+x5MtT9hf/VtHrAXWerIHeiPm7E+f9eaH+K
/xLjymeHcrc2EwmgieKxryDB04B1VzNWxkrOE0MvgxVV4RZGS/gR1jxwXAmbU3v60ApM1VGKT8tg
hBoJDIQnspFWWPIBXvWfVygAqCB1gFvQbQe00chMIkdbelzF1GtRG1fQe8ZcA+4d3ChE3o2jVC+s
GKiRXIZittijuQmdmPmtIUkM6GDWlm0n8bd7zxkgs79PHPSrA3fl9KveO6WzZtJI3fzAl3RH61Gc
izEguB5rF5QJv556ZjsoU6Grp4Qyu6DYfQ6qQ+9+Q6P7KNvsG3QqHdhK5rr7s6WpNQ/O8bR7mPpE
djQrlrsCWS1pU2DRUkt0mmjJ7BmpvnYq7xLyDaYb+GP32OMcIfHk49x+ulv3MD+xjaNgvvSDgXGu
LPEbbkbMRAmSsTq+i2TxdHWzdYQukZxt7EDzhYrYr7BRgRyDXN8nHo2O9LeyAmvxsxX8xreX5liP
U1Deg+fTHV+wBVUQY/d5v2pMHAytQY+ryWWKg4TeLNjCwlFNzvp1dvIzZ5Rzmi4ufklgmPEdxJ3N
wOR/zFsQV/F0cHA33cIQnRsr4fzi+qwCbAEmy6f0HcQW/OCYcfBmVIa4h6jTdjdUN5GmNot3M/C5
HCUxuGfoD1pd8RjuYBFvZKqA0WLuxJ5Ub0rnfd0sab0TC7GhGfdJHj8f2OAAcby2ox1vb+CWfglD
qzFcJYmzoW29GkxeRtj64EmqVJ8uB4Dt7CfW5QPhDgciFxNXjvmZyzaUv/wjrTphsZR7l6PjTsIf
waQnsKQGJdCiCdGPWqMWrzkpy9xPSRoDnP8BvF2MknFPmb3GAyFtjgXZ7wA0dEaWyHTzJs/euk8F
jhwR1/cXaadL5bQANzdVoZz5KXHZGPsl1MoP68lW+SOpCviDwdjfOXHfRo2v547jTT7sb9pAVwOn
Yja5nzHOKAukfK6MEbzM2PuDeRD8HzODO0Zx+9N7js07WvPTk6XuBkpgrtP0EutP1NQyM7ZEpI9v
L0kbTOkupQvYcnx9zbEFJvtw/Lb+33ukeVsQXvuXPtTABaVm/alZcjXh7/nhgN3H0ZnlFcYJm1YR
kQxLG9Mr0TPPzG5tA3bLEoVbJRrv3/J27uvMRYi6ULI5mI/CKpAl8r+Qpnei7rU167Gns44p3LsV
qZXIuXKTeNhnyS7fTOE2qVTdDqeu60YBE7J8zwRMTK96lQtD77QLgw63lGAqMfRy7a/tYG6nU2o1
Qtqasn4IX4aw52lz4ViGQGFyuuZCJDNFtV2piQ4IJAwZVAWBVqgf7tHOcVLWTiukAbKTvJOtMtCs
jYew3TdX/njhmQun3gaNWSAXeD1936PVSbaiRSsXxgxeYVZ7wDTOJ2PHB+LA20U+j2IEN4+doNvJ
FQdrnPAGDVn9Chqzf/7LkKYFjK5CaJaubdf7uR0D10+pY6VY811Yootw7CaGiNS+Ipp3fXfWg/ft
X/gErcsDeGPBZWoDSmNIrYDh1wQPdboRrL/Kv/Qdk/e2vWCCix/8h3axdRG7gCWjV5suho2vxvpL
MEAgSfJaQKwxocUImcooUj/3kXGbsh3687OFH7ywxiXjG1Qw1J0gQ3AWbf4uG+rN+rOQOKCmdCCV
xkty+AUfWKT4OfpiFUyiML1KR6rkkfShXgXyQ4/wYezrygkTIa8mmEBNmV0QRMj1NApRJFo9dxdq
q4J30luJFLdWLoA10+eFSeA5IiXaoAgeZ0d9gi7z3HP6xgNyvGU+SsSQoSBj1pPaS9qCawevQs0p
Gf+1lcRc+d+t2c1z7P7m+vVJaur0Z1u7uaWlkWJcD8O3GtYo/5RPJ4BxvuWwg4QvrGBoNtWh+VmZ
UtiukfiELi/rtkks6i5qrdYI2+1NmZnIj7nXhq/HAEPTWrXKkwWj4tOkFrTyGeiNcR+HLTtdNFv2
0rQ824GkL0d24v65bhAmBBqYgU14gmNr1kUnG+fwZNUlOEzl52l5Skc/cJ49HScBRp3oWg4LxJaF
tQ2PrE/mTSBxiQ0AyfPVfL08NE1PKiysMF79E6uGLg2YrePUHgy+cxSJZOO53VK/P2Pyc3Ldw3yq
t2C+4RU1EcoDs68ME+FTfqzIZYFtHkeelc6fUyTyTkY68pw++KRCpCKp9zYAeE07ah4yqq6lapE4
W3DLKMQuAT3732RPbJQSTZVbM/rStH5UBr3WBdPXUvl8/i1BbSCt/GSYIFt9MUq4wQ0xcycV4vrP
KSTIRAl20NGm4fHInoFAaO3cwxP/qD8g7mgtx3RwVkdGCC0YelnJfEaAido+RP7/+lkn00d07suQ
iWgIumdyR254KSbk2DLPZIlv3LTZKsrLN0IUHqyNZVD+JnXXa4KrmiWZiKdf5pp5A4pbYIrzhzgl
KYtqOOc6w5dny+gC1LLKwEnkMEd9WlM050LQgNkKSMeMj+Fzcs1VJ2tUKyNFvGgVKkMIT4zN35Cw
xyptzG4q9f0F0gUmjtHKXEdjnJDZfCw0bcq7a/fK2n225Ehh67jukdnmUV52e8EPCo0cI9N45oTE
+EXzAaTwKya+cAbKj9RIO7WHUzEZtAylgo2qMdr1MZ62OD+k8iPQEjfzOM9VtES0FxTJ+VKHw46T
zJC0Gut1GaJJTac3/rBD2dIJnsYf3UklQqm94q5adMfU/WKTRCtTiEAwi6A7/QJjaW+Q/hFxIjJh
AhqYa5g0fm55n1Eoavevaa5/vGbVxcYaxwgofuH5sTtWwvH8CwaDSw85OpcCrsShgjQJHVY7D9JO
xQMDiVmjwCr4uy79vMvKBYn1OI8BFJZxtJOD1nIvX/XJ1R3WsK+9ZuTQ1f/53LJQm7ZoPtUcN72J
0UjDCASiqbnhnW03y7C3hXcijKcqV+w8R32oAKs7vxV23fQrucbTMka+XrufyNB9et09eyt7MDM9
4PCI5ul7pXYVR2uRzedpZsyVGRX6naICsxxP6ar/CwnExfT6WDhpmydS0uCxOW+zs2+XJUHwsQ7/
kA+pLFqCWupjzvE9FetXxg2tXRgOy9qI2/kWg0KY1v0Bd79bPn6CiD/f0Atm/XLU4sl8cxcNul9+
vQiFuYw3QlJJWxBzYT6Mq0TdFvowMQBx3Y7tlw18n+tQ62dvGxie0L3J2V70lv9juneT+5SElL8g
dqTQ8HP09YNJURuY+AbBqBdFmxxoNOkwTlLgEFZ5Zwio3yS3lmbns4f7QvS6k+QfJHhXG8Y38Qn7
9Q+WPTlLOCifsTZlIaDwIGtVphr4zU3ud5vklF3u/axItnenbCiZ+LScYnANZoBBs3x20eyguoQp
xMxqnTEmKRu0ECgmhYjVTPghHEu5uERxBhUky/CKGikV4I7wsiGon4/sKKep4bZpo1c7KZy+/HGf
W3AIaXk2ZQUo2s4wk1uU46kAMScmVGRxdvTfo65Qw8ndjrql45D+D1D9RmOt6Xzg9PuQVuDQJdsk
FAu0tIT7cMCZYY9I08MJW+cX/nl+XRf+PmiVWil3Xa13i/BbRz8poW9eHXuk7YNNis5p97OQP3c1
yt6GgHdQ8YNgzy7GvMISyyE1PIwz8USayg9GCbgxB1WA0XdNwiFj+Bs040IwHxxMVA80y/DqT8CD
f6MKNeyp51KOA6Fmnw5IWdt8+jZPbyYcmRg/nlCs5W2hexn6g4ql0tu2xQlqxhk0W6W6wo9hW0wE
3uwJiiukx6atSmE1HCFxzoOj++jMwfgHg9jgmtAzjzcmkCOUEif4grxlmrCiDZ/ergyXgwy4raZc
yScHopXdfmEHrWxzxVK/sr/RmXfUS6KYsVp34MjpgeFmgOn1+t7rr9KfT4gpMoLAjg4PknHv6W21
SafD+ExFvxVI6IEUEI2hhNNgxR5cHSRCTnFRMB211Q6VvR6H5d7Dv09KqM8FKWE+YkmNyipN/KS6
+8UW4RDRx0NiPY20/0k03GQ4Zk7INpv6eVgK4UMRenWSL321SjRlZYZsB3uP6SS+e/IGXeK2vkD6
YvvQmgIAu5eBYb3AkX6nI14avB2Y4axUGWyGER6J1dSVFrrho93MLcPMRHzZQzESgwo3wIkjmBJ/
dwrpPGqNGWlSOPjvLQjf2ThELJNXSKLANihDZ7T2vjkPZgYYEg1HHYTlSajcLVnUEcARqRbrWX9t
jOnW8YSntQxhmiahVtWT5ML6/q3Udhl8FMkcCfHvJLSxs9KDCpxad9V8pIQF0lgkghncDf5xDxmr
PPus7AokvG260Qcak6fZE/i2PtOIXGXPYTnIuf0L66YU2w/tY2xKbuLJT3zbKKjhHXBTOToALi5F
l/kCehlQXaMKzrMokTEvmfdKws9F51BMTpmm6EEB8UWR/9IALexoJwV0qlfp80DMKefdjFYwt43A
jEmV7IpP0txGx1n2K89d/fRgOdTMsXVsnauCMg+dhsfo6NuIWlKnRX+ynfXN1WKO3oya3SA/IpzS
BtQRe+gmG15sHh/Pm8QfFxmOBBWzW6PNzDnR5z9Q1gjjwk0gHMwLIAjbNPXYp3unwztPfI57mou9
T19TqUMnTnZoHrYwjW6fWAHoTaIJ85/rkh34meUlB7GUTlGmISPOcw1m2i+6Ubcp86o0HRbslz7O
QZw/Tki01qxUuwgaRxdB/WQKgJU8GyLZq64SJNnW+Di3/gHBM6qv7RVFBo8aYCtCnWaOgoPEi2FQ
/3OajcWEZfxwnq+nAWRxHaGwSPqBpdP0y3/96C6FnOark7jdAKUFx3mby2wyp4sZJEAH0qUmXOZz
JF29BFh+ThhQFVia7uw+k8BmXKvxE3kl18N8WrUbzUogzbqjjxdqvcY9gjtdRd1TfQo2JM+/PrhT
NuTZcSeCHzwXFc4eblH+Mwc9LgGUgR1qz2dNQLsmyO9H/b/gXsa3Tw2rMPEF7YTNGeHFOuaTDmp2
F/GzqFqfPDkZtnKYqDYUN71yBrGtmnGrvoz7+RrZcWfJPRRnlyvyPRcV0CwyTzBV3JbAakHeAnw4
NiTvX21nfxDUfNg0smgaP7KhWi3TuCwcyPsdH7GgqmPu2ySR4uZJPm2JClX+wBM+qXEQUxPf1PXm
rSaIfWVNshk0GlI+xavBpqgc9JIt+IMjS+gnW5VLf6cZbCsTzqTfRe93jPWRFhKfWZX4GS4MUFEe
erpHXybjX25wdv2Kwu7AyPaDyDOwbWbOyBIGy+3yNbs6cZ2i3cYAd+Wd4XQY5r8ipZfKaPA1YMob
8ZMc1sPEarsobT9KXuKb3YuGWMtjIZzywiWh3+xVE1w890oIgtHRcJZmSIG/HpQOnEK1SsciZsQT
P100M3IOFYiA8umLKuRfy2i/DG9rm/Tp7X3q38DXiAPAw5z9PPTCywJbuCG4Stpes21uXFbKgbIb
KGHPJHguEhy88yjb4+Kfkrkmb+uCvyAKCcFvkLCXvUfOgjrvkClr2nB/nsEUOkVNkLmxC6twUXgV
WUt3vpFRTaZpjMD302NGVhEHUNAHCK/VV+Tnw8kbUHrYXWJACpXZAXCsYr0OdSCPTI2feojkhLS9
W+cUyjoQB5sY5dyAVktt0xJoby0XvgNwU5IAkB9Zw5iDISqW/v6qXC00uIvmZ489wJCUmDTxm1fQ
9mj7euesZFWV2ihEkUeYi6QbBYtaiNBkPwR0fPelK0YpmaIG0R77f71XBA0H3oE09bS2IlnOoHb5
IDzL00flRjjjz7CcIWASPcTUBgjt6jGA/6srdvZHojUzGDpS/ndR1HXff07mFMFXLN/4UdcC/yDs
htc4xk937mr7kzelCbKbC+80e/0i+PFd4PQ3PiVgxYuXIm2q2RxHQHDK1dvuH9f8cxqJVk9KKsoH
gWRhOp/ICFRYx55xwVIVCpNaUshQ8gIcdMY24BFRHd3m49tIEtarkfXCjjHWMh5BsWwu/ouWrfZf
H5HUwWoI/wixh87KqDLapAwQFDhFI4HyJdwEpoD6e99NJcwlpBjN4xBT1lUZwjZ4NfaZ28yagBap
fj0TrjVhs8lXO7pjN5cDJCV8FRZyk9ppBIgptQfcJ/sJAQKQFJdEyEV+59q/rszxZgOPlBbivH2w
7VEGGl+6VHOAwDiJwYQge2nEQYiQwSCd5sGSasMcbBqbtZ78wo03xtrrpTqpbnwrFQa4s9BKCBmL
oriyAdlCJZZIYdNpeB85KCRerJii7BgAHk94XIb8oIfud3Gt2g1Uk+m1IrCYGH0VmnRaCBXPWEMw
M+sWPpTGKG15Cwc+pixMfpiGu8J8p61wPjGpwYsh6lE1FYYCeiK4IdnYN2ygke0sUclze4nAaoPk
cZwbp7RMQcWJReTW7g3qzTN3GTrC2bXWqhs9ZHxYlqVifSoKUKZCdhu2BCO7sCz7Ol+0FP66NIhN
/v84dIAjx9cMBRvyD7HahFwzkG0WJ5wDRBWc73cjvoDHQOQdhVjlHPUa64auoPw6cbjJ7T6TUROE
nfLio3RnfDzDT/9ll7VQqCUbsc+bTRjy98GyvEhIVfnAtB+un19wg0XT0TjmoDMQXulAZ3nLZQbN
Uvcwk1gr2HgCN/d3NAYVvkD2RJRVL9Hvjq9M3HJP4T3sWf7XlXiKyjAFGkBvr2gGuVoQwJqnfQ7C
vRevgBTwrCyU5XPD0jxWAELD3Q6UtVxz9KVfFsSEBk4ldERpotJ+yYlk/pfiZr6nePDlRs9nG9kT
E+ZrPPea4KZ0kvfKwAEBlCa42ixDBgDSxLikhRRxh0F1Ot+RXIGKMognwsV4+IxX1tGFkhPXstmj
m3fWgj1EUPNljIQwY/7qqH9FRpSew+Na1kU1DKGjQ7nY5rwXRTDbRW9GGURpl1lv569RRcl+cXYH
XZvUzdotnI6QF0QutvZ9woux0m/1ATtyeLLN7sDdZR4NpVP9vZ2haktWN+9bMxP4SCvXhxHX90j6
//aVD5L/DORzq0i+y767SvH+PLQmVwyudP9+ZqhT8xYmSQf66wuM2hKleAyR0ESXU6Ugu72Ixmn1
4MJTWmv5pbvNc7OapClF6sCYdEmn+0wihttsRhqcw63vkvgdv6J58tG5dhL/qRKZP3N60+3smnuC
l2nkRrLQ7OOTEJcIkgSuacRwDqRUhigyeAG1kbauF4uWWj9wtr57pmNFiwSWG46s0GMKa7K3omsk
wHal3LVSezjxaZC3VcGV8cVaN6tTvcGuCexs5fHAHjaAGWV80nC+rRQ+GAoDkh9J3ECq0RrZmZh6
eRqVttZiq4TPLwqy3XVznhNTI28ExHslIIKQbvYJdC4GAnwpb/A9M3wWzrmMWNT6lZVuvNivX5QT
LF8P1x6v/0wcVjei5qWfKO4IjIMh+ygPhdnupfL+I7ulRViQF7At+BiSlqhmXwynBDasAq0w9a0k
sNiVMIWHwhuZkX3UOuMcUSIloVsvmfud0qcWAHqxUsMpdsFUxOsnAGp8t5QXbCjcalUSJGNvH0t7
s6fQpac8PCyZ1djYe3TUxu323JBXwpXDHRJcd/p+ScNhogd4oYvGk447DJMlClRj9TWgoJL++tW3
RZDYPoPSanLU3Wy1S9qsqjiInvOUs/YCifi1QvKDyiRkyb5GbCJaO6BZMFgqOfSrfE3b+JBPdxCX
YP3IUWevtstqjkIBq9J9iCe6LoQYQ2CkyYq7ZcnG/dm0grnabfLbo1Hjz5bF+CZceafCt30E2PmK
GUYnVyADhUMRcg6yJPMv31v98Fm9EhJ2yKfTL3XXrxCTVfmXz9WPmdyrG0joy7HjXW5KdUtKBuhj
hQ94nxUv0Aw1hLKOj32ihwNZAXRtbc15RZ6p7OaUU1DUFlmNTnEqj2kPVTtzhCpqphSy5xnRKhWI
VEuLIdqfv1Izz7z2Zp7zwy2zyYVlvl6PJNRAdneo1QGtP9sSZJIViIs0CLpPs2V2wV+zjo1ZVk1K
FYvjW56VZS/kwLQfDAn5BS+ZyrlIbN9ddG3FuFyC0FhhihgJLR0LPMU/heSojZHIzRTrqVU5AQ/T
q/P660zETbkVtaAILZveZS56tRcJ4xcn/Vnt84/G4LWPUkfLEHTtjrZ21YQ+RNQ6e9ZSdLBxExpF
JmrPiv3N1jHegEq0k3dyqH7UrfNHwoJvycJbwGswC6OSOb54MfyoA+TD0d/RYsGHpFRWw0TELNty
FkmaYFx9T0s301H9rwxd15rk7VgCwn6qTZze17CnxvpW5T5WF/4qD6FD+jxyfGihnGVLnqY9mSJ6
6VzStGNCVZ3WdpJj4A7ca1JKPU1lLxr/t/M8f3+WbmazyUoSo9QUd+R9nIejpzUksk2w+sWJPLOK
MZ807EwaKulg5T1Dk+qiRLWQCCvvBqnSffJDWAZdwS7geWr2YiFHZJ+cS1dyzvLhTjba6GxlIvl8
zMVZKibbAPNH9z8oFoMAkSCrj7t38RoPj76g8nWQ7Ae4oQEmcGz3STDODWvZLfKaAj2wBBcdWYo1
/dBYWffIOIAFWpkwY8audZtYLwrKccLmaHpTcqu4WTR61iaqzZazN1ZGU9uLOaOL9EsJzWPKoqTx
TighdJBT9AToyt0YzVPkQvhnXK/eod3A82oSiWhQ/Yzm2WjEh160oFhPA4LxYFkp2FvZ/4ysKVlv
/cW3B7cnwaGNZbYnmwivoq5ZRj0s+MaYf6GSJ3MvLIWfNYBr90J3x8LrjAaWpsPQf0k2/4MzdiTL
+zpG6UjIwwnwtKHVZ7DyvnOe9uZs8lcMgosfMwbMllvp6YqyhiH+xB1HLUd+mbAqMZY+6fy8W3wh
UUmjOP4MoLnpBJc1cse70GudCogOYp2W6AdrFNmCZ4BNUCUHUNxfwoClY7HVn2E9yFbF4DdIAMb1
wgoErCyRb+0ZqqScrTJY8qWqJF0mMNxz+HbevZPgmc2Kr3psGb3CNXsjf0MUh7tU5I3KZps2Fv2g
EOzY17s+UKcSQpgucxZdla1PTw5tDQhT6F0JkynWdfVvGKPnNrqQgPZQ2kg/ibNMh8y7IAVAghPw
2neygkcl3abblyz1IW0nRcdWMSLZe99sC/PGRVNCNC30guHWIOo7tf51QKonXh547u1WtvchnzcZ
Ot33xacqzxKRARxim0kO+v7HP87skhiNM6FOUVjnd8aBLBRx/RIh94+bsACkiFvJN9MNZH0LsZT3
QLgIr7QvuGkmI6Ey/pQERcWk/pSzRriGDiG8x0RkSqaqGnMyQDMKlZg042GzhzCaTPEzYvANEGcH
MNqtipvs4NQvjHxNkAa+wURJRGjbV5YJsk4hgsvt3OG7C5XuK2AYUm7OBkWL7BraSrKJrd90DopG
f/nYaBTJIBw1w/rCHYQU6bZNrzw4E1gGaMPa+ptoHPRIR5XrV09ehks1BK9rlwtkGKMOA1kNqWyo
ywfShjXJFuhER/ka+r7HYJ4M24Xec/utzSVsKNcbE8697sLo+5oFtasPMHI3GX3cXZqlONUQB0ND
aQRnSRlgY4+a0COfBYxVAq0EuKuVDEOOlMquLSHJZ6XOxZyQSVcVhUNJChp9znmdcHzXIeknBBQj
WRZU8myxVwyYWBdAYhnYaqCNfDKR9jTSU5uuImD8JfSufaQ90tH3KgLMJ6hvb0ACcwsKL0rFpUh6
QCs5Uc/NOx48Fsn5Tgp0UDbSxLT/2RO3RRo6HBTcEMaFc5xdCfwvoj4APad8jU/tKWyfZehQGlBk
o7sp7bjVr8VOZk5F1UYeLKgJ0ltkIhtHSEWCTIGB6Hrd0KgNPDMbMl3+Q0jvQTEt2Vxxcsbs+pNn
UKzFfS1t6YznvPTpulZigYq+XXf+ErR4tiaWXAwvt2KlPxKfBn5Al/aCSdIROeE1kg1bnS6qKzW7
JR4/KBvuimMysgZ284Gp27cOQLJ1ls2FCPNpnSz+H9DhlZByQ4EESshaA6JKnKFg8g2ejBlnX/yR
qlQQ5HoPl7ErJ22HvmFNOcLkhgw+w8ybM9eNbSobo4oydhRLywzCOXbTTrXeWMFOts8Tanko6qrx
yIMnzyBFTI1aHdADZX5hiS5AE9GpuNIKUnJuZDYnbAwIOsjZVhxzCcK3ut343csZ70svs7jHRvGA
MIB9ZmnATsR+0wuhL0zKn4D/dq8myXeRahdtrF2jbw2HY1SU7Rlr/F+j6IUiMLEec/G4XzF3bgxf
9k3vAyyd78xZpIjy1qqwPYVd81OkqELH+7d1JIyh9dyh3jnc1dlMxu19jodTxbxYVXIv+wPEsk2G
qB07jlXfUh86GXyzEwAvI+mell5i6SOXh3CozLHGgKVV5M8XbBk8zB/mw6njs971L/oIqbQ+MXnG
SN6l9dkWnaQNRp3KF44kkWZNXuf03gjrojAGnK1bnucDMqYqsOBs6bFxg7k5vQcvILYLAIUnS1ui
ELMmhVnWOucxQDwxadEW0UbxJboJeOS2Qio/9xlfAn2JhCvG9QuHxge9pFDNpNDk9ExnfTj7wCAh
1pPNf7LxLC8zjQbngQaNTZfSrOP/iJu9j1Gpo/zceFDR4tv76LVq79d1QRWakVV3lifygSzu8CUo
xGs45kvf6x/6oVD+DmKOWlmm5E8oroeyH7k2snWhTQAd3qrsA4xEySOkdYKYSMgdFrsLj1fFjMYQ
PyCwqe7FefeDTwD+I8VIu/aT/7KJpw59aaZEC6nJqx+TFs0YFAH7oP4i6+2DUTvZ6SxM99ekfFeC
yX9KiZoR5ooRD3wU5WuoEoNJU+WLYmqqigG+ajVZUrKSFKyY/F4GkV3a4lGigSDhWJR4ZdwJXC/i
2yiWXh+8ZiH6ROgFj2Ff5UlFit97RTdlQwSF8KEcUdZDBE0oau50Px8Y/aYVHvQD4h3CCuUygaBc
mm5gzpH0OlJIJJ1XnsUuOm4irRuVIE98ALNfG2HlSzGXqUYBNjogkLH1D6N0KItS4NSxoRAAff2Q
mrSSxs8Qp4WfYASlboEAxBD4+BJ3xw3OjDXMhhh6y9He8J9TiHIiuBIB5icy8HbO/4SGXHGryuwP
s68JAsQzLoGP93DkZdeyjERCWLXGEUs70JJrtVl5o6TRJnNrHDgmNVKrsiiCsG8U18twYUWxLUPZ
exEhav5aogL6/dnKbOXYgoZ12YAy/k/apmA+Azi28FZP+3PLyRuMHvMcAEEXn3jUeNEbjpoMMr5n
/uL6SCQnlwGrqzfLOqkAgpiUJINRxrL58BSDNesvxTfZBGWyB8s92rgqOkx7SfYxHCCSoBXJK6Zs
MC/yB3mEu24lTMCXlh7inMgkXBXy8yP1AaREXlAIqP8KQoVoX6rlobJRGPb2Q7ePTZhq+NNMoPBB
t1yV9VoYXcAxv9A6hVVp2rVoMPpO3qnHZVEhvbcd2V8zhIIHrHWoowxb6Pol87oqKk6ZGClZ7vr2
GMV8zpOraLHaaZN3eaLVzT0MSDRTyKTe6XxQMfDu3p815iiRrI0VmbEX/X3vSUmDBJ4AfPzWr6jo
hwd4yif/MMmwXOpdBDUxGTNtUnRenN3a4uJzJgbXRsRl6fU6mu8cndT7fpXz/a2zvAW6OmOjFQ1u
Yb+qNteb+xEeQrqKo6VPp6yl3pRLYBSRBzsopSH0cVUSNM+rZYt7ShC3wgBWd/1xWddmktQOf4u6
zRKIWBR9J2Ftvq59uIxIbvTWZkoxIyJEx28IK6/2QKq+yxPcdpKlxLfZScaq3TuvoDiHs/05r+0O
AbIoBe4AOSxlFbgx/GP1Dle4q3QaESaPOU/Xz0eBU3Hottb5aiePa2kSiVVR/4ljA8ULT7s46RpG
XMQPeVWd2zvIUCAEWN/6g6DjQq/qbPmgt0qGfPDYDb6me1oqsCWzYBruOvcgQ9F1ppJ+iRwcpISm
DPRANE6u7//TmoN4olLWurLfLEWYYNLB7GJGZMDOnVbpe+E+1nus28lHys7Ac6fFfMUhmILHB776
zr79m9JzpTDBhrCinGitI0h6FTvcbdrUyM5cnQRC4Jlr7yznqoXsmI1MUprR1S/V9uZX2P2V8oyv
s0RLFhqkU2jjOdFRQEPWljio5aLhfQ/ptvBF+zCCuJ5MrmVhIXosVE/8AtrbvLMDhCqtA1QREwBk
X7JzD4pzXyUS+b8h0LFcao/CndCXPmf5ylnmwQLABH3LSll7GLOsh8sGfyuENYnBYLBIZuP3KTJE
wGcvnmtYbKVCOTr2Ctw3YnJ2X6Kw50ow8pqUU5+1TgDgNvzbbj3ejRdnoMBv4YeBmnAserEPe2/p
fjYa5jVYOSi/LMDwdmvsEQWyqFgFdN+YwZhiPVE+PPjdF9L7z5k7gXTuGlOfHrZ6/HDeUy6l7XXC
0ANkME+xTQA5LuWzZkcMoK9e1svBqB7f3fP1IJBB8zv5Qo7njZpPM9mfIdUmLHhcZalwNB8qfBsc
xnqY7AS1jk9dHXEcefQKglkTJLjXHO0/JS3bDn66n/u5q2KVucu6/r0Kyp1QPFsVgmXQA6Nec4dv
OjecBWy1aig2AMJn2v0ofDUJ1zS2MVo3Wdc8KVf6hI2wtKZOvSQ4t+/3/vZXL69e9QcqjwkvRojE
HKTtIz6kXoKV3CSMsXPB2YtiDOXQEvs483CE/6mTH+B00T+tnNiGPDTvRNJ8RsuCXQ3QTCMNGOFY
uzxWdQp2qpbIPnDe0eLLC27eb7prSqW572GHHtvpxCpTIQIC3WFLxEC7mC3kpGsWXLKNt9bXrIw9
ezuJYQpcalc0M5UxWRgmaREBkAmxWvKUxcR1r5MA3kC77Kt+suzfH/O7yB5l7gvMkyLwW5O4FhFa
iwc/EqFvhhsXmHwHXY/Y0Q82UkCePspP/AWIzoE1c4LVgGk+ZYyiKUX+TLYm/vdH59Nis1FzClCD
EXIVt5RyYJ/iuR73sEF6K+Vea19Ssn5U4jSWm9bEqp9iZadFquCREHgQzp1k6MrDQvSTHkGXB4K6
A+rmHEFrRdgBEh5fge6mDSmZUmRcqeb2g62728ihLJlIOUOe+AcBxnCYVvi1km3EzY+6WQdchrxm
GsMNrBifLgfTDjOMCysrmMOKtz2TPvKBsOHsMzUO3lsnQzqxa44Do9InYaVPBgmnUuN7VLpdOhF7
m21Y8dnx1xQWlLjrVXg+NHZWb69e2nvYI/hfweLzJkup33HuXtR5D+eZwsyrjOjB3YHYL7o8+tM3
3+05N+XxuwQQjKRiO3ZuRNnYcp5fPmNROh3S2sl0LTMotqJnHVWFH05ppvbg8PWk52GtzD3aQoLc
JXEwe/UyQ1cp8s/7c4W2CTmpeU+C/xFsszYcyHIDBKqPBgoQ7vlvKjQvPbF9+saF629nPHGiSfrx
lUebsJX/8WA4T48fms6+wQ3H9EslUa9+wqEN70jiMG3SbdEYocDJNrDLLNozj3bAT+NZ1cphAtST
ry02qppeSzZ99IOh+yR2L6mRKQmYxNkW8d3uUNMcilbuUEn+heRjsqChu47BQ1D4nt9tAESawW/N
htvsi7UmtfLyCotFz3YzGvBTmcMzyKO/rVUpIlVMsRba1nPzk8GFinSTTk2p7KvIwfKHfd4WwPD9
MRUmrG+gr1xaAb6ZrLgx7O1LzQALZ313HEgm/wKQ+Kv+CbL97bjP6Io+rIMIgiyujFidcctPrKq7
u8kCfFlbukgTdFqU8ADsCJwKHCaiYC2UTM6hQde0YlcieBoU/zrvNuZQGaXagE+A+YuShA/Zz+Yg
V8EgA/pSElYdrmLNS/MAItpsW9/PtecunuSWuAPrH+NzRQ09Iii95wkRyfF0EalfeM5b/VmDbVNo
ej9GEOoXxJbSn+2R2ABMW9rNgKsR/tCbzt9XlnfvgsI0HR5iTUHC6xjYBa4fwEaBJ6KVWnh0jQNN
LP1AYyR/EZMcaJgdYAIF9XtA5VjPDdFvvU3DB03bxfIon5//2X5fzN6xVQY4Q5ZcO0Mpf6rvgKZD
A17o5twNIQyIX/wpfSUGWhviBcia9zpI0bY4jK1iAHFmu8bIkY+AMumTNjTT2Y8AuPzs7RDZMAKp
++SpnkfNVj0d9bd6lX1KN2xRk2XB9gMsTa7AaNTdt7T1G3VKhE0iXJIoQ0VaSNcfIQYf3i/XWNtd
dToueFW90gsKGoi9O+0R8GQxTbCoLYfmsGjLCXa0UbX5NsTIdXm4Cbme7xZQeJ1b93in3Wtr/q+W
mjYh/lxXcuAdVzVZwzQO7XDbmIPzKDx8vDtJ0zYFfwve39zlqPHRGjF6b8V0vWq7wOcd4Rcc8zpG
2zsbCTrw4BTZtxEIGD8LWu06L1aMyfRCk9IhvutFVsvz9lZPb5P1pnfEMImyhp9Q9Io8XN3vPCaw
JzheWxkjLyvj2H1eCKanokbQusiM3CjHL355TDqGy30mpcqSodhl7YpeRQt/sWHkjvVgLtQocEmZ
98O6Wi77m60CU8FFAi0pjKRqklttgULNivNX5hCI5oEbMhaOKMxyX5B6xbISecNlXglhs/h7sbeS
SCyG3sQWhssCpxFsBEoqwFpoPrBPOsyr36deO6dfZ17puQoPH3jMRGrL11ZKqwM3NdsUvhCua7Fq
aS6SDR23FEU4WJ2m53t7V+PeB0Ef+0bS6LQpQzl0H+rcwwLrOyiarY/g4FLsa2HS+ICPCKFXs/Eg
0wCUhReTVCpOYPvgzvYnejNOam2vXpXOPeWZgr4MuhVjeFxBjLXb4nYfO/QbU5R8gHxMjaRmgS/Y
6TanDJuwUFaW4i0xTq7IXlsPdEz2npwzfpD8hWm6xwi3x1E4kEt7j93Gr4on+xoJ7oWkZbsTaPKU
EDKneMS3/32rs9BbMMFN2Vu2xzOGkrewBwmoP+mF3YuM5ePmGI0hljQArWVl2QPHWwV0zOBNTzml
jIjsIFmUq7eC9Q1swnvy+SWd3eKK/w3sh/VDGTkq37X9xE5Ymk67ZmiNJu0AJX8UiBhpMPcS2bpO
C5PpDaCO6N4wrxPO+1kq26YMWPHkO50AByNXb30sSXD0ivnrUOOtPQ6kAcvs4azldelUITFxOe7Q
Tuz3FqrYosBK2f//BLNizrtablabcVR+X2dcUd+cGv/hiw4vOTiQtM4RgP5iGgv3uxFBsgaiMMrC
hVhHzdp0RHL9I8o4/fptQo/40DC7MlV/FgFDXD8RQAZxb5f+VmCsgQkmHFaGqpKVALouDjgNSEsn
DqkpB1IrIvuduNHF6ZK8a9imqB12fnqsJwARQKxm6WwKDveRWLjVGQoA749ofLE+qC2ZKm/744jF
XSP4HwLbqlPCqe2QQIXjiLPcWKpTcyEOCl0kGetOY078XAW5grFv1FggKb7CDECbLZGgxtbC+ugn
0WnQ2/LIozmF6uDBS8Z/hrbCH0pNYpAtToRkRXJXVVs+f0JnnE6KU1lgtS3emr4lVDNh0iyGuROY
73QJRPwFxd7S51tlU+U+BqSWOINx/NRG7nizMfewkeyaxlPMlGBMw4HpjibT08VCE9qEYoWnxgdl
JgxWwqgu4/ZFed98ejkgh/zmZQIU3y/SySqQFuUuYsnU3L7rpeAC9RkYxVggHgUE+ywpTwzg4uXr
eqQTm4+pvkmocIHdwFgiOg8JaixMy6jMsryg9sZpGmat7NGJlWFA3YDf9hQ5SPm7Ne3+MXDtA9mD
Uf7KMXrZOkED9gffuGDWFaSup5BfCaESFo+Nb+d0SWvP8J6c8mImnAScW+7IaALWmOktE5JbQqjs
2aIvvxdVPkfQl+rcsgFOjYJh6Q/jujb0TgD3HQcWp/7byt/SlZ5urNihxGLGlv5LOO5mPy51Ho2x
OtCpnYRuYpvp+u68vu9grmUFjQZFvhmXIAHIuS/9kd5tTLqyEd/Wz7umR4Ajt2aR4PkTZxsQlCDC
EC6XAXPWMS2K0YffKH9EVBdx/uejLE9C5nwcMzyOy8sjP0AzWHGIZEn63m/yWhy3GbmUDn0UB/QM
s9ZkSNtOIkM6rXijqYe80wACA4CO9KYNZDYxJ4kZcPhlTQAXOONx8eRwS1w7L3reJ/aoI4bu71sO
Szi736UOnXuPxUIQkuznxwWnvBQRfC/bgHg+qpFRzA8I76nCF62Q5cz1y9cqpuwvCYQMSX/3f3Ws
IvR2uidxK8lhDRsVJdVzNsN0n2jcKotCELiE7KSFNyAkTl0YPMA1ExyYZYTQd3/9gjXQPd9cB+uq
yBJsE2y3+Gr0IBYv99LB0MCo3Ze2uQ4rotPm/TgdDgo9XfQb3239l3NBeMoXP7fTVxSd2cgF143z
jmFhiHnHDYXnmh3f9Tfw7u3vF2ZKQdB+Ko0hSa8royTr/A74PXxWYavXUqkplyczDMObcPMScrt2
aQzgO9nDKzR/hSBMA7RkxMbpEJ5IdXvZPtjXMSl+29LuVZagkENfU0svzZMIM7BZFTmUhwEAyNSC
IIMSPd9tDJfO/vjOmlvike1djfrkueKQjTfEkmyr0MvWZGiyavwWefTCGmNo8c5Nvx1Hg6a1LShV
7/Qf9amVa1e8UzpJ2QEC5kAiJv8TkQo1qbE1hzeFQzWGzXmvk8w+mdeMBQQGzi9CArtugvYpmTiv
lIH7OngKjooZjNovwszx+Np2Fzwd63SRACCeA3yu1kgArfoiTgZIYgzmiiFgivvC8u5eptxw49ED
M9bUtc0fcGx/ZlHLUivkzQA0T25FsV63gl8tgoIFT5gvUaXKAhPvJJqcE7OQnD8uXS0XXWuryI1f
GbuC8bY22CVVQEYtWACvB1z9VBe+odFcaOMzVllaMtaQrDytCUGJ06TcTqBKu1/HMvGMQr5OOt4S
by66EHv0LR7SvryI+aC6DFljbe+UtIsqFqfi0dhQHUuYCFpY8AJ0dVBcbymLJDdyiWWXmf2H6ryI
TPtGBfmwhLC5cyEWP3Id+6dm6ansZRYJZkTvB19g4nv0UlCXXeC3yYiI/no37Z+b6Q+Nntesgkl1
D99WPjs8NR2/u91wuezBTMpNgnOxavC6k6EEc4WtYI4JKIKm9DJGLI519P7JOe/MGmZk1DBCDwaM
ADarlQvRebN6+lkQvI20l5MYu6I2GUthuGtgxe1k4bv7bZ/KnYiKMzrdY6KC70VdjfVvwTlM2oaE
IrN9oWD+m4dL6zeFPEOwP8ZXE0tl52RcfuQabmdh1beidALIMd37V5hAR0AzWx/T4lAumvRMIiQu
OU+EbJzWXBtuCVa6yZ7iR225IlCpmVMlitX7CG5WnYYLcNUAa1je9dkM9yzK03hiRDKWefjOdWie
9ZJk/5gR7tBAUsEIQ+GpqXUQv8c+XsRSTNLZKYjYQbxRX41i4Dl0myMichBMK9lYrB06qE/FGFTt
m0KY6Iw+RFRSPBkafiswPuLxK+ZjzaCJiyOXuLmcJS6FHEI6+lmflIiN4e824NJOzEORg8xKIgjr
Y7yRBbXcyjDp9GY62oxnaWiF7IRrg6lIXSlu3ZRxYil4V4ImcSNo+oziV+9XqItfNdCblYmEbACZ
avw8iAjMOnkvXg1jz2WkXdzIXD+QxLVRMAhOSdWZxyrADc/MX2wuLVoDPc4K66+VCeV9MJp1b3gf
QdzaBJws5/3ki2zfW6oMfm4AJaC8bsO7FEP3IhY9dlyIMXcEpWZAqDlzhMSdWkm86wY3qYvvIJTq
zUZRo3MB+Ji2q97zgQPuviu1rLSjRvU8hDWVm7jIXQQsSfV1GD5xh6iGfDlVU1ZhCsHNIsQVKKHz
nmcJkT77r5Xsg1fboq+iIClnzyzZErA8OG3uEqiDZkvsrPF0zWnFfX7CYKwclgvg0ejAgVs9Gb4C
Qzxeb9itj2r8SQzUWLDwF68VzGPtvnz6y7HSGT/0ZPHB1LZiwjgVKn9L6vfgRlay9+SbNWt3hNEF
e2acuNtT5XXuixapKOAISl79ftgZrOv40TeMl/GIhjvU9uS2W3679D5iy+for5osEo9E6cNY/ACW
0Q2AFnNeQZmdHPXdgISAel9rVB5hGh71/Orf6pbjhTPQ4+fICQzLWfcCbcy6a6DM4uD0wiDwCF20
263YahQJm0F4f7IO3JxDAEcLJgMyP+BR85oTslSRvemTYUJa75kst5fMrOAOWT07krt+MkxIMuU4
Zxs3dYlIc9EWqnT9GGFB8UHH0pHGP+8gbqMA9DhoeVt4wGPJrYhr+JjSLgXpgH7Wv+7Fr8xp8so1
FLlXsCKjMkoCcIYnJjn1Ai1lw+W0aQfVElV26S4Y3UYiMszMrnAetoCnuBTxY24RZ1W5vv9Dj5ev
Us1IHM/Wv1evOvOgRCX/+lPiKpu9OB4CCsA3/X7NcUTJnsBKv5MGg8Va14C8MBXGS5azHaj0HtPz
QxiqOpei3bE3em5KInA36vlAtsioqNiAKi1TTYXklsAocY2Zl5/iFB7tH9cIZ8p9VG58GJgBkWUE
wrg++0mxIzsjpnpPwkrwd4jhj/a01u0hCqTbQITolSu+BCboKAmD3vDGG75Trcl4fldPZLfbTtLT
6V91xsumach1Vit7g3fxOZlNbBurbrXa/zvS2il9OBuYmwYXAxqlSRHO2ufj5bPwhfBXOECEzWRr
fPZYzhLNa5sqhMgxbyC1nsLv8am/rI93PcFaw5yJHoCIG8me7IHJKbcnCQJgDiytplgI+mRimcqg
bXFWzN+Rb4mR7f4kLbU0hO+NpXrUxC1YGW5DPBHpWVDly04zMChVZDs1f1AlLsKuBhudZDv3QQNt
j0g5aYpcqKHOWUN9ofiIHJCSPhtBnKkkbGJyYmFnKQoLJR4CusGs7/dBExI8e0QyM+yZ8NaMWY5g
Q5TDseNr8dN1roANi1mjgbgk0/0hBTcpppzfdrKQj2MxctScwOoUTGua4rgXAB/lCnexkRYfA3ld
XFE68y0tuRynVUB9SB2wGP49dSEiofCm0I2hIeVPjPdLDcwDcHRWztrBxfDDCvLsVIPs5GDGARbh
dPz35gMhGE7nsdJEwB0R3n6CG1SohZv96lrbMlGO4w8uWNGdvDvact3ACPs5MA5SFUOpm2mjAR2V
H11rP8gaP8003TdMjqjM6m/ftUzmqpfcyBwQyuEl+jXpLgQzrVAQw+Q+lcerhkVJr9xTtfBzrhEh
hTNCQ++3SLu5YZroK3IVD9w0YHRHeV7yD4x6h2TrERmbG4B1F6NhAVM6p40j5lFbzkEGyviPG/Wm
vjQNuxN55oW4c+g0Xh1qLbzUP1RNKiKL0UFXfOKJRakp/VKlJj5N5EyTV6ADlZ6cF9KVSZddaiqi
boHH22cjGHRSdWrdVoy5nwQaSFty1v+Ibf94wtK9IfLbJZsOr2xEuZk1jeY8LvC9Bi2KAWQA7ROn
7VYzHX9+VQTl4a3s1d1HyR5arOJmG+CpWg7zA3SEZP5QYPgGaZr7V3EAYdwX6wvyUwVQzt1OwhCc
sA5LFGZA/nFxIzyBwwnvpeHrxM9l9P0p0PcCUzRU11idU+6RDZ/FeMxS+GywLTkgo36eUaF1gCnB
JVqxEUXJx34XfD0o8FmPvC6Ned7GIsJMvZO+uSg8ZD/xxKnGePsuOAbsoFUBo87iafffZ89mWlQ2
qHlTN0NxeaTJn6oeH8RMk0344HA9RnYgKl9mSjt8nW+lJayBfYmMrIwgKAghDoQz6uIBFsn45YKN
Kf9X37AzINcCDbIfD+YdO8a42Ey8w2GQVYKa2xX2j8BXQ3rw7OFs1bTZ1JC6qXMKwKynpkBfpDaI
f+/cvYVr/0d/RJYAgQWbl7XqkEObcY6ZehymLn0EtTDYVpuz0o2yzxXQcAXKj/Dqf7CfoEQQ9Ht3
6G2t/GatTMSYez3kERkkUX6ZcdEAgsRW7mUVQqF3h9DmxRAyyr0atIDWt+iO9lCyJqMaIKbN9tnD
mvlXvt3DanK+Yse9vLrK+3rmUhW3WwMm6vpl9gOFc/5jwujxcd0FTJ8OnmM4v8Oxta0ec8MnPTUs
uM22bwRdZEfktLrHZ1ekQvCKrCfXzvCSzqt6io92v/9dk4JY5DWMXDEWVxVvsa7L3kenjU+Bnl0J
rLl5vTi0TOBN7UJcgAVGG6212Zc4Ffq+w6ZoY0FnT6qACZoQJ+OA6A1Jt6+PHQZOkfyKS4YOKGvS
WDDf/JN0DUYlPEFBEUDcuUeiw1LFzeuxp5PZ4TqS0A7ZYYjvMV7eIpQPOvPMqdTSkPCzFbDqKHTA
2kqBqJpqCgRX/p+StZ3W7Vz/VCvrjhczdGgiHd9SdyVaZD5o/13du8ZlDr7L/1rb42wDvJjdYntD
ab7kp55WJ2Z6VtKJtn91xn5d9H7Ud90hztjaqZJz7QyjoAh2ipfjW6+0r8sQB/zSSBhE+0ojA3ic
ryqc9P2iwvNFX9R4+ikto9+wfT5df206rP2vpZF7yI8MrardCSOL9KV+HkqHiw7W6ozSj1hbhena
gctTPrlHVC983hYL8hdWnvXa6HhKdzHigN0Bj3QUhOGNVgv/RHU25ytVWffhyk2TE/RFNs6NPkTP
yEBsaaubAu5dRsneuFuBnop7AuCLu3Yi49LDhR1ApWS5ChFhKNNbfx7WYvkAM2rFqz3PbtE8s8My
qjbX2NUp90ct+u7R8PBNhBYVP+fERCiYHu7fj7pN0dZXNmY31H4ZT8NH32QxW47EmTwNegFYD568
Co4MVvYqEyst+VX6tZ0uaooaCyxOlH98mfXJkIXYRwpLtfgSKsNGYlSFyuH559YToUo4Ifyu1a1t
KYQL562ohLx70liuHwbrluhYOS0pMaxYjthVdbLetoQFXPfpVKtDaicxCwi3Ffepz2Q1nJpo1EMS
hfyateFI5bERs9CeJaV5UOyFsYS4Po8IvCw3wdqybFTHXBo8KsflgwvPA+qR97eCMdxecttLtUzo
/+3MF/F6cga5kt1Gn+iz8fArq2I/fkJp/3lbNU+Uw30Cn5VF2cTZ8etbeq2hnVAkVFG8j1bg2qli
JuxxUTKYSba7gPFF6G6yVj+/CEAczUtekIGufnV281QbRXZuDNA/kbDfP8LhLP8b2xWk4yedRqa5
4fKxdOXtDbuQRTkeovM0LNcKhW2P9UQE5VYvtsn+8ThxzmVuQKG9E8+TwJvwPwrUC03HO+SAnwth
/fDPdS3MCMM+UXiEvT/HypMGMByNlLWHXpcXNjfuJ9yS74kdDRAQQA+xiS0eN3jNR9jI7FzrznOt
UBJACuwv5+ffNnKaRmwr8bgArSWR6EF7EUIC4fovumd8WnqPK036EBSj7PiDFNR6kRMGiGv6LkIR
KvyLzVsSTo0XqV9xFIYtxnbbG/rUruzRdAXhtgy6UUNOWQBtydSNo0SEmj3pnHWLstgQd4W/38F2
+oFsymR84i1iUpDgXQHQoPVjt6Oo/NyeEu6dFS8FVv/PDOE3GzEHJmEH0rfO3EngN5KioNfZVlaH
Fv4vEYwh6DvWvOKLJNYnPKxnG8N8qUeWWFP9q9s/fhS/GUi+w38ELctngLcc2y7tdNnjyEQZlUb0
JRgre39PbyyQmjB6aKGNonStvTIZiSAMc3YUkjJBzfHqtOyNqK8Hjt6A4rSjIMvA8Ben47IFTBxU
BgXCptOT0mkEtf40KE2Y5ONid6TwincK/Mt8cu0m1UjjWoEPxxpwNPjaapiZ4sNU06Jx86lHXGJT
O3et7JIl6kKvQhzbpT8b2J47rlnJ9s4BdVcUGJ9UfRkEWTzXV0AXtJeZpPV6wd1IGaDrMQRTQgmj
GcvtBhaWicrd4TJx4D50PnZwOJkEg+0hZuRZZnsj9GGO4WrCJ+EOD65o7dN+ijPdrlI/nOqpDq6g
KtlHDWCEEXdnrOIcPTHqvZ42nDT6M9t2odtnEbmFP6kUUGV6f845aBop+QXrK8SQkxT8vv0AsSxn
YpJIHohmpSofgIo+z9WmvmeRsH4y/xKipxNBvHFcxPuTmHNY9bYKSCzhsn+4by4PhdYnto0YORro
+WW2uD9w1prQ+oVNCEgXbCQTcvxSJWuEGn6MuFbueIOGCKB7jxxltkjNe17/lLOsiD84rZckqgMm
xKyYJrQec/BoqVPzD2KLhvYeUcEUtJNjXH16qc5dHgGgwQrwjAzEKGLyGPuWhAc+78Dm7iwVLVvV
/KoCP7xQI3NNGWWDLSpHz1EouFzfBoTNtwBdMdh7cpYuLqt2LBgl7xkVQuz7McddT5q8/mc5/bL/
3afRf7/YEessrJplXZ4oPZJ7AphP6cqDVvrlF0PK2SVF0KLTI4CLEEIeZmJ94ZJgapoxmIYhc8vI
oiWlKMhVFKW/AH2IqzsTLoZ5aX2oq7Qv4rE11oWPt7z2XZxaRpyJBUqwL3JcHSCNJ6+3wMzfNVcA
tGSRsJJkQjnhfS3MJKDZzsXiJuPOxS1PeoVGisjSb8NTQGXTv5iSU/1pmxZZ0kdGX96Yfb251Orp
qe96wQyrecPOya93Bhc6tjCYdEXXXbsgRbu0TuXfaU1CTv813zSSpCsRNQdqR6bI5xAazGFFXHc9
UufF9QtXgWI+vYSe6pfxPqP4/0SyejNntG+PR4rSOfQbCeVbTsJKmyugmBHPgH9n1KanHxl8oaEQ
wBN+JGnTM3QLDtPtiBls7+klQ//NoZTZYlTKW1vwPqtgL8QTCR3VYBS9oF1QHceuV8k04Nuez7Ni
uBGbonwZkYvDI+LCoD+ZGhw2HhB1CCgoJSG/8AZSV0sHPm2fPvnErRmp9zCq3uxjQ3AbOIWkz5ze
0bj+vlpAiIozd5oOw7DcBVBvyyx1mlAXHH4fGv2qPhmZfI4jEXt7G7KP8pwrRPdrxcwqglvHuSLK
pi8nVEAd+Sm53q+61N+QyNddUL7XMhKlFRTQrPUPPaTZrHPj843Fkn0LCL7aag/nSOGCxYpZtFq+
tNxAQJzXZ2tOPt8BC9MSbjmi1AlrUhOC+mhSi76FutDOj1UYxaBOaOxvQdGzgUrzEZyK5mzuuKd/
5mQERbePFkZYtZRKgtXhF3WxWRffIlVHZEZRqAeYUlTFvdfnk6T35+2YLY1K8Vgajg9OrtxqcBXo
OVC9Y5DeYV4j3Ess+JkhiNpRQeLpqbO7AN1uo/HI4Jz81AIlXTy4RID3PB+UBYkhOSe+Ylp3TZQw
L+3rmILR/pE+K2ih2scycfSag/i/dooPGKdNGLybtYMBphSDxU/OMvWfP5nUlpuoejjIcXs2arTR
OkxOIg2rFfdFG67nsajgV/A8qxlwLTiLgzW5YyAQulvvTp+fv9ZI4QO2TYkkj+lCEbSFTYPC/M6C
OC+Xed8Mlbi6ik8AsHjMxdQ6c4ql4rtj069Zt/Z500Jyip2h2QjGDB/0sqPt5VFnLCL9FsNo3wjo
IDOI3FO59ZFELKdPX1DyZ+X5cvF30f0JS3cuMT0Ql9sml+h6maF1vShLl67pXc/lI2EnwBB/Apfc
+MSt1LAkrQea6FmHTbJ6hDSYNHn8xeVPgAo1TMaHYpVKx3gc6jHYgltMGmirsBiACQXn+S3ltZzC
nQuth5kW4TkI8m+vG2gMm0FUf4FyFXIgnTndAxnovKTuVhsXi5kshJsGqoppVOGoMwG5PMPDnbVq
qYrDakdrOMWjyut2dDzg+7rzs0sUskEvnRzhvXatT/sX+zGvRmsejBl5ifcDwU42Fiq9zq5JNuO0
TaAUjzzcFaES0IizpA1tleK+2awpGtSvZy7dlLDT4M20/aHSLk2tHAytWGwdLn18tZMxM5eIFKw+
vEbGg0LiYzHxry7//0CxcwW+IaxLXiuAkYIexpdPsrMy2LMXyVUorE/Ee40LqdWcZPHWEeE4dQN2
lrzCVD4a6THYEEmS4KCnqog0iyOlJkEWdAWo0uO6K90ZC30BJuOJeM9EzR7MvVHf/IzOXQDLgWz4
KjL+GRK9ZoIsIojaF8gjWdocI9ApkRy/1ZQmoDr39PyVZzKuM/rJ4bTHRsaBEdfhO3iOkkHyhCJR
LHB07Yjc/3ZXiR3Yzn0taNGF9mhTTaVBmnPN3KLQfJLi0FSQ3hcy0EnuD1XvaTNTHBplu0uyE7/4
x8POvjApN05ufJmTJB+II5ZqBxr+UiTgOi9On6x6p5yA1w1kEf4pTIx7fxY8Rua+vzjsmDJQw1bf
vk5GrPGH8QJYvapiW7i5zW459uTC4UYC6I3qGkPhX37tN+omIDbUwXh4GH9qekSilYCIY36QM4nY
DK0ChMo0vIwqpi3IpzAw9iFcBCRPqBeEMPXnh8eJZhlB3kfqMvkH+IGUeXT5mUqj8uDx5bGyQApz
fWqjvirr6Ioll+GyxU/xHHEUnKcjZDFcrf+TG61TmEDVjyZgjVBY6YqefmX2B1ET23yrRIxchpHJ
s3XYfWxfHHiECXsRchSShcN2Fjjmu3nsiFyyjYbrsAh9e4F2nkWYLn39j68qfKhYv7LCh94cxlvw
a/Hm3k6b3v9QpgIGqLZB2qOb8dvk3yT891L9I9j4tt+9ui3s2D0R2/cp80MSJIzfc8D/1csepIVs
ryyvBu4P2xfCQ18ZcmxqLMyb7DO8xd/LladCkt26zFaJIGPusyJDfrLH+kuKnFqNaqaqtbOY4k3R
KXaZeOK9M1guXI5Sn6i6xqqf6p1iBmKTqCissaQjzgQufsePQ1j0E+swrUNtTXz6Ui5xQGcUZn7r
McxEzIo1b+Q8/WnKCUyw01KZUiGldGXAp+NLVvi89GPZJRUBb1LDHIg+/O80tqL8QT09eNbupLMv
9UImn0Mg4mpK8gFy9czdF1gLJp8c2fKLl2Pdh8Kc4VEXqAaMVmvLYcPpW03n0noPZge3I8nk5tYr
W3kfAEYld8WrBkgch7O7MTc3aNngAcxKdyLOE8jrhRv/x42DIHSEn5Is5RjGwhdryB1Ygg56Bnmh
quLqpdRz5ONPtqUyqBEvSinYlIic+fkA+iymTvpaSLiuVZR1jV3w8yBr8Wga1OcGhGkCSUY1e5C7
wXDbtUR8pzODFcuOrsLZ3bN8jAohxHA4hfUTjKH1QhDNNswD4cWhPq7At4o3KOQk1SAn7u4+3AVD
/GaNeDBwVZ9P71F1dHvEhqdgvKHNME4lYeBDhFo93/aA+HsdiOBuHiY9OKX3tizUG0IQYnBMSJik
FNegLxdV/bbqrfur8A1JOBkLy6+C8z7kuhT2AZWB7b5gqLRRhVbhMfk5zv/4VWxMzlQ5dIuzg0HP
xvf93rcg7eZW/X1bz8TvftbEPRiO2O5fG+PzOGFoq5j570r9Hc8/gQhC77+iDAai1t44iJJZIx5k
5y4jFxMnY8Yf1S0fC+HyxafMkvq4nFK405GEMmw023y8YneyiNAVAa5PGgVQlF0dEmBKcGDutXgh
xwvb7IhqQtpbKLHVMlb38JCNAFrX509tuRLARoJRSMwt/CagGT7aY42xQl2ld6kbGDl0hAUvwRFI
fWeZzWgB55aLJm4QPdSydpruSUdV1I4ji77vgDUJii+JxkNCPhoDhz12eAynSLfiCiQMdOj2qqyC
FpQZ4Of3p3xNMp9LRx2ESXGDL7VsYKRRId1rgdeTgSQTqnsIRMc3WT3MiABE7aYEAn8TlHY+nL+N
W3U7GxoCjhQQEIHIIohGgcx0G4Ps2pja/CHBfZLOqlnKcHetYPFgvTx3v3b+XyQMW8MSYWB7ApeY
MOAzCFjG8qg3/JbXqq6pEeec9HATAgR9FRZA+FzI9xbReWzwbV7w6hNtvFRYwExkBUZ0h9cMnbEq
sh2FUHFrYwTmHZOfcpC4zL/k/4BPzmxQv/XmccZYcz5ITK9Spki7LwKljLOIIGINM8pEOVSsYaDq
ONFzOU+o43vdKzP/0H+7/tH0sKSwceDONE2HE6sqxwuVGshZnCf7yaTQdM8qQXtUsOP/qvZPOCt2
RI0aY0VTNe17+jMprPOCNxJctXXb3MGKrHj9dMVh6Dcgj876ZjFFAJogRrylGPNLhs+3rNeRq5Mv
fWQA9a2S2o4WG7BpO5j1hbqDCPv7LhIxCR8omu51oNUpWG9s2kGemG2ugOC3hDirDrztHpGOE3+P
OrWNdS1AG7lVixhtQxWwkvlZSmtCAqauk2HLh9WamfHAPLurQ/Zj9lFUvzpc8KwYR6XlWzF/9hAB
PJyMZ4S4QGhEon6XWyakvHTQv5bQWJMPmZOsOv/KkAGDwtla2SJSU2GFZCkzhhpW0ZC4Aa6UUd4z
c7mtiBeo7/Q69noz9eJTLuAhqoKlLWR0Ulrts0744INhp4NjKS/JwL/azYZ2DssJqPLVxCIeQqpR
w5OUiryOoVEwIgzJSeejyHLePUYR2CeWEJ2FR9vi6uH6lq7gxLzSDEhZ89vvc2tj/Bb6v2Xv37Wu
GOjTM9OeK7MvweWxmQiFSkt/WU7yD7MSujgh1j1Drp2t4ozhUdoxZlfLNYBVfkaliQWcfSvPqmJ/
8+QgyOKugkaSKniTPgWbKr1YsZd1zYhd/2Po9uCFkOAWAcbDXTnsGlf72dYjD7qsOCDSLL3bFnv4
x9rCKuIj1nSDHJbC1WVJkiljNSniHg9KpPrAn66Vc6Zr3GUrsDnOf8foTd2RP1ZQjlTH4EOFfnc7
86FTgBHYaohe+l3VbtL1GtR4lFCiwzcdCGDmolRPRRCR1syeuWOdujo1qF0CVBc7Z7s7OzpfdCjH
qFP+UIE0O/vfU5CybEg34mDF/o9ghb9kVCy/oYP6YpEKqNuHQjM9zALNp7wpJ/mqZUMO2O0QC4Dl
A3W7QXyk/qtDmIEi2os0usQVynF+QuwiVrhUthOyTdxHcZ3iZgIqm5CH/K5L3eDz5pX83t9XxQz5
AiOjcRWxBGLFRBQbXxJFfjH5uHPe6tCuLliNqUO03x984265Oy7QHqnefG0ZowPTDq5q15ju6TbX
8GNGiqNp8AE4NjRk43NU9gdm7QUUqqPhHLHxFRdGs+6VhLFFGfJYED9+buUzay95pEXOiERDWlOe
sN1+RT/QBmawdHuiwKEa78cu/s0JNXqz0efK9P4alkYYhpkcPXEBJFTiyxhzQ5mp/LqDQZBSMMF/
Woy45tkCDQN/BZSG6L14Js3oKGL/D7NQzdg662EFeQ9NoBoFDHJnErpSJjkNvLEahuASZ2sGrucf
axecLK2komGstzW9pcdQWY2oN5oVEkRaTg3L8k97hXJitDGBQQXJTxmTV68S7Eh4Nr8/N1TYyXhp
uAGR024vJ3YSuh6RGk7ilhqlkgTzQXAN5nQKZbKQe6/9F6jxBzs0+EOTPA+UzjzzYSoSA6rX/SfW
kMf3yCWkp+0S/N14pa4+Agt5t8Y0z08qBRUcSbrUG0Mj9hzPkaOMaqcH45HUn5scedli+8l5xsqq
alPF3zFCWpt3TT9tSpTr669drMwzVKzBT9P9etLRkONYjuPautWb03gtCpkVnNLWGq+QuQ6Ze8iU
stbQmZzsxKMzM/1tsqWIK3AVjE8v23eyfZ4VYqJwvECwCVzmfZIgMSze3NOevbyrknHki0CveeS5
UZP36SEV6IXbFPBDN4PS60FJ90X3ARZ2m6AuuSOXCctt3qHI4CTbgN5HvPkaOCvo4E+qQCmng/ls
nTTsafKRA+HzzbxbS6wxcS0HTGLSJ4X8I0i/P0OwVU5HsqKksgK6/O5p+4j852BYctuRSvNGArUI
C4tXa5D3kbgF1ih89ZSSvJgErcTPrWdgfHRLDWogIw66WJzaDoPpthYclSAt+CQlU0boR9aLHZvD
4cfp00W4VFwTcG8SVhSdgJ0Y/IWmzwiFmM7wt+LUV2ao4YyfT6KFuInEYGSDa27YgFFeV9t6grlu
AMkXODgNTzRJ7iTFleedYe25bvFMUIpcJe1kMyLzViq1Nrdn31cvcu0Dmomr+NFIJpg6IgskuNtp
coaKuPv2kHbiyc1hhipz4EXwkOA3C9c5ka2ccTYgi+4lZH1IWlmpIMaspVygKo6BcKhoONlQRGUG
9RZyPW8hPROt67yOR3lzduMIMsoq2DknS3S80NjYUgjHTuDwzgg5i/tLORyO7SHFRPC0NJrLXNpo
4AJCD2pyASHNhX67QCQRqsM6GnWxnWVwzaoQP5SuGG1jlyexHKmPmyB5BG+g2b4y0pGACLyrx9VM
3bVxUJx0dc6PaCQSUgNzBAo4m9xVSVSY96tpEPCFNhq7RqKBcXkqcz/5g4IwrLzdMz3rJ4x3Kt3H
OaWpg6DoynU5/cyjncg3tt5UMlyjAwPDwFotlS66EUnG5S2EE/dFdF70j4z2P8Fh1noFAVnM1UUP
1qfSiwGi1GiFy0RPLvLdp4Kuky9utZ6dFZd2y2PC8zk2sNepKFvaTELBdlSAo3cUBJfSZTc+Rgda
Rg2WHpk+MDo/PlNF3CS8t9IZACv+hOSc9aFy4qeE5dcwnndDa6aN0UrhnnDjVgLV0wgma6HUs6Kb
TNHSxrE3R5reyIzxk9ERKxPKVtxXcsKjZ4AE27iPW2/mfSnnyjE0vd6QSm/OkYTgtBVLx9boVnZw
kiyvkqyXZGkqQSHNlv78zKdSXqh/2Z52se9pQxw7ohqx+S1Hs6fBojRHCcfjB/V3h+BndlLPWT1j
cw9RofHvCmip9FTZzS6s+BdJECkDhlgGmFISHl3w5EKXGgXhw8K9M4Tp9RLi+POap41ekD5dNJ8z
RKy3b+2sUr+jy6CCQDNq/wzzQRf8M46LUlLGNTq1q55iBXTjnFAyeLZ/a8BfZs7lmQTl/ir9M/bK
6i3fOGmt4crnOqt+AaRuVWhGbiZeA7ZHaQi5dGVsPigkHSMOaaWVdrFyOWhSBFMBEr2YSqPv6pjX
E/tJVFr6hOi+SttIa3NUuNacy1PBRkXf/e943C0TFpcRcfLpMcLvD7WDlKAJWYDjr+sXyhsXmfPU
BqEasnjJA3D1y6p3QVtcZU8sPRdvhsjwi02fHV4CcwXBnJmBNJfvKcztW0HuUko/6Vj2Fwf3GnCY
nPACqHDGhHaouGMvY1cXBF6qhhbVXI4wqsm8tUaP8Pu8Q6XjESFIFq9mG6iGO15XIzpvGy+KNMiS
SUb1n9uVCiJFD6XwFAdFkpN328imcbT07SG+UjKVuDKU3wSMH4j31QBzrTgpnzwL4aB4ibmFvD9L
oF8XdB3tk+5Z81CG2FCRNnhaZk9ui9MOMXpPSWZOU9/4U+rqYzuuBQ8K11PE/o3+zlWqgDRsKJ2i
VNJLx5Bk1pK/a7UK4Hq+fCalclivkNMXWaUCgLkKhp6zPKiTK9m3/WRK90Lio2IrLSX9b0AcWcB2
9uVaFsebIFJHVCBMOOT3r+e6S/M+pZ4M6y1cOgEZ345qYwffXJVQFD1skxkfqO5qivpHahkE2bYs
IIv0B5+eaG0fN7ZJuNEWhJUpEm+d5SCP2prL1ZYuBccubqyGGW87XRc+p3bzSO67MLFGjAkIoAT0
nL7xtHEs7ajHRMeGA3QgL61MqxfmAZG1cMkE7/n/a/yLQwcnJeoPsq+0AUnt9IN5Z9ZqaqRp+m83
HybwmganaTCc1Z2nk6EA6m3Zx1UHNXj1Q4txHu1fZ6qOExDzhJaEbn2Yd6d3KlGfPppnp8bo7HFO
mQEbdzV+QmDct+V3djsuI5UdwCevXQ5jPFyNg8Zhus1earitbuosv9cUUaK4HnDnYyCh7pWa45g0
aTCjlPLLb7Av9nVhqZ+hWprWald3U8pW1H+KfziZYZKFhnD8GvPEv2V90sPDXutUveegfdhOEj45
sp+sbKVmG4Pa/VmbUaUNAucRqTfOZrDjLFbzUZlJPTwSCcS9zMPZBH9rwnJvPAFVg++Kj34KWciG
zsfxUVM74jAn59PDsGs/27kk69blxqURcvtxeUQAcVcoOWlPxyh4hwKhM9X9DXN1EzF19ivSkkDS
rmOGzzCZETAx4oUrFi2gVtPU6wKRXSVe1oxP9rSMXg2YbFG5/aLwX0FU4KqGFkK5bp41jmX1jLR9
SEzS5IzYEsOsBdEcQAw8QpV6n/iQvbHFCFlf5yPKXEmCO2z+NLoJlGmjl9c8OQa/5cTXes/Enk1d
a5P3v/5ggfn7V3JS/tk7kmoZkzSyporzEfPIaoLSsd3GqXmg02m6c5uIgb2VQdlbYfY0c7s8bibw
IVJty1XBqS9885JHPClU5lq+Zb5noCDPU8HYSwsqP/HKLS+0dbnT1mLa/o4OPFaXsZXvdBkLgad4
n2BQXBkxMP2hcBrbjbQVv6rd0Z/avC2j5BWYJyAUmWOfSdGfbF+Bjq7+14J0RpyzhWvemnQjslqd
9oCPi2jmrYO5NVNTnUX3ejL4bdmMtyp0DmwoLcpyvnvMNSb/OGEFnVCCF6wZv/oL0hxQBuPeYqsd
1xHgu6kIoHfdBp+gP52fw2c/wxhX/ST48AWWwWOi70iZ2LINF7Va5drypfHxvv7qf453OkiPXRnM
85jHaO7VOMewteECGhrYxgNNvMaRELCPKcL8wQv8DqiZDY5T3elEERr+W7tbHezu0tHsitDcaxnI
7t9vkJfKfxXRqqRU/SHvz1IMlPwwKerHjPOEWEp2qm0AK6hmEjPxcUfIMNXDIvfB4+JSm2iCa7RA
DkF0fyVo2XS3XvK89jqhROyhEXKLh+Nr03kvHsgNJ2NT/T+EhPJ5xtr0hT9usB4jJb+4JtmbX+Qq
6uYdvga3clVZFlGpRbnUG28opVFWZ3SgnvSoaT1aLjUpavpRV4gPSKI0f3bc8xBsZdXP+AVsRDcs
hkKobw3P7+uFFp1ROPufa9YhIhZYwEInIP1JlmNCO82VOvZdfE+WnR1tHJciStE8f0grl2l7bEjJ
j0aKLNaBqniZUa5dwWlmUgZc9eam3t3DT2x7W2SkemN46Pfmv6IFzIoOM9Lx9xN0j4FzXbLDJdfk
OErtVUqDnKyLro63GeWYqPfJ9DH5QMeq0kGYf58rerrH1PVSfgnbF4CG8cz9LPigsKWYTOgI5ho7
lDMMWJE6+gejPDGAsm4lUnJ7uhSdDuZeK2UqPQuL7QGFoHxQZEVDq7XR3uZA+ouDgId0KcQRskl6
w9eFh2sY85htRlv2be173uS4k6EK4NFgRHKXkTCP9Tc4kKgEj0LFHztpuWnf71DsPvcQsH1Aa+jQ
sfMbm0FZa7t24mjeq5P5zTZp3TQDhDcd8URl7QYHi5ax4RSb4ifO1A1SE5QBcG2HqPlpNnTIc/7G
U2LyuxtkNf/UD4JL7JkACt4QoPqoSZbY23otZ7nn54xyAyCa6XQvEFuxM7nYcDjxg9bLA1bhWYQk
mpS6OV283GUf6ypGLaIzEItJXeMRt0R/vIrkd7SVFFcorLeRsZjflwgtWdwPscYAoZwAFYzq2opd
FyMxM3RNb6yZ6Iet0GnORDYFFe0kIriQyPgpFJ/B2uITOHJSHMBekptWHVWk5XaYEVuW737aSc49
knKbMtTA8vNvFZsj+serav3R7Wq2CB89HuegaJgLVISTBoy7nqkXI7rj7dvamejW9o4nUs9YjU4s
gJ0Gz6BZZNnXt41fVba5NYBz6/5hoXfnNXdG8x3NRn3HUDLTpJ196QQ3MpAMKrzxsY3fDafo5VkI
I3TypGSPGXLEIymsN9dMHi5pcCnvcaNMEuwm1KvHlgKR/aVFPi4rjca9Oe/RN0RYHrXL5qzUYubT
7lstS+/1AKhyIH2WQdeHqbjvnUQcV1EGrYvfmbh9xexGx09eVn50uQUs8cgfRxKe8AEWKHA7eBbF
VKULKkueevkqQbkfRNuSDKw6Xu0gX2XZFoF9UXwhZPh9fFB/kNngU1zSnNgHubBpw4cinBlfqNUE
DA/iwJWqOOLNLiDnM9+n04Nat7d4pyrvL3D/7wWKPXGbS2JF4jmGKqBzR4BdiGjuKGUyJOsGm+/V
9C6ZKgQUM4kQOv6/u4OfJmGXzeycnmeYLNd2mX7/q78dsjFckSAb6spoqvxuIiBrIQ9ZtKdAvl2v
xtmkQwHQj4X3Oi5YLatZ+70TbWW/2BQ48L07mvv4y+udc3P+UnUPM8EIQrQtzRrlLnibuFzEtwP0
YbcGtaUtEcbj+dA6ieIM/rxBt0j+qF6eQL9EkUqAB+rPG+pSHKpx35Igz2MzGoMZ9i7SG1zkegd5
HmgL0mUhAdfdaFqeiu8TYgCIeOQUEGThPsXB5BV7git6vcAyv35L8K/QHKYcLgt8CvsSbfir3tF8
ClibFN141JUfgY+djzDZ9Na1eDSiB1AgnPuYBJEMGi6bstO94JW30DVkctvfLqAOiHHAxlQ8in7d
Zntx+ML3nKUyetIcBimQKsBxEki1AFt3hlbQ13/Af/u8trnD9WsHJmcXW6x4sH2UnSd4T2i01a6r
6U1q2/fsgOwUN9xjB41Mn1+PDg4rxpLM/d73VdwoTl+AH1l+4OMEIbLEGBhHqe9SUP6Tcw20wWQN
xDMPO2psJn6vP2EjTH4RlScxBY4shW66o/G47CwVckDz+qiK8s9/a+BCxOe0n5FKRPxUKcfuHw+I
xVU5EbUf8wB+vpJmN2QNoH7RdRY/uEZKZnnhMNm7MCTvhVc938Glfc+gsm2rxcY6VM+wMIBWXWVK
fMmERQgGrw/olYQAENIhWdSlP2qr8L8fgFBNWUdpBJmwzZkUAR8J+zaMOrbkdayrbkG0HkcXiZxk
zoTLVhy45O03wW0I/8Xmgb9M1+PcH/IdW0AQojRQQD2/ksAtxRBGduF1hhhcROO9UUxkjieJ0z7E
TqC8hJ789VBLLKBkY1E8bQ9KNsWMYsX3TLUQc92ejKv0AK8co4EPResiAVp00ylkCezkdnFdBHUy
ADCfuyyZ0IQrlGugSmkllZij5tdoi1pqPb+PDZvC3GwakWentcYSnDg/kM4W8Vt7i2YaeTL8iBrC
OKXO775rI2QWRoOj/W0bC1nz1wLMblkeVawr/PU/m3VSr+6l2P/ivPgkK3ObNoASns3Z8a4GISd8
DRmQJp7vSeXemNEeuXgFRWxqHB9ESix6KQAzrgqk4Iit6AhdVJRHpOA35GfMJWsXrASq8A5k4AJQ
ZAeZbDfp7Htq89q+RETFXPSjy3JYNmOiNFuITud4Mw02VLpNlbn+/ytj+0JXkrEgoWcJN892603E
+OA/n2l2sCDowyz2fSy+m7IMwAAh+cwt19YzxQwyEw4de+63k+whBP8Q1SzuDP7l4rJVBcKuMLAQ
hZXcqbgoMTmELBfiWGXGNoWHiOrQXjSx76AI4kIVu/YkeKfx1f2hD6JjOn3XgfUdoZ8UTYIEbSwf
b07Xco4q+dbbAdB3PjR+kJdhG6Igkvc+jwV2Y4koPCSn59gL98dOmxhp4M1SynVg+/dlmtSW1bNA
L6OdRvCqV6Wa7khd38hACKPFmtpFhnUDKOqPnUezKljxZRBHwSeALTfuLmHpFUd3fTDZs61lm5k0
Q09TwzS013jnBpPX800fyv2Gun+2JlS+az3ZDZ+muUQi88F/3HSmUXegAaWAlmMcYCcB3bdONDbd
UIdjpj6MUARqKdrXLqyedXbhlWcZqWp25coNxbVSe11ZEWo7V1SW+u4nDJg2DBLy1PYz0xcIjWBC
sHLp/HCWIqEinjmkr7L+tzUX+Zh5gQm7oMAQhT0aW2AIk2ezJ8jcJdmORvVvb+I/p2RznMl6xY9o
Al2tz4yvrLwIY9tdg4LnujOX+/hlgHT8HLwQXM2ptEt/kOIgtliAnGq+ZPU9L96zDieWD10pnMT/
UesYpz/3RLFF79z9+bQ0b5I9t8Wj9MXWdmB+MsJU1sztCn9Wa5F1pBCB3YlIA0ZVE8UEFxdAzLWL
pkILIhOKkW8iq8njbnmaPxg0IT7GIheOQQl/BlsVRHMvBkK/MFGWqgUxs+rOEU+NEDvwgQXaPDoD
o2OdqeoPG+/4b2E76BuQxUqv/FoCmL1HIzzM9QZOTYZ5GjVoM6rkhe/5mo6qt/hH50qp4fWdOmLA
txnYKX4ExwQi1crC0fK473wpensFXC6df9KhhWg67pxFvZYNF9ZRw0uyOQV4pmjIqTrUZOJTAhcd
Btlrtek/aOPyt0QoKQ2YYhHfM7hh7UYQpxKbmF9CaudKafULGvgIwC/i4A30rF9fGOGQdJ9PCb4F
r9p0LwczY7nLB4LsZc+/R0BcHf/x5BTRRF3VveKLfRfM0Kzd4nG0hteyMhmH3zr7k949Ugn9yKYH
DblElJOSm5R/zV0FdWdPQtKI3xlGkrEgwYDDjjGTv3/Bw4FIC8P/2hgOZwZxTiBqaBsIgqK25/T6
6ZfXz3bXsyW9YqKQPAYAyB8JY8GK+Q+6Lr1lKOttYrgudxNuf4Q5Ak2bKNE3AuHLd3AopzzqCXB6
yoeFqGRJoVDwqzj8hea9BjjLCmQtkfjYXUry86YDbfhw3VEp1Z1oI5/k8oCMFAwSjw2cWzSZ8xab
p5IeTjkYAiOu6d9I+fcvX20lIlS7fnZ0tDXoR86JHZ72NBWp3SOL3R7OJ6tdq/Np/SOWRve+XzYz
HXkz+f/cSY61aQV2w+HODI7ZI2iW2GipHTP/hhYPkezCzt8svt9IczL8gXF5OYgjbltssBTwJjOD
0ka90uk/eA18el1WJspazzKMBRyTr0O4LtlhS9165mOai4FEfMB8y76oyJegduRQoxj4pr6tYEyC
M8ddOiDKiclVkiPCRNqDbWzc27PiPfxaXEZfOCa/SKUSDz7bxnn52HJxkntPGyzJBneZXcLIhVeS
AoMBBkNxLwr4VKwA2wwX/txtvGqijEUI9eQkBSYcOkHBF4tSdnMcZfrR8UsWe4tV5gEHA2HgZhNJ
EJ9X4yjYDjxfxf0dPfbU3Gx/irx//mJVA9U2S8Xu/tmtuW+ZdTzX1wZyg/42iT/aAEY9/pS7s+KS
YNlfCaPjF6QvVQbWmBbc7q7ocn7gIgQhQYa1U/2iKO+xNLdjOZ89HLyjLAhy5mmAd3o9YMt1F1m2
mCLv7DKwbOIjgXVISDBG7XurUQEdQmY+Ut7rWhhBBRDv7/hKVxRJYtgm63HBS1FWbExK9AjHWoIh
zyK3URNg5nF0FDGpl53tsvPO+OhJvqbfEoKRn6mZwh47LwExzr3U49/EAjeVX1dfUSfs3kPl1w0P
S7EMQ0Zpw7Y1VUzUzsVqCygp3beVRsq3H7zRjULOZeIz65T05qOGpTns2FlpYO6YwUbR0T6/6cJ/
0r1/t9YQ9VZNsXnSSzxXjRSw5GuxyJM0yMWWf7B4jjKlO8dfDYnuAJpW8ZO4InZwQ83Q7SaEgsLc
ri446B+FicV+Amd02Xj3fUbfmOZKpgTrvGVqT2NCfUcPF6iFBANoeI9F0H8iHNHb/vnsbSx8fQ28
YV6nnq7oahBgGaZqWJa/C/tW2KBu9f5baOppIJx7oMKgp5dWYI3zzxZpbViidXQZek7h+v/ELtk/
iLWV7yAbeZkUuJ8krrjvXTK0lpAUJTVVwpAFNgi6NnPdDomPdDvseauyapdopaq4fn3Le+cQURu6
m0aTmjq3QJk4FJkcWsdcyX67IQI5P7V/u9uKeoXNdbsTVLZ2hqbaJ30cMGB7f2uRPS2upXmRIXfK
3JwQHWLJ+S76UrU1YMeB/FnF6Rls6nLcIHjWbKsNvp3wNq/r8WESLwJT0a0vOWZjndgcuyf6ALqD
b9KH6p6CQbrjLMLGU9I7EnB+0BzBhbI8Yj2WSiTlRSktEOMiHJIIP/3hdgkj50WM0LHi5BOFi2j1
53rZcYYF6YQwI2pkW6FmJ+Xa9fJ3XjuEesM5ujAwV0THpP4bo0clYJTv8azoj43RNf6pqm+ATttb
SDriHSeGPgTvrNJiAc3IeANr/7BEzb2ZP8X3z/AuaITQWnBEusBYguvRdsMkxXuegEQN3T0fbBAE
PHfY3Qra0UAT9a+89Kn92uPjHOSu3v4P55Ccn6KB6AXSmfRjm44mWNpVpHY/Nym2IyQbSjytCvVr
YCii3UFVRO2uMRD6PPofC25V3UE01FRGCOI27YAJC98S44+cLe+GYvAYILokZHNwMr7Ezy7E4/Io
5Q46tSIiyAFEJdj5l+YbHr7t2YioCHJlzMjQ8BQLaUiKc4L1aG19zER8QteUlz7PpHCgyE/0TDys
0q8w+kTYPJujSnkKXkD6q9gCefPiH/JQPta4IQPsR693xEzLfosoNWNB/JdpKzELU/E4MIuSmuWd
K4hTzITnmfqiJylrxGy3vdHJlkLvIC2Yak8y4cGgnvx2lW8Vt9wCaIjuaW19KlUCEl2aXFGTnCWI
sdNwKra/OZaMsSt7tTmJ7qUNC9PD1h/Qa6nx2eSZXQN13HmIjRK133Xrpo78e//xYv+/EHRA98VN
ozaihhsENIoTv3HaUSJDTpECr8L3RFnufogeC3srmhMBdEzbmvtQBrwJ2dYWKxGpAaoNv6VjF2x0
8GHkt0nVhGFgVQ4AV8vSkuwmNHET+c96vPHiJ3uEYV+G5sN4RXLNluIiVzQScnS/10jNhzJzci9C
ZHB+n43arGuaRoB2ktWdWhzMK+VOE++QNqjcRjSSsJrL3c/BYV0UvqTYLtUDAE7nDvWB1w3ZuXCz
JxW1ELhZ449CYuNyoR1Y+cOwsS6zBL4DMT4Z2bklgCbpMf+Oz/j/1r//2nrj8Vt9xYC1VPRzkX6W
APHhAW1K5G5YTmmyoqK4vUErm/Fuay4z//qyZblf5HNva+CBs6W2oq65wU917JEgQrLZ1ROr6GpG
ENCkc7bk6/okeFZkG6TtremxGFZxhbJzToPRp3QaIRZWboKCuaZCNAffCo7xRCkoiryEUbsfG4Zh
NYpjXwsVbCWoEik4757DLJINatlomqxZIHAhafjhl14QCs3RCH/F1004RQY02Wcp5cehgt833TpE
XRLpfFCDM8540KoXEVyxcBKFqCbGiG+uOz0vD0VVpP9WdbTa8OEXc1qn4b4OCorYhOwc5SiNCQ6i
BMkI6H0TYpeMI5LBc17dT37WGIGFwB/mSCgsAOlJmaZCCkueJCQjcPgJF/SYpPXILiJVqg6O0lh4
iSil6e43CjAYQs7sutIDn9nNiVjN+B5DvBu9UQ4JyAnx8dvIfoZEbEagA6u6HYl3EUOuXp6vOv6o
gdW3CCvIHRp2c+6xXql6i8F+URoL2HQQpMemGl8BJDnGtaJXzBBoM9/ciGXLPqOsHZj/STXMb+ch
rprCwMmUmz/eCNYw940IRPlEWHfXCWxuPX4Kb54uj1iwLu4XCcb9Oe2XYm8k+lyb75O9E7yAPiff
DOVCh+nK03Ke/xgjmh3K2SZbxBcvsPCurUQVN2R+ZVNX1UNclG5COUxgUhrJLpRyTcXtgE1g0nmO
wKoC+psab1FsS2lqDwTkogfGZul3qr3fNb8aWy+R0PeNVqxBJfSDbk0DXbnj8l0wZvYXvVDz0vUO
RiMSHcr5ePLiOL+Z9bmwuaNfOjhp2u05zssykxWTXgYV8z89yW4jFzSDJWrMvGJUJImgMia3JvYi
hJ8VlyR32kp0NcXlhesBBCIjx/uyI5AxWKbdN4WFQb3F1kG4ansB2JYMPGbCFUaSfyNF50yZuBNk
4SG1feF8QeVqYC6fCOLik8jodQTmcbYzY5P0ajX+EkhnA9cH5AYQg1+d3GApnVwQFpIy7xsHdiZp
feT9i1oudOD6qOjxDfSZyMFMgZ6d58fBM4c/9nyQbRPJR7NkFAXtP1l+8SPz1wqEZE49aTmc7onN
1jjGnGaFcO8cS0H5uYsCku/1qjDx7ENNo5xYz1Q1YDdEVnCU5fAaM4Rr7YChh5hhvl/WQgIsBEWU
CdrZrtDnydexCQqO7AuSE8q0FkXIcUce6KTuBLwPMlykf8AOMBFhmCDN1HmLDR4TUOg3R9IBzxmy
2mfY7vdY30dp1pLVuDYM15Q0CPit4sAVEmJ0slN1LMAHWs2RQRbiR59qN0g+ZV2l8WiNk1UBNgkx
DODVnkLCjtJNMthWx/0se0n+MZ0OjnL6nu2HAP9KTa2m71q8x7ZmGavVTd6OAT/jsJOG1K3IzYlc
E9OL5nywMRTr9cC8cjmsxntBqFgFwCfpbV265sZjLKw+nAV11GpUwGOdL9GkPQ1i3zeIekdW4Ci6
+4BOR54EL1f3K8MWZ9PQAHIGehlK1n9vCGdLi02XJKQkAis5TMRAj8SHy1dGEMKXG66FJFLhko+S
WUDVuWMziaWHW/Af2BK248wgNwsjDeN50KqGNnMzHHLtuLSHETweVtl5v1m27jEls3gcaRU0roPb
561SBvz429JAf/pFUfzf51uYuJhMVxhVjL3GuGWy9+7GllfTT17GMVkB7xLo63mVW/3CrskCzuTd
0bcQuBKjRVsIU1nAlv+ZVx+xyTlBxDXQiYFa1j++82WlisOVYvy9eAqcTW2xz+l9f/8kjtfubuM2
FeAMjCvdIT0oi/uXs6ZThbh8/yfX5H/EcdELyma5m41Uo3kysGNmzihyzMW+32zJ/FmLFkn1zLPL
QpnGnQ2OzG2kzMHoxTApVeT0XHyjvBjGbh/BK2TXStTnwyxRLnGPK4/zatYmjYaD4B3+X3ILmhMV
hQmKPYIFGhM60fBaLXAInvv2fPHghJnRbmCbic+HTe2dBjMFr09QWtuQpQk1maeO9mc1ZRd4pzg4
CO3YmGqzPADDLunu2GiVhP6W1Ll9CgUUSiw+KhjD/PFBwYfgNGUJUJexkeWsaYi1ycn2MCOSAvpc
Om1PodrUn5CPBXyk64L7iHiUJFgDLEfTcByQhoJOgtu8Bw9uV6CSYtCKDk/JcaLJwSpzzCPuEBkH
jWs/2FiKtxXknyofoGkTxWeNL9qlU76+q4BxYvuDDckWWwqpfzxfq6ZyVPR9xulFrkr4Vaiapmtd
FEE11kQusuKdP3e/O6gKuP+W+lCUa43pV315/MPzPpDL8BxAvAO2+iflU8oNqsKhm6ERTiAUTxrd
6hcyhPBHph+c2xLh97aQFkU1S8bwxstei21xWNJBQl2uqcwuQfgqtMwATr4swi2AIWavjpOHIjK0
w9H/ZD0mXkG2QhSITZk69pJiOOkPIwLezjlFlEi1y1LwXyIrIQdFenYYG0NQPjF80x4vB2Cbuh/G
4tF10m+yUm8fropSMhdy0H9i7437xjjSAg2NZDNnKjsBK1QlI3Xw/RDjyMSfeW7GHItSdGeOpwHX
AK0gx5ri11SrC0H5n/ZKMtA3kdVZMLm31IK6TH9xbRm962oqZ5085rnBiFW/BBKDrZ/iIotD7fvb
1jJIEtjGTvJvuViv/5Fzno7AJum4Zu1EsTUfHW/oHAQtJgbsbvybX3C1pANRrKBxVTpxQg2/W4ak
99sgMrVwsYZaiLCl92LACFprQYQGMrJ8FS5vfwuq9+LRCS5mLKAjX32wxGEQQ7kS4y8D1sIlpjKy
VjznIjxMN65d3qtMzv9cmfaTMrgXwzAty/r7hCns7QHjtAT3XB+dPDft6Gi2cBFBNkXZD4mqHrrs
ouRmMPjiqutpd9TFhYaQkvqW4F1kFPlSMkDBr30xoqBGWcix0V9T1cA0NVgy6LFVFku+WyD3znIG
k4UwA5IBdlH6vSCVVFZHE9IRlZF+N7Zj1aMXPd/QE/5CBRTDpQVUqUv+XMhCWcRegZpvlPtxu3aJ
cMZqK7/FZKnW+y45PwbkKh/SoQvyr7s9nssjku8ooJiOz2Voei7geqS1WCbJCh3wLH22JcJ3aHuI
2rS+XBoDlCt+u9firFbfiG0fw3dyu+mOhVMPhKCugDF+F3iyudQANLQLiKvtij5/O81qvLbuFEl7
EeqoJEnmFbsj+DIrzevTzj+BYEj7icqQz7IIho+Iz9Tk0yI222vvOUO+cQnqcldbn3ZWrizD+zpf
1RNqY6R+mfyuaG1OL3++2qUdRATeISR7lec1ulsnjpeA1aie4xV00+uwXW285YEGxpnunjcHEpt5
89TJAMI0q9BdIZf2NEVPq1jgO8R5JV9X8RvJGeXO3tbFzgf60K26gKFdacM1hoUgQKkYrqVpDUgv
36hw+UIS5V8V+J5J4YRrChmYWDhK7TnQpxkWeVfvCO8/DONw98fbeyLtYwQ3igHUX/Q3uMj80eHt
AQ+TNvwtbiPtNf5VHYC9RO3iSIgxJPgKO9Zx9TDh7m8NeeiJFD0aNn8xIjPtQufUPeYr8tALyou0
PSryHQLWjeLGTXhdLjQCQ5oaHIJLLI7GId7pMDtH1nZhAZBoA4G+SSBpNraJOW89ElCgiNCXisjy
ollZhUMhboHZEuGG+yih1Xdfq3sfjBpCfkM0KUmJdEHZyEAbefq7OT+ZLMkGvd8SQLzUE9zJDpHq
ezeml1CAKloHo5SNT5MlScjZh9p0ox3H7fJRq+QABr+CeUZzbUTiC6Gc7zJAIUy/IlLdEdj0xiaF
XMrO5uazFv5VZc+bDsvySfuIszgEIphulVgiSobn+/JC1DB6ANNzfmAraPie+Ld1NdP0pGfbzB5y
RibVMTRK4du45G+qsFmJlE/OXBEcDlc1SsjFKBr3a+YFQYMGmqlnqnjMAAv7puAkfpn+zpF0xmYi
J3lyTW6OEL5IWDP/yxFTzx8Llvncx7AHvSMmB6zXTFAKsWgmBR9dPieiSErsCIeaB/VobYa7qR5b
LiKyucjKZBAWcgL6SJkRzNIe5qQu6YjbAFDLKoA6O/hH/kuU0IMouWXbM7VEPiYb2L9cQLEdV3yE
3UvQOf4gb+SxfhxDK2yvNQHNPt3kTROLsvosaNtyxQMpSxpKXSdpBVb/5BC2qEi8mNMxf+vXHbD6
3Lp3coR9RSS6VKHVmPfVDvYMZMni+rz1ec1ZCOdNjQlU2679164c4j4aoPNwBlezqL/lGp/35QIa
dlzdiIt6mkhzwYPaXVwiR3/Dp438SJo9tBvIVC4ux42kde4LULMSYMoS7hK735XeJKJTyOSAU2+1
t+LBorwMY8WLmBXszrTYN6kZmttK7BJDWkPaKKAwCvWmA3oGUG+XgisiMGWxeFs1UqMmk8d6iIjy
ioghJImPSGFTv5GFLvXgnn4eLApKCNuWBrHBiQHDSe3c5Iz4/q1MoyPzHdDFokvpTRb5ROXAGR9A
ipzQBaV9tcBtbib9d3qFLc92d0ME2+x+UxNgH0EHlNUrnQsIYYl85mfW5YwjaaVIxJP0Y436s53v
3w5ary6/yG2JV+tcuB0JsyFvptE6PaB74SZ7O1OTpuTD7FbCLYP02WyRwSL+xXdp6/H+H0oV4Qxw
s0pvPwwFpS/K5WtKKunL3zASdji+kZ1JJeXQR5crfMpQuIewLH9XbQeQRJsA9hMTBZ96ddoi+0+q
yYbORIAcD9c2Y4hGZl16XtlT0VkNWAcOSfzzo3E/Wv/vSCd4uXtIAt6q7efmEQswYZI54/Vmy54J
1vgmfHyzflyWpmBJ0m/wSmkuVzdU9aYy/rkp0tE93w1kh6Crq55OttLL6H6XnmMnvza+GJp+ju0G
iUtFYyynrIzcmRtDxM5QegeZGtJs6xV5hzdGYqrIHlOVJ3Kis9+PHTuVeaObmo9WjScit9A2tWSQ
nHLuZFt17lOt5/DqiV6iloOHdmPtW1zshkhQi067BZJG+h4SfO7XQUZiCF0r4csEYZXkLOlDGhwM
W53DBAZTdIOGzjnWWQOcbVTDabHxM/X5toUdSIxCuBL8m7Upue8oyiWmml/dqYaXv4EV0aD2+bNl
FOulfWEAA3VjtU/vzv6fQEzXcPI939Jf8yOdpfoGCXt0DGdomwgiILvRloJHqSp8ZS7xoM/Qm0/e
DPxsfnxlIM3bhEptiPDjq8odR7rqe9S355YlFV7oapoX+Df4Rj4LZViva774InKa4k+Ts8ZQx40I
F0cbJmqNk0XYD8XZPsUgwmvo59m7/KrE9CzynE5R+evwPLMFA744Oop/cx4DoINiQaA9AKyA2cNq
schvxVDoRkZDVhmGV+wedE23aVBBpObRErcP3/KfG07ozSUwEzP6KOeg0g3c2kU45tHn+PveyDSy
n98txSUTY3yiVXwHUBgZxdZQLqEFRJJVpzwdUPXmx/fyL2Uz5+UBezmNPDaUXWJJgcJ5Hfgthrlq
4NuNdUXNqzFzYEqZp7kDp1BDAlKQExxJRuhcRky+Ovriewybk/6MtYX8STId1mYgPVUhzkm5Ljy/
MpbI7rhi2kIvmMlCCEFax9dxBZQm8paB9WN55Ub3C0bKIiGDFWMGlOc6+JRxO/cir6BgI9hao9TP
alHf1OK8gNgzfcc6Gg3OlZ4BsoiGJdMkDGSHKTUACmisDx55473AbGOfa9uYK3LmrfGeHMrkNj5o
3yzh9pxsKNuK/9vDqjUYT5trTInFR1okaOLAsu6+Rzpe6gtcyiY1nc2JdgP+fwnkxL9+E567zT5N
jR6fDV00A8DxdVRfd1418GMkH4MgQNXJ4br5mjzbQsXF/a5CsRIF3D9/zaUL4JELxuO0s3CayAF9
sCnVOuKmxCBVwjIYi45ciINEl79sKH4DcNab5KHnnoyGcs7RzfcQ3HV79b9m+o/AtiOPg+WjMdxg
jnmGQk1LVg8AkLohijFseCDc0IU5TjGsOHXlTdmoxra7Q0ouswiljCUik6WvyJ9xIsted7rO8A1V
YOiuCE82MYXMrW4id0XAubgCM2RjEqbSTdxQ6aQyC3qEjL/2HxuC4ifxNhuHr4Uyg/g0LEsHQ9rL
mkVvGJJwVPOPARAoOqdyCb7JGmDFog/hhlcl1boOpJInDBPRK+3lnMFxNU9BeXEwMd5R2kew6DZ6
X+wUMg8AYtEEyu0ngpAfh68HVgm+UIl5dMIA4izLP3RkLseLtbYcAXn3X4ttoE7yNKxUqZ15B+Py
ncKi+rhTFMLlzLe6DlmZydp5Jp4IhXZo5P7R4WgvUnKtO6JzTKTd9qaIIUgd1kS4CDfDXnaUYYx7
z5VvCsDn6wh2q4qdXTzPvTYQ7Q5mApcJXAI2aHXCyRpWp0XfLB925JquUNlOF7iBIIFx3oxBU6Ro
92SvJWqJ799xihO8SgP+uSOMFaxVlv+1MsfSq7g8OmeXlPMp3aGWpNMo00LcS3x7u4pTBPMvxW58
JWgMrq6OhASKG+VFpjqfrMsw/hf1O95D2dY2gsMQWv8hbAA9VvFBxZNVvPpqgjlP3NxdYuTIa50f
yk3GiQOjAyPq0ouEj5hH+ssvUrzDmK8K4l8CJ+yDYpmsszVqWvA7yAk2CZmh1v7RXbAbpDUauzf9
Hd+EA9rM2ZNW9YpLE/0CyiKLWChp1HV7pNjy8kNIcvbG7AwtgV8INFnIefZEh7eDnGZPlzV/0c2G
y2v+EVHqzW+UNvPGbFUW39VgclHgk3S4HMCOPsxjrFL7kKjSgkr/oZiXPpr/8v131QafpPt/n9V1
qd+2AuTdurRdBAsdz2V8+VYV/va5fMxSxpZJJBu3lflZ9REvmx1zPwjAHCvXKuwtA2AhJAtlAzgg
0egtiP3RMo7FR0eawOC06zycPPIXUWj9tXXG8BnMLm2OQQs0u/K2X36McgZw8p0SWX4X9U23Ij9q
6zjEde0FBnDBvy9GgNMPW7E8BIUUMKZ3fgchsMBCP/tNBvZMQhfBR7kkZN4uZ6rIKKDe1VH5eEoy
iYoJp1BgIgN222E2tosIAMoXrWjMAsSLH/mhEvSR07Og9UIh6Xs0YmdZxxgHI83QrRCeZhf4Y8Dy
G9r0WNE5B/wq39x2qdAPlG4TD7miHl546FFKKYrefFDfoaShYvW2D0ZYUkq/aICZh903zEvXfUyM
IFeaTHaG+4puIj0vgp87Gs1BBkxdXEHfwKj6adiBnIu+E6NSoTCrZHqSHk3Ag92f4/bwsSmCxyYO
tQ9WT6+EgM8UBDHFhTowEo0CN+XiKXIjFEyfX+dcoy/iZOjjNqknygSgqFXEeGao+cINZvQvR8Pm
nIYetH8M0veFwZaISgYREDgTqIM5IB0JtFpr4xC1nUd/S4xsAWddDfwPQzAG+tLbjAl/wqL3jWHt
tE/Ys5y/YtfrDeMHIRplxV73SjzLYcU5E4JtiHLkqrq9Iv3IK41RtxJDyenr4ylwz+jlEN+4JnZv
VacdC4lUiXRRaCoACT5xUGJPCgLXTfPtJIxqEonjN+/LyyKcBol3x6E7hnqavPe5rNsTvWnZQIwW
7m9CECzejXZwi1RaRD3lOB2d8N80IvzL1L62v5YPxaZ3vuiKBkgv8l9q22+tqb9KReUurj7bbxWP
0oC+cUh3gYbmRv2vHDcAYoz458iaZ++um3YjJ4cSiXkfB8xxmej/zxvDEreCO6HmFPycz+kgeOR3
PsFsE7HgLu34VSP//wmPws2wCSwdzEeJUmuhZ4dBm+lw20LaMs0yisi5QwUSAFXV7D9+s8ZrXlo1
x3/yHDDRg8VXTEwpPFivRL1dxpKxY8QNkVkhanT34brS/BugUBXV2kAlILj2C9yhAYXmnIhcEcSA
eBti4AyPXXXqZpcG4eMh6HNNAxb4NIpaIXfPkGHdz0dxsk9k7YiMvfZdfTie0NFIV7NsP4UgC7+Y
MDAIeCOVDQGAQZTumEZJWfNKbFuMjNx0vJU/7XBnKyDdHN7pRvCUIMGXSl3gCIQM2FeNlykEnmwN
1dknsxAsUeFDP+U+6nyzf8SEWrI5G7tFbBHEV4DtyEvkDGsacHoqKK7fLdTEdeJvvhGZjfn1Xs7P
QAgeXuGUFwI1aGlJ3CW28PTZY6HrtfE3QvbLzeiuIjb9GkOySfKZpXJ7dbGMcPljziwgdyyPtWvJ
fsjFPCk8tyiC79xf0i7qV1DzTl1SSB7ScUh6sYE/cVp3bzeGYPzwxf5b12RnEamJK6M0qjiqpYLb
UTV1ijLF/wzB5rDQEvn5By78/dUDAKcp3pMeSWPOea4nP4aPemv7rGJXQQNRLAUIguwLVUeCzkFV
GF7yK2gb4tajkaEnt3la+v7exmvHOjkxv/67bEIz5M9OxagpJ0mvfMqqneyTyuFTGsa/Wv8GEMxS
nm4HNE7cdDazw4PuN4q3ewpBWtn5AExzlstSOjeewKaaNUG5DA0TRRLDdaESK+2APupUbZuRuBML
6A7GqDYovAv0Mp0jhJsjp7DlUfIDpuYcyE6XHdQhyUG3Ul2LG60h6CwfcSgSBOLPT9GRjO50MT4t
kWySRTO38dEp00coFJpXYQ9MrdrxIr7jxH00wZE+LNlZqIsrADN3D1YaU0M3zWnTTGwqeX4Ta7Co
j3jyeI5m3b3Ej7xkj3AjVTYt7iEVtKy9/MfEPXHNzbKMWrw8JSRkeoiwATcFA5k65msD2MNqinBz
+jp0lvfMy7DitGxKigntUXfADf9CwHRiyWm1sJGQ/XINerSLSRO9xqmzKeTC0sx9Pahnj/XlsgE6
jN3MiekWDdUPyCcKUmnzfNPLlJBKpHEQKewJbD/2L4AeC6cRg+rg3YVhp4PlKlWoRMXYOTxuI475
ioXRpx6xXz25mJLr7yV7TDmAzPP+xKDVkl8VwY0kxK8yPRGx9SGKQGY137AuvM/PAVtA9Jw04Qyp
KaijzYDIGQ4cBfyOsE0xXea/xm6YHkSnDdJqBDSYbGh4c02Kq6+iYERbuTz6a+fgP0hvo5gk0A8W
XSnVpTosDQQHrCmQ1KFEAiHQfPurPwWylUcQfSMx9puf6FEZ+zy9LBLvwv312lZUv/xqdXvh2QBo
QQQVsnw3LID4j1nZ1D6hWpOFWXFhPORUD2LDIau2zAIlqaDIK7Kyl4+TY/+/WB8dS6i9o9/CxsmW
WuXSPLg28AVeeKUpsuGLwPelmcTxqlLj078MfQgfQOXvK6YW5VyVxwWOKXflOaZxfLKtTtjAIueh
KP42KXvgNwJlDDuXoBvbEK7jCrTPmkWCDEzHFZflRuMzz11iLOoemRw0cUSYCD3+SLD05WYneuRn
y29qL7/P/tookq8ebi42pybdmru72Y4JbgwtIpVp2U3gf6Nf0L0ZywXDXdgTgsnn0VHG7fGhHH4u
bSlMYQzBmax6p+YCmCI3uydYK+6f4i2TxSxuuMqqSfKMC3SBvsnCXTjk5n6pgINgqZlW0baJbjto
8QWWc372jPzMGPpJC2CJjiuAZleleTUj6HOvPfA3jbJKOVb6zMIwxtP3iRIohbDhblNOaWhe1iJz
ek5IRURuBCzUB7ll3kv/+EK3MdKPqJQZqwkKZ99B5Eaka42ulOCJvVKxeiVaJsSlqiFF+udyQk1U
65n9U2qKDf/0bnfrhbH47vsHwig0qqKvBlv6/HtvNdgQSI8o+apM6Q2BC6PvOwFiR4t5oiMEwY05
L0xJbyXCJqkOo84qQdCfuxb7zIZiK4kFI1+aMlUy+dTQx689fZYfkz747J3Asq+N3COSfwvkfp+l
k1EizvShSRnARv87uC5lsUPxvhPMJVzdRQwNC8rpAT1DA663VuNxvn44VJdFleRhGTuex7PiLYBP
wHobwuH6O+nOw6y7aKK9E9IpQHcgee+c0gAnJsA0jJrvKh4gMwoEHa7GoL1C6zWup/+vADqq+z+i
XslcsatA1uJFxafMeeeBV2/AcjsheqKZi0npYUdH3N5JUx91ycNjLkmg66kP8qxqRcrGaaHCCdaM
JBQihtlaKJihm+Rrpc9M5faLyi83fLAmJKhQXKtA4SOithLSunpy6GcUOQmXgTjWck+n4n5ePQ3w
dDDMWhubpXxwg7hdhMFXsnpX8D7mbd5PkwSjTaUTbFpU2db8drga0mbGb73vYYXbF6dWvu5j/vgP
q8bENpbfNS7mb3woxcsJ3P+VwrNfnGE0GkVEs9QDVq4PKSeQelnzkeXG8sa8glrpmOfubMH6iBmr
pA8Z+jfO1Wg2DKWPs6a5wuyVyqm61vNokKVp0c7wpinrF5DiQD9GACqKS9viV6Rv6/IKOJDAWLUt
dyw6nug6fOZGQekYY/eBWU/c/cN2axh8r5Iex8aYlbo4fZj1gOtA5FTeG/kfw4ncNSnqHaJ/e4jG
MujSUQBEOmHYGF63TFzlN54ImE/Q6kKKWRDI3OOTYtFio6FUlYMOOY562i9KfZ3iSpQelZug+wVA
r4sX1thnbAcDPbDOUCI3K/FCdYSqTd8MxJN+0HiOy3OGdW5Z2elhBM8z2pfJARzZMyJE8S7tPlsw
StB0HDyCVTmptpdq67t7ZObaxKxYUTOAh+2iXWu5jNsdDzqsolxmLJX3CWUka+gOfC1LiTlXnWRl
owZ25tz6wAp+xcyiJaOyfwqsRa1AWomYeMrrQvwIuQDMQei6qX7O35GbipzgppD4YdCOmZVaeiKm
tfMepqYfwt++h0Fu3TSw/HzURZX3GSsWo9qcCW93s78hGjImBnAnpHD0GbES0dxH8V4KUoy/XJGq
xtQsYM/jHx6K1uZw2pJgQy+H7SlRnFqRmWbfK+DPz9eHIAu1rWbshLJxCmeuaEaPwH2IQQep/7oR
10IXz7T/ub7dzFuKJa359wNT3G6a5B2JyLVqy357sggqfwoXdWa/i2DV4Z4MUHXeGFoWv4p/RX62
/14nqIUG1MyxDLTTEisEHdAH9F+cEH7K6NzFoDqUi1hA3P8LQ1xcCE2T737b+CSQwhX3Q3Kk5s15
q/FpDNW7I787CyKcuhymLSDV+pB2XvK2OC8xWnxTAtmckCIWgFqBVw3dDyqM4L1tDVXNWFTrRJWi
RSsFd9QEcsQfnwOobKcCOB076aemEE3lCkPJ88NygDC1s97nHXa6dfxT6CUZl1QUI3z/kn2HQcrA
O8tX0gq5Ach6IvE7gS5YueEekbP1yjeRF+4JNcPvb1B3zergOCxe7fPR3mDqbntRmvJc82j3ckFm
DudtqDn5//uC6vJQpA+Jwr4b5AiZ8tzgg7T7MNA3Fgz7TIm3XNeA7oxBX8SpWLgnVyPHDmb3LGnr
naL7dPM1vvx2ExYbAAwHgxMs7KTAk2io2bhVzB/40a8ijMKOgsuoqVOnmbFkaPKsSD+jf5kbtQM/
ggoj83O8NyosZKzM36+ZiWpmAWIfJLMhi8IMoCo2mgyWM5D5EcY7R+0uMpH4n/UyYs7eUwn3EjvS
gn5rV565j6L5SqZlKdF45P9GV9+qkzPPYyKf/RCiEbPyLhtfkK28bDox5i8yjrPni3CEueE6PgVR
fL4FCZFPaSy6mh6f3jgMLLnqQBVYYaC1Sb4C1n5Mmpd4H7YNPk0qui+NHlm/E9xgc/sVhKHYUq23
nixjGjfL+Vv3FXQfFSVfEW8zQDo6e9NeQaxMvwNWxvASdgfQpwkBceV6WFb5csrD3WRAt8eUEPi8
K/1y30aEURSMnL88K4KvbYR6Jkl2MAZyxCorSG0ZHxjbLdNHtEtFx6fF9mliuJqOL97gHpz7H6gt
e83Kwpjl2J7FhWnjn5Bhy26YBKaMyRKB8BQjDEujOeJ/7t9G/x1Td0vYXmiSIgE3yjov9vdnf0qQ
EreFwgcoO8LV1jBLijCSj9ikI5I05i7F4UtmTq6LKFH6vi9ZdQbOoI+r6afSvYczXr8yBDr4Jr6v
GK92B06kG7RSK5TH7GN+kd/saOQwq40ywtZMapFphgWS5Ppl0sEq5tje8ZQudhFhRCpOYwaNPFcb
ShSqVCk4rRDhSNgo3Gf0H9m9d1oTCTWHVxQiBsqL9JuBv9uM+GFSnllgNZwQKEPFme1BWlgKxs7M
PPavi4X/Ce3wyeh//IVcwXS8ENqM+flDM9kD74cIRPPXmlne7Elo9LfIFB41q6C0USdL/cDAuON1
HIsVgOnqF97Wo6xiemy/ElhFunHK1fT/s44J0SBF8iSHPu9YscDonFoPHWn5Bk+cirmmoh13SjVg
//5b+BzOi2eKMQ8FqXympQ1dfQTLvAqvICUEytNGwzQrx7wZtOa0WpzLQIPMTsGfs8Ezg6hocZE8
6T3cnCOLrYnYN3wqxXGagqGy/onVhXd9/m8vzROz8KsrKHZV+H5uRkWSFh1uPmhNALpjYUXtxiRY
9zzfokJxPiEUBUwKkAwTGhK+21WqhuFjewNJESVJA+sC5yoVkmk4ECm5HRwsz3PnNrjGEHdGCNP0
SAAEyi9I+mWnScqeoE1eDb+/m9caaLzkcdH049ugVP1tdnBdfaVpZ5ABvkILYFkh2QmigOhkrk9v
j38ZdzSBg7QQKISWAKkhUyhNHB9F2BC86WUgoc6ON7Rm5cFN3ni+chkQRc5rLcI0vrBkTxkD46JA
nRHxD+1u5PYG9vyk+14Cm5r+mGPyyKbIUF2sPJ6LYEZm9hlPzS7d+djOCkcsFsKJW9Iz5umJNLF4
tp4VImeVVokQFrv6qloerx4Gy3+U1nVewOL2WosuuMd2KWqiODoi2zIkugqx0TyozoWKKOJnYWA8
XjcDM5QmgZcr3QiyuifwKXHUqKt5B0AVu6g6RayhNHjf6MrgFzM8kbvFsCx7sig2He9IdtWaOIEQ
YngVLimXafE2qnLxxXadJZS7R65kCDsiL8g+pSgVN+/HF0Q42c5A4P+8dtgZBtAe4wyXJd2VqZVv
uYwoWF0Kg70dQbDvMo9iD7EHY1oDcOLCVXowrCDSVWy3yHaojLOIQ6nrLtHLxn/GpdaHvczgi1EU
0H2VddL6IziHaOcvKc5cITKUZfEdU9Ui5E4OAlBhDD2y9xu/c8z/EnCSevPgF9CamRRM1yCMRLBU
dJvjTjAQ+4FM+lOJ10W0BBIIPPGG4g49mDZsN/6w1g0C4KymLGTP7xGCiixwRNP276dRP4PeB6RM
w+GMWG1bPAv9KCElOJq8grsJNNucRTsz0TU7H/uXBT4KTbh/nZkhNq6bvzvl2DA+mkp+i1pRp36H
uzI5PgOIOy3Poc3uXyljDiAVjz3qAwR30VYErCUUMjP/e4JeKRhWRaC/EPbWKavh8w1ln2Ayof7K
JD4R/3hLx2y/KTwFdiRH2hHiMqeD7sWq3dhBK9vqkVLSz7LJSdAAeWzGH1TDfB9K13xks20v4S8W
eYO+L2XvItvBWvz0PjsRTDFc0Dut/YZ3N/RTeMZ8oAVipCFUBkyvWfRhS7ldcHrGMGMXfDZmwjaU
pIGbh7qrfYkSNaVp/XktUeVq52Fm8VKzzzLFtpwurTJ7m6bs1DDxdiDfyMJUUST2axWig2jOtWEw
mokpVtPPtKFdgWzzND8grgoe/913zZyKXh1hWZ4imHUi5lHQ5r2YdwVrFIDTej/BLuOf/nxyC6ia
CqeuB+llSFpqeBE9laHvEp2MKRx+O8JI5s7sXbdHAp08uvtxBf1iLEhYmoc0vDvUgGJ5shkmIAfx
Ev8wjXMk0lbl0pwZ2lDalntnwRe15RnD/E6k5uIXiuxaYsezOUn6OAKKhJuEat3b9EM5/zkbWnpk
M0zgiPQJnyLbOlTlKpF1cJOvPh4JJYVczopJr2hboNIRdAZA7OywrKmVN0REEKlEhAHMzvktCZEk
L4o7g7qg95kX5z3K1Y4k4A6Pw6R4JuEg4SpY3Ufzb04SnXeRcddgoq84Q5FYZGpje+/MKLIojo0K
ILYZDQWeDBBscRc25dxWS7U+N/Qwjd1Cm7cKVQJjGyAiM7wzSqDOyDGvtqBDXV0+zDsBG0eME56P
jU9Dk1aJ7qDV0+ItiUOuQOTCDszW6Pl2JAk5teOeU/rVBHS64yqc+brDFCdXZbkCctr9ViHJWy/L
u1ywmHqIquBa6B228bfLZdxALZBZ5EsrYQQqyjiHnOrdLJQTb4PEyP1U+gjzglDuu0XgVqQfv62z
srD2V+r4M5+JthaJuFYVG21h0vU8MHcNvUexdH1UV5qF/9Gzx3Lw8oJSGMKT9iM7h5NOwf8fUVbx
xTS57bmImMatZSaUJb88DPLUwS+gjO1PZ3Js8Sl5CHC9EixKHyPWD/ymlWSLCdlZjGFFShoJQqXn
aqXIhu0nu86C8QIu1sY6FMye+90r8xY4QorgZoWYFz7EaHeMcv6aoShzjUJ7fzoYm6hohAqfe8g4
CuOin+uyNtkkvf1uaffjwY9vkqPiBuYwU/7k9dBGLALv9htFUiylJHSylgaVBGWCG/R2exF5hm5r
OyAjrubu99Fj1widxk5+VVrlg8IRNnwhs2Tk07/w/akr6N7j7Nkn3dvGGUijTgUEzLUauoJ8HJoA
O9SVy9hx5i/+qzc/BfP4JG90jLnyUdc/Oj+qa15xn6gpFJpSruNVkwRBG1Xw+Za3mf3r+i+r5YhF
CFAF4mbwYm53i3yHlLFmj8pWjSv7mipOQb+Vxazw8ykX18K8ay4mpQJ8JlxImDH4KOS+hX/NI5N8
CIgT8EwamA4ZHJZ40S8YPANKaAYCI6+/lC8ogZiVl5YF/1i3r1fd1Idtvutg+OML0tScyJnNxnhS
lP9msczcD/Be9fYALOpu1xrUtE/aNwB/kv8lH9LnzjjFnzsHRsWeS5F3PW1LdBvMiAC/DEG9Mfgh
il9zlnx2XvYHdPDEjxXjHxIg/rllzFdLzN5Cb2dhmHC3TnvKI8wTUG73eTWF6BntVwdYZ1phK8X6
7SkgmEzy6zquW/htszWJzpnurMVYyqsrtAy1uHsibeboCE4pQznN01TJsYV/zKWUkIaQZ+wVP0+Q
n7gRJygJ59fuHD4FMnSKV14NenwnHsG0yqa4EhAiPvIyb3eDnhL9aIyC4CAiaUNBieRZ65LUUtf2
GLgj+bx23mD5Sxu5+sCk+D/CDAMsqbDn39pU3uOXbQjOpiHBrjuUe8V529O1FHO9Km/p7R+Yv6GW
+dttNEctHu6PI1Whh3MrvETAHnomQrIEEwdgxMP+2SRYs+jhxePiXX8OCH1DfvTwUr/TiKXD3lNY
ZE+1L+1nXVruOPSiUkGJXaWPFHAJZDBty2UxAY9SP6mACJqTUNMkKZrpkmTTLyWWIcfU9ad2B8v7
FxrGxfpgarVMk6P0zNMvL3AWb+LiNopw6+4BdZ26Vf7fKad9FkA8lfJK+1Xom5joyi7uzG5S5hRz
8oMODNmz31dTgk3KvgnScBxTivRDXoEgZ+7+hnmRlojHtVHC+Ioe/W3Yf1koxJhsBmy5DuVm4agJ
EpB2J3YXbPS3OArXmBKkwJ9+c8yurQs797+efiUa7tCxvPtghQFvZD9zr7Yn3FKMtlul+oZ39BHJ
35Cm5ZICR+Zt/gPLEfCcyF+34buXpSEXRJo/ae1/XqPYKto7JYbW44oNknaeWf3PazMprp1ElXYD
Z/9ATmn/zg97L6zZnkQryLz5D2bVi91MO4m35Qe13TZIpnwxzM/Ii20XRcI1R3URNl6GuA4r1sCJ
ngVP3XFHZnPMoi5yx4SCoehWylIu25Fz9f5fQaZqcvL4g/IdQ6ftfGcCLlukFGJuvblBhsRiRbRD
qbSEmix/XASA+eLFmNFoiCxjus6pdllQil9aUBhLf7XdVnpYwdP30dgIe065+DioUV1ksSwY2WBF
wsqMJUy9Y5iKqeaGbRsH+rJA4K+P0fTF5JvZfiNrqRRxYMZpu9GSvc6k92lSUTDPkHoLtyM6frXy
SucMkc7YH0t20hPMDs88eC7ZrVht927Zzbf3yLo7GczkcuaSFUfzltGmy/7lmwus3NNZR4emxKaY
tHbdTiMDJeuFDA8HjBL6sRLUbw+ygBFUvIGm3g8fP4f9QFQbnb4UIDa+gcIYi/E1I03Tkkq6D0Kl
Y4Ap5uhQmbU0u6g24wvjXnhEOPpEcbGa09WF1izCyP8y076sLl19lZYDO7wkVprI7OMNTxKhh3NS
MvNWTC1aFUQd2Y3CFPzfB2w6TdIMrNR2MH0/Si7g8kGgtjpFabEIZJHhItsB8kAii7mVqIIWzYTH
hxZFzSQ1sy8aHkaTJn3V4DeHW9sYVA9z5Ov7NYSqhU745TV7nadgO7hoVBigX4SM63BGLWG8TXAn
tKmQZYIiv1AxGHnase4IRkLal+oSA9LNAF3esw5eH9A5F4KcZmFEWmPuvT4tf2OwbQxVIeK9dFg7
ExP+EpzipAFYORdMigwiyeP2wXgTrdk/5XSIhhDPGBvnikNp5IZqZxy0821bDFX+a6LcM4IbldNS
14gbTS3zVHnsg2Bz6Rcg82pgk5Exr/vz8CvcSaDOGqYtUyGC31yQejabs/7yM67+nkn2GxpdskKr
euBITkT8cokcDra4o5T/IPXB0kwxdwiJp1DEF5LRD9MvMw21PSIOPDJBR+Dqai3pEoTnDsMNXL03
T9YOlIWB5qVvyVve7GejX/DQVqK3fceAtTZJlwpvy+vzfEEVV02mWn6Xm1nyTEuaNIzx/hVT3mWW
7m6U8uWelAFY1FdZfbTM+MZ1J8GVih4f9X40FVEFQwkdJL8zj5KlqRtSr5nO0btwmBn5nwH5lIS7
vwCb91Z8QW30muGaJ7lWNWgryJKnC9U7X2UF67YjuHzMtA7kKpva90yin2Wj1gWHxPTWrSpyWzip
QV/SipamrwKFHAxqs9FhWw+sYCZSUjrnvKU+FKL7QuSu2S65ihRewsGTWJyBgExJabyR9eJP9clZ
eiTA50yVn4FvZCs4jPbmNTTRmjfLp2ITPxZOGZYKs/zmwabseXizVa/GyyXCbqMghN9i5F5fHUoW
j94oMp4u7sZ9BenSDZXJ/9VRZdIPPNHVCqX3cP30gFgE+2/jwi285i8wNIoTQKcOry/6L0Bqd0Mo
6JRyDS/TbQqHqPFlnfQ7/tt2uJg8cq59yre2lOuNdjpHZkUrZ9ED6SZpPi0xW/5f8XBKYwrbqSEw
Bpi5KDDmeMsMtmmljbUMCSQLDCRDu++T2UedL5TmRdN+SDrdI+rJSSVzOKqXWCxiuRl2i5bEYuf9
E1fIUIZO4KauABREnhDAk+Di/LHEipbXV1qoWwyCJOuiL2GOyCOnGDzduZHjS5Y+pY/TULJDO7XW
rbuKd8NbOU0Na4SqHqNnBi1GBOeU62wU00ktrTjNGYRdWdylafSLhBG4xRI0TcQ8eJmOf6iqysSV
O2KUv6MU3FltekjNvajNEeqBURKGPYuqAg+n+qESviucGE8hCUkD54jGY0AL0HieNgSKOVH06lpI
bP48uvf6RtLHhPtogfKgZU7wsEccXEkopd93WXHbqFuF3Tq3ooDE76xOaeS/lshjKffj9+CSfT04
1gwcGRQ5/rPpI4mUmrDbz/OAFIeqe3/OzlecHLr2UY1f0EUkInekq/GHTE8V4ovgaqYx+Qq/19UB
4kQ9YaNm3Q4j+phx7RPSltNSGWXBhQMewn1+zpTUIzCSQ56iqBGSnN/mKk34Mjq/7SHVn0WGAcu4
nieUO6GrRyEYUTc4uj62/3PKG0CCpFrxNZKrllqs52DWwNXEXaX2w9k0OKmroFXFiV/1DgXS5eie
2WxPVxpX1hBXbFqGnr2FN5PmEj4d6QyMsoDvKcgD4z+/SyNzIvlO27Il1poyqV92ys+t+9K7UsyO
05m4P4n8XlVjsbD0nmYkRMe4nE5eIkoIQDWP4NXOuCuLf4B6PiEZz/WHYL7ZV+Z+V4rFNuIUOkyg
JrkNHF2Zc9fdv9g+L6Fmo92Gyfik8K+qU8YhBBcwpDaJg3pJ1o0d27GHWEtV/NQ/vSiuMNC8qKGh
LZrciAkwTonDZxuzYNmSZ4V9DqBoyNQgBpOisSxb5Do3XORcJ2LZ0oDqjNf3fNeeVxlYZvx/Dh9t
2asWlhaMR0eJKa52d00LBcSO2vFZpYcvNIXbP/PD79GOAVNcCnbGORrMDAc/83TkbxI1z+wjIqKM
rNE60RXtNEfLFytt4+K72uxRES3mQu6JvDEXw8ESCoHxpwB7As3EM7bB0NvgiP7zj38FohxoA8AJ
uxWEPoz7cq7kjaoqnR1iQHGfbSR7BZI4442QkE0jkTpVGdj+LLG9pvcH5VzhmECUdzZ9B6MMPJxw
O/FA0zPxTRaN90V0pfd+WVs3y7UW84dRhPtOfsw8fkvBGOT/RBZAfrSegMI2wXAO2Q0HSUvIXBe1
QjZwoM8zNKkfV3Rqkv+xhldIieSVIgt5LN7BItjmhVz1JaZPJru1v4nT4By4XXCVO7VgGcHehptJ
0Or7aVcZN27RfO84uoqxJRE870VvKo/5jZSpuuLNuCmNBEiRjgmPR9MPh0Gx9fQ3PsByr3abMeuP
YIzUMt/jAbljIFenFCxgQV2OVTPbG2Esr7qA6xt1GtyzZBvsl1pQhIxTkddH/19I42zRT5ZxaJCk
YQV3vuv42HNe+bVyxZBPgPGGtIAMioh90s2z5l95OmlSWDq/Ox+fLpLTiHFVwZoZQ+Df/a/+65np
pRSxiqJ6V8UFc4GeRtIvObzEWwb0ABBl8nAcHM1Q61Ho/kgUSYsOOde0APZfHCaIKnZvBfto1neN
hGE2/gvtWTm6Bh0otEDiX77LdV2DA2Pk93p33hHLX8WuHl+19eq5tHGK6ZI8N2q0fZUkNu2hatSx
wUhaNwvnJAzY8kawfU204x5jcw8ubKbg+4OT9u+UmkyzHtNGDmWW/xBDi6kfSTMypZgUV4s1KyeB
r8Snbf0A52BPupfrOyZYSAog2hCpPwdj+buDBauOVelTYH0YjultgXfiDlo48kds6N3EvWi3UIqh
sKKepinsme4UZ8KuZndnDFOzn5o1qDKADIp+WhE18iZQKLzh7E29+89bDCNXLJhO0yXhVsJ7CLuC
vTV1Inetkg9BbUdInUcrszsjYsCP8V30fBzzSt7nEz4GCPVVwLxU2pRA6zpaAE8VnFgR8++nqylp
Sk7jiz9hHXAPLG2ZYr5an5EzeOCKhWYCNHhGgVB/JroJ+hAxaYZA3Z9Qakd70zdnttkdD6Z5YojS
wVTn+ErRATRE9NwFagsgNU3z7HSzDr0P7SrTUGN3uVem7YpreJdYtElfaOQNcV73Sj+JVskkUybs
n7eKKCAyf+zFmC4FAweHSKwSYQkxxiDgnGL5LRuSNYDbiXwes51P4QCOGlZiZRBCXnXVNQzds5Qr
n522nsWzauj2jR7wjfMYcHoGsDPDuhB7I5CudQyv9+HjrqEUjSs78TAjnrHWAmZ3yvqxDuuyhMnp
0fjdcEzwnqI8yWYTjnGRHzoP2DEmm2s/xiWMoFELoyJJ/Vr+kPDPlxcvQm5/3N1PXQ1NjtT6uqhW
nykJgWS+hAHlmxH8ODiS0DhyWDhIPlvd0fJ8o4QmB2fUIkr3O3a9mCaJZQvwWUN0kGeQt9/35vXq
5dIuBfx0qgU9yBMi+1EOYnItCVLP0l6QbG/8yYjEnS+rBWDAgUEHhjszSseQHPC7BpWx6mNS7s+0
00xRkoZtPM+A4ZW8mpdpCeEDqEqec4/O1Hm9xYk0AE5KVxK+GUDVGrYHjyAP9Z+lW3eiraxzfU03
TwLBsrQQfLFOpXfJTvDhJoLAoxXP5jJRVWnJLoxBZfxOeAi7fe2o6eSnVaZF1xRh+z5r6aBV1BV8
/Sz8PY3lCTvFOjvBB9WHb19IeA819KUt0EDWnWTf8zvBLLLoW4Mxia06BSngumfuoToD/s47386o
qHi8eXqPdTBT8c2oH/qpE36R7t5KKn104l7gsk5DDz8jKnMQHzreZq8CFWFJcoX8E4POTC/1oRMl
zAwakqeJIIfuz90XiGWVEgt1Z6CsSavcIILZCl/HI1IzjUX3ekK8kH7H9KBc9PDsn7BtItY2Ci5b
ASevu8zHUhsRkAoH8JWS1pr0U174cHA1i5y32Wjd0HWw1qrCb/xEHDFjtmI+pVpGVt+CA1XM0Qnn
xviYlUdcL9GZL9qjqkSxSqW30leL+yBRzIUgPdlW7z7LbW4TTEcl2lvsdArRWQJ756C4gir5dzlh
DZauJjb229YuNXXKXK+IrbgiuB2KXBMztPDMzQj40qN/Xzwl8nuI9oscWzH3Sy93Ul9ykA0W+JR1
aolkSkOLhW37Em5EkuWanlAu1Y9utjREMExAn3M3z9OSRP3fAWqKRuZEoffur5MCd3IMpdpbUiUC
wF9iGuVvdmpW+2pvtEdUYso1vX+/RPBCEHnFN8puG0yCxwXfVxeBdMxIG13d5yYic4SYDgVyEZ3f
u42L77CJNMowuOzKlLzFv+kL7FwPEmf8GDsCv8MpNrU6qn2jtjxvKa0eUqBNyOrr/24tPfdu8yy9
sKnh/WzRETcbpB+aivpvytCJwACiHQteOB3eQKxUQV85zr6UJ3Dr9AzevVjozueQE2+HA+oCLIRP
794vZFOvAB6E1TpMk2nMqId10BceTpA6DRNvNcVUD3ONJw68zR9LrkEMhDVv4bfXIIQbUzU6+y0X
aZptMkORhHZlfP0mQ49qxoTstezAEVBPMogFcjND/zbu39nhXc1DDTyq++X1Fk+ol7rKnDMvA5TA
EjoRyjN9FKuApGXO+T8Fb5KS8TE5hX2x+kqx1lOCiFqLTlOn6YA7H3C60S2ZtQyUa3UTggpg+vJ4
LC/XLnxTxv8dlMgiAyazIMb/0Q4fDvrTXmGIIG6NJpdkqQ+aGGbhUWnYe8jsgTi7Uyw15GukdaGK
4ifQzKOXOx21KNBwdnIWg/KpQIT2zzslVIxVPCsH7PlpISAD3EKvr626N7uEDOlXlyGtg1CM7+rY
KtPam/UafqTqq4ruKG9h4m5td8iqFQ2rc099tlmTp3rC38WmsNVClujU0DcAh4rTGHaPI7RYZIhy
pKIB5ztogGyUHmqHbu+xNYW0Gj1hXq61XThM0A7xPUxzEhwC9O0q3/1gb9gSqsUiUayQST6/tCOq
eqwShHWhysw0qXtDh6UqnZnaYzOkaRp6fSWLehWiFmhEwF76IXiOf4GkDz5N3PdZ/QL/witzdq9R
mn5/P9OS4kz0dHwbDH3z/rH9jJn+8RHQz+w23oKiF7ksofnow5N/iyQP60BE7mVmh+Af8aTSMz31
TU9KMKN87D1XUHHEiLClbsebaFr65z423fwtxynqCCQtZe8Bk14+nIZHtd+hnVKObP+ua9leDUFq
2KUoUafV2b5iA+F/iXsD/0kloG6LjypeROQ8d95DbBrrQaw6iODwCgF1Z7ZJ9L627omyx0Z1R9j6
t5daO/mLzvWzZ+5zFmsJz6SgHbjLLywAV/gsWz1/XNg1xDfGn7Lgmnso6fYEL3UQA9ObSTx7UxpH
0+B7R1PrQVoJ1C0/1uPVNl0odfnbJd2l5CMHb/blCiJHRkbFRhFWThm7G6ymtQ3uegrUHqiLJidy
vQlRIQTD9f8L5Zy/0+UEftUE/0XNWy8GqfBmrBR7fj+nVjhzL16ot3T+LFD47u1DmBrnsSLpaj+r
3Nu9TDGH1A3tPbKiirNAcZ9QFRSPojA2G8Eh354R+U+RY2NaLCXCoi0fzveb4cyk+mbVCZf1/6TA
JiqtKO3iOE2SUoivQiEsE2i22npP1J6ZAtjBh17nBIgCvqdJmaCe18jqZscW7rFwqLEVOxSw0Z3c
w4Oh+SP6Q9wn50Y5yWHRUNJbLGV3v9nAHWtiazXGCNKcT7pnSU76HT/6JwAfcZj/+eotRTTaClc9
Vj4NP2Yhm69pLjErQ+NeeQuM7Dkc6Mj5jmlTj3mDAvsTNPttS78hGuz38a3nD3Io9A/RjjZ5hCHm
wiub5U29Rwnb/S3LwNnDpw+Y5NcUwpzEq7onPhwc7eKO/Ci87nzbCq29ToV/tBVqu2ywaCiZaw9l
x9mIcKgzzup5ENK3i5NIykkMxfs8Zehj+TdWMy19YqmE/bR5TFTV4k3y59rQDl8qWqN6vehUMwJp
WiXiOay9nCrPHrpMDi0Y0H5G5bHInR7CnzoYQSmuUuULYOnj2Wi0e5wgHnFDMhO32TidYQUGHnsO
NKVxQmw5Bz5Q6Wo0G9IcEBJNgQDaWgoMBu/40Tq2lKoeA/I58UNJ3YOZ4NVnrVSZtoZJZ3wreuvB
zso0zdkhkAWS0t+AEcyOPhV92vR8ClMaLvNJ3Echpgvi/m4+SReRMc6qAfF5YVkBK2NG8x++/1nJ
H44FYMKD5sJVrwpfIopp8EDMWGf51z10gRlNwKghk9hhE1jjQVZVElVBB0NdDvWaoPcB9XhZN+hr
UNib4LeZRErRZTsMvl7shNU1ij6qSQzr6Jw++Pxcww5FseuQA1BxSGbPdNOlU5qfk1l93Y/KDGjW
iNsbsgihbpgDwQdPtUjeW8q6yuGN6/xYN87/yKhEwl4JyVx2dmNg4xLAR+AZpOBCecoAr2MUr/1i
vdWZ3YYvGtnLakfDzMJWUZz5IjevQ9GEUX2D9/LVtU1UjkRJFyiRRD+kY8pbugYm4T6vHfmfDxFJ
qfF7gQa9PxZtoBq/Btv7R36RjaTaeac12V5wG3H3FTHv7aRcHgwSrKrQkQ8ZqFxcZ2XtSxj6kEI4
MYTx7P1NNQBhoNU4fLEBDQIz0DDnYk2l76l+LYlCWRq5NaNslCsMLYPXAcsg2pohh5j+ebyn7Tg7
sYSPKkamOE0hqJfMrZOvHTyvbDMwBl80clf2lVmJ80c4Jj4RvSLeD0nYmD5LLnYuaJ8W2E1Il3xb
Kz7AG54Uw8XHAcsB3wFMxWc/3luZC6nHbjW/1OkDY7QDRTlT+2AarfZfH42Vk/15HwQHmk7w4g67
Iega9WfjgnhALZvXDDbAFcVT2cww+9iiM13LjGZL2jJspj7XTIO+CjguNPyqiBZJzcuD03/+rG7E
re2Kh52tTKp8Ky1YK6jRsN7lIJFsvGDz2bknCVF//r3JxVQNhCQaU73xSHwlX0A9Yhixvoi8vLE5
Wud/KC1V5w7UbuEUOtK0qcpNZaE8f0+OAH4Dr5va/424Pn78AQiYpAx3G6sWx7OIB9mpHDjsyAcn
78sEepepRhoQhY8AZ+bsFNO2ut81bCYJyCTrVva4LXRgSmi+5qevorsovAEX4Q6chbB2k7uJkdcc
6BlFW4c0pH2G/aymqxUuRpLRa3MF5P7ZjNKuubzlowd2HDNETA4KM1kImNUYvORauREHnqKbssFL
L+5aJyAO58YWC9fWyYXVOEb0g8qxCiau8TdX0X9FvD7gyyRy34WMNkaxjWAX/QrYsxb3Sy85Nk+q
a1++6TJFj032y1Exqj4a66p8jtnBtP897QMaC0feL1Nnidkigez9e3IrirLHHmOl+HtgMqF1HS3w
d6rp0AUItwqAZOPrxxgSpuLO8ET4zTKMcc55RQcgs5BiC/EyyfEcvVoizN2BBrsfIPXT05pSZL24
4/LQBVJSGVUsXIs+1lCv2vXb2GbH0kHSkbd1CXjjiWKIfG1FCmYJPZ2Upz/e+HYGIdDfuJXvKeuO
NauldeCIWJIv9acNZwIRuIgvo1ISt3LPTuS6JyRJkLmEptgltZNsfHWobUP6R0KhPhvhRcgu1OOs
OG027yYRlaXaB25vRrFRnGZP0MmJ2sPRrD718F8P8+3edsb3oucJ5FqmlxyfaqkUKHrWIgwfAVC6
cg8ITY6JAWo76PXdkVpmE2R7DLyaPt+efdlIC9ZpDJ6WXCjywABCjuUpLWxwUDFa2HHuRqu7xmBo
lUDLDy8wTNRgFPhzZUkqM2ZymbMP0x6UHUfg1U1+YI2v/j7Ku7cAPG/Od0QKXkr18zhzV/+7ODKX
nFwBXxO56vfWX5eHc42aVthBClsfUxMTD2Fu61cVKsAx7oIpv2dcd2IF7FcskFYs5xlSzVRf78vW
6H1sAY9PMD+pLdih5yTJUEfjiKYndh7fYMaEHESZe4PcPc/53jBKPshqTc1lGd5Pv5ZtKAPwBkw7
yI9q1h9CfGmBzmpSf8HpWoaJFiA2w1StHb2D0LVhCpxKeq6hpfpW7ZlPFXXWk+Zrvw9PIscBOdp5
R+L+QCVQEKpsNA+CWfNrZ6pdspH5XnVdbtuX87bZoA9c/1hOK/rGYReW7X6n+m28ETvRnxNVCLY3
7IleSbKBftqv9isOdQkE4hTMDbk32pmcxr0IU6GiEZD42jv9/CnfurPS1DojKQiB0YvNAUvvwm3b
4bao6IuxxUw6siZ6ADuFThm2brg/MJzlT30qWHAUgfeGFNgwRgXP+jmSGAa5qTR8RFjSwfwHoZM9
mxAHoyt1AXi69wcX+Q4K5274qsOiIWBJV6n68xumKRKd
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
