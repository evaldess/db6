// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:17 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ sr_ram_adc_iddr_sim_netlist.v
// Design      : sr_ram_adc_iddr
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_adc_iddr,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
XHE3IrNUR0rAgOSs7TaneZOCem+xKOaVUndAgQMQ6fiqQ7sNz2l5jVXfMEx0J1E5drsp/vFpyBfK
us9s0XKVnQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
iNP9Rj01ArmVzHoVSW7lElSGoWnbQe/FKLklfFiFiJRRgWHkBTgJfwNby6KYAgA4XLe1eWz88cQS
FukoZ18JES1Zuf+KwL8zwISn6iD7iixfZNEwpWFYjyj8XUfUUjAVZiCjZg8f5vwPfWs79Kh7gZBj
vgDcYNXjxLehTwCVO1I=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
nmobDEi1pust/app0GNcoN+V8y2mMEri09/oF7dQ5ZiEiG2p7rMxs0iS5vx/JpQ6fiI0X0AJUPZb
worjx3dSanWZxlmpvUQW1C+LK9h5RA4c6zjOdaM5qZ/K+NCauMad2OY8ZgcddQsrreoTh1nJ2DWa
TaZPLvv5pf3U+x90B55qP2fEPiqbYkbzpATAH9u4NTH7sLWgjc2AhgaoW5eC8oXtXFv8D/e6aVTG
z+0zADy8vVe9/EfQm/dJ7Jg0DqAR5qYWGcVn7yVF+tPiL3kEf6FJZBjo3JgKIu+qAthsglm8Cx+j
2KVIa2CX5Gw0SJbZkMW71N8rkZU8FopYgshYqg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
sQodddsOwbYSlSsSDMNCYLeaJ51uv4v/ftdtzRqygsJNUO74ZhxTo7+viqM/zY+gFJjqy+vyVh6/
lpYCCvOfPW9ohlsyigMit+d9OfUAHtHOnSwar6P7DvEbD+534I8OBinFHuDcHnDIFirvT7RdkfNd
uCfMWv1oGIMacpnu8DitSYvvt8DCB+bHlF3ijp/IC+P6O1hD15eQnQpsDwpKg6nnVcZHA+6NbT95
rwOncIqFR4E+wPstj6ayfvxsin9AXJ/L3hE0nmxedSpKDKOwBjtiGDED3rRIS/N2OZSt7dsYgyAa
MHSfsznlBT9CuauHVihH/u5MN1losnUyYm2/QA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PcTPY1NzlVv/1miCbWVLH41v6m5uRKf5NQUVNklgE08sx21KGWF+V/ICQGqfMvIC5eom8kSFM2HQ
dFf8l+zO8zFaHEcwmOu/VP5gnGydh7qelqNx+0jPz05q2jp495ez4dMFlOZ8sQGQEzx0VockI9xn
YjRJ00trguEtLmc6trk=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lmC9ahCx71j1/ZSeKA8Rkt1tIlMKGNu+RHHj5Xtwh0bt4FfcPDS17km8+8ppXi7OUTyBXSIFrdK0
NooakhmRZCmMYOTdKwnxgk20HqIlahm9Iu+bxjgvH97W6T5jJcYvFslglttPbZrvLoRpnSfUfQT6
o0EtaHvsEFdvL9+ScRUKPku8EqkOu2Bw/VZKo9IMnl0FoU5KXba9O59tKh2rkrbNw5L2gwOiI4hj
K6KuGhkZNMCIC23+bh94VLvhhAbeZ4zYdMXlsjm/BFrp9rW2/KEFj1X0Rlmh/dk5PzuDb5p8oOdz
YKZejj1J0rHlMYssmi6qnwXn/kI09IersaxdRw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
anbwWs0l97JVYhigoT6et3H8TOlASkW/Y/8eTKUdRC9TcUSfTU88XxtY8yyw1fQpzUYR2pxNi2ri
ijWnRd5cdXyd57zrFR97a5gvOC1uBQO+VwZqLcjkcD+uCBspFim6ZUmqCQtPaJptG7SMYEatmSeu
5AOckCi1UQBo3bcklZM89hRwua0b9rPBtFacTvBkGGMEj+3Kb+3nEBjrhaIJyprIebvMvsj2unDq
NZN5AyhAJSQgoJgaiptXgMjTKV1UKRQ+AUYG3Il2upp7ugSL5p+QJ/8P9M8v4jzmg6XOd+GGtyl5
iWC6yFcF9Yjeui98q9M6xYivbpBmKndva6F27A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SEfonlyNG8YAcVnPx91iCPk8borIGPaWiJLZAjQ4ei/rFpUclmCrmdDaAEKl2C6egNjlAS0+sjPS
Y+zDUbgB1zmvlc/tdhSobfHENw4E7nVpOiO3LpH0RNW+vE5gVHIgH14HjipI+MnMpA0WPM1yKTc6
9vNke9I8uopfYKPwA83sQD58OW6+jvJsOUI+g8qfuRMbZKYy/Y+NS2tS4ypXR8KfAWW6gdUxjrnw
P6T3WgTbG/zxJarG4sORWn96Yc1NAiD44AkpnonzeL86+briHkw7CsuzAVLHENNjRtcIeC4zYXDr
LMlHg9gcMiK++n43ZX6hfeV9cJnsZRPwcJdMvA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lo9lKufC+4lUbxCisEYQ3GipTP95COa6tmahcp8LSG8DdAWaHT60LT7lpmYwIBAutlJSIqVJnIHn
qUrADSaI85BggWmFFPiBJ9l8F429HJ2/9X1wD1vQmQTxvt/NBuo22uXQ/9tVB5jGm66HwdD7M91B
vQ/PxfdS7joZd4HlMEsJLq/DbvxI8yuhcPiR9juvFHiU66JL+blx5ETQSQ7BUFQg9UthtE/ZNgFO
J3eLiChOF77wzbPzU9J9Ypvm/Py5gy7KUuzfP0RlH7s+PK7XKwdoCXUWxfvIJ8LKfFQP+lp1RpWV
4tEypdUV2MqqFIbhXuNGlk4AdOtkcO7Vh1IvXw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2128)
`pragma protect data_block
5O9+tVJGuq6cGGtYmPsMmNbOg5f1sMGGTq86Qd9YMb7NypoqhDIOvksLWYT2IYbGFziab7X6cL9A
HnP2bRCNUR52hW6oj30AVC+8YePiyVhRSJ5dS7ziI/cZWX49ZG/fAf5CRhtz4VHRS2pWHcX6QTdt
n0cSzUn91ufgnDO0hT39xOSYRwPdfbvEKBRv2R+tlRyPFb4JApD87aMrL83ZZYkmJ+/UT+TkDssE
siNc7U5FjllcJUCg2SabrOhLAKphtCKweNE1ciFPQS/5gSoknI2vlQafPVHe0v1R+kbz5YT8ZHq1
FFpAoupKsduVnQ6apYk0kHwRuujAElA4S40AzOZZFr9OIlW8uQQzgg/lM3BWNeU60CMOM5C0Sseg
D7+Oyd6F2wEGWFrQhZ88i0C24+gngMqKgqe2x+wcIEwYk4FgYWR3a4wVQ6bQVOTbnKMeej5goYwU
dXN3ekhVDOyfY1mMBKLxuHIBSxA9/bgxCkNXGZqH3X0aPTIMGbLlKgOwfZk3WIihKGW72hDt2cJe
b8dNvTc/XvC1kBPSMzCeKgB+/ZHHHsd2lMPUuHUhQyKBBhOpzhancRdC/KS7kFingxzuXBshHUu+
RR4QE0O/H/zn1upT7lYhgVEA5rOsTcz1wIqeQ67nmI7hxAdmX1lPMEr5kZR323Vq/RNJWtRO4+TT
RxBdZeeoSjV0lMrlCd6Y5IFBoaYH/Wa4YZCk6EaWvDz8LIn4HJeOSJv2ogpnP5xRhbN/fYPJPqo3
o0rjuIKuWgWrQXkmUtoS0/ijva3OwZoChCYjMPphrdUyJXwN8p6oaJoF6/lepMLOCZwSjaRZJzi3
DovxYhfkv/7vg7FD6iNVK1UlgaJROGpTLuBoeapWaqNgnw0Uc4vXQ4vCSs8t/z/mGjSf7dKfVK3H
fEaH8V1bCzwAZfb8snIUcIsjlktJy4aie0ujwytjTvwafX0WUz3c65TebnJTCsEZMb5mRz+B/HTa
CUtaHoQ4nlkGa2LtrS07V5FO4vFRmFjlqXhGVCyHY5eAih9IIkuG9UhEW9rdYNNoNVqCBFfrsPkg
QHnwkkgc1RbRPBrcWsmZxyIHc1zbIbHLaXSDqcIHkH3u8IxIvXT2YgxJhIrffQS/empLaWhgfOih
ynKyeslgK3Uc3C8pNq53Vf+nUkQGRPvjh/H8pZY4mTN+eKYHI0VcXs09pJAiB/dVjdpJe+cTmKkD
yxeEHPpiikhmTIl3enPmV5fLVgihFwIzNhvR6vZt0/if0y17sxvfLstgnnaBmVUuaqACDHMrFQ/2
QJwD8CVs/wtZEXv5exqY5mAA27V03i5ZufkHP3RPEngXN/BuDxm88vMkPoVRM0GcPw4vSehMjZML
pSMSyJVT3LTgTI5xoGAPqxh5CxuMZ22AozjWyE7+kdLbi+/yA3DnOSUmNn56e0zKEQd2emjHVQxD
7iDN+qq5xu82kPuLB7/GAopGMworiZhM2Zh+6P/8VzV26LzBZ9s6rZM6sS8zHdKf2A8DjphnvF3C
UmXOlFTzoztIQdg6Y/YcH8Mo+joW7OKKKxyL1/k8IOUTAJrAsI9n9u1JPeeOLFf+/U3MtVjR77XI
cGP8tn4gqpMthIHfHaOxT1StfIXUAp2NPAGGfseIb0QHc2Mf5HjiMg+cPc6w8iuQXnenVqEmKw7w
11t0aXa0HnTfU8TARAIS7xsSMHDugRgRWM8SrC7TevzmjQKoOn0TjssgilhKsFJcXPCm1U6Q5E7S
8WrLQMW3rPVQqdZKmiPW8hZ5gumybCLCMMDiiRuCXu5YYE5biFfX4QCCU+BMuKTMSwVvcAAaQhuY
sPSExFYY2xDmrJ0X0eZzj9d8rLbAYF2GWPNs64HMM9DpZWqy2+tKZDG+d6OBDBhB74WCFpLdroaf
FHmf4MJe0yk/lRa+fPM1Scu8EH0pjQrpBWJQu69vpzTv9xp8iQh4U6gR/+8rix5J4H3PAgCO/RDr
WinS0yM61WhPR834bHNBXFHPfkHA1Q0wuD44XokgaZAEH8WAeyxW6Q1+wr9INVJENoNLRXzL4zX0
+tPU46Cw3P9SPni+0BGnIYMWyYrghF2sikFeq6PbjuZ2GBkZom7oEUH+mNsNf6kkHXRJfu2LUTew
UOqRb9PWklK5F6QNs8L6HzTNc7dLqnnJDvSV3NDCTUHKt+nkoatpmE/fRXSBP1Yy+VKmQjoDHRjd
xc+YDwY9Sc4b+pJJdGwpLS3Glk//VOCBi7KtSlE+GKOBBbupTXKJ3ipTd8YaCBASjydt5cWA8oeT
YrJA5CzbtbmK1+aN1zigdBaAcTRS9zKckzorYctYepZFMEGHfXrVrusR98/VkYG3DK2JJgETUx14
54Hv7y/j/Gkdv58hVb0M+UCIHo4V1y/RpjDhZAZdF7uEhF9ARoxzUDYRMZ2rqlovD3IgbWy2gWJD
sUmUwFVykuann1N19grqlQvclgGIXcr5wBGD4N/JnTdKvBtMlOBKM91+syk2gyDg8Eqbhx4J796T
3wo7HcmxW32/ZSpivragGegSdJKFci5RmLsAQIFeKrDBKFreOb2rnIkChp4CXl1I7I4tHUKSZMeF
hJXM3GRim+Sa1Kcb6Y8USO0zRLoaXgpICADM0cEOO+PW4L/Lz/HedOsMPC5/kBJC77+jrVRxRJOT
zErtQBml/jtFf4lZmVq70rZqvsosyfhrAElYjlyjYKQPQ9g54CYlkrUAHKKKNBDxsOr9cApG0R+m
mQNhAzCquZyX/fGn025W+O8LYV9mYMcG0D7bua194bAvzwET5DppOqYCozKbi4p8Cng5l1atd4VB
IYWTtxdFg5NoLxh56ZAh9i+j1Q==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nwI9apodsxWnt8/qZ84l2L5r2ru1rYRvzH+cIiU2LZ7ZFrYGVhrKUku8GacxvPmk04mNLHGAUf3D
0KN1yrZ0UA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Sm1hR/bXnEX5hSLJC+m0q+qTo+GE1jW/bGh9GYODVR1B61WO0x3DI91rmMkLB3jXabqZYmZaVRnk
N8AiDf+w3tD5cTm9k3UfnHfkmqEgj8LBJAWCYHciLWzjmW7DKTQG5Copg5YaoAmLrkH/R11p2QBq
US3uTE+2f5z8QlQwimE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
y/EngzI5VWuiEHV+TKhmZG2qH1QkzhsLqS3InhpMlNY6l/FsFenjJYgIcwfRB5cHNIe7FLSQt6Ne
y3HMmpsqF6xetN1AMKtt7yIa7k99d/5TC5vyU4dMYs9g27cqHYJzk93esgZCvjIZLHpcXw/tu9/b
4U5FbTjst4GUWQQ7e+FOVWa1BC4H7jo6ZOE8mZ1oMeTUDMRBFFBQWv4xUZFg+dKul2euXKFScShR
h6tknaycBcdNbA+6dQJo+VgrTTewvfrkpNyifPBwk9vIitRhFkJJJVGsR6T+AF/UJfY5dEYYFuu5
J288ggKjbjEUNQnIyNWOpZiuhpClTTay3laNkw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
htKUMvAlzdN4BbAAeNmEM6Yr1UUCORwvd6+1cV737AnX/e5QyMGFY1ZuaVzrrzfIKK+VWd/bFDYR
WeL3jKvGUsyl0cMQ9jcxLrsCI3RnUD8yDbbqyDu9KMj34D7UA/k879CbEg7mJQsE/OUuwmk5Rusa
S2E+UVp+HrYNnNymuLmmn6wOTCKRZjZEMW81xyRvJrDTTqf12SjMprM/ubdETBwwiEzoIwLeibWv
EE77NEiYVwYpzXElBkB+JN+riXCrervjpMbAzHbeomW24pwXmffMMvkj1nRzaEI2QRT19Hpc4iqq
tT7PSLFxC6iyyFn2bd5a57kSCEK5ZaaxszxEVg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
ST+OORnrF+3QguD7AuqTgC907V9FPxT3xpP2TfPbwAQB2+m85/czQ7xrlMYLNRNl2qldRPC2JAtf
yRLJmvKEgyRtR6tv/9gg66CdnvMVGbBmprZnmsgKpHGXcIGIVm6FR+ifL/5pZcFZyTQCKYlbE6bz
YNrIQ8EskAk5YXNHRZU=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Zz8HkbKk2BMn9pYqHdEWEMFHnKjJed8tZnBzajqsks1G6q0CzbV0FSYoWS1nKj84tIU1JkBaGDIt
9sdF4TFidxOJyhtrmpNfTChKxpMr41K8vo0yCOwdi29v/VShuI/rkIBCSgrdlmTBWBEgiBS9aabp
Jqqjo1ol263k6jlcp9rOjaoU+lcQMEXCkHoZu/V2+VWtTqhoSiWKgDQ0jJptGQig3wemEM16ctGQ
xX4urrzlEYCVTlr9g3mn6x8NgAjEFjJqmg1uE21AWGXfsNowkj2dYZLCXuVTF108ULXlOgx8TBHk
tPYc56T7eylPXV3Y05Z7agtvOLTYldGNSnm7qQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VHzNHo3jyVixjpbjlcbNuO7IrIjCuYoXTAjRb06/SIYnbUS1pXATLQwryf5S2ETq0CYvThlIAGS0
xbNOLpEIhHMaY4VNrUdhUPBHXcXHWUCHudYKaUCB/Pk28QZKLuHYt3FqZh6wdzI6AFJdP/pykVJb
M/Pyyc+uLtqsAqyWqtJ0puNrBSpFPSM5259v7Gum4dwYGluRNUyJPq0CnQOQDcjaKw42cmf2DAtX
CSJb79mvoLdsFiW5ePQbcfrrcT/FhIkNj4/DqMVl2EB85zQgcPJw5Up3lLGw0Qd2Cd1jeq3A4qcf
LraHhfdfhy6tS33yDqFUeXlzvLfkicvxivScIw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ir7vg+6icGbLB3CLLO2WEVH7p5OyaYzRs27g9ktjlLGEA8UZWJVD/LEebYJEdrotzhB8SWmHZMDV
/tU66bmEBeBvDhzPDFffP8JEne90WI2d4WsOz8gc/qUmQrWkWWpKaGeRzRKobk6HEaC+nXg3PqfM
0b03fbE0S205+4xE/rEnuHBIRBfZd3xmeVaB0HKBt0SGPD5SSQQZpPD38QOtCELjuuuA4RtmpS90
kaKEHc7Je6wpd85YQOJtbSfSfwms8QmBrV2vuYX5vgvFoWdrKhFu6ei5xOtYRK3gX3JKdEXLebbV
49uISo0iQ96Wfdc+51UDQD4Z2sSmPF/BKuQ5nQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LpdRmMYH4gdKs52wqPlK6TsP8t36Rz9etYG+uFXIxoYPOw77GvCpHTnPEq4wgKvtHfjSBYM58T8o
VFR+rx+dgG80Vv61h2/ALXu7WMVNRnj432YN7jUfiNGlmdGjYf7j5bb6jDSZd9SGg9hOG322ua8w
FL0iNhZ1+8bqOC5DHZhVoYhtH7wentMTqEBB4I+Xy3zK2H07hbY20A+hZ5iviyCzHMtmQ5LCJzAb
8LeBnGRdOv8ntIJz3n1voQKFpamiYGRWqDwIHC+A3vf0VlEiw8M53hPC9SjoIQqQxSnkzTditbkH
fDStRcfPfMIOJ9yoREe7QoWlh0XCwpflnMvnNg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
NgDZ1UiUVs3rERT/QzrctuA7AwYQKaIYmXjoZAGYg/n2DIj0PppaP2+2o5PCcWC372dJ99ilmixF
oTVdXEnuw3nzteUsxOoRSZf63Prrn9Y8knBUjNi0xgPOPnF7/bgSNl6aW9aHUHtqlpei4RT9BN1a
SSgPbtKZz/00rSoBNdUXVPfFMpu2LluLKiqOpKtSTfcCVBUk6ivV7uGYDfhAfDDml6Vi7vqp5FTh
SITvZKvOlRhpCOh9tFeFdgUgvIZO34ZSE/8xZ2XMljrJxi82bEHIhcCyOJXma2qZL8EDjb1Ent3u
5TJAUy4EutMed+rAWlHUve8C8KZqb79+iPAiNg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
2AM4FWwxmD+rSWFnGaFjtM3Yy7jWeD6JSuh5yTOCbLRe3LU7IkW1Vx341ss0inLv9k3lGa5EXlYo
Jb2rk6iHQ2OfG68YTIaJGur7d2g+lVQMegOdE76mSucVDH9O/nG3TxwbISJY+HajtX4nDcrNVHFl
/F76gQkQDbuDWUdZdl2nc5T6W/+uSel/tWBtR7DLKwThyphl59M8ansF3+VD+5NTVT0x6y4sDNtt
vez2s9SKy+7I6X/+YEwvMsdYhrx3Wt9ooQigwTI/fHgk6JvtcKlXhXMUapHVoIcDTRkFYbt5ps3s
ry6mKFmjEDLXrN0rtQYbDW2FkBolh1TQIlHxdw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7440)
`pragma protect data_block
ofgK/RpfEDtujG3iZX7yCM6LV2Ju4hzhxzhfz1ojitTFlHSCp+a5Xkv3E+VuysEo5oS/Somb6v+N
avgoPmVBdxare4EdMigxESCT3sU9ZXfKVep+hDP1Wa3OaWr3ZAsbAEXOiSTeh5A99PG/bJbWPlz2
Sln7YfHdHktNGBkMlDBQgfZMT3qZGBVbuRP2kKFzCxA898mPVu8hvpp/CxCewHdyq7xTOamnPBXt
eD9jpzUbZDoQi+Sqzo+H+s3k4CuNUzFW8aztsUA/4G0jPS8eQrgc0mnMMrvW9YLx7WR0MMWg3FJ6
Q2xl6LhOPAt3ETIZeX/5SA1FyZQErWejjvCs83qF0zWAS3p19r2xEyaU64FCLz8d/cJpv2dVHb1+
Y4ZoUQQ1o0VtgJA08erd9Y7UnCluV/DiFgbAwbHLHZ5R1Az7L6ZekzjbmzZWvo0Vax0QHl6MlSwu
fRDriAAP3HkRjD2Rec1T/jZT/rbTOipTrvJJo0pC9qDiDfNGHyU3aqO0xOz5mIb+yDYeAK+qt7K0
PYiXE35CkqfKYJeD4bCzjXG3WSqgvfBfeWNnnVsSyD1dcfWwSNRtrUf2L4+07DSrAb1Ly5BLgG0N
65VkohRrbmdmSoA5suPGNKUXolG46wwvbAohbTaCZD3NvoxOGfwidm+q5s+qibprTiv99UyrTR4Y
TJX7FcL6STmPjJDHf2zyfesB3QgIxp/5y9XLOAK3axdFZUxP1cSg8reyKsavcqd9TSMucChU8yt9
IcXTaFc76v24WUaw5HeejekxSr/xqMYU6zSQ/KpZiAbCPH/SeoiupXMQPqeC8UiI3L9IxW2fLL8n
AEr1pS762DG6XET6PH5DNjqYU1EMw0QRZca1dxV3Xt+P8k6mweh8jQlON/MOqgQ97LPBsUaOzlJD
STGCWcjElMBGp4ZydsDxdEpWtJ5u0cU/xgwBJL1AOhtOpBnbkcZqQUoFFBdEZ4vSUJfYPgMiijLm
83rbpxK1N/LcYQKyTjbl6YxsQNrUxj75tIEceMkZLGdcsIW8O2+GpeOzMnMjtlIhpVYgwJRiwcw+
FGXv0WoyFprC6m2s/kv1yedjQM6Y8GVJL2vpclh5eX7S3ZUU+zpQ/PkwyMn62r3TNRPB6O5Lf/fZ
hJRVDse0lWblN1LruL27X8Mr6jF+agGy0c94BF1q5LN64HBLlcQn2mDn6oKSwCcNWgpsO+7Yn2X6
jnBEDFWdsBrFoQ8HkFovSuYfqvtBKUCTMKJcuRSKiY6mHhqQ+6v+6ojVabhQbuPVm4tXTNbctsVQ
x7zO7UWOuLyRFiI9yXTf/VWFskbcvrQzzIOp4RmyrBGWUfnG/xovujQwF1KK020dKbOfjd6Dst/a
G3xywJrsHhn1VB7QHLFrNMpZcs7Nh6Vnlub2GyWOFbhjume+WzuCUyV4/7+kIqhSpddt3ebGgulT
z/tLuTHJ83uZ/EUP0R6hwiCfSjqffeytH0ihLSR8tt+YGQH3UO1NB/+HWUl817IbFPICgo1xnQNz
CoIqmgqC1ScyPQ0ZvFjqvJi4kyoF2zJbX6aJiAZyeFLgvFIQsx3pYRwZf8KzeJDfwf4e7R1k1qdP
X2+4tiIsrrWxPw1CTrjKiFfO6H6m83EDGVOWHuVpkn3LQCzE8GrUm3ciKmi2ErtcZnUYqlJ14Rwr
9tSyl1JmInFycLfHjLLpqiQTazivwmVlJvUjfGmBX9zyiuBBniVFrR1NWoBFUp0GXGT2N8Nrivzl
9o1/9AKe6vqlXQimiOa5uRx2nMP9nulLZzH+mzwGtogH2+ra9aHaVm6EwxfL4rxhu+fjnsW7Dj+P
1Uh4evekt4vr7XcVqerOD8A5TvuLgKUlCPTbD1iK4KF+PyrVJ0egOFPj+IyNNfNVUz6zbwGwVMfq
XUzmidM7GhrgSmW5Jp7k5QRiVmr4k8QeL4DMpdpUuFwIsBrZZRvBux+DF/oOOlwpcPzPezPjvVkM
oQ4cfSCVkX3KO66RpyD7KKSkveoBxInEX9oVdaG2ESK/2fcITZRsd7BqNM0LL8nzy3kKdWbB7giA
c9BHgyFKvPlB0+R9idzZROmgxrabK5YCt/nKOc1ZN7qnIlZVAQ/esJCQHvlP3ffIN7ukgWJb+ruv
slWWtRDZSxTmuHqa6GDatkwmlTxqV/6s1mcpvtbi2K0sbCNfa7HNwKIwJGsQC5EAb1c/wHBJxng6
0BLpQqfHyw4FdLRwP3Mh3BBEDF+6UygpTH4KgxdXYulxrEjS2iKTwcDxwxVVMRzWPLpu32SFjGfz
D2uzcQDTA6utarXnhuBgZpLJkudfLILHstEtGrum4dmSNkP/jtue7p+7FVfwQrFP3UIqLneOVMDb
qyCRobLF1JwpX2jHFonQt1qIJH/ZtYG6/X/JOVLd6RmSb6q8AitLxHAyamO9C3jLycvsrzfqTjIp
3/ZHCT0558GCeHvlCcNH0OmcSKMyR/5NOx1jxEU/egKPL6M9ZNVnMeQqJ7KwBCgspVlqXK64T6ae
NPfRqkfqyQEuWbJp6/p9zshrWLpvhY1taldwL8ApHErTKBZJQwZ9t0Xm6QEqcQZYR6MUbk5NXqTX
ZMR8t0wiUXEXOGlGeFZVmzhJL2qBlYq4D6fe+eOfDmWW2aP9jHaxm6Tk0cvsZMSExg7cATHSR2TW
NX86aglAXks6LfjzlyMw2IGZi6P2tn9o0/uB6lbSc4eR0QhCSPfN7bPuX7l3ut3hZllRRwWCPuBw
w8sELeDWoIppg8y/cxuUOoPiI1VzlH31+s9QH7tiAAGBIHa4km/zSdJEIMKhwG77PzI5vCk2MJ1h
MGKhOTXManSdGqtK5uFLgleL1HrGBmS1XqfSbRTFOwJMWcpbElaGP3fT8Zsa/IPu+FkfVgtVbtZR
qO126ZKIpbof+6lcdDC2XZArxbTGzWdlzAMEbszx7bpMrBFNpWEuVCcnFsGMXuFN9fKrJHU21R86
Hz7STOkIUHJGfN1MfBAP4MM1Z/dTIb6O4IXMjVl/FpA5T3uALEUEUZiCztreDfDTyjekpqtTdee3
nnwFFhULTl1nRftJXjAoMDd63b+OhrhEBlC0yk7jZbRd/F4tDZkK17pnE/4gRN1EuCYvJl3u8f7b
RR5e/ccO2sUCgf88xjG3N13iwgSkCVjuSjIhUYmnCxEIUVBJKRd3H32law1BO3XQhAXXgekEP9Hc
s4Yf/qGDtPlecHf2wcDwsTaTWbHRgoP6IjGvq0I42xtGC6zNKeByUEyuNeWgk9swNy+jdO4URBG1
zPUDrXyG/RF28J5LCq2Gzj5cc/S4o/4eZNai/WTxJ7N3nFjJ46J4ROrT8K+ST+BTpU01pChik3ps
FCaomDYtHaQnWQlFx7njam8SddyTDfHKffhvahlVyrPBTvcB4b6zKjaAIVl4/XC3LnwueV0p3xmK
M+odTqtq0iCUFl/p+PbmmTvSKHS6P9r5vbImNqBLHDbisEwtmGPXzYl6gntLTE3twcfBs/8JA53f
dCWWZA4yZNp6Dxvu3cI1CO8+4ZOiGWtWn5Q3S83800YiEbF75SVkJ8V8bCH4zH8yixP1fp5u+XBc
YwTl0xxH2OpPddX6Uf/L3tohgCwHbVKxT7yWu5b7PWwgCI13La2zlSf7mzQ1KSlNZuvtlBiYcdrV
JMC/Lx8+oSB9KFUIgBGlxavh7oJChermeinBGZ+ByMKyYuqyy2X2xQhfIwXcoqXpuvpystFq4AeR
z8vtZSUJi+6YzwHrnPxZ2uV5UeKg/65MJr/JQXurFTv4kALZibPQlS9Blob2f+oO1ISK4ghCfJXV
BZ/ToPQK0EOUrjNTDlWzFFdF4etPhuAJTdi73Ub2jWpjK0IW8LMUjPJNLl0k9f8CAPnyvM7cXUqL
hPSI1dsJ/nl/2tIN7zjlGfLwO8FEMfn5k1jFW7Q0DBgImv1KqUffQMJIl/KCjLI8fEo8H5/8d8vu
yHBEiPvAyJXD4suBCSNhdm/WolEu130YZ5XYV9say2DCnCrxtf8EQNa+0qOi74MXf/rEvylO8fHp
ihbzd8I1Y3FJxGfx0QQWz09ixl78NGsTiqx31mfByrbFndmJqIaNwJwMwhWl/FW+nES0njFSoDZP
O+TXuO11ReRShhJDa29vrEpOIKl8Bf1qyPJ+tbQl2NaPn4rmj8zJL+pcGjgKF89Z2mHgEF4lutBJ
4L8p9/ggyqiBGIC2uu9EwumYS5YuMKw2cSs1hxNIE17p9SV2Dhwfke9mxMr9mEVORb5JBvHuUao9
oIYqTyOurM5FqNgpf1ANuLQCOTfE5g5FbGkMBNG2sNPOYnhMh/VNoRwcTEq6viSKU6iHthf7efvw
WxCcwaWPa3s6KvhjWVXli+HKAbOVmy7yeQ86Ou/TLW4zeG+8nuZ3De8AwoZaYR65zQK2sj7zKfQQ
mqDMJuAms6EMqPKnG69AoGh/1Kah9KfZdbBy2Gs23l+10Gln/jIMyrbLbds24H2Rvzq8i+7eJY5d
d11iud/QU63tDh3jHdb3Uf9cc9ZXE3mIVZ75FFAOtRldy9zeS+CvfrEpw/iQ9m9/ZTLLpLZ5Zz6d
hnt3eGh2CO5l0mjWO88rpiq8ZzNgH66OCnhD5m4QImiD3lGM2yrFb+/a1BM6ClIZ/nO80C5C9dOX
C4qxD+QAYcbYp4UWiswEAgZzNVg5zonyUsvunQNfr4oIO0AQyf8/GXSFZMumwAKkdi9xg00yz88b
Sdssa6pztHk5MrebG5wnKum84osd1tlRyVSMtrEokf9oTd5m1DRe5MKoy/8h+mloZ3Sc0MSGO1aL
QWCmKuXmz3sQYYZv9YfR9t9UTxzzLP58VNhyYGI5CiR4vMxkPVKz7RmjF1iI9ymIKNTE016pPDd/
SxJMugOiKy3XFPUgX0FqOy4d4TTvbSfBWPEKDOnr9C4ULPO9FqasX8/qEKL451OmVhrpOfZPMl3S
NEa5FAoQE4ktkzs5T4Cuh5jag7rGb0jSXa9KKaTMFEfNTxCh+M0UA+2ilQGLc7LHywqkYTG3g8gw
+VjOw1O1LtMtfeRKlpY7PAXSBQmy7qFUimv2GoesZEYuYpq6EcrVBLeVeUxzFFbedjDhR5dH0ls+
T75RdUTKIcxbCNDKfOQ/AIVUorwsym+3ojhmA4SdRwY6Efs5EncPDLP8hh6zOsBRjKF+kVLqx8p5
i4iOm9JuFFcKLOK0K3gNmwgwJrcLV+O92jdKAT6PxyhlK/49RceuFsT9/i0zH80mm1HviSNhToYb
Qe34hNxRw/b6s2/ro7Wg10NxEiu8FcXaQtisbkEOsjSSX03wNIRKRn60oyxhqTMvzHxmtEz7/Jwr
KAkSqbPcEvUjwRishrHVSmjOt53xNPYchOtVpIJ4ItdUmRh9wE1zpu3mK/9V3WL/pkkLaZZllWdY
X2s8qgZhc57eHQei/a3G2O4eqT5iZ2ws5sCLKOcfqVRshbCfhmiLNdDd23qlbXrdqhi5ggNCDA6c
V0o65WffZ6XgZU3TvR5NaSJ0MFjokYPjpMbRT2St9ujHvOATWvLijD1KC4kDlK45yf26kcsQv4WP
JwxaFJTov7GsE4qUZg92H30ucuTLG0KDaS35vN/xPnI7VNfShPviiB+qpcpP7pQj1EKn/sGnpMxR
R1nP3/wC7AH/Sk0yIDKVMyIfN1DC1UvNFjGke/dCnRmaVj1RsHwCWEXybRLv+lSVd+Z+e12LLpnS
TMfUo9JWAI3V1C0Jovgib1T9SgnMBe30jiDb41Ch/RYS7QSn/v/L2ygyxukNk3Mkn2ddNYuVxoF8
V4/e9bpB9u2aTGs5pELK4Gy1uZu0DyvVxt4MTSAUB+Vu6+RMAHa3otaTJJ2ITYuoOA9JbDNPbWPP
BzelrWqIGRbWMUfU2wLAjLxqI9IYn7gqUTF2INpJuTFh+pDg+3yy2RePbFKA8b+xwQtIWHPfPJZi
L89JRzj4J6CGFwXK69zwl2jEjHQZ3022/7U6gy59SAmPGi233HYW9lYD10zMs60gKJcAJm+DYTtS
/tNu4PMtSFzzBP/zj2rQk++ydWVwqaVNqF8gDxxc7GsTxl5qHbK4a3RXDbF3gccT7+QXqetN53pR
JakyjhnCIPCQhwPdm49DLQSEYdtTUL1YA1HL3PE3IQnvacKa1T8iTHxMoMqGxNWtzYeLla1qottD
/WCVTnu+oIoKTZuvuPptMfhrSTpXMkeSZvO3LNd+6X6EcchzqkZVuDWTuuZR+hAdE4ZIzk4ynECV
TyG9laRqvVfRXfph42Zje0X3t/o/XfApT5i/0TRTWzlIRX18inZ2taCXUvrC1QomRC6JoBcVrFru
XS0EvND8WV5LoACfk5THeCt9mwzL8zi9hwmp8ZDUBvYbSaIe8drBg5iWQhKCz55SGQJRxswmF6FP
pX2WwA7ucj2rwRjps3n1rYY2DUQIzcjmnzmgoEHp/K/YpbmbGKL2ub5YWtJMdYHBMkPc5Aw/eMkh
lkNQ4M46TyiX2KfBYdVp+ssZH2FACWv4c1mbY8ZRgPkd7AYtCGbo9F7s5zNwD0vHivRcVj5xh7UK
g5i9pyoKmtu319b8JTR59e15UxcbwmqvuoAWUH0SMPtSzwaXP+bXA9ltNNgv8ZPwBHMpBeDxhwfx
LUiWVsS+uuvAsSsH3wA97wsflXXSJQxjyxDZRARYKrlI+VO2bW5N46KVZL7BcAWJ7y8OEDjf0X3M
/ItHFQRL4Zup4p5kRL20+a+/atXAi36Dqhkx4xBUX/plX4s2010MO26RkYyeLKr7J3mWU+NVHO9c
0iCYY65qbPdba5Qgov5zoIW9P16DRnX18E1ZQpoc/0eJ5d0PDQfen0nMo+JVpogy6XpNkDieevML
rJvgGhZYZ30DUhSdSuDTe1XfLjxOpcGT3lu//9wDcUhj+rJ36Y6f+c/TtpCkpe7Mdu3iMG9Ksw3o
SlysQ9k5D8iO63SVg2TbGO9Bc1AUAFmWqHi5qh8RLkU7GCuGMVgO5HGOVP7od1qPoee9WthbcGWc
Ko43IzHlEbiNOBM0fPNJsp2bMdacKxdNEUtxc4VaAu4CkVw6SZC7BRWJ/YmlFqZjuv+cghSo571f
Wzwt/IsGHpgNI45KUCt11c7orhyySmLWTummjtMS+jaBqAxWNyZP+nb86WhnHITWMNSUa9Oh7J3J
H3B2y9zmRZ31bA4sFdwdk1eemx5li9khGZv46TP6c4qoxDDwksAy8ko78YbajjJHE5soNHrM2FVp
2FE23kbq1H2vcoJBcN4qI5oY/PDQx9eyTTBNjyDjVgXh0YGb9l37ma6NKBeW+iT+5VX6JvN/KPV6
sOPqUF3lfdyOmRYTyRekHO8IOPrYuDlntlNfGhhsvpdRf0OQa03HpvtVTPjklSMmtHCmrbr/EwCo
+5b2F3QpNpxUL9aDu/0VTjH3HTBDU9/wVeWkORoutYxbBDKi293bpRm3RP163Kt7pNODZuR/zOKJ
Lgksrr9Sy/6blpjU9yUT4wc4t80MnHRjx5yxpJadxxMOyBfzLOGNjZMHEmFrIy/1zvz9gJ9vSxHM
y5OH0GZlSSA/oeTEUfvc45LwuYWWLSZa48HgJQ75wjDywoonca4flUs5LoJG56MvX2uedf7Gi8SF
tmW3uXEVQbEKoUwxDjoU1pKwy0Q1JY3Jq3HCUovH9CNzevlsBm4xDiHRKGu9H/kl+82Wdc5DkdNW
RvK5ISnxdaG+m+ODKjboco1zqTjQECes2B+RuWFGSNGKshcEmP9x4V4NgBs/h9pu1LtjtyB38Yzr
2BDbtf05FTaZgZqmW1CKo4JLYZE475ixORSfnxSpfZDqmj/ZBOxtAcTAMQLoE5XNVPbd0kz3Vd8J
GYUPzZMiIEK/K9F0yqKcvkOKtSld1Ti6Xzc7QCm7wrWSCl/uKODFDF3zyRSiL9NFXjR0ot6is5Yf
7XzQbiT369J+kTTZXIkZT1QllR56Y9/w4/gy1PpUa+UuCnc26hox3HHlUj2AvSB87IApYO3jWeOx
PA+gPLxuGrWnjP5mVW9l9s0erK1dFs874VO04hjlB9VPB4fUfIgxOJTEfcsiaJvgOizjQUvgaMLi
mAM+92FndgeqjqMbapG+md7vCqvbFDyKg1dtt6fPGf6A9l1yyapF/G0yvzjnI9R0fHpTFWk2IEvW
IyXQEFmOMz5knaENjJ/ciSc2Wh4kRkZmX3epa6+CRULWHr/ZaN8Dpuy9GB7g0RfMCchLHIzwHsdM
SkQ8HMi7k2kOrqb91Vjlbu8VML8BeHq0aAZPYbQlluddcSXPE4yLUQL/2L7Lqr+yBG7GuQGluntW
oPOgJT2D67Pd/Iiar1357YUaj9/Yjlv/wwp1YAQPfFza3S3NvnpWujXop+ZyLvXKd093fwiUTtmk
j3k2pmoG1h/iFYdqQLel4pi4Ycm9XpXWy45u0FTzsaMVmVFKI6w241NWfbyCSquaXH3ZKWfknBgo
n6vdLm+WC46wmZKj0C5OoDNKDT5VB5K4ve1Eg4K+BtBBCpadBGyMRcnPa/wbUummhbTUmb4q+Mgf
T4YV1+08IXLCT21pZ+y0s0t9I7X1JQiR5yt7ZkbU6cDYXO+dIsQJtnTTMlKHDPdcdIno47iMCBOY
qZz9JreJvtKRdumQ71NWLQBE9PwPthL59r3BGhQDREQh1bcuhWFQkHM7Ha28o9pDE6Ee28JGKn2Y
it0DPXuB957W6z2fV3e7ZRe9HtggPzg9FO/e4xEEvLD4VmNUyZFTYi+hiG5LDv6N7aoi+OMZLyIe
c9VQHeJ8K4IYxaAUHhk1rb8SJCPzWcHpY7eoes7PO3S1BSXHPXpoDxkI/LOag3gYPMNAJNgevQhq
+aebJrOuQxsd5mbK2qRxlG2e4O5h+rOKN1yGbJsYuTGxB4LIVQO1E03IOH8+QihjF1dyzkbE8Dr3
IilSfvMTe63I9rd9AH7NEqCYoDxkvEduyQ2n4GlrS2V/kOu51f10cj+2c2KyTqVxfnYewRC9ym1/
Z2GdkHOlkXZ8ecI7E+KBb4qiKRxt79WUEUIUIfjOOWPtZqvIVO2QJhXI/rMxXDDyRehK+Jw8GRom
UWQkZ/fUGi4XVGHr9n3OQtYsj1wSwyR9XF/mv9O/TzFCaAOlKKUXIklSCGBqv1P2ZGCgTP6Mvoyo
boSibYj+V4xcAvqQJmdXUDgwGhc4yZjehgvo4wxUEiZ/KSKwXEyPG+a31jjSFI8L4cwTFub8/EyG
nmcBklp6wY8h1HXMAaSkDEtah1NsjmuW7ygLVcnLYYjrc5V6lVvW5c0FIsYn2Q1wvHXXRG1mryHj
Pq+gxn6CLWaYbNO6bY+kpbo1grj86mLhxKJ5a4UDQEJLV4BbiFgs42eH7RVG0y28rAP2X1t4QdCR
vjly4AWBvEdgh72gcRUiJ01uFlmcFAjk4eAIkvNPQ5EWal3f4ODQRHEM8e7mDGdT0PduV4XPMM5S
x23AzKHtC2w7zrhQnXLG82pHKeedyG6vu4TQq/HsWnu2WRLZKkBDwUgSHi2sSdEm68VsQS1WMJg8
R3+GRwxWSFIvaAIY8kGeDHthI5ZYMiRt8ePxKAr+c5IOtix2ImuRpbVvy6N4efiF6e4o7BKqxt3B
Cz5vfbyYFiKKttYKlfwxqU1vzpSbh4FY/TbMvCUqqYKZRK7joVzs6/cWimrlB9U2mrIiTLawu0BJ
cuVR4fueO4NA0cxJi4tcYzOE3owDNDD1vcyZeRtyt3Cgxun3O9EbBseeqzA5EB8LJS8s7P/cB9R8
Cbvc/Wo8tGTtXXnyTuZveUjeqyJ6RTpxQb7AkKJUMts3gD98UYwu7VNB4GO7vS8YI4KvS4W0sifz
K7JqM6uEgkpnmLhACwavJpOJu74OcOIArCHp1cmqrj8KsBI09lmdHp7vIlIRSEvvDSosIa/vEF8h
0CHQTYnlY1VxbOHxjYCbqjv4+jaYTi0nidt5Mb1E
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
