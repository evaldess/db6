// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:34 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ blk_mem_tdpram_1kx18_8b512_sim_netlist.v
// Design      : blk_mem_tdpram_1kx18_8b512
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_tdpram_1kx18_8b512,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    rsta,
    ena,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    web,
    addrb,
    dinb,
    doutb,
    sleep);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA RST" *) input rsta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [7:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB RST" *) input rstb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB EN" *) input enb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [0:0]web;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [8:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [7:0]dinb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [7:0]doutb;
  input sleep;

  wire [8:0]addra;
  wire [8:0]addrb;
  wire clka;
  wire [7:0]dina;
  wire [7:0]dinb;
  wire [7:0]douta;
  wire [7:0]doutb;
  wire ena;
  wire enb;
  wire rsta;
  wire rstb;
  wire sleep;
  wire [0:0]wea;
  wire [0:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "0" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "1" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "1" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.805807 mW" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "1" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "1" *) 
  (* C_HAS_RSTB = "1" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_tdpram_1kx18_8b512.mem" *) 
  (* C_INIT_FILE_NAME = "blk_mem_tdpram_1kx18_8b512.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "4" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "READ_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(enb),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(rsta),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(rstb),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(sleep),
        .wea(wea),
        .web(web));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 28672)
`pragma protect data_block
Kg0pkFyrai+q4Lf0QQ4FhkOFHzP/H7k830DFmwQdRVMtCOyUMQr5O/NPCxD1Mtn0T5PTF8MdvmJ/
N8ZOhT+habXK6lF+gXXIDtxspbAOzPJcxkdHzgf+9QFeM/pyboouGcn35wt97TZGRuc+0zOVvOk6
DZiNsFkiNc9bIynNPeShvY6oyhJPm4rZaPhn62l9dnq2RQrqL4cUGZG/H61vxucIi/FTXtG9j4Lq
Kmz/fDiQKAHB1f56IvScoBgkjhU/7nEfrT27qjj/LymTQB4LAxDSp3egbmeAv0/Y3hb5ZIknb4Rp
+aeKZOXYip7kFBKz8Yfkw3Di0dKOeHUCokIyjnC1TlzjEUgxKa33senRL8rWUMBJfCB8/L13LG1v
03VCUxAAn2BY9ZJSlL5e/lQGSbcJUHnUuTJfn8RVqj9Ep6xkJKn0NHwprmLSFnEQ1+vb1uh9Tl9l
ghfo+a7WPHQH3fPDcv67DpQCgOy0ib4pYQcybhx2kxBY1OFEXCHP9Bdb5wXrvhJNkqhGRKWZYkVR
fMndHmAzAIeAWYgjwJXKTqO1hF2NVALWC7PEu8Or2pdRZFs7tkyBETBBcnhdV5+Hemff7F/CgK5C
Fq8P/wCrnIHXIvJJVM77d8dhtxOLAZB+INI//FhNS/sYmv1HKFdx7PGKv9DvLyEII6bKk1K91B/D
R+IgqiH1aRwXeDCDSQbaMnSnBwPoBobS3BfBCLTu20AVLZ1HzPO5sIW/YP+lslCggz8pF9zAYs0+
p71Rnbi4Q6OkfKTtRfxZkZY3Xrsy6CMt5lJHjn0X2msVFR77R1ezxTNs5OPUz/V7GtUKAcQ2Y31H
pI0EhEr5wMmjijL8DQ2/CpZUPSZy5D1a+3+S96zoKAR1TUA1s1XZLwyPQOezg+8lXSUI740acpqk
ra122iGwXKduW5MhSDF26VgVFtEEkUzN5kFGBADXQi+a7vtKK2U+kuMCeGFEFyAwsZ4/9qgBujre
WqlARXmp52OKWM7WfnqBDb+ox/h0OnUeFAdOFghWJefQXzPVsexbRheOPI25EoZoxQSe6R/voe+f
QjqY+qPl1OOqUtl9SGwZ6K6Q1eIKKaeYohd6EFpjx//VhzpTJj2BeayPAueUWrbvVzQ1fzfMdzHu
N+YhZSZqsVxC4Gks1sDLnwBX0Ygqs1rifQjUgMFAgcs/nQhqc9Tc0f8N1FzmSycnS0jR9zmxyVRZ
qmeehBTSglMaQtgKoXnP3kETCXcWrtNBVKDBW3v5PgWY+MHAYKNqHBbzprFNSgtANRLfkVVBI7Np
pq3+0fAp4XFX3mdltd9asQqPuT/KLJVmK5r0koyKbXAaWJLRKff4wPLPz/yYzhxWGhAoloyHodDG
k5KrX/GZD/O4I0et3D1Q0Tux9niwJHlNexKFsNnhCLMNHFXBAnu2+Jtd6dAYEApHqGwGAiUgIJ+Y
apd9iVH98DnpQDiDtizfeYj+XhjoeFFh9hbEFghdKT7mpt/0iQmq0Ulq8WyjCJ7hpTBCznnOQ0MN
RK5XpH8xO5kEMK6CqbdZvvE0ossjHxMSvB6JH6HlBxaM37yAniugSm9I1q++isPZ3GX36vMvPX1t
L0jQok9br1jAoXm7dNQTHwD7Gb934/9wYbp2N6C+jDDDftBxWl4d0h3qJD4ZhOsZfwl9j9R/jOYG
O5rtE/fxiXJSHDno/9XK7DeaYtuma/8E/+0eE0afQ2KxNzrWp6YM+H4yhSfMRQJaVrlj3VbIEqAg
f7exmuOwxxZTMMAygYvo9ZH2bhJD+mQVUgnCH+TZZlMKwOKdulC7gFaXsTGEHxx9xbPyKvAVf1Ig
Uqc+V5qKsxWNyzKkgxPMN5By+dnwsO6MyeCXGum4upBUhhEpbVpDZqNDC2k8lcma8kRg5TwiZHDU
Y/u9UzjQOYjG3BkS8ytsmA9mVcT8Vc9S+7sTEkhaUl5SOxUaklWOkfOT1jF4EH+j1H+j5WCxgMTX
4hwaohItP5imh4zZqPDMFn+S/PLS7kLQ/TTY8zlh/UnDIJQCzMqBuG0LLhFFddsBbdtmlz19dpmx
nlElH8YJAXQWiyp//cp6a0mID3piUCuMdM5HCC1DQQnG8tZj5N+wer+IVr83FhtbX48O24dK7/s7
kF8cyN//Y77OEa7MNFFIs2rd/pyFjRfPFZ4dPmPyVsUVaDvt7NUf+C5mvDye8NzsyQII9DMbHCfe
GIAiObPH5h1CPi+uiyQqIV4uQKe8mjxeCZ14k7wiiKh9AeeSWSJeN7HqdeaCX11m1+nIDwFD/YVo
/FmlMpEiPlQW2jLj0dNTTWrstfRe07RAaknYlmjo+t7tyu3pSp0ee37VNEkX+JaRKNVRSymGpck6
S7jXnPKpcQIVk4T8YF3/+58c5M2Pj5e1fgnimHEEnmE/i1SiUctWuzlDuxobHnRJsTRZcVeQXDvn
ZFqqjzQiPJ5ZwXjXMAxUkSsFsIcZT5tRLmubm5IvjNBpCMn6CvrEMKJcOZoUDYawWGRLIQKnguut
/0iujiE4zgFYNB+CJw54WSm9DvxxcwE/LqIwOmLBe3rJ1oxDArlH99gqm51FHPt/2/WRZGXBtF8I
asReUWWn23GDFazs8k2W4tKK+Za3Jm4BoMDze5NwG1bRwRJzEgV/KHym8JJrH2S11n0coqscVLiZ
DlAoZ477594BXUAsOcSQKVzwmJvf98Sx3XHURMQ/IkE694nGkzlFnhv+duBjVMxkCaPH5BI4cMPG
0VgVez8Fhd5WnmKjavw4frUDZavnjeVY8stmem3TjmK3e14a8UJ6+iLYnwHd0rj3CEIWb3BRtgJQ
aoahnSR8k+oTl//JroygBDmAvOaa9/+xoETv6+Cgk43ZeuF7sIyjJaKsmtioud6PiiZoJ261N1eu
PGE0Qu4lXEOuH8r+7bAYmxImhEI6aQRulAdndWR7/TY2gXiyR3L9aisLbjqhfWOPysNUqEmKqCxs
v7pyZfPbs+zTMeg8PtdtlsXfkzd1nyl76lMg/8s5V53AMx8sh/epeyMgmiEKO3K6TSzi0vE79IEw
bOZ78rpbNsPchCfsQphysGJLJGLyPyoSp/19CkW/LwAzSjC54DES5y5Qy5gdRgABI/xRyaKBQoaY
qbN8MTiWxziVh7rVgzySADicLCcAY0N6fR6KphgVK2N9mpcMWvtS203qBrEIIBlpdhhRpZ74DMG+
OAOT4c/Yn7Bdqe+cjwUFtfuONfjXqdO+lvSrc6kIk0Gl8WajSn1BbD60ljXLAdBhuK+oWNm2CpHK
AUvADX8mT2Wnc3H5M2lUgq7Qucq52/+V/XRNiR+ZQip0Fk4s0/S+RoA9SfXfqKOjiBxWhVfQNLOK
Xdh/Z+weh5ozMiuKZGeMuBxy5FYRL0Z+wBXpHeAyDNJcDY0ClyJ7jo7IHQHSuZhwNdjucbVCzLsf
WiAryEbvpc+1W1QNeU1hcdDS+cNtekzKJSoUATbX6LALkA3Qxh0HIaTQvAdRXZVblFn56XBR+IN7
/yLAx+aXwudaIcze78LHy+YfTI360HR8sQE/1tnt/PlMVxy7FuvVp3CezdQxFoftRbN0aEUw8E3N
Tv5rkCmpgEJnivtHjiKAheDMSGqYzHL1dwSZOm37xoiO2vM9SWuxMmWraxUpdtQEn4VgOJde0qut
f2hzFQaqRtJbqb43aSWGk/1PFlO7dPLT7T9SWIj+0f73yjBeVLondwk7PRKAQh33xvZosLAAw6Cr
63i2/HLrmmxULaIU4geLHl4X7/5zu++QcJrJEB+V4NYJqKwTHY5R6Pzlx6+a0qHpXkBEGwQ4ABAF
RchXuHHVlLjIYOSzF/t0fwaU5iqgCq2WOMDGMX9am/ORJdqAcUtD3gm8TBhDQ8C3Y8cYaNwttJso
PuOMsEUI4PRvYA+VKxcoftWV8KGUDcc3ltY4cfM+Kc7g/fsvLUO2cxLdgownJfbYMDG0of5XBxFY
OR10AEJFACo/kBecCziKgC2oOdDF4tyZ9cS+01pkOLpzr9Ml0LTz7mLfyIBCIEWbUM91wRy1ndB3
R0+b5dW58bmk927JAQ9fpUER2LXQo9NQhoY5lstYIzgySllZxqLolc4zCuJWE70yT3wyTi4o5okv
Zgcu7FGZrnLa+FlaEW0958S3/U4w/PYUkrlwErpfUU82IOxaCHX6InmNjj31jon1ThBbCZEnyylQ
VnUcni+obWabZUaNappzPsXDYsVio4v+8EcXtEJJ4cxSMiSujDi6D4Jb/Jp+AA3qqo2tEUzkx0B8
AQA7wNZsd103cCK2EeTNkbP7KlY3Blb1fKoOZPnN1HR8dgSljXf92lnXbQGcSwS8G4kFrD3u4ab2
3dUbylp6/RfpJYc3cB6hboajzLiqPWA54DDPQYpoPlUfnySgfULnkiJJAlxlgJwnfVn7TdJ6nOrO
msnqRbGlHnxYRXTqqTqEyTi7EBYMvSIUzPkdBq778mSjga6l25hh4EOaNCTNHchfRIsi2hlR0AT6
1HhGK2JCh+MDABtCuH+llaWp1YSk0qSjXfgUKa4JYd7dD+X6bFxk3/RTD2ak5kt60yAW0GPvC83W
f5ewJl0XB3xq3G6RG/mib4j+T41fokiR9LWMn8JYkUtoMV4EMvl17s0r+daxe39tKpIz7C94HEfP
zWq86fdasLDuxd9OEytqnvWpGMP7WeH0crDphCVat18jz5EcS8kLh9ntyRdcxySq8mu7tUGYvQrR
zr7WFh+JXE6EgpPuFyeChiVEHnSTdtRF11UNnYjh89+zYnEvAhHIzZy77zHOyokGK8ldfdAq+51M
hbCL63rtpCfxP9zQEhgyxlH2fKFn0IrIKV79u/wHVEbafdsWM7yDZ6feFYqjULjJO6FKXWimqlk9
YVMRpjAix6QwXa9oBDSU+2A6IsokmamtVVEum52DfQqJcA5KmxRnJGd3CHmwlExBawJVGaLuy3Ll
Az6LJsWsYznAEwshCIzMBaJ0Y3nZ1C52OTpuiKXLfql2Jd/Xj8jMwFaruZf6//jHDmNtvbqZ9yOa
NZ+jKFSgyEPOrZuIvSXC2Uf2K2FtsIVz1q6twuKW/0djcxnEk6GHRbE5+9OkdcjpeUMq8Nierxkh
Z69lBdvgiKniQOwJudBBxAvgltti1rk8JbeWRJWs/bj7/Qbx/R7BsE6Fw5Kz0aZ8WJKPi6BYBCnR
fjjFTToqQi3k/SHK394goeJ7Nh+In6TSgRIIiLXDaPGP8vss6b7lgo9iC9sdeyPltNvANpvPeoI4
M5rbhBEK9sjO2kwe3pVgIBzagCaa1ZcA0LCOe3FHi+JgkotxsKWzFA3VZ3l/VHufKjewC1LnoYTj
7g2PqR1D7hT7b/FZW8cXot3TbHLgUsFwUR6lAzgwrOcJ8jph9OiUeKXhgsuL9JVANFTA18lcBAk0
ccbeJvp3lc3mMQ/ByvQutXvfbF/PRBoRlVUCa4gB6BZhyN5p+3noGuWIUGyA674rBgPbtj2SnQOo
1pgzpJDtvd4IFsCiMTAbJ+jIykFG1L+8D47frw5NwoibYbrtdJgIkBgSvyjf8H+AcW+2S173ujGf
mAi3idvFg6K1p9PSNDr7uYge4AB2Afg5cMFbmkv+IkNx+2BhM/8ZJzT4XEmCp7b77UjCHmsIr8Bh
iria3ZvElMd2NF2gxGndrbmOfw7JpLOont+4i7iP1F505Hvi7VQHFTU6i2aG/xExTXc89OMwwGQq
pgIA9J75/H7/mTd7Y6uM73pDHTZmSv7A+z9VccEOZOC+9paye5Bh65PhTTegzcYBrGqV0CNEBWFV
luDATiSVG9om0JU4xeV3HWVB7v6rvEAzK0Tci10I41ctO5vwVJF5T+/U0MVKHHgPmikNoqlmDYEa
jW/Vga+/uJPIuAlVF6C5dWx03GH4gDyy3D7/5d5rqfH+cPs6eByQ0SfU946OtyccCLTdyz/2uGT4
n2nLbawCtiO2gM5MXOu4koq+z7RQNd1kkC2Cvz+ICL1vzAFusK7Ugd9hsnagXsWxdg9SawOF7Yrt
hqfphXGQWDuNM8lWWddlUF6yV+INzKUTFP2oOpplny9pBPZEFqxksBO5kd+x5dfF60yUA/DHmM6M
suOuPpZQQ/7bral7HQyqnomX5uu+jTRh2Q3pub7XMM4omkSQhSEMLp8j6/a2c8sXp+efod0h3kdK
IrW5m2RmoXsnYYXksFkM383p9aAZaqHfIznKolOk1sqAXveP26kEjbBbd80TYX1St/CLc8/KqOcF
5v46htm1tAm5Li44HcrKugP08XyEPNHMYyk9VA+PVKP6XpV0PiHuzzKL3+RKlAoUHkfPKT27L/qz
KYMlDBVeFFq6P7tUIGfYEBOBdykHcaFpeUL1YBfviu1MSW3GKJpzrx16P0mAv/VGpjy8xdaYeMPC
WP2LgQfQoTvb4miWhDvfmfQg8qigSVFDTmyINgpzpeo85/UV2MC5umELf0qAxlNQc5WyetBNyT/A
4YDu7Gf7TtXL4/PBaw+PTPXoeKodKN/Pky4S9IAWpjq7svsdZXJzxRqy521RzHZUPjU+nDoheBuP
AdZp23noKIVtH/ZTgSM+72zX2FKsukgjpyt0MgZQCdIG94RgFDueywGtKz2klwJrYibrmK20ibbB
6cPAA/K1liStuuiFja8QR1CgxjfiGSF0ee9iod0fe0uFHrR7o5JqTq4zM7oC+M1JHon5YkPRoUrH
Wa2uzeiJGbe5gqKB4vQTU1V/Un6lllkhn3Uwbb5+Amb+P9axIe7bo+1Wc/FhTECfJaqGQPETgavm
FNSjLovGBwkzerEQYLL4ws6rmpbE9wVVVy2z6PLsrLGM6WYBGxfewgqqB6hrZ2PCiW7ofSt1L65c
aFMB3HB7cFjOOYc4H8snWoSaZjslDo0qsMLSKGk+/gwjxcxye5/N6zQbBk8f9MyE7jOpsHW8tNXQ
7rFtzFnS6zVtrzQLWH4sRZkItMnmywcjdtaUWDbyI4H5eWF9BqVi5XB645dXZEJadLCi24kpTpMK
nqPIMjdsKmwhRF0wFAkjMlcEJ4hgZGC9i1C4Z3S+GyM3kt8qII4k2ju16Cnwv+lTMLnPY3/GIdLi
ySnu9eV62b3cr3ha1N1IJlQvlYxYsvgYfL5Z6yRLu7wRjh9H2K2A/XDOzr/K62EuK/Z6vced++24
Cz973vkUtDQhaXjYIKox8cmAGU+NtwhPLq5JUgbIJKWg91i3AqAeKjbaL6hvVwLZXUsjAlc+4oKJ
ErIhtm1GzeBeABrkQ3BXfCyTeOLJ1A8eVCS3XUupAHNALIoSxD1IMVTINveNde59hwOaUrOO7Aok
fICe5NJURCge9ClbBf5t3VOH8JaLqLNRzpt7meDeUzB/eZ6NrKkNhdg6hMaBs1XeWhf5qIAY8NSC
8OhgqVtTq9hpz/kqNc1tPAYDa+3YZRiN9jPoBuPG7tLRB8DCzvgvpr8IqmlxlQ71X6BPXRzwPe93
dRwWm4ZLSc2Lp1Y0cU0XFx3PjPY3zRUPVjjfz5oOZMgKe2Eg7/tYByZq8LsR4SQRDXAWQMZRJLpV
4L89tij0C41K9WltNov764pY/frGhU+i8LyJKhtHN0791AJpzC2I+LGfRQ8wgue0Y0ZA2VngtZ0Y
hbsL2hxy9GMW8Ch4nj6g5okp4ymmPmCQSGa0cTWouJ2HhLaSGzRVbi+a0Wi4nf7rctrVRFwAvh6U
Xz5Lkmlk/tOnVc1IAR7TP/Ys612zE50tGr2H1Fiw5Agk6WZYPFqjYfvoPuW3fzwjA5TckudfmrD6
9VV6LIySXymykB12J2nQde4FC+5dL/ph29QUEADzRPpDPfufBb3/h2fSVvW6DJ3sDDEvgy9PCoj9
Wsd0y86Xf8qw4QLYyOrQdCQNgCz0+QrX0EkL19etffBEOv5jAAASRDJ/TBiS0NQ260xakFDIoX7O
LF0bnlEp6PDNLysvE2XDCpme/HVZXvQoz2gDhbpkBPqhY/Ul3f2AiQqz9nzRibbX5dxsHo3HKVKZ
yi4UOtSzA+Hjc680VSSVMAujVAeEQqC3G1C6bjb1vd07VFrvWoQJDshrNlXLS7FzIC+XKifsNDiT
/f6dEa75PlRhO61yOMOEOx7y7taO0bd6qjcTOY+G7ItBbI7Yjf1RCe/t09sF8tTctjVi87Iu1RLN
NFJMSwCgDPwZS6MylBSlE/lDqMYlKf74ZlrPXvBsz8PgeHPqX79tDi3juCVFOyUU3xomnLpMBXAo
OnPw8V2cKmpM8ll7DFrgRw7tsqWRx1ZmvUXJAU9EnY66XraIXp+VlrjT8KuB0EaiKQTkeumYmh8l
6ApeQRNt8gAoDRXOH245HE/omwDAzDxTwnEs4K+31lqTbWTS6zQZvlV5rqv8LNX1dRaSuM3fw6xu
dDqpryiAkNO/pfN7938Nup9WFYvaWHYjbPSJO31IQtdH2++E/oTiJ9fuqLJEs7E/xYRvY4EDXTf9
wrbASYADP5sqKnbMzvwwRGUZHRDtU26hju6NvKbhEW5ulcqFfym2SY3GfgJGoCFR6kWGRyFai3Bq
BAh7thcNub9y4obyoDXefhpkdVRMvzKzH7wKtM+SwGSIFsW6K01JmAXBBvlybgM+wKXy/8ptTszX
w3tv/84YMnMUruOrSHqlIZgmxY9vdF6Erw1VMc8YKX5oLbtDuOw0fe5hJqS4VYxvBqTaqSLWBB5g
8HDqDFRXPf2vb2Iz/Mh1ifPXz8V2VmPiemnv8HTh2WNTV9jGFqVLsIdDJJOWbJZwP61IjnNZ3Hxu
/8Z+48x5tpLa5q86KrYM6P4MyziEtr4eYZBqVz0JNYGvhi83v/6fpWUyzLIc82KtZTPJU0GLXx8u
Zfg5Qy7X7clN46DPYMi7COTCdGzroFi2p36syFmRB4+j6RZMke9Z1p3cmDuIWLKZpHNcq/dH92UN
0D72rIRgKycNQ9oIaVKzXasRHKNFEeCiKLcbz2r4d+LbR6UeCoN7LPKAeORgltoJzuBK11HhC5X5
VBNGptIdgQfCZ6PkXhZrjBMX6SXGFldGfl9fOmn9mJngJpMCGMkgvLAwatppVnzNxIDc4mn55Nsl
b4cCUdxwGsTvluIittJehf5ekPF2TqUeF992RrFu0ItNzwMA6gT6y2/BEbZMhC6FsrEXH7kHUAji
Q18tcUvRYBs1S2fwvUIKkE0kT8T52bLtmlFZSLrtwwvvoQF72w1rLums8qKrDDeC/munLej/Evot
/ULsHoWm63Lq0c3YTyXGPJ3fAQJg86YvgOdJ3CJ31RtPipojzYlEpuz+KGiUbQR/aAyBIErl2LwW
kaSbwypS8xmd09RIjWWzkh6pM2/UCKWZ7+/n473vvMYPZMgtKlhGnfM8l4PHCDrcHh9LyWOUprRR
OJApszzSG8uDUXHfTcqqXB0iU5ydHuQe0x/IT9B7vQngS5KMAplWKJ7cHHQxeyv4B/uQ25MGYjL0
se1VjcjDwuLc8L3RZJw5gNz9dOAYbdPBeNOk+GeYKe0rWGbbH+xklARczv9c+tMd07rUArKB+vTm
1usocvCwCLxgKYixdB1bAgNBwmr36K+IzzcVltLN2Rp5HSCLRsSMnZ2HEMwolEUFsdBe377z1GNi
Oov8JsKCs1ixQFREeQ9i1joPnJJKSDkZ3rTfwLK1GY7x/QW6t5kkLCZP63Ll5Ia4tYHx/dcqAheF
Y/WiwLgCn2ufkYPPyqkczwIevA9wtTjGUfwkPyqipKzJCjGNs56bhpFh5cEcJw0R0afj6E9tk8qO
XBTBnxaZ/ttiD9rPuPoLt0ODpeM8YbJoTWBOzwml3d+5anbKV/rSxj5xbdoIcMWd36wfuCP4bXXS
Ympqbl9kAkSpAWnpRHjIvQyLjLBlii8fHoNOClnpDqdzs3szyVyv0T6h4tusrqCXowIV0JHhPcf8
sK9lXIlrx2reBUv4kU6s9Lh/ONYtt9/tR+uIE9b0l2l8foL4sqeFQdBIoBZ2YkLHVuWEKfhOCkLl
XZR1+JlXGhC1Hkc+lgezD+c14YXww1jR3lslNqRL6a8BdORwUINTXA7bWEOgo93BSQRJ0+37oWN/
kao/YLjqMtiaW6xGxATExJTuJ02mkFEC2bwvmuy0s2COiCVkq/+XteDwMg7Hm1AqrbC6yNieskWf
VfNcvRpc0V7bL/R4t8UuxifMTbwxFYRqsLshpSiExP+G/FYVPe1+Xl+CIJ94+0FSKS/rDAJqubcS
jWBRQeWdOE5ajTokTdaKDkSVnB8NIVd0AvwVP8n1nixYVFjUi5XCOOOOSMN7i7c2gFs0xAJJ75JV
pI44yURwAc+T6kiTlKUmGdhJYtaq9kyThTrakoLMjBG0xn30L8aQsDX5mid43+BP0xi555WfC+Pl
KIGM0hQ9jG2r3bq8QFCXwjyK9ZaxV3sSy8yxhTh4U7jeB4OwNt8u+QqYO1LYwIZrPP45UXJNYhfS
6IKnmKZwa2yN065zqdXwtKtamA6h9WLLbMkdpvg3hOUOZqrR9Eo7u5VkXjtfWV00ry4facnm4MqR
ugydJhKi92nc6p3x5T9nc/V+kOTqJjgJQYqH7S3bQtLPYXs3yUh8ZUODrYB7w7ZB/pBjCqNgrk+6
h5cdgIvWDs9eD7tHAjp3J0paV5dL1UTTBmz1oRy6LuiR/q0yRg+MctIK8uMl0k+uP1a0aQadwmat
WAuB5Xe676kmPZe1ZxSTQOy9v9dTegOgr63B4TiCrmb1mHhNBBrXyICI1iHqOPCeSL2qiAUGlUbS
HoUyiVC2SBFDwEYlVzE5PPHjgyKqUmokBbJJ4qphYYF/VKuPSoY/l7kxyYlh5Jo34oQB4ohtJ9oZ
VJZqBCBUnmGir+Pjno+bmlSaQtn0b2BWQUNYQ6H/t2NjlfbaoIevdOcg6smET0b1n2FjNobzMH8L
13TkiP+S+Sot0uaBPC2oBc33IbDIRSuFykEJMIDlYe3lWWhhypqRUDkMgsbIoK5AmmbB7GNZg9iR
8PdSwDCgzF/0+Qz6IU3rMZeA4MUV6U4Of6ysxtAjGijUG/v9FF/kItzU38bpjqmnAxsn7B1Lg7zp
ELQsvGrmwep/8kK3EwactFQNQqGV36YFA++MohJB5ipjMHv2CiAwqTnEIsEUabWjaJkE+4t6d3Vr
Vi/qF5b9BmOZ3a+6q9WBjdwbqIJREyatrRYi/T67FrgBrHEeAsLpU43x+vas0GQrcZX7tj+amQ3h
voGR8vwP63v2pllsvmRbJrNkzNv9iNuq6aB6f7v2rtFhtON1mSPw3g6Rlo1fGk9VcqMxQtqAy+wz
gEbTQfLZptzrbRh+JEGs4Wwba2T/9PiZIfjbwYs42EB20BICaHygJQ+8kZU1bWV10liwy0iimFHz
DAVUP2SYJq54d7EQ5u5FOFt4ypZhIR5C36MF8A4SsbfMHGXX8Kg8AF6Ds/FHuD6tQaouJ4s7yf+l
yq/Z6xRSHfDTfkUvenYOOj/cFw4l57MA1ppxh9JNbruu5XF+Sbhcbua5erffc3JCL2mMrTgzraC2
eY0fffJNxNQtZVRY7J2R/h5szLdmTeXMXyZ+OVql1kK3wJV6jumgqw6/7+qR+cqDakCyOUfkXxNg
ocjWrFBb6G1uh+sS0BMZhkkUauvqjFLnvUAC3guPBPxmP5Jfzezv3xgeKNyaHhQ8DZGwerKJ6GgS
20WTyRDVfiMr42VgZbYOioaHLtPaLh/JekxL/G6wx4YXST2/NtqbaHg4OOv1JtF7nQdfNEYe4S5O
CDgwA96vJUs4lHZ1vwG8+nQd7PqM3k/5r7HOBiB+i3uQMs9jicmxwQDgcu9XaFcxpitY9y4JK3kG
/1zGU4mwpv1uJkxnw6Vgo2fnD6U/eGAwOhBz3kdIGQSHyxiaQ3pp6ph2viZEZulcFng7Jl7gcd0b
JgwZ08gzD/5/ydl8DUXC793Kszeg+PytO9EoX5MDxpEwE7PKGGRBMe9sS/6E6uHRLJv1PDtQcauh
pliBImW/FFYRW7aJWOQgUbeDssYLwqWzQcPE3sdOxWW/h5r1jsUOrHNAkATsSiIYg9V2HDPFn77x
nGAahjmKfOZR4lkbXLdZPUKIIn6ecG9TVM9tdhIP7f089MTI6zSs/zHVDZ+X5bZ5TPvOs5PPSi1W
Ub++FV5n60YnragSAO3AIN43OD0MqiYSgE57I7nMVNeN634aZa/AjxqlcqA8e3Gu9OnrtJgfGOKH
HIshMqRy0RfATvg1n08AfVN+dg8kAJtu8V+wsR1RJiaHczTvotMq/+TE0WAL+2oOmJActBNuG3m9
Vupu2fPzQ5Sk75g0Y7zUEEuyPm4nOkfVJfgLY4EZ6HLUeNgyGb+ueORa+h3e97k6+jzUfm92RCZJ
24o39Jf0AIksJBmTAlO4ARUK9lxI/oS6bYlDelqEiiqDQIgk1hDRBc256ZoDv50Zy6KcUdnA8kJ6
EAKhm6ia2V5ROZ/IJYhkTR7Zc0jxZJFnQSLPzlkrHf5Q0+UhlhB+KDTew+oXxLlbKKjvd4Z5I1rb
yp9+y65uKa7r1TNXr9VS1xjR9N8cOCVQXGqU6l6qNHwiUroyzL9OLdyv9BTQ/25nVlAVxG8BTJ4g
7xiNfQut4OmimaznsNKvGNg5h40fVZDRc4aC8Ywi6uWJIYMhhjjIflHRTbA3jvkPdFDmk1K7H1UY
4lHs1OGEoYggvO0a46o9e+houASO7YswTZIKGYXKtbfX4KWWq4ao3UCqiIYjdccmCelLWUQsReWw
Fc214u3oXNxgmYOCujGcjxe5YjIPSGBV61/CvRgXg85a9mjlQvXpeo05SVwKuAdc15DE61Cb3y2R
L+XjSgEte9JTYMwlHY/nTLpgmbjSkWwsssQg7iCFwg8urjH0ucmUZL5wkTqUrqUK0iuZhtwkQm+1
axGYXYm/Ods7XK9IzT3rPey2Au4N+1LzAN5yq6nGQSzYL0RZ1Z4YxE8o7h5wVnKz9cJhktoF15LA
hy5XIp6hexu/yP7zJrEXRFBmJ9S67kpTdk/r+yS89XBeAVJrERI+n7b+Kd5SKonzpRXZDyKpBCQ+
JRLlxO6O3T2cO+BL5cRrS0byhhBhgP+IBX4PIg0gITYpvdOgIl8tA6jUXpt5GqxqTJQiN6zBq396
ggGaokd2iasz08qPkEMXT51j7mod0eorvl9Pbs0PEj2OxXrSI5SU3QPjrHxEQgcJfAYuSmXo6YrN
X7vYOYTOh94/MQ1Tg2TrHh4LyXVoGuJnToZDQ1skId4CgU/92FD7TaPr6wgXaOFHxl7lKnmk9Bvx
4TWcJjnf3Gk8P4dWc9foifwa+H02iWXJK786k/b9Bd2UYSbkqSmWIFHBOzL1soUw4OVOdN7tyMDE
WgBl8FZH4gl40vDOfp5vluNCsLkn96lrzUM/LExTsl3nI0sRQZDH0FqTG8jhe/3BgaE8OQPBAsvq
E9XmiYY3fwXfTHDU3IhlHQ3H4s6rubGya49mdZeSCzNP8ITeKPB1T8iGSoiLXhOibcvMa9aqEC02
eEKKYgc3qOKCplcIpKNGN856GAyksOmm5UdtZ3w+p5NhcjNnKp/TyVrBOJIXx0jAna628gGlGHHS
aXUUQ73XXAPFxzKAC4CwPXXiQ1egWv29VVKCyBpO+AIj5sbrTgHYlWIaJGp0rAA0djXxiHkPMbJD
rr3sFP0Km5s63+yq8vkNeMo8Qbt0r9pSyQv1WOaqc88bVZ36OmgVTYhQFHjBd6LBE/lHxrII8tEK
V0i31ArtF5GrrYZHQLkU7XHQD6fMo+AABRJ5M1JWqibStXPm13qzuTSDVQ36/+kNkw34rgGKPa0W
5RkYrtksNXGGmhjdCLqWpzozm5BRzrXqLE8Zcr6BQWbsxLvdSdST+77zUGPPUMDsT8M96U66SDhs
wnzkp4TLlCfQp3CUnvjpG3dMWgURuUr43ioy08NHW3jzjieOmI9zrw5ZMD2e62cFuvaTMGeDe1bp
fmGjD+vS/TYicDC2JOlXx8VLm++OnXHwClpcFpxuyWLywBxNkJbIKXFz5AdM28LsY6AQQwQS7g0w
fH4by46pBJG/L3GQYQA16881CkQ0I6cATzkg2GOGBBas35DGufWSNgcUC7/0eLUNJB0IxxwitL3K
qXdSzs9E7sQYT/uFK59pjiZhPer2286H5lXVsEt6xNc+1tvyXx5jL6JP2UWmPwU8GqJ4ipiU5/dF
dKxgR3Unl4WsYT5KLtDySUZYew1aRoU88yCHBDZO5yizfD3XzQ530P931Oo6n58onzh2zy7FlhnE
uYaobRhyoMgAAknzO+vPnnojuXnbOHEGICKmVQIbApiOBTsG6CiBb7dmaABkDQgC70SSiQjoKaq8
rGQnCfdnJO88fFIvs7fF77cHCCMuRzGImfWR4BV5PyWMKz9YgvuvNiYVW7swDUbH2JxwaWyWOEzs
pfm3qHQdSEJ7SXkabKe9TOp7pNZKLdimnzIUFsmy2iOjJkYX+k5NYuWO5cGSBFAxM3gZO9BO5bKn
UQGKSBFuPk2fqsqSeqX0M1nb3wUv61eEDP7RnWsnb48BnWcypXB/lAO2KTJUAOLinuKthN4zh8mo
zMOj2kNfO7vnXUbAoJGNxr3eVjbpJC9cdof/mjhrOf63oygQ6h+krXWrsJS+WI8uv6A7XHKEPcyv
CYnCRNxzlsbLJcJj3UVEMefYwjhRxwbWUbUt2H1Zgy2lgIINnxxSUcJbSVug+HBahhtoT0A3E/JJ
FVJhRcfAFXag3mqeOzM/h4Hw6eitsBdn1C+KPqKCuGXwgVndUbgrnRE007pNNkx4fBAA443qzcev
3b2Bkg83qTlqYdli7f1wIRhyh61ZmBt7/lsDxE3vfUzDsJZ9+m1QtT9gs+F8W3CG4x2Me1QyI84/
N/PVTlc6hCCt3wDuMev6FTc7FdXjhKWpLDc/2NouahAR3XCFEs/ess5wj99ndm3qFolGOqik/Oxr
bVMnO2Sch4PjP6eipQz91XZafk4KztumfNhm41eKNffyyjDD4OCdbZKECsYX1xbgshIySyDLYAr5
/ICQSUP8rI99L4gxarPsefE21Jg912M27+2UxbS4PumhN0Eoory1fbupm/iu3OaXSdFy0V0O0mjv
OoYJ5Ls7O8OjGilhaKfVoRkc1iS0dZIsNMv3nbqXAi3MkjurTi9aora/IzaqYq3nAXIqJDMa3x1P
bFRYxiQ+q3/BabDStKN8LgVJrmlqRXd4um4Bo4FReK5zZsy1XS3vWTmiz27vB/rcbttg/AhxgeEc
kIzIn8OdF2Ii5GMasW4BUAEt7u97y3bIMSpSFsr7BNeaHaNIVMl0YjNok4vEeuPDnkIgDaXHE4Ji
BwgBR4rZ0jzc7th5u7vQpFjCK3RE75cdPVlCRIiEXTAGXah8LrNxyiiqHciZmD4/dsBzzqEwI1lw
viFuu3+dYr6JBPKyrx5TI9y21JtYJQncN9DAiraJ5FW7h9HVCZsC4I4GecnuQlbNIWIUEf90ZOB/
Gh2QoTtpD7JFyf1KtiYsj0e8JC7ygO15PIIOsvSW+EBba4HyGyld8nxtREfjWmFWWDvFFQXBWnVW
56rxPQ8bniPF/rQ99/4HOVUJc+aqUVkzMy5SDPIHByMSMWN4nM6PshCDLUw5U+a5IPT9WKUO/XJX
ryKZ+wMFlDkvnomfeLUsMPWamxnygwsxnoSNc8FnP+SlXV6W3mG628AcEPpvSl66gzLNguW7L3ux
bN7J/9EdJv/D0I+0XJmcmLhME1ISy+fu7o4zex+a9XjrlSMUdMx3xb8a2Q3AGGI113XLTw9JzSfk
PA4cX3v6I5deDyESdEbMh7pJmMwZPcM7I778uDadBfb8DBa8MW2A0Egl1YOZcGZezVSmXTckFgMp
kUnHPXg6p+z9aTNNZU3BTIly+Na0kQZBerNwYiwweHYcObORN/o0OFbZMwyxA+wa15IOcij5AbIa
xNHMbZJJU6EyGYj38aSDBjfWVCvxgJeIO7/uXcW5z6D6P8Lrf1RT2Vt1g6V6ZqcI84Iam/lwQWi3
OHEX/ow9Me36587vcd7PLewCfL8aNSw4PUmajhKfU+tX5Xtf8bLVqw4crZWLZt9z2S49LKk8MKCW
uiD8aAf82fgP92Hmd/nPImqH1iJMXSiaexLZYv8t7xX0YGla6xlmMQa0FxlXIA/mByybUgVuev4M
RNMEOpEnAuYz01SEA6cz5JtF6lK2ucubhatx4QkC24V0sh81p+J5eipp63Y4kUjeCpbKVSBZXN8g
UkO3jkd6BmxgNe8sAze+95vSscfc1E9NNIt4UiO+H8D1ZTaJ4OX/PBZCJ5MjC6YyNnI253M+x0Nx
1Q7dg74ZA+wXIjP7xYSprWkCAjDDkbP5cA3DG/GZh+hie+eqkXoDmRUtRGDsoEq42D/5/cvbSMDM
BDMyktzA9dWzxPic4orC2xcRROaUlHUm9XM6Fr4si027d7GT5k9V2GPxQw3Z2ytH8ZmxO79xer8x
rnbJ+7JdlB2I7J/AuD8DaZexb7AaREc3tQw33w6mEGbEHLG02P1KRxjLe7aKCzvOShoq/VgE1AXc
Zi3fwCRqdKp5pp1F2Q3uOfYQOCfBd+f47OPUestxb9h3xJRY+U3ozRuOFCz5u/wg5ppQ7Vn9Y0Xt
jhT+VVgZc0YUU710ZHqOhPlNnkaE9PMvcobVmY+wbyfCGhAYcgscMSx6c3ZiRoaUOqnis+32XWG5
jJxP9ww/OrU2VI1hjOPhagtF+34jLxN+T2J7g34n4XYjcUgNJzSD7XJnO9Hh7wptb2b2B7tWG3eI
/Yenut0g6UhYmsdZQrnVDfrrttgFHyP0cOd58yqGVjx35/iAcZrESWjRpsSBp7ohpjcknY2Tep9V
T3ONn+nGvaE89AMSM8ditbKkXvJE3OvEYtHMgJvv3PZQKdmxpc5o2gNEg9gwTvpqctYQ5s8565Rl
L55PVnCq07in224u8F2ev+U0CPlISDj/dm32Xaoctbc2OLHQ/j4EPMPW8LFwrcbsS68XbmdPg+qf
Thh+tafqPffp17+XgB3y4g0pwatOAMOWgLZFodR9MpxUf3PF2psot9yUkt61ve95/QtOtFiGzWoh
YzGevIM68MqzzNEAQjXvoNGys1+bVf+3u+HzchwvOTCB/afUcZJN3MB2ILbqBQ56xlC5HZSeI1BA
kL78j0ayatucRh+zhovekCtQaynj/B9RwUD8bVs5GV/CWavbh/G26XOog5YXsgSDl1cS6FExZ8rJ
e5hGTBdLowYTq/2gc2C7YgfIo7QXxK+jmE5a05eJBC80efk9vLcUeXyPruwJPons32EFIC4LHz3R
eEwZffzWTlOodulXou9jxLjwPAEIZQo1Ylvs+y7ABu+n/vvy5PjbNZWiVhcIS0uBizGxaRj3Qm7w
yTlB46BzgUvX8vokzwdVv5fb1VhPn1H+ZBc22QUr5xcGewqtUtbdazCZcU+J7WkR/8pv8sJHZHMq
kRmBkiImFyAeFxJMm28fLgpr1cJtmPxhgBz2qd9Y6uSijIeWdpiw5ulhKj8a5fuZ5qL8eM8iOL0i
82SQBb8xMqFmlLMYZH2eURP2C7nXsYwZnfcrxSkRjYohIibVBghDhbXD+XrXY3sgYAiiCGLQtNup
wIW74qpA5UUvPdBFp8DUI3Bi2Pd6A483tN0OjKI15WfkhQagLcr5IKpfW6FNWWKosqyQi70jlnJA
j0epY893v/LkqTIxZ61BsH20rMELQL4/lJVYaxBq5CfNobK8KSjWwpjlDvO5yi0bpXAB1GzBzOf1
T+fSoxNW9cW/R/iE++zrXhNZbm8zbH2yJJGJJ2bzzLH7EsmAI4P179fh3TemqOtgWo585oD7m2+h
GLJbfqXlmVazZO1bG26yflC8uMoFRL/5WkjY994H5E2w4WyxULovOWPYgMEqbRWd4dg4hoJQeAXc
9cyzbSyAKo1ky5lge+iDIc/ZGQoKH/3Ktj9hyWjC7iwYeqrF0ht6bTjNx7XDlQEMc2QvrMdPeBG4
uYmcY0ztF9nwb6H96xYYtVNIsxEkKqwV+wsVjqntokuwlJjqgY19ZJHFAAq+yWDjXKytLHkGne49
PmkQ62KXHN6xF5X9qCL2Ml+PwQ8p5eB2+Hh/iYnrBZht8kl9BiFAYpDO1Z3VQSqB5NjkMnQ6k/F4
4wZd85qRY+GSJmFA9QbvG+uoRVhacfB9SY6/aCLdxB58G+VK00wFs9hLMOD+pLcTJe3yyQQ1VI1L
Cv7UJlP5axGflC98S353VhDf4Brz4+stv70oVpNIJqxs56BBTvsgmBRmfFXkf3jmSPZ+IbcQVyqt
sKI9C/pwdJRJU7UIdT4bAO/Tp/BYbWGrf3QSeXdEVCzOT3EUJLf++OeTtPvIUbznlH0xYlynFhgO
RScU+LFlh4V/YxH3eKzDwx2ccmGFpew7gNTurJBRa+zg5SRtRShwyXGW64bcmw46V26X7EDWOKNy
hkVpIMCJtPyin2GKXckESGf8t80IILuqSJF/eJwhfwfU8SDHHJpBd0GnL0QDsL2MWPV7LCEWKuom
bVf1IabW2wvhK7c9QWAHJwnE9mvTUId7Mws6ZBhPNQ/07CLdoHkllEFcyTLTXGUGo85qLfeZz7ev
yPgQ7JYxk+nd0YXRVz6uNeAJQXRaGRjMOkYO3eIFybw7kisZGFoTARIjf+67WVSWryYfTc8P4dz0
+tb+PpL9WPq/nBYU35ens6GT50R9G8KE3zQIq8xZK4y6YQdB1EFsOf3dCEdOZS1z6yvmXj3b969H
NDPfG/e3LDYxcgaMVY9IAFvFXG22ML44fI8XTnBonr6Hhmb5Rv+SQqMDg+wUXgkU6VlD0H4GKgYB
O3PUSqvIPcZFfEKQzkxTmuJ7xTQkvir7Fxz0/3ugUy4xI3YPD/I8KiGuqvIqg8tQINVJlS4WcbmS
Jz/05Ai9iPjv7/1vb+JOonnODYDS2wcImXJ6vXuB9yFCLlp0L+StCNThTxY1Dq1kYslNuPww2DOW
d3KyHM/Qwmhc1RR5lT0IATUufjRKL6z/OhtKSxC91NKgb5TgCAQiZBGUJH6UiDimg0Eo70qFW3w9
kvRuC73yJETMyY4EpsJTYz9kPMm5zOGzv2vKCW5L+vT+Y4flnj36s034PwX8jn2+Q0ogKtYjO9JL
VXcJsEVo9ji405svNFTfcNMtzMG9+/jS38WjyBOJY+LwjDJVMAYX0DQw+CjfGf/gA0/tahi/EqKb
UT70/xEh0N1SHYDmirVnQ7LnxclRyQEVgwMn+nZHdt6EUBIQsBC6Z7ycZxWGsvFsMvTJe5BhAdXV
7Im2ei9tNHifoYbt2fIybJeCaIDgVEU41buSUxzmurHdZgH9TFUbCcCJEaRF8pxf+267PBe5pn8V
l7mHAoF2U2OZTyNlyW4Lg8b8TNCFRYYJOFpDealuXl9aBlfb/wNweLBG0m7W5j6/0UR/m+6aqCkO
CHExiasvuN9TGmQn+UdCkl2FASBNdiZIFIIf+Dwij711Z1Ccp5jP7nFX4NDo6x/YYAjMIkqZrJAm
r98ZxOxyGcYyHQZgaBpSQhscWpwGKU/mxg8mU0vMTyRMFfqXkQy2BeloKgUN9EQB21KXcUakgofj
bbtwI3v1s5SbwlI1oavfEjSAdKqkatOd3ODDApiqlqinvA2hzTROqD9CzUk4UWvdla1YYmY52NH1
V9Kjntais/bPtBr6ZKh6ZiKnP8n9MlPkvBYuFGwCoReEBb9eLHPdFkWi6710WkZfQIRiaGi9kQ1p
wcmrLERBPuYEQ8LFCC+pJifFSX9G/Ynk9FPAwVkmMtX+OJitH/YEzce6+BKizVB7fb5+jN+hxNiI
A0VpzYx6d1um79jjwhGathujE717d7IAmp0tvqSO+aafbSPRWydjHXVghRazEEeCp//Np0yXDcNB
DYWqS40UBDKdwxA9wlI4G7VpCcJmnf5mvXcl67Z5lcQ8UsNB4LliTMVQgvVv2H+6Con/kFD+XaAi
6SeWBqabIOEUlDhfcJ0FFwhpMzJvX7gVWwzIUnnrnAbQyN7QgerjS5nqLAQeZwag9QCqBFRd5S+n
4LZxgFavB6Ds32WEV2QkUB5fPndqa8hdry7A77TKScjiPeSW59Z/IOE/BP9N5D30Vs8cnD1jXnsM
HYA0rayNreAuKhftcpSaO7JNhI5bXxXCnlZWO0qDLg6esGpONPaAeUkwpGINxD8Pr3L+IpFd275D
Ap5/HH1Tu3zFODOlqxpSLOPGcVkoSRaw4Psm/RsRusnECuFvIO95SGCMao0nm6tiMzPnZFa9yY+P
LQwbeIAcTkf8BjzXc+l4pL1bTBy0PzDpk/N8D3EOKDw5+x66dDDamd5Eg6Rvz8fOUcEOwLa3YUkX
vVA0p5fL2O5bJQwvmnIBW4t1TZUhBCzEnx+uiPdFkRMcCTquS8+QvIElbD2vMo+K2SLs/1NkOq6R
EffxsR5pHsbHPIMbih4AUEvFNnA/kmObMjjblQ/XQKVg8DlrN+lwyQMWLtUbOYoewJYwNKXK5Hs/
mQYUvMFQW9eBQwwy9vbIyVd0Md76dK9VHhquAeLEvk3NcHoTWUtUMi6nyz1z8GCulZpivpFis5F9
/OuINTyD2cgnjpK322Qflb70NNf5NlMUuVMGtSWsDAL81mOvjZ2SfyjtWF8k3atLK+BEL+zrfswx
VYeqs1KCMSU/N13dAk8zMop1rbPCJWuR1PBPK/DSVCbI1t10j3VVnIbKcAg8+AongTSDcgRSdsim
Jzho9R23tJ4YzPAFIgtZeoiA5KdPdG4SEYO/Vznf0g/1pZfYRsc0+y9s3ApEolbIC2ev7vuUaAUL
9jd+x+OY+hwOkCkIBApL13+yVhhcxAUreyhprKyoDf+5LH9GYb3VLOl8/C9i0lKRUrgEi2BCRqtm
x1PTjp8/phFa+Qk8VQLWlwerjwtf/OS95i3asWceNMqAHl+F3nUGwWZAAgBfaT1FCA7ntiCqPoY1
ehOq+DT77cxQbol3W2p70op/07DWLjYOpqjYCYUdYzi9QqpTfBXJxWHvf6pdwk4Z7TnVZ1MfjCwQ
Qr7h9ozzUN6RRVMZS9TO5p1qAQKJDqRlgYudzjZ6Gx9LNohB5h7rQouKZGC88k9PERmNuamMR686
YJFb+ZludVYQIwj8xVH48YEErixmFLMlSbNM1J1fKW7XQdiLuYF8Ot9Fq2kgZ0BDgb9nJOnPp21m
7pgkebk8utVfdq6DcP3ZuLBwucHQjHzriHBdu+WUJ+UcvNbTL6riKEEou8VmYZ0A63iv/bqhjPM0
Dfc5l9c4uqbs+moLyRPvGDb2LhKlYr2tXKKCv0bdrwV2+yki2d1Zw1vvgjBndF5qpIA43Oj7NxIP
JnbTvin8Mj8XUTBbkSQX7jkNtyhQsWIr82TV2rjznqYolD1PSkumBBQ7p7Jo7+yZiJxksb4gSk4+
Yuiy2pLrvNtZj2LZe9ropEqFnyTu9sE6epaTeaHbKf/d/I/li1OZkffuoLBajxIZut/ptCnKn2hw
vqQl81aQK8SNVOyJ79U+QtFbSfOTo+Q2aKO55C7o/DVdUqZKwhyJa8i6fmNmP9wI/BiXLHhK7rmN
ur+Ovo6cvoW//P3cehWzFfMoxyJihlKm7WaVpsmnTDJFx7hWCWQDMqUAQnxWrabZ+ZKtIfMEdRRf
xZefs1mIO3HLFzBZO+VSthk7tTWe6e22uzLsS3cpYUFxgYG/wXKlfHSIh/sSINNLUzbm8LKzgV0P
EN9nNXOJUdvWYwDM/D3xsKkDgXsGkryDI5H1EZ3wt4iHezJitHqYc/7KSE8LT9uv7QNUAjfMrZpM
rjeXdkP9fRiyMaRk59+UKBtbO6MYMC2XcSoc80wdFHVJOuf1PjwPOg9mR2zOWmfY7Fz3S5feYMB8
d08RSndvmpdwXGhW8iJZm1ktjM1biDobT/jpjyJqBaCNorAiQ6JF4gOeGBSj0n10ydeLbG0e3Q0K
3zNrZBZ6bqhoyymCqwRsLwdmEPolO0w5qGPzV7LM0oJZxwS9qCyd1sQBA+VnNPF+fFxefFUgVHLV
vAOLCYYhxOr1P4nuIVy6Dr0sNDyych6LfihiH2pn7HK3mVNm4dab+e5bo9bBahJwZj4B1eJ1zSFS
SEXdTgpls2lp2M469/s8U44En6T787n0eJQg7dSnf+1qBXVL87h7IoUW0sSDLpD7v5Z2owqoemFT
cU+lRXw4a0ykst1LHRjkmHV5JQepi066eLALfK1AudhaqmXAWni34lA5eUqlvPlnBoONtUgTB7GK
qnRrJhiHQrL1CwhuFeka0waLw+p5GzB1Zf+qFlvpUszeqbPTaWfZZk0vOUGFmeMTlOHxav9bYwb7
w3DIPNYNvnekOeov/w8PK9+uu2g5Ct1XyTkvj8w4CEFkCimF8s34HrL8Ly+G5iiQ+51BoMsc0yl/
oUm03HhTqz1F21wER0ldxM2qq1Pe/OBq+2+1IgDnfGV4NoA3fh8X3aQgTm7GXxzqjI1QmbxC+QBD
JN0fICvF5LB+mxyRhyG7T1HWyIsBciKmrEyDj8US9Fw1MZIuwiqbKNmSlG/h86MJ4PT4UXvE9g7a
NSzOxtR87yThLaoX4t/IW6DxiakX9MTIAzKEV4U2kdfyBueDiD5xDd55tarzl3t0hLjUq1FgVGB/
TNBAHjzAvSdBT8VAZLPecjn17X3wF5YkK47r1xueipQ8GVCpN8m3Td95RQ21QDYM2JXueorO1077
2XHIf3K9WrJLR2mpq1e3TU7ArJrCNkJl97Mx5mOVMWwEqvkXEETzGLkrr2McUspK+M50K4/Jze2M
yHlCgMCM1Ac+hxtjCeRkbhP5qte7UpQQK6fzMMM4eJjAuXzr0bRK7rnY38TAxgJ0SdZmQj4pCg90
t2sjCaWZYPKMLzO6RM8kLoBD75Uf4BtGteaa1i7kSMwcyUAFhbr8AukDuBLXWefc6DfJYLUyXXhl
8vEsMacTCmTUuQtPrNaSvmofk8NFyzc7aaA1NVk9gs7cYf7EMnns1VXoSJ0hM5ph4eCxarW88MSM
ovB7EX6VW57onl1b6/Hq/p6xlbYCwR1n1o146ktfOdMyzo0b46LV5xqVGu7uLqDoamB41xvjxRkk
Shjka6zAkipBZAjWwiCtAbrdjr6KWYr0e74dlw4bM5ChHB1IvMEmiScJxrSbo8N0frDp+cecqfF2
n+5ZS2SI7bZon/tX7SPL2aRsSWMo2Zm3rE5MZVauEHJLU0m7Fd6mSB1ZOtJDLuAwlQc5FnMuLwSo
/0bm+Sp2Hp61fknkgJmXRPzFyIlTChKCVKeAxOx1uN0qhG/Yx287kwD813vqvk04PUIeuNAt5o4H
+J2Vz/aEQDagZfCoGSe+uJQVwjHpEh0D/YyPgBpukMdwzUTYFZnkmYNa5QcPwjvZy3G23LYZ1iog
Rv5B7uZ22JUCo/8oCxKpo9e3GKk5rcWZHr8mRVxr5XB0dooUGwtzoQpyoUs317s2gipumn+xYb4q
W+A5DDpbumIce5/e1ahIT9p/ZbiOlxFl3RLzM42sLxBVC27ys9VMOoXRHnNuOHKTaomQD+EZyb8D
AgtUAzNbXkZEqQwq3tlFtivFqga1/G/IR07vs1sCUtPoBHEwL1QOUrPTOwZ9Wi5Ws7hNG651ppxK
dOT5W1BPw6pXVpm9RRig0QHXE2/kyUEhuiCX9dtzdKsN1NagFO59djV3nuXDOKLMHm8D/tL26IDq
sfAEMrtdWxg4rOD2aI7d/6XnDaKzgCoKnH+1j4GZV1rjwtrrRhAC47U2L/8h41ee8+Gd960TZMbd
MH/kxS4TnXNp+ywIpMMBj6PhvMcMOVJ2gWNhfOlF7zcFBcWNbVf0UWYh+OGiMy2ienhbK003f8ie
fR83EGQurQZ0d3R5pygrI8tL8ZgfuTFldmn1oDAFlP/x6FdDWpWVmQmugACxyZ5HX8VYG135K/eS
BU2PTfmwlVwmGCWQaLfHkvOLBoQbLWGHGMbkG2UeTyFUCBMYYELxPIw76lfPRt2ZgsdIKvojuFoc
3NhinbLUysmenaUobXPdvH2mWu7gHII8FmPnJNlHTgrE1mshSFE10vXhRGUcchUT0g56XhsOlbG9
GW7VAgYvEhXzRDPCGxgi7SWCDHFSuI6lSxbOFs2UMVJWDwBhUenYYu9Ovp4EA9yPtZO3SdXnAjIL
VuvORIzEGnTYVOUUvepB/c6qvrIBFttxOFUTzQkXnLENIw0pZT7fvV5pUno9HGmeWpwPJE/7t3ad
lFFJtn4fDfTcqYnwGCszX+vYvk6xH7BTQVMQMZdmRuGLSaJoiw8UJjzwa+BLhdrewJUPeCs6xdaZ
OMdLNLVRt7ehxGKV27v9LAoUVBjI+q9uokoz7WIKcC0CtPFGxkEIB/8pw8LAOgeGnnWxqtLDnnai
I9LvBbdjtq8bw/xrq6DCngTboc+S3qXx0szK4WzcAekjoNGvRfox3EG/PTmbcFOuZxs4Bpf8pIAW
pimXFB1sfEy3H+1rnLRHkOJHC3eXAm0cayVOr2ivVkUYABH6T2QsIJJWkz0jkNMXYbkf6l4K7y6i
kRlYfYYWg+e6uD/J6tRXeRmQ2QQ3k036Yszw9zdL5hXe2NY24IcDOOlCKeLPQls8p5swulqokMAw
1HOe2OnhFUIxdIID2GpvbF1gPidlszTgy0U9j3AHOWbwkgo/QSqHVIUrrZo60uHMtF17BtDDKrxh
I3PE5d/BJLm6LawV9gr3i7Az1AuNbrlsEZBGsi5kGKp4WSPEWWNQ3P+Fk6PINUxn4rnVzkiaR0Vc
2SrQn22Ud1shvDtM3SddeI/WqwnLgGwo2Mkkycets8fuRNpFDN8PkeoivwZAMmvUDK/IAWl0VtrF
jnjYg+zB/dAlu6k5mLD8nDNCnzVbqsAZ9FYR91FbQo91uqN0tjD/5r7ZV/hU79TDHYIAWL6JC0pu
bUpD70p6NiIiXFj2Vhx8YIb5xVUJCjHQ+3khjK40Up+Jk61BoxLMCoJBSX+2koBs7yjG+mlE/8gI
VM7EBMomKjFDlmiDT1JUkI/Uzn04Ju2ws65Tjgo9gjR2LlkOd53J4rYSrRisQimkp+RykPhWjmjx
9bfwg6Cm1ni0vIlzXC4TBVS7hfJjLIfcOQUlgLjFRI149d7zBzdU+TZvIhuVzS88xs2BT31x9Od9
90tk+kqf86zYIdVjAZ8uJk+0NZ23/TJVB/sOEGk/LlkEWP+RbPR44gt9xj/kktq0E847Ip3p6b4r
TfSziKzWsZ5pjVvzyXLuEznZMYT7A8t5u6CXy9LxNFvDWJI8UaJvj5QE1gzIuAGSdUrxmHX9xmfu
sTSE33hi7ti7sCTJDr59Cf31DgzHnYk8s7JK4GBiubpupaUTAScroZ+1FfRFTUVNMFlfkMXCWBZh
GsZpSW/3MwZ1N/ZAO2CgneZ0JSZ2hnwidh33QoW9R+640cr7EsJ7fd8Z4HhssccAuOZuocYEqK65
h3XyulUlRif2yRstPKCj5VeZvkOKpJU52ZJFMVDC160T1IUuiqLLqZa+DGHgl0jBthub/AN33EFz
N7CAJaFYIQhKRiUfLSsp2CaZZM7nt4x0Cu6mSlB45is384ew1PXqzmTsGOU2HAO8pkkZU1ZemIfL
KNiRsEHSD4AFW/GawopGDEz3qPzDT8RPMQqCYTlyd1F24LHQFiU5GEYQ+/0EK3RWUmk0De5dWh0Q
o3RSrcVx21gC1/3auedMkT6e4lRhh0wg1gWfHJFs0YHXeB2azGZXMf7E1p5IlbLamDWkR4bdm3eM
66Y+VjOf5bKgHgz4K/gxZfs5gAoxCeyec3NXLDUhH6D7e0npRG1/IDEHbKnvobU5zTI24kyP5C8S
/ze1iPnhiz3hIKe1QpGqm6Z488PSwUDspfP2p9bVNW7+yrUcZG80P6Po8j0dL/UhD+h3jeugqlHr
VtZPRdcCENWhSAsUSAEyTh07wo1hMXwzSqSwESvejleA1QwUa7ME4PaYpDg6Ycbj/ObZ875N3nO8
AgKEi9pCqimrm7yrFSvX+X/KD6aRCtVlnnySrOtoIHqDosLglGk2AktAUPHWrm8slRKTj/6p93ND
nJJMT1z411C8IophP/Cyr53rYvZK5T8/2f7YZBqZQSy3B7uSGyHonKBPFeCz3Q5MBdShV9azl7wV
frXRrqFWHmcSNEkmLEB8RvYacuIgEiyX/UQq8mid18TdPq0ku4OSUcjmIYMLtDwI3JLvB+QZX4uG
3Affljppikiull6G54/kntwc78oOaIVMhyE0iolYoLaT8/D37x6xMCE0jtWMNk+kLdkdmoRLxXcV
4MqMXkOn/uIoYdLqAJptUtDyllATQmimV/4WYg7d21AL+N37myNauM19wA1PMS97q3Z55yP4x88B
VjI/fCGaLfWqy2RhZc30R+Lo+WKmYRJFAC1lv6Y+d2qnc7RbV+TIhH5X44xjBjCqZOzV0G1Klt57
/a9ETN2iJqzBh3YaxR/duZWe67fHRiTIqM7HkfmqzyhwpCTe69uHalKoF3aV45fCA3851Mux1oKG
nhOMUO/eZqYZXEIwtwwnu+/q7s1SYkgZYh3FnO+YjqKFG8TZssP1TWovYAN7q3hwu+WqMq9aGXyE
Ny1hhIwHYA/2cg2tEjs4mWJosAtBRYB+IPyUKMcHf5NTrPzVq5CCvuQ1RgsPnJkRkbuqoV0VWsvv
GCKzMecjYO29WuM4FJ/gzrRnY1QnP2gFmDKhd87TCKJHbk3jq2zI7uj9M0vRhpZiggYWVGhZ3r9n
he5vNjqM0T/AR5fLoMV1VaKbR6bbtQ/Icgaeo8m3mBjwjdvds/4edVzsB/27/yqdFudSF736I7Vc
DgUHgfPVwGbKiYsPUC2+nd66u9E+9TqO8Lo76EPummbAmndVU1sZMMZyXcGnvsGM9Rkyp4W0Ksn3
C7g24YIEdwuBS5lIZWfYo+M3uzpcXiZPhhc/FnF5vqkrDAhUyX2gKqIW5szc5t14Z8vu4IhXJ8J2
eiSkj3LhmVitwBuLDzWoIoKOOW7wFVj7SYkzy2E2Pan7MXzJ8FtQXvxjCEHN5Ut3Zi7OTRvIwkNN
m6pdLy5rxrqdbvAHW1M8ocN7tHWv87z2vuiv9MuVJ/rw1K/GbKE69L7QPR7VIDuBVjogHD8uaS8t
f41QvLva5IfaLsM7JrVPSzmrslpydzaAPV9cQBaNfUXMx3TjbP0VomgSn5yYrsOTQ8B2jiEN+J5h
DEBVbIcdu175TokYlPNMQf1+67Xy53lIQMjGMn22kl51dgsJepK85K1W2vG1ynsNd2UBlItLPRpQ
Wrl5D9I7OrjqDxL5EbjczAZSYrg/GKkTKbm06tMPrqClNSWuJQV+uMw/NlAlk7dHVzQSRC/LJ5/L
pSPi84qqkA13zaQluVT0GU57aZnG0WqREkd/Mimb0Vwo3QkV4J05S5SM4q4lTdCQT4F6iPCDQT04
bnvnzv4t+qONb3mPkS6wyJ1ldtHHOy4FXJPz3B+YNxjzQkrqBipqCPijnXYz+SMQEHqbx9IR7m46
AyLapTVzXVwJbuVkqsocROwe6sXGysUh2rhSFP47wzd+Ul7Cyht0EwGD5JXaCpCuDGymklMregdk
CdwfkKb2S5g9tvd5EOCoNm6Vot/cY2fBOiOavr/5fUiWZlFTf5mJgtRht8fLfz6e7fOEp5TgrjJV
98/+nI5zeMgHwWLqtqyQamuHkpQ3ZNgYqexzcnybwroDjy6Z266UqrJR60SwSznoHoB7kNNR8HFN
rv2jiWsARe50LqNrgGSae7MZazWJX1R88cIlA5Oah8Dtc/atsWYr9fUdR7M4C6m/p64jtJa02U4W
9a0PTAWIxJcc9KAgqsRCY207fT+m2MKd3xs80smR1QX9agWMvXA1aCiPRVEsf+lS8pAaWEwu6swc
Xo7ruOa5uBiCB3MM/ENOSxCjGbsiTYdAoMZ4+sKSumk3tslai5CYP1CIbCrU325uyuCUMdWGV0ym
v/9ilcRg0SaO1MiZzOd71g1cheaNkRdUWjZotFSJYs2Isjy+eNlhGafVhLwkB6/T68G+D9WAQDqA
2Vl0ZOlfAgtvSNzgq2GOTGiIRKHaesW+IY4GSQdnLTm1ib3EjsnLoHMh+Cs2iho9iized3iIl5ZT
D46/HSIqYEwTIz32hYPfWz/UGKCnQ+BMA5m67ZPkkpkiHU4UcNP5L+DAHIrbMgYNuV8nnqbFKWa/
u1DaPISkKNOYqDm47wFxOeu9/4H12ECnKjezfExZ4wf6BO0uwbICIa61QEvBRaEaavawlVGrUlGe
tmGmKxyy7Q9c3EGhJt17569AP1cGM3Bmook8QMkc9SorOxB1Mxxk04w7j3B7qv/Bm/bSLnGe7M5Z
9q+ZDc1g45PrG5SBSu8rixTqZVFzuBj9RdjEmqL2G8wxTkP2hBfvaaUr1VtdY4gzeN7yKxDW28m3
AXVzEIEbUPN1joGbOQps5ntTtmgcoC4Qe34NTPAf0v/IimRs//lOxHJsgIDMv391TCjUPCgl4gbI
wL5P+k4zpY9qyiis98pqknh6+Jm2D5aGC8UBIxbmEJI8ykZXjxG8eU7jjE/NWIRr+8B/MbrHxpnA
WJTk2oDRQxSwVSoYhj0+UfBr6eBR7/05njOpTwJslnUhm1x9dEzyxTiciUyLZj+TetQrL/d4f6zD
0NpuudV/7/M0okw7iEn296sGuIQgiv2+BuNI2aEnu/cNqa5xXNBLUYj4koBBoC/70YHStQ271SO/
Pv92CGrmhenT0AVlyTOwpQopdisLfCpAagYyXP7cV17FnlT/fTQ3RO25J2Q7cy2QVjaU4OiL7HNg
MN+APFmnFgxcZ7l/omVyIFgi6v3n8JIn0fpU6nqpylCaaD63KHOxW7XHA/7EfUzZP3eTXSMTM/EB
5u+ymPY7cyZ6oljBrTaLzEHx3JpY8QV25AWBOg93mtmizA+Sg7hAceQFFlK9nroyzQl9yKFwT6Zf
sFKrEWYr/ukGYSxmr4BT1rETVvfbHs0bXShdMNUv8PuHenUKgIXji5y1bLBpyfn20jleeAOJsTAt
IWMmrFApLK2oUCH1NLOvz01E/P/jlChIzymma1xJCZuOPo7aX+blsKraYWbqL2oKFBk6SLnNMU7d
BR7kWD0NhjfcnlDwI8REQeG+n2dOrJ9h3t6T6kTrWo0x/pfR4RJtw7oeqQ9nOjQK9esW4xF4EVFO
Wnf+g0HAl1+mbzdOiLh+nkb8XArCiRyQsldG0nb+V6vy9tpp8HxiT0ct1sqnz+pH0J0NznyDrErd
gWolh1FwmSoiTYMEkpjK4nDfMWX5ZY3q3yLIrlfPXY3Bx4cmNEqAihSTkHvpxlMfsh5rgUeq3IkF
bVzHsad9gKLwGwep23n4eGG5LuL/qEwVktDm/61ZEKHl+8XeAX1RL6uvJu4SNWmcV8Yn5uVEmJsJ
L5ZtD+Q0G+0aB/Z+OcMCA4qSSen8o/aCZRWE9Raf3vYLvRevEmFPs0UPs2VmpEG3jad66sS8C6hh
eH0kEX3bwR8zbF4Ky4qh9cLJJlYPinsUCKMqGVYe9UDXeaoV52CEttVHEKoJ9FmAmZCGqBR0/nNm
WqT5gEs93Zl5JSu1O0abyLL7em7aR2WZhloLHc25v4JJGJxu69SFHdNwvUG2j7g+auNhCc4GIynV
/t5+AXwFI0Aksyi9LWgDSs+GXf5nJ+VEoUI8qINbMTIraHNz5ophO6EM9C3mgo9DIR4Ki/ORvTRc
vR6nDLm/I0Oy1FAFCs5BeewGk0Vo8c5yd4wU33rkKlpxRcg6siM6PXLOSceOa1BF8OBXYn9Nbj2w
LRx8gAsspvXOz9fVQWM98fsWwWBMycq5zn3a8k6M6XqZfprg5XSluQvpvZqD8U7K4rQ5dHfDD0Fc
MrdesezJXbX9qumM9xBI9vSZZhaOyNio7srw+XtVaHLxu7ArdKmSl83NZGxQqVKroPiGCKy0zKQ9
bwu2rOovNx/WXtb2vCSj+DsnwAHgs+YuJjcRPKnft3OmcD6JTMrzJvqIv3qe04RFFGpJpPkaA6ep
y86lTf5+MzowNQldsxUngWag0KnOnfVKMivI5lOpy0ptp+R7uIYOm/azsaPcDCswNedpbbviqoHJ
ZRDFXI/1tO78kLnZA6EITd0+BPjyGc+VW9qTQeqEgi4QOZhB4XCkV2dQze13V01JvuRD1ed4nzc7
10ZyPgfLBnd4Zdj3vS6dE7xrVNSbr94IdpFWjTdHyeUoTAzE3EwX5e1YTSAJpBDeL8J6Vc4YKtuB
491CM+xLeVPuxcmK2ADqHXM7suodBjr1QLQ3R64MIIHJv8WfM14VZLF999o5YeEW5RPS9K3RwtfT
456mbsh8uYmItIJZ/9CskaVy2yHfYmIbyNA27TKB/TiFtoZSKDWs2Mkxn7EB606T8t4HirEDulY5
lHoeCQHJfrlPnX7M+4EHFEEHJeLwMlaXikU4YSU9f5Rzqqy67zFSkWmIzOGsHytrtOloDUE0Ie46
xBcFzoH8WVcfp/6wt40o85r1TkjdxkFPy0uaKUhMoU+CIWJBphIKZ6UWoPWLoT5XUWK9YF4FcPJu
JUCq+ohOW933m7UnftBArTqGSrjr+G4ccL9GKeMskmG4c2qAhe787Oq+SniWkga/+gm1igM6ktf+
aZDCTwAocmSaHeqGJM+ZxFB1SFp5t898y2H9JFEqV4bpRdBb55/ITLOsCmunZCNKDyT0KtDWnhwk
TQoveoEKucvxy3FLlF9Vp7Sh2KFo04t9dHRKjPX6FkP5NQCujdBZzVCNQcWJUBS8c9e/XvxeAi7k
kKXJCGbr57iJfiV5ZVN70raLJZN94tC+zNpf6gMANoI36DezH5tm2QkFpVjuun0GQ/FV+evJJDQ6
gfbVbkGIX9oihscWdml7OMgxl0c5HoJN3W0DEiRy6+ZQ1pItziWV2sB1OCaIbMHjuw/+6xzAZseA
ra53TtVgPN8mjoO2aoy+PKzNjlYTjQKvRQIX3mGKaq2G3mfhvVoPxYBeIeOiSCWZUe4dvRui1JDw
aDlT7R5pzif2nzfrIv8cbwfQi7KfZf9PR6F/q8qvyP6Fg4KLP6ygh9AzRzOLPEXikShv09Znw0w2
cF19SdIQlcl5nHw0zVxOJzy2E2sNb1X0qDY8fr+1bq/8zXvzqKjAJQBafBC83sstaNu8YM0+7WbL
YTh2/Cc2tXxcEALgmkrKvmwoIRJtl313XAP3hSVJwkFEKLOXMGfULB3LXbMweUtPnuy2L5CMp3Ys
nzGGFTTBTjVmM8326wtgxHSdiPkmMh3HmTuhBXVuvB8mjhJdL7a64ZLSoS1QEuLm6coNfhxCCqRf
RQLFJTAtj1YqypM3NQM+bgKpJtmr/jCrjVnAo/hA/6852cLn5egjIILk9srDCY35Yj/Pj8h0WwS3
b56z2P1hgzpmmk6y4JzgCV344IC/jEOei2OkiUJSNwGvuaOjKRoFLM7rAfM5xrmnICZIBQJnGWcB
imqeO1jxjOITfUOG1z8oWRPC8uPuGVaj5FuS1G51C0kQ0+l2d1jGLqZTLipqjW8od8qVnrgEQk1y
/b0K5nON9qorq6oXvgic3irMkWyDC0oz3cpFBKjOLqMVe+GI1Vxr4sRL7H6Zup2FfYc49+fnQw5k
yFdPddazlO10xyfMM+7yWrhSo6J9tdHr4NS4PmxoyMzvGL8bZFhyArQYDTRRt44FovatdfcKF8Of
cRfubEIZJuiTz2oL9rAhOBgxmYWcDDqdPbNMPNszaAlGfyUTAjLEFu8MBwSueNGTJ/LjQX0GdVnD
KbavsB4Qi79szmCtuNwu5iLwlV6HP8IqQ8joYxXL3TKIO60itgEMdF4LWmdmATLAXauBuU5DMKIS
t+8nVePoFFMkptd0gJV1SSkj4iGjZ2lhlgfrJE4ELAfp8hfTENF0BEWhlUE2VqGVWI0yIFoiAqdr
bL4MxNDfcwQwfVAwGS8UH9Jkg5Qp7Lrk9a+jYhSRQJooRTlvAZ9VEsJAYPiwuUC3YvkIsQlO0vzd
ku4GTmmTeC4+Tk9uhG9A0FWH+xYLyKzbhZ9wDbEG1K2PTwVCMFGidsDJ1pS9F0PYszwOG7PFKtwW
6s5EbN5RpfG/xCyHNwidcpRjydX+g/3uzZX5k9UpyOYSh/ypDWW6MJO41PXflzmpH+wwjy4JAAQS
/gx/NQk2LOBm8Hv10HYheeduBX7RQnTYHmrAMh57j73Ol7QiaVxwIHpDAA9Vn2djzmLg34PbAu1g
2kZbADM7MNm6bIk5GLGoMSoJkc1Jgi3NTGOEb5bHn8B5suF2N9hgN+KMSm/tz0+j8ZqzhoPPiU7h
nyBrwj5ieRGMWQKpqyRAAz0u5I47e7GOjnvcUdFCoDlFmXBqLWahqAtspacRzkNuWVb+cRZRfZvw
oiO9jA5SnKnQB8lh92e0j/UuRNog/rw29uhNf6LwdfPUT7zskTlS9K/iBMAgU/k/eKQ2tpuRR0OA
O2D4ER7GrwBrzBvUP0QaitHbzqpIGyP+p5aKa3pXoKnghqasfe5R0WDBf1fTV4+3aQPTq39Pfatu
RTDszBuc0Jw7fpjA64CUev/jCuSnmQF/ySNqQXmaCP3VlJgqvNPB824H7WzBxYJp3sHk7DsGm6I4
EpcQt0Q7x1ONxPTjJF35caUcekzAfes2xAKy4tQxTkeN4J+Sa38Kt/GG/sHPvfG4CLnc9DAnRiZ9
sf7KwLdIm9U78qWtRExOstIS6emItMpQW0yas+1P1f25hE91BljyFPUZDO6ZnRTI0QLEdYZfhAM3
Cz4gUpRkSWKxm/Ywbh4UCcjQ99vgaoj8CCVlhdGWj1QCJlDejFeWrdzg5C7YOnNq4R/toDh0YJvv
A4ZZyU90z9YGDryimBaLXG+OU2rvAK2rTA5LDV6eF9fF4a0hn6wn4dtcwyk8+5+dadHokqKTtjvq
mcVpw8U3nJXX93HjhwCQ3radrVcmPvwc+Eq4DGc5MD8Mwar7CNRETiOkKPMyAp4zuwWL2RJSP38c
luy1/QRKx5hwef7tjbiw0M9A4VDFucFXoVB9PRTuxjiYO5sc6HsSv3bP2WX3dWT/NCNrDvHktO13
acZuaEFE2nGRlyvPpIN9CB0pJwNpldL9y98zhOuCqAMCBEYzfF+NQckHqbtFjv5c+yTl2d1R7ojT
psJxwt0qV+YsJ4N5EYtjU1EIQK3TqIJTOql0llyn6m1PFT+lyoLzSKChBgUq/rtywmVuw/aYt3kD
Ln7k96U6LYGyNWzCdXYK6UmgtONRiIJqmnz2XbOjmUumgoG+JdFLRCjWzFkYU2IPGH9pK6qIkCnv
36/EGQizK4NCVryhZhUgKhNMJPdu8O93KmWTyB0I/IgiXr+YZ9uREauziE/r96EbcrHSAOn+EskT
8PjWVd+2YhHAacopgaVUjMvly8UThy0zSDsz5NSAM5qjR0nAFDc43yxrJOqeJXwrFEiuVladzomY
R7GkG1a5zcjC1YMAjhoCIN9AcWu/na0Y+LDKgxVvgW9CPSfVsLXB9GG+WteEBxZyJxcSHBaYkh2S
jfebmZVv29cTA0faHE/cKSoBkrfM+QPjNzPR/tEpYeIHZJ/sBD6VoX078TpFRbPdDZJd2Yzsyzl5
og/LpNWYporuk2uoBYMg9CH/P8RkEgtub9dEJOexBJdjs6P6fTKz+LUtGFod/zRY3aypJxHQ4frp
js+a26qUUX3YDYvQlI835/JlKtAUBEODJ+/7LKn0msFk8/46KC0uA+K2Gtpx+eG2feTEKgo8L4P8
1amIlhZFLWxMVJK1U38MwKKJtQcoWUCGfafPaB+SLa4Fr+gforgNI0sZG6/kuRRTsw8ptRW6KSSI
xI/N7Y3s3HiWmfNOMK1IDPBSqv6xlA3h7f8sqW/Nh9aP2PFC++gTdWIzW7o1Yslre8mNa4B0xxNR
gDTBzs/dzW5f48vdhYXpK33iTMiUcomd51CZdw5sAvOKezdfvfrYsNahdvMXHdzAXbrb+7IEJqDJ
NosYKBFq9mCimdXApg2bmm2lqRMP521bFMPrcxUV7X1d+FNjcZgrFuDgAXSz/llvKTRVkarqrjSN
vIu9YbhyD7xgc5EJM73TgzKeLwbBdGxatguV0wOKnxMdGTGvBiDzWxJ8kJjqTRalwfdcZge8rSNW
VSo/K0t468akCEIMP1NtBgKL7dKjqqbarU4kF8Xrj7ivwxrOOJN8rUzXYfLL8sLl/vRQfVJSV3ju
C1wxepKdvfHtc4NGjbu/8+bUsmmnqJ6Rc+VLjPBuVz6JtAPDMQRrY9LyaHG8gI5vuGoFlIyuHj49
423IuuWfYwvjSap5W98x6Q1tM6y+gFGrIxKGOWIibqMzQshJD5DdItBfujhXREff6GHsRqm5SkC5
EcGgHFnpwW7HjMEuIX2TwC4WwgzqEpUeJES3eu5IjNEzmzzJKh3/nDCmFa7GuChuodSt2eOlCDwz
PqRtn0DF4nTdxx4xycNeF/p8c7m3/D6QmFDFQ/BhvEr5ahauo09/UQrtYLcyY7Y8oK9wEGd2DSh+
TaUlWVBUcEbTL0jD5sVaDK2bA4tWfgx+Mh09GQ5Ewz65y/6HgezwFSjN90u1SpVvwnH5+T/Cts1Q
WTpdp8saRGmVyiecBiiPc2Jd3UTXdYywJlCfNPug910c5Zo8G8Xrm5/byzbQNbihywpVA6tkn0H6
a9Of2TMPFCeMc/4YDUUtnBwRkAUFwhQOeWzPt16qgpFEeuE9Tx6dbZqc+aNutHgn5T5Fuz+C3ODG
nTfDhZt9l9pkYkkM0KAivjxKQ2+BpadHBTcPBlLU4u7qRr1TldRO92GXi+x75TnhPkRXOAl/lQ8X
BGITDncJCR9k5mGRPz8KKaJhSjLyOQ6xBImX7y9QQKsBuqgoPWgK7nQJUD2matwqxiHBhPQoHdv/
UgzymuOKacneN+EfySJymB03ZCl2i7sVSksWkvRZjgj/rfsQ+FX57xZ+jTp1yf8HRijmXfTxUDiU
T/VmGZH4cMNCHe+khYcjo+ubxYcNDkPUk56HREVATHitFJ2G/S0EjYSn3ByQk+3iHygDOhqWCPav
EfHLVe7ZKjh4Y+sc6v8HIYs6BwtOPdvNuUcvIIxhE3MciGr3xNmdzVbcyasFB4VSmvWZoW/VbSHn
2QdAA6Wj5qZOs60BkDaSLatJ38HT8SKmAhvSimiMv693nuTwSAgtLoarHp9T7CtW5I0AC5eFcGMg
wk36NQKHvuv3dE09DN/qY0hZghSHx5xXhugDDBOgi0Bf49nN56oHDAvrcO7x2u016HIENB/wpE1N
iT9KPJZXbTP6SP2D7cTMif6ODzvbjA3nfTLjLeGlaowFJdDGyPQMubu38sRUZbJoXO10WlFNybLp
aEdtaF77S/CRQEJrm1ZTKbb4chRwwyWDC0IaFf/EaICcPsYjvL4LHq69rRic8BExJDbYHijHURRn
tYJ5Yy2FYc6K/6e6cRvQZ4WEtgOKMoTau8xyK8bPfirMhs5AjO57SkRV/I4qWwN2SsHFMm8fivmP
DP1ho4P+El17afQMa/RzoWstW+aBgLQ1h3qKHAoT6/ToQknm28U+ORJ/0TqmgfmF7lF1Qx1NlmMO
yXwAM991JH1Lt+zUOQsODFAKTqtr6N2TlkXHoQMVDMwbLiLCtWFphyQwUvP1ZxdnGZoRDVTHrh/V
8qOdwE4dOqP0HycyYG4fRndL/cerFzpQAXMC1JKv7WuSLUDSVRL++Fgpezqj1SzWK1v8f6jeV+Cv
neYPejZmWGsHBWcNcaEItmaLDUE4jxRqCc4zUzwtSK1J/JElxj2CHtx28z3zIkYV+670Esryu4yS
BVquk4u9ByrHp1G2TWBwQcuPFUk7j6pc63oX+VJZPz1AsfsB8MX0/FZ+4w9/6g09flqljIAdn69F
vCNMF3yMUbGHpjlPeRg2hWxU5OVNMrLUU3EsD9KhHcEsSRuOvPXi32YVBlUjynBH/13EjkuWDkO+
bJFDa1NWvczLkZQcfOC6uq+XAB59KAaKbIJhv6FP4qDmK+b3BvGMXJrk0ZwAyRQIkQ9ij7NRlvi2
fpqzpjruV14VxXT9buZ3cdfgosDCE2dI/dUYE1Hiw9pgPNvzLfHRP8534ZRdOXnZMpcothNGTte2
33RBUyGqEyWU7FXOXZRFbSjv6XtWtUl4gBYwIY4ymGqlzWGVsGnPcgVlyggSUkTLB9nEwZA68Q/V
Ym+3ZNOgdHwr+FAzl7pgAjWErSUtri4D1m/nhBIkqc99YjGVG6ghQ+KKb2xDnVuvF4Lkuoa6n7aY
pZ0pmecXVlE3UzqxMXT4hfly4Me1JTASf6Dis9mzTenTht/xgN0sTf4Ga+5nGc2AvhGSHHxu5NsV
WK1lJC4Wtw7FDUqWIox6s+hoTf31ms4beW/rV+IRqDcSjOR716NkpqyoGhZDJgvbCSLLbEqtpkIH
EbZmDQYrjwEnbIphjz8cFQ6C65y27vlCThCEhX40BpIn51X/Y7w2kEH4RZgJRF/hrOE+qPe4oJ2v
7k0aCd8EwI8Zp+oW1syMkRbi8HRwESjiTHclqWuWlbvMmdWMna7M2YFurHzeEw+/3ZNyv+oTo09h
/TrvNUG6R8DbAS5sSyWPddCg6/7bwSy0dYs98T/NgwHCk6yDzZPmo2U4r60oe87a6h99+BpVy5Up
7d6qKhT12L5XlDocJjFSuJ/kMrpdbZkLGlse/jRCuqB8cV2sWYl+fMHkFbw3sz3QogmlB2MvlNTE
QnFRToMWHyly5iMwBxokC3P47gH7jjIPUXP2mbE0kBn8DLoODl6xQU5SU1mT/JD+t4Vf9z0PzUyq
UUfLdswbxuB54FHZneMjMz4PpdfuwmaiCUsKRBpPjlFSTlHcypdm+ZoDBjWfNBXiF/7qpzinwcKt
BO8xYd60FssdRPddD5J+kbdyJ5//8WEx7uizuLKCZq+eBqqMX3XCw3dAIlOu4v8lUv4G+MNB9nAk
qOz5y8NjDLBQFEUwkRbpF/I1N+QEaXLU7AoWwb56wN66DzsEKRjlgkKnBZxjJxqwHpxU49GKmHWJ
rnMuLdJwvML5HjcfAMr1gS+4MTYg0eztatMOC0TDAZ4kNp11FrW4+/VYTB0IjWEImle4Hx05ZOIN
it5G39/iGXQ5gblbgyX1GbExp+Zq0Xs61rNn04358YLN/cRz4wlKERQ9Pg1Ua1kzqwyGu8UOCWbf
1/vUWEUhTXnzaRyqN8fkzjUf1mRAmPk71qcrNYab/RpP1qHvnXbmYPm7YDXwXMxySiIa0+2hEsQ/
EM+dt4dcED5pQaH8ZsfMwIEg/6Frq/xJaRRs2ji9hnSqyv41UiEqA7zY583aIrCRhT4WhNbbjspd
YhAVXSe+uuRA8eXZt5/67FXUSGbH3Xr7eiXV8Dpu0BkGGtzZXSklnJgR3v8fVaXaUwjjo8VD9otp
x4ZiiG85uvla7kNwQcG2cE470p8GGQBCwfib3QKo4Pqya9QRdfM4epcXlWogBA1XOGBLyZSQ9dZV
zJMS3SzzumsQjs55/I9jmoSOj0CywbVzlzuOZKoBlzG3GfDoVCgaeSCIyUobnCqC75J6+Hx1ErTW
iBVhz+wQ+K5rH8pmGteeAaAjNqF4fGy/KXw63V0GvmtiWoFmNFTygy/DekXRaQP0jXLB0Gy4zYR7
nuFaQN3Jid0juWZqtG0Lnwz6+9c52Lk7iMvNFFVY4aNRWgR7vzDQJcsT5lMnEi//NSkKtBwVCgfQ
EBX2x6wVgcawebUGMAlRX3fxwhR5LUp7vNmW+ob15mwzuU8ARxtye19PhbUhlAcKwKBmL2Q/Kc4D
/j4WfmMk7szpvChbMiIiL0dTbQqQjDZqNlQkoarIfFeXiVaIGuXFAE0OB+8fpOUADk6zCGF7eVFF
wzkySNrsHjj7Zt9Ybd85YFsGLpqHzDimj9GszLFmIlKcQoDvcBAw5fCMZ6VQ4JG1AZdSJz1ttPIV
10OCwU4Pwmw/fC00hEYKQ4b3UKt3xTI7np+acg+I1qBVL6uiXuvkNrrROwecUd1qEs2EigfNiP+v
hw3CqqCYWOem1bL1r2HSulrpOLH8XNttg9sbozLeqn7lQzjmsCd1M8/PvWGYNC5+xRF3EKmkOxdY
OGjlWO/OEDyEOt8osgAs7pCui6dY2vLbJSV5NTVSsoY+o30TbKqu7TFNaUBRsQlDNjbqst61NiT6
AWg3/CaaAyKQ3SMNc+btfvjWPvvIigZg3TV9lVvg2UqU1veievOZf+ja4gANysDZ6Yo0Io+A1GtE
j4lV3uDQlB9l+NcKzp8vtGYwDv8JHs1lnBVkGQB92e/+krUMnFeFrJQzX8Jq6YQMTakr0SCs0mS3
SA==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
