// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Sun Mar 14 00:30:32 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ vio_leds_debug_stub.v
// Design      : vio_leds_debug
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "vio,Vivado 2020.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, probe_in0, probe_in1, probe_in2, probe_in3, 
  probe_in4, probe_in5, probe_in6, probe_in7, probe_in8, probe_in9, probe_in10, probe_in11, 
  probe_in12, probe_in13, probe_in14, probe_in15)
/* synthesis syn_black_box black_box_pad_pin="clk,probe_in0[3:0],probe_in1[3:0],probe_in2[3:0],probe_in3[3:0],probe_in4[3:0],probe_in5[3:0],probe_in6[3:0],probe_in7[3:0],probe_in8[3:0],probe_in9[3:0],probe_in10[3:0],probe_in11[3:0],probe_in12[3:0],probe_in13[3:0],probe_in14[3:0],probe_in15[3:0]" */;
  input clk;
  input [3:0]probe_in0;
  input [3:0]probe_in1;
  input [3:0]probe_in2;
  input [3:0]probe_in3;
  input [3:0]probe_in4;
  input [3:0]probe_in5;
  input [3:0]probe_in6;
  input [3:0]probe_in7;
  input [3:0]probe_in8;
  input [3:0]probe_in9;
  input [3:0]probe_in10;
  input [3:0]probe_in11;
  input [3:0]probe_in12;
  input [3:0]probe_in13;
  input [3:0]probe_in14;
  input [3:0]probe_in15;
endmodule
