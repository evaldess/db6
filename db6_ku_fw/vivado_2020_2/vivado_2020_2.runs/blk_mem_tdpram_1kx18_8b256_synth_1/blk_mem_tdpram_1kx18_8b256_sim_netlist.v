// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 17:00:20 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/blk_mem_tdpram_1kx18_8b256_synth_1/blk_mem_tdpram_1kx18_8b256_sim_netlist.v
// Design      : blk_mem_tdpram_1kx18_8b256
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_tdpram_1kx18_8b256,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module blk_mem_tdpram_1kx18_8b256
   (clka,
    rsta,
    ena,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    web,
    addrb,
    dinb,
    doutb,
    sleep,
    rsta_busy,
    rstb_busy);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA RST" *) input rsta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [7:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [7:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB RST" *) input rstb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB EN" *) input enb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [0:0]web;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [7:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [7:0]dinb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [7:0]doutb;
  input sleep;
  output rsta_busy;
  output rstb_busy;

  wire [7:0]addra;
  wire [7:0]addrb;
  wire clka;
  wire clkb;
  wire [7:0]dina;
  wire [7:0]dinb;
  wire [7:0]douta;
  wire [7:0]doutb;
  wire ena;
  wire enb;
  wire rsta;
  wire rsta_busy;
  wire rstb;
  wire rstb_busy;
  wire sleep;
  wire [0:0]wea;
  wire [0:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [7:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "8" *) 
  (* C_ADDRB_WIDTH = "8" *) 
  (* C_ALGORITHM = "0" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "8" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "1" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "1" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     0.32832 mW" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "1" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "1" *) 
  (* C_HAS_RSTB = "1" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_tdpram_1kx18_8b256.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "4" *) 
  (* C_READ_DEPTH_A = "256" *) 
  (* C_READ_DEPTH_B = "256" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "1" *) 
  (* C_USE_BYTE_WEB = "1" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "256" *) 
  (* C_WRITE_DEPTH_B = "256" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  blk_mem_tdpram_1kx18_8b256_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(clkb),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(enb),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[7:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(rsta),
        .rsta_busy(rsta_busy),
        .rstb(rstb),
        .rstb_busy(rstb_busy),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[7:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(sleep),
        .wea(wea),
        .web(web));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 30928)
`pragma protect data_block
npIzyn3E1fPzWTBIVjqYEvcYRMoDkAuLqClW4wnNNAUqDUGPMD7hju2xSSGcd7ANbMake/al1CkY
k0UxITFZJZDDmvqpQ6hwd66L8Hc5YN5XOV8fAigEwTeBu6D818ac8o9Dgf5ahVseOb1Zl+eTYeKg
lI1zkizdyL8o0pr4+ieiDyTtopzMvSbitME2MJ/jDxMCacMRjFZFgDornHTI1m7buDjVqp2qZgXh
jCRz2a66ZvAT2Dz4WQVzmAb+cuHQHtHBD/Kjy/b0IAo+q+D2PBD6xD+hmiP6xbyk8tdX5VPOOrKJ
chUJKkMQXzQBcHNvbgYGClT8UJ5tcV6ul/h2BMYhekKg55ZY78BcsmtGrY3Y0csrHCcsz8JoGtVi
EVDYf8vhh+NLufAzT8GvHEEzdU54/HUkD6ipG9QO449zU4rigQNXgLod/BnLWtSx51bOfnUPZacx
bePTcK95Kkpp0t4Hx2e6vxRMnVnqNT2GLHW/+zYOXIEA7FrTPJfda6WMckjbWogH5PmNhnzqgz/Y
63zrMCdhppSFoEP+up4CQ84rGYEfb61htPALE+Q+An7TimbHqkOtyTmen6+BEohgtyVryaANKOLN
T0l1H2paAByx/95ZxQx1cVWtIo11TELVz5L8f7sHweSRwyuGGpM7C7bXcY3d7Hn2+C+iqN3BGqVF
cDihdZk54dl+RNnKoJIVw4bty8YIkkgyxvxV0BN4JNOFpZXX0wp+BdcEunHjjTUWKTjcq6xNYeL1
/11itPzVe93nGwh7i0db+roq/xmnUvFxsld2+gUMvFOxI7gefMXfupHoSc02T2QkgoLajhklZRjS
yVld68YS0/gsmWlrsX18tSrJSlE2sFKNNh07bjlN89NggH8OZm8sQj/f+2nSbcFzKXBRZqrzC13C
NwpQc7wS4i+SAXENWJQheSpGHyyLOQzP+vDYFynUv9KjH2ef+SeSbmTxh2Ud5mVrQAzSs/JaZxZ2
w4u1nwjhFlg2nAqYuS7eYz74blA4nO2AzvhdeF42JLcsJh4TmtIvbMCs2sJia8qujgc/hGhtWO9w
2mvs8129TYuhnaIkCZutwas1Nh9spcLRXplHHO+JDSuNzZslNiX9NIribqiVWfyUEY591Xo1d4Fa
ehIoFcjy04mK9zj62W7epM53dQPI9Y3L6mpNl/cQE0eHrQsSGKoQAEYexynA86QhvihmanwWJ0Ai
Aw+40w/ec/zJvcRmd6MlUF0GwH4cI41zM8/1Tv4ymzEMn0K+Dm4xhiqOSsdnYhtisNJ8nXLkgpuA
0VB6mBcfdYhdIBM1DXIwLNr3IeGdtkTN0VZ4r43+fCwmkxJJdW03qyGAAfaiU/VkaDF79NqafEJT
L1a0a/z7OXCukdtAO9HAgUVSh5abVHFo1OFeYSPDOARz6SNSU3RkA5nzc52J4xRzffKRG4l40Ix5
PhudNPQBjYGgFeVTgf//YViZDy6HDiojLsVTQghQvZ7mnC8pGZ5G/Ik9TXxdCX5vuC/6+2ToVRco
FhhraB7GNq27vSf4wi8R1VjLppb2hPGv12do43pluExJfjSMsXrWiVR+KR2bBSMnyf4hADUB3PCP
YirHXN8idjYyWgrjoABM0OKJSXeu3naYphz71G9kZ4qVHnGLiTwZ0q4ab8bqOPaAAym3Ofzvfv0C
yTJNKpUHCbKHnX4nCVA+41c0MNu1xdEADaCueP4VpZgIOyYyTJhqA/yki/YOZd0L+J+DLc9ibW+0
eHIHBZ86sNDO4z+v0D+F5h2j0iQM4RWItO5yzoWy2bxbFjfzYCuuCQ5phPJ34GJAWJz3dnHjeNiC
dyHbfpStupYuiY1Wp7Kt4c2GHFOxStpkOYGWO2X000kc8oilomJxkp5IWqqwlB053v9E+fI1V2JW
8IeqxFLMu+p4W/GcffLh6bXxhgPsWZikdakqRHHhGBXof4vXT4oBQW01UJbBhgOuPZp+H9n6TU30
KmpKwg8lLMzeH+1+ckwTQwatgO2hoefWalnhhlozWwX7pjmcRhisayRyTN0aj0nfX/mfSxBdfgKq
SqBaSaABB9jhIxDAhButIX54uhGAdeYZ0LhVraEdjeVh0GGpKkoV6quiWr8FRW5PjzYQNwm6w6ED
KQSf8chX31UbLR6PNpgS03jVyQvEz+THmdRnqiRTAz1zETOVVkuqh8+6BHBGuQWsvd71BIe2T1cu
ZFsOKCGejICz0MDLIDhKjUUg86IoWB0JHZTUYdaoBLJWsh9V0E++5+ooKyYk3uIuCbEBZ+0JDu5U
9ZF1Cf4NIlC421hU0/BvtWiouf7a7XAeNCbUNOvQ/HmaLfn1h5uof0h84aZyQojh9KTJVO0fqG/W
iY12MbSmsZFRNHe9hzITKFiwcv709kfqoCfAZhvBMUqRQVC5z64jYsgRP6T4Y0WTPK6DZo4fSjqW
7o5Nlf6M//0luM73F/rmY/mfueEr9ePATgOQYJiHwNYyoz0i7TuRJj/H+bhzi7CHYbD4mgBi5r/V
OPba+xIDSKB4Iebw72HFjbX928QiY9npxItg05OjOHPcfixKGwhWs1s/Sfoz6CaC6uhKEt8sdGq1
WWgyhSvcZVvjGQY98f3NkZ4Tj5FCtXDF+ffRd0hjke6Slg2I2DoJZExteZhfl6BPgTXuUiQpPzAX
ndh9DzBrhky2zptvip4b1GmdLmhZ9NvX3U0MWvg6I+teJj4CiX5bz1pCH5J/TNGmW2ww0xJrw5Fw
rBGXtX5dp2LLe4Z2auEfC64euFi9XAU8W4mwrdKA1VmjAkqMC/fxRRZd+ZrdA3kfrlVD+ij2P+Zf
rQqkBTV6pE1kyR/Ez8sNrFolHLf5ow/glr/xcBqqaX3+7cUjpShzlidZ6+3shVS1gAyJT2O2wjrT
urg+jpV3d5nDlbiJeYWY3qV6l3zUwPk+k88KOvYWDG62Fn+enMWeB9Mb92Iy3VQYwdcNsNtvAb2y
JXfSaQ7pmuKMOPtcXPCSDlia0W5RPuO8BmyGcmNk6pZwBzJt4HIjNyEk54hMn+pwcO84pYxFtoYK
XE0Vt9yuBFaLYJK2a9psUp40b11Aos36LcHb9uvC+JgnQanSwG9K80iRPKbJtTDSULa+/cjcimMR
pj5V0pQFOVExDW0u11tiSG1X8bU/80OM7dA5oicgHg0rcq/w7eXilxdGLQUpz7pftSD3297yPHR5
FqU/rpVCbIXd8o5ydewRUgGUOQOKplJrf9Anp2Avk4dLqy88CkZk/VXk3XleX8T8Df3K2fHGqoDZ
3B/oaRAfdh9PxJqAFlM+lPgewtXXVYBHxZHZR4jT+LWmd/FGic6Dj8A5DjkMlzq5jXtV/PtvBiew
ndTjnx1Ibe0d0tbBr54sH+0JfsDxX9puGBY42+3XaSM//dEHK1N01j1EvEio5iIttqB+wH+4rP25
OTbR1ZDCG8pGlxWmAAVJy/PQTtfhFxNameMBE8MD9sDULwAvMEYNmOEBb18GpHKS/Bi5ON8pD6e5
tJkzOUezZ9F/0PrRiQZGJ702xqMBUf3lMlOqscl2mlSHWXrDQJig2tXW0HZ1F7u0lC/gQXusXkNq
ptUGMdMEcC0b6Ve62JEBIsAsx2JlVx7JiM+TJvfng9a/CZMHIMmv6AI9pPjOS6znHIofmLc/WAm4
c/Sq0MHKyEDBE2w/7Pjuok+wJrw6ysuEEt6d7w7eImlInoHpU+rueztEredCCaQVWuNfRc5InU0y
n3Z/pmK62Khyl2UnbqnczqzGcLIMyyVwvj0+KcC+Q18LYG5MmibjcQO3hypvkJk+tZADmNihNECO
pq4uXaYZvPwAhnoS0GlzDbKpwobkAjySGuJOHYtCD86m2CEjxfZ5+ELdcDyrIDJPTHbd8PlF5rjQ
1V556U1Ou4a9u6KDj6oSCbe4q02jTxP4Un7qznFE1G2RwT2hpa5/yYx/t23+L5HkdyiXoc2o4CPz
BmQ+V7qbmUDpS5EJSf2y5HxP2VqSSLZMz2/Z2RoyhRR+u8ElueOBS0wtZtINhGlvUKjR8dHc2mmI
McqiJGqemITOXYkF8pFDB8cibTlb6hFo1RzMojaJkPC0CckCW6Wd8nRbWMU6vX/kUEg3f+2TpC6/
bnJ2493Hj98wYmrlpteIgUyKffTLK9YSO4ov3uZVWoum7xxtrk7tJK1m86GQ6zEfbVx4EEQ4mdF2
+dojAzbPpS+D/J9Ne9FrrNGcuFT+bXtbQJ9qiiJ2OFNUV2lNqCbYQ/cnr+xg1FKwn18AhHqzbnww
+xgcnGxdHar4SQFvxWqgolYgNk2YxXwJLyAXufdWpKn0ylT5pggXD11MylGdDOgaH2LlxHXrKPVG
BFG5P/5U3WirQyzEewsRVIyDZbKrP2BsKg1DPrFAAXpU2Z3yx+zjYD6HADs8Y8h8vSIns+uSkFfW
gM6qlYOo37vhy35zLXfGXpAUvOlggBeDvImYTo4DnbY62JU4M//bGlOdeBYuR7M3sei5vWUUrD1/
6ju4+ojotso8vPJVfozX5X987IgFkjzj7qNf1NT7CAH1ewr/oi/8tV//qO8vqR5WrbUS83Vz7ILq
W9tHyfOnPV497820y/WCnMwZApHY/1I9IkNCbGyBIte/+tQadQ+ajUdwffr9E/Z0JVYsRQv4zBEW
9rozLE+Z+bDWIANub0KmT2xEzQxLH9IQa2nsZT+sgqQoYuwReb/nVTE+hwlDRLKJZ7aKG/Ux8cmA
17wRj+cmui0WeuNmvRcFL97YUM3cbPm+ipabY3Q2vVFG560qmGKPrTgtSu8aKqQxrfpAmkpvwX/8
XtoLX9QMBIqxNWn2njHmimi9EDeY4/YayhuTqd1m3VadhiqhUWSMfq//a+/lJAWteChXz9of4XfD
GvoZK1svyHa0t1V58wfSev+Cjo13NbjqljRcCsbGpuROO7WwP3qb4X2qGq4UQlPpDbDndrP/PMCw
4CvaJ+scgt9M6hvIxbTHokBduDhTlqjomchsSxoQzoFHKVqj/pWms+Ld8KvIS0vp7gTteyGlDXWu
kR4W5uGyVMX16QKROm0fitxnuEn6Ht20AxQEeuTmHSW/yIedbbkb2OkibHEEdEX1NkGsybiRf8EU
A0YP0QzlwxuXqUg5sQwpgU1JJ8UQTGSroWdorDI3CkOm/P5jKzCDRfxYKqSpOjxSi+0y65rxsEE4
EY4YX6CjG986t7l4HVgFP0lvDD+ZNdj+1cpIi7BwMEkedN+Mb9Fs4u+XP8ornt1bALrhhKcLzzwf
RDlXktm/JYIl/yL9vqiAG8poI/yK4LBML0xCQgnFn2S/lFwwffLL/GR7XtnO3PybA2Qw75bdEl9o
entOsheV3W4xJrPTEml8qhHYfOsYTpPbijsDYF38pox7YMe8nH7m/dJPq7BlUATdlDUoQlQpZ9oE
j0R1YACeimuDRvZUF2da05er8x20Cf2O5HWmVvzr8VZfpHiLDT/6+fLRAIIH0PTaD/DRUkQ7NKa/
Pd6es0++sY08oJAE0fKLyTVMrYqi+K72rprLTFqa1RUx8z4psniAyhIhCCilUTq51JPQad+20X8g
UE57igYfIlbTK0Sp+TnUw2evRdtqzWUzczZCBIaIBfXG9mV/4wEQeRasEYO8Tyr4m6SLSW5QD1Fc
z/MjUm+GfWSm79t5w27CRZPpqgDxhnUlAwe3SgTySeONiLnzt41z8XbbPCyY0IdpTSI3klfF8miN
NddMvzZC19zfmxudlXDRHZk7F2dJshBn2Ige97LnVzKJ3SjpZ4Bew+SKyxciKzr441/3RiAXVylb
kDAxB+/CyJzjhCOlPzmzMU6273Xf2RIDsD6TKd6th6rPy5FortTzeuWe8N42RWboslQYK8mKGM+V
RlnaOic8WXKCoap0IqEMdaRv/JREVhLcBeQSDQPGBDqSFdBi1eU5rfjF71FSSCsdITrn0EenOhQb
/PEpbB9Ulh9EGYFMHu1evBc4tEZ+AlOC80AXdLoSAVrds0YipNATUioY4LzROV/Widd+ZigsJJVK
n0Z+IFq6OaDaXEy2oiJIsC9Ariwq2BoGd07aJTPwWFSWTprskkHEmZcfc5Xmd0NQozD4TI7xB4nw
M8RCZllY6EBvyquxXRmG477Mnv0YxLAa602uznoyEMGyHDZN6My5Qiulk5aRlAqXRRs1u6vVxBzm
2eHjo7tVBPoY5Pf/tg6to0sFXld2uxZo5wIIGaS7Rq7Unweu8RE5ak9j8uhBapk1U8E2xvdBR0pA
6vxGgIhsf1LwlSRvgwL4oVCDKHd0VGeZZnP0/Boua9qZl8mYnFyIcV8RTPrOtjIyNVCJ4uwtT9S5
vnR1Es+PH4hbXnRBBpQ3JJXhuNWNlX76/G4tu6aaxpkuGYDug0Y8AeAKrAC06cZcgVOfiT52Ykan
yKg8uyUdJm0qcaSsndlE3/a5aSSG4m3EDhtCYe2LHFSMGdZr0z1at2+O6OFlPI4fB7ntCN6sWVP/
d3I3nXJ+/+V/1ETFOPuLEDwT9ty6zUEdifnu+XzZjXlhluY/BlOjBML0jU+TNvkFGiNCAuAsxll/
g3N9V+149YSm9I5z/oQQa4nXe75CcOT8AmtXLlct4D8cmKHi4VHI2hjN/cPpltaOJ3sbCfq+9HNN
syltnXGmbEEXCM2CDuwLJKZW6tabAeVrwiE6CGWtC/8bgmUMYevjMChSpXpXPxBZn4JKg5VwvOd6
Ek7qphiY+2vdURh6NJe2im21UcQ3ZWfrewTQDWYkjo0l98U5j2HAVMETPrQ0TDdO/eknbDxguNi3
FlR39sqmysdkzqQAVKkMlcN3G0dnLDBaC/9s3BdsBKkmz8F0znTaPW6xFSpId1EDd39ZbvTxPITt
smbSJWXyj32x+UEtDGSxDvz2KRzmSiJLEvDlvtej5NxPeaP8fSz3K+u/71Qu6dpY7ORb1RMc1Mh+
n7nQWSkbwjXQIGZOtoLlunXcFLqgSCLFBVHHaTCf4ZZlnK8NR/BSnhE9vAqeuhc5OfvYcCJEiUJN
o8tSGYQMyZXBt3XelUMwcqK4l8xdoXgE0wWVc+u4NDuYzBhTTcwHm9DIk5Kmr3KU1TorH9bu7ro8
Tm9TgepuHislnMWOcgKVY8LSz3diMXVZFeUuawKS9iP+aXqHrkGq2PCYme6RDSb34H4Fe9RvYbXd
xRKbvG9Yo8QQEK4dBJqAWFxOgN5Qhb2/Axz4Xm/n2WYcnU93VI+BcxuLhp+NZZfXfOJh2WfmaQI/
uTE5TuJoR0+ya9HeTXJMJATVBiM31Vi1VqZ2ilfqDWbPAmj/2H9/YkMbCY9dVWN0Av73lkcgB+4g
4/RCVLkgtX+bxAIfJk2kXZZp5pryCh7HQDSCNSBXoSp4/AhyMN7R8uqEsw5cpQGPy+ML9j4+Zs83
Z9uPHYcqXLM2eJXTzsITLuQZMmzSH4Im504X4IHPYKJZ1Pzh6s5rWaZqL3VzRPVBW64Nb7VAW6y0
lrXawF2DSasblpcnMciYLbJ7pmN8q+tVQc9JzLHTZuTRi5qrgd2W4nMG2aE/lVCn62twc92CiKKn
HpY/FrZrzhiWiSH6r4W7SSmNeAcltX9V5Y5eE7sGgLSzyGrlfqS1FqAJBSgHvB3g2U4OPKa05Atm
PzVbwRI9pSrUiRChqtMycwPxyI7XeHDnaBlxmbKj34gOSdJ7+Uu7WyjC4RX2UDfbWyg585OrDndt
tttYLwwGi+NpYaE7UF84GwXfRnUj55HJzENNt72OH4ccuq0yuWozZvka106SuXP89FndNdX0NlY1
zMEVtUuGUlScXHNKnHvWhc0entr10a6dccwgwJq9xwjPyoABufjorhwvioSg2BI05olPQCnz8xDk
janZkF5JVkiRWkFE7sBMWL9+bz+VNLkE1o505+/nG96JcqM56s7EccSQhdSaBd8VOQJV+tpCK9g3
LkDpbxbxLmhD6aVgy82QrJjAJyE78lE6QtojlFkdCZsBqw9OeHywEOVgIXiubhG7KPG+fV0s+bH1
I03+Bo3Mk/pX5sBkJPVMOjjlG/XIz8b60RZHtURGBPKkfGF5JLojOlRVbPwYqZplJv9cVIIVFFlQ
+Qxj8gPKVZlU3v4X6pXxnnQVO4b/7gwhXrs3ibaT3tKh03QaFORTPE/JZsdOSBOoFsmuJW50JsPj
Y/hcvgV9M67KyafGzoDRao/KkOoFwu2Mzd0FGM6/bNR8TuLrS8F7+iI3QzTf8botJJ9SsQWu6bYQ
RfZz1yoOq37Eb/t7peNsAjI2uGASB/N947PGQJZjdrLV8zZJzcpsqKdEUzhCIp1JvLV0CboHN04E
Btmb5DFz7H4DkKamosbpLkdkgjRT/XhyDAjlkiJvR9ojbNlgARRaaJZva2WXubTRcq7PqOODi5j2
9c5SltFEwAYrCXGwDhWREJ0Lsz+9jf8t6nJyyfdDIULEvCMqYoM56ums76puqHV4GvsX3qpr0TtT
oyqwDxP/1ep+rbX200yrVBHUykgyZJHUjDBJF0MRrHvriZ04+WsWu70QLtMeG7X1R81dITEkWvpr
jLiMHwB25+BLPt/Pq0lHYby4Cty9103KImCblX6tvjaOVdN2qgJGv5t0iLdMYUwR4O1tzStLfRAS
h4sqO28JjQ/9zh3c9MO9MEzE+Gyg72MlrGLdhOr2mUvKqKOCpEb6NncvZFEpLKqYLlPSNy9zOuD4
ECUZG1uLStPnwCLJDv8pZccp83eVgokamRhPO/HH95IO+oLLcau3e5CoIoLN4yWdmpuuUrOsy2sc
PRBf/qa36JDjY8xC/c0gvSxi74PeApNnu/Snz0L6Qj7UfDO0ZDZhEJsnzh/81P8TNJBPXn7Fg52F
pxq/P4izOilxq2OR37Kn5UUIxFzXBZr0nXGZEXEt3UNnJHT8Hz+Z7sa4dxZK7DWEE/Vn6uCBUnNk
IhoWqY9jdRtuwzdGSoumN2tm8hNC/yyONm9AG8uhsQNYLkWu7c5SR5OactJomfbQ2EeluhBoe3cu
J+pqMho9mhwv04BRREkWJ2ZFkHzv81uHKEfMpHzTusxWUJi2K7zoJIO52V9UkXbUqHessr4eWvj9
F/uMabJq69yb/hnjn6vWzAsD+YU7RNgrsLXcVN8oRmUmTXEC+OgbXY3n8kU+0Hle9wC/J58nD7X0
hhWHA+vTMKxXRAWDMuf7NcnAOY91bNBPf8rG2+zGtfXPziH8FfGZMXUpj03xTAO+NdRYyxrH+57J
Qc8fyiA/yStXomwTENqlktF6gtgQ/thRqG4jYijwNSgYWBGQ7RT1MJtQlVLtQNLesgyodOV8szyj
7ttQHlUOaw7c+EJHGbZCp4PRp8aRIKHS4nteTZ8o0Ak8jmN18NHqfTUSP2mPWtREx6rcFKNdsjbx
xLFbgyL4c1NpujulksZpYAf2P+E/VBzfAlNsLe6+QgDKtQ2VHdA9RT0AwXKHFdVsBRpaq79L3xsG
v5E7K1tB0hj0D2oTilZlMi1uS1A47y9KL5mNnAymAuVSoaC5l+Tn981On2ZKKBLJ1ftStioTiceC
5huLzKt1op127jjRYXS+Aa9g4V01YkmzXdNcXmMDRlZTsmMsukfJksD1lZlSOWZ5h7/UBCNGk5+l
qGwvVh130vmKUmlCBZDUmknpRQkPLdFh+DGRT99esl4QYPwnMqgNhw3nlxWF1a3GCqcRsKgXq5aM
dVVmPIu1cF3Yj28SPQtVg3g6gH19okBbEeYUAKG8aiG7RmEcLTTgv7mKZLc4ljumIAi3JZZY9Xq2
Tzo32RAVADXSkoQjemWYrMai0Od/2ghi2c3/FPDLWebvOwZ9FTR7C9cFqbdT4SyDL32/0Kcncw06
1dYMoXodCe09RkWBt2PSwKsZ/DzSN+bUZESrVhbGwwTqOMjmnAvmFnDmYieUJVWDPLtGhJZBFJqX
cTFy7CPNwsFLWHDZ7GIiVivtO79MLXQkRg5PIDLMJnvFFdnxXNs87wTkBhbAEsu2RKngcraLDG8e
CsMBnTNXWVlN2itaDEPC9ggABlTJKktZ/SMGDkCnmLVMzXsvcOHqinp+Ut9v//UJ4L3GVaCUAWSs
ENB6aJcZGbIXS6vEMAIdWjQ0jLurMAnG5mBc2TxNstlo0VsMarEgGSSKhkxwX4ycsa7T3Hm1iL4j
jlKIbEmTu6qFO4FcY6rsuYtO/wBxYkAplEFazwgBgv0d32J2HhGlZHcLZv+oslDWfV7ebFNjcJrb
CBDogyz4sE7VgbV8OpI2Dhac6ArKFV5xIbg528MdnV3xkIVXWCt8uR//hmRWwWHXO5ncb4rm3gA5
GPK09yv5DsMrD5Zp2K8X0zs5CakAPOw+EWDfqaXz9EFadpzD4ZgWDibyUuPBQSOxcoMiwPuxve6D
lpqtCTNqWir9sY8Y9bGRC7i/8rf0K+BUm3390UUgOLZfnfp6Pw4NYlgZmODf/0KeuGdSCZgZzmBj
Sz79pXSn/8usbJxqy+GE5G2Rrghxq1SYoomDUBdicPLg9f9VFGA1aPpSvJiX+gK89ZYDtwBEQfUE
r0PhZXFD5hLaeKqOjX8J2zjTDHjfI16A3s3c3IK54/+ZZQgUFT818xWRRwIAlCtWDtCOKYjvtX2H
GOLLM4r9gXCwy8atTzSjrKLdgCjZeusPEB3VwH38LgSDCXXLWf01gG8e8MPEtWXNEagKJ8sgmjlS
G06NG9l/odicBXmp9NitlrutJwNNC4wXZu7CWt25U8ZmSw2BcSEWWvr0XhtxyaVLEixSHOle+Wnj
U6bIJ7hU45eXvgi+TSB2SeNs6h3VHO/vCVXBx8Am0/+VAHYniR+WOVIrpjK/c9nc3cHTWne4bB6G
DKtdtUIYqTR6LolTGIvSbrmFDIlYBNkcEzZcLx6yk0G59WYDcHu1VJVEdoxb+l81p7UW8E8pBBMh
c6GgGUASCXui/J/KLnZ8sRFK5N/G4HLxyiIbKxvVcxb4hxPD4HCUr4+zGHShNK6zoZBowrb3oTOq
JGOs6JYpxBZ/2ipc9FiCj9/NFjsdafnhGH69dqlqb2axh/P7Wwu93WkAgB26uo+oukfNWnDoZiz6
/f+yiXFTksmooVEB5vo0E5PDqgmbdwPI47gy9YTqiq1RA9oUEBd6TQEppxyGsX0YiT0RGYjNfFRZ
Tq/FE2MbqwW2aiz/4q5AW9uiP1hrl2QUvjylyoB8KSwFNf/v02tzFjDqwx98bcz5qv64s9k/2Yef
ktTZm3DJRZcQCMEn2Bo/jsYWMmWWK69jMrHu1RhtTEUj1XVUW1gLUNe4FTLsD/rcpD5U3/ZFpt5I
dKcQ/gBAJji0ymfYk1THH9iNpwZPSLARjbw+XH1bGEloVl4BTvgcvZfHKWQxX/+8AvdWqBT2fHvq
oIrs2ZoCmguIXSyqmxO1bHccFQxrO9PrUNzKfuaxWWQ59pETsCRsdJ97ecrEh9bYCMWEt0b/C4at
kyfFYJj1xkGJpRDay9XGZ0mmZHlIsiRiTScx3soF264szkmQCMih7/T5sBehYCOh7dxsQGH3VEZF
j5pSRTxnMUSs8t3fSkZXQIM22DwAPRh556GSug4Gp+RyeFyXt4o31aPQz8tZGInkPF6IMVwCMVIg
tEnLstTHcz4lNq8MAV4oIukNlwvYfUgleRtGVQZjkHKV+RtuiJQx0yPBDMorF8Lg3iGbgBR2N75t
8qIfBKI1O1UcOFe890BtY9ktDuwL9wKMQ2Iz28cKl2w6AwCV/LPNbvYFvlBVMQVPem8H+cP/fC3x
RPav2OIDKVq25OML9WBbzsw2JCbHfBAJfOoIjpaYjNHPfUX3Um6gXEaHhUXzDAEwZCIv04JXD91H
MVSucq9eRSW6wI8h2fml+GkNJZM/urTzUxCQs0vKaq1GJPTBtCDZcTk+CmQS7vrhnPtFIMl6oRet
7FBq8HCWQ93hmOW8lHnJmUWlcLI3z4zpe3trKD8aRTOC+HhmZTdhU4OPaKQaenHQRwU3DFiHF9P+
3vcHSprnBUI8jVhuxQJrt+05+/G+leS/EH3dhd66/x/7QUlOSYLzeXfYbKGlqNCuJF9iqOfPt5uV
wFkunCaujwkfijkbWuWYngMVR8pfRxrEXzsHYJyloLmfIqAFcBhhMNtszgq6rS28w9v9VZY3x0rx
Cg/8EQAV2/W1h5OMPQDAEx8HmT0kKed/npmgxCAPraFQufsGIcOewQ37m4J/34CxMDctxhJkfzcP
eM70/SVz3+GAriykQTa0WQrZxkxGMn3oD/T4oILuohSyUcu2cu4ofNFymwvB0bLmHzUs1sdKQOWm
NXXVz6otthYf5AkNINIVHbOVCQv0LQZO56NRUFmBpGa5lF9wpWrYfCUapvRdUtlbHCQz0BQMUC5A
QjEdsHlLzS3AaMHz42Z6A++j9u0eiNhPiWnPnV+VMErj7TycN+mYFJwMdjprrvYp5SLeS89zUGd4
Ew3eL4QTCtc7MHsFXkwb6bYP+zU+S2GNdEKMFhb8e5UZGPLs+Vw2Ij7KMU8zDXIHP4LJ7g/ATyGc
pJPJD3UyRNXHdOk9Gr+c7niKsCyOTb8Thg2jcH7g6VAi/ABaHDNKB5qP/56cw9TW/z/C3wy620rS
o5Xx5IHhCIdytsbI8bZkj7Rjd3FcV13maCxuRMgh4oB2LTLcQ4bddez+aUr58kEopHDlQscd6i2M
LIoIVOds0AciVtu8KTomF5ZWGOB3l8FcvB01YXEHbenzrg1ufSw7kEc5HLtl7xCNn06idpEIe604
SDpADkp5xCMKDARhNi2CwPg7AIIuSkgqYxjd35Nw/hzsac69PTYrHOgYqgIHZz5XfMTQxwaNeEt1
kohXQUhBAkx9tXkWY0U/m4YrW5bewvdE2w7L9vTOrjrlk/zIg18z2c9Au3J3zejMzU6tZiSotSzA
Bn69fObwJ3t+GEC3we0xjSmsbvQbhmBII2xAY6qWnQYjHyGjQWvkSIKkxZQNMlCQpIlHUOHwln30
VxT00rhh6U78ZJn303SYGnf0gGO24XmjZya/gj45GVl7r1jbpg9GwUrFipz6ZJz9OP5E/N0llsSk
4Wan+OK6nBgFsGVBU73UfdJNCTpItsKGVEliliFRB9mI5NN8NCGXN12cZDZ0nM/jwo1C4OQkkUlu
95kE2Y+Gp1+ZnIPyF2RmUsxDGnfJ0UK0M+kFr+Seh2Mq+EB2CRI+SKaHWSR1hKiYm5U6vmSxNOez
1KFtj3Y7N2Gwn2Cn9qjm/wyHBr/gmpJqi5UwhQ9Ze/KITexa3clUWrhCXfVMNkVg5wiSl1fxvuOR
HAqR1lM57zJUgt33uVAy3ny9cT+oyJSiKJFPg7f8nEOoKzANjMKLvPgkpLDsOedS42/J2cI28yD/
y5RS1kwIHZfootODzawYd2H3tiam0hSLO1jppIAvtkyRonsct+F/gsDpCj6u0dAbjZh/JHN4c1tC
9Lw9n8Ic/KVeai23g64ym8VgCTH7SDQqVxbGFfMxtTFZQ8BWfHISttf2RIwfeN88GABiknqxnUAs
FR64EPM/fgp8/N4vuvjUljmRysLNuev9s8xOBiIru05pQdrHO+YlC0ikmCqpgnGoCoflRyabQWST
SYaCtNrre0mfgr53Fqx2CIjF3lIVmr+P6fIoY/3P36g9wlmT3xY1JX5g0hkjqPZ14cG7kwU4Oksz
BOizPoOfakkUVL5AK6hhaEfkumnMZxfmk2quqGo+LLq8LQaxMFne8pLnU/AGX9gWJylx2hCNrUJ6
2bpliI2tfQaXYOMl4s00yg2OGbtoyEoDNCoydA97Ynp4N4fKVybtq950FoIWvgT/XgK7Mp6W+MGG
cmgXB7ScKrT25WEn1s54W9Q7N46OERU37wwYPAAdMlwAA6WX2rB9Fa3qHrsa/2N9m53x6zpSexbO
IoIY5uIDR++o7eRtsC1PnDKcdMmsXksPwEq+gS71/JNd2A+bJDcJ0DkxdO3lzXjcjXy93bPjtMar
h/jW/4enmFl3HnUUZFbopHuWY54gScjlkhMo99+hGgi508qnLHuDbsrU+BS1frkDbYrYG1b6zlFW
wBGmf3yd1GwgL8iyvG8XjRH+Cv669pUeDl1ESjoSAE1IlHlWnaHdHDEUEbL1QOyuIIvWxvLqBGur
JN3ek8tPAyr8c9UmysHiMe60bNgtaq2R3dwHl5d8qGtS1PKNvIGElzvI5fwNVYKPQGIHaqIdIYo+
Yx2G6j/i3hqmM5iYYxNmKJJcukHPwTYOp8iMuriqfzBDO9z2MvQ3wLCi55sUW6/1MsvqDuVhc+7b
ByRuj8f9VB60KukxytjveHVWg6iGFdRw5VSzd9EpWUlmKGdnLP13DzwNCWWQNgxaxXwMtm+mlJ5D
xT+Y0nvS/fhXU7YmjTuS+ppRP3/8dHcLr+urfr3BvPy+Dspzy7bHbUMofitT8XiheZTzkPt9FMHB
hX/UJ+BL/6eACo+rkIGHO5xGekw5F92Rkt3g/rZn3xNufBskAXHM0z37mtAbsiyl68kzQupjodH1
ysP3ySfPs86eWpsunZuYilUxQG1HayZu8OOqp3jDUWIISx4S6yxeHaB2sztQ6GzivzLZRpQNbs6o
A045O6GJj1xzVNUn5EC+B3TtCGn+prnTwbukeFaPB2fSrgUuQHlBFqxUuLhoWZAb4GPJg5NqC8bd
oSel/svpAJ/l7uVe7l1a8oHzClsxcyAeo5DJmg6gxH1yBu52w/V7oFcTqz5BrtSLdDPLq7l/zHqw
WWPU7ZLjls6NGxe155L6nVXnaI9wzdzKmjMlW9fh9Jg8wJywUEzCc1cTEWFud3GoK7gsKWVXXIiV
u6lcvLCw9YqvDybBKExqisw6j/a0hfNxPNHKzYvlFEBXjMGPa8cwXIk/CJzqVHarKoHnnOyNjC1n
pWzPuYxmeA5YwYzUXCTRmHlcyduAN7XZ4wkH4DtQZvf09xrWpWoYWVT9GnUaXTnOYY7MOSAC1mvo
4BiqIR78rIaYxvO8cNm4/nF3C/8DmNQiREiFfIuMr4JFYzYGmt+YHVFJfbtZeyfZ7ukgptIXatnU
OwtmLJhZSz95rrQcrXNG89c2nkU2Q2rLgo4524DGXOYw9gqKxCLHfERNrpLivP9dchy76/CeYx/c
jJi6kKFSjDSFDrijK6oimS7/VXQSAATa8rYnqYz2f6C1nbW6gmDMGjqPHhsS7DdVQmLYFEgia7ew
veLWLEz0YuPtTZw+cEt08s++tHaVLn2OC5m1mT52qrcWWKD044ASnpoAXF9JICZ5+wW4ygj77jKj
tJGeYdZE3CysPGf+wljvohTNNQLzZF6ZhcG4LRytkQEwxJU5o+vCxq4c3zcRi+ZY2im7rlLV+g+2
TiKD6y1YZR5iArC74+Rcts076ZoEReFT9TzpSgzZU4XVzqpBP7KcElTl6n9mMqEOuYizUhuwsXNa
Lwq/3YuOh0jAt0kzgyR+yLxiZjuIQYjikDGyzIQM7KByP+DIKJo/UfVlKpAaJYfocGHTc03QpwJs
qDMDas+nM6zg5opyelZlIK/iVIeuf1KQdkEX22MLFo46Al2OXWjJhnbgshN1f93zz8zYPWm5xoGq
FIFjF2swBuAwy7Yh1Agb6GsFhhm33ry5Tlv1PSVVRuvyfZ2asCUK4AdTK1jAJZfCgEHh1chIDctf
UfzyAjEdbWXHusFTY6pKzDSMgDeKDX5+xUOdzt5OD1GL81H1qDV25m2eONIrf/0Cmz6QadKf7QkI
ABiFeS/X+Hu2sWxT0PcE1mMO6rOBoxAdP3sUS7/H8/62Ostt9T5HQT/5SzO55SUW8Zq1wBUo2TVf
K8QYynLABfzAwIJyUXnzLXB1u9W/BGJTz31/AmlFsjtmin7VTEcbq8kX6Mp1CqOcH9f5PJ39x6Dj
xoeby0gzi871QrOo+NyXLxyfhFrGQ4LUyCMHHn3H7TfSSAxQCr0iGhc5ZZuHs1uOrALOvz4/6s0E
kO8pivYLQsyfE3EAdaV1OdkLuRgou+T9Iqe/cxF483Kc4y4iP/yUUZXiZr1Q7nFfRuRLEKPbGsBo
cifuPK+RowQBDTuOlducWQv/KWzzNvn2KB4s5yHn7pJSkeNGZpgL7wco1HhDEdiYPLy4OBESsXrB
hrDMnrMHtCZ/xgT1NIaXeXIcIKiinN4Kv4mGqduFD7Muvb2Ksg0Rn1lDDGpfOoJXKc2iUv1yEtzb
VEcG5St7f5CjAR/MncvDsE6tDI5QdS8qK7ETUm5KvvdqQbQybRkgqmj5V2K8swiqaO0aWNqX4f5P
F0r0S5WfU3t6zILmLT9ik1d74+aQtkyT08wSAtMT4wePBsus36dciTXtscv0x6XLhSgIqMNx34xc
4jRRWLNN/F2vIOJD9+n/asWqSXbdGuz3sZOnIQbBiUn+60nvrX/LMyrCLg/7oSKGeeR/4KrOQ738
T5G34RHnl5w22e98X9NzzFSyGNLrzbFS6sKZC42HXr/LnIioZShQfuGxZfCpP8j2r1nWRHPmjfR/
2KjOoZ7Mbht4x7vkNqF46pjQiB6VPcrvf5Bifv4ZygO4kIysTH/LIcWLFNQi+GTPjUqmoRhzzTbU
XdL+8TuuBsIglJFu4uPHp4X1lXGiEA8J4dL+YTDbkvHqojbDlGBnZ5uBqq0TaYUuk5rz+TnuYF3k
qM4uduuU355syu1obgO4osVh3qi08rKG31hqc/o8d7WJ4wgtMyLmM/Ky8zfantxL8R2PRBHDiXkE
j+XaLt19L6qn8Mpgr4UdruY7i7bGNaFipG4moGjYdluThAjL8z2w5VAcoZiDfAhrcRY1t/4PWLXG
DBLgQSeWRo/RJZ/sCuV1lnLj+HQIAeb4RRK31PkXcBtrjLYsH8QMMgCbMNYIPniUU51VXNZgqxW/
Mnj2HvyOd9t3/SEpdUKP/QcHB48fxfXSvqGNmlZSBKHuhiPz901Env2nvjNEafYXxYnKoAiXk1E4
CZC+w4CGd1zZ2mdlT7sObRkns1OprA6K9wxUwQUIuE5NUjo7/HqxyTekeQY1Pys6aBVT5o8kfD2m
gdk04iPXxfBq7EkxlCvKKPujFR5ZsjMPwVADR3l4TNJ+e9uRSkjgL42EVbWvXMd/UUxMDlhIkjgc
/Z+PE7LEwTNb+8C2Xaf0Nb2lznUWIKE7hb69X6wQY7wlNvsP7X3DzMXamjRc//elNyDFrTxQrPLH
HsSCTRdmo8AkOevaE5fBP3NJQ64Yly1EUukUq2jNQnVEpnysAU7+89PZsg0kckJNI1VCT+wIJ+ik
JcjR3WRTaIzduDFVUGWiJ/LBfwkXpEUUem0D4aB6XSfE84utWuBBXG2KM8qyrKWKMirVKC5KgqvO
t5hfnOB+vtFryi8Hb1PcnYIQMn+adOSEJrmCUNX7cXQRGtnXkTnuGfW0Fhp6oLC1MIDirsZ+iCDN
h5XxAOjeHwqoftdTRpL5xtSdeJRqOFnWzQmuOv8iEh4g6z30F8jNJT/wR1TbwJHxscNsgKmzsery
k4pFCMgxKP4x5gvUoOJ9Wp1QxB/vvIKTb0xsERiiirnGoIABUsnRfN1lrc1nJOVba8I7/8z9SIBi
PUwTBIMTYAecRqbkw7QPJOXATzruiCBn/PNG3oJ93MpKX+gaV3F67cJdhYgcEqGgAqcuSMwav3qs
0SUkoneM2zLgvj/ppilz9zynwWRRShgU/FmnXb3wWnGA3LTY55vCSn6PuLSuLttstUv7+elTCZE5
oC+cpH2moHsn5cE3vMl2u1uVvTjgbcHZ7uH4UVcpKwOks9lYHjbmxKO+48IzcK77L0hSY8nwEA8O
k960lsIqHADZuLXPytXjCsgxz7UQzUwINTSv7/7OppM7m3eNifCrej5o+sIF8maeUiCruw+fN4S7
eadTDIfoQ0BoUgLnVl4hgi2ol1c1mmjC50dMNJBrowAW6JXbtSAiYeTzGOURhLkgYmcGLov3Hdgl
44/0r8r7HZrUHKoS7+MqSC5pNbAiGS9c27favx1YYqnwM9mTeCYiisuQb5wZNAwP4G4sqwRWrRfc
45/kfJdDAHar0T17ZHARmnROCucNpjlgrrQXuAo6kgCLNgJiJT/QS3b2byPPocbOpkLtBh3+GIk2
gt5EJj/JP9DrvJQR9d3BUdkJkE0u07TTIr3Z3kIVE5GbbfSx/0bju/eLWNgdKDx32a8pq6eumCyq
aXlQMX0vIkjx/ZowVHDtYcMeMlCjjORoEiIS+ZPvGUjh7xfuPWmfb9X5ppRwMpwe5dWIEjTgFlIp
36IcUP5gSpRR1xWujsm5XMaJTU4/hv0uJ9qKfNlMgRInCLRIiNywqSH8PzzL9IqWWzOtufReqcHg
seSNxGKEDVVJb1/57M2ZZf6Slqf1lgd3sPR1yhkYXYwYw8etZXmMTbIuoeMxAzpFEeIR4fwxw6as
mkOQonSh37V0hVJUF3Eh1u/NHbGAwXTICfnv4rKeqWWyNXtbaKMXj2uZSCUXw4HM4tJeUGRpg3k3
z/Te/vUDBa1n/W+XXrf+Bc9Ab4QkTIj8JqsJM9zg0dLaCINDrKG2Tk53ICKo/E0EhpJDHbMnSiTs
NpkwxxF0lY4sT1223c9FDnJYFh1GSMlzrS/GJ+Ac3xEuixkdZF60Jt/vSFjBn3aWd35FOyi0eVWP
jD92LkwjZ6gJMLYzPmy+E0ALj9QX8GFxMU1qVgDHegfcghGmJqpQ1Ud0DSs+kIv1W2z2YkIR1LXq
j319CE8pT1aGop7hjEa7gGQXfXn1KFlTBP2THX/6/CqC2IGTkbbLA+wmv47WoMGzZ50VBfW13DSZ
DeCxKsjxiK4JUa9xHsESuml7qE9TcPmhfC9bUzZf2k1znadl9za8A8PZH6ceCibj88SkMgjIQr6s
q38EG2j8JTLj0h0939HJNAee/zRXgNmRSLWU2x6Euc7ehyqlc/RSbGZR4GqDiP+aL4TbWiHyAf+9
pI/e03WR1SgXzzYCIQ+ASuCMScFro3KzImeVowvHlKyJPbZat97whMN+Tk5WOW1SheVfSmcvjnFg
LQUiQqCenN7IiShAgzULPdc9/YV+xd4z6D8GbnKWnhMkmPgbDSbt+m3UuhVPnFT1T5aKF6j/Gbkf
wsyV0x4gSBKq9q004EaCY8kJ1iOOmuVWRwrSpRWbr48ndvVS2SHvrtVn2OLAEbr9JIhghR/uwbm0
L926jicILAHD+fH1HkWkTTv+O2i0oAHjz9M10iNRBcAz1u1IiKHQ2Amj6M+zqd1w2xQvUUvYMp2k
aTh3iiAuKpaGBmjYrZCl5VvYPkOPXRB/drL8tyWqhpaPeCTuMMGqvBdtJM4UbQE9MaZm65IfW9fy
eBFpKU7VHwf8yImJCbjRRA3H5XVoWtMBBn1uAI1w040wsnLEGSp5sczT89IeMgVYRfJNqIpNRfsT
e7ZwRXaSxfWVAVsFx6qIB248Ox95H/el+yxLKvjCjTJ73L27r0QRPWBQCRn2i8oNJep5qytmkTr/
Y1ycXHeBPe3iXwCDd4KBIsBfYrTZf5t1TRXzePzMq50GtRYVdLiMqYs7T7bprR2W74ZtmqmqSgN7
w/aVMUC5JUwJiG5LfhQ+iSbqLGi96LekHKATUcCoobh3iUjMfhN3YZ6xJJkhTZ8CWBOK4SrSj5pd
s6evNFnESGWNnqjEmOUtTlxHEHWfeZNsYe2Tc7tGV9w5diVwJnn1/JsQKsVXl3zJrUSN/o4/HI9J
cTq2se7aIO2D8r2J6Rk1taGS0ukQrDrrZKV9pe5nbJv/te1RiafN/kcE5ys7bdhT/CKiMAOANRFp
vl8lm3w8tdkRMupuW1rEa2i5UxdC8StD+4bKecmn/Y6JZn+woSEFlHtKSPFrgbz2DBdDunR+EbI6
6KStQOcvlvAfeYg/cf02S7dhp5TsNFlC7n1Nbydl0lk3G4HTvHhdVY9wn7c99uTWlK3FkqrEacf3
/vlS5iEgk2fNUMH2uR3J+D8KUQVtK47rdW6L9AJRyWKsoc32PXlLPjiJctzErAwheeiEm1sQbLu8
M2A8obhx2ZugfwLmpVOpuEfD2S0fMdoSNWn1UJKRzpyfz1vMZGg7AH5autFv+JjMlQ+DwVWVzJyV
0SodM8QQY4Jzdf7XXlYYmFdCHfdUlFgmElgryBEkQfSnLv27jgQD6vyrfVZoXMkOopgjQKKHRW38
xVeF2HFz8Jkt7Wnr9PXHZRiKXYmoyCQF3KjTo4N2tlF1kpuzHHuOXQUU/gq7iZ4C+dk2Wu5AHd3n
oPGglgx0DE7x4nNSltk63O1XBANoX06eyrr9NiBN5716w/1P75M3qWDnz3C+zgzjh9kEyCbEYuYn
kheLW/P38ZT0MUvdslggNym2EpOkjg61UfNZsdfrzo4aK8Gz5QYyQs7edTtXaEVrQ5KFF68OpAc8
5I/NtTHJBE+j/ILvL3tm9jZHLODfCrgEPU8jBrY6BZBdC/Y7QWBG482Xu3ynOZfzrvs8AzZ75xhK
yZeTDDwoNZVIzINuM/Cj5V9C3Nyb4o7ExGOyZRDmc05CSIbZbTjcO4F+BPp0eP2aAPTpHr2vlb6k
AfjEwqqWOsazFUATf1nLOkDQMRBiFyguYR5HgggIQOaSTBlpV+ECFsDvasrW2RkJJIR6jLcaAMap
taEdc3GJjRRXYzcrlK3DBJwxR9WSKq/8Pv+hM47rp8CHGvEsciwbTSvdK0SAeH1zI22bW36BZhZV
0TwC8xtEfH5FNz3KP7KGC7C32xE87sxms84x+0YW8zn9Fso92z/mdormBEeWCRhqPE/AYozCWANv
7CiPWAGTqk2TzLJL/m2huiYdZlTUEFrgxF9FYZGPfJ8HU5ALUKO2zCk3P0MQ/qN9Xj8zgjiHu9l7
4pi5G+Auh+yPrp5foLt4GrrCY8A6H1wTm+NKNEJxVNwSAzMlJXu+XTwvu0/Pn4zBf1iC5AwuSS8a
URSmB7mSyNZ6MlWi4S2Cx2Jso2JwGIhotrbG9GbQ3A+sRr23f/E/hsn9IwtL03ciDQxgKblslCVJ
SdwFj9rBQJqh8D2HF3EKK8rkUXyPAOEf/FjtkxsVW8+Mu9Qg6UJNeUFnWWwkPhoZ2qgcnAS8Ih7B
+A2W/Jvfw5U68I2S/G/h7RMPeYDWyzYKI3DrECoaRfGnuB8c+fDyPYgMn7lko+fJ+49L7En/Oadz
3yOjuqthurtWTCcUFdkBWYV5Nu2w11D2EujI+IAz+5oxtBZ0Iuk0g6Q3hUb8vC9J+4EqmgKaqFrU
zkzDua+yBOHtplKdUI0HsrcLS57lzTVcm//Vhf5Qelijz1hqG897HwifZ0XQITAL8Qo4aZiu0PD0
lUtqMvo587ozzI26TTivAzC9pkmrQH0eUj6+WCbjAne324KvxTBN8SE+QrdklXDL+22SmV2RZSu4
DvRL8loKkSEU8yHHmd2d896HV8g5zC2wt2EXdh5B9nfTHdyLymKphjm5j2VX+7gTPC7TepJ6U7vM
EfypSNSdvHv+uoGbKoezLgDsP4WhVYphZ/A++UKOW2PRjGCA7TBBmqkXtssAS5LUU6vQlgFJXPMB
G3+TfI+Z8FVq1mWmXJZelZ/ekw5kN9T/EdwZc2gJ33cZG/UXr6RWzKpoUvUiqgL3ogA9/GhBxuFT
vfDQ38okK769a04v8o6MxrkEGDv56jdFdOw21DotGKsC3ab+6UfLcYqlb/Ylas3SNLOz6jbrEH8c
lNteNy+HHMfZOtL5ns4WlB4ThlJXuUooZyMLn3klscDz57IpCP8mKkACxsdHH+NPgp+00tJeiUgv
lSs4N7FwZ40Sy3r4fTY4HzkyWWXg9ETX9/ad0A3Fu22qz7fywKvqbr0cnPxnlarsWSTShkE+K6ZR
jsL4VIbuY/cHmgzA2oZKCjjcPyRWDoYmUYbvCJHXLRfyqcxIk5Q2ZVGcEzCtbD0Yf+BoomqUApDN
o+Ofch9EONeIAi273KZimP8I2miGyLPFIfSpYLPl1e8obtgGeMUaktM71kA5SHdlzCujL0N9p9mh
dWff+joVfzhsZWbTSGy3Bs76/jaHSAvyXIVYR+qkJGZLl8ilSItXdyCbNgu01ZjeHn0cZsqfcfBG
wFJzR4tUGIkLC+zt0hw9tbEc2pG/oVpfu5UpiAxkx3hn5HGPXVtergt9Za4vDECFCegJ1VkE+Vfr
wN+2dhODI61U3bfPnqMcQvJ+ttEuLfOG8QOsGj9EdoyM7fg50thzbRkfXpdrk7AiP4dIkd/UYkfI
DS/MIHTGWDkuG6kPZB/kgf33D41aMIVRQTLigGFEghiEwaTzDfZ7ldAXj+wbXm+eRuFob+nishTj
Bwj1gaCY1p5rjxvyr3fzBLAiY+B0RDdc4XpQhZCw99SJAflh1Du+0ttASKDh7U80t134pLL2dWTI
z46eyr8GeEs7iUZgVWURlRWT8A5PEntT3llMltLzOZR9bKBeLZBt6n7z70ZZ3cnF0tqitNCTnTYt
5/aaTYSdjiq8SE9C7vulCUIxaSOK/0Vdl2Qw0gE3i7lwlivo37Q1LWbvh7ElS5lX9D5M4hb9bOX1
IIj6bmzlgYd6FBbqbSqtkmAUeZ0Q111NzdN7QKLLjvl5tXos0+BphER60x/Gdpt8VVLll0BLHr08
B+Hde5dYGEB4OzaQEuxxgW9q0tSX333RqFg60nbOQLRCOJz4t5dhLEhfMZHQuRciEH0yfRVPGyke
IkJJtI2f2fCVQoS0np57hprgQRJIJ5kHCa/rSSWbfRAxNPgm4lz5C0mOKnBYm3J7qB+QjVxveo4f
tQ4HRl1prHZgqwsQVK5LMPEe5funbzZfwM/6qd1XM9uxoOxeTu0wMu/jnNZYI1gz7QgYRZeclMd9
mTJpviO26XLaDtTs6GOS9ec2pASWDnPiZb3tZdgswkdxRAA0MuDLfqcmiVDxqqP/Vyin3x7keRpj
qYPdvu/5j+noAtSiitLaMHpX7FBZA5IXOxEtXBxK+o/GlwQ7YCbf3qVmMkhcfvsrn0QE672vUfy5
TJlSLToLVk1iC4uknFZhs/IR05SsWSQU0BnbAOdsJf5juNcUGsQr796aiAwAaBQSxnyr8AoKZOFK
oFVWkQ7CQ038Gozx7LarEiUWRjlQ6tTbpSjAa79sM8KX8xFXdDH87Rq17lEeO9xZHRGdeQ9ebde5
Zvmm9wDTKhdmMElWERwjKXs+JqlFU0A1z8T5ATAf4KRfc8Qadxe0ORqOL+Kot7h4/8KYItbPfVpp
qcZTwJI7avhzetCHzAqkOop/y/RviB7quoJzjSYTVgI1RierIeehqvEgVr/kauXlBILSaGstWlYI
AhLpbgnL4B6ogaQsw0Vl+aO2Uy4wHLiNJ93Fh4tvTFZBYD8nBKK9oFSPCGsBZTt+bKcaGV7QdzBo
2DOzglQ2nKrTWkfZ90i2xQ+CRrto7kAgIX316BgTkqzcDM+x64w6H2YmH/nzwpLz/1X3aebm6N1V
fe9TlETsUGwJ72hImnfIvdFKaZpg9g0kg+HTDFPO4PhKUgHJfF+e9r1+fUC0AhOIgtNGGL4qOtsM
yZqccEn7pUnRAzexGiwvSzMTPdvt7A0PwXfX79/1yKtbmwzgl6j6ZlLpyucVif8mb0XMBcg9EZrE
n11y9AeEFYH0jWSRonTQ5Axs/Kf8+SoKMI6uF4MvkX01jY3LK2drR94pl8rAyXYQJ7uJ+KP5q5+F
5ZIXfasRzUiqSWznP1rDmVPY/QRwDmUeUnGzLs7igLZQ9MyVV7da5UNUCPIzQ2s8/li/VTEysYx+
6X4nbqRsIug10lvL2GQQu50m2SXHMKbD+SJvwuhucsBMgsCzJnqk1eBuPL9Gkc9TvFcU3UaufMNW
LIF+d3gklqolZHwfbA0IiovY/EOdk0G/K7sK+B/Uv/v/3w0YcWGKnt5z6dYRZUPTzuaA/KLhIEyM
/wKcU5SltAzbLPxsHdwmtPWPnOGHj2l6jUYBiVcE95mRWEISSEzJusn6EotvW6c6MVr3ztCYRijL
3Aujm4IhMwLyi4prMKmrzQWol/EWi13iHqlZb3GU6tqtvcDT24+nS3G7K7INrhHg5yOqcwN7b+v+
SN427iwnm1BTdjx2EZW4DVt9LyprM7BKXArdCghgurASh0Z0JBwXN6PFjgc6TJef9ForWvqXsKdS
vNNxhc39jYh5xBDnNf4CxZlTs7712hLfuBM7JggNlPaMTpru1n9LUEkh4sJY6iL4ePu2r1g5kSrm
ig2X4f1TGdDfA3wOFHLycIlG5JooAltqliUlpgRCQG5i/jbi9jIld4huTy+GP6P4xOdad/IE2yan
2pLh+Mnr/1pN7xwVnphgnC6lhEmYhQTh5puzpp9By6HGxP63rFfCaYscaMKqavJvjPL4lFcfw4j+
0L/e5EsTX4aK27xTPpY4ObmkqY4xocKf94Wm/MhJ2xtR6ENpG/MtbpgaQb6mxwqq7DJGsWbhPAUt
V93+1d4Cag8c8t0IXmjgfy/ZrR68PCyv091A8XCJENBjLO8sr1dZa+qg8kb7El1LpJQcbf6S2mCm
UqbCgGx1JrN0qT7Xcam9JopKiRmvSmJcuMob3rbzJCRUMdcAX+60r9b3o3MDtFJiB5coXn1J0CdU
6cf6NOskMD1sp4t3z98wHJWgLljqcKjPtWdKere36cO6bZeXFq2cVHtXz8ASDvKRoDHXWXzUp7+P
C984nwY9ro4YIqHvt+yJGNMpra/9s/PdESczTqS52noMeMVHb4ipKcKFbD7KRhNxuYk9flZnSJcS
cPE4N9BhCcK5vbcop4CceAC71Q2MBwud+DG4P2yjFgxf952v2fxOCwspE9pXIrWAoI6NO0rxUNSt
TjeU1hVx7NVBZRur8LmbQ7TWYO4lmikVf3n9DECpibNG30HdFQA8fX/lnPP1lZUstMXfh4bEt4a+
nGlQKoLPE0qmNIp2w4n4aB/j1llacYXFjVYDriqOcfa/BCEpBL9Jq4dV7mjJrWVi7PrKSHLatzmi
kqGwmaLcqp3ooSy4C3xaCtsx3Qo1Lw3TEd5T7ex8oLvRL+dbPThAMkSeTLb8HZ5T9IwcCR73TopD
dv9brX9L2jxsm1YE7KcUwTlxoD0UTF/fsUD521gp4u4movxxyJeWfAZwY9fGV4jJRo704BqhyXeY
RqhVtn9faXr3LkniKiT5kJbZsHpas//Fv9laLw2zePtEz9AhPwDP6E6YMN6os0QjLImMOf8ds2tf
N02sztWicG/yOi1PcyMbe8WB6T5V/9FP7XC1q1p49knOHEBF2hxQ8rK+9LsV8YmajnLquMCPAiA4
7AGCVdyeFmvoe32xMIJrTAWzLzPbMCC+sN0CCKDONzzZDYg3kNNhg8/PCvnV5AcorXuj8gzsPqVY
QSWTSWb6YBqUfmIkcQ9zFPXKxZoFnwEWTh63iyyzzyhC8Vv8OTHUmQUct+tzTEYoiEsbeYA3D6Ig
glSwRR8PwXxX45md2hoVuMTVu2OJgVwaTEF42/YwywN/pGn0cmOvO5pL7Vi4ZiPVgQYXDab1RD6R
ODR8vtdvrhnZIqtBycBEDfV2HmBdXSzdeM0jOiZglddCoXABmVG4UDKl2JzlLT9Ta+hVUb2ooWMe
sSmj22sAJswz1ykZ5wQtmj8QW4vDfFxh7KIAcJ1rFYekz9rNly5VbySQQ4LM9jo2wT5/E/NSGcg7
TAFu8Tv3fFPxv60cIV+fu5U9Uq9YoxyTMTyUEwJaWrDgD4lFe9oyrxhFJeVOTT5BOqyWw9R6oJLg
y01NTcB+Rj15a/R196SNSXNm5hv+R+B6avd0cW2DAWVIGcBlvXJDpc/bP5WQYNmGOeqS+YkO3h3G
H8IDR8gyEuOYzCvr8l4Fz7Tjmnx7hXt/Z16whZEAXlDP5DcJscphADDs0ATpUxZTbR9aL+7+ngQc
r1ppZIyji7Ca3itNLM56bCp3lWMzFI67cWCyQVPR+Ia8Ft+5ugM9FU4wa6G9mPytYrfnyFa3LRB/
nW5XqCTNjnskqvEK0E7hR7H9fL0i5cvkDUeZded4d4mXS+EgU+gCecy08ThFmaU/uTBeMHEzqBw7
qPZhj1GuHKMcuRvUzmznJNsvpEqvzdr3COaMCgThnBaW2JKdGWpoiTI1X9GdhC6ESY7+kwVycWAd
/q7klu0sJNIzv4VfID9MEepwqjhVrAPUaCSC48EC6QNVw3PhEF5/B7YtFtlEXSEVeeIqOTNRlqz6
gz96TvTijt7yu6TX47IHxBwscc3dvTJsAg3oazEaowin+QUs9hz8W0noY/7J5Tk9D4I9fAdzLd/Z
8gL72LcI1WMoM2I+6LRbHhxZgAiy9azZ8j9SQMdLx0HQcIsraR5dTgEgBeiaTs4tjjHBbiGvgWds
ypQCnGCYuI6qJq0dKS2MO5CwycrkOg0oLTaeb0fFKUj3azJybT/6n9ad0WLxsWGaW/qJ+qhrR0d2
GE+mqRPklvfM6hR1+T3jIIwrGCROLopis0tf2l50/SdRQ/7egL7PhRAWJWK4cBbzrzSyWztSUWRM
5IPB4QvYebNxZt5oSI3KuG63EEq7bO/R1uBiu9/xL80V6AF/4DJOsvRMsc2LiYyY4EiuWJ1AlFuX
D+iZevINV9k0jL/UFy2k8F2GyVF/9yMWbBKOSBx1c1NCoPOGVxv+07ZOjc50ed2mEe5mmsmkRLt9
yANQYtgnIWYtduuGh3Cjj3T8vEnxl2tpFC9Z4XOEz1xzi16cJLDsEfkX6XTWZKBxAvUWFn42ZCYg
Zsoq9Zr6F4m6Lq4dIxTzu1THOyrtx5o3DuRQYFiSiUTW9AQPyEtO+6ke7AA1DZh53pYtSmUMwZWx
Hi30VgjYrkBXLuKUyjfjgpdSCYZGf2IHmDZtD1XomZuN9jnBsh2FIBMOrfzY0BTGT56y9MZGWdSt
YeZnTCWuCn1X5/bdh7844B4/1y/mZxoCnDZmy3Nc67SGFTZys9e363cMYH7BcQbAFf2cNKDovhWP
DAV55TKBgGTr2qWyh6+wyIWqNAs9P7GHyZ6aaR4l4wREgj++RGll5VS7EFxikyOQceeW7a08ZXfi
9d7AwyeFrrapzNaXK/qxHAN0kyOhE+8ofTI9gjV1dv/39+78huOrFVdskb3pqB4GYvZgZNEkJf/c
jwCWdHyFL4617QxiLRJo1PN6qKV5OyP6vENpkPz4iyfKRBvg8XRomoixbBMzeDgJdNCG3kkpfQyO
OmcqxLMqRAb7kQ74a7ekKK4J9vKFqASvk/vR37AKSUOq5+DLozTS0X8eifGYXl4yS2d8FIry05A3
Zhg60rPjptJq1NbdKx/zhG0Kim0Yd4v+L891Gue5mQkvxfOBKj32ahGWGFx7CHB/MV8qhpq7UkhO
aPDEPzdL2GzgRkaG5yqcksnRM5AFrNbuD/+ykGHUnCz980NxzjVtSI+DoJqpgVMfNUKu4o/Nc9c3
nDlNJFWwFzofi4nIxQFj+tVBwaVw3Bi6Cp/zHZsTgGjjQ+94FoEk4Wwc5mr96nLxM3/y09Zc66Sp
8sAThyRN1nqYTbUc42XdZI+JXxC8uWHjbPPAALw6A9pmp473wgQU9psi+Qj2//Llt+S63tjJ35VK
h9vwYa7qImNMjUUYE2dCv5EsbCEUoWzOlxhmZBrMdpUBx1F1yhluFyK4fYvAEICyOf+hwXFFWpPM
KaFSpDpDEbPhEda4reeWAz+kfDpgTNNQ0t8zBpy4nusiyqcgY9gaB+WIaBZTwb6P4xmyIp5OVupS
mhji42y0JUQIuimLnHfe+qG1vO4YKZdn+/oEl3JHu0kmc3htw/1m+d+FRUHaSijjaQU4BKSv1zHd
QQ/oA4CBkIVJ4OTpGe2/mLouEqJM0ZKwvNNDV9ZzSn4c8+UxcQAk2RpAFd+gxZM5pqDU/xgf6f2j
5xAHEIRHMePmODSgXCnBWZGouhJrRlpNhm9V4tOz9NberWILWjjbRw9ogcQRJZ3r9uJ+RY2u7ywN
NYt4FEJ29jY69HDDH/EFv/zRNy75aVCPoSaAqbS/3GTcb8epgk/ZtR0ncSHCoC53NLk46vFgCoYC
cjoeUWnMoYpqVEN/9k+sNp7vyoqRGMmkbnjx96G0vTjyU+ZTYBPULv0ieqzhxESnJyQG3F2kJCOr
/33Q/BzHcyJU62SaUPBY8wkmAetTJc7VeOHGDac7Tv8j95Ol4jBmcB30TPZ384BA5YDCzGFKe3Y1
LiuMBaxKmyW9jFnXSSqlPwCPObkeqh0MqMHWCiQmu4hrVaz0mcv9/DOslAUCgCHJ5rFJ/XooCMyP
8T5niSy29kXEuMYrQJVddEGoybQdj5ii+yXhPCtl9dKA27Quw1e+5BL7N+AulTyBhPzaWc12FfP4
l12Y67vSW+yg7jVkIx4bCbZTr7L0iU8C4mdy+QUompUrTJHLy2hmomoNqQu/tYSqDHmfdl0aHNgC
cqF7lqVmfUs/kTJtj+cGowRaUDQ2iYPcmehb3akmWIW2mcuQGVOm0uXZHWoFF2h2T1vcZ7higfws
t3BYaqttHcdNuQjV3wT1GJ8j/sMyTPJwR6Oo2I+UTBjNXHw4hW6yfVI4hRi59XwQk9AG/ZoSBrA2
pryO9uMkJkY1qajmMBl7j0qTWR8AarWwCgRcbJ4zON/2jwMlWwjuD8B9WzArG7+h3uv+U2DvMNbD
aIvo/3hWARsvVUUc1ZeedO+TkvQG8Eu0ErWRjdcJwjPmi0bH1p2apW4F2X0pfb7velzSbSDQH+xS
UVmRgjXj+xHoPNAnLBHUg7YqiHODz/q8B8sH3seaJLxwDc9tB4OhiASjizYIR2uGcLsSjoOroZmm
FHTqFCf7nlR6WbgkblZtePIzVymSV6juwhBBxImJn8bF9iXtsYUK2WFySbU6dS64izLHLcGkN+Rg
JUb5PoIUDBkJkVt3DZiJcw8hr4EfDVCtjCP/NFOjQgDs+vr/dJG28o3Xu14wxpVVcWOZFqbp9jvK
wfAFT/lNOLH95bTvMlZ6B0OQhm3np0B0fjTN5cmSvX4Z+2L6j4XllB5Rg4Dn4DS3MuHWiTahti1L
/yN4nBeIlkkIf2aqAM/o/QOrEtpBPvEdLBIe66iNXKypBNT7p5fJ8D1sZckKVIRpK9IDuaqiJTcR
Sj17kM0j5rdxw6xPqowBQtckk3KLuwOOqyIlqxpubCJNSpYmmwlKdC5W4VdLRUOOQHErgbqeHQ6k
EDQwMw/SgtReYQ9RqIcPIRsvtVcbZnEtcnVnb6h6mWErSnVHkcyltmQCB2WPYgF/fp+F9f7S3yda
hAqGDo2P5cMlEUqssAZaNAu0mvI8jr7f73dbBfwjA1alkcCuepN/2jbcgVyIu1qXzBLshi3mBUXw
ViUZhOut4wRhMcZ9PvIiR3SZUupM9Nt2eV87rs2FNISUtTBUSslEs8bX48r3DQZh1mXhPcKxWF6l
A+cINATuIN87qMVKnRw8EN9SoV9GTFb/ts2uXkCrkqh0pVmPo3rHKIc6ld7K1fmtvkxek3t6yLU9
Gr4HrcQn2zOKN/q5SoxM9Lr8lG2iqu35rJ4eea8sOhS+6V5thoIYKSsgZ+LulM2qnYsqzwXcsWGI
3M8wHZLKvvILsK6WGEA8sGjgguTrIPrpKQWeg6J2I0bQzXVCM+ZeYe04uT1UyqCTmVWYqE864tJ9
MP5273RiEgtwJfE7NDzpKfld7bC4s3u9KvBAvTKGMpxcyaX1iUDIa7DHBEnr8GQIkZsGYYs5kZQ7
raBzUeuebFywWTLa3GQuQsyfMS4mXoEkiXZAUvSAhoGakqytqgGGbVzrrhMHlhEmi044chVu/af1
hBD77AV66JOdG0nLIiXWYWfmDlS8SpPwtN+udsrunCpVNPAkljlDXWc5pPgZGcUCII2zGRT0kge1
jSU4W9FulfSB3BbTP48sUigfaUugoWkVt20rm5sh0Rk+OeFWy/AwJvr+GRAi+r84dmvIwT7oZ7WZ
YdCQVrOZ4ePvNm+DztP9f25EQFSjOA3m2KE+GIL2pHsg0M8QyMdy6/ak0bHkyHpJ2kWDR48Yeh5v
L2Q9G6gCG2tqQozbT0SoG0g22BRLfSiygiUAoyWI+ClW/9sVcUj+mXKeZBy3Fv/oY9Hlfq/DjuHs
7yI5oMPtP+h0l6hyAfW0S78H6lgv7FKx9Z+CyNEMtIr0QAHTSZzdRqeDeuhpY6A0isXLAQ166LNO
VtsVwt0dOOmk68ypTjXEgbxaP3LVvJ6uXiMdRhfHRzud27d6im3V7rkL9t3GvCYicqmP2w/LxHxz
7D6elQAaYQjFEIn8KNXDSkR4IAjLHbOerCZcGkzcofRAtD0AHWwFBPwUYz1DdiRLkLXG4xQWUf/c
609FRWYyXGzREj5UmzVM6iQCcw7AEcg1MLXYlqRfKLnhlbbIE6rSKG/pmH+U6OhMTJwHsSgFBbz9
FTer0GxLpyY00Ri2Dby8MStuZf8QLUESYeAFZ3mBVsNs23kVY+q0w4rkPg5hYve7Zom/y5CruHVV
cuYEihByDPg6zJ3PjVmpugpmLXA32k3RnvZYYsNQNkDtpM6V9ywHu1SvV7MSsYCcIcL0llcA4FOJ
lMOBWKeLYuyHHYvTh5rRY2ain8sxGHzVuVJutkjLgxSf37RejQx3EJw6E+g9/6QeWF9XN3Oao8Ry
EMmflOaCJtMeo9XlLYNdy6oYl79DEQdRI3zvji/HDBLI/7tuJAOM5dfB/2j/R97lIMnUC2ihYJ5E
9s0s+sgBarK0CNtOPkzf8PTvDgE64O6FF5vTyrTNmn+EdnIkwPl2Ph+M1E6fGsna3bwjo9QOGVCt
2jkUeFo5a93SfPX5IGWyYiBitwwOvS8JO+MqrF0ZV/0J4lbX73l9eVdjzQwvKre6TPwp8HkjNDeY
oD/S6XsLZPR4qBf8PKpKX/HNq+pB3jssLSsgS1FHCEgFCa4C2PlR0++Z+o10g9x1ltNZL91YxcBm
fq/Q8eHj+RsT3eHC9vlc78LtjfqlSXQkStDdD/dw4nfAtYPUEGxpCzqP/06USE2RXAZvnp1XmNKF
ZhCLLdMiTyPGb7leOEgIe0BLYRtpog4S4ZN0okJyG/RQCvHaoqEP8O4IsR01ccIGPX6NZVMg12I3
dx3vtkgVshJMeEFMrVzdG7IyS2EafIC4VXwJENU+/3PH62lV5kOtL5TWEwFdWXe/wIyzmeJrt02r
3qLYXFh1lzibBNwXdwvczD3BiED7EpSvBLIYN/GWahqbZYhrrXpoj/P54njs5KIgud8L7Y2thFVc
/f6AYDG7ZzNh3b0VEI13H5n5YKZWMcunyR2g2oxHLKyZiJ9LHEju7fmFKw+Z1nwfucICvf0KJqxl
aXJnF5luFQdE74eXDs9NGZgOQLOteyCieKfw1yzvULoHu1mmkfTSHRyYi7pmeYBIgbe1Ew/Yjy5+
2TIo7jiDbK4gi3FWsvF9f5SXYPHVpj5SmIH6O8R5V9ylWxoYAUHZuMlSjEn4ECDHjJlHOqQcQMVB
4YZCWgsgrzs+GcL0WN7puW/I5dmy6s3062Wc5qiU/hgDoOxySpt0xW5hcKCuXdoDWK244xwunlX7
iv+PAt5HeDYJeBV6s/OFY0Gk4ejQ3y5SNu4iPhp7SXvXKIphKy4MD3iqNEm7Rk7F90F/SXzvPcCO
w4dDKszAfGxDGk6EVx2pKQIkHQ3X2Th1i/bVsEoSA0/VaB+AWMwvgt6u14nlSoVlkaNuA8zkAoqw
bDhVv/wGKmuYuq06cHIjA3MrP0tFScChH168VcyiD4tLBOP8ufxwG9ldFGI68p8Hxl1Mie48SKqq
yQD464RQJz4pqWBgI/fPmVM615zGFTMclsnomqcr4FrQsJgnBGjuf3GXJBekTAEFlYhvDEf5gMVg
FB/iLnZ/VGPeOQnBbUyp98uMhNoSf58wS/YGTCzI9F61XST7NU5FLX7V9GETISSU2oeNdIa0zH2J
Kh838sds7QgzqaWDzDK4XEv2uKvY8EwMHflnA0Muds76mHS9GGQgBkVzr3KVfMGrz/ghjG0gchDd
FRJZGpXXbFvSPLsQJNRkl7JLv0xbWtDxI1KePPTsCQd2C8EkWom066hkzq9/t+84X+HuaD2hXQIk
x9iEmA1yL5kYR4NtkchBqQT+3TYwTc8IU8NoZA0orio2+BIUfgw+Zbw5DIUzZnbbYWsI3w3Qbreu
CpeA2DpXiEpiMMXsjFMMA0+JdYaZVmFtIv7VsMRGiBexoMzg7O7RrSyw1HxzzVWLRk7OQz1UOm1U
y6Prf1M5qrAJHIlbX9ycpqIZD42XD8Niid5bBGXkRMua28878JdI/X8NftBvOQIMUkPm2OmJ/d4k
wsfd9XLKjOBrBPBTPJjvTRqwg0ISpD3vt1s8ZvScyY6wLOjOq0fwhNATrjQw65yVt/8fDUiB2TSP
J5nUKqzy4ZVbiuJF6ux9086aTqHTEhS/D52MaO0tMWuB1oZ7PKRQT8xDYKEiwrGy+5tyaETSLpDD
RPOYU+6KouW6P+sThTXE/W9DhtCUAw8FJkOgfBlCp5Nm95f7NN/RhOK02A+q5huzxrpah5ytUdjQ
YzJ3jDOHzWCjivkdXbCbR7ROca7xiTuA4HwOYTC/5cpUG/Gitt330mgsut9w+ss6GBmB2S5CKh1C
wE/ShqUEVVnOQ/19W4dtypfKmsf7sqGA6zQQ8TBiEfMiEZIBl3eWxP98OmgAm5Sj1JINvRzvko2O
e1Al/rtDz/ek0B7MoCp0qrV3yRR3TPSF8303PVOVC9UD8HmQxeeL7Yd+n9ygeuC2POa4ohln/FqA
8UISNQ/YAQ/sG2vYft4SWv86OuGDlyh2HzPgmmgDe/6Zvhz7Keus66b8I3eym0degug60mjGIcwx
3rR5SL28opS5AlVGqyGXSpK30844RIpY6cI68WOkqCRt/QCiVXtXKXBWAkxAEGhphRMOaapDc3ES
JIUU3p7PiBMfiRSpXh6pO+EFVGWKz/xyxHcMrhuyu8sv/ucm0AZsRgjSB8tMCC7FGZYMwYdP2Ok+
06+sVDzxiJ/agsDXzaGYTML7HcTHyvS04noDFB4eN6Oc/q/EVXOtDRYVGdG0GFBQA1Ms/U5h7Oww
VMD5ZpZ2Xong7A9/ExINesJ0e3r8BoZv3ln9tQC/MfcTsBob2LCK/yJUDEhulwVRLRE8Xns/fwZr
sl+Ov9pbbQTMXeun2+YlREARrWLbKi11/nMipPEuRPK8Eko8tzUSX0G0cCazeVEqUqdrLTX/h1B7
VbkWJWkYXqwfeIO6wpT0rVXkAfYiywqTdY4sQYHuH9W10C2iUMP4Spg4eX1y2z0LUB9QtIikMi4G
C+C+K6I87pd4raMD+bRAdxK20L56lbD1aUzUj2Poub3BLTbny6PtxYHQdYtwNTIscYWP+MuMYX9E
cXvt3mXqIduuO13czPCdEiWLif8qIXwhnjbfcF2q4F7tAPWH2JzrWaB78tVg2wah85poJ7Dnbuvj
gaPBQR7VylkAkd4EYe/wkIrPvrpeBhviNsEBwGLsay14UyWYHAZTDx0rVNODnp+mqdl4U2TYD28R
dJxoZQ3mWZJdrp+x/idrJgqv4ouCFFgjGa19mTr+JUJo9C1C1i+zalrTOQdp9UKsnaen+Yf80HDh
ix6UWC0xHFQDTNLpG1HN8N6oVxiJBA4B+Rf52sbsYxJAcaIoHyTjChw2mFzUlkrNdqEZxbtS7A5L
JLRQaM7KjwnhzD+IDt26//T0eDKY9v8Gn2wGhpijb+YZA9auxLM5vCopHT4pxNU2hTr9rMeFZwNo
Oo8ELOYHfzkSU62iQsvZBZowbQLkIEgv5SZihxr15xM7PYcku4mcWcwjic2hdwja7XbaYBwuJWPr
ESZ4BVGyug3hz0EANbn4WPsG2FObbETvrpA3xIShj1t0KbMbYJ8J738JwZvXX35Ar/o5UHGAcWo/
sRKirL1bp2oOdqVrgJpSgAOy2a1O5tm2K0bxwwXqIcwnpFIZrHP+Pbj8lTK9ONSmDrwMuHzQtZhU
/ZqhAQPehEDU/MC9FO8LWl76CDvF4CCpn0DE7z8xBv3BTi5FO8ZZI0Y5wH0xt2Oa7s1lgJvTefLt
yFOLyDsNlNDAjFcV9Tg6UEMGzzk7FmUAWH0Ceh1pUIue6lAXVYPRZ5VfJ+Eb4+9pS+MIQxd+TGMl
dUJKjjR29LH4UQC/gs2fQWGAqHALDbrXM0RxZCkNGwJZL6RB4S96rNuUN3FC3Fd0yDDQOBgHAUeE
ii7xqJv9UXQQHyBWqz667bjkSRF6mcHCasv4VMG8cste4jI5j+2YQ3bJfpBe19zZHAoEpLKmg6el
CaRoN8JiWj0O4xLdjUcOeyJeAk9qBniPc7xhZtzpDJZt9s56rHkzQtN+f5NZap2eCw+Fdxgnxexe
HG8wS5ZfK7nK7gpzD6p3HWbCaH+ginvTWVYrnZsuxsURpOtF5JQV/75QDVUe6miz3W+THMW96/Hd
rXCZcDgteI8+yc2TtwBm1VLxBB6dgoiYdmznwL08NTJNiunCnISuhwKG5RAxAY32OBtN9FFcoY1h
QRvAeNdLejKwenLkGtGbEPxknYsuee3wTxBIKNUbdmTfwA+BEw0N3DD4qRMnrMNGjeD73vSo8KRo
e3peaJHwSxsvKLDrA6Fx2V7KAnhPE5D/CGH758rX6+EKrJT/IOXX72dsmEdNSAR0ZP41wB9rlS/K
0/GY0+zgAev2vz2aDxZsQuDumyfdtu+BNq0SYUUEMUx86mv8l6RSW+2dGfnWqDHUvJ3jhTUiLmpi
KSj2t2S3px+1ieFjr2OOpF1WgpdgdAzdb0XNmRNK/es/mexTbt3q5z1gO+P3k23pUhAuwaVyiiJt
qcJvte0zrMJCqdBshAL1Tq2V9JiuiFcvDJPvY/HapAtg6IeeyIcrzYPH6LNJtB+efBnqTzhW9l6C
An5cKz8DYFsrmqmSyaRYD4EQZ9SAEBLg3c3K6tvP7wR5PbavHa4eJOfOeiLx4nkqIlHn/fqVmyO5
2/Ix2xe/1wp751eopGSp99d+itWjCU6IVvLcap/GSjp31D1n1bwRmhr+e3YPQ/H2Q8q4QX1aRBl+
ilJwLTkwp4xCoDgA9M0r4jUEuDrcGtPGqd+pOv02KFWjV5DRbFtK562SisWMkgcRaYtdf0fdQDbN
Ouey/XdE7HmwiPpmLzMNPiiEFazHj3cobKYdEZkLStG3KB1mshtg8wlpz/13547qwTuMPF/SAK3Y
YTAbzZ7qy8pFgIwmom/2lNc/gM84KecP6oGal5CTfv/H6vY7N4Bai5hahRJJbosOlBGx+9APUMjP
5NbVc7Fy4faYxI2IAch0yCsGkdtuUHJkPYDAC3dQlLLtGT+bPSsnxZ9J/eAFR9jtwGarLYWWV6sI
Wd7MbjwLoGq+G5zMR+VSE/wxOgho3HQPTz1nGbNDDJ92jXd2IbPd/gNl+dZIwsNQZhuuIOLsoZdH
XOVvRWkV1Q2s9Y5EaIngMf1tYavZ5kwfn/Xqiiw3gIlk9ubICxgIz7Sd5PAmZtnJtIPsaleBzRch
56L2gaGyCSv41uQAwPs/hdomMNGujplD1v6E2LFo4LcxTN+HkoPKZ717/qDeVX3INs/bgzRz35u8
BqdGCxMmik/JLUpwLbXANNXMKb1wJ7Vh6IOd3eLSXNAOq2DFXAKSYcazHOtW3twK4DkYekK195qm
BI8V3xTgjdkkczJwJeIT9PjmLd0kvybVzmNi9xeVMiqrW3pm7lQlvwpiXWVxd0l6+fr+IoYBG/7a
7p2lzka6U7clMjpoJSPMe102oc60TSJTOLFYRIjLpPLx0fxumvefuIwcwH9sP9mDaX0un60usq8A
8T5kxogy9+auZ7OrxKp71pNKZQHGyq7435+OGZhIsv+KOxTRLmL7TA1gBImpIIb6KeE/7yA4ukgP
rXIuoaLZLkapnXxg88GOB32s5QkGj1lUGCPEHQl1s8FDG7Du0i8cRQkSeKccdpUBszsrm9zHg5Ch
QtjVjPOs+E/Rd96lbpy6aNYFLnrrMWGLaLN6EMv0F2xtM0qU0mEQzcen+75Aia4n9akNeYKWF3cF
o55iYSPKnXlzj/ra99EVV+pUEhU4gV5tGWV56MEvodwzFKP79L6nkW2iofkvbM0VH3+2TOIDKt0k
8nYFZS1yExZtwFG5wJ8G7SCxd55F8LKXgWM4OF6kAVB9QIyk9BiUW2u3hbX/Igt49AknUAmg6O5A
UImxcqDaq6e4RyPLP3kp3W9LK9uRBT5cfu8knWDUYBqCoVh+CGDbZ8kTzN23Bvqv75EBw/wOB22W
8rQYrVcLo3jLGMwkjkOpbbOn79SexCF3xWUsF0UNfaf8VBDnUDExzvkN+rnOGWOisVdn+7PmglkE
ijqKvXZ1cXVgKvVr+qnkwGK2llRM4hyJ3CX2xWfuE/Ye/oTATmVcOqmwcQxA0FeH8GwlCTSFr+YR
KTyQBaWUNzm3FCGQezEWRdX1FvCb2fNL5JNdcp8zi2ZlFdtyKpxdwblv8CcwvoBa7QDhmi5kNHdh
UNZ5UQsdr75DxC7bnoQPQhmyCvUN5X+9llwU5bbUfxLV+2ax09kRu8r1OB1d2B1C4cCf2FQWF0LO
q4DmHuFff/yF4xWUpyLXZ2OMoO4nVz4fDPLuCYIsXehr3VM3PlBmhnEuUeNPrs91iVQ+fJodHvtq
WmTRjS6BL47vfKhDT21m6JXo5KP4dO2302znHMPX8GfxY1WCRnC1LbsLWfbkmRqf6XesJUavjalc
wh8RrVn4qYwbsvFW+qlpifldAd9043r+piCGwMpATThfBkmfrSCE7TksPlu7RQS2HZV5jst33ukA
MynKK/LDn7KEaGLQHDrZoVkUOtWAeCVr991hKNEhQWxFwrzoogz1PlKJc1ZBT6kYP4sV4SKNOX7x
W4LOoUg+EZPlQw+o9InvzotLDHnMDmkBMlAFg7Y1POCDqY1m5o6ZYqjyte8a6XjfpuZIvo1JC5uz
aLFbAEw7D7XZUzjkHxe9RTSRLvUB3wnXxpHVI5c3gldAdk5Ea8qXHirOYnrKLfpMihPfJV5+X47q
/qHoFQI9zPGzXQ5MBYkGbPjftyYvaBT/TTVBzUcomvxbDUnF9+NAdNlliabO7ZbSvZFq6Bx2aOAU
WUEiZ37kSAD5rfR69y2SnF1vg0Gxsd4sHl5+55ARLA2agxHewVpzBa4G8T76osVdNUJzYGqbdHOa
qRlBhLgehN9HvhuqaVCawSefUhivYy1Cu6GZ7ENOe2f9JedJCp0kAjjPOvrPFTGVpraksYkbCzry
/7BwiMV4MqtDxe7oVypW82qKMuliIEugMsnGCIhiLLLAa/G8OQPJfKjBN3L+n2TgjqpcBD0A9Uf4
RzUBqcz3HJOKY3uFq33QRPtj9ML4t7XPTc1GsKLL7hkJdnrvv2NOGTMn28YNx6XB1NjlMvjZN5RX
fnKfVPLsY/Qu+NHmFa4E+U9UfWSFa6k/nXPcjqJOXENOOKK+t8+0XybweaCGVciTKaA/QNtL2lvs
qz//W8te0xXmUB1xAW2+pw9as43401/+RwVc0JQJjdCyP0QNjyU6I+Rh+fbON1D7iSGLCNGgU9Hy
BEuKo2T5ukxliZw5DTyroRYY0GGQKMjZ9SCZUa+thjoZjjDaL56sjueelrR0Qdkzi+X9f1wn/tzi
5masqV4C5sEK1qlf4WBXxE4nGknf+OhoecixqkIMj3+rBKpnGGNZ7UPNlUw0vy6IwA1UyumpOLmz
zc85eA/ANW3NAODXWvZEcBvYTXr1BRPRfVngMVyDvfhbor9htori/vUj4wb0E+V0o6JVH+CzWnSy
WZ8rssjqaPJ5nN+OVENLrgxCXACx3hWL2F8IEbatwn/KDAE8dG6u2OIuvn0B1Mi5zoSYCT0npEpm
smHQW84JFSDFYJ33qNWv6pSg7TPhrLd6MYpHyWv8Y88flL8xGVhMC7SQ3u2cMlBFxsJQh2Pm/gGA
r0u/JyCGAbz0K4h6WrJ1NTYMQYChvSTq+FqPHtBZ1dsBCp0vM0abRtN5TpxBiPFGkLnP2KK2xv/4
u5E+AHPRGd3C/xB5LXHaOB+oZImjTuwhfh/lTWQ1RAV8Wb4nd701BYUY+vk9P1DNavIBQIH0VoyK
pWFgkhFvX8lgDLTkjfvCNE5NIDosqsFGCQZUoL6H6kjWgrhbCUEj+lnx0xtKnzYVfWkLpZhP3rHr
u5RuHp+/pPIpQsLB4Kf49ToRpmLAbdqfAWA4J9QH7cLRBYM4AwCjQcatbCQfkK92L9xz9Wgk7YlV
H3FUqTIZTg8yNU3D8vCM3Cm1H0Mn0Reyl2vm8+F33XUCFc/GITRU1gC6MlapNdiDDaXDfuAfNqdb
bPYFKSgkHibbflYW/3w/Y2x5hOzEJULPwolc6tNXcb/MAX+OMWwuGNS5ZFWKNh+pJ1s1F4NXJZUt
nG4m0fR98L3QTdN943vYBzm2C3ZJay3RvKcaZASO915hMJnDJODI4C4mqLGBYaPR02/EgaB9hsLS
1i/jsqk/dkwRVlCKEt4R8zJ31igh2/Thcnu/mPu4lAPItHhkjBwqEXSDFOa+QyT6hVnHbu/cDkpU
qjvag6HU0kVlmLyNhmEx2Sn+II/8DbHCQU7VqdEn1K4TPF4+uimtbozHmv6TpfbybbKobqsHKHeR
VVy+CHNLSxZA84oDFDwkDb5Ql9UgCWrn5bnZ8l06nEGtjmnY0fv/lnf8puIMC8zn7sikaqMpUtC1
13sz5BrH93I9pSt9TP+I1xJmgiKWmVWgGmrfmjf/TKR4L0o32kOSM/+vXZPtgwqfAmp8ZXQxYZlL
iJBemZ6okVgJ0tOv+Yd6R2UaGrNG9QiBZw7xPbcIcRLZ/nDtik2vF4ki+JWq3z1oeAqMIAHHhGzq
MTCY668AT+yIPxarWgYKz31yA+qW6TD9h4kUO5HzH+dvSVUn7gYSmRwAOux3tGQV0XNX92cn4knR
eajrhk7XmzHHRsjR1G2wNpyXI7NzCswUJeM2taaww9SYqe5dZrQn/50aMYnwwLAZPEuTysoynHcg
E8e24gRHRs9tLo9/4CyvFLEP7peBZROC8nV+sklhlbge0Pvt2BLdAU3AXsImmx61h7QDGLCVLeOv
S1HIfzF0aZv7+YzGirMQuGhmqCC6gmZHOnpg0SV32ttH+6xCxSdMyYVVtqAWxOO94vkYlkrf//cE
kAEMhN2J0aCqwrWX63GA8u+2RyRQrSoYeE2sX6TCvUWV0pPItFv+xYzn/0h+C5dE+bAsdd4tn87P
a4Sxctk/GSvtO1J88a/3Lxg3XbZ/w6BysryubqgTsmPJz566xTsh6eK2yzOreBxjECGaC2164RF6
KU7plEKg+mxASCX4vokiMaS/TmQ1d5BMuj0zXMkTpa63xX0Lq/OAdZH2MJpMGcKZ6sgbSmhE/GEK
Eb2z6HXjycsu0t73VYI8nocCM8/DDncoablJ8fvRPPLgtxBWz4fqvAyM18ogYYQSfrngM0paModp
Li0tEhaxkPPZ8kJ518nG0K9tw4r4/J2A4wZbHh5ULGKP3tC9d3gP5d7KRzZ5rkJpNH9w5Gd0kwaa
WCmh/Ip0rt0gkageqTZ4ZsntERk6gUPTbv9VDHCoN42OpEO+VB2QWksFZnJlqTV5GCsECogQxdms
F9e8BXIzZdQhSjfvFexyoOINuJHDjT0RK2mh3BS4Y1/poeLKbof50kFDXds4txOBXCXdJ44Ce6P+
wzLb2ubqhF7uz1w9SvLv2UZxnMYh+fVJr0bHTCwYpNsE6xyZJcT/Z2dXyUHDHL7ovoJZWeRpH5cZ
d2csQ1gwT2/Bm4EiTmh0gKKyuISrVUvwVl0ceQkxreNv15zcYDp8kym9HofirnqyncxAX8rXvyu6
ve3hTLVhR77Wbc7gQaOEJs2Q1wFmszQejOgwDoxnOiOxwUl91NIV9Oq9/3Ll5iudXbBzYOni65Xy
gREXCEtKJkv6O8KY3BPXbl8MyLiydu5d2HJQXR27JbFxZJH8ujfNtMSY5shBJZ/NL/VXPeIQGFu+
rS6THP17akc7IEGAofnREtJe9TJe5DHmEFMXg6Yl/tnp4D1cN7e0yL2cjqZkTXHuioWXk1E4Abuc
qbF0j8oFR64ysv1jblXywHr1/DtvXADhRDj0HocVgC3Ll8YOBRcPrmTmk33nSO2kFowV3MgXkHjX
b6qaIi4AVtPIkXll3VEQaTyFKEHvdncAmhyt7to//cOYX9ycTqnDGNB2VtQMss/z6szcPJh+vyrK
z4Dsl0y4FtQbRDnwAdiR3//jC73atYUJAZr5DTX+uk4wOmRcjnd602iIIfi5lQY/1W8vaWGLRuiJ
UJVdsxWy8wrKL9LIeMr+oYrEdOf0yBmVkJ0WEK+kaaS7QEx6MJ9jBb//ckt7HqXK9rprh4ViLIAI
fB1pEubFTmdG6QfzWTUQXMcW9y+jd0hYK4CNuzrLtHHg0EklLekv/lv+++zEwGa29yjnwRW0B5qF
DRJ4X7O7fxydUPmpmdsnQOarHF8RT2CY5+NaoVucmwg/RtwRtUYcYK1fqAbWj0ary5abY0PYhBbC
iNpldLBFpka4YaltRNzGlhPBBMA9ZZSBcWI7povhZrr1d203yEsgEC5vtGgajcj4lVErkyd8/Khc
09V334fZNKcF8Zd3p97qBOqfokiYldu3ZhlVo9Xg+4IkcyJ4OEFtLPFLQKsqr/ftvq9E+zV8bwzn
0RaSZBMnoP9veCGPyYIYLJfWyAYRQZ10nnrNUGKWewUYN+i0qFORaX4N/fA5VKMaLnR6oLZXizwj
WOJDCHjCApbnZ3xB7FFcC2nlraZeI1RwVLMuoDeOlA1hObe3zWt3V1uMNQ4395h5tD0/dedaUGQu
TmcAJX1gSyWuBb+GEB+lVTdkuZ6QMuooH/97aa2wxgjSvS/H/wFVnXqJSvDIseMT+ydUWvbJ790k
fOyXX0Yz8v2D+TQBEGjFa9MUEfZbe5L7okh+/0Veg22lJCSPNqvj7XO5iuYAf6gV5Was82/dNYzB
uH7BTOFhK8599AWCjbCQN3l71TFJ4rzSYv3g1ULQHcUZylW6WRT6PEFBbWNF77vLkEDqpcsWClTV
08QJqIT8S1AerfSc9GUf7VAFTU47aIjhHW9h/CBBhfoSoo/UfZOlO3yTCiKOL3Pin/tNVJLHazWH
/RU2A68cmU7cW/4dUq/6BywJ4jr/NQRvP8Lpo7RhIkT6xgPe/u3JaENsJ0OvVpTK4NV+BLKCrCgZ
qmzKwfI8Wuc2CYOp8ZRDyVov1wTXRV/uAC4Fdk5+5lUS9+LgiBoPd4xRWRtrf32nHLOXzwP+oLbp
o5FJZde6cxYTvZVnxR5CxE/nfK2oQA/9oww3niNuVZ14eLEgIBH7rxQTLLLmtKdVvHPfjUJPk62b
NUVS9hKVUvYwemqr+GIelgTDmrBuCe1pMLJU2CPmXJ1uZQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
