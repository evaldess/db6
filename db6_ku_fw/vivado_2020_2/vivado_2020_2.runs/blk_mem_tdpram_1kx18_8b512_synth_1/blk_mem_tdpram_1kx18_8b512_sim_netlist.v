// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:35 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/blk_mem_tdpram_1kx18_8b512_synth_1/blk_mem_tdpram_1kx18_8b512_sim_netlist.v
// Design      : blk_mem_tdpram_1kx18_8b512
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "blk_mem_tdpram_1kx18_8b512,blk_mem_gen_v8_4_4,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module blk_mem_tdpram_1kx18_8b512
   (clka,
    rsta,
    ena,
    wea,
    addra,
    dina,
    douta,
    clkb,
    rstb,
    enb,
    web,
    addrb,
    dinb,
    doutb,
    sleep);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA RST" *) input rsta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA EN" *) input ena;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [8:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [7:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [7:0]douta;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTB, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clkb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB RST" *) input rstb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB EN" *) input enb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB WE" *) input [0:0]web;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB ADDR" *) input [8:0]addrb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DIN" *) input [7:0]dinb;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTB DOUT" *) output [7:0]doutb;
  input sleep;

  wire [8:0]addra;
  wire [8:0]addrb;
  wire clka;
  wire [7:0]dina;
  wire [7:0]dinb;
  wire [7:0]douta;
  wire [7:0]doutb;
  wire ena;
  wire enb;
  wire rsta;
  wire rstb;
  wire sleep;
  wire [0:0]wea;
  wire [0:0]web;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [8:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [8:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [7:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "9" *) 
  (* C_ADDRB_WIDTH = "9" *) 
  (* C_ALGORITHM = "0" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "1" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "1" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.805807 mW" *) 
  (* C_FAMILY = "kintexu" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "1" *) 
  (* C_HAS_ENB = "1" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "1" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "1" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "1" *) 
  (* C_HAS_RSTB = "1" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "blk_mem_tdpram_1kx18_8b512.mem" *) 
  (* C_INIT_FILE_NAME = "blk_mem_tdpram_1kx18_8b512.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "2" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "4" *) 
  (* C_READ_DEPTH_A = "512" *) 
  (* C_READ_DEPTH_B = "512" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "8" *) 
  (* C_READ_WIDTH_B = "8" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "512" *) 
  (* C_WRITE_DEPTH_B = "512" *) 
  (* C_WRITE_MODE_A = "READ_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "8" *) 
  (* C_WRITE_WIDTH_B = "8" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  blk_mem_tdpram_1kx18_8b512_blk_mem_gen_v8_4_4 U0
       (.addra(addra),
        .addrb(addrb),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb(dinb),
        .douta(douta),
        .doutb(doutb),
        .eccpipece(1'b0),
        .ena(ena),
        .enb(enb),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[8:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(rsta),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(rstb),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[8:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[7:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(sleep),
        .wea(wea),
        .web(web));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
QGLtnqZzRetDH6gCWT4Js6wuLlZfrNx/VJp3sfR2NF+cxypO5AxN0oDKLJJtmdrtE/ueNDg+Qf7Z
TqBNRojORA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
B6Ger3hRvfjHkaJ+W8639Kl3TzC9TogLuklOXEiMNdc4Im+DjEUzxb3DKlzu0VW3zxZqjJ3+wsW/
LnRmPCESi5Y9eRJaLFXg79EMfoj4X+nTdHAP6yCfltBADKegZ12gpnB/8ey5yn2KA74LUtPC7jna
iyjqSfsWLGnz6UdXzwk=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
BX+DxgMPRyZbYojCUR9Sk8Lq+3ZigBz4yMFHQkmurfdfDzyTPJCE827eGiPyTenK1QPVhEtf9g06
0BFXq/0COPuU1BWJwdkz1c4dE6/exDwhvEh+hPx3vRY6z8fDEf6aGVIXrHDvrmddehe7yMSIpo+k
aXHR06EEdfHCFY4TggYwhcJVXjkE+ApsVuyfmEfPmYjo8hCWyQyBsUWIOY03q1+MvUjjsmTwgs9g
fh5MY9ToaLfoJxPKdCpsqrBX4LJ+VDGFlAqIcqHTE2jCmPiToZAFXB7fzf1wDjFCBlJyFVDBGi0i
m+CouLSb7X1mvVhdDZgNrZDJMV688Bu3o54vew==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DaIU/Ddc8USbZ2mURzujJDWDH1JbHl5tFVOOQ2aVaUPIA71yyE38OXVLEtF8rNmujYH30nEeQ+FV
LVJ16aaHw+iiuaqorTM3K5KLohVlN+WlcEtSXHuPNHjw8ddqtzpaX7pH1zqZH+YmfCL5oaNLqDH4
rkBnUl0/Gm/hzSwKjYhXGQFYQ+gGP99OjXakzrAqZzp/Iq4gt+Z5902/JV9thd/isHQImJ0QyK8M
EKM579iPAfXGes2mbiNYHcvDmSPYmW1zlhOE++N1EKeea7j/msnKeyhlC+hGE4Xfn4TVvqgQexCT
rp/wS/MosY6WH1aKFQlFH2hEppA7KXUaQlvG+w==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XmWoAt4X8hrCJ5yTyug4ajJW5UhfkLNibzjihWzZ4Cr9hQSvWZoTc8rjGsLPbz6Le+/9iI5KxecS
eR0wiAO+G2IkwhZgVBeZdKoFnlnTVAyLjk9wMAFXNyJZM6b1NDbfXlPcUsC6JePvPlwwdWknkSsC
r3KvgkWAS+O3xvRmaNw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Hw3Y+rShKrXiUViyNU1/O2qv6TgheLHBnFMj1i9MUGrHYqh9pLfLYUgWR7S2vj4jv4S+Ks0BpP4p
dKEqVAFmTCfQNEUHaVcFPkOHgig6L4mhLY6HUUKJoRgiQepgLi/W3V+ZZPQSQFkB3CU4MsJzhXvR
yLcpDriZy8cnAHD87Zi5DrNGBzj3kigJeM0du6lCQbxtF5aEdoaNP+YTnIFtcqYhoYnswQlYt0sV
HKgFA8VzqzL5WYnpH7+1IKmFkJBHkyqHCa9wPK0qCKnxkuDj70YzPVqQ+cocdKU+/gNdpCOdZlci
F2HTxrgfrXndJru3TiDqu4UavqAe0MNuFp3t0w==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XPVggoWL6aXz+MpODTOZhEUQDa0vfEnUDaYeEHXm2vGyqKJujN2c/FFAFBeBYdJATLsIsQ+BqoPc
pBbcFYXDBfOtFIW2dH6Y1OoD65KyJ/hAq8coa21kFgq4hFat5vzZ2iIfkCpTUr4vDZO7Xne8cZO9
WsHffoTCt5rS59wWm2b8I5R8Eh2TUbQg3RCyrcnD66cvcEnlXe1CNMQ4/loVJpA4IBinBf820Wjc
vw2fZbGI0jXC+ACSHOviH63Xwmn+aRV5Ppkup7IYoon/ieKapRQeASu3TTY37xSBXiInSdtMTzJ6
+4GfO4eSHVriCk/sWbuTBzfRzoSShrnHjzz5LA==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
L78XuiswVcgO2gtebzL7SA9BC/jJGAM0v6S9pzmyqL+QYzRneiYeGyDmsW33jEVVSTuNjTXkBLY7
yTOKQruatwe4V0OLi6174saSAmPgerSV1GyLP7KhmusLV/N61avC9TPam+tekhKeE0tds4EnJ3et
4JdLh+SE4Z4pcuqCjB5MFneIYKKWDx7siU6oesAQtoSJOesfMchX63MhOjOHFP/ch+1gHv3T45hg
IGF7V7TrdREVE4f9631tlVJ1o2Dypsmo/76Itz5WCGlTMjAnWXN8IXxKN+PZ3dyt1wjrZm2P/td+
xiGszFnSLrRvw/HferwtSmRx8q0fiHZ88roGTw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
kDX5kq2QEe25429T6vQqBCFvV1McKTJRYfK99ymVNK2GGvGLXSzgwJHwB2fj9rM0wme3zYYY0vQR
x+9F4L7KLlOVY6qY3LB59uDzyXBI3mMZaS905HXHJkdZHWtQWpfHhl27LqL+8FSluaD6F+KFfYOV
CwIOVuCIp/XjxFXpNBik7YiPt4kHOlDA97IXNLnYUn/g1csGqeNWce4UTne50ggWvLYGbTFGmTjT
N67TpUiGRVRCSv8Tax72GWFIMFZk3Tlp68ZUSQEybZMWX1U9XdMdtxfvNGhf8mi5jQJ2SupSzKu4
T/+53IN9T8aLePAiGBKKG1ZBj4y1ZyYA7XYvjw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 28800)
`pragma protect data_block
kSxUvMk7/Tfn9BlM7jU7pe+g4w7/03mPqpm4gJ6QMowiK3ldfn5Z8KUiClQUGfg+DRGV9VRcew9o
lQhbbRcK49O5C6VgXob58Ftz5jHLQitehMuCyWJhmNwkWB7Hl/ByKfCODDXvFzG8SI9E6rN7oxNL
U2Dd/G8LbmYn361mlNoczsfuxVcfmpRzP50ZHjzGntGCVIm7POom0l8iIniawKk4Ia6Qh9mcqPiO
Ao4l7f82F+2ZuuZOyKv8UHKojj9fTrh+vsWzaW98akQNtaUDufvqzT7+49nLutxdFmGW813mZfcf
q9Rrn7fnmcHS3F4j2eSl2Iu/XpAyMF/BvU7IJmvB8QSzLzgGbS0n8BWrbPiJ4F1CtVullkb+IkmE
PC2CLQOe5PyrHdMnYz0D2pIOCZ4m+VDmXMaWYv2gmKOGHzyRM9EfO0C+eg4i6P2lhe0ovPHuDRa7
zvdw/Dvp7alMOVpSWpFTDH+WRHe+8r3VNU6iakpGjov4cL/1tuWJ5da0gb3LCwP+2U1Bi/GeBsHM
ioAdlQGYDhI4zr7rub60luGqItvyZhPrAWZ2iCNWF7/1q/oVcpEmpWrtkClw8SF1ncCmaixeWK2A
RzHr8eHiVu8FBoieX5RHz1HcDo8tGjaHyWIaHk09cLCIFZxu7MZ0tEgUxGIeyc+KqX/wBoiKCgpL
YN2Fk9YIdP8L+wZAWSQU4Kz+NTv9EjL5+VINK1pzNxq39Pr9W6YadxXLZCEHmaw2HSWAtyp4rhEM
JI/ttbeWz9TF6Go+RCQ2BS6QxWmBJ/Am3LZSgeIlR7mXeLEzodqCY+Q9sIk6QMaFfbpg2Zp3Sk4r
Eo9acqcmsky+EXjUQqBsZYvYjUnsQs9vZP4f7vwIb8kzPiDT1PCuTWwb6BIKocZD+Z7OT1DZ85Bl
GYOIwM+cl8kKUlIhcB5mEGqyrA22tYpN6skF4zOtLns6g0HsHromPaiHDl+UhNwCy/3djwV7atCa
Oks5+oDS7cQ71/68tSIUPs9LFUuRn2mjvzQCpxX84Gasxp0c77LYlTR6BA3ux7O5wIxx9vb2fgTQ
LILhzdBdacbrDQ4+IO19/75m+1GF2r3hI+FUI5rFANR6bsCuPw1GPpbsTg4AhUK07H3pdlNLqAWf
OhLHVn9K2osvDnvME21j5QyhPWOd4NpTiVfFPwnzgg157e4W53/+ROnBcKtl0z64G6P5DSciWlzb
4mTfgubcqRkETBKFUmbIL8ZC7dGEtL0pus3+0n0CNH/utKYUqnfi94mTjVRw7x1KJn3OpGAz7NYx
yeY/5pass9sMYW9LY4YPm8Mur+ibY6voOaEfA5U0aKoJIZMRdOZ6YiYLQgjqMJ5QZRPmzPQMx145
vlDqD6TuMMSAMrBbyQ7200+iPRx3tpxjQENsgZWjmOoUnnj59xdsFyLZsPy5Q5t5VHk2x+bvdgif
JU0VbV1zJJivMUWSVj34/7QZ4Xu2Hv6zarTWICGLsu+4a4rIqpz4AVWG+1B3bYqeYbLM57T6izbF
ZbWVI3DdUmWR3zO+XbHAqcaIpaUSEaH7evOINEHm/4MPa5K2IYnO0g7JUyPUK99naC8T8vXws/xA
9iGTLD+TxxT30e9cQIGZClyfFQnBEjZc/EPuI4zMVMc4LGy6E3W/P4ftdpylxgN+ko1YQ+YB8WvT
ZevciOP3VwypcvgnOZZQCjDzh2ibN7yupb0d8vUHxDqgBDk4UrgcQd9yk6vMPL8UkrYm/yelhyPe
65BFaoSAOdE+umV2Wbp1K6AIArMld/AyumYCD1T/ahhHOs+gxoDkq6vie5xVwun35Hx5RXve+EJQ
0g8XTcc8OVsBRNZx7SnC3YSRa3uj8LbX415f7i4b5hHxfyKeFAyAxWWO5app++b0m7wq3S/UtLqO
N0J/FEXiRMCGObWUceglURMM3m5mXuozxqHiAPUCtX2N3n8O51Td/p5OMhs7EfBDveFWdQLYLBTI
v3OMlkc9yAcnb46zkGxvnOjk9Lb3yYpBVHOkQrmvW+W9V7ovl/B6429vsPe6N+au5bj3znm4jdW2
1kg8TSa6VezUK1+x9uO963LuX+hs2tzL6UQRY3M2X4rlyYhfky44FIoIZbXLv7aS06faFk5TYiKT
wh3W6Bi4M0rKQ60lCnCgOxE/gg3vVzLbR9fAtREsS/gZhtTzPmgPL6d3GhL5kRkgpp2unFMooLaC
kmWX5zQnFrj8RdNjiNVjlEqe74JfL3Nb6ioFBCsb4n6ToW+x/+qWQYRxGELAPQ+CO4fx7ZYwD8c9
kwrYDJ8qgd/TNg8jaTCer5KC+wnabHpbdBlMtZNkq9PSrhnbVSQdNc4kM6wJEy0n2qC1LFERvx9i
EPeA6vwMQd31LXeKh/Rp4CegNfKDyYPlX+PEpo7t3N9AOsWhUbb4D9MC5W8CV46N3felV9T2Mya4
gICfw7vDR5Z6JjNIVbv6fHE29MX56wDrLPxJrE0tqB6T6aIXx6P3VCca11sK1rJy3VEko6jKd/Gy
8CWyoHZz5drl6m2XLrY+bpy8M5PjPZy9Hcq0Q0bUN2TKdKhRxEQqE4ZrQjBL3ZsonYuhxoQpcUPO
TvLHwzFHGLzHNEGIXhxksjEkCLsRDRhhlUktG1FzNEGrDek2rPTAZrtiBdJDPZ/Sk/nUTMvgfS5D
OiEd9gGMdcIlV0zF7cIrM1aGy/QQUBtge/ik+PwUJvKpv0Te6fLj5GqlR2jRvNXYk2FjI0TuLJGo
6+9cIXsdXPC8hTOL4Aji91icGt7X4Rp62yC14v6EyNc5DuUmxHvzwe90XWd4u0ffp27r0yvxorlU
Ow16MkWJOF1doROoMdgEYeo1wh6vvr8Ms/yJVT9avCrjr6aqS6HVclo0zaM8Sk36E7zl3TDAXge8
hOuWmfR5FdV1AWf4p7aBP+cFuVp5YoJTfFsb90x8TnjqzzOPuhYo9elJPavF6mUKcihwYJouAS0G
DNVlJsreRIX9cq5i5SlwJSJdFKZQYQMoEdiD7BHSPxEtGxaKV9RYjSjKYW4IQZ6E2jt8Mh0KYfDK
sGFB/ANO5swUaSJqtPjekMASn3wAV2tS7BH8UrsIQibXkBqZgA+PsJP8sjhd6LLVMMOHwZRGOmK2
KDM/7aGKY23KGF+gkqJkWjgJQoybJcvqvbJzxBLuSERoUpzyoa8IcdomZ3fXQw4i1uixTcLeudsn
QzGxdMRa1xcmYeFmGlXHFJp6GvLRRslWGedrJRR+yPWY3EPlJQJrFcLizzF8m23tR5oeq2dYOjHs
7080BLwjZ9A2ZtA9yGXIU1gHB5UBLmftJm+Q0fPUoFZhzV6aSLNBiwa6hC6ErGLNzoN3hOCTNHAG
kKLS8X69IAUUReBhQGqpDnY7yeKoHpp8s5ZYOWVnB7ZLe06VR5M4iEq+UfP/0ASgicOMGdLnIzUm
Xn/giiW9SOvgiV4DpjtcnvIDffdsIAV3N2OulAK9+qETdoNVEE1ffkKS9F8qKdAd4ZnrYXsSkICK
ntaZJdzTFG8Bb6t0tphk/r6IDJlc+0YxMnJSAAc/mU+bgd86Mx1WIBUqwyzudKUHZNvU5MsFwfZU
AKKnWMlX8pW3uXvXMU5B4z1OcvQ7hRHbSDW/g8zBuX6JMFGVK3vbJ6uzEj7ZUH8RRyJHNATLLwE4
k1HkaO03qwcope96aXFBhcO+SrYz9O4mhGHtLP6CU4DSLrt8WvZPbNJTXPEF6IiNbyZ5Ok2NzsSH
i1jO0x+qCYQ2qErbGw4/17+K2IM76wvG/+51wUc3vvhXlEf+1tfXAZfTSI9jDZGzfMpm15GrlEvY
4axNdrHyziC+W4rJoMpbCFXWnsv1iCHeYNy5CDqmXE0TBLHcDLmYvuyn+10EyZl0noqQ30Ilc4nN
+vO+TXoJtUcnbMqIWJ7HHx8L4JCMlX1Dsm66b3ewG0qORNXPd5fnf65a259qy1XVYAwDyZWTrLLJ
QOYKFZT2MkJnEPaGS0zsynXVYfpl4bTP5T62PAILun8vDFpbx9g7g3952ijv3T7hauNLTxYTE8um
gjBzTwVO4PyTrU6KKIFgFZ51Dda7Zj9QiVFcSTeujhcAPstuNHrHc7lmWcInISrfuev2QR7vzylI
leJ/IOiXx/tZIkz/x2LpP86NVMbXJYE7iOFcgUGVlDPnDN1Dr8Ms+QMO/uX2XSsmnO6AqaJJXXPq
VuM5H9LVTh2yXDUxj7qLn6YTzKl5SqrlbejqXJ+SM8tjjjZL8Jy1cGLIDEfnKaa3B0NYlTCOGq1M
a3b0nglDekHgjaL4kfNYiDQwVFExymEHGXV4NhYP/AHS/zxvacEQCUXpATHMhHU8bAIzXPXIKphq
g6Ik6B+sF8kSeObc4YfcYEzp56XHIjSG8pf9+WSPl2wLFlPalv+/zBL5CHBihwUilISnPkHXLCfu
/mTwAviV2wtyfCqjZi597S8Uu8DOgc34LsH0sfJDzPNxoytQ3kvkTYlvQrwjrkKi581NbGweBEX3
+47ZIofauwHTARDAnjsNh0+GExI82WiQGjdOUA4A7wCeuqTdVuBGxX4I8y+CGr0YSwnAepZL1nOd
hn45+BM+nm1KZYfBeH/tRDll02Uij6ulizIHZp52mMwtE502yltwUDpBuy4OFrjPxwZaAERcDl4b
ZdsakuXV09Zit+4AQ6EarxJyB054JIdMtPlGXJ3knkC9ytbXDBVR6E8Tk7nTmRUbA8bnjOi1Yf2G
fL+Z82fxUN9tE1eYT0m9FJdGHaR1HCJP4NzUEDPY7dzcSWi70u2lBoW7AcwgFUx8HWFVLEiawBPG
MP1kuv6vbWB7w0F5/3nKQzqN3IfWB961hH93kgbV8KBSB6lORH6TBjyucpE/fYaynkEU7KSndtpT
amy9Uq7vQf1jkTiwosro3GBMZMobN/qAF6DzJr65oxBkREDc1dDkzEi8dc+NHqwch5ACUZaJnzXb
FJAZeCvHgCulxzZ+8XX5z9bjFIgmvWvmTu/QrRXwUCUfZl5gG7rMWYRwsh7DGyQ/d3aQh7QSLSGO
7v+Y13zQ5MXv658RTeVYEoLNlWglg6ERdj/isg8DCxv7LvPvSYeLFg8LqUULESEF0quPaDOs7XqY
QnOkcZ+pdjmgo5/Wka+qx2LrzCdQO2hUpH7CKuIz7DJKiENKUn4qfcBK5nl7qGDeymAHDyuANwL7
mtaKjRgybeIkl+o/x1UdS1YWV8F3iHsZF14xnUIPMQbWnzn879cU3atN3JnzKhTvh2+ErL0JinLA
rtMS3jz9GBWLVEMCpvNmWe/0rfRoY1Xsu7f8s3NEm+A5wsmG54rqqKLw8C2Vk2L5MmTjAbAJ7OXC
5/yeq5uIov2bx2Pj6oIUJLr8hEfFP0LzQcXgQtzPpeeYtEsibo09kCSuuGQOglhte+JNcCt2xutM
8SkVwj0AAE4Pn5rEs0gF9YlXcqMkfmxHg1rnsKHBy/xLvMNIJp3jWoxbUqk/n1yfhA72K5yJj9kC
JBg0oKd/RT1jfjA+Li24Stg4eSmFrPg+uabIuU9q3ss4CydP0DB+Q3TYqw/2so2x55w6Ivs/B2/E
fzh0mFjyMypA66aNwG+51MyMBZUC359QgU9VaPUDO3RJH1R7lxg2WxsRNp2HU5Lcus5rH7O+14PD
1jowr1DlkSQavZ8KB/m3YeJdpZjMi+40C7hJTFqSaoRYbx8zNvUSXgmh/aCZwt+expl+rQes5wRT
7t4BbkClIyT+qX1TXG+QFrk6kt9HTFrIh8I1af/YAn/wWUzrIfsg6IY3sa9rH8+tWdlkcwW/+n63
ThHrbO9lPa+ck/Pv5ADPjZodTzCV/0QPWCZGGqXG/W+AaTC1DyZIoropZ7ILZpko3p1a97UspLaa
PTRBm0SkL/37/lOFJukQkhyajc6DkZ6WBPvpLrBfJbtFXQZjnWmMjHWBJvl/61DgLymBejIq8KNr
6Nks2Ktw8jdZadm/zdULkC+hP6Twz6oAQj39gDKC0QKiyWaknZvntHFNlhIDzblq8FHzIP0IViBD
UYI541QD8bnA4JRzHMMTmhKD5Tg+cL2FBa0zziPuKLRX2X1hjYkful4aA3ykQkUZFNqBJJU4sP0R
Pw+2JVJnVru9Upr7tSGoiqCZhj8BmbKLaMkGI2hYcVLMu3JSPoJDNdfFuybzkv5yEgZ1eB0FJ2J4
BJ9z/1PAnUdhHRd7NNO65SE16cQPbGWjyV1mmsk8DOq9g6XXO0PksyuchCKQ71BZ0NyPFOU65A+w
ZgaE2JdVE6GWa5OsbA3xvB0Xh3RDjovQcnx8YwzAL69DNzredXgAB9VvHrhR49Xsur3GmY6K5n1W
CsOQPPs0lkY7q8bORbGeq88qbrSxANgu8KZwe5dp5G9clCoSnk07js4I8W87msCNcRlMBw7duTlP
xi7IJqwBZ7kEB1wf06X5HOgrSYCaDL7knT/a72LPrUCOZoL4EBLPeud5XGARwiwikLM8Q85YG2Ye
U++hj6YnQetXXmIdk8thE9ljewva8PXzqMxyBqZ0ShbFZY7PVgYKzMw5VsFc01QyP8+4Uh9xb2zQ
Ljzu2mwVk5oHu4S8gZZT0Fo+3jwn4k7vFKSVDeHTbPBTKtFZcQ31dkiD2oa35/6jAxnfLhI13yVI
vFbHbOqX1gckxY0ilWHdM786eJDNz6++KoWsnmFBc2+NJaW7RfqGiLhB9sb8JzwmQszVeeK03ijd
dMgSB1FpaFYDqYK53DitSCvp10b1z6/oz0jNEObUf3Zq+Ka2+nPpKRKlHRvvxRleAA9t1EnzXuqm
HnzScdT4QULJ09uOQCxMhZv7rHVrVenpT0/WjEd2tEsK2K8MKK8ZgvqbYzPBXZIngaAo8D/bXObK
wKcOIR4ZKoltSHOa+FAXrJFkRAZCfAXWhhR5SjvesoRcWjdH/e5VLE5lm2z8V5XOARoLwgeUq8Gx
hLwkmPLITNc59x7BIu2gOWQ8eTIlO9gBQ6C/SngSmr8ONV23orjRNUeZgs+biMUBrkTexCYaplGr
zeCYlkswfBdTFeJqziYxNsVgTf92Y/CAqZQDmMnc/zkqxuu1m6XRNK8VZgqti1ozRw4NsqVU6KVO
rbA0gKl4qHrDTyD/t6gK86bI6Ij1//+MNtjk+fjyXaHSKqamFDiqXU4ZIRpfMjqmjnWFKUkV/9I+
QejQMoSqG5zNaVC1z3gqi79CGn3RTr5dE7wlUR3cFqR2gSNcj6rS+3qJ6V6bIeoNxrNda4tiUawT
uH/d05huL7bBvVPAH7hCpzcszDsjKFvF0v40hkVzgXgWyu6aLpZw9ae6RDvXO4BzXzBjNVFawMkQ
d/Z95RnYKQIGaNtKAI5XVMj6lYLMNtlXmhAde+MFMHigKZyq3ZB3Xhj104zsi2z4KJuZrVSWwIzG
rA74g3wn2rfer+t1SxHUORz6b34PHqXG509qUlb2bT7BxTB6DZpRxJ7f4a8F/HOTCR3U7pq3kN1/
fo5xlUEtaBvfHTWOGitcE3hZlwCBCeYCjQpfeOWrPAJryAMXowQrA3rlWZoeUunY1MO8tPt/k7UT
ygCcxBe7ci8RWskVx+ivlDULJgoUWD/oMPxt6AVZ1zlEi3TV326VSKUj5Kfx9hO4Ihh1TwVW5Rv8
V4zJZzTxK51Y2EGn9+2PTm1716UL5ZwrKFQuopKogVwgzDG3/Ui39yL8xTjxqNeJ6XGTVDsQOka9
6f8GzySUftnGlT4TD9B/UaFCi1j60nnYnhvtjNFkqM0moNw0BGWBO9I4oKk7BRFhH76Vda5YtSNQ
PhFSbC9PBV8BzgqAKcLTett+dBwQeZ3UaEvlP3eyVi7Z2i4Egm4Quvo+kXBzkClzZh44LaZzesV8
3/rqPZrY0lLT+ESdZJMqN7pdptxVXnRNj4pnVYel9YG6H6qpz3qbEjaXHuze5UNwnXBau0pAJEGv
OQnkmrfleJkJZVlW0biV7+Z30Epa9517nDTnns8cBcv/KtrS94euttMwXfLlyYSkaG5Ddxn24wSN
W6biMPKKj5+re000rr0dpHgUBI+OU2KxvvIaX9WsjbWP51nfaV38XN8WRBVH8Dexnv19Y8EXqnx3
fYlpqIxRNmeGfJIjCfAvOrDnmoa40Jvrgn/5Dw0/XP9C/cGKcVRzYdQyP0pP2KsgoZrcUcFgRz4r
jsQKR0SQEV9uy0hDIEn3AZdr+r8hL63jDlmUYrNxKC+2BctEtTRSYiYdgdnijU7Q6HG0hlF7Co6n
cgm6ZZJG0czuUJkXE5Q/N+O449HrdPb1k4M733MqqUKDckmVKkosam8QQ2tq4g+e4U2VWcHfdiu+
jHpnXMMp7XxjLHpPnc9gb/n5SE/9CuJ0uNgqL+TIVFmYiwKGcHQjxI1zprxOLkoOkcjzqLLYsoSz
6V6GjGIJMP1y1n7gRDxP6ihFgx3HPgQos+X/jDoxr3kgXT4vt5Vv4akwkQ18zbi11APDOIXDSe9X
kBjFby1We5NHSMAbIZgvdxzR40v+SlAhAPKd2RkHeFrlkLVlf++KflqfGzZESdffoNyJQwQ4eLsc
ORC1uzJrksbz9Q/34SmQd6JQLhEUG7+3IjwXdnVAQVJMO3zoFBVdNwrLRlzKInNi30cSkTCM6Eb/
ttcOjBBhB8yuo25sgL1Vanyxo54GzMl21oh64pudnWl6NbGGhRBua4tk+qITJsg9nMTwUlZHzQCh
fIW2L2PAsGV8Ove5feErrqF7fua9wdt74KuyUU39yLPcdcbdtJBkwspc+fipqcw6OrYDW07k0F8B
eBdzB1KHE4pKMko1M3yFZmw7woJV4S0q9zmyTGx9smR3MJBlFfEZAZG9KQB3dRfDRQbBFzPHxSp1
VXlXBgEqzmdtnmQ+JWChXfu2oMvJ0N5GiHXUMnK2DPwv14eKddIUjLrPcrJOSIifNBMg6WXRoMNZ
TiK9Zj9LhJ4CoweqODblUusDcCV5JZwa2EsmaH6zbrALKxdZoqPB0zcycpqOhhLe6DnYwr6hkak8
NxwEeZJjFxCamqVvJnQCuIKWNgHd/zVIiL61hJE2yXJHOUYKAEVZBxUZjRW+/1K+nJMWY51NMg6n
0K8ZsmyvltCu//wouVOUbixF84b5SdG/LbdUPgt4AAI8QiEFG3Ze6jIrRzPub1w/U79m/w359sod
Py28Yot9DDE/Dvw79uDwCrKOJmXhwQekTFbqERWS/x/JMKm1uJN4kYyHeUQnEpRxhRh8qlVVw799
bN8FanrnkKqB+7N1JfGTU+NxVaJ90xWofus4trvoCEtR1S+G0lhiPxyFmQYfZGFjv1/EEmEsKrP1
KBcc/E2GziXA5XLI4LS+1Rrlr6bOsU2RAfWmLv89KmFN2jR4sLfEljAuyrTf/tmtRrx8Kv7VR5NH
Vw4q2iJixfD76DbKp05vLzoNNVgZONNjYAHNUBvwZvkw3i9x81Lj9ApawTEP+44j20Ak5F9uKu/n
HsnEW6spL9YyUxJRq0q1dld0fN5fdiwHAzq9U5Je1U2EaNXIzmVya+NrTgfgWElFoVKXHSHuxnV/
8wv8UnnwN9gg6IyPzv1Zoff3h3uk0ECL4xnl8vaJJMXOj9HuQa3QD30han5tv//IdvtkW3uRnjoh
NKBja6dfnshlOwkJe3XL/kJoh22czWsMfSUmZigo3bIKpc1Cr23/XqxkF8B8EcSh+pb5l2jyepj1
39ba9U0zKqfUtumGcMg0PaYrsLKINuIhU85fd7FbwYeAuNh9/osVzflpGyfO2kG+eo03lw+xZlSA
RHqT1FOgRig2iDsTYXJhOukrfj4EJNtNQLcHlHskNJ6DEKOSsx5kZ0o8s9FrdsxeBjHFFPnyhAp2
b+I+MlbGBIdIIxa/PU4Gd1SkVhhaxXKlBvRFvaD5Bpm2ML09Z7+fln0FNC+5fQp/mlAEkizs56vA
0MVlV5M5Qzrf1LKIcrZ58+OFZfOLiFdfffU0ggZm97h89dZ+msvYC7chuz8zjKIUWKaTo5MXSTxg
TI5bJ4hrU/j6U1dKk43OlkjuVYYxFiymCRiJ05bJuveX7PhQYj42LNGDY1Y+KqnDE7Tse0YHP+Hv
kJQKeEcTOkipaFs1D81VPz+UGxlWFvvKF0+ha88jrpXsWrhLz8QAxzIbBOxFPddtnQmdcrLezPA8
HvfKfxufF9YIExiqDn4I8G47qt8o72JqRqi3Ybm3FUuvFcsOSdyTpaZFUdPm9Wft0e3YROsBND4N
tlfGGwycGWdZv3AyvxdNUL5McOVv1nUCbV5LwC9tkv8PBilH0C1QT6xmayjqvBvjq8sCZPczkmEg
4fhbpuORBtdRASDJc+XhdxtdSbbWeNMRIShHuotkD6MBs5fsqxxBWgrKwPsqhhdwSh3N2+p7LiO0
4LSn8wePEZi6UJKIDisZ9ttQTgbJk9WPLZ/Tp6cf4Jq6UZ8T4KsQjfy2ncTjVzmbtTwxPVCv5TUB
hi6z4THYl5WKGEFCCOcdgFbgP6WeFWu0SM4/PB6cYp7FCZqCTPPWeG6o/GSt3gId5yHNtU68n3rD
aNrgduiAY8QNkiI4mq6fzv5GDHYfTHJB52ulYQQvL+c8WbtH6ugIV/IRKeutrYD45VSMvSeDFMod
50PCqHSKtCxR4JSdEO2dvCtpq3b5KwvbM/KYoF5nQWNdFjh7cm9BAFNJYMdKe/4Ys/AO6Vfd0LeZ
RvN6Qj9F3GTBgehGO+hhbSkV7dTSPFocxdF70uQm3KI00gL3b5DCSLu+h+LA5mE44l+Jf72qO7ur
lbBXB0vRES0zi9wElcP8MVCHCCy4KkF5QL4k2f4bKmadmjvo9VuM+PDL+58jwFdeJ2JoqNjm4ZK6
qGVxFISa9S+JA7JfHRS7CeHvutFhduBpYq0X9D6Qsw0e295o4Cj1fOFtcthCsvQDu4SpWMKjx5a5
FHzCJro0FM+8xZ+yLa+sD2ot60HZqNxGmZDNGla44NI57JdiKVm1K055p712YPXwDI/P3BdHaybh
BrYlI3oSxxt9irPYduRvaAQz0rWZcMLc0UfI7wZla8h5DyTPsT9qgibmmKoxKb7uOYImAKf2WBlt
hxolFQI0higyh6gfK07OzCLOMA4Azzg3+RJg7J55GSSEVVGOuBnwRZz1BBIuKuKIeyS7bt8vUN4T
rh92afb/qPhXnfzqOcUvJoWMygYutCCwY2xQWfBp9sHbHw1uRdDfMQrwqENymbSDeqDx7txzjHmf
v7267Z1B6ELt+zGvKo/ydt3VjTCeB3Emi2Wlb62c8Rd7HOssM9WfcUr56AdmiS+3hxOeFSi6Yebn
lNEWsoymO95qWO/8hX7i0HdhrC+k4P+hlWobn7l79YOXwODqKzrEpUF5N3cpVMhy9y0DKXAcYE3O
VG/QuSq9WDxdqyCNTpMuxWVbArOBMtxSo+tyR8M0MZNJWqtdPo90jDlNDUqQPj+ieGaF/p33ydBr
ZNQMu0tUB16d2xchpA6iS/DjJB9V6h531rM/7c3bqEOGMUXDw7Uv+Z5yYT6xWW70AhmhFcAuzGtP
OMrBM7YjFH6aUpzY5+bbik4pLmeGzyVpq+EFX9BuXBjxH5S+MF3hM5t8xcrn7ADf3nK6UPp9fpbr
6dFvcPqd8c/DY0Voi5aJohAsgox3IGJz1Xv+7uzroEEFFHsoABL37luCrMl3MhewU/Exm/GfPOQU
viyE5e+rPBRjZdEWEHbHQ0YaXUJaGBZeUf9kkqGlXNNfHsjlSJSwd1mrl2f7rkZRwEJiC+QHD1qq
t5B3QJIdIhyWjPMc+/FgDbiAunnEV1ENMt1GPSU2GngzmMc5O9UXDC9Fo/sx7R/HzYslIYz/14IR
6zbcWFYtFoe2+dJQ5iBOUxNTKHoR0C36OD3q+OrVGC2TbfA0OmC204Lt5/0dmz6T5uy4uxKCEPb1
uFDivkE4JQfQ0pqpUtVsdC9rIFO2Ou24JcYAIF/Nh8gN2VEf5jA//OAc8L1oMc70jM2GzQcb6fK3
HU1hPFaUCgIA5PdLHFkG937yDQgMcYVrk3w8Iv41i4tpp51w4wl5BwJ8kHEC33jft3hmZSfFYM5I
+KHYJhCe05/Ld5z1Dh40flxzM7UF8uc6C4qRTPosV8XWjlZRZhvhQXpW2EbYv2kuEBsHfkSkhmVn
xRw7XKSA1LJxffCCTzaHZrkb5A3NKBpX2bl+0+QrLy7I83plGMYQrh1vhRncPACYntG8nTIGb7/I
2pviysndRs6INUPI7k5O2LGbTdbU/es9lh03zez/95XiCaaS0crqgEAs8djCz4NgnGvODk+g1weB
QwJ4NO262qt8y2CplLUhQ8uUgi5//ZBDwXzaOEuWbrXTTLsQCON5Z42YEZstPWLw3j1AWRnUKXT2
5x0U3ofB8lMbYFiP08/Bh+10jwHmhOAs+F0sg4ilNpbb7HnEkUIiM68isAMjmgte0YvWnLgGsJx3
tn3h92lAlxxCcqFBZB56Br1bpn7s02zeJ/3VRncxVC27DjLwxx4eWgUxsLp0gVrFEKqoNkIAW3hd
JbUvMOKGrhW7Jpx/5DV8hDG9V57jbswpcMIrZ74Dd75SlKRcHY41JBmSxCrUxnK5WhPXBKVFXvi2
V/OCuPRT04HIq/P6fi6DypIMCF4/jDMBAcsawBeLVwBwyVKvogMNS2jOQ3j47K1PMm/mfXNoLkph
FEuiWrh8Nl3EBpXK6AZLEoHyUFYshqvbqlI2tGvGyuxlcehQ3heU4Ryq0jOKIj7IwPsXb53vSYe5
7tQphHQbhti8qhxU12jWYajvwcLNjGNafu8I7kaYp40E+OuVJY8TiQFaI30LAtfhsMHjKntQlKg+
sfsGifxNDaV+ArN1NW+Ypm4tHf/wo1vrZh7ChyUmS9MD6V2gmxsl2qXM3p5EgR2ieULAxGnqkXlH
BprG8oINLKk4x/BbamZIaBQBoUY7aW/GhGa/Y2RhYgJY/dhPWUQk6/LAW0bZtqZ4G9f5ftgXDjsX
NOXJOjsg4U9S5Jm9evZPYIrc2sDA1pHHvePhcX5pnNkOIOtIX1yJCJMWO/EowfM3nlzqNzRLzZLe
aeBDPxQJmCbRRy1YC6RJoZkGObyfDpmj+GU4k95HyaZk1TWGuvr4twkHlZkb5AedbbRqHVUTAmSF
New23YmLGdPZPugY5AHgeqJjz1W2aVLxHfTIcfgwQUNbXvOeLqvZqIfgbWuFTWTzVHItRCPBEazy
9zrs/wDPZpztCrfp48dOXdVVYb/e15njl1dRE4R/UYmG5VkfGthJA1wiUVYUoxFCRj7ZPb/Gi8WD
I1CR4Pkz49H1HoP4lWxDpBHI15BHrs/6kAxU4yza2v+QvZvpkXasrZR8W1/J/RrYT1tSMD9Iaq/y
H0VuZa4we7pXdLEEbu/ZJX2kqGodun+nh1RbG5S3Bk0V5voXn8ZbErsSopZcBcK62Nq0YcG/AIKU
2V/by18HFFubBkthMS5KwtqThoEut3t795PYewy7Ln7ffKVWvbKSPMcLEAllNKPQVk6oRKhu8meQ
RPmQm0we7NvvjeDASeA/zvH88RRd4QJLYXejeeAn21hNQ8djLlgfDWcdmLh9BYshk3Yy8yqT0ARO
M/7mUiRulnSSqXGWmc56zxMHq3GMTr1oC5aEu/x1ZajtVkzpMRj2suDsffc7/Fj5oyPdt9lzwRaA
UUC16YlZrJDd17GNocc9J4F4M/0TXck/BpViit01rpCIUzAm5ViFtd+YOOkuGdw+cgU5VuuKV6UF
j9yjVZ9t7WhkVolojYB2XW+h/x+RFpObFSWRaKrCROIT9cdTDQ3eJn33AsCY6xwZVKC4u+Zh8/8A
nblo+s50rkBOEF6x7JWg2FPjZSkWe1t8Hq6d72RI0bCldDVAbHmDR7339xR/zVWd7NG6ehO9TYtT
2jrU+UJF6FJotcjVSs/+n6NcJRi4FUGitt3Z1Buh3CzjPuHiiadELNNmDVtw7Zl3oTUdy5fAFXH+
Q1XAz5upN08h9FK7hnvHVTt81knGWR9xpO9Mt6vTg09gtCG+aoYCFpa81+NNk7Qu2LzO0jjiC1h9
5W0ASRQid37fObV0KSkm/5puGIjzgyE24KTWBFKuXXEdvWVLUhaHRk+InZ9q4n2aTYSkc+5Vhe6p
rbC+SejDs505xvJmElrEIK3Hfp7OWk7GoBhv/LecaSM+ejfTkC/MuQWivhwr1yLhdh3VnH1psnUv
fdshB84uz9yw54ZCiTS1Ubp4iEZH9chuLJZGD1VVbe8HfWEpsR/BI71T69Oh5KA1mZlqpRvYThNT
A8skZJKJpHxv/zKvr0/nZa52hPG90AOtYxR92P7iVVybKYjJojbGvVN12XRwU5rT1Hq/xMDCqC4+
FPBJiqbE5QDt2mN2kbWEgQcevB9M0WCBNppb/kYr/KjqeMrrAQtFbLed38uGZvq7KrMM5BZxziCf
tx4tHnyFQGeAqGZMY3OAQYZEZNzkB8cynUqx6AYIpe3pM2M3vtHiG1mJG0stsNcsRjxugyiQk9Hp
+EPJKYJQXKbwc5IGtDVkRw33izWf5ftVdc/kM6/P+IHxIfMof4Ejit9PtD2k61lPUuWwOHRs248d
sH2X8F0D4TD9elWtPDpmErMJSFRJIMpIw+T2xYFG9IqvS0h2PDTbIxs7PvxomO11u3DisD7FCYx0
5Yedbc/XmedpYLJjjg4omAK3ON35YihVq/h68Gs01vv2NJOaD2sCK2EKssv16Ellwd5uQJ05CSpS
j8fJUtr/fMuBBpV6r3hgYfKUYOxjW0aaVxVsyVCm9vcSGiaZfYfu8vd6nNXwSpUbscWyRlRw7JDN
bJaJTS4ntTUss0lzq3GmMScwj8nU5hFDpobd2kECaAEdGNPsHm19cMXVBEG5NHRH3CWx3Qtlk3St
0zlqGK7lDj21QwXgxNE5/UslTy1VstoPJXtSCsvfZKizPvdYx17hPpLC1sKG+Eu3687EJAmCypnM
aOW37rrbSe2ESK2COcfrYZDaG8bKWOIww6TZvuqUBVQd98oM88xlArlqaU8hZ2N5GhEpm1sewoJL
UFmS54CKHHZdqHlXBPIpQjsqZj3eHdA+TcXYPi2Vzb3KqXiuYh4CuPO2ZVLEbAMcoaQSMsGTLHb/
MqlBsmkcW9son1aaBgdkBzcClPABRJoTA6R9oNvdRLkfi1CpkEKeoKwWxlge4T9zvGxU/BeCjn5J
Hzu2cUeYl+JxYaTk0bVHJr6XoIgvRZcvxMzPXC7EQcVIRFFgL+Q+zawktVzBNCAtU1ZJC1iLs+7S
CsZy/7OJRmPMqf6Bva8TYgjZO2tZvxAX46HG3VwxJolT62PMg1aXj1g3B4zzv9X5k7YIT0BFgPRs
1H/c7K2BSqOqDqzrdhIUm44lYQQbto5+RGrXBM/JskJeqf0CqMYd2GLYPrznzpXR5Rh79bnM3VAX
UjCy3IOAq3YQ4YqYGCVu0lNNXyFT8hYq++pzkiC37hoiQqsg+BEJssTZDWmenzk94zPe/hJcOHP4
fJWdANv994i+8nelGZlKfskddEsmf70S9+LHlE2lOTVb8uPip0BUOxnQpzGgDMUL+NECO7DsnJCf
3f9iu6JvZRl12nOvG7I0vp24VE1RyiGl5dQI6vz808tIkHurX0eimysKsOvXRKUI+ZpX4UnwC792
OP6CoZTyPAsEchVkCa8MS13Y12MNYhjuiRliDbcTRSjOcllUPQUve09pbHoaLPcTYVpqz8hERt3e
/ffJd7n5Q2lOBiSWvKdAGkbbcW5F7zuoj2uIZYIP7AW0ibAmjcRAUGObI0GGDVzo36xy0mWFX7WV
7kYh5A3TonWYuv1r1dmQSFbCVr3XzL5iXKWTh0LhtzWSkcmTMr2swx6h+lI77Xcfq3pBdRcAtiPn
LNUeaHu+NfRdlbJGAuyhy/3L1oOD6YTNessouvCbXfxr4KmwwPOs22GaGsV+LgpV5jgJIrq+Lsnl
TNe1vtGm2VfHC6iP/nwKT3sr+ZqrR7G/y97E/HzzKT48bY/lgXrAYAsD+LLmNA9+zgWAx3+2OE4k
18qxB8hTNlauNzEDZrTNPWrEdkoMZnN6115954Dy+5qPA02SANnrzTynzAtAVL1esaZ2YuM5GzEK
7mgL/tf2B8aJM5OmVXIodSN28l1GOdT31Qu+OEmrPzeiJKidzlqXrYvpA7YksywNi9pjQeStYk/R
TZD37+V8kgN0djacJyjZH1/CpcZ+aj55SFbTdtmhY/KwiYpDNrvyBHVnPMxRtPVnJyDEn3Y9x8dA
xkcXD8AHmkwBipGmV+ZpwdsI6vwDY/heDBcv2EEcuaIgBREf057z7OLEf1pfD42s6Xhr/PaHbAwC
5PTtAFPRs33gBgIz8GTrlxak2g3cMlHtTJaaER9WMrDDSLyoaBeUS2Wfd0QtHvXWQzLwxA9Fj2HN
+SexGP2qgia6m4jh1y2RFcDUZ64lf2WbWDO3GGQ3QIDQ6IRNAVGxdoiroBYXJV3/oaLWJxMapSIr
CHpKs5x18N0qb8z0es0yn9Vm5OQYN0oJIER7P++HBwoTqaFjqDDYVSZxqdaQt0cF1U2Y/e/YJcul
beBrv+1QCYX4rVs7hwbHk3ckhVqoUzIzaPIcXq8XnqKd+TZDyPjJ0DXiUaoPVmaciwroCCvwEuJq
iI4p42u6LDyJ0QJrYzGcWba6pwg88kwDbExO5Nv1ycR13j1gdt8xSoKIdi5VUUsFDlTNb7rLQjIJ
ry1wRm8zhL6cB+4v4VF6BhsqNc4wlsNxqpBhukhe+eFnezND007j7aYBtH/FzM9BU/f9dNL+xwIN
yb3FWYfV0jIZvMjqP+IBSY/5dbpYfXna35XnM90SDKz2dizLACfkvhP0ymDCmHSYr/+lId9bcB2T
fgQbd8RZdP+y3GMpb2rpxoGswqbHHdJI9ES9KE4o2I2M9leX4GEmhdyfcyiml/GOgIl0TeWwJq/e
xVQo2qFdfB5ImZRaYsfqsFzJJzjD6z+m78/kAM+UUXwrheHtpLyGseh3FFOnWZkHfOo/eM2lh2zh
I6WieJyx6n5XyLGOLOPfpIWR0oX6PLTWwrwQ05qQsxeoK8tiNbAhJQY7V7prCwbk/NwtkJmAbIiy
OXqTuecA8nxow/K7lPhD5ZaxZPdYXZ1ZcBdIlSsq0pi+5PDpaGf4tslPaRGZIWMsSe+gWA6ewdMY
taU6wfFRFWGzXgS029ptzgloT8BgCfyrfQGtXTr9ZZfwY0LvnvBcdBvafb2Okn4N1FHxi3vF+uku
eP2k8Tib5jnUUfQkoNCixWdl9O96EBfhvtsLXNK4NynSMbdvXYLXZCJjUmDbzJpVGeCR7ARBUdBS
pTgCZun6QSze8QO2En1y1xawBITn7hE/2MwqbiSqxIMzKBDPccOUzWYC8qdtOs2RHwlZeCYqz6kB
dTKhHlvU98+O6ySh4TQOL9Gt6X9N2AdG66tPU4pXzdchw2X9J64PuB6VKlNJryBd5gKKb7eFrBNW
exRSZJ9rUDaZB+Vygf5AETWJ9Xv90JSOUPsZDtuW8BzSkCj4NuvVXs6g/9KLmNtSSOX/zGdZa0xm
qMaWwdGk6UtJ3/ES4emptf3T0Sy1qgi+fCafB/MIyaj0fwizJRvLtT99Wz/8sjN86664bGQ7nXyL
wlY56K1wv+gEg3wRPphaYoRXN2BZo37aZ3FbgxnH5myUctExvk/c/w/za3peQK1ajWrf3tVzXjCO
px0tZhjIqNWAjsEDUIW97yjT34FzQOmdhkDZ+W73Nx26O3to+z4MvAJnk1KIHxdM+dyWNLrO4fwn
4LxxRPfF9lECR60uExIoxKve5dr1L2d4vbxDwl24/I+FRtVYAcsDWUcre1z3EyqZx+A7JBiH9yvH
nOl0GsfTDvAyRKfDU/By2wIdV7DR4Xyj/5PQkL8ESZlSbz6IvQA2vTFkbaAoDaGpIAn4sGEaYWVj
wlLU+OwdO9st/mcvelWxVkv+6n+iuU5VSL/8ttvk8KnYTFRcILnb1YJWpLQ5i8pPy4zm43kEJaao
dE+pl7Volx9WZQvMrKp76GaHdviqZwhud8siCtFJHHG55cNeaBRv9sEfJ6ZkKImd2R+jFp9HPRH2
jS15kBv9DhXGVHLbO4MDTLWeYigkXxzDjyjLH/xG/qx2Nll5b+Va/oPYMyedoNvoLMaDSohEouQp
0HpkbX0dXeEjKDuFLNlkF85uA+TrpSC4DC5tAMf4ZiNeMNCNk52Jc+mbvv0jPeXAkx14svrshDCK
AZR8FWdZx6tdHJF9y1PQQg9fD8+ep9bbXa8eVT4xsjdots/cfwZV4SdE1GXLN4nqHRBaYDxex5X2
wl/nUpF3ehwqpmgvr8/Gk63fPnlzkfRZsWgT88ek4WHCVqBweFnPfw+WZiQrh8KGRWDoLpAll8dQ
1XEl/HBNifAy3gvOPeOapsFQpAWBuldN1oLQWpFkaIwJ/Ovz4VNEfO/Gs+e77TslJxPgSIgEIyV6
GWzCiaLVWbBjzLmDKupOSHuvEE0bnU7LCjCQuR9W7nUX2jVmiI1WQprSn0FfnKn7iHa6/QEEtNwy
Xb/n1kNsOLAfD8i35gaIQKqQ+ZDWGQdLgCQhafCtf4+abMvrU1RsD2LTVUTm2GQEZSyYO26Mt3fH
IdF+BFlL4wOgLkLvCXIyYEvNRhG/H+Yo2kXjcgs9IUttpt+Icsu/SrfcQSLjxzltSLhH/q9DEopX
gFXRYmxHTcGKDjAJcQ12UddJLW58ljkJWWQQttinAlJ0SVmxQ6RRNKd+GeHK0Y3Tz8aGc4dO8iBl
UZboTJmz+GaKbT3ju/Et28mLdgKUAAK6KQCaFYMzIwrR8wfqGa+HWFXFW3qurtKapCcSrpLQtLmL
LtodqRhz5Ks+5Fd+PXk1kGegZG7uc9v1lhT1ndmu29fgtMvLHaC+jlEz/4cWonuSw41gj3qUzNt6
jIxSO0NiYxkjxnd86WF/o0kRMj+7CZVzcv/7RKej7XzkLQ2fKKeIANa4PJWsFOi5tTDxEUTL0LS3
biTbB0b5u7hqy2DmSqbT88M56TeS667Q5fklLRBt/75wWHxAaYMTbzmSwFpYNKWyyS/gjGM1JE9u
RbQL5+tppbIcwTXxL1QkAimAgob7f5dhGEayWYUicAIQ0UWDyt69LT11gL992QbSFEs+UXKe3mOG
sP/H0gWP06ZCE+B+jdwqandnLqBgPvoW/JqYaohvj/0rBmObSfIqn/CZ2HAXr7HrP4TLAXVPuhz9
m/wDH1EBgLrQQAzK6cERtdg6rXluPrw7jI/R9PA8D6ebPs49fHIIpko66Ddw/YrINJESoBN3+DfH
3VK0PfHueuB/zLxNZ30y8s3D5XHzQUvlyFSW4Fvb4PGxpJONUuNsF92bfdhCbgO6XxS7AfCKwylD
4ynWHc4hTxbrq8Agjtj3vvwgY3JOGeXi/BULsoe4rNhdP2mi7SsWbS9bc14bNDWYmDfGEoTRX1ZJ
3bHx+71INzf2aN/s1KSceqW2Wz4AZWRD40FJqsTSemU/kzdb7S+NNMi8jh6xhdjzWslI9EqkKiq/
fXxJJAs5/UTIrJaO5oW0KL1jiUmn/3+iy4nceRBjUd41husM7+stikJ9kjVRUvPEVPYtbcnPLkVt
Xm2eVXBZtKBZ9Jye3iuq0gkt0IJleUgMfw6k8ENhzBPeNF52SU2GXJ9wJl0gr0RZPd/dUCmbyNqP
Hw4YpcjT2fdl4YzbeFf+ODrChD53at0LgKrSZJryfJA0o51yx96e1aCNOggp2M3VKqefA8TITmty
i+95UI4d5Q78TkS1tMakw3ES6W0gQfCg37PmkJoITLW4B4zAJAfpOxBSzGwu46VtH/asr/mu60Kw
9gzp8C5Mq+HpyVZ1p8Z4SFccVx6cqYf5inAMt3f8vL1qA8jx0CITiGLc3A36qNeJRToxsTBAKCdU
hGIRVNw+wHygWa4gjBtfP1ZYbKdrlpNmgUufeaDoFN8iL1DOMl5p/H0VP+b0v3E+ZfJgN+uertvY
TqAlt6BCmOqg38P/LTR6AELSbt+m4kA32JOT+y7bZSVZ8A3iLmqYRnY+MocmyeNArkgxPRI5A/p/
oqzPlIiDXIJRoVFixSlfFtD2An1v9Zfb/8u5q9nCqCJZt/NvQVOFxMHKrptnws/lUqNBCPNRTMyU
vOKfTmanggQPahzh3QZgKBman9RA29LzXZiP46KvG37YTyxT8wzntcaMKmrtaMeYqeDVvKZcs36l
zn87QEXnep4HJbCsjZbSnQQz0ioPer4K5pvBkoBp/XARlMukEPCohNFdN4PSsYhAzclyisAe4kL9
JgszhLApm0MBjYO19dZbX0MvQWJ03UiNorZpAfNIjepEfjhFedZkANkkX2sGW7/9ulCF51UkYzFB
k6lArrZLfud8Tim9lvD+ewRa0RZpfxCuXvFj3zIlJFKMReLx9NjoS2wWy5mJufn/07lJoEaUHC5a
dNDO9VMzj2Mc3xLNyLRT/zW7K65ZrTol0dk9vA8FvFc5fC7TQ1Fc37zbfoJ19Gm6iVhg/AR10v9k
rKVNd4XGbtn9oAKJHLBidrpXsiLB407deyPd2KxiuLjq2po+svXDKKLpGfs+8Mv31qeBtau8/mfG
Qmuq6XXLOJgV2ymlxOx3zATAgRETYEZziGNxkTUs37SRoAPeTp9iig6IjoV8WCMsrafYlviwSY0J
y68JcCZRamiA3fh0VYJsvwXxfk5eq//ZRGSxm2aS1FPPAT210MuWvlx6DukJSj9duWg/nb5qo2CJ
QjvrADkvvHyzreL1FRVtt3+f2bP8GwzUGrzwTi/N7s18S2MhaiDMqnibj9NSr5Ow76f9uq2waa1m
/+7TIVrYQg1LtGKECcWxhyuvL711H3a1OkJukUFOuaq7E/vDBAdRGP2ake4Fgqcsvl6cDLCxg/kn
4p5mUfk3dDCgDKAVkIl1sos5tsjYPBP9XpSy1sp9/36Vg+M0YIda3A9wfUgIU0ZR9V4yjqQrUioD
dL3CR0/6SDsC9QHJWIjNhj/P3jjs4YrEHfpquDj0FZLmoouk+uFyHwhg9i+5bBl5KDSdyCt4til2
BAp3fjP7wEaMFZ28ug4MNYmUsE4v0Ms5xsFjcPWKZwq4dVpZKmkFTIC09JzPOAs8zssNI7M05V9B
J5Kko57qi52bVbJVJOav9RkoQ6OUplwlfDX9FBBmQ4cQsc54cpR5kOo3j/KR6KCetYZdEt4q2+uz
S/BGTZWh1voUZBdCJYDWcW58Z5KPVrDR5jPXjZhybB3ICloeTC7k1xHarux48Rz+HlNPf/lwdof2
CFWZtU2/yQr1bkXKgj7ffnEzPWKVT2wMKc2Dj01eUYktQ9e2FUf83x93i/RaAH1oN/dretQLTuR3
DApwzvxVb3C1D5MG//OnnKF4sBcKXzNwtoQf49LRGN1/qwpXnAO5GYKVRCWIgVmg/vMIw2RHZQpi
vlwc0bvhRpfhHLr8Gb80fmpRtxk5yWTrOtL8FwRlZP7udljWibKJqnl/XO+NI6bkc4Fm7HQSN41c
sI9S2aMETnaG0GX/1QZjuD+QddcnT085MUFqxsdOvNP3irWRTuhx0XMrq8UwGZstNplAwq/ddpvr
6CgueiMTW3VLJFo2uWyaRDoJn1rXrGD2u+3hiAGmMDOGEFysuvsg3mgEpwyxvWOuC5RPVaYUwydD
U5UUOckox7laWsbwSOk9GLnX9cmnpqLSS1wwBWfFEk5tGB/sn31ZoMQdIhIQNKzuALxGa3v54+ds
Xq4KBNPY1tvSrpuFea8bBLlTJFXeuHOSos29ItYS0eOSkbNjNNGL2FJkmGFG2PJjNLv3l4bAFg1u
xYviMAMHVWOLA0GKTG8F7Sm1bJ1wQBsyUBbbgGhXKHxzH9UHA2ydgeb98bDi5rUL08VmCl4VKk+r
0EKbTnSwPp7rKbla6EKs4jyqlH3kTBh8Z+Y+Ic16c89rrM5gb4av0GfSzaBY8gp6BzEayUSsxihY
Y8E+t3uJKS3ePMAV7XfOWcZ47gUlHE36vpKshgsrMc3dswfS8s7yV75HywZQVUBnu6K8tBKwR+B7
k9VNZKpEqzPWnDGmpts4TXLZe+AZ70TW0lwUFURwFh2phQ6dox7EnOiqvFEjV/IaRGyWpn74/V9A
kXmRlaOLOodoVFMAvvD0XH4g/AoKovPa644vEPNbmxU4h8Dmi1fRa1XlLqrPnWxDE+2u1xshlT7+
w3YTJpLtPYHtwxG3++aLs0Bb4IvOfa42iKY3xw+SVVqS8/G5wcpJCvm69kkcyypNdhz1IG9T6KTQ
aKFKFL3x+lC83PU+vxpC93IMIpqS3papnoIDvMWNlHHww3/XCZEq/zG6hqJNB5fPTrdZcv7Cdc9l
hvudNLoCYJbjj6trWLz6eov8vE1PwAmSSKIjmoEXPrNvLHCrVVqpWwruTx7PqlYu1vfXiJAVTIZa
tV+BP6/i2VgaXvh751T4zsnzr3+KXCdUWSZgvv8/KKlD62eEe1b8GdkP8ZeLOhQnJFF4gxQCr480
3p+r9s+svxEWxrhNpL6BwZd0fhvnNtEs36wAGnrj7PWjJ2LxuMfj0d/zRzX75p7R1EqsCK33DPHp
fBnysulM5uOX9kFGJnYOljuJDcHsvZY6uheXelEBFGkIBL2WkHsjKXjlrhjGyjt53EJIXVt8uViY
H/7dhV/5/qb0Gr/PYELigaIeZtL/CqVMoadZMzNh4RAS83wg3hWm3yKKZYcjZR+w9n6IbACl9AdL
owu8H2GQ9K26DcOM9TWWddrF/DsTJV1cPSimjJji7GgNagjj9+PGDlZq6xQcMZgEZbtQyOhlYFY/
EcSwZbGG4hNNECLK1k2XYoQixBw7mHhxuVKzFrIe0qlTUMvpl8JKDhvdxUe0mS3hStDsqLwCj3I+
quPrhekAurTUmNmfvKEvkZSJFc0z6QkUQICL/LriT2b2WFB/d9l55ehIjRhnuUtZiyriSBmX7J3T
DDl8o82gA9aTn8RSpbtCOL/S+p+TDcz1kMOHUIcX0wB8bS4okvX/oeSC4njyf+cJvFJwGU9gTUPK
CFXMAJrdTShc11xb1NKTj7/EzslW27gL5gMG8WDSL6dK8T2hveYk1ewrNaRfaJ5vEUgXpwvqQLS8
hSaMFysYdGy0EKN01BCDIWXt0tJo4Ku+5OkBn4PmMLWy2ywRfD18Npd8NbKFSIPxdS5zJrcvjetn
kszJmNqAC7Q3DcAOc0y9KDn24wu6zzXn1nMip+8Dh9d0s8nlFMY5qRJUsoTu4ecwvUEFbZp/9ehR
VBVNI5o8BdE8HqcNW3fu6+5ZMd53DAGXutAKrZhQRyTWDB37Pz1XEx2C/bEHKLpj3ueTblL6GGAL
Fpd0qodyW8eNmA2HmiPCUSfhf6BJJ8mqErBGJbPfm9S2TGka3ckXCvAcPHID186wJbmDmQo2kViA
Qk394q3qGeGLh6qLjr+GxKtB2uyjsjCAD7AqHKGQRqED5cqY/bvk7KJqPUEpjW1AAOaOT/lGAknD
YVEVzD19qe8yJLVNGJNn/G/2brQkn8C9ydZXWCf2VoK/QXz3J7urJjIotjwZ1HntaeDi6BFPhAKJ
cr5vOtK4hUNRdhg7Xg5RWj+ofFSiZQTfxzhEc/nZNzsaOk5rxc7lhtG8bp2WWWIBjIyKIat5x5/V
bl3SVXcRwTDwCpKk4KNpi+Es9pK5S819NFZhTXCZq/21or4q04iGp66oc15vUxoJGONdhGbGzOmm
AZGjVJo45dQi9E56OgVMWHbh0Efq3fNtVb29MTo2rCRRZuE+a3xG7u7wsPsdGD07O3JtIOlx6mEg
Wr2JzpNe7Y7ZJbBcE81LfHfWurWs5awyd7rN1id+yhb3a24sX4M/7REO+EfCAcwT8xv5SiZPhKtE
O8c/koP0nnUtbEhnjiN5B1UeobTMZ3Fh5/mTjKNnqTnx0sXK9lyX4E2BatgSNngWAQOtfz+5kFMJ
RoKdkgDKx9mziengZswCc/oLLaR1aFjc/EWcD8057WK/Bdk73P7y48pZ1aA9uStuGbXPeepuNnzA
MQ5SgL/yAnZuRdeozNLInLFeoI10yPBt9+aA+8QukPTpdR+xd9tJ7DzQVvOl+rdgtaGz9F7hXHoO
H6ezscC2a5ofHHKybDj84KqMwUF3cGias4uJng4eD4NgN1TVg2csaeR3q7KG7LRp+2befjlEDrOy
LoEAIXGaNxiIYu2zCyiX6IWVYPCs+vqCuZhsOeD4eYznGei+Ywgd4GEtGIbnSkJvRN5RrRvaB5fh
4lpTBMaxGJQpiw4nIFXzOtpLOU7GqwCwDdT8r/EKw6fU0AGorpCgpKD5QVByDHvHyNZ3eCSxNtUR
EfE/CBACnp1t0J8jUVJRYsuYd6Zx8ZM7pVUXlpUtq+Uj38d5yZHNCbH6PB2+wPG/yTCkhBFib8sJ
eA2udnk8g972radYnyDO/rLwW+bnydstAdAfnJf1l9abmc9oHVUh6gqWEQk9jfqRCbWOzFJ2exGd
fXKO04uMbcDe6mxzfQ03YG7nxO5A3fGufGwPF3yhI0ZRRhAm24lJK1TOp28EHgPay5QZ5R6nQLBS
tSip+KkRBX4ZTcnDQeHyR8LpsR0RqtOpUSSWIkOVYDou+JFv/ViWaMu2y0v/bj3gXRyXoJivbMb5
NfIUM4csPFCG2qbBjD5u6t5SuIElze+xIl8fpKucX4gW9owAb5gKtF61ns2+OhzeKEmJzEg6dRvZ
GVSW11i5Gn0M3GXNk7TH2ncf6wTBovvLFD5pDeiHUFYK7b/Z4sksCEU6kB5yia6CIkVr6F8BJShW
i08dIfXgOIVKD2Y1zmp/drEcWUbu5G25M435CFsYudxD3+PfJef70qbseyRuZVtikKPdnZndIZD1
lk5DiQjxpYq9KsGHW8J/4IA1DB4p8ArB/zxWkHsenPGPMOaEHg78DtGV/bIfxi4nwWJX5MA4UjyB
LoNwLx5xcvCDOW6chyq2H81HqA0zg9K7pVkr16EjvdT5Z+kMtiIFIGmCBMzs+N0AbnNYjs1T9d7R
sXEspcT7nv6KajBCem+56XTIXDjBSI358EtyLLGboDoyfsihUl/8sK/avfRArzW8T0lipRnkphR9
xjl7K6+rtb9ehTN++gvyA8gc7q19wu6KK4///0xXYuprOWEhheMTuR5gU1OD0bEvzg5Y6ElHcjKN
XVhqMf5P73d38Xe5j5HwSpWtRaGRFsXJSwuRLQB/2YvWFWUAwAeYRr1qnwUruPB5lv4fCtT3Pq6s
IecUXxnNB5WLAT6ZbqnvFnkqwaVaDNbptI67oyIMdPmtpfgr4i+8+3ltCb0M5RBVVYDKv58eSjyI
Z90zvwWxnPmhWq7DPQcN0O2qfn0yXRfjqQSadyJ5IKU8r3yqMAtNpznjLZFjHgXqGv83uQuoX5Om
k76EV9c3nEaBZwRdqUtomox2SnFrvD0d/1dWfTxVeoYYRRMgeOOu4zyCjFbdUKZVWqJ8fRc/qzaO
z/D7D2kYKAufoolB34aIooAGvN2iF/jLEzD9wHtuKKL9a0NVpmDJ0usfETYcq5be1QdFKOoMIi69
BdhglUj4XIAVJsWjGuIjimXdDyYCAo5eG/iEBr/usYYh8owtC2Avr5A28oTMAV7vlfNbD7oOTyy3
08oePtfGh7uTvgxYIUDdZVC2PEtjnQZwf4ihRfJKE+cyyeQajRgDj0gFbLWpF5xtxOIz4glFCrIS
3h++A0PfAXaJTHA7Rht54EQX4WyKzO/eyc4i9AuyCrR8gPnw1glOh7Lxd4xIHy9MbnHhe9H2eXNS
bIQnFN2Ydo7+49w/JZDmxZ2IhqDSbdKHeeqWs6bgp4i5uhWFDkHYwnvrpUcSWEa9pXGb1LEgGWyn
GsrKFq6i2VLWm2Jdhi0NH8QS5KEDegT31A71uHhtCRl6VmBFOVDKygY+Adhfm+Ns94swea/12kQH
/fOrbofEb7sgS490ygzv4IkUspQtXMukyWM8rFbDpxly2ZyOZ5lceKvgJQdcDIRtC3Ah1T+/HIpU
8cDwFH3Kg5H2J/NmRfgUg6dRBL6aL9Gav2aIbsKbZMeUtwys4x9+RRYAYW059EtgB34ECnkx+EZy
bPWHDFte4su4B9zWXtmd9kMUrKV9cANtfKZR8FmJj1xQRrThvzqFmQnF2xxsWPkOC/MfS7LoS9c7
8kzvPrsJeiElqa0YjnNKnGNW4oaH8VqbQ/ehfR05cgbUWFCSCA/Uyd5cNbIHIlRr4rqY72sRqiki
JpXUV5cliq00dX3KbJsxOtkuYTzk2CTYW3xRnd0mEZ7Ku++nw1SKP3AZxxD037Mi2cNaQZmWoen+
YkJeYuVk0T6mpOSogGdSBlEiDFlOEs/v6a7yN35kvDLXQN1GeSPMcbIrfX8PpCDY4N66PrwtA+tf
SsH08qJKiwHcNoggp21wJdn/5ZxWL7jO9uHySrcgKx5+ViTMjaNwFLUMkoGjCg/0b7X8Pm+YDMzO
pFOFlQwVN+efuE2hDo0Ekk+pcFXCI9czVIGzxV6vAQ14xYUKEzLJEZFzKsTaGjd9jUmpR/pvI7RR
i9iIgaLagK0THz09QGQbzLFfuOUTfz43rkr/oeZZOkhOcrGGMgf+TvNyYRpXMgYWNz0gmTomhWsi
E65oSWmRJpBBZoIN1Q7QF4cZl2wEz9eFlfBhgX6iCJkInrh0p5Ux9yrvng5FpqadW3xQ6hpTHzMs
NqfboETprVAMqx3VfD3l5hZAVOr6ZWNYJxzWkWq7A7uNz+XzZ4/M6B4ByxEuq+bnyiswnFWBwFpp
Gf6BVTBIBGfEeegUMXYkobgBu/lqfssxwruwG2eMjThC18yhqNR4+mcBsF2lNojOUUtUE8OEQQC8
pgZ8Tx0k+ggOPZfvmdcvnOrRlAhSTCiPYY7dl1WQxSnHilX9C3fjRyib7x0lp7+oPqO3Kqftv8lp
3Cpi6spA5/aR7m2kDKiPqMoVeJkR9yRefZGlV1qIJcAywuvB5qmuQAcHeN0hHJ5ezorgI5M7BeCm
GAWI9Kb4g6tWAY4M5VI/7/Cg0SdtqU6tvl8N//64X3pbcNlnzya3+QVxi49X9vOpsmmadO+86rrA
UzemjjcX4irqbD+TBpNg6tPmLsnlGIY/cnp74Kqp4xkkNmv+3ghnLGxZ0sOaoeoocBhixtZu3qYH
xIDGq8AvDHNre06CzSO73QIDyJTVOvB2VFwEmN82y+P816dJ7HrF3Ou0Bw6YCsQXzBW3QkLLUoIp
VVfJb1nR/T9nuos1EO/vAvp0Hz19/EFT/uwXvpWyNgPL7JuTfjaHVGBpi0ge14BU2GPEFEaiM5sd
Ffigwr+9Erhzofc2m0QJ6300okRrouPqbVoLZdWKKyiehzs1DtiK59mfR9eS7PnKgPZi+XdED+N9
YFBleJ0ZWnd2i86oyaqN5zPuZ2sY8BjK972i/yJd/vi9O8XF1iyVJe2CV4oKLyHx4I/cx6X4T4L9
+wpEJV98T/JLAokReoYYXW0mIB295qat47cdf8ZeifSbn9gnPL94cTNYqkx7CnXhJ0bRLcR4TFW0
dxO4nZs0B88txm+OB0H/tWChnBv+GuWspLTFjGMf99XaKLXK/172junCymASGctkFCwqqw8p2Av3
ERbvkoDVSU1JL6cTyaT8Z8akCVGcou4pToqSeb+fy8KbR/QCYQmcpDmggTt0RO0AhcpqEozyVUIh
ITi8+Ftjyvlm/yoVmpzjl4W2/96v+fNLysexopoiE5ZHb4oMoLe7rvMhqr3OR5OdhiAVF13l30O3
VrDMgxpSxHz8L+c2WoxSnePK9PJkWGynvlKX9VwCk5fRLQYrVMGmgFICWO6SSzYUkpmPKvqEbH9u
5ijAq/njTB3Y2OpaujrBRGU3oir1xUzTfXu6/0UxhtM8tAowCQBHAczKDWT0OENZ3x3sLJKEH8xo
V+64pzPD2L030HSbteW6SDfRNQXt9oWKI+T6lemUkdx7XfAC14DQu5Bx/fYY7ffU3Aisq24UXWAX
FS8YDTRPwmLXBidcDSOpNQScXiB7C0JHYVdfHL03jwS7gad8ZJnAQvSqrgSv0n9LtbjsQfXZR1Ku
aSWksYImaS7NlyIJjMp5iad+iKup2yKa8I6gxHMC670n9Be3HL0sbZjOFzKBj+mwt/33WRq6BRwU
vHlINC9RAgUNgeQydPgsiqskXsxiRw+uztO6waOYfR1qgkkG0M7vw5xePBaodg22FMpKO0SmokrX
eUjRhh2+Ho+bNHbKYCECKKZkU2zafLmhr8DbXdIPKVPEmw02QTe/AQC3PPU91P58TsAm4KNS7Y1y
KscEsJgfFTY0ekkpnik+IcSCcBEB2cq+LjpYkqQnrVR0NUv+HQAZWmmq0gtYVc2srFH1Jr96tsWI
NEB6kMnHh+yxTEhS1FzZ3W0twIFKm/HspeHrJi2oB0Ah0vL1CZGlBRXQyjx5XqQUMVI0pNblIlPB
uJXgei0JUfcDGd9KH57CZnpHPUdJxKm6L7Ltx7nfXYk9wQJHsBfSw7xfAwo/e/z0tMU3mw5Pyt/5
6gPMcMCWjB4jYGurczye+xx08NYtslPoTWp43ZzQAA+XLh5DBtZcPTV9ZPYHLm36zd6DNLaKcm4p
5LQJljBYA0ZndkHbwYmsHYAKg2mSF8orh4HYp8QMDz20xR8QMm7zAp9uSt68rK6PaZOIMuKf7kLF
mDIwqNNj5v4l8z6EZC5vXqi4NIGj6fVOjhKZp6vReu6ZXerJr4mmMkOvem18SzzBO9JwEWZ710x+
8JV0TOva3s6jcTRptmUIsSYzhYT5EI993rg3cGtuV/5b6RiAkiGfc5wDKRlqur7vT5IqjVwVMWFW
1STABt5PqM6IxyGHhAhsKznAaavQihVK2M/sOPMlmpASGZR1pOn8mNviOuc6PPthXTQeqY5MZvMu
w/D552aXaU+wCsTFqlSyXn5ybkb1LUWU6FB/9BJZjVvPTiBepiQcJc7U/rUT3OiGoLdrdEVbOv9d
I3+iaY8UOwHD8VhtjFj1y33tb9UcGC7nCKPrA41j87frPYEOpGDRtvPLnLboLz23snXvUnEYaF3a
gjudTGLfnX3iPj2/S2LhrHhVHTjEHgiR232++4WSQwqyP3VaQqV+KdrYqvZIz28xnFkStpytI31M
9nQ10w6/p826NOlKQtXXlDFJ6NXgGTTXsG1kKkTQerBkbXYEV9VLxc8bFj0fz3qjopi3r2tL/vwY
4y+h8RTVk5VNnY9n3BTxEkpKDimKgWlKG0tHaBYjdXNIF/7W7iEflKBSQrSlEFoqVqbdWNWiUf4q
etcZqfYA9Rzh1EjsWTWtAV2Tb41jFL20/aBnm69SYzyt5jM4HWMP577jVn1GRA+1Lsmz+Thwi65Q
RSRXvIi6XrAXb6meBD3Po+vlXXNiTQER7mZJUsqlVbDiKEzn7hvpAkKIe0ZHI7ouJdpLvxuaxf48
898UJiVR97H6gSjcGlnHnqbgu8vE8vLeATdJayI3PQdIGH4nADmyI0kCT8bOXUbgOx+VbQ5pQjsy
xREy68wFOMzFhyPqlokt15wEiap3Hdwz1tke0oBbBbgb5/laMPruEfp9oxFf6mqrwGsS+0A1GFjL
NewapyozG7B7LaH8MBqYhQBHC/NfhGKMCIVjv7m5W1HJeuk3q87UgKD6ETNdgjwi4P6UmKdw3Zsm
wGSlelI74hbzlWjwx+zprH7qmD2RFbaatFghDqVYfRMkGixJ3gQx/Uf/mya8cwm+gGIQMzmiSIra
lBvhS73d9UcTAX2sZ38dtJcJ7nYnlTLMJdBDgJeKfNshYnVmv8CuF4FdhNgh46AbUgSOlB0balyt
+cG1LHpIsuGymQNkcxwJauNQpbym0wihf4sXcR/PJbIjdoRzK41/zqUS8041un0U3He7WiQiR/tq
0n7aeQ1T0s04jXnrJ1mm6WAmSmvj9yXnf9IXyxSfmSF6P8zkoSlnlBjsHjBC33o3QyPm6q+R7T59
kf3lwXoNqsoH+duLBZWOa3GaS+F2qNmdQlzLbMQHsrInEiGjgNmRU00uinqwhSuHzq2BVW6ZpDZX
JiwFKAlIBqQ6GwiDWVwznfL7fCLCmlcmxp8tygQs6n9J4OG5QrnxUFz3M3HKWn4s7pfgEcoqdyry
J8Mz7boPTrJzLO5rw20GQu4nyTUx8ZUGLAX8hcjtEyHjsK/GEoTAkZcGDJn6ZK/Pz52T+5bXuE+V
TUZArgQDnxBye0HYuQPaj/KgeWNwGK1wG/KjXFz0F3O33b0PFxdUwbJZ2l2mW5ZK00hcTOSNMc4p
1AzmoYseY9a3TK8qnpHpIWAGhtqrVD0zqczPpyAKYqzGGASngsUSYfQXXW3rIpcwUw6tIlaTdPo7
4dR7015SfjFg8iZdky5Mcwxw8tzkywHIlnDV9to5jsRrRiL72mizbEqMAWtz2qLy4AsKnPjX9KqV
Ev7+PgkUQoxi0YJC9e7ReGy0BrgZBFkVd8x+lPq5YFO/jnJBsxk2LY1yjaMxtnVjmH9fGd8c24xb
RFnb4H4FO1Sp/b8ejMqpPHQB/evu9kjrBdx/7oUlK3daW6zHLmT//CF//gACxDqO3BEgu8GqHHX+
dUyc7x3F7FLJ9LM4NeXIpTb/lp7olzP+Jzc2lVAo3ZPieOrvnC4GNaNZ6RIkOJ7PnlYtsGIdWhNz
eSx/0rWCtgIkkKEv+bAAivf5ibKs4DdXbUx3z0X/z2ZO6B0ZGhw2TabRqXwCdfTFKurGY3s/had6
q5XZ6o+gS/HHsGhdz1ny6Gu2hSpCb5KaSSDPimKhi51MLTHs3dslhDDGvrkas661QRPAmfOHeOX7
LWjLPKQqg6J+JjRDZqzT1gr/MDonysXO3rQtJ4gJd5Y6CWXXkKUIxCaCC40KP0HLuxvb7LLW7AHZ
VWehXenSeIx3X04KPLUzHgrZvnb03FhQCGYPiCtN/WvrCUWlvvtnAYEMh+4HUbL1WFQL2zb6zbuh
4UTsefuJATWzb9cMwlaaw3hQrZUY3n3T/EzpwrVs6k82sMtEliQF23tcNGdpSs26JIJyKAGMTojm
p+1f/zueuPQArfNIM8LN4haG3mQMKO4TzNplr+bwTD7Z6iwLMRdUE0HQWNMgZJUbSCw4lqMWQ2ih
43aQ5Bz7m6uCfGU+ONaSn9bPGT/0eZCDN4UAZvJPkzrxKr3Tn8HUfjS2VJrS1h5MTzAHQpNuhHqF
VC+VIAv1HN3K3R5F3BlN+fmX8/JkMjxdVUmGbGhj5pWSb1XRyTa1sFboVey6dyvnJRD86M1LS2pS
apmS507UBduRacPE579r4fLpcaP7Vczz91omPrisXqryX2gIyyUB3DUqtkJkTPqGsKNWH6L4T20Q
a8tvtu76luNSXx4+AHpl5pasHucnvcgWiPZMZkj7wa15TB6CPG5XPu1aIUobLDshxWpcWeJcEKk5
DEvumH9pAGa79PszzvyXJRPpTGqyQ9f5qUZJ9sMIG/VClEggGfsqB+SKBo69PtMY50o/Rk6CTPNj
bau3I5ARipUGGoiL3I3N89TzC3N8IOHUoBaqN3/m0GC1vfbYqQKsjWwF7j1+hZSCzvJhy1pFD6Ua
NLeU+Rk6+Z/qQbh60OTl9AmIDmIIPYXWn/AeOOOw1GMD+7SCzJTPs6Md8xcyYNo27fjET0ECAkTM
fswEHBeblXSZEYFJWDanehjS3F1Gagvk53G+Q6bM1PpiHWqzhmGtqPA0EbhMj2nI1VPJ+9HSY1Fy
NlBBo8aRf0n2cS7sruZAWPhS1YappeosjLu56ClzMtsrd07QrnhNUpaAs2zYjmY7RWO7m3KEACLE
qrJkxUbo3nGKbG9yo9m5PDAnttQ3wD6BPFZL3hEQMDwLK6roKxxH1mCDjeZrjyP4Z47hs5EkVTsr
sEsjtRVOgMvsQlnL5Nw+uYkcRAuu1uvHCliTk5UGmTzMT4hLRt0rqTgcFCMZODOFpGkPrvWU/11m
wv8znZRnPV193tV14DWwch8ciV9mOvKbI6UnKjGkqTA8OJZ/HCBedot9RAB6sNcW05DfEtQcFZGk
znotmd/MLKFi1NkWnrTjKrYbzQD6vLmaB9CumCvXOgYSpsbV5cYfsoaEIFq15OQxxeqhGBA5puNe
dcL+QOMCoHPE5hYs/JlX7dP8CutN9Ih4j+fQp5nOW2hQqVSfI1R/XMjyKRM1aXBgaKiofgx5fJ6d
CXJuBsUAgEiYEqw4FcjhO3rioyFzEINGPd8i+GaxzaMP2Us8gg6wmV6uWTSzsB4RseemQqBEDmWL
3ji5gBQ69/4jxZU7bQx18htIzzshW0Exr91AdXxrxJMYtp7tCOxWDLkN4y2nDM4eKpSyb7U3xIxt
fUTJ31Irlb7OMSTPP7faHHQW15rX3kCuSadRCNSrXnvkQmjX9S/4PYOi7iultKNOWmxx5RjGngAG
1c44wH4u7IXfL9fuc0r/1Qvj5KOw1UdTwpOqBNQeovb3APB/EnklLkHmX9o54t0kjLLoFThWgASG
ne5j0/lcuyDc/1Uh88tspifdC1kRVd3HgaKLOSFsuoz1vReUj9ksW9T+ZO8eZtzAakGuXSxvWRcR
sTmdM75gRzsbgeEwY4KcsMJtdKS9czncdtXIzipcna6rum6SLmtGrVNmAgY5o3q80CPWYY7xmxgY
sNB8pHfXUY6Ccvq04A2TMqMUuHty005AmXzvItd8IUFr+LqUVZghMmZz5ogAeyTCcsqOok1rtWKT
bJUey2SX/nk8Mw/9anF1NgNwVr1uBVc+R6U0KvbGc6tjALIXu+RG8f7yLNKmO2F2KOJBXtfrhBk1
aiPXuNUYqzDDxTFC+mXlFDqGoY9uz2sLqbt4bn2eCOT8x3bPqPVlhDIliGEa24poLGs9vur2Ynzw
rdFWzp+jLU6WWTt2LGWnycVit3HwjomidEo7hjKitPbRLlNTUkxa2tR4uYQEzAqVpIZ518Xo/eKW
DFoLMWDUqKhbGbo6IsuP3muzIecmMWtD9dVhg4jUF4fgcn/8NfaVtYx4ZoqrUjQD8fSdZU2nM/MY
4QduXYUH4hWQWJyw3BqB2M9az2oLev43TfajDR1YsnzWu3NyUFF1FiJx3Di6GPeIHQpKofr6m7DO
cyXwyV0MWU2syXxPKY4F5sCiiFPSUB4XKJuuFoL2DsjHYYe5K+fl9V9RPcRJ2MPjd53RdlW0Yv9z
A7ZTSNP0N2Ix0264DeTQpy1V3ZHf2FyIX3NK+6CwYH4Oppge2CacQo+9k+NPAy6wHGalmAX2z/EJ
39Tv4adpMfraKIYMhb/3Bdsypjf23IE1kEdYVCkJDMVdfaVmu55QRGnVdKWejd3/zUMw4t5UTCU/
onOF/80silSvTYT0lgfmi2IjWWdfnxZ05e71qVmIqkMn3JPUBGbJHaDnprSSbR5LG+uCdLlCJdbG
VZaniLs5/01E1PasRixYljK9fLpsJBxHRs5TGSW4+UxCen4Pa2qhNYXhzPI5K7toMkvnv8xMpOvQ
WbiRcHr9QlXqZBz1VIUgZvcvNNp5UAFjcmHDDq0EtS33mWi9m1tVrIGcBYhWxvCYS9IJAoAbzH0s
i3FaEg5BvHXu3fYQK82MbqfeQREyQxWNAdYFaUP6w/l7FJCr4bZ5Q2qlyaCptCXkiHtCpdzJoVQk
UEMMPvBtkZKPFSyJaknKIdhHTx0I9LEOTeZYbhKdbIUxVd3LorGz+fPc8pAMDYK2Cs9RkhZy5TIX
1oVqTRrMhlPlfScFceNqZxii3BovQn7eosB+068B3vvyJrYXiXIuzsoT8A0mvR1bpQzXkgYYDSDD
zoxeV0K9B86tfNbhcselvjw3ULSX2TB6QLrZMgYYl+0eF29OfcFgRU6aBLY8CbMyvbfpdXCBXxGk
s463R5lMlQlaQDt3NgJyt0iEPswyMTmBnguXZBH45biZVL+EPPhBWpMbPjkdTYStrfX6LVN4OdmV
0YTrVsh4GYazYz96s4FZAaFaRtTHEOp78jmOrh5aL3fIjJ/ZoVjoN3wCUw3OjOpy12dxY3KkkJDY
SLcT1CI1fRWw5Ic4IEQo03kpdY79gSl6EZoTR6WBuOoow2E6PTDvzpB3o4kYct2CtB0hn2qmEtUl
yHskBV1VckTz5rrbKIr1m2I4XmJp2QHsaYY47IuuZb7NWOLy26Ag3PwOHLTruxY+f+ib2ZI5bLUB
HO/FUI1x+yN+tZO9iBrYySnEMKjBdjNBYsaQSk3hVupDSRZZ4qCDQkkjZdpEoUCKIEYshowZcw8u
CemgS+Ls/BQKYQwuVc5ZQOZal7b+wWiVPL/k6ofQ3eh+uVYmfTyCVRKev21rolgs+Jjfpcw8rvcV
UkKvXtwF7whV8TNFCuuvZV4N00lxYaPJjJA7cKBMekkDE06SGEg7svagLd/x9cVidc3oa5GqRpiV
cgkH6wOpy1Unq5F1VGkFBt6YTOKo95xknjsw9rP5G7Ug0wiR0pqFWPmUMCCNG2rtAYMkJJkzFa1B
BLBC6/Nd+5SCcl+YPyAoaxZD3W1RQ3ZoBv9mT5ADzaIvbSNI8RyPmimxugq1PneAaxjz2XrzmocY
lLmidIjN+w2oinJlC9NN+uorfgp+hRtJSYqLE1Z4y26Dy++9D7Ypb1MxuVyGhXw3JeBF+FLRRnCG
tq+6BYsJLydcxzqbdtJebmiMsUT1u3MIPBZ7u/RaowoFPTqf9mCGOiV5lr+gr87ffDFhgrVF7Qq6
5EaFaYZF/8CXmSb4Ho7yJHGsTIrwmFadd6h6q85sOFtal0TYS0UJQe13S4w/STcKF0I0U8dfFugi
nmZy/0i9YzzyX9o+afWSEpsbkKQlrOKbCHRmpsDYEhPk5OzEk2IRu51t0BNlnWXDYB4cr+zy1qUS
iROlVeN3C4R4p7x0pxRj8hgZBxQMi5jOtgBK/YlJ9Qqw+ViTrg9ROmQk5NJvfSfOv6T52NslL0Uz
a6juAWzWDZhbLSIM8PLRF8TeIjhsXl/6EwfWo/MnOrZXljGHIh/5Ui6K5Eh95fg27NtYI5DcYBId
o5rTUAtrONij7zcz+OrlkX7cFNXLy88i/QI4Uft8NpM4Xf23An4GnaH8aw2QodGh506BXWSdxdLD
b1FGpn9wf81LcVeJdZXmMIo/nN2Yy+TzrlogbFwmOyhReJJs4/MEEEachAblkbiVZjHSliKtPYls
yCLH2X67DnPKoAuOevTisUwY7GffeO6u84wB1bTWESp6k+IrN8bl0DHfyj02OzZrMDpU6p3QdNLg
Or16qD5s0OHkCJ7ZUP9wuApjcJMlJFRv8Kzyuppul6d9S5gLQk2hWD0wE4ZVTV+UzRZhtYi8OTyh
dt3nFaPE//agzTWu7MZNzZPYncY+PGgdoV7CLpmEyM+n4ieor8aupCuPmd8PZqoXFnRY6/7SRHrf
Ns8TJNvcr0IA2uRAlNc2vk2g/93uTbPO+FZmpGn05J+gq8nE14N1RO8H+Li5I90smvLGfgea9nwF
hJxKCgE4uwwr4MiVW8Q+wEXCN2gq7TBcDltdO2LS8vr4fK7g4YG7cnV2jAc2vPkoKf2XS3QYdRxr
c/aTY1EGiZnLUrcgKDPI9x9YTcLWWg2fSJRaNr3ZKWQ4kMTPey3C6Qa09u54Bz2FSw+dnPnxd3XK
W1GMu5swPFsDtMd+kFogksaW7yKwNN3F37XKsOD9a3HI6cu0QVcqepbs78jWNqgsR7ghmT5c4RiY
8mPkI/JGUHiQveJQIRPEzb9UNqGIpJj7E8w9rVZNdx9jBWWSB7XIsMIpFAZlC58yXQSyL3VvPbYW
RqHqxmswzX+GpakjI1MY3ELEED8EpVn+acFqqr2Ql/Zsblwx3zsa1kzCmnKn18xAY4e/8hfowYsU
aQkmvXl7ou3PfW3UlqiQNPk+dPiXmeWL60QyLrsQl5j75BCnJzkRnbObcYWLYlvJqx7Xatdo5hi+
mMX2REP+artnyfvfxOfQtyUTOWRl0ONryGvoqkaiIaIbY/X7EefMfdcLfXxQ6dVVNNpDOAsfvgPO
BP1fwuby+ntmoDfz4N6QjfV6HsgsOYcKGvvavHfylIsgf9UYbRXdR3+Y67+jsu0vUvpFFpmkbt48
ckJDlWKsFPIEabFWJPkzl4sYFr2dvCjEtEwddiIfsCuRYS6TvMYb7fcpJ75jBmYOzXukE4meBZY7
yjufsmkjkQCuu0fkOMlSrWnjWbNf1+rvWHR8SvtLXUs3EkjKdsOGviPTNErXfqHwQ6gtdU09PYV3
8uz/4bdjljS7zPwRzSgi0yBl7PA6p2le/NVTBiciTd1AldiVmVItEI4Ddb98AgjoqJp2AmUZG/3e
SRzMPspPPDLLffUxSAu2XbkN4btzlmTKX/wd+CWw5RfLvgmB+dlKb64wGYQJykV56ASUz+CdtcTJ
nfGJsfCsXXP1VrcQ2T6aZw0SAsXzs+uwgf42u7s7/rbEbRuDK9ATl/1xJ8PE5ZQ5Tk+ONslBGfJR
+HL6BCIkRrCDsx9X9wxeGeX91K7Yp4vpvXlicq2xyVOSAbg9axVjmZf/FBsw+QxYpFzPXrdSt3dM
Wn3JGyHwk5LzNTZMD3T4KKb5Uo0+eqwOeWkgi7wcqFExxDRB4NPcwgxM2zOH2CkTVB7FIYiPJSat
vMsvbCQjGtOVPdTt8glRHdmEbJ1oMlSXxADTw5dljMBh8taX0z4k928lIjiShwgtbQz2Ri3DQdrg
IthcurmYMlaYyIwAv1ZgLylI8Ri6QEUu5b8vcaPF2NAal1q5ntAL9Kg2yauRZSEZgJdyOG6WKxjj
PAbVAgh2KqASQ0hj1qIM0IncgBkelLt+d4zLQjHejZIUjxRJVtHHR9U3QuTXIVEXGXiyddh7ylnh
bKxZ+FH/Ef9UCPTrgjRchw3UW+XMGPEKj4v7kbzQ67JaGReGUePv3JUX3XRsVHNw/MBOeT8YdYle
oZ++eH6MlHjrKzH0ETMJDWHyUs5qTlzJwoX9IMBp9IXce/KfCW7xrMqZqfaATk1UPwzIA8HL/PoR
+Gj11FLfkBRJAoulwSz0xPlV9uCqBomTFIwGVYVzbZLWyAyuXMuETQz5U9Cdylfau6SU6PPi7zcv
/fV5cMghbvB2aC6fVuHZVMmRq1cyqRx2hWkbjcuLuLKCDYyl6zHZQti4Hm912wqVYDFlKgwvUMLC
yewiY+Ljab/5I7X3WjF+fY0T6bpRiObFjMFoVIq7CbXMrnZRsXFj8cpwwugiki1lLdkcC4L56Y3m
f+3s+mJrQx3aLobrHHnKIgaqh3cfsMlwRbsjYk/kXQn4F5AVDo6q59nzOaaiuXhFOtRNO1CAvFr7
vmNz4Wb/Sw6o9HOc0WN4doT+3nxw8i5lWI9myfAB33b9SLNV6b1LHHbAGKgqsDlVQQ2wVNAcB3mK
jS6HpjANaDjmwnOaP9WQ1j9b0GgeXfqOu0bZXK2n31y/NMUFbjygIW20CFSyJ1teSg6Pk58bnyXv
PhTFhVZzXabjPFCrJzj3j2aveXj9/5YTrgfy4cCH/3X0TPbGjG3tGv9FkcHq8POROENINTgYYG8m
z2My36t2Ji2jgMGZCv/z5EHQ+bwSPwauZkTM5QGrGkx6zYhkWj1ZKyloi1LMPaQ35h7z7+OCkWyy
ibSWMa1wyTuE9XLKsItZrVjWF47bHCR/AuliBNSzXJPE1E/8e9F4gSo8iQIL5pEn5H1h014qUNcS
jJUYRxOLW/LDFw3h3xPpardJ1BLITlLUEF7f4jZ6kJJeVJgl0ViGvrSuXmL4n5xysecAyu1gQj1I
L90clxsG13w25h2Jkt8KhWzJWVKLNrDLAB0VpvUqXN6EgCiXVA3fxZoaaT/p1qEW7xF6SAp0EuUZ
jcUoOJNDi6Lz0s6dHOEOE2+A6jm0IC699opA++ATPmK30g5hY6OBEExRVjNdm0wg6wF9CJNxY9T2
SXUl24zH4cCy7keaBpowec+hh/zGqTiG5I/B5WQABksyq09ruu8jaSMo0R+fyn09gwoT/kQBjyIE
vrNVaOdLnYgqB3JaKqd5s5Jklafb1XQiWZctjPDOjmeXPswiAP1HUhZ7Lws4FHNNlAhyqCrcNkKA
jRh4o+c/ShPFGvzCECi7mN2aMXObuPf3Y9/BuvQ+uAIAPkWGOr5xYR5D9yWI0Skd7QXWy1Lp5JOl
gh2Nq3xCpad2RnNanKLpN6SVKvDHJz2RcV/ThOyCo3bl7fmgWjvbmFsgs2phjkrEh0lCCQnPy+13
sxOwO0zvDnL4ssOycNa+k5vjflCwgnHwpjI6Wx/uUV9Izw37FJCA4GetktPc9nTJBJ8Kqu56983q
zn8h9Tp557c66TrghHuvM+56CvTfQN7lKOd03iJVD+02/cWjIUiAf0jjxfIf5DPjiisEsex037nr
GM9PlUhiSE0kiRT5kpHRiqbs5PzTeHjcxG6FCMDPHh/oZkkBD1uwIpXSr1chfUByRuxB0nD8rnMt
OI/tYdOEyZr/1c9cv+7/q4BZiXSSodzR3/qLNo3/9h3MNIlz/Yb7ZkXs8rYAbyG2tfP39qBuj6P6
OxH8T1kgulx7i0T4dsTs
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
