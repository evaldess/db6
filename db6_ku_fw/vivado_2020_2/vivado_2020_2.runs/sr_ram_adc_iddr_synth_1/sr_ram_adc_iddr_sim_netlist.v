// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 16:59:17 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/sr_ram_adc_iddr_synth_1/sr_ram_adc_iddr_sim_netlist.v
// Design      : sr_ram_adc_iddr
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_adc_iddr,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module sr_ram_adc_iddr
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  sr_ram_adc_iddr_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
XHE3IrNUR0rAgOSs7TaneZOCem+xKOaVUndAgQMQ6fiqQ7sNz2l5jVXfMEx0J1E5drsp/vFpyBfK
us9s0XKVnQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
iNP9Rj01ArmVzHoVSW7lElSGoWnbQe/FKLklfFiFiJRRgWHkBTgJfwNby6KYAgA4XLe1eWz88cQS
FukoZ18JES1Zuf+KwL8zwISn6iD7iixfZNEwpWFYjyj8XUfUUjAVZiCjZg8f5vwPfWs79Kh7gZBj
vgDcYNXjxLehTwCVO1I=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
nmobDEi1pust/app0GNcoN+V8y2mMEri09/oF7dQ5ZiEiG2p7rMxs0iS5vx/JpQ6fiI0X0AJUPZb
worjx3dSanWZxlmpvUQW1C+LK9h5RA4c6zjOdaM5qZ/K+NCauMad2OY8ZgcddQsrreoTh1nJ2DWa
TaZPLvv5pf3U+x90B55qP2fEPiqbYkbzpATAH9u4NTH7sLWgjc2AhgaoW5eC8oXtXFv8D/e6aVTG
z+0zADy8vVe9/EfQm/dJ7Jg0DqAR5qYWGcVn7yVF+tPiL3kEf6FJZBjo3JgKIu+qAthsglm8Cx+j
2KVIa2CX5Gw0SJbZkMW71N8rkZU8FopYgshYqg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
sQodddsOwbYSlSsSDMNCYLeaJ51uv4v/ftdtzRqygsJNUO74ZhxTo7+viqM/zY+gFJjqy+vyVh6/
lpYCCvOfPW9ohlsyigMit+d9OfUAHtHOnSwar6P7DvEbD+534I8OBinFHuDcHnDIFirvT7RdkfNd
uCfMWv1oGIMacpnu8DitSYvvt8DCB+bHlF3ijp/IC+P6O1hD15eQnQpsDwpKg6nnVcZHA+6NbT95
rwOncIqFR4E+wPstj6ayfvxsin9AXJ/L3hE0nmxedSpKDKOwBjtiGDED3rRIS/N2OZSt7dsYgyAa
MHSfsznlBT9CuauHVihH/u5MN1losnUyYm2/QA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
PcTPY1NzlVv/1miCbWVLH41v6m5uRKf5NQUVNklgE08sx21KGWF+V/ICQGqfMvIC5eom8kSFM2HQ
dFf8l+zO8zFaHEcwmOu/VP5gnGydh7qelqNx+0jPz05q2jp495ez4dMFlOZ8sQGQEzx0VockI9xn
YjRJ00trguEtLmc6trk=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lmC9ahCx71j1/ZSeKA8Rkt1tIlMKGNu+RHHj5Xtwh0bt4FfcPDS17km8+8ppXi7OUTyBXSIFrdK0
NooakhmRZCmMYOTdKwnxgk20HqIlahm9Iu+bxjgvH97W6T5jJcYvFslglttPbZrvLoRpnSfUfQT6
o0EtaHvsEFdvL9+ScRUKPku8EqkOu2Bw/VZKo9IMnl0FoU5KXba9O59tKh2rkrbNw5L2gwOiI4hj
K6KuGhkZNMCIC23+bh94VLvhhAbeZ4zYdMXlsjm/BFrp9rW2/KEFj1X0Rlmh/dk5PzuDb5p8oOdz
YKZejj1J0rHlMYssmi6qnwXn/kI09IersaxdRw==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
anbwWs0l97JVYhigoT6et3H8TOlASkW/Y/8eTKUdRC9TcUSfTU88XxtY8yyw1fQpzUYR2pxNi2ri
ijWnRd5cdXyd57zrFR97a5gvOC1uBQO+VwZqLcjkcD+uCBspFim6ZUmqCQtPaJptG7SMYEatmSeu
5AOckCi1UQBo3bcklZM89hRwua0b9rPBtFacTvBkGGMEj+3Kb+3nEBjrhaIJyprIebvMvsj2unDq
NZN5AyhAJSQgoJgaiptXgMjTKV1UKRQ+AUYG3Il2upp7ugSL5p+QJ/8P9M8v4jzmg6XOd+GGtyl5
iWC6yFcF9Yjeui98q9M6xYivbpBmKndva6F27A==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SEfonlyNG8YAcVnPx91iCPk8borIGPaWiJLZAjQ4ei/rFpUclmCrmdDaAEKl2C6egNjlAS0+sjPS
Y+zDUbgB1zmvlc/tdhSobfHENw4E7nVpOiO3LpH0RNW+vE5gVHIgH14HjipI+MnMpA0WPM1yKTc6
9vNke9I8uopfYKPwA83sQD58OW6+jvJsOUI+g8qfuRMbZKYy/Y+NS2tS4ypXR8KfAWW6gdUxjrnw
P6T3WgTbG/zxJarG4sORWn96Yc1NAiD44AkpnonzeL86+briHkw7CsuzAVLHENNjRtcIeC4zYXDr
LMlHg9gcMiK++n43ZX6hfeV9cJnsZRPwcJdMvA==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
lo9lKufC+4lUbxCisEYQ3GipTP95COa6tmahcp8LSG8DdAWaHT60LT7lpmYwIBAutlJSIqVJnIHn
qUrADSaI85BggWmFFPiBJ9l8F429HJ2/9X1wD1vQmQTxvt/NBuo22uXQ/9tVB5jGm66HwdD7M91B
vQ/PxfdS7joZd4HlMEsJLq/DbvxI8yuhcPiR9juvFHiU66JL+blx5ETQSQ7BUFQg9UthtE/ZNgFO
J3eLiChOF77wzbPzU9J9Ypvm/Py5gy7KUuzfP0RlH7s+PK7XKwdoCXUWxfvIJ8LKfFQP+lp1RpWV
4tEypdUV2MqqFIbhXuNGlk4AdOtkcO7Vh1IvXw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2112)
`pragma protect data_block
fM/bpWe7/jqlLsG9y/SQgSYrCZbWHHRA+l6LjKUiONlYHznBOAuKT4+zFKBjI0KmYypuw2l7ykpM
DVtIjZ9GYirjDRCvhhFFTdM6tyzFGeWFfsXgaOhZh0lzq02wxWmxc8kylJjvQIB0c5UW/jU8fzML
Ssnnz2ImO552fOGkRXvRx4hoBNcCans6D0t6iYwtRjREgrXIONUQROAHTsfACiOOnPUvJ8Gf9ZU5
rBPyOvvG79tS1wrVLvDCTClaV3ezbYtQnb+QfPaiOJiNm3aCETSiGVuRAbGp7ZwFuf4dfuYtsoPY
+4hf7T57qwhhxXxFZjy6+6SsoQtm1TgzlZ/gjL0RRtX7jBV7m+ajPuB6x4tJ8b7wPPWjVgFg6EPW
PKOncGIqH1HyhXED9Jq//v+bRla+n/1vdkukUJQU4kI+c+aFrOwqnkxl58hp70TGqsHTcJJHQl3z
12xt+9ND5R4ty2ZivlbMh6zqiHnKJrZ/O2kEDIsBJJ97ZwxndkN1/LM/jjD7Tyd7EpsPHOg0cbFt
AuAyGYP0nambdhMii4b8FnqCPf69Q2H30qe1tfKKIWwbH14PsFN+ZdihfnoiOfWVvaliDEMHOmv8
2l9vCERc7KQcrds+xrLVfVQ5e+CX36SsXfcIgCRLvi5JDrjAZXfwr2/7BgaXHXLOgO6sUm5BjMer
smM0UI5WoxV3IPByslkTM3DiMZpFN1kRV2gc3npnuIQXDaY2YFoPGAD2vus9iQ/7ry6miA9omwvR
/aq+dXkxeZPlVUybagvRfDFOUSbtQOL0csJ3lyoZ/LiKjY9E+5ToO6OmULoIy1HaxcN8Q0tf1U12
3MKW3YrwXes/Lu1o9ci1gfzKKw/NDXJAcaYCULMbqmmFxQ6CJFyU6uo/h/qPv4o1+Svenfm7ul6A
Q/Y0EkJLOZOmynSgLDwGUJkYPsqWe5V42xRmlpy+Gv6Y9C0dpAbExI35pmLGIzgxSwXjCQCpHs+x
qjDnZgw4vl1nMWEYVRxDSOW9Y9h3KFms5fZMk+ORJmKUrVhHgBs+MmQz5NMbV7W17aYYwhPfuQSB
PJVUsJEVboLjYRZ6SjaXdShmKCPE3xNAi5znyICqTyAwtMSUPsZnjsy3i1Q7lADbqdiLiNLPsKPl
WLreMyhFPTLrg0wugue6ojveAjADYFto08imEa/zHUxnwZYi8OUwlJs++FO6pa4+U3MKMZ2Vh3fg
2Hylb7gdmSePzASrpbc2LI4ndlr2maUlExXyak5lSXAGS0OaorXzSxKRlnjm9OlquonSPEdar4JW
onwd77JL7WG0QAiUR0PK46RIK29a0h7/g9mnyl6G4+qavNL0b8KRA7tk3H6nXGBpGJ9pX4ujo61D
Yh49k5F0FIKapYQNRA8hY9BGXPlzeei0q4mKQNPBnRLlm5+lqplE8DChhalEPNyYg/t8FFLuC5j9
6E2fVuCxNDx0kEyRND71l5icpfXVwdPmQweaVH6RU67s8pMS9cZ0rXBUOIXzLUB/jXDvPjcYI7bC
pb1n8NZCN9JSfMw9Dz/2NtzfYKq3i1x6FDBrWsbHvbEUWAw9c0jAbG76Fmuz9fWIt4bkl3sZTPrW
AzS1CNbjkxXUYeyrC4Tbfq6Xdnl3cwA2Bn0uvOhhIRpmSv1So7mowHkfUtgm8uZCGhq/CIweLcyG
Erkyk2GFhR0ZdWmI2Er//QHkhI+v0HRcIgc5FVjXyBv3Pjf9JO0jcXkN8iIosJS9h/LPFJztTyuP
aiFygYuuameObfPKLea95QoLUHG0QizxYlGsTYpF124iv5tJzlgbge5zhKGhtniNaF7lSy/WhdFw
fOBmOk1jSJILfoEp777+hty+ljZS+81mB06FdbcBzES6btT4CNEda0W0dcLOgQaL/LyNajULWGwl
qCHcyviVuo1byQuZPK2Yr0GaJMYrg4lVFGTIUURi5PThh/YBFGxThDhC3xEPjGROtSp6av4ENtlw
I4fpMz9phO8xWn14ld2JI5IS6dOPGwroRFz2Lto3hw7NrAwK/TKVICpcS/W5Y/jfyrvHM5EpvR9t
p1h3d3XOm7HJdPeb7lRp0PbztNA68Lq2kjW8e20vVv9IK4DgIakoEfH29PYdv2vQPIIoytgi6ewU
ZkbSO04YJpO6/5zB4XarjSn8U2YPN4L3e5bUjN26msZsFjFJJqWH66GrQicoByIxxtMZ+ToBN3VO
92VrAGadHbwRLKUQpTueJuvPmVBNUbqTJ4vyg98tSMK7khxDjq6iqWsf5ymy/C8AcbQwdlJr5M/S
El7HHMlkIEXVY0l8oacMA71TXWjcrPCKvLeXtERkzzLrE7RLI1U0iHT+Osd4SVSIlWjNzjfGKFCm
/QmeM9WORncWmTitcd7ZJD1Vt/lnjAv0VBdGYSiLLedlgMz0QAH8lwKYPZ8/1JKPhvUohp66bq1C
gTPfxrXxOafqEmHGcpW32vfI2Y3EV+qrYf2DowxZc9omcy/3QBfzKaXIMh6GQMSdzQ3q+lGLv5s6
1xGm6tj0/pMyYt+j9DTajyufnfozy5F3zeagd6G8F+iMGH+ReHy/kRTC85A4Lll2KzYyr3QXllw9
bkvCKbbKIL8EB+NXRX/N5w4WENo9ns9G92msjrSPDQu1BwqNBAVHGHCcgMLqvAR8zwpaKovbZ9Pn
5tbznL7Kf2for5z3TeWwUCqByv96eJNXKnQ8VnDeuscaqix2UzOT1unAVnDcJLkiMJkWs+TSAiVh
oCE2gUhUUZvtGF91X7nJTNMVw1nyEbXrBaBPKtoAp3QvQ6EJyJHQoryiahnqefX03waPBX7Pjxip
EtMg
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nwI9apodsxWnt8/qZ84l2L5r2ru1rYRvzH+cIiU2LZ7ZFrYGVhrKUku8GacxvPmk04mNLHGAUf3D
0KN1yrZ0UA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Sm1hR/bXnEX5hSLJC+m0q+qTo+GE1jW/bGh9GYODVR1B61WO0x3DI91rmMkLB3jXabqZYmZaVRnk
N8AiDf+w3tD5cTm9k3UfnHfkmqEgj8LBJAWCYHciLWzjmW7DKTQG5Copg5YaoAmLrkH/R11p2QBq
US3uTE+2f5z8QlQwimE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
y/EngzI5VWuiEHV+TKhmZG2qH1QkzhsLqS3InhpMlNY6l/FsFenjJYgIcwfRB5cHNIe7FLSQt6Ne
y3HMmpsqF6xetN1AMKtt7yIa7k99d/5TC5vyU4dMYs9g27cqHYJzk93esgZCvjIZLHpcXw/tu9/b
4U5FbTjst4GUWQQ7e+FOVWa1BC4H7jo6ZOE8mZ1oMeTUDMRBFFBQWv4xUZFg+dKul2euXKFScShR
h6tknaycBcdNbA+6dQJo+VgrTTewvfrkpNyifPBwk9vIitRhFkJJJVGsR6T+AF/UJfY5dEYYFuu5
J288ggKjbjEUNQnIyNWOpZiuhpClTTay3laNkw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
htKUMvAlzdN4BbAAeNmEM6Yr1UUCORwvd6+1cV737AnX/e5QyMGFY1ZuaVzrrzfIKK+VWd/bFDYR
WeL3jKvGUsyl0cMQ9jcxLrsCI3RnUD8yDbbqyDu9KMj34D7UA/k879CbEg7mJQsE/OUuwmk5Rusa
S2E+UVp+HrYNnNymuLmmn6wOTCKRZjZEMW81xyRvJrDTTqf12SjMprM/ubdETBwwiEzoIwLeibWv
EE77NEiYVwYpzXElBkB+JN+riXCrervjpMbAzHbeomW24pwXmffMMvkj1nRzaEI2QRT19Hpc4iqq
tT7PSLFxC6iyyFn2bd5a57kSCEK5ZaaxszxEVg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
ST+OORnrF+3QguD7AuqTgC907V9FPxT3xpP2TfPbwAQB2+m85/czQ7xrlMYLNRNl2qldRPC2JAtf
yRLJmvKEgyRtR6tv/9gg66CdnvMVGbBmprZnmsgKpHGXcIGIVm6FR+ifL/5pZcFZyTQCKYlbE6bz
YNrIQ8EskAk5YXNHRZU=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Zz8HkbKk2BMn9pYqHdEWEMFHnKjJed8tZnBzajqsks1G6q0CzbV0FSYoWS1nKj84tIU1JkBaGDIt
9sdF4TFidxOJyhtrmpNfTChKxpMr41K8vo0yCOwdi29v/VShuI/rkIBCSgrdlmTBWBEgiBS9aabp
Jqqjo1ol263k6jlcp9rOjaoU+lcQMEXCkHoZu/V2+VWtTqhoSiWKgDQ0jJptGQig3wemEM16ctGQ
xX4urrzlEYCVTlr9g3mn6x8NgAjEFjJqmg1uE21AWGXfsNowkj2dYZLCXuVTF108ULXlOgx8TBHk
tPYc56T7eylPXV3Y05Z7agtvOLTYldGNSnm7qQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VHzNHo3jyVixjpbjlcbNuO7IrIjCuYoXTAjRb06/SIYnbUS1pXATLQwryf5S2ETq0CYvThlIAGS0
xbNOLpEIhHMaY4VNrUdhUPBHXcXHWUCHudYKaUCB/Pk28QZKLuHYt3FqZh6wdzI6AFJdP/pykVJb
M/Pyyc+uLtqsAqyWqtJ0puNrBSpFPSM5259v7Gum4dwYGluRNUyJPq0CnQOQDcjaKw42cmf2DAtX
CSJb79mvoLdsFiW5ePQbcfrrcT/FhIkNj4/DqMVl2EB85zQgcPJw5Up3lLGw0Qd2Cd1jeq3A4qcf
LraHhfdfhy6tS33yDqFUeXlzvLfkicvxivScIw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ir7vg+6icGbLB3CLLO2WEVH7p5OyaYzRs27g9ktjlLGEA8UZWJVD/LEebYJEdrotzhB8SWmHZMDV
/tU66bmEBeBvDhzPDFffP8JEne90WI2d4WsOz8gc/qUmQrWkWWpKaGeRzRKobk6HEaC+nXg3PqfM
0b03fbE0S205+4xE/rEnuHBIRBfZd3xmeVaB0HKBt0SGPD5SSQQZpPD38QOtCELjuuuA4RtmpS90
kaKEHc7Je6wpd85YQOJtbSfSfwms8QmBrV2vuYX5vgvFoWdrKhFu6ei5xOtYRK3gX3JKdEXLebbV
49uISo0iQ96Wfdc+51UDQD4Z2sSmPF/BKuQ5nQ==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
LpdRmMYH4gdKs52wqPlK6TsP8t36Rz9etYG+uFXIxoYPOw77GvCpHTnPEq4wgKvtHfjSBYM58T8o
VFR+rx+dgG80Vv61h2/ALXu7WMVNRnj432YN7jUfiNGlmdGjYf7j5bb6jDSZd9SGg9hOG322ua8w
FL0iNhZ1+8bqOC5DHZhVoYhtH7wentMTqEBB4I+Xy3zK2H07hbY20A+hZ5iviyCzHMtmQ5LCJzAb
8LeBnGRdOv8ntIJz3n1voQKFpamiYGRWqDwIHC+A3vf0VlEiw8M53hPC9SjoIQqQxSnkzTditbkH
fDStRcfPfMIOJ9yoREe7QoWlh0XCwpflnMvnNg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
jHSErHCHnBX0hbkBUcHRZ1Hn+uG4X4sjZ9TGvx4YNL3Fn7dd3HZN5J4vYKDCD9yjX2GBlO0hrX1c
SK4hCkhQemPdxD/5AsphwcveTvf2aDAXvCjMKLoJTzQzoaji2leDKxQzotMfyRHDei8SgwcIt1pi
wsKzwqS/6uxs++xk1SFKJ3MFecNWO5wfVr0eNchHBTUPJ3P5h7n/ALnE4I/ElWhWxVuD4IUrZL55
xSVvSIpaaZYTs6s9xTn8+xhjpLRdbtWLeBdUSGcmTSYMgmo4gPPUWvO/BmZO7M75vVzO39DIzG/0
HSTdjh1Pc+sfEROgQUJEj4RBWyZACbx2FEaCKA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
SHA6KGAtk1S/UMQyxFDm5fY8AxY6Og7wFyx1g7i/U2I5T7VCVX128/ccbp21FMa2teAzAldSU3fT
H05nH3Z5bVVQknkNTx3hU2KGAh1NM5qIGGNOJsvDqzNliogrBNME9IrHvsVCMpZ6tEo3nF6mRwUM
IjErEla+J3GKudAxRI0B4i2ZG0NXaEX49puzpBscdMaWKqzbNsIXz3gl9+wb/xy1lYMYSt9WKh70
Z7Vzzn3VkqcrJXfT45fmxrdWHcBOZhCYbtHtPlwMHr7NslYEzolo2dQ0R7clDVxPB7peoZq6pbhJ
ZziLyurLfwFG+ZgoIRIH/sXkVy9nNMwKchARgQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7456)
`pragma protect data_block
+KLRq7ob07CLV6ZIVscH31R4YozmLKTvRwndHGMNIl9LyP51gWUu/ZmEzlE/kaRP0T70gxqoxG6g
P4AcIFIEpTufqM7MXwaoj8XM0tIGmQGf7PWxqETWjMhuHIJCEhdx4cPec4lgmNVfCW6XQufz8PaH
oWQsHSMfNznv7Qw2viAQB0RQBzUCW3n3RIpCusJdICi2qikRf5CqtJ0ht2p5iMTfBo9SBfrkyg1m
RqChMCf2GlNxMRY0/kiXqLJCsbFC7efn+KKg60mYjD9xomPsN1WFrXF18SW27+HVdvQKZBd3Y7yd
7Gjm+c3aMSd0ZR5LDsKw1ZXe2pZBUU75nvTE5kaPlI19JNhOVLH4fv9JRiO4hS98K8RVkkqeXXnO
M0rPcMXl95pNuWaZmL2Sde4vNA8QN0quWjGz4GNqLcK52b+DygEUmlXhKqlXVWjIjoRHZgpJIpr7
25RGA6iY1WUQ6Jj+mP+H4q9vt4geUhWCVBl0NEdS7UUzoFrk7O7yst+i/7vTm9I4SF54k3Wk2IGK
cbJwLLpzqkLp1ktS4OxrZ9WHG1laYWdTPAjfWJ2jV7eXUpW7oiiTJIaJ8K9b6wKIINuLliF7FtWO
TD+rez0V5F5xH3BxSwn3ODLgNxbJkiduw1sjKxqxFlQCZqAYvJnu/1dSv9PPsF7gfwIi5BJ3mUhi
7Yl8318tL0NOY6lpsteBKUpsBAQtHu8fBEQCk3/UKSOsyHoiTmtiQ/bC3L7786poLNlTZX+245jQ
Jc8VRF7WFFdbOVdSKC/tGiF5jY/o8QbpPX1kIOcgLVk6io9FcCvV7FvzAKBxhIcZ5S9Q+vjGJ4lc
ScdosfrQe8NNIb48eLTr+c8xwpLKFFUDUgMeSiKDR94utQHBwljmp1wTUK/sjxFKuYUrLAptEcAS
wMZM/TiCMMxrVfh4AhwTzu8lI2SkGU2DO8xe69WBNj5qfZT39b4lzMTQKJvi71bQtpwBv8Qm4DmE
pdrjvgEZbiCwHtRnTK8yblSXXgfy+0Uf1GmSbuipMtAmqJug5Uy1aEOdkVes6UFiqGSfitLcd728
NTMjDO9dKKOJo44OEzMeJfbjV8pKufjO4x0pqaupUZT4+qEgK6xJeQ71PC+8lwTns2DeqTX9xSQp
Y6N+uFuXJW2Vdh90Lzb9rAnvw00Lh0kh3qtdqiEMuY0rHAgaVyuzHTJHdz+iIwHb9khWslcvp+Q6
WsIb2l2kolJoQZz7HQctXKValaD9ZDHpZ2yNrkf2IJFLZcQ7Rt+g9IknnhkDYGFUwKpPInZHYISK
U+Q6/9YwW918K9Gxrj0isniL7ErLLbbCiOluwTg0dA6Fq+bo2tfzMKe3kMbJvIfjJwMn9hg10VV1
jMKiLUOeT84kqSTVHsHwKdK8U1+jzyXlvSFQyTN/0ltRWOlRtw+tGYgOOLsdYTEWqt82i2MFWod1
5Qp/Yyb2Xhf92SVwXr62ML+Yp2omuadvAEkQEkbxQNhq92ZA7/KoZScnaBg+FF6+OzG8WVk8bXOb
eMEw3OHCFh3bBT6K3W3RupFIvPE61JaOYtV3VDU74C7rFqNaf+1KFlLGv+QtZ2cXlsFpDfOkZ3lE
ZhsYOyY/8/HNQwNQiswte7Q4hvsyXlO1RwwqNs6oMr1Db44/rNLBn1aOd34cNCEDQefFlcbJg0fE
g+10BCLDSEJh22H7jAUmncLOEjRrrXDw38daPEgBPDZPdO5oeepauez79XqisEDgwGItDRgvf9Jf
LoHlgFtPkcMIVObqTtPnbLydqovThvp5UUSs2m2r4nC8zTwLOiNxFw4+d62PZo8JYEWc50B8Br6s
M2tdNjbTzLBsA4ca327Rz25MIu5t2HjsihqkkN2wkOJuf2Bv1bOmWN/KYQLlxURDT4L70mq/KlOR
54k/dw17F9z8/1fB/4ykNlIyEJPvI1j2UoBPv1EbCZ4RI2wZNsHoIPZ2Q2EnXg2Wfa0iIdSZypGP
DcjbACJ1UWO+xVk+41CHggBdOtK+kD4k0TlJ1DkapaWyB6sgN4QfVKoN/c9XL48Xl4n1kgsL2DRS
Z81TmcoCdOEQT9k5QnPC6IztIQgipOcK2UJdn2/yjFsH2yaPu6+Kb5st6JHJG9OMWbiab0xj8HIi
8aqLuovojAGZx7ZZUIvkh2636Sy9iNQC+dccGt0CnbR7wsMR53+69B3caFxTnFjlQeeAShFnp5L5
8BTBm4jNt5kKZClgfjFtZYKWl/ychEAf5OW4Z/x9YDiBXZFjsaERuBIWuK2FjYnz55Vq6pMo+RPs
i2VIsx0ZrxI72tTEz6TBexxhjOCL++GrUIN7jS7QKUkcp9qC3gcIk8Toj0E4FFdPZpZ4UsN3bZxl
cIItUymAgP8jxvFA3a9UxtbyYODIvhM1aT4JPz2tA/fh9aoqP8U1GIjTEyxkzYStSLEDqW/jBjiV
yrsTuOmd3b/bjGndZ8w2KrYqMZXEkTUBXUEQd27gROVXEBliN8zk3rMc6LgUGwlS9QszQAtqSl/U
g1fzXvy+BebYfDHGnx5/xm7vR+sG1oDvg21Kt6d7bQCVtgdIGbkMpzGyMGG8TTuZBK/WbWxaB1BG
QklMiHjILlsEU2OwRYho/rYnompq/8t9/Pg363OYzfPjbuwJzKsmDY/V6rvTsHxcJFljfj/dzH+8
vySrh5kYUK1flrQh88kHj2p+TFNRCgnILupd8k4XWaEst9Jxs+CBPgaZso/iJroZkkZ7rlvojh2u
EuWGaIcRwvXUV5b6a9X3PUyfwFMno2fgZat8pGibtCBTfJE9Fsp83JKnkrpeAGe4zhcXwRRF/af/
W4e25kKK83XFgs3a8Yu+EdEVxxrw0zSN6TduiOpKVam1gmLTB7YbxDaA90e5T9ABcA69IrazbCPJ
gmtWJZSqWdNmloSyDHeLvHDgwUrhbmvsCdR6BfIMgaSsYa9XEfuLBMHMUy6l0gHvLgbyT0Km09bp
Yvt58uKeeE23Yx3VIFyH6OAoKsnMquYCEztiCmoqvTF6hjVROIL50sy1CDwhJKMbVZXqmT1g25jG
0+Lsu3wR/ycy0EbO191OjXU4usdFmBQmAkpJcT8bXvjXUnzccCsJyT8/O+z/Cszqovk7vdtwzWoa
zHgusUoFQxj+xgZOlINu96FUE77V8QsS12c5SW1saaYrrvcQ6Pa/kTEng20ixERqMIRO6p9hxioN
nAfon1CRsrW/ZeoMLmdcUuvy3HA3aSMQuCkhH6w1rkDhcR96RGSAh/q90N09J5OliOsNhtSuVv8z
HpcYEBrjHJTGWc9A+m6M1/Stid8Z7A6MiTsKIjI8xUFTEbiSuy8n/OVTELzYybymIpoeP4I9XXTO
dgYLRWJkxptVK8CGONzuwoBAtenCI/AR6lQaUAZfdHvLJZpehULV1nXrQ6AUWJMxMDORGRD1La23
hAc0ykXEJbbQd8flk1iQxUd2ycY6xyCsavuKloo7y6PTPyYfxmcaoxA9VnpX+JXTWAy0+3s2e7jf
E5Yk1qFO51/9WAhoYCZ0+XNb2eQ6v3EXGJA7lUM6QAgtjdJqJferuc7mXe8j7PJG7Qm2R8ip1gLQ
7XQh1A1V0hhDgvB3v6DnMiQYrNtJHIx1TYzHdcYGkihuQQ0PWCry3KEwPgUryVusguxYpUtV8G6m
In5GHYVnFQCdwYzfgbnarNIrEwZLvqMYP9ACz1s/yUw6d660RwlUPGk2s+d+3FqGy202vgJ7U45o
Z4asImRd/W2Fa+/1k169MSvQKl+PAHoQE+v87Rse+XJFyfYzQnNCDtegocW6fpu4kxCF9H0/kmMR
jEbpmJ08xrDnVW4/+FyaXyT6Y5P1zSjUrnKGDiJVGx2DR9/JmG90wXONUFC+diYp1mP1zSJMxzJ3
1GgGvkN0n67i2qz4ZWRjP9xxd6Be8hlvRh9vVZS5sBp2iUSpt8HK5QC2V+xIeL1OzhapOFGd0pOr
qy9D5QqP61akg+z5mPEsXXeN2osmi6+CoiehPolqST+/xo/KmLzCqSBFOWPpC4QjvRuQV17s83rv
GZ7ZxanOGIoW6SOTiWgDl//YvQiZZ44OmflWLIEA33FnoTKOMBtgiIBY202XkVKgC/lIzsGCOqvv
mle0OZxIj8uMMyKPuVn79mJcPhT5b5n/vnIidRES2EJrKx4vHNBuOYLmwFTPlV14eGsCz7DWoQ53
yW8wTlREf8QgbuCYo5j4OPp+QUyzHB4fHsVeNQnUPuXg98E/2+uNHokgOCysR1f/BPvIU5dbcKGj
9rYdXNSzY/0s75BpDhDEk9hskAcAIOJKBgFypt3Lv3TeUq81p87VMwX6u8jjHazzUQduR0rgfgRM
OGbvN2eVVBdJyzifURhZLF7CWXIL3P4jkvje5y/uhhBYXR8xh6UoZ9Ls/oSDEwRd9mSL3qsZWOdq
sXXyNNVsWmWASn0kYhvfE5vf/Om0eAIuKBovB7TktXxMCL26edc7R28tlTXbowx2Fw0QPRpVWdvS
jNU/BgjQGpTYCT5BkOvniMfmQElfFuqHsRS7OhVcUI5MUK4Aotafdkkexm1j/rs7dCyrnMgmxmeF
V4uc+ugu2sSZGzuhkf36FOZbZuTjZJu6g3tmuj4xz/hLhDpUnfXEZHMjEkDXr4uaNoBD07wtoP84
9rDBPXWaX/yNdzE3DlvEvmvJEKlkc5pXDHfpx316fO0b3MkcGa8Sc5PBW1SRM2fDGqnhPjJz0Gpd
1cFzOLDC9SsMKHTP+5+HRfQMVEJlAXr7NBpQpHE1IQK6Uk5ZuTU07rgt78t81xuekVhVx9F4gwac
0/hlzMXnNm5rkeUXAI3Xt/O6razm7bPntIxiuBkGdK8ljAGiXhV6rJ/Nc+Y4zCMkXEN4Ewt7qZfo
oP/wwbc2Kj/G9xCmTQNOvGJ/lQLEBb+Zb8Y49pCaFCt8jjyNM4/t6GB3oEPw1fR4RrJyA3kYrib2
bhyVjZcuX7LnIivEN83bN5ao0A2kyrcRW3UMtZJG2g4S9ZawlOJI9HORUuf3qP2TdfYKvS9ytK3Q
sFuZIshpAPzJr0cJnC1gTxo8dtj6N0AdLRUnMVmo+YKuu8XkrwjRl/KscKhPASyCvQviqrxcSfOT
+7tmH0NnLuI/Pnu5qoFwBrBvnDfN9gpD10pmhGRZHYI8YFrrXlRZcxpq1J4de1YiRZikWOBDKR25
q6mQQ3npLN9aGa0lHL0rBmN1sVsNhxhPsjuopomZ09uZoT2xkHgunA10TQt+Jh+mOqIZopfxTtiQ
YzYlLf79bQjNl9a5kL0cpoQlakLgi0z29A4FQiF6sDCiaadGisDyQz1zgrRleehafSKIN+opP/P+
Nyw2CjZkMIDI0Fr8tQ5aeS0++xJWOpS0sCT0PN1dY2wxqmxciv7U11A00KZahWzayd2Qsmw1gIHr
6lO2dKRPoNEz0UiYTOTVY5pCmA8GAtBLyCLFYt70flmc6N4zeARKvhDaTmlLR7qA4A1xWNCnZDTp
OGJks0+//NF4AW5CiOIfs0ME3Opc4CPRwFwuX+9iR0WdlnmE3mmDARYpP21GCAIlTGSZGU/X+hXl
cinIaaI4tK/s/As/DdN2Yi1F9FqMHCkrvPpKhTj/CM5xrT5Y6dkosKa0Qglmiqu/aLB4bK9EYgRO
z8TYVK6jW6W3/YlwPfLrMhJ5qTJ2gmpnCx76mihAQNw2ya+YNPKiciow8aF9OIz701T2yAeoGGTh
Xp5kcITggOG1mvB4VINx0wpJGFitvKqkFCwPks5OxlT9DK6KOyj302+7+JFlWSkjFnFYg9G/hoze
iP86HvGDplPf3WK6f9HbIVaWFRFgN2/bYm2BzUwAtleGiC3+SrPtcWiRVLElBvKQnL7Vp6reAIS9
sPHJ7Vmsnp3cUFt5tQOePPiSWRl5byTM6j17AVad+PGV39Qarh3hmQjTY5CJm66JsY8/La5rcvku
aMXQQgnCqb1ZKpMPdyKsNJEv7YVOrSzf4il4+21VXwZT2MZ6MEpRChzHm0n2YRUb/nCCcStz4jXw
a0uB0EjS6xOsWnmkWc7idu1/4gpjrhUGae+w7M5wZN93IkAZKkwPZESQ4ox5iNc2im7/gzL/YVG/
Y80k3fwBUHvAQzpjTAOIGmaiSNSs2obdo4J8jyFGPnSzw4pfSAVL9396Rc9ybqr2WZBVphqbY7jN
Cs1aqcqqGyp0og8T5gAcTsX1NCQluBBiPCjDM4wdIy5z8GLwbOLN1zXUSI9ECmbYzbEDqUDkErEa
JwKqvchhJXiPn9+NI7Bet+xG89RVkN0rjvgZRRid9hp53wKw2+8AGFzg5VgN8fSbDgvlY81SbFl3
oCreWZhxshQbpPkKVfiHYb2Gm6D+fHNRPkPqzFAr2D8aSbMLjzSlsNmqWk4xuQlugMc0oJP1JLbI
ejp0LYtgVXNCHng7R5644LGz0E8aNb8vyF4L+WyHmZoCkansfNbgQEeiB8SsLxwZXv43kqx2IrnN
BHdI5uYLBR5YnGjERbkvc3xHdcrLKx5zD462i2xeRAW0Aclxf6bFICHssF3LLjBZbd74mF1AxkUN
bKbT6X0xHTY0cayMwl5R0mRYLBehfWV7Wuyl3CIFCOYyJIPZD7CfXU36DeKJ/er6e8vOzvcjFiiH
UJCPKwLZbnir0aYNeSMiFftKujwEE8SkxFXWFDhd/r2R6I2OZc2oow983IdjM9HLk4emRJQk9z0y
LI1cqzafhCQQ8OQF2wUbyfrgGTR/al2roal+90icGCHIi25/Y7r/doLH2R3KmZKEijJ+3yf1DxvD
qqb/hiS140u1NRrPzbuVBeqAYj8WkbCZJL3oYp+ple7H7MM4tg4+gNCaw/0/qUnNf7+GCIORhhrD
6WG6JJy+eW2AvgvIX3GLMgdSxD2j1fpo0kwd0rdr6fUJjH8xpusbUKrxoZzHDMA1kQnsC0sBPzCB
HeUSEbSXDUxcssrb3/MNTUyveO4SEMVhTU7G+c9rDdQC8Z5nFbsACGfyaGiTiitCK1pJhJg11Nxk
eGno1HSxLR80hY21S2LEhD51wrjagUTb4v9Ttlg58Gn2Ussha26Ne6qaWOp83/aTst0wEpa0h9w6
xOSJGUrxvPTejtvLqdAsZ0GHnVPsy3tHu2bkXCH+6WmmhSNdDoQkxHXAH4zEpvmr3T+TnwlMAtWQ
gHFrgidbma4Ptujn4OPApH8625oB0FN0Os+H+TyFWvek/jNmAmGyPn2vxAshyFHCk4RdBh1fvAn7
nTMJpY+S10nGgK9XNXgioIkVa+db3ok6i6/9u/dVaCFmkwqn53uIZYbPpinxwxKUPpOo2SQGEJ7P
u+HX1yH5v7eNI07gTPKGk4/tnmdnM9FSuZculY5Vl+D4CACjGBGa59FiwfbchpJYtFcI3Dxi3LXs
sQETi9wdJQFlZexmlYXXuwX918/JQEOjNKUxtalbi7McShP7mv322ixgG524dYayZs4NgrlVd8ju
/CM5VWWRiW/Vehj5YCdn60wX+Q4rivcsecVfvlkA0rgppYVtsqDNjiRffXAd0a6DjnFuRLwejU42
cPw4RiaIjFa1FtuJ+BOUDZVYLLDVZ8E2UbKzxToMSSRGtG8N31RinvkLiBKTPP52tmv5dBSS1Slu
Y4jzI4KOokuoBmNVUgUrdHVD+qxVZIxjt/heju3QYvU2GbbCxIGhhTH6lM54clWE5hWVf/W4GpPb
T14CEGv2AZ33Qn9oFW/Lv1e5vQ3Wt+t1LTniT/DAT7yXuD8bqPS0UM4MV16LpOGI233qdL4NYrJI
nFgdUK8ENesL/aqJF/15gv7MagxZB6topNN6fwg84Q7tdjH2SvuFR+Ngy5IUhnlPKYiTrvCE5rpP
8wTbaPhfJhNbohYX/kusiqlS3XlQDdOYQ96shkMgDhp+55r+/vk/cGwoWj+k44QRKhjsXJ1sLiql
cECcKISf8URQxrICr/m8vIlV7FwIDPeyt/vrcVlnxtBm332D3E7pDzCkOPq7JJ4t6CWgkZGqGgHa
4Saod5Y71gYctgp4Lk8qWPSGbCJb+eayVRkMjCI/7Tj0a0UPFKKvbGLGm/FvrfuYiLJAz3HxAU9a
2s9FMq3ZWFtln+XcSyil39wzEEWfRJL9cRTDaiQuQjwug95Nul27Opz5Lk5E02xPxBcD/KEtPF4f
LIEyV2YBTLnpmDCw0+73cVz2wYrcEriCmI02qQ3o5Lnt2dWNTrxHjx2cWVT5W7ATLRE9T9bWWX6l
w/vMOKPmBGXYm+Su2S9rGLGz2416E2vV5bo2+QEF+9JgZFUY7/Bnz/PQfY3SB6Jsn53K3WBx9D54
DPC+Ce92ddtVLzcQYA0j/6d+pWTwaUvta9e2CcI/KK7qoPbAueV691RYquwq/558sOTk4xNJCVDg
OhH20BvDgHMZ8l+5/8nomUQKKNe/tpIA3XU5uPpzYTGfImkzM0UOXBIYJP04dRNPVZcGVtx7mTvH
rpb5o8dhR3PJrxNG7k0g50zmnhGjih5TeuYAd78VmybIptcSmUvvQvFhsMjpxRG4kyns784q4Ar3
f43EaYXKTdTc7KSF/V6nwxMemTupw2bebb0WFgo1qEeMk2Ubc+cg/vzgdq3tGGb/jANe2I8DJrpP
tyYDyALVioM6iqMCRY9pCXqTi4qsG12ijFZRc2VECur3cMNQcwezPWz2U22xOwoAmPTkkivqXu8D
qFKSeyieUeCLac6pOkQRJc+MpCfvU7HQhwO1TjiRi7gOMvSFMbZP1gx1qepEDJ1WNeR4j5u+Lery
BzmRyv+bJGAQ5odilcAtUvDfUO68bdcjhtuttWyANXFmtqU84FdY/uV0lPTJLZEmKYSlU5e3NKig
PPzWvmPSTyqNzfAlJnSoaAd+68gW55UOlEpbG8rihOgIjl6xbaeeLoQSPpBjCEbTCTcij+PbP5YS
X8khjOzS0gNCjDqmM8keSFO7q6Ka972XOFL/rnTBXcXJFTuctWyEj0Zf0f+sW+YPVYZjDRnqFxfC
xiwhLpgKfgsk2vT+22UyLIXuJCTP5oxQwsTpHCP81Y7WNc9Gh5KzdrybCxLkHXNliwLbbHLcHOxF
qqVa0xW0q69xJ1TDTphVGrSY/GXT/HL8HA/muxLBF2Uwoh4HBckDpf8DzxQQE+cOHwK/GNe/m54c
0ZkKZ3/olQfjGWCGQzgAhr7H/Mk59A1JR84vhjltxUiz1Va2tdTtSNkBJdhp7aJLhTsxy7iU7C+H
F+rXpoFuXdJ/C39CicIX0dqVB9ZsrAdFVASsaTTk3P+9Qg4GSyvVHnPyVD1pDJgtLH7MucSlodBv
MXNEGg4ccySQTw6EC9ulFsC0dOswEDj6rwxCKY3n/8f5sEXmwgz0hGjR6B+oPBEa7IUb3LDE1fWL
6rtGyPG5NsZEC8HNMo2nVoOK4SWCgUx83ZD36AVyB2rZi1cuFpQXSrIbSOV/qfkcyRX0QP/Gd8Pm
r/smWOUKhfW2KDnqV0yM+qsbVXbA5Q9MZVV6Ff8cZJtzVn0vnId7KCFnsSLDHEjt3/7gVQHQtMHl
ilfmLX6RPNRqkUs/rT0ODDTVAoMK1fXQRpoW5fNiy9iB+CslNYxa+EkeIMvbQ7bUjH+xzcHt0g/8
Ls4YC3bW3mI0JLxAUK06UDUjLAXpPQitg6FPBKV0NIBmNEz/FAOjmxBk0SnCxQ//untLO6aRJ2Se
rxWxgI4nsyq859yWxNTbD7TA5B0bOxntSbY430jfQMpBAYcOGOg2hw4cI1kgU7gKEVBi+3wCcSiZ
NxmCZ0yF3/nwzukPB6s6aLj86UPmg3xsjdf6dXOL+CKN/8GWHy52GlXDnfpvhhBN1JHgrWFhA0vR
Uh4TGQ4qhlctNGcdEmppnxhdYcf4tOHGvA8bJWHoIZlNOjrWtt7/YDHbUSOdb2cXt+tSLsvFEKsE
vIGYlzURWJ3a26nkE5qd4axuBTK/H6YDOtC9Zhq+lSM1pCAlKpo9hQ3Aq4gr0A==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
