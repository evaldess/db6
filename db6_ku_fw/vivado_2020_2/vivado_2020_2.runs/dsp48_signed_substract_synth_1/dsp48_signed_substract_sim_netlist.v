// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Wed Apr 14 22:51:02 2021
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/dsp48_signed_substract_synth_1/dsp48_signed_substract_sim_netlist.v
// Design      : dsp48_signed_substract
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "dsp48_signed_substract,c_addsub_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_14,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module dsp48_signed_substract
   (A,
    B,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [14:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [14:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA undef" *) output [14:0]S;

  wire [14:0]A;
  wire [14:0]B;
  wire CLK;
  wire [14:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_ADD_MODE = "1" *) 
  (* C_AINIT_VAL = "0" *) 
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "15" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_BYPASS_LOW = "0" *) 
  (* C_B_CONSTANT = "0" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "000000000000000" *) 
  (* C_B_WIDTH = "15" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_BYPASS = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_C_IN = "0" *) 
  (* C_HAS_C_OUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "1" *) 
  (* C_LATENCY = "2" *) 
  (* C_OUT_WIDTH = "15" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  dsp48_signed_substract_c_addsub_v12_0_14 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 2656)
`pragma protect data_block
cpMtYfVFNJvT1jypAShq4ezuEoNltxxMmxQ2h5oMp4IPoFgNS59RIy9qAuvU/932KHLNhR+KcE5I
4by2DCjpSSpsMkh5urqs7VPfS17qqbIE5r/I0CHEw6kNddjkIKFtVUWY+JKwDvYUixIF7/BKBkBt
TFs27gli3cdh+E1XNWYnUK06kCoIbl+3WHGTh7sChUHyo+opD4n82XhBZw45jlj5jVy4nP1E2SMt
sdBBGkvC/33U9RmeyDo9lXTkaUHOdy64mlVM6i0Uqf7nWqm1iUKzmjq//694OZcd4x7+Tr9/MmKL
TBBvJROMIyeNpP/DalOslsiYfbdqMQBXIlYhBZLvc1G17eHW3X13GuqTk3ZstAQq5h5LZdJjRUh/
Khf9hIkmxXqQOfXpeVotgHR07De9t9C6SBX1viebbAYQUJEYXIxzR82+uVhYubrrcRy+DDZRBR5E
0EgasrsTy80YpD+uceka6rzgTbGi8SEaLG/tKQ+sUEsbRAbOWrWnyJENOv+9mB1RGplHldDJixoT
wgTPJpBrPpvgzoyMgjQBho1XlSeTPeJBrthAN+CAwqdIcyr2tkI7tnjRo9brSxPMVkNSOCdJDnDy
raUDSj1Lx4CzDHkfED/ba2OOweAd15gpDHuV/Gz+cvIH2ZSzWAqPlpMTqjQ96m7yhWUCBqviQl7u
SQbVhtHEd2xxJydmiNDLJbbDpTCauPgfZl7U4V9ezxUNJLPzOdOtb0+5UuDWS9nm5kYsG+S2fdf6
45ubpncdW86AH/HNiUTT8cp/6W8K2HGs+nU3XR9paPIFfDHqYcOu6CzDfX/9YzTMPbpfSXDpRbCC
ieXPpSs6/wPmRk/JALYrjzqndj4RZ70LPvJu+0cxlQDQDWdxT33W2XzM7EOGXVvLwOz+Anw2F0zb
FzBdAZfmiX54dDcjBlPyRyG/V41XP2CaGcEef8FhS5WsdmvdYp6zlLW+z+99EptXm6i0lLT+2PJq
CwfmYWFUa/s61+uu99gdPsFD4K8ulj0evZKnT1Km6VZ1t2r3t89KDqXUuFRkQ8ulCeEDOHl3YdOf
Dlf/+UWuui3hspA1jb0CW0h81dYMdBSr9EUxvwh675N3tzL6062gcbEbjhwl03Ru+O2tpVnbv9z5
034ylhr0X31/ZdEw9nti+x1w/a3ME2zeThLNQaaf5LdZCpV3NOU+bQpb0rrx6vCY/1SRCq7vnUJL
xc9AT3adjfXqZtXWXtgyS9BkwReDnv1VjgCmT+/vg6+sa0EAQAMYNPcGnyR7NEx9zj4Jn73QGzTj
vE4wWsr9H2yGJT1w52z3VA+Cm1xeX9vRcS6LpaP9+5UIFi5sLaAVnvMrUvnJ8OscuRgDegg6y3y/
2RZ1TAv7Aw0wB75KCpUwDu4O+jgSGKTFCb/xv6FRV5G++sdQSZI4H6Q8kmlhTr9vR9ChW4nz0+zJ
4rEyjcZKiWCrUQG75FMb2nC3rqoCwKk7MjgJOEXFTsyppXHTIqkaN3TmSx5an14JGpKZbwHrEh7Y
Y32uIciZLkzbUT4gOaWhHPZlRp1Y+mh0wCm87gtAl49uRpjhP4/TFt8nG3qMdV5sakv3IVhKhlz+
UODikWwbINGHQL1g2cm9ZO1nZCa3anOULVPaRyJgnMWSLpti/4n9y2/mwhLYfxz85Fm6XfT5Gge2
gJHPnBTq57cylSGVjaTm9l8RLoO7AU5CPaGk+2YRkY7bXAjmkKcQO/5k7mFsplCImqsbEV8KMgnU
98Vd/9fm3KdmWBcOUHe6/1tDI5uE4xiepEfKZ59XYrascVJg1y2kVodJkDx1yx+pyY03z8yJTVUc
gzGPFHxgOw49PuSKms3gkF72vOoZXDuDBOUy5+WTcX/pu7VxV971i9zob4xWuJCMxr/XxaSqwaZV
Gmbti8aX71npri9wC54a5PEUChjbHkGaD5GWwQRcRWT0p++HuPxPHTy9tlWL5IZKqxpmTAG1SHpD
DSYCFlqkqJxM4CILgxsgdHggwlFoZSJovlH0V2suddrHuCwrKWgs3WjAALZuIBRoM5/34osmF1Xa
fLv0Otple3K5Q3OajUSwhWW0AmIulLjnytmINNhGQUro8TyGvA/W9LhMGpQ+4yPNDOFVX7QfIwDH
LCkqml9v9GCmcgnJlJ+iz0Md3SWNA0EZI+pGwSc3MpzI0bhdkzUyCVkpc2t7Tpzs5vPXinrA/Crv
WADTCRB1/312NOGKzRvYzDKh0S/DHd1cXsCmgGlyes5MvPqvuaN2kUdICWpY+A8JlinpzNNIj2NL
n04eqViGkTT1SzmAJmStw55xbuVmiGDjaWDn168C3EtH+iQlOgy130w3q0vGSb0NYYbJwIiPuJwT
QrstFYZbWpnSBCJPR5LNtCa0e3I1LTAMP/iky3AVoGRb1rfyH9FdGv8MKVl2tT27/xcGHEGtxoPC
T++7E6QRybwuwmYrnTInYAl+zaKV2PjPkR/ru9FhV44FXDLbYWRKz1HwW9cvtiZ+vExIredLUNdu
7Q3gVZTFBBp90hRm1XW9eJtzTY926Hak/266sAp4GAQ7H3t5K3QC1hsewj3BVF4+iVjZXtXdysa+
eXUWxmHGMk6d+61raeX/4q1yJXfd6PEm0/Mb0DGHxAkhKomDJfyFkGvnxEjS1VTeJA7kjor+57Uj
VvOHmrMtLW3e0MdkGHA8yPMhcXDbSdgq5Zw8JTVwAkuwRsvR2wrqUq1gSzbsUqQAoa5xtAdLOrOa
wgwrWhll5Fh+MmQigraD4hWeKEm84cqlxgqSNg9vbG+ofbdSqIq5uX5O3woFD5cNfTP9/tM2xt9N
iWPdF8pearUjhSvCTwetAhdILQU+uYNMZwsRkyCJwCH2lMt8dTaHNE3bvliZNOQSlIBo79bPnjFz
iVJud7RSYMlErFnl7VWrddejOFrjcK6siMuhXDXonlSxeKTQwiVs8tbQHGHEuOzNbeCqKc9sZ68/
tdERgv3SdvKBBve4i9N2VLtKTS8LxCq9t/cPWTRP/5nD88lzHxRA4XmM+k05F9i5XDXpvlKYB82+
ROl1BWvfPZ5qGlbx2Wcmzn6gd5LdYxdUxKQT/ntbyASz3BuNnUHFzeWJ+lMaPnby3U0IiV/QpgdO
NT9xfKU4qT1mJ0FM32RmZc198glE2hQSbWi4dM/YX4ejxP34ST62H063/4R7lxLrdqxmQGpK9Oba
cG1JDG8Fwn0kFgcQXhuSawGYsSHeu5uKtBWZ8DQCKcu74wsahNWTHA+WYGRZvycoSo0Zp3SjzuXz
E3cVPeJ8uubm+TkHcZohlmW/hGdP9DFR/xM8huZIUusY/bpvsXWklCMSH1FIiVB2JbIoVyePqvO9
/JJS/AZlRoDmfGfDHaK81l95Nya22IrTSvzDr84AHg7EHBHxRWevvmx0/6Iwj63qyxf4sxj6gIdT
2qqEw0ockzTgIu18MoLpcubJbCNLzcOUN+aSznmUtXi50TgJcq3dFMfSFRiKJYYWu3JpxHXtjGr2
FQGKodjTz3tYvhhsFqIPS0rczqaHgkzAZN5qcTx7QfqNUg==
`pragma protect end_protected
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2020.2"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
R+TTV2BAhe9Ek8IveLCAIK+vyB2qa4TorazWyGCbrxCKkVhTBvAD6RqPeP/JqtRuh2zDPzraR9rT
gUyNSWD83A==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
XM2mYTm+gCT0AhW4S5p7IlzH34WHm/fa2tLSENK5xQp44huwLBqk+dBcYbe4GM+6wqA3pzoUNE9T
SluI3P6DpsOt14ispiaJSciB+VdlU+Q0e63sKyfq++TGO3CTW5OhLIxojUbYrTbdY4WbGkk4yG0Y
qGwauBBx1uBueCA2GC4=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M9U+BjMD5E96pT2zTDB1OSiHn8IS+G+aDNa3MIF/jeClLSPAOJwufjuzRcyAtwx0354Pb7AaFOwR
6CcoWPQM1dcUC6avyG/0PRrtZP/KpXS3/9PiWsaFHPYVLfqBMCUDoraXwfpfMxmOy8hD0iI6TtWc
j1xJUXVsbv+kqOeTUloYmwdRx/8cs46FvZfnFpiZXMFMsTsT9zvmCyNxiZefgFKT064BWsCkg2fa
W2IXperFJQzpE9mXVwGSjl6xDUp55esPyEPcDI4xy0T+q2KtBQj2Qn2DJRZ8DKAvjXNQmo/tbweh
l+RGgbFge035kxDZ/t5pFweR/SYowAMdG2yOwA==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
absLoVdCG0/WeiZ9M4NtAUjz+XnLze4vahkoVw40DL65GHoB/ikdBh+LyLQ7V3LckxaJp7Ihe1ow
2yXZZfuygvynBc+n/CI1EDwjo64cUTgVLg6gqySahs3D5Xkp8kFBBxARQmdoErJqqhefej6SXrxx
13OxNfq4vRGx7YG4l2M61gUhVtUX9poQdq5dxitmrLXD1kpdnUsj/YIpVBaLv/TBn9G44WiyRNIK
ojx9q2JyYKiWBfcBh+fpJV9PudrBUPMu8kvWsRizFr+r8Ya09D3o9iJUZ6FWOBiFsidvZNgmp1u/
nv56cp+qpaTesLtwmKiZbrhQtq6YXQvzPpDQXQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
t2oJ825g01R4DfbjT3g+VDPmL9PAyVC2t8Ozl94Xb2xucD77bNiPcvutyZFkA0lqWfRMp8Z3kkTE
OOo/FpGS3c1SP04/jMKLZD9E7DL6iVBRfxa3itPHxsSD0RAP4yPHw3yCiIsmB0q25x8+so3h/QOv
DKZh98m5ku9UnG+pY6c=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
koDeaCPE+GNu9rMKu+nnX8UvNKbOa7mKCRwRUXCmZNo0yL7JuxnKQiStr89+6Ws9bOIbY8P6XKLC
WoSokcQl2MIZuh7gUJ+LQSPTB9HIkHPuGGPibAaiYY3e/6TBvv0+QG5gTvuf18Nz0UQyxRzNBFY7
2e0fNw+zoh4XJubbVaqqBBqTNyIM/naqx2G+DBhvJF/RlcpsJUe2eVt+uttis5ukRD1ndenp7rvA
+Ub6MDtoxunfFJsXEQ8QZkuZiT5XfcmJdkquGywSafJqKksYNJZpGleQnak/ePqKq8cYIbfpqOo1
MlqTFX2khe/WU/cqsW+5jXmRAgWueTOvg5hW2A==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wZaMVki09KtetQFaQKbOEpc8bkgxHSc8zyuzh+dwZ44uN2hbx3K7ITnC8dDkn3EMZGwk7C0u4eBt
eru14n5jQ1LfuUg4cKuwRNAgFxc7GaymqPYSRK9OQZHWZ+w6Alh4X9YWb6UVcsv4sCJA8YT9QeZ2
8PJYA3L+OY2t8Dcx3JcdLeVgMWDrP/zfpXyfMdPpwgBSSCqJHFsYdlG06onoQq2DDJ/SpC0W2oHU
JJAOTss7Cf3giWx2XTrorU5k4KbClTaEv4QAsogatkMf+oa9OfJQg5b7OUNbNqSzTV2IvRXtKIBC
N3mFkAtau93JXZzbow8bF+Y708RmUyIR5AX9og==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2020_08", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gidhQdKtgCKZpycO58SKONz/x64JxoYiDvm7CY7FhAgR8N3zqVR49qh/d9ImLGjAjXhz9ISSvhiE
1TpzIsqbVIoSEHhHCsw8fW3eNfjSKG9+5c0qMghoZBwnf9txWcso6wczPV8wSYfFgOnId+/H4w2u
MtSdrp2j2HeGCN7hmduXDeRIcLF+ekxNNZVk0wscD3yxYdFDWscebLgM1N+Cx8uwWvloVVe1fNSl
IBecuxue/tBnCdqw10D1fC8gGorhdNUhO2bTYqZL/+voIIAXkux7Z0BGx6B2uSJYuZ0j2LS23yyk
r0QDrL3YOpbEPBbFhTy9LQz59rkITBRhVeBqVg==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lv7TtlI9EkMH+4ifu40NSGcF5VLP+fQr0uBXzvHjgpvggoEPEBlbTyXFtewlIbLNuHO4GjqSxFa3
oGjcKGgjJ4JKEHh9NZ/42sDCCnN1TS1zrfhPhpg3aJ3aGsOq5GxB6oAuNGvsTC7HgKk9lvgZfAiC
9ubfhd8fCUCrbS2jYuGLkpNxtwRxEbxLfMa6l2yusSJt8g6sfH0aGGBJWZjKnUZ1SyA1DmzZW3ox
o1AE17uwesEX5+JGPaqlsN+jLpbHhpv24GF4NS806LjJrXOO9qXbZScc78Z/R2xMBhLYAC0AHR8o
o8hlz9kYq3NSGSCdEMOcxNjVxDMYBrdZ+Lc+ag==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
m8XXt9Pe1EguCzqMGHKW05oadT3uwPd+bPQvXCm7xF4Bhh4kGkY8eYx1upB26FQxSlbB6UgkuUhK
QAxUcZhZp8Qiwp7iGIqFAnXJLcxZJMj2j8PtNO8B7M1zQwQEQ854eLL7hFxO7vc2RvBpDXHsrUOx
UPEwpDq9mSkH8Wa7wg28C35rsx0N+9HkDJwNXYfhMaWgxdEK/6G7kxnIYbKgqZHNQRf3xgkxzC0H
otYE50Xy1WiX8Kf9iPCStnS38cUBxovPMTRD9Ho5R45uQXqjslrkNnmQZAHIhzz/Mblr0+Ue9m4h
Q8VPPKx0knNT1WYbhsUe6OI8d584+JP0xND4HA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Vl1Ni3QYHIPc0cLoQ/IBFUaVpFuoibCHCjZDw6lcODwAfKtULoyZKTrrZHKnkmHViy0B27lCipuW
tZzkWjXwfddykQ4eY1ux4WkDQazXXXjQVq/R+txJ66RnvCgDk709dxIcQDBm60ilMaalg23TmZoz
VOBXJ3at1FJdRxQFvixrj8BE6o6WHOzHdhkYAElIP3kszK5KXzm4DCMrlqKsOj24hF5m2Ocs7hEL
H11sRL8K9zL5q+XTiRmQzPNiZRc71TRJ/lPJGFx1JvQxusACRX2Be8Py4YnL3mlX6LSoxHTOFoBr
XdAvn/ql2J4wLyEDqcyVg8g/H8QwZkNBzQoIJw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 12944)
`pragma protect data_block
cpMtYfVFNJvT1jypAShq4ezuEoNltxxMmxQ2h5oMp4IPoFgNS59RIy9qAuvU/932KHLNhR+KcE5I
4by2DCjpSSpsMkh5urqs7VPfS17qqbIE5r/I0CHEw6kNddjkIKFtVUWY+JKwDvYUixIF7/BKBkBt
TFs27gli3cdh+E1XNWYnUK06kCoIbl+3WHGTh7sChUHyo+opD4n82XhBZw45jlj5jVy4nP1E2SMt
sdBBGkvC/33U9RmeyDo9lXTkaUHOdy64mlVM6i0Uqf7nWqm1iUKzmjq//694OZcd4x7+Tr9/MmKL
TBBvJROMIyeNpP/DalOslsiYfbdqMQBXIlYhBZLvc1G17eHW3X13GuqTk3ZstAQq5h5LZdJjRUh/
Khf9hIkmxXqQOfXpeVotgHR07De9t9C6SBX1viebbAYQUJEYXIxzR82+uVhYubrrcRy+DDZRBR5E
0EgasrsTy80YpD+uceka6rzgTbGi8SEaLG/tKQ+sUEsbRAbOWrWnyJENOv+9mB1RGplHldDJixoT
wgTPJpBrPpvgzoyMgjQBho1XlSeTPeJBrthAN+CAwqdIcyr2tkI7tnjRo9brSxPMVkNSOCdJDnDy
raUDSj1Lx4CzDHkfED/ba2OOweAd15gpDHuV/Gz+cvIH2ZSzWAqPlpMTqjQ96m7yhWUCBqviQl7u
SQbVhtHEd2xxJydmiNDLJbbDpTCauPgfZl7U4V9ezxUNJLPzOdOtb0+5UuDWS9nm5kYsG+S2fdf6
45ubpncdW86AH/HNiUTT8cp/6W8K2HGs+nU3XR9paPIFfDHqYcOu6CzDfX/9YzTMPbpfSXDpRbCC
ieXPpSs6/wPmRk/JALYrjzqndj4RZ70LPvJu+0cxlQDQDWdxT33W2XzM7EOGXVvLwOz+Anw2F0zb
FzBdAZfmiX54dDcjBlPyRyG/V41XP2CaGcEef8FhS5Wsdmvd9DetaZv4gXaMeGvCIrnEhs2VO/AU
GVRH55iQmP9hSzOyyKjeNoE7+PDAiiPH+sl8xVKE2Xnfb1ffX35T4p7cpNy9qRvG+x7IFtSWQcPx
/VRGIE4kiV4XWhXIVm6rvRWOoY5HJVIn6wBv3oS7g8dbEkCtj/hK6MjNZFxLSmOhvzc7Q6VIhJDx
1yVmUYdPel+Er6nhk4c4vYs/ZXIB9iyXkiATtAoQTG0UdpT3IKuGCz71pLvCY7Su5gzGHda6A9bw
xhZyD4IWsHKa7gBLZaUP9Oo6lEi0sDf2utQEBBIRGHkrO6UDzdQL5cZKS8UDvCdl5JmSZHSZg4pi
ix0umv+2C5a3rjsbdW0DpsS0AXofBKE1vYQdImEjGrpDEd2emhuW2h7NnKHD1GPhgZv0JtNt/LW+
tId6YFpxNr2Gsol8ahNhSXIFKaZWxtoZvS2ct58UeGzxPVq/a7d8bQcD7rB7AlZ5blUy9/9T+YQl
HYOe7M6SyLCBnNao5s0EFnKZ76OeD3eBmUJzOoxPjwQNNrGN5ml3sAAGyC8/SmREDm4NrR3S1Lae
ebpmk4FxfCCrDAIYp7joVOpXN0fmSAPpPkqnmPcAq3JXdsF4j5EZiwZ0Z74lwuDBhdISG8kYSL27
5tVE2rY83KCqGK3/varwJCndoORc94yEvMZLKKY57+OInadm83/pFlWVgSj1ah8kwghnaYpp/uuk
1KERACYKOOXcJdwVZ7umgdPdad5dga42vg8G3bU05pAz4pWCtmm4yw/ZrSxXteCVZiIWF5su+s3w
oIARYLTe/HtLBhaw+YhU3IjCsFvrM+BZVon5J6Af8NKJ/1GExYeXexYcOrHU6TiaNviSnbIm3Vvk
y9GjdZ4b1+jDLSmH8kSbYFCCgQCBthMtHI5qk6nS0uyBd/fQFbKO5FJ0qetogeTsKlWnXwMPEVJI
Mu/uNAjCu2S7vNu/MbekfYnXj4dC38xOfBnK01VYeTrf3g3JTPiRezh+aUZ5ECAtPiTnJDYAnjO/
kduX3QriYPSiMxBYezAYW2zspQ22UreWM8X3clOtO1SyFUklVhsTP2smg3916NlcrjeX1Q5S4v8d
lhA3jkAhBStRRrYIWs1csKqgNAD5lPXAs8TKQckfk02NFY00DvnZSrS2fZgXr57tpmE5j0IFLrC4
YjgoZxDEzAD4HLIAe3bpYt4iKXLx0scM3ELaQunPP5o6UQYPo2RqKKDQln3gTCs0Ighja2YVlTeS
Y3/5CSyIgp3aem0oQH3Eoc074a0Uw1XhvNCX5gmM3HmbIjBY8LoUwCg8Y/tqjAS9xKv349ivSw0z
7wttuDzKEAVg+USa9PZyBaC5BvOmJFsI37wvEOiIqO8JbbC27BGULWeCCUzBw2L9mYp2hR/tAyet
59wSWm2waD6G2CUBGEHVkD2nT7iXAuBWvZWJ8U/VAarCKuqVEXCCT3KkRB2ll1bT74T7Z6vBdrbC
+NQN4LDnHbbAgQk5rsjT2xP5e7r8irF1Y1GSe9FVVCyt7SUnzWHaz3pJv8PvjUXjtwFzeSg0fCrS
0zU+EylHlyRlZixLis6MAqzH30s/nf0SreeGQaJHPs4LHRnFF0asu7WH9Ze+0TmQ2bpT6OiGr9JF
jOB53sn2yZERE2txNg9dGLzwr42sX3TdbgmZbsbiIepM+yvXZLIZXggN4msGT52F4fbsxN83j7yj
qIZRtUSLmv70UQgPIZFAjl5UyeliV5/UP7OkvV5kFxciGjZqj0NDhhcNVW7U5oeVMAjD1u2k8a6d
GHs3EeHmJjbjca3PNs3z48r9xUXDHmkxEs5xClxAN33xrDr2cHdQbweB4/MAIdt1ppgRP9TB/aJw
no9Bes7Z1g129dEgSCR9stFEmRvhbHP34W0MYIqUoZ2DkC1+5a6KJ9FoVSkJB/2wMet0wlwFaUBZ
Hisol+pvI+8SLNygM9USgnr9AVPPGpbVaBNpE/XW0OtM/Kcz1/8ZglczfubnGG2JOR6ng6rEsuXH
FLdK2kABy1r4/Niz5XEq15DKZ6IcroG1Ra3l3wmWPEYF+gi92ExkZBVEF8NAM/mI6znvSsbGF/qg
G3U4oHIKPiUP4FvWXmVaa9j308uXeB3WVi+kesgUhMJxDDecP01OhcbIiQXwgSHDCGnZv7mjRvAB
YMVwhiyZH7kTdE6XldPgNafXi66UcpULUtkTQLKRai+tv9gBZn9BxQE7r1B9FXP9EeZUNULDaRZP
Kpo6VQzFMDON9AF6/6AjO/OxE2bjV/Fv+pql9Rf0C/22Wy5910dWXIlkcS567bm5g+9p7OenkbMx
3hkXAS9KKc3YF4wZAa90X+SaOFJcs4kHFvBJ6kQgeKwYrktcnH2PmTm10zn//TVUzeliFnJrB5nN
SkvYg7rPXfM/h639BIUyRC3eO0a4Xhb38xiHcIBBmhTTFPWzKMPcSH4+EHwJxSZrdC2JpE9I+oob
rnhTGI2Lmt+vamIWTwM/Pb7fYooKJNUmIq/S2sxSgQNfkbsNU7har8SN3FVW40IIU4O/PrxBLCK8
pnw46iGikgsCiji9uvxB+vQ7dJvyY3zYIPJkrySAzMDYux3z6WF9qSOjrmaEgNjum5VrPO5Ptmv0
PyvsCDWjXvgLQ1VY9JCsp4obD4NcWXR4bPH3TMkFJtCl8RmStEuD44yoWVfPEF7pA0bKDz3OLSLr
gmbrH/TWx41HfJQ4N46HwtLPp6h5BU8eoR7Nfp/MsknomHET+zlM8oZ3Ci6+ijsn/bf7HX6Q7uhP
i1n5UdwEImnwIDYaVcmjdLFZSqBXA/6u5ETx1cOsUEBG40HFI2B0ftZpdDAXunBskOaaNR0L9wCS
CnB/yPzaEFf+dLW9C05lwSZC7vCa3WKrPA7uCp3f90t4dDVoq0Zwwt5daAJYiIrQ+vKiqQs+wC5z
anN/XMk58TPBJbtk2OMWOMokSKi9bl3vs0ZOiTwe+ujNFMZpmy/eHAq2iDWLxWSun7rXN8lYFw+F
lCgsqY8xIcbNwB8hn/S9z+qZmrDE7K70v0NaJiGiJbf0x/dAXEowuOqJS1y9j1DazQNekvH4p84M
fSh/meiT0D3Am0vEmuyNeSsFrSbwp9tIBpuQ0QxTxwmVVRxeO7K7iOq5HCzLznaQ5zNTijKbmeNx
mhuRzfVBrX9e98LWn9cbf9wZUa74V6VCF0tAvi3tPwWNWOVi9VtvKcrhiZf+0piFc3Dt617MlXSJ
y6Y6VHGd3dIR1G7/uuU01MgJNC9VI9qvUd6mLwg7jkqyCeXe1W4CXjdz59HhXqpk6KLct9PHfNCQ
gWp1NMeXhFECL16xCl7ttgJXbYt4Hx7jcLhnYvW2ZSETT5cUZ/UMt/U7RXWQASapvKhJBSIA1yr3
U8/HXdhFPY+0ivtvHmgkM6CLr5mCRbmAL1Pk7vokEeq2fVSAiyk2D99yn2dgZSpZsdtQVxOI6UJm
/ajr8H7oI2VikcUsmcNsGt6KJMNq67EojTS7c1YbcZG7pfnd7VxXBRlHsOamIhmV2iSX6ozsB+Io
cNYNybz3jDJv+5fMoZGTZ0opTJwHhyV1vxQ/TJaK0fVJcKtNJZIy6B9TyyuoinSUTElb+rQeIb++
osYLOIVZ6HmuhsiKuEWVCt2gXE2C0HnQcbW6A1urF90ZRQDI6PZS11pQjliELMPW0UX5VmYefZz7
f4+8b1sWvg8C+dowdvRyfOF1klkUPLfugNZz5pZID+I6drCzIY4OU6onTb2UxD9Z3qx9Dr8R38/x
TZa0mU4f/eki60PKpxCtNWiNUKjtMrl0A0zX5iLADLkioV7z4Gg/Vsp3QBqohJbE1OzjTompdtZT
7zlyL1IltnJU9jOoMuHcCHj0ZVi2aR3S4QbQF4EwJL+aewxBt4gif0MImC81+QdCtZ+KLqBqrVFc
WkF1zIiCJ9/cqXvF/aPZyzOmADkWc+XAwnrQwQFJwaESC0FxSSuw5LzFXsfmoV09xiLjyLkiMhyH
5EI1Qj7avBtdcefFMDolTuGYBARg7mvzGpp40RLQzZYkodVNaFnj27vG5cDfYTb+lWBlvRYqvf/l
7byOvJRxAcn5N5+47jcTD6CTo1HqC5qp/92oNMn/hcICCPL6jiZgeMUJYwxslWa9jF6FfVN7bAVn
jSSLlzvUJkkz2sYSyGWRTppoSxaVKVU1pFrigTMSZbNAk1iZV6DtnebitXp28cqZdbyiXm4BR+S5
n3KxvGxoY4sj80U+zE0o8ZZaspWsCkHrw2FJJhwhKhEfmaqPnQ3GgTIn/pSP//PVB4HZcLn5kyi9
6pn55fGueP1wWcoGxlW6aXkumqBpQKq7YskbTm9cEJZsmijLTfKNzd9nukmvwesmqA6yI7veJh1B
nABTRI2ZwbKCKmK9lq9Td8xiI7TDmzyPZbyczNchJUuWcglDh+O7VZSKiDGWt0FDkqGSO4PNrNg5
fy9oFlOXYV00J81ErGaR7mOuNgOHohq+M5YAQ9sE8u1lQ2IOGImyaN4tSE4R0S6HOnI4f0iWcOtL
D2ieYfy+5WqiL4FzE11BLqg44uG7NtVyLl4gmIrkOPZ97+2IEISETD5nBX68broNBFmR06jtwk5P
XY60cERqGCZ1Q1EGIF7avPgiR1PQzdNwTA+G5Q+atGjfsXWMG55clc31sf/EFG/d71vZYqn3TRA3
GlFsK1azKxJ2SqPl3EvD0P3d68CSWKOJv0WWyoZkYV9dAGhY9D5Q+gNHeNTohE+CgLwE+2jgPPHK
nsCMWT7DbBJ6AudfLXjejIi09v07MevIlNQXudSfO8WpQziowbFz/Al0Q8+XuqWTXZKMWla/PFkm
DH4HnoTZWiuawfS4E4hL5gTONy6x/DJ0TyM515nbppwoW0cF7SmUkXdJe44o2riIBvYhZp4kXwUy
gt2asIHm0IhBptM11dzQKNAzwdL0RDaec3veyuI2WHOMvwS9t0IhajMbvj0Itk4Sb09DeenIQAY1
7rX9545WvoDdcNABUwTyvSAn28Gg0Z0DI5tNxdcBTWh5tklmKF4yS1X+DnsYYXaT2dxCjF/uOmwN
D9VttZ8lH9+pgIkTALXDV1JwrAhSSCYUc/SVleDwZ//yJbVXtB9NVo0xrJSolaTNcrEQ1QYdIpt1
Xs1SJMRjv0gScBTPVUpegJi7RD8aXM/mbUcoxBHGvJS+9iHPKPpzQ0GfKCeXZjf0sldqZ7+aYlI/
f4+/m4t5vkxWsFD1EISlDAKXvKJuVT8uE6D8xuwO28HMw29XoZOgTD7j1E11ctiyW0fYPXIDAbeC
2mPsn5C2QyKSrgd6TV8xf9LWOv5aH7x4IjpODSXzdjyL/E/0g2EM1Mcc5Klbym1HsczcIqES9cgH
/Cn/O5K9JPPdLZ26pjW+9xMyTbU9l0L7FAToE89JKj/cqSIxOvudutGVpL/RbMb0fkcA/r8U5ZLk
jo4+RCDqZcVI24i6lvhxWjjHz/t6Wezos+dCc4kvhjumzk2QCC/IhqPNIpIwmW4ZMDQmZssDJsAr
lQxvabQS2LZA9z6wCtOMpPVWTFNUXaZb3bStRJr7qzYMmBQcj1rNd0TjMwZD6NmVLXLC/fmP3l4p
XKhvEhztoOXglCOhVwQBi65mW09yE5RANRrqpIz5knwc2Pdbh3/3JqWX+jljby3VtYmiLkCZRyJ0
ZX9UewA4xfSTktu1agbqydLHTbeeI+31gShODSjE3TWgfdimYbGVcUynoLLad1CjdivpqLEx3bfb
N6QbiniumpWbo7fERpDXuA6fOTqNRlwHE3oHdNyQuAeHjDxY4KeLE5jdLiNrgdsZvZkWCvLLB4A0
D5HCm4SUCU5flA6PM5KwWsqYhPwJ+B8np15gM5rxpOLbvoqp0FmxsjspY7LRo9wc+CRldBQ8POB9
HyjbN/wlSpWC6LshhnUiOKIZz0836+HLmxL9K0I/H4Ysbmva7ydcLEwrMo5sgUFXX+3xR1Gipb0r
o+lWP/+diG+kpDG7pTweDZKk5VdAQoVJLln+ZdSQh09/hrN5rPELEO+poC8rEy1PSc5yrRjLF9gY
X65l3oBkjpjM/invZDGylqY4ylXKx6u4WgiqorzZPpeqPv4+iTCyblr7ifXHH9oQHv17TBHGgnek
KPjGuNfR/759x5NuTBGPe6cvopKWUMUc30joSMUef9QYJmlJFQ2jIy4h+IwiIfS0yCu9zvM9aZDW
P0gtv7xXvWFLPYPwi8L8Dt+lJeDsMJtpCMrRVWSuVvqG2tFU3z17EJ1jTfrrrsEwBbRuGILbgbWX
MfqUbmwLWrBDsSMzmzz5BSMHwstJy0emZpuxbq0c1wSdR77hNZUEVo+qUpuSvXYme+g1D5uKswdN
x6XcqSz/NxwZZ7ADuT1Catl9MWOmfaC+38QA6QffWW2to6UOK/E/LHN1MAhwStIPuM6XCRhTTD35
xOudqOoDT0UruEu8pReEqTHhBcgOwLkzeKoq163mRzoDx4uMJvwHyf9pb5Qp4Ajq1CpyYYX9s9i+
dY4upaF5dXaJQaWhG0AAo2F7v1eXm2OPyGMRAzrCr/RgkuQYdMRDSOabIbkB8WbmJpcmj7TdcpJy
SPS8fBJh4suK03ibewO0gHwmJIHYEMlr37Nyoz9OOf4/ZYsbNBDIqeGEbz/wBbiDVsaG7jPtS/UV
uxer1qw9Hho66X5Cz2PxYDxDdFNh/lyCop3vuFajLDL29blHoeX+qkWPbzEdSiMhTT1ATMPgMlDc
3pH+NLmcjH2ypE4Cb7c6pH/mxIdUH0PtD706z7RGUF3hHLsK///z6Va6MCvKpPQ4XMTcfk4ahNet
uCEKQuV4qLnhqOd034C0j3HkPLcotzJDGv8lp1Vfs0m1xj0kEYL5V4/Dra6yDPxcqnd4S5IFiQv0
UHppv0OvEwLDZF76csLx1I7oQKN5CtjdmgJ3h4raw7bMOxR9DRq1axxB01oaBMHxG03b458yq9bx
IX/wN9r6qElcXwOD+puGB0hJih4XNFhNgKqI9xQDjIpFGyyQXNXZXZHa7FuUEolLM9tKpNqEKOjy
toRDRVKNfWapVVjUBG7jYs8KL4DF5wqx4bpYb7Ln1Eedz2ejOlJhGl9xbTTzV4AimQnY/sla8EeV
GLGy+zgjYqJAfg8XsdFRSMSKn/ztWBByOdvSdc4+TtDFiCdwuX2t9jRl0F08wfpv5vlkTNXL0WYa
OuNUTrCY8XL1VScmVtTHJgS9RgJUQZNoZg8lWYtGCNPCspw08z2zfYk2gQXDBMO+9TAhQJevtu1L
mVUeZ4JxxCYPiUOtb6p/+0MOldndDTsmQpsCR4G35CABHXz7WbzXhypxB74+wNQTgd3QcrlnxvWo
InQauWveOQZHNL6+zcuIouHoJzBu3msQxgUjqJ+hcwMtOWRA9Kzz52okXDQDN3qr3BPZIWFkCDFm
mimnf6GGE/FUtz7Ag3xHf9eymCwhhj3IvHZyEICyenbGabW0qtKwo86OFvt3U0/qgFnduBkkPEwo
BBvGpowxd7nIoc+hX1yXDGYJvo7MkyO9CbGkIBkfkpplZ8ClNoBPB5lfqbPFje18lrSieMv7RE+t
ugU5C7XhoKpf3tXmgtjR7SQJZ9pjZhq30y0/TheQWjAlSyX21LSqfdmoSDrhlcwV2t4WhusSe9LH
QwFejBSOpTSLEmG70758R5aptz66Qk+3gRdKiGN8wqrqS7BEasU10pS5ocdA1Z/nCUlDYObtJ962
zBM/ClVS8KgdHVHrNT8muIbcd/KIwadNgosyO6MGyzujI6CeMNz2WVNWq1TkH23Op7K+dHgogsgZ
SoL977JRQIbv+exOhNUJbMxEXMqOy1101hymfU4Pu7iVUSNMzCJ8GXXDBViOvt7b2Ck98eUm4VjD
9reAK9D4BQlWmpBeOLpMnXGL20PXax+QTssDelDjrJSCQhsNRSKCP9MVsFlGbZZ+/B5W1lmmAXAO
XhlR3XuINmgTnI4R/bRYoog9TVWvfyDPasn3pwcgyDdLgKJ0Z5v4j93xAbrXyw/cYxUQsPxA/4Bj
hBSt7FJPUpLNcWu8cRsay8TJH3OGZgstqObaFQjMrXEwR9qOPWahvebNsf9jWWjw6SgMrIMzXrLk
WIUNYZwXFC6yU8UUyc3JtRc7m8LPWpKOVn9NL6f2NfpakYZZb8OnOY2NFS5RDDRDeum3ufm/7U7h
W42czCjm0dlO2qNSJ8zP+Hye4jHW/ACB5qjH16WEX/pMWu7AxCASpSmTC4E8tx7YL7gYbZ1JaEc4
HUV0zE69Bzl3ifP78Pq8Je3PUcBy3gmHlpgj23MKAqoFmM4y+Tikhp8+FAdDo6XZmy30oWpUlu+0
UBQy9SJzZ56IU1MuXc/A2t0LY7CEefDAsfzr+Hkw29L/4hQK07qy6dy4YrwPeBBzMkAIIYOEKtE7
TNAvGqjR7fXp8fWXOlzShz1Q5Pjui7/NMA6ctL9u3S4VJ9AIN/5nlsXY6ZXoNdXuhjM9bIvcFJMf
ZwopIDAXjKnqOxwYccbF2mDect36xym2wMz0C7HW9mK5w3NqW7V/4Xc50CWTJUESA0dopSepjzS9
lZ/CJw1heP1KhO8AKAgPyZQWN67mThbsK1DqmsiL8siIofYrkRAFweYt5ogE+HKCjxrKWZU1T1oN
RiXNtesyD4I+NGhbI5S5vaJVFYgucNn1+s8SAAG5of2ud5hLeyXaPL6/xXyzrv+cNWqbKnqkKR4K
CDly/TPsRA998RW9aANdGlOgfxF99lx5FL6ShUewgD6oC3D5o3UeReGaJYHOCFHw7rjdlz50cZfl
srtJVz6VEb2TQR0wa1MqP5tN1fdkmWyB85LwbozLbjtK8IkkyyMgpdVvt8fnDBnXlfRGvyTl6Cym
WEMfxq1aNJUeV+m0f+Egkai/IaxPodcROZb53/2isAVus5Xqqpne5kKB8B58xvfrIrSdMQ180p6T
ppF0Vl6V8SXcQMXb4ewslnZ+13wlBkgQFMKmuw24vatuD896Uh6FAWsk2ERJORvqk8aLiZP/lLu6
jL0U+fYRtH/iHc3Gbdsb4kmgujg+xfentpysuNol/r4wG64SRBL330LNmgTMBxqjgkUMERu3XtNQ
DciHoBp+4W7NtqMpa6XdZzZrbt3tnSN53cUSpd/1j2b14etORtrQ6QG7jDEOKBr9Lp9vtAuaHjZu
PAIrmy46GJvtQWa2DDRRPqLvd0nR6X/Bxe3hKu4WAuAPoQECCSrFL9xsqdWTPHHrp+3e5jrVZzGc
821u0WTBa5xsbJBIZ5v4Hn4anE89jPL9KHW4mJi5uqKR1yhpc6F43VAqjZ68xMlxfUsjsZ+585DZ
DHonsiGDj+NWUDBFoblZAxTaAAyVvYthItwzfZMQuLrIUYgFKLGoLanBEAzFnsdbwmt3lhX56WPu
gC++rX10CLINkRgDJeYTrQ2RzI+4YvR/FzOZvZ5SDLNRWVIxe8vTbNVwTBjxXWs4UPu8ITEucvaP
z93PH3l9d7YReM7VizVaoS97c4oQhDgrTyjCS4au+8tuyctTYjZ8VyoY0U9FR3RzfTv0m6oEEjGO
tDFosZ++DKtihIvdf4UYnzvXHKnzcib6VpaebCxweztJ0i6x9+AAgMUpu1kXZZwUvUx7wyl1fYaE
H/zyqUr+OjHtEc4f7mVv+UhSk40GaTaeGPVm+RHG6aMWsbJ2/WPA6S6oSbrrrdB7yCO8Qhr21gW1
UkvNassQr5C2YE3UqBo3FIQ+vkOO6ac3uXk0fyb8h3O7CJGmwa33IEBPFTAotnynsfBTmS3VkJfA
tOplv8Z3j/hADIZ7p4VCGecWjl8udnoqW4NV0Vze7sDCibPfUpZMy7bEp7gFFe3jX7RFjPuGj90Q
u1XRGHFInATb68nSJsjuXzTUEgJ69J3maIgxktmYHrqbFnJZhkcEgg80mvUEm6SGWuBsDmOtwhCh
dt6zCMBxNBQBgpaHqMG9AHVcWjkNh6J4750w5himYUFB1maP9CItwro9t5TKLQUfZ2SARV6REAGi
ifhnQyuxShg504PXk54rAXOsDqMVVzV29eHiuqBgqFZteOjSpB8Hug+xaAJ1xgc6oBIwhw/1bR7E
S8szcPb7fQ4Lvm/qvTYdnEmL7yQOiNrDuR5bo67R4oOb1G6IorQEv2p1et4FIDla/xsR8m2+g5FO
3NWbIh/VaFIiKCA8dPqWbozuoZiV84qKeGmUaHCjpuNGcJALyR9kSGh4WJQ90J1vmlQAyCJM6ulG
oBSS77cg3HsY4qqJ+REQTTzXR/JtNWqbDPbqbicZ1/gb9C31Z1cgJ4ySVnv2Oa9abHvhAhiMWC9l
4NXGzcHhNgREMJorCVw23ay/gG7bDT+zmqsoZzIyAY4Vvx2UreL2cTNV/7/b2hmXFFqYsXuIUCXN
dMHK5EBUZZjepDYkueE+A6UH0L0U867ir6ggXDiPUJ6WT8NE8yP+kt8LSyfhyDIEzdc8ZJOVnesC
zr7T8wosxPMQd82761hE9WurlCTneYD+2A2DQY0lK7OmwuXY2Cgh8KWEx9I2MvjkJYH7VDKjNvUq
7gh4sW8eKnw07zqFyhynOQSQG/1OS7YkxagJ3nOAm0oIbIQE4HHMfzzKd0M31xHyefGxGh/c+nAr
S8pnwbMBaM2Q9nGTQKDwSj0PWJtguJBeh5bdW2g//2XnpxxeTOHUpRknxNtGjJsvSLwHdXSHqL2H
EUP+ExB7yprGm3tFy393xLLzz6OdrFjLy1THYZuuQS9CaUWztKWFsKP95rJGLcCuxtgXw80DVm/N
UDSkwS+vH0rSnspctNpZBVhrafejhU9H4RtpAC5XuIH+FWzilJc6/2qh7P5XatjK75eQXEbXVn3g
RYq3Yn9hW9zU+UtNgN+VIHPJGMgBP/HjMjLSOzkxVtU14dh7hyJfPIWD4COkF3xUwA63l0z5bE7E
IHyg3xEJRa3jnc4LDekFtgHbi3Xehpb/L6luAKnAwz+0Km1/AAWW4rVj3HwrtBjClPlDbFHszdFj
RSITZViFF2TeafPzHbFb4uuNKFdc/gXXNX8xmw/bB1LcyR6HCo2aHsczhu/A9CzIIqrfI1Rq2/Rb
+ZEYOFQGe9LX2nwiNaabDuqdxGBEzNYuwX85he96jIye7WOVxNI68fe6ucprBW2mw1C5PejuSeTl
QrRYEbUvyO5zbz/khaqe9ixXCbpIx1DkTdi5a16WMAcvG6voNQ5bYwcdSWW+T5Vd12JywQrz30ZC
wvaXhX5+EtFoAR9ft2ef5Ij5XINc5KVn7apzglnVSUbrOxRSoZSGxfKm8kXAEqVNPyRCjhKSDwPG
2gc9n9G33xrV+zUsDQnjUGvcCOaElF/6Ghn3t22GKBnWoaWmxpaAvuuUWhHNkQuqIFsK1kwZVMbw
0k7eANzie8r/dI1aMmllw0uhhA4dmJoENylXmiASsHv3NaqzvsM27dzTYi2KCg6TXXYJhCDwtvUN
kfFrYj6uhahThfBbmAKr7bPbZdZsUp0mNs0HzO5JX1IXaTAmRJFUEBirgnr8huCle8CSfSOws3Ys
SeMPVYhufHO+x8p5W6CKQ1JwOlexsPBT0uFReIYh/yIoKPgiA4blsFdbTx57UV/i2Xz/O6TyF9k1
wUXvouzjTrYWTjQGUp5EoQXNaCdObjRQTxBzOAQRsd0TAgR80gma+sotZCbnA3YIHupmYlU9Oqlu
tsZ31d9FF0LcowBSoUtwTrED6JOfLyGmXY3EGWsmHvQLuHcd1KCAcJTxoD2uFGAtbFw915SmV0z7
OKz2WvGh05LKv+FO9QVjxbleCK8Mgh27FFlkyy74iyJJCINtx2E46D3RH1SXjoseh3uv+A+HXioe
NOroZIPVPcO5P0nwmiu5tYp0t/nqmY/c9hX8jbXspqYHZ+wtgTbXefa4PggsujJn9lPRaM/yzXCW
ZGyAG1EuJ/xPrTWXP8mSuVGQW+rOu4cf6uLePepXRhM79SMe+Yn+a5DvEz17WKS5A/thHWzb8ZHr
Hfkh9v3gYdwsTyhSzP3lhKkvA3nP887+L63OJl21YbydXhH5nbx6/Pk3Ul+XjCKk/8G87Acntr6f
lH+fhLUZZ6GbEQebtQD3X6Pa/WiDcm/aMkQUpIIwKk7rJPCxNjn9xoPdw73+4CFiaLBTStujng47
bXHdUOgfLRl6TUObtXmQeZ6+Viy3ggR3oczN0A9cdzJTEPr9sBnZghPhZH1PGbPnsrZNFXz+CI3q
bPcNQed0Y+1kSg2tqPOQ70ay4IX4EhjDO3HMXzeVOLbrHo7wvYkZawO/wqWF7wCedmbq97Aq3Ea+
5UDQWu4zg0XHYVNcwhq98VQ/v/bxbRMEUZz8y/fucuVB9EUJqb55R3XjbYcicl6RyDNINK5bHkji
ohXBeAtRc66vJ7hCOUPzsLUXkxtq1FFQn/kR1gCfQY312o/bjaCD1HdzVn6CTKZLDlTTJ9/ps4WC
ZOq7IWQCMWdQgDNJBqRjvF3Po8OVeupj1Z8hePWO545dyqenvqqedswKZkd5EAx1snVXNXDrhZ3O
WcMJVat4BEh72RRnEWDQ0bN646AfvXRS+6cnC5fyScIMGQvb0SZjbJxc67sh3wWqSEdokUBZQvfN
3b0GJwGUEPA8cJCkKNefSGzH4ubGj3AM7GyThfHFJAq9vaOaPfm5nBAAw04TLYzapQr8Wb0vaCJS
/j60fxg1bH80hxbeyexn4BJtB1RspJH94Bs7dMydgnYKPlE7LZ2YEqB7lMyOh0zqXmp49mexjKxA
Mi3O6RnC87ccAcbqB/664OH67y5t3zycsOrvNhLSSXa5xH4YXJUkwIeRESzBL8C8VyFmQBu7jXRi
aX91ZUhcHJiyYyVj7f0mvwMvSR6Ue9ZWt5NcEt5C8+kvPxuj9d0v2wuuzkeeSSIgmWclImX7zKvY
HlMwdfrpSyJgHkmyE9k76Ocp8QquE1BTs5aPGI//nLFKhn2PCNOulNPuBbt4P0XpZVARUjU22fVY
TJEv6CVSq0v4CoocIo48v8wOfIoIem6mbT/T54Fbh+fdpRRxlmhBNSKi9skwB+5MOBkTA5vilwcR
IZQg9oEvH+B1onu6KQWuGZRAssFb299lUpDt02+x+U2ec2V0Fi2cht43QRAOXD/aMoJ53PnvGJED
f0pyO8AHLn02jpjv+6BrYmxhKemFSb63YvQaNFloItW7BDZnfe2c2S1EZezyCH/b/gy8QYWLF03+
jrR39rhPPMJOSmS9ojRDXwbhPgwMi/7hDJtxcooCvRd0G69a8Uy0YfVkin4Wv/RTetLnfGsudqgw
yyaiHri9uv331OxEcLpg9kDDQbRAJtfePpztnysdNCZ2kqv0hA9eco73G+0KBU0qRjwvLMwE6l2z
Z5E96GCTt3OMM6gQMbmFfUQEs88BxIkBpCM9yS8EfKgai/X8MSua3ulb7NoylTgyL3AJF4fAAC28
S3+vcno3AdmGH+Ysn7N78COAtpeplU7T2ObjxqOQ71Jv6X6wuoGA3df9uGVzL/QQHHCBrpXXSTki
P4O4KPqJCpWBbGrLCG8Qel7ACmhCv/pkgOpxX3VsJCZr5Uw+/yWooLTQg8Bl7x52Ju4SO4QBqqq/
vGcTRbakzgLgd8utPY2f2AwFSgoDHmxOT+461cz5tt/zHdtsK0yTivtOUZP8KKsRa9iTtAEW0EEm
c9yH8m4mowyBcJ2bVjCax6YpseUwjRvICakt5hMFQZmpJYr+4fg90pO9ZzwuBLISKYrFjgAbXB18
V4AkqcJJB4q3IUoemAIFvOjL+5HvWmYs0Rsoyog84IQWWSgrLpwLaMDHUPRC9YgTPfpcqUVyOPDy
gu5rjwg+NpfJz0a2of+qJjDJaUgVsLtt/+Z14br2svZKV1xkwpKAFOxF8YZDelpg66Hr1y0Lir+P
VNBoNl5YboPpNS5P4uVkwGxk1TJCo8FxMrjwjRkkwvhMEvB1oYm1UcM3bE3w+pDRIUUz6pS/DLDO
sowKSvl/vFJmC8PMtxN7C377VaR4qwTra26S4T1YWF4sBOahcyHLL3XPO53OaUU6LU8V0JkpavvE
SsG91ACxbV4/UkpKcXGosusRLft2oI26dGKVUKm/hVWQ3vo1jfZJqflVN1KNtbsoDGwvjp38L69h
83iAsH0E0hkT+1yQJGysS/gomiHtGULO0HH2d1O5ujunxXF6SsM/4fdP37twrLLa7jflhdq2FbGa
ToFrnmmMu2orfAAraZE6gi93/QiGEf0FX/D+Ccrw+c4LDxzcVykB4MhSvfEsA/Xx2vHhbariSCMZ
wsKKKoO5mSStPkDhbuaCOcJGvgjJ9Z7uLgSQW9QWUgrXF6OzMxM8Ig6JVYnBPJMca70GuDxEum5/
c1lOdnXnV4C5FgzgxMyHpXpwked/KBWfUfaJgKGJHkgowHapw8BtvHf7z42j6fEy1S5cbFjKhkrP
3YEIs7qNpIWAgYeTTk+IWdeD/xJhy3yo5o1ck/3ZDKAeXKwaAV7ALRPRZhUeujAI7Rh/2jzNOa8D
4Ji2akaH2rwA5sSUua11JU1orsA+e4KS1NVo9HzRewIYYms3OZ6Kzt8iA7s9fj24qIw+Cb2ylvH1
NkzIcgq3MI70Aq7SWxGUNzO54DeLOhxARWT1nT4nbpZxwEBkIuiZ6fOYmXzDy1sL4tdk5elzuBHt
LSpp1LluUVhWISX5q9OWaDfh1duA0Ly/Usg7oIkLEOsGH4q/AgB/bcBvo2Y3aNqqXQjq0WdHMUnZ
Zk4xONCpAY7qX3fFq7+SkS1MaF/1V35tKZA4Js+4yaxm5mhcNag/CjH/uFyMbpAgHKkUshp5dmrx
fKGdSIIO5LU/2uRVJMymsmmsR5690GnMCCZ+fUTZX2I4k4ViQOgEd03YKkO9WYwvMb0ZiIQ5G6Lv
UmUOmff1kv1neDEC6mM1EFOri3tjQrXic2Xa1PepK7whhOusi8o/fhUBoNN87/kyb1LVvGOF/Z5O
aJmq97JYTWQJX7hCD/mU85bxhawX1jJGtOZA/qlE4QQBaZ4tS6ubpkQOPNqfak7wbfX7WGaQ2kA8
CP42S53K9alswXA1gFCH2aZuMstVPzHs2hZb/oWsk/MpxVp8NR7AZ+SDlxalFliJQNW2zbQZwJ8c
xCJDfw4j79cGAdLcgNWEwz+yfe8/SA0mGkmsBjh+yo9glNsbM6r2LxHipMcTz/obTfSLtgeIxEuW
7bM5lywnxSy10OEsk70hFGKmRQlokbtitH4/YV0dqazuljosnciRNa6h6Zol5NnQ5DP0IwuiIc6l
C8Jec0sA7cdsf14iXf7cjkV1Q6u4i+ZC24Ye5On2L0dw0mqKXw16cdFudPmLKM3I3qLNPC9ipHBu
pgVgkF/wQXslAGa7Ja1KN88mAugGs1ygQ7DTrHc35tjJSK60UjVJ73PkYNqdeJrKgJtiefmUDAsW
YVRIMz+GfsYQL9oLmxFBB66C4aPAch/+S8mW7lWhh95TayHMnkBpo+uTA35N95SjfjTKldW7d1mn
j3lSOs4G1A4pDaDVeBwLGF4sxtZsCRRkfw86wwFJmDXtbjcnJnNMlfwPDazKsKxPkxumEfdBUA9a
BK+/viV2rbV+2JqUa68Bhj9clw6PX7781eegAnahJEiDhXYnUyxx8XNcOrsxBPX98TEL3nH8/pAQ
lSkzCXn1aCNGP3LemY6xoDq2bKhwqZ3PsRwdi7W9GZuTFuQbGAlT/FY16Z89U1Nhdj72cmJHvC5A
APcp8/QoGILmJ0dLzpavfhmMiLT4wO6FU7w6UYMvK6kleOrfHo4RW0FLpdfLwX/zIyNqFRYKxc/U
yh/1hE8BpKpluYHSfyJ7I8bl8MMADpVclrfzJHXo1hIXtXYl4bm+ScM310lqwEIjRaF2jqONrUIz
1J6A1YrV4J79gAMKFM413IlDHzK3EBRnGfOoMDdXd8uyEHfFpgDPNHhHCcM6zW9RWovF3Pqa//Z1
oOazBjHWh5hNucO4oUdmFTce2NVQdNC5bXhsm9mdEGLEHQEbkzueZI6/ZyVnRIpDcmVdDLVoMqbx
8R/2GxUcZ0zMKegvpWJ1CWIRqnOU2HYRfnFhEXOXlv48h5IJifGR0Nbr5+vihvKvXQCU/4caxkuO
HL80LyilAtZByYazJDZ+6OBx0dcVmVcXSIqfF5jxLKzCSciYYbE/8+v6TbJCqiGFUCbuc/l7jfXc
QaZOPMp9oOAPkqnR9KkefI2Chw/Hz/0So7kx1mppZjkKnZ1LY7F/Q1dozJltpjtjqN9mkniyn4ty
hg36BhsywPtqBIhuQLITdGYfJP3fV7JrHhPgCUc972kmznk8xngjTqFVUyVWPM5PZHpBzaxME261
vzgfQjA=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
