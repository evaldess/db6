# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
namespace eval ::optrace {
  variable script "D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug.tcl"
  variable category "vivado_synth"
}

# Try to connect to running dispatch if we haven't done so already.
# This code assumes that the Tcl interpreter is not using threads,
# since the ::dispatch::connected variable isn't mutex protected.
if {![info exists ::dispatch::connected]} {
  namespace eval ::dispatch {
    variable connected false
    if {[llength [array get env XILINX_CD_CONNECT_ID]] > 0} {
      set result "true"
      if {[catch {
        if {[lsearch -exact [package names] DispatchTcl] < 0} {
          set result [load librdi_cd_clienttcl[info sharedlibextension]] 
        }
        if {$result eq "false"} {
          puts "WARNING: Could not load dispatch client library"
        }
        set connect_id [ ::dispatch::init_client -mode EXISTING_SERVER ]
        if { $connect_id eq "" } {
          puts "WARNING: Could not initialize dispatch client"
        } else {
          puts "INFO: Dispatch client connection id - $connect_id"
          set connected true
        }
      } catch_res]} {
        puts "WARNING: failed to connect to dispatch server - $catch_res"
      }
    }
  }
}
if {$::dispatch::connected} {
  # Remove the dummy proc if it exists.
  if { [expr {[llength [info procs ::OPTRACE]] > 0}] } {
    rename ::OPTRACE ""
  }
  proc ::OPTRACE { task action {tags {} } } {
    ::vitis_log::op_trace "$task" $action -tags $tags -script $::optrace::script -category $::optrace::category
  }
  # dispatch is generic. We specifically want to attach logging.
  ::vitis_log::connect_client
} else {
  # Add dummy proc if it doesn't exist.
  if { [expr {[llength [info procs ::OPTRACE]] == 0}] } {
    proc ::OPTRACE {{arg1 \"\" } {arg2 \"\"} {arg3 \"\" } {arg4 \"\"} {arg5 \"\" } {arg6 \"\"}} {
        # Do nothing
    }
  }
}

proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
OPTRACE "ila_configbus_decoder_debug_synth_1" START { ROLLUP_AUTO }
set_param chipscope.maxJobs 6
set_param project.vivado.isBlockSynthRun true
set_msg_config -msgmgr_mode ooc_run
OPTRACE "Creating in-memory project" START { }
create_project -in_memory -part xcku035-fbva676-1-c

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.cache/wt [current_project]
set_property parent.project_path D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property ip_output_repo d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
OPTRACE "Creating in-memory project" END { }
OPTRACE "Adding files" START { }
read_ip -quiet d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_configbus_decoder_debug.xci
set_property used_in_synthesis false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_v6_2/constraints/ila_impl.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_v6_2/constraints/ila_impl.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_v6_2/constraints/ila.xdc]
set_property used_in_implementation false [get_files -all d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_configbus_decoder_debug_ooc.xdc]

OPTRACE "Adding files" END { }
# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]
set_param ips.enableIPCacheLiteLoad 1
OPTRACE "Configure IP Cache" START { }

set cached_ip [config_ip_cache -export -no_bom  -dir D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1 -new_name ila_configbus_decoder_debug -ip [get_ips ila_configbus_decoder_debug]]

OPTRACE "Configure IP Cache" END { }
if { $cached_ip eq {} } {
close [open __synthesis_is_running__ w]

OPTRACE "synth_design" START { }
synth_design -top ila_configbus_decoder_debug -part xcku035-fbva676-1-c -mode out_of_context
OPTRACE "synth_design" END { }
OPTRACE "Write IP Cache" START { }

#---------------------------------------------------------
# Generate Checkpoint/Stub/Simulation Files For IP Cache
#---------------------------------------------------------
# disable binary constraint mode for IPCache checkpoints
set_param constraints.enableBinaryConstraints false

catch {
 write_checkpoint -force -noxdef -rename_prefix ila_configbus_decoder_debug_ ila_configbus_decoder_debug.dcp

 set ipCachedFiles {}
 write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ila_configbus_decoder_debug_stub.v
 lappend ipCachedFiles ila_configbus_decoder_debug_stub.v

 write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ila_configbus_decoder_debug_stub.vhdl
 lappend ipCachedFiles ila_configbus_decoder_debug_stub.vhdl

 write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ila_configbus_decoder_debug_sim_netlist.v
 lappend ipCachedFiles ila_configbus_decoder_debug_sim_netlist.v

 write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ ila_configbus_decoder_debug_sim_netlist.vhdl
 lappend ipCachedFiles ila_configbus_decoder_debug_sim_netlist.vhdl
 set TIME_taken [expr [clock seconds] - $TIME_start]

 if { [get_msg_config -count -severity {CRITICAL WARNING}] == 0 } {
  config_ip_cache -add -dcp ila_configbus_decoder_debug.dcp -move_files $ipCachedFiles -use_project_ipc  -synth_runtime $TIME_taken  -ip [get_ips ila_configbus_decoder_debug]
 }
OPTRACE "Write IP Cache" END { }
}
if { [get_msg_config -count -severity {CRITICAL WARNING}] > 0 } {
 send_msg_id runtcl-6 info "Synthesis results are not added to the cache due to CRITICAL_WARNING"
}

rename_ref -prefix_all ila_configbus_decoder_debug_

OPTRACE "write_checkpoint" START { CHECKPOINT }
# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef ila_configbus_decoder_debug.dcp
OPTRACE "write_checkpoint" END { }
OPTRACE "synth reports" START { REPORT }
create_report "ila_configbus_decoder_debug_synth_1_synth_report_utilization_0" "report_utilization -file ila_configbus_decoder_debug_utilization_synth.rpt -pb ila_configbus_decoder_debug_utilization_synth.pb"
OPTRACE "synth reports" END { }

if { [catch {
  write_verilog -force -mode synth_stub D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_stub.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a Verilog synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode synth_stub D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_stub.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a VHDL synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_verilog -force -mode funcsim D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_sim_netlist.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the Verilog functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode funcsim D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_sim_netlist.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the VHDL functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}


} else {


}; # end if cached_ip 

add_files D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_stub.v -of_objects [get_files d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_configbus_decoder_debug.xci]

add_files D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_stub.vhdl -of_objects [get_files d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_configbus_decoder_debug.xci]

add_files D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_sim_netlist.v -of_objects [get_files d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_configbus_decoder_debug.xci]

add_files D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_sim_netlist.vhdl -of_objects [get_files d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_configbus_decoder_debug.xci]

add_files D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug.dcp -of_objects [get_files d:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.srcs/sources_1/ip/ila_configbus_decoder_debug/ila_configbus_decoder_debug.xci]

if {[file isdir D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug]} {
  catch { 
    file copy -force D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_sim_netlist.v D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug
  }
}

if {[file isdir D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug]} {
  catch { 
    file copy -force D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_sim_netlist.vhdl D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug
  }
}

if {[file isdir D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug]} {
  catch { 
    file copy -force D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_stub.v D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug
  }
}

if {[file isdir D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug]} {
  catch { 
    file copy -force D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/ila_configbus_decoder_debug_synth_1/ila_configbus_decoder_debug_stub.vhdl D:/Documents/PostDoc/TileCal/db6/db6_ku_fw/vivado_2020_2/vivado_2020_2.ip_user_files/ip/ila_configbus_decoder_debug
  }
}
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
OPTRACE "ila_configbus_decoder_debug_synth_1" END { }
