// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Fri Dec 11 17:00:20 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               X:/db6_ku_fw/vivado_2020_2/vivado_2020_2.runs/xlx_ku_rx_dpram_synth_1/xlx_ku_rx_dpram_stub.v
// Design      : xlx_ku_rx_dpram
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.2" *)
module xlx_ku_rx_dpram(clka, wea, addra, dina, clkb, enb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,wea[0:0],addra[4:0],dina[39:0],clkb,enb,addrb[2:0],doutb[159:0]" */;
  input clka;
  input [0:0]wea;
  input [4:0]addra;
  input [39:0]dina;
  input clkb;
  input enb;
  input [2:0]addrb;
  output [159:0]doutb;
endmodule
