vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/sem_ultra_v3_1_12
vlib modelsim_lib/msim/xil_defaultlib

vmap sem_ultra_v3_1_12 modelsim_lib/msim/sem_ultra_v3_1_12
vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib

vlog -work sem_ultra_v3_1_12 -64 -incr "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ipstatic/hdl/sem_ultra_v3_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../ipstatic/hdl/xilinx8" "+incdir+../../../ipstatic/hdl/xilinx8/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo" "+incdir+../../../ipstatic/hdl/diablo/db_rowmap" "+incdir+../../../ipstatic/hdl/diablo_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi/db_rowmap" "+incdir+../../../ipstatic/hdl/hood_ssi" "+incdir+../../../ipstatic/hdl/diablo_ssi" "+incdir+../../../ipstatic/hdl/simonly" "+incdir+../../../ip/sem/source" \
"../../../ip/sem/synth/sem.v" \


vlog -work xil_defaultlib \
"glbl.v"

