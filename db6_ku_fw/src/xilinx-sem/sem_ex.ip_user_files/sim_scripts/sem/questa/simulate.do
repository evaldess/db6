onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib sem_opt

do {wave.do}

view wave
view structure
view signals

do {sem.udo}

run -all

quit -force
