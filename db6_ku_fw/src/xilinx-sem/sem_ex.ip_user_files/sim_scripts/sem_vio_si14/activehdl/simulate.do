onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+sem_vio_si14 -L xil_defaultlib -L secureip -O5 xil_defaultlib.sem_vio_si14

do {wave.do}

view wave
view structure

do {sem_vio_si14.udo}

run -all

endsim

quit -force
