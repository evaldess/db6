----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
--           Fernando Carrio
--           Samuel Silverstein
--           Alberto Valero 
--
-- Create Date: 09/14/2018 01:02:55 AM
-- Design Name: 
-- Module Name: db6_gbt_word_assembler - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: this module assembles the gbt words to send to the gbt_wrapper
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

library tilecal;
use tilecal.db6_design_package.all;
use tilecal.tile_link_crc.all;

use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- uncomment the following library declaration if instantiating
-- any xilinx primitives in this code.
library unisim;
use unisim.vcomponents.all;

entity db6_gbt_word_assembler is
  port (
        p_db_side_in         : in std_logic_vector(1 downto 0);
		p_clknet_in              : in    t_db_clock_network;
		--p_clk40_in      	: in  std_logic;
		--p_clk80_in      	: in  std_logic;
		p_db_reg_rx_in          : in  t_db_reg_rx;
		p_bcr_in                : in    std_logic;
		p_tdo_in	            : in	std_logic;
        p_gbt_tx_data_out       : out t_gbt_tx_data;
		p_db_reg_tx_in      	: in  t_db_reg_tx;
		p_adc_data_in           : in  t_adc_readout;
		p_adc_mon_in            : in  std_logic_vector(1 downto 0);
		p_gbt_integrator_in 	: in  std_logic_vector (4 downto 0);
		p_bk_done_in        	: in  std_logic_vector (1 downto 0));
end db6_gbt_word_assembler;

architecture behavioral of db6_gbt_word_assembler is

-- slow control data readout

  signal s_sc_data              	: std_logic_vector(31 downto 0):=(others=>'0');
  signal s_sc_address          	    : std_logic_vector(15 downto 0):=(others=>'0');
  signal s_sc_tx0, s_sc_tx1         : std_logic_vector(15 downto 0):=(others=>'0');
  signal s_sc_switch0, s_sc_switch1 : std_logic_vector(1 downto 0) := "00";
  signal s_gbt_sc_address           : std_logic_vector(15 downto 0);
  signal s_switch             	    : std_logic                    := '0';
  signal s_bcr_d	             	: std_logic                    := '0';

  signal s_adc_data_o_lg 	: std_logic_vector(71 downto 0) := (others => '0');
  signal s_adc_data_o_hg 	: std_logic_vector(71 downto 0) := (others => '0');  


  -- internal signals for data handling from the fmc --
--  signal s_adc_data_o   			: adc_data_array(1 downto 0):=(others=>(others=>'0'));
--  signal s_adc0_data_o  			: adc_data_array(1 downto 0):=(others=>(others=>'0'));
--  signal s_adc2_data_o  			: adc_data_array(1 downto 0):=(others=>(others=>'0'));
--  signal s_adc4_data_o  			: adc_data_array(1 downto 0):=(others=>(others=>'0'));
--  signal s_adc6_data_o  			: adc_data_array(1 downto 0):=(others=>(others=>'0'));
--  signal s_adc8_data_o  			: adc_data_array(1 downto 0):=(others=>(others=>'0'));
--  signal s_adc10_data_o 			: adc_data_array(1 downto 0):=(others=>(others=>'0'));
  
  -- fifo
	signal s_sending_flag			: std_logic:='0';
	signal s_datavalid				: std_logic:='0';
	signal s_datavalid_d			: std_logic:='0';
	signal s_tdo_from_other_fpga	: std_logic:='0';
	signal s_bcidlocal 				: std_logic_vector(11 downto 0):=(others=>'0');

attribute keep : string;
    attribute keep of s_adc_data_o_hg : signal is "true";
    attribute keep of s_adc_data_o_lg : signal is "true";    

begin

s_datavalid 				<= '1';

-- clock in tdo data from other fpga
proc_remote_tdo : process(p_clknet_in.refclk80)
begin
    if rising_edge(p_clknet_in.refclk80) then
        s_tdo_from_other_fpga <= p_tdo_in;
    end if;
end process;

-- adc output data mapping

--  s_adc0_data_o(0)(11 downto 0)  <= p_adc_data_in(0)(13 downto 2);
--  s_adc2_data_o(0)(11 downto 0)  <= p_adc_data_in(2)(13 downto 2);
--  s_adc4_data_o(0)(11 downto 0)  <= p_adc_data_in(4)(13 downto 2);
--  s_adc6_data_o(0)(11 downto 0)  <= p_adc_data_in(6)(13 downto 2);
--  s_adc8_data_o(0)(11 downto 0)  <= p_adc_data_in(8)(13 downto 2);
--  s_adc10_data_o(0)(11 downto 0) <= p_adc_data_in(10)(13 downto 2);

--  s_adc0_data_o(1)(11 downto 0)  <= p_adc_data_in(1)(13 downto 2);
--  s_adc2_data_o(1)(11 downto 0)  <= p_adc_data_in(3)(13 downto 2);
--  s_adc4_data_o(1)(11 downto 0)  <= p_adc_data_in(5)(13 downto 2);
--  s_adc6_data_o(1)(11 downto 0)  <= p_adc_data_in(7)(13 downto 2);
--  s_adc8_data_o(1)(11 downto 0)  <= p_adc_data_in(9)(13 downto 2);
--  s_adc10_data_o(1)(11 downto 0) <= p_adc_data_in(11)(13 downto 2);

-- mb slow control (sc) logic (may need to be migrated)

 control_logic : process (p_clknet_in.refclk40)
	variable v_mb0_tube          : std_logic_vector(1 downto 0);
	variable v_mb0_command       : std_logic_vector(3 downto 0);
	variable v_mb1_tube          : std_logic_vector(1 downto 0);
	variable v_mb1_command       : std_logic_vector(3 downto 0);
	constant v_mb0_fpga          : std_logic   := '0';
	constant v_mb1_fpga          : std_logic   := '1';
	variable v_pmtaddr           : std_logic_vector(3 downto 0);
	variable v_cis_t_cmd	     : std_logic;	
 
  begin
	if rising_edge(p_clknet_in.refclk40) then
		s_sending_flag 	 <= not s_sending_flag;
		s_datavalid_d  <= s_datavalid;
		if (s_datavalid = '1' and s_sending_flag = '1') then
			if p_bk_done_in(0) = '1' then
				v_cis_t_cmd			:= (p_db_reg_tx_in.mb_0_w(29));
				v_mb0_tube          	:= (p_db_reg_tx_in.mb_0_w(17 downto 16)); -- this register is shifted
				v_mb0_command       	:= (p_db_reg_tx_in.mb_0_w(15 downto 12));
				v_pmtaddr            	:= c_mb_to_pmt_addr(conv_integer(p_db_side_in(1) & v_mb0_fpga & v_mb0_tube));
				s_sc_data             	<= x"000" & p_db_side_in(1) & v_mb0_fpga & (p_db_reg_tx_in.mb_0_w(17 downto 0));
				s_sc_address          	<= x"0" & "000" & v_cis_t_cmd & v_pmtaddr & c_mb_to_ppr(conv_integer(v_mb0_command));
			elsif p_bk_done_in(1) = '1' then
				v_cis_t_cmd			:= (p_db_reg_tx_in.mb_1_w(29));
				v_mb1_tube          	:= (p_db_reg_tx_in.mb_1_w(17 downto 16));
				v_mb1_command       	:= (p_db_reg_tx_in.mb_1_w(15 downto 12));
				v_pmtaddr           	:= c_mb_to_pmt_addr(conv_integer(p_db_side_in(1) & v_mb1_fpga & v_mb1_tube));
				s_sc_data            	<= x"000" & p_db_side_in(1) & v_mb1_fpga & (p_db_reg_tx_in.mb_1_w(17 downto 0));
				s_sc_address         	<= x"0" & "000" & v_cis_t_cmd & v_pmtaddr & c_mb_to_ppr(conv_integer(v_mb1_command));				
			else
		 --###########################################--
		 --## continous write of register contents  ##--
		 --###########################################--
				s_sc_address          <= s_gbt_sc_address;			 
					case s_gbt_sc_address is
						when c_db_reg_address_tx.mb_w =>  							--x"001"
							if (p_db_reg_tx_in.mb_w /= x"00000000") then
								s_sc_data <= p_db_reg_tx_in.mb_w;
							end if;
								s_gbt_sc_address <= c_db_reg_address_tx.db_version_w;	--x"00b"

						when c_db_reg_address_tx.db_version_w => 					--x"00b"
							s_sc_data 			<= p_db_reg_tx_in.db_version_w;
							s_gbt_sc_address <= c_db_reg_address_tx.mb_0_w;		--x"00c"

						when c_db_reg_address_tx.mb_0_w =>							--x"00c"
							if ((p_db_reg_tx_in.mb_0_w (11 downto 0)) /= x"000") then
								s_sc_data <= p_db_reg_tx_in.mb_0_w;						
							end if;
							s_gbt_sc_address <= c_db_reg_address_tx.mb_1_w;			--x"00d"

						when c_db_reg_address_tx.mb_1_w =>							--x"00d"
							if ((p_db_reg_tx_in.mb_1_w (11 downto 0)) /= x"000") then
								s_sc_data <= p_db_reg_tx_in.mb_1_w;						
							end if;
							s_gbt_sc_address <= c_db_reg_address_tx.db_reg3_w;		--x"00e"

					  when c_db_reg_address_tx.db_reg3_w => 						--x"00e"
							s_sc_data <= p_db_reg_tx_in.db_reg3_w;
							s_gbt_sc_address <= c_db_reg_address_tx.db_temp_w;	--x"00f"

					  when c_db_reg_address_tx.db_temp_w =>						--x"00f"
							s_sc_data <= p_db_reg_tx_in.db_temp_w;
							s_gbt_sc_address <= c_db_reg_address_tx.db_sem_w;		--x"010"

					  when c_db_reg_address_tx.db_sem_w =>							--x"010"
							s_sc_data <= p_db_reg_tx_in.db_sem_w;						
							s_gbt_sc_address <= c_db_reg_address_tx.db_reg6_w;	--x"011"

					  when c_db_reg_address_tx.db_reg6_w => 
							s_sc_data <= p_db_reg_tx_in.db_reg6_w;
							s_gbt_sc_address <= c_db_reg_address_tx.hv_w;			--x"078"

					  when c_db_reg_address_tx.hv_w =>								--x"078"
							s_sc_data <= p_db_reg_tx_in.hv_w;
							s_gbt_sc_address <= c_db_reg_address_tx.cs_t_w;			--x"00b"

					  when c_db_reg_address_tx.cs_t_w =>
							s_sc_data <= p_db_reg_tx_in.cs_t_w;
							s_gbt_sc_address <= c_db_reg_address_tx.cs_s_w;

					  when c_db_reg_address_tx.cs_s_w =>
							s_sc_data <= p_db_reg_tx_in.cs_s_w;
							s_gbt_sc_address <= c_db_reg_address_tx.cs_c_w;

					  when c_db_reg_address_tx.cs_c_w =>
							s_sc_data <= p_db_reg_tx_in.cs_c_w;
							s_gbt_sc_address <= c_db_reg_address_tx.db_serial_id_lsb;
					  
					  when c_db_reg_address_tx.db_serial_id_lsb =>
                                  s_sc_data <= p_db_reg_tx_in.db_serial_id_lsb;
                                  s_gbt_sc_address <= c_db_reg_address_tx.db_serial_id_msb;

					  when c_db_reg_address_tx.db_serial_id_msb =>
                                  s_sc_data <= p_db_reg_tx_in.db_serial_id_msb;
                                  s_gbt_sc_address <= c_db_reg_address_tx.mb_w;                        

                      when others => s_gbt_sc_address <= c_db_reg_address_tx.mb_w;
                      
					end case;
			end if;
		end if;
	end if;
end process control_logic;

-- need to slow this down to 40 mhz (sam)

proc_sc_manager: process(p_clknet_in.refclk40)
type t_sm_write_state is (st_writing,st_idle);
variable v_sm_write_state : t_sm_write_state := st_idle;
begin
	if rising_edge(p_clknet_in.refclk40) then
		case v_sm_write_state is
		 when st_idle =>
			if s_sending_flag = '0' then
				s_sc_switch0   <= "01";
				s_sc_switch1   <= "10";
				s_sc_tx0       <= s_sc_address;
				s_sc_tx1       <= s_sc_data(31 downto 16);
				--writestate <= "01";
				v_sm_write_state := st_writing;
			else
				s_sc_switch0   <= "00";
				s_sc_switch1   <= "00";
				s_sc_tx0       <= (others=>'0');
				s_sc_tx1       <= (others=>'0');				
				--writestate <= "00";			
				v_sm_write_state := st_idle;
			end if;
		 when others => -- nominally st_writing
			s_sc_switch0   <= "11";
			s_sc_switch1   <= "00";
			s_sc_tx0       <= s_sc_data(15 downto 0);
			s_sc_tx1       <= (others=>'0');
			--writestate <= "00";
			v_sm_write_state := st_idle;
	   end case;
	end if; -- clock edge
end process;


-- Reformat low- and high-gain data

    gen_adc_reformat : for adc in 0 to 5 generate
        s_adc_data_o_hg(adc*12+11 downto adc*12)  <=  p_adc_data_in.hg_data(adc)(13 downto 2);
        s_adc_data_o_lg(adc*12+11 downto adc*12)  <=  p_adc_data_in.lg_data(adc)(13 downto 2);        
    end generate; -- gen_adc_reformat

--    proc_adc_reformat : process (p_adc_data_in)
--        variable adc, index: integer := 0;    
--    begin 
--        for adc in 0 to 5 loop
--            index := adc * 12;
--            s_adc_data_o_hg(index+11 downto index)  <=  p_adc_data_in(2*adc)(13 downto 2);
--            s_adc_data_o_lg(index+11 downto index)  <=  p_adc_data_in(2*adc+1)(13 downto 2);        
--        end loop;
--end process;

-- assembling data words
    proc_db_data_assembler : process (p_clknet_in.refclk40)
        variable v_out_data_temp 	: std_logic_vector(115 downto 0):= (others => '0');  
        variable v_lg_data_temp 	: std_logic_vector(115 downto 0):= (others => '0');  
        variable v_hg_data_temp 	: std_logic_vector(115 downto 0):= (others => '0');
        
        variable v_bcr			      : std_logic:='0';
        variable v_switch		      : std_logic:='0';
        variable v_count		      : std_logic:='0';
        variable v_bcid_local		  : std_logic_vector(11 downto 0):=(others=>'0');
        variable index                : integer := 0;
    
    begin
        if rising_edge(p_clknet_in.refclk40) then
            
            --get bcr and bcid
            v_bcr		:= p_bcr_in;
            v_bcid_local := p_db_reg_rx_in(bc_number)(11 downto 0);

            -- assemble lg data
            v_lg_data_temp(10 downto 0)    := tile_link_crc_compute(p_adc_mon_in & s_sc_switch0 & s_sc_tx0 & p_gbt_integrator_in & s_tdo_from_other_fpga & "0" & v_bcr & s_adc_data_o_lg)(10 downto 0);
            v_lg_data_temp(15 downto 11)   := v_bcid_local(4 downto 0);
            v_lg_data_temp(87 downto 16)   := s_adc_data_o_lg;
            v_lg_data_temp(88)             := v_bcr; 
            v_lg_data_temp(89)             := '0';
            v_lg_data_temp(90)             := s_tdo_from_other_fpga;
            v_lg_data_temp(95 downto 91)   := p_gbt_integrator_in;
            v_lg_data_temp(111 downto 96)  := s_sc_tx0;
            v_lg_data_temp(113 downto 112) := s_sc_switch0;
            v_lg_data_temp(115 downto 114) := p_adc_mon_in;
            
            --assemble hg data
            v_hg_data_temp(10 downto 0)    := tile_link_crc_compute(p_adc_mon_in & s_sc_switch1 & s_sc_tx1 & p_gbt_integrator_in & s_tdo_from_other_fpga & "1" & v_bcr & s_adc_data_o_hg)(10 downto 0);
            v_hg_data_temp(15 downto 11)   := v_bcid_local(4 downto 0);
            v_hg_data_temp(87 downto 16)   := s_adc_data_o_hg;
            v_hg_data_temp(88)             := v_bcr;  -- bcr
            v_hg_data_temp(89)             := '1';  -- gain
            v_hg_data_temp(90)             := s_tdo_from_other_fpga;
            v_hg_data_temp(95 downto 91)   := p_gbt_integrator_in;
            v_hg_data_temp(111 downto 96)  := s_sc_tx1;
            v_hg_data_temp(113 downto 112) := s_sc_switch1;
            v_hg_data_temp(115 downto 114) := p_adc_mon_in;
            
            p_gbt_tx_data_out.lg <= v_lg_data_temp;
            p_gbt_tx_data_out.hg <= v_hg_data_temp;
        end if;
    end process;
  
end behavioral;


