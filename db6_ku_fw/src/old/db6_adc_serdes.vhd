--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Samuel Silverstein    silver@fysik.su.se
--                        Eduardo Valdes Santurio
--                                                                                                 
-- Project Name:          ADC deserializer for LTC2264-12                                                                
-- Module Name:           ADC_top                                        
--                                                                                                 
-- Language:              VHDL                                                                 
--                                                                                                   --
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_adc_interface_v1 is

    Port ( 	
                p_master_reset_in          : in std_logic;
--                p_adc_mode_in              : in std_logic;
				p_clknet_in 			: in  t_db_clock_network;
				p_adc_readout_out       : out t_adc_readout;
				p_adc_readout_control_in : in t_adc_readout_control;
				p_adc_data0_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data0_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_data1_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_p_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_bitclk_n_in 	: in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_p_in : in  STD_LOGIC_VECTOR (5 downto 0);
				p_adc_frameclk_n_in : in  STD_LOGIC_VECTOR (5 downto 0);
				
				--p_idelay_ctrl_reset_lg_in : in std_logic_vector (5 downto 0);
				--p_idelay_ctrl_reset_hg_in : in std_logic_vector (5 downto 0);
				--p_idelay_ctrl_reset_fc_in : in std_logic_vector (5 downto 0);
--				p_idelay3_load_lg_in : in std_logic_vector (5 downto 0);
--				p_idelay3_load_hg_in : in std_logic_vector (5 downto 0);
--				p_idelay3_load_fc_in : in std_logic_vector (5 downto 0);
--			    p_idelay_en_vtc_lg_in : in std_logic_vector (5 downto 0);
--			    p_idelay_en_vtc_hg_in : in std_logic_vector (5 downto 0);
--			    p_idelay_en_vtc_fc_in : in std_logic_vector (5 downto 0);
--			    p_idelay_count_in_lg_in : in t_idelay_count;
--			    p_idelay_count_in_hg_in : in t_idelay_count;
--			    p_idelay_count_in_fc_in : in t_idelay_count;
--                p_idelay_count_in_lg_out : out t_idelay_count;
--                p_idelay_count_in_hg_out : out t_idelay_count;
--                p_idelay_count_in_fc_out : out t_idelay_count;
--                p_bitslip_lg_in : in t_bitslip;
--                p_bitslip_hg_in : in t_bitslip;
--				--p_clkdiv_out		: out STD_LOGIC;
--				p_channel_frame_missalignemt_out : out std_logic_vector(5 downto 0);
--				p_adc_data_out		: out t_adc_data_type;
				p_debug_pin_out : out std_logic_vector(5 downto 0)
				);
end db6_adc_interface_v1;

architecture Behavioral of db6_adc_interface_v1 is

    signal s_data_lg, s_data_hg, s_data_lg_delayed, s_data_hg_delayed , s_bitclk_in, s_bitclk, s_bitclk_div, s_frameclk, 
    s_frameclk_delayed, s_idelay_ctrl_reset_lg, s_idelay_ctrl_reset_hg, s_iserdes_ctrl_reset_lg, s_iserdes_ctrl_reset_hg, 
    s_bufgce_div_ctrl_reset, s_idelay_ctrl_reset_fc, s_iserdes_ctrl_reset_fc : std_logic_vector (5 downto 0) := (others => '0');
    signal s_channel_frame_missalignemt : std_logic_vector (5 downto 0) := (others => '1');
    
    signal s_frame_strobe : std_logic_vector (5 downto 0);
    

    signal s_readout_lg_sr,s_readout_hg_sr : t_adc_sr;
    signal s_bitslice_lg_sr, s_bitslice_hg_sr, s_bitslice_fc_sr : t_bitslice_sr;
    signal s_adc_readout : t_adc_readout;
    signal s_lg_adc_output_word, s_hg_adc_output_word, s_fc_word, s_fc_output_word : t_adc_data;
    
    signal s_idelay3_load_lg, s_idelay3_load_hg, s_idelay3_load_channel, s_idelay_en_vtc_lg, s_idelay_en_vtc_hg, s_idelay_en_vtc_channel  : std_logic_vector (5 downto 0);
    signal s_idelay_count_in_lg, s_idelay_count_in_hg, s_idelay_count_out_lg,  s_idelay_count_out_hg : t_idelay_count;

    signal s_adc_output_temp, s_adc_fc_input_temp, s_adc_input_hg_temp, s_adc_input_lg_temp : t_adc_oversample_data_type;
    
    attribute keep : string;
    --attribute keep of s_adc_word          : signal is "true";
    

    

------------- Begin Cut here for COMPONENT Declaration ------ COMP_TAG


begin

p_adc_readout_out <= s_adc_readout;

-- Differential to single-ended conversion of ADC inputs from FMC

diff_to_se : for i in 0 to 5 generate

    IBUFDS_DATA0 : IBUFDS  -- ADC output Low gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_lg(i),             -- Buffer diff_p output
        I  => p_adc_data0_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data0_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFDS_DATA1 : IBUFDS -- ADC output High gain
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_data_hg(i),             -- Buffer diff_p output
        I  => p_adc_data1_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_data1_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFDS_FRMCLK : IBUFDS -- ADC frame clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_frameclk(i),             -- Buffer diff_p output
        I  => p_adc_frameclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_frameclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );

    IBUFGDS_BITCLK : IBUFGDS -- ADC bit clock 
      generic map (
        IOSTANDARD => "LVDS", DIFF_TERM => TRUE)
      port map (
        O  => s_bitclk_in(i),             -- Buffer diff_p output
        I  => p_adc_bitclk_p_in(i),  -- Diff_p buffer input (connect directly to top-level port)
        IB => p_adc_bitclk_n_in(i)  -- Diff_n buffer input (connect directly to top-level port)
        );


    clock_buffer : bufg
        port map (
            I => s_bitclk_in(i),
            O => s_bitclk(i)
        );

    bufgce_div_inst : bufgce_div
    generic map (
        bufgce_divide => 2, -- 1-8
        -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
        is_ce_inverted => '0', -- optional inversion for ce
        is_clr_inverted => '0', -- optional inversion for clr
        is_i_inverted => '0' -- optional inversion for i
        )
        port map (
        o => s_bitclk_div(i), -- 1-bit output: buffer
        ce => '1', -- 1-bit input: buffer enable
        clr => s_bufgce_div_ctrl_reset(i), -- 1-bit input: asynchronous clear
        i => s_bitclk_in(i) -- 1-bit input: buffer
    );


end generate;

-- Generate ADC data input registers and output word mapping,
-- The output bits from each ADC is stored in four shift registers (two even and two odd).

gen_adc_channels: for v_adc in 0 to 5 generate

    s_adc_readout.channel_frame_missalignemt(v_adc)<= s_channel_frame_missalignemt(v_adc);


i_idelaye3_data_lg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 140.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_adc_readout.lg_idelay_count(v_adc), -- 9-bit output: counter value output
        dataout => s_data_lg_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => s_bitclk_div(v_adc), -- 1-bit input: clock input
        cntvaluein => p_adc_readout_control_in.lg_idelay_count(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => p_adc_readout_control_in.lg_idelay_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_data_lg(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => p_adc_readout_control_in.lg_idelay_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_idelay_ctrl_reset_lg(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);
--s_idelay_ctrl_reset_lg(v_adc) <= p_idelay_ctrl_reset_lg_in(v_adc);


i_idelaye3_data_hg : idelaye3
    generic map (
        SIM_DEVICE => "ULTRASCALE_PLUS",
        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
        delay_format => "count", -- units of the delay_value (time, count)
        delay_src => "idatain", -- delay input (idatain, datain)
        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
        delay_value => 0, -- input delay value setting
        is_clk_inverted => '0', -- optional inversion for clk
        is_rst_inverted => '0', -- optional inversion for rst
        refclk_frequency => 140.0, -- idelayctrl clock input frequency in mhz (values)
        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
        )
        port map (
        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
        cntvalueout => s_adc_readout.hg_idelay_count(v_adc), -- 9-bit output: counter value output
        dataout => s_data_hg_delayed(v_adc), -- 1-bit output: delayed data output
        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
        ce => '0', -- 1-bit input: active high enable increment/decrement input
        clk => s_bitclk_div(v_adc), -- 1-bit input: clock input
        cntvaluein => p_adc_readout_control_in.hg_idelay_count(v_adc), -- 9-bit input: counter value input
        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
        en_vtc => p_adc_readout_control_in.hg_idelay_en_vtc(v_adc), -- 1-bit input: keep delay constant over vt
        idatain => s_data_hg(v_adc), -- 1-bit input: data input from the logic
        inc => '0', -- 1-bit input: increment / decrement tap delay input
        load => p_adc_readout_control_in.hg_idelay_load(v_adc), -- 1-bit input: load delay_value input
        rst => s_idelay_ctrl_reset_hg(v_adc) -- 1-bit input: asynchronous reset to the delay_value
);

--s_idelay_ctrl_reset_hg(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc);

--i_idelaye3_data_fc : idelaye3
--    generic map (
--        SIM_DEVICE => "ULTRASCALE_PLUS",
--        cascade => "none", -- cascade setting (none, master, slave_end, slave_middle)
--        delay_format => "count", -- units of the delay_value (time, count)
--        delay_src => "idatain", -- delay input (idatain, datain)
--        delay_type => "var_load", -- set the type of tap delay line (fixed, var_load, variable)
--        delay_value => 0, -- input delay value setting
--        is_clk_inverted => '0', -- optional inversion for clk
--        is_rst_inverted => '0', -- optional inversion for rst
--        refclk_frequency => 140.0, -- idelayctrl clock input frequency in mhz (values)
--        update_mode => "async" -- determines when updates to the delay will take effect (async, manual, sync)
--        )
--        port map (
--        casc_out => open, -- 1-bit output: cascade delay output to odelay input cascade
--        cntvalueout => p_idelay_count_in_fc_out(v_adc), -- 9-bit output: counter value output
--        dataout => s_frameclk_delayed(v_adc), -- 1-bit output: delayed data output
--        casc_in => '0', -- 1-bit input: cascade delay input from slave odelay cascade_out
--        casc_return =>  '0', -- 1-bit input: cascade delay returning from slave odelay dataout
--        ce => '0', -- 1-bit input: active high enable increment/decrement input
--        clk => s_bitclk_div(v_adc), -- 1-bit input: clock input
--        cntvaluein => p_idelay_count_in_fc_in(v_adc), -- 9-bit input: counter value input
--        datain => '0',--s_data_lg(i), -- 1-bit input: data input from the iobuf
--        en_vtc => p_idelay_en_vtc_fc_in(v_adc), -- 1-bit input: keep delay constant over vt
--        idatain => s_frameclk(v_adc), -- 1-bit input: data input from the logic
--        inc => '0', -- 1-bit input: increment / decrement tap delay input
--        load => p_idelay3_load_fc_in(v_adc), -- 1-bit input: load delay_value input
--        rst => s_idelay_ctrl_reset_fc(v_adc) -- 1-bit input: asynchronous reset to the delay_value
--);

--s_idelay_ctrl_reset_fc(v_adc) <= p_idelay_ctrl_reset_fc_in(v_adc);

i_iserdese3_lg : iserdese3
generic map (
SIM_DEVICE => "ULTRASCALE_PLUS",
data_width => 4, -- parallel data width (4,8)
fifo_enable => "false", -- enables the use of the fifo
fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
is_clk_b_inverted => '1', -- optional inversion for clk_b
is_clk_inverted => '0', -- optional inversion for clk
is_rst_inverted => '0' -- optional inversion for rst
)
port map (

fifo_empty => open, -- 1-bit output: fifo empty flag
internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
q => s_bitslice_lg_sr(v_adc), -- 8-bit registered output
clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
d => s_data_lg_delayed(v_adc), -- 1-bit input: serial data input
fifo_rd_clk => '0', -- 1-bit input: fifo read clock
fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
rst => s_iserdes_ctrl_reset_lg(v_adc) -- 1-bit input: asynchronous reset

);
--s_iserdes_ctrl_reset_lg(v_adc) <= p_idelay_ctrl_reset_lg_in(v_adc);

i_iserdese3_hg : iserdese3
generic map (
SIM_DEVICE => "ULTRASCALE_PLUS",
data_width =>4, -- parallel data width (4,8)
fifo_enable => "false", -- enables the use of the fifo
fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
is_clk_b_inverted => '1', -- optional inversion for clk_b
is_clk_inverted => '0', -- optional inversion for clk
is_rst_inverted => '0' -- optional inversion for rst
)
port map (

fifo_empty => open, -- 1-bit output: fifo empty flag
internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
q => s_bitslice_hg_sr(v_adc), -- 8-bit registered output
clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
d => s_data_hg_delayed(v_adc), -- 1-bit input: serial data input
fifo_rd_clk => '0', -- 1-bit input: fifo read clock
fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
rst => s_iserdes_ctrl_reset_hg(v_adc) -- 1-bit input: asynchronous reset

);
--s_iserdes_ctrl_reset_hg(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc);


--i_iserdese3_fc : iserdese3
--generic map (
--SIM_DEVICE => "ULTRASCALE_PLUS",
--data_width => 4, -- parallel data width (4,8)
--fifo_enable => "true", -- enables the use of the fifo
--fifo_sync_mode => "false", -- enables the use of internal 2-stage synchronizers on the fifo
--is_clk_b_inverted => '1', -- optional inversion for clk_b
--is_clk_inverted => '0', -- optional inversion for clk
--is_rst_inverted => '0' -- optional inversion for rst
--)
--port map (

--fifo_empty => open, -- 1-bit output: fifo empty flag
--internal_divclk => open, -- 1-bit output: internally divided down clock used when fifo is disabled (do not connect)
--q => s_bitslice_fc_sr(v_adc), -- 8-bit registered output
--clk => s_bitclk(v_adc), -- 1-bit input: high-speed clock
--clkdiv => s_bitclk_div(v_adc), -- 1-bit input: divided clock
--clk_b => s_bitclk(v_adc), -- 1-bit input: inversion of high-speed clock clk
--d => s_frameclk_delayed(v_adc), -- 1-bit input: serial data input
--fifo_rd_clk => '0', -- 1-bit input: fifo read clock
--fifo_rd_en => '0', -- 1-bit input: enables reading the fifo when asserted
--rst => p_idelay_ctrl_reset_hg_in(v_adc) -- 1-bit input: asynchronous reset

--);
--s_iserdes_ctrl_reset_fc(v_adc) <= p_idelay_ctrl_reset_hg_in(v_adc) and p_idelay_ctrl_reset_lg_in(v_adc);


        proc_shift_in : process(s_bitclk_div(v_adc)) -- Odd data bits clocked in on rising edge of adc clocks 
        begin


            if falling_edge(s_bitclk_div(v_adc)) then
                --if p_adc_mode_in = '0' then
                
                    s_adc_input_hg_temp(v_adc) <= s_adc_input_hg_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_hg_sr(v_adc)(0) & s_bitslice_hg_sr(v_adc)(1) & s_bitslice_hg_sr(v_adc)(2) & s_bitslice_hg_sr(v_adc)(3);
                    s_adc_input_lg_temp(v_adc) <= s_adc_input_lg_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_lg_sr(v_adc)(0) & s_bitslice_lg_sr(v_adc)(1) & s_bitslice_lg_sr(v_adc)(2) & s_bitslice_lg_sr(v_adc)(3);
                    --s_adc_fc_input_temp(v_adc) <= s_adc_fc_input_temp(v_adc)(((c_oversamples*c_adc_bit_number -1) - 4) downto 0) & s_bitslice_fc_sr(v_adc);
                --else 
            
            end if;
        end process; -- sr_odd




    frame_capture : process(s_bitclk(v_adc)) -- Capture ADC words synchronous to ADC (bit) clock
    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
    variable v_frameclk_sm : t_frameclk_sm := st_low;
    type t_reset_sm is (st_initialize, st_reset_divclk, st_reset_idelay, st_reset_iserdes, st_idle);
    variable v_reset_sm : t_reset_sm := st_initialize;
    variable v_counter : integer := 0;
    variable v_bitclk_div_buffer, v_bitclk_div_previous_buffer : std_logic;

    
    variable v_phase_shift : integer :=0;
    variable v_phase_control : std_logic := '0' ;
    constant c_phase_offset : integer:= 14 ;    
    begin
        if rising_edge(s_bitclk(v_adc)) then
        
            p_debug_pin_out(v_adc)<= s_frameclk(v_adc);
            v_bitclk_div_buffer:= s_bitclk_div(v_adc);
            v_bitclk_div_previous_buffer:=v_bitclk_div_buffer;
            case v_frameclk_sm is
                when st_rising_edge =>
                              
                    
                    if p_adc_readout_control_in.adc_mode = '0' then  
                        s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc))) + (c_adc_bit_number-1) + v_phase_shift  downto c_phase_offset + v_phase_shift +(to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc)))));
                        s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc))) + (c_adc_bit_number-1) + v_phase_shift downto c_phase_offset + v_phase_shift +(to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc)))));
                    else
                        s_hg_adc_output_word(v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.hg_bitslip(v_adc)))));
                        s_lg_adc_output_word(v_adc) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc))) + (c_adc_bit_number-1-2) downto c_phase_offset + (to_integer(unsigned(p_adc_readout_control_in.lg_bitslip(v_adc)))));    
                    end if;

                    if s_frameclk(v_adc) = '1' then -- Capture when frame clock has just gone high
                        v_frameclk_sm := st_high;
                    else
                        v_frameclk_sm := st_falling_edge;
                    end if;
                    
                    case v_reset_sm is
                
                        when st_initialize =>
                            s_bufgce_div_ctrl_reset(v_adc) <=  '1';
                            s_idelay_ctrl_reset_lg(v_adc)  <=  '1';
                            s_iserdes_ctrl_reset_lg(v_adc)  <=  '1';
                            s_idelay_ctrl_reset_hg(v_adc)  <=  '1';
                            s_iserdes_ctrl_reset_hg(v_adc)  <=  '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            v_phase_control:='0';
                            v_reset_sm:=st_reset_divclk;
                            
                        when st_reset_divclk => 
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            s_idelay_ctrl_reset_lg(v_adc)  <=  '1';
                            s_iserdes_ctrl_reset_lg(v_adc)  <=  '1';
                            s_idelay_ctrl_reset_hg(v_adc)  <=  '1';
                            s_iserdes_ctrl_reset_hg(v_adc)  <=  '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            v_phase_control:='0';
                            if s_bitclk_div(v_adc) = '1' then -- and v_bitclk_div_buffer = '0' then                     
                                v_reset_sm:=st_reset_idelay;                        
                            end if;
                        
                        when st_reset_idelay => 
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            s_idelay_ctrl_reset_lg(v_adc)  <=  '0';
                            s_iserdes_ctrl_reset_lg(v_adc)  <=  '1';
                            s_idelay_ctrl_reset_hg(v_adc)  <=  '0';
                            s_iserdes_ctrl_reset_hg(v_adc)  <=  '1';
                            s_channel_frame_missalignemt(v_adc) <= '1';  
                            v_phase_control:='0';
                            if s_bitclk_div(v_adc) = '1' then -- and v_bitclk_div_buffer = '0' then                    
                                v_reset_sm:=st_reset_iserdes;                        
                            end if;
                                                        
                        when st_reset_iserdes =>
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            s_idelay_ctrl_reset_lg(v_adc)  <=  '0';
                            s_iserdes_ctrl_reset_lg(v_adc)  <=  '0';
                            s_idelay_ctrl_reset_hg(v_adc)  <=  '0';
                            s_iserdes_ctrl_reset_hg(v_adc)  <=  '0';
                            s_channel_frame_missalignemt(v_adc) <= '1';
                            v_phase_control:='1';
                            if s_bitclk_div(v_adc) = '1' then -- and v_bitclk_div_buffer = '0' then              
                                v_reset_sm:=st_idle;                        
                            end if;                            
                            
                        when st_idle =>
                            s_bufgce_div_ctrl_reset(v_adc) <=  '0';
                            s_idelay_ctrl_reset_lg(v_adc)  <=  '0';
                            s_iserdes_ctrl_reset_lg(v_adc)  <=  '0';
                            s_idelay_ctrl_reset_hg(v_adc)  <=  '0';
                            s_iserdes_ctrl_reset_hg(v_adc)  <=  '0';
                            s_channel_frame_missalignemt(v_adc) <= '0';

                            if v_phase_control =  '1' then
                                v_phase_shift:= -2;
                            else
                                v_phase_shift:= 0;
                            end if;
                            
                            v_phase_control:= not v_phase_control;

                            if p_master_reset_in = '1' then
                                v_reset_sm:=st_initialize;
                            else
                                
                            end if;                           
                        when others=>
                            v_reset_sm:=st_initialize;
                    end case;
                    
                    
                    
                when st_falling_edge =>
                 
                    if s_frameclk(v_adc) = '0' then -- Capture when frame clock has just gone high
                        v_frameclk_sm := st_low;
                    else
                        v_frameclk_sm := st_rising_edge;
                    end if; 
                when st_low =>
                    if s_frameclk(v_adc) = '1' then
                        v_frameclk_sm := st_rising_edge;
                    end if;
                when st_high =>
                    if s_frameclk(v_adc) = '0' then
                        v_frameclk_sm := st_falling_edge;
                    end if;                    
                
            end case;
            
            
        end if;
    end process;


end generate;


        bc_capture : process(p_clknet_in.clk40) -- Transition to 40 MHz FPGA clock for readout
        begin
            if falling_edge(p_clknet_in.clk40) then
                s_adc_readout.lg_data <= s_lg_adc_output_word;
                s_adc_readout.hg_data <= s_hg_adc_output_word;
            end if;
        end process;


end Behavioral;



--gen_adc_channels_quadrant_0: for v_adc in 0 to 2 generate

--    frame_capture : process(s_bitclk(1)) -- Capture ADC words synchronous to ADC (bit) clock
--    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
--    variable v_frameclk_sm : t_frameclk_sm := st_low;
    
--    variable v_phase_shift : integer :=0;
--    variable v_phase_control : std_logic := '0' ;
--    constant c_phase_offset : integer:= 14 ;    
--    begin
--        if rising_edge(s_bitclk(1)) then
--            p_debug_pin_out(v_adc)<= s_frameclk(v_adc);
--            case v_frameclk_sm is
--                when st_rising_edge =>
                              
--                    if v_phase_control =  '1' then
--                        v_phase_shift:= -2;
--                    else
--                        v_phase_shift:= 0;
--                    end if;
--                    v_phase_control:= not v_phase_control;
                    
--                    if p_adc_mode_in = '0' then  
--                        s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift  downto c_phase_offset + v_phase_shift +(to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                        s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_bitslip_lg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift downto c_phase_offset + v_phase_shift +(to_integer(unsigned(p_bitslip_lg_in(v_adc)))));
--                    else
--                        s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) downto c_phase_offset + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                        s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_bitslip_lg_in(v_adc))) + (c_adc_bit_number-1) downto c_phase_offset + (to_integer(unsigned(p_bitslip_lg_in(v_adc)))));    
                    
--                    end if;
--                    --s_fc_output_word(2*v_adc)<= s_adc_fc_input_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift  downto v_phase_shift + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                    --s_fc_output_word(2*v_adc+1)<= s_adc_fc_input_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift  downto v_phase_shift + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));

--                    if s_frameclk(v_adc) = '1' then -- Capture when frame clock has just gone high
--                        v_frameclk_sm := st_high;
--                    else
--                        v_frameclk_sm := st_falling_edge;
--                    end if;
--                when st_falling_edge =>
                 
--                    if s_frameclk(v_adc) = '0' then -- Capture when frame clock has just gone high
--                        v_frameclk_sm := st_low;
--                    else
--                        v_frameclk_sm := st_rising_edge;
--                    end if; 
--                when st_low =>
--                    if s_frameclk(v_adc) = '1' then
--                        v_frameclk_sm := st_rising_edge;
--                    end if;
--                when st_high =>
--                    if s_frameclk(v_adc) = '0' then
--                        v_frameclk_sm := st_falling_edge;
--                    end if;                    
                
--            end case;
            
            
--        end if;
--    end process;
--end generate;

--gen_adc_channels_quadrant_1: for v_adc in 3 to 5 generate

--    frame_capture : process(s_bitclk(4)) -- Capture ADC words synchronous to ADC (bit) clock
--    type t_frameclk_sm is (st_high, st_low, st_rising_edge, st_falling_edge);
--    variable v_frameclk_sm : t_frameclk_sm := st_low;
    
--    variable v_phase_shift : integer :=0;
--    variable v_phase_control : std_logic := '0' ;
--    constant c_phase_offset : integer:= 14 ;    
--    begin
--        if rising_edge(s_bitclk(4)) then
        
--            p_debug_pin_out(v_adc)<= s_frameclk(v_adc);
--            case v_frameclk_sm is
--                when st_rising_edge =>
                              
--                    if v_phase_control =  '1' then
--                        v_phase_shift:= -2;
--                    else
--                        v_phase_shift:= 0;
--                    end if;
--                    v_phase_control:= not v_phase_control;
                    
--                    if p_adc_mode_in = '0' then  
--                        s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift  downto c_phase_offset + v_phase_shift +(to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                        s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_bitslip_lg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift downto c_phase_offset + v_phase_shift +(to_integer(unsigned(p_bitslip_lg_in(v_adc)))));
--                    else
--                        s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)( c_phase_offset + to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) downto c_phase_offset + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                        s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(c_phase_offset + to_integer(unsigned(p_bitslip_lg_in(v_adc))) + (c_adc_bit_number-1) downto c_phase_offset + (to_integer(unsigned(p_bitslip_lg_in(v_adc)))));    
                    
--                    end if;
--                    --s_fc_output_word(2*v_adc)<= s_adc_fc_input_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift  downto v_phase_shift + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                    --s_fc_output_word(2*v_adc+1)<= s_adc_fc_input_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + v_phase_shift  downto v_phase_shift + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));

--                    if s_frameclk(v_adc) = '1' then -- Capture when frame clock has just gone high
--                        v_frameclk_sm := st_high;
--                    else
--                        v_frameclk_sm := st_falling_edge;
--                    end if;
--                when st_falling_edge =>
                 
--                    if s_frameclk(v_adc) = '0' then -- Capture when frame clock has just gone high
--                        v_frameclk_sm := st_low;
--                    else
--                        v_frameclk_sm := st_rising_edge;
--                    end if; 
--                when st_low =>
--                    if s_frameclk(v_adc) = '1' then
--                        v_frameclk_sm := st_rising_edge;
--                    end if;
--                when st_high =>
--                    if s_frameclk(v_adc) = '0' then
--                        v_frameclk_sm := st_falling_edge;
--                    end if;                    
                
--            end case;
            
            
--        end if;
--    end process;
--end generate;


--        proc_frame_capture : process(s_frameclk(v_adc)) -- Capture ADC words synchronous to ADC (bit) clock
--            begin
--                if rising_edge(s_frameclk(v_adc)) then
--                    s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1)  downto (to_integer(unsigned(p_bitslip_hg_in(v_adc)))) );
--                    s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(to_integer(unsigned(p_bitslip_lg_in(v_adc))) + (c_adc_bit_number-1)  downto (to_integer(unsigned(p_bitslip_lg_in(v_adc)))));
--                end if; 
--            end process;  -- frame capture

--        frame_capture : process(p_clknet_in.clk40) -- Capture ADC words synchronous to ADC (bit) clock
--        variable v_frame_clock_phase: integer;
--        constant c_frame_clock_phase_mux: integer:=8;
--        begin
--            if rising_edge(p_clknet_in.clk40) then
--                case s_bitslice_fc_sr(v_adc) is
--                    when "00000001" =>
--                        v_frame_clock_phase:=1;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "00000011" =>
--                        v_frame_clock_phase:=2;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "00000111" =>
--                        v_frame_clock_phase:=3;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "00001111" =>
--                        v_frame_clock_phase:=4;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "00011111" =>
--                        v_frame_clock_phase:=5;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "00111111" =>
--                        v_frame_clock_phase:=6;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "01111111" =>
--                        v_frame_clock_phase:=7;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "11111110" =>
--                        v_frame_clock_phase:=8;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "11111100" =>
--                        v_frame_clock_phase:=9;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "11111000" =>
--                        v_frame_clock_phase:=10;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "11110000" =>
--                        v_frame_clock_phase:=11;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "11100000" =>
--                        v_frame_clock_phase:=12;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "11000000" =>
--                        v_frame_clock_phase:=13;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when "10000000" =>
--                        v_frame_clock_phase:=0;
--                        p_channel_frame_missalignemt_out(v_adc)<= '0';
--                    when others=>
--                        p_channel_frame_missalignemt_out(v_adc)<= '1'; 
                
--                end case;
                    
--                s_adc_output_word(2*v_adc) <= s_adc_input_hg_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + (c_frame_clock_phase_mux+v_frame_clock_phase)  downto (c_frame_clock_phase_mux+v_frame_clock_phase) + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                s_adc_output_word((2*v_adc)+1) <= s_adc_input_lg_temp(v_adc)(to_integer(unsigned(p_bitslip_lg_in(v_adc))) + (c_adc_bit_number-1) + (c_frame_clock_phase_mux+v_frame_clock_phase)  downto (c_frame_clock_phase_mux+v_frame_clock_phase) +(to_integer(unsigned(p_bitslip_lg_in(v_adc)))));
--                s_fc_output_word(2*v_adc)<= s_adc_fc_input_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) +(c_frame_clock_phase_mux+v_frame_clock_phase) downto (c_frame_clock_phase_mux+v_frame_clock_phase) + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));
--                s_fc_output_word(2*v_adc+1)<= s_adc_fc_input_temp(v_adc)(to_integer(unsigned(p_bitslip_hg_in(v_adc))) + (c_adc_bit_number-1) + (c_frame_clock_phase_mux+v_frame_clock_phase) downto (c_frame_clock_phase_mux+v_frame_clock_phase) + (to_integer(unsigned(p_bitslip_hg_in(v_adc)))));

--            end if;

--        end process;  -- frame capture

--    COMPONENT ila_serdes_channel
    
--    PORT (
--        clk : IN STD_LOGIC;
    
    
    
--        probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        probe3 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--        probe5 : IN STD_LOGIC_VECTOR(3 DOWNTO 0)
--    );
--    END COMPONENT  ;