----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
-- 
-- Create Date: 02/22/2019 01:10:58 PM
-- Design Name: 
-- Module Name: db6_fpga_commbus - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity db6_fpga_commbus is
    Port ( p_clknet_in : in t_db_clock_network;
           p_db_side_in : in std_logic_vector(1 downto 0);
           p_master_reset_in : in STD_LOGIC;
           p_commbus_clk_in : in STD_LOGIC;
           p_commbus_clk_out : out STD_LOGIC;
           p_commbus_rx_in : in STD_LOGIC;
           p_commbus_tx_out : out STD_LOGIC;
           p_db_reg_rx_in       : in    t_db_reg_rx;
           
           p_commbus_remote_out : out t_commbus;
           p_i2c_busy_in : in std_logic;
           p_main_st_in : in  std_logic_vector(3 downto 0);
           --p_gbt_clk_sel_in : in std_logic;
           --p_qpll1refclksel_in : in std_logic_vector(2 downto 0);
           p_leds_in : in  std_logic_vector(3 downto 0);
           --p_commbus_local_in : in t_commbus;
           p_leds_out : out  std_logic_vector(3 downto 0)
           
           );
end db6_fpga_commbus;

architecture Behavioral of db6_fpga_commbus is
signal s_commbus_remote : t_commbus;
--signal s_commbus_local : t_commbus;

signal s_tx_word_buffer : std_logic_vector(71+8 downto 0);
signal s_rx_word_buffer : std_logic_vector(72+8 downto 0);
signal s_rx_frame_pulse : std_logic :='0';

signal s_header_tx, s_header_rx : std_logic_vector(7+8 downto 0);
signal s_address_tx, s_address_rx : std_logic_vector(7 downto 0);
signal s_value_tx, s_value_rx : std_logic_vector(31 downto 0);
signal s_rx_reset : std_logic;
signal s_rx_locked : std_logic;
signal s_transmitting : std_logic;
signal s_commbus_clk80, s_commbus_clk160 : std_logic;
signal s_commbus_clk8 : std_logic;
signal s_commbus_clk_in : std_logic;
signal s_commbus_clk_out : std_logic;

--component pll_commbus_clk
--port
-- (-- Clock in ports
--  -- Clock out ports
--  p_clk80_out          : out    std_logic;
--  p_clk160_out          : out    std_logic;
--  -- Status and control signals
--  reset             : in     std_logic;
--  locked            : out    std_logic;
--  p_clk_in           : in     std_logic
-- );
--end component;

--debug
COMPONENT vio_commbus_debug
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe_in6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in9 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe_in10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

COMPONENT ila_commbus_debug

PORT (
	clk : IN STD_LOGIC;



	probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe5 : IN STD_LOGIC_VECTOR(79 DOWNTO 0);
	probe6 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	probe7 : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
);
END COMPONENT  ;
--signal s_tx_debug: std_logic;
--signal s_counter_value: std_logic_vector(7 downto 0);
--signal s_lock_counter: std_logic_vector(7 downto 0);

begin

p_commbus_remote_out <= s_commbus_remote;
p_commbus_clk_out<= s_commbus_clk_out;
p_leds_out<= s_commbus_remote.heartbeat & s_rx_frame_pulse & s_transmitting & s_commbus_remote.write_flag;


--s_header_tx(0)<=s_commbus_remote.heartbeat;
--s_header_tx(1)<=p_commbus_local_in.i2c_busy;
--s_header_tx(6)<=p_commbus_local_in.read_flag;
--s_header_tx(7)<=p_commbus_local_in.write_flag;

--s_address_tx<=p_commbus_local_in.address;
--s_value_tx<=p_commbus_local_in.value;

--s_rx_reset<= p_master_reset_in;
--p_commbus_clk_out<=p_clknet_in.clk100khz_osc;
--s_commbus_clk_in<=p_commbus_clk_in;
i_bufg_in : BUFG port map(I => p_commbus_clk_in, O => s_commbus_clk_in);

--i_bufgce_div : bufgce_div
--generic map (
--    bufgce_divide => 1, -- 1-8
--    -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
--    is_ce_inverted => '0', -- optional inversion for ce
--    is_clr_inverted => '0', -- optional inversion for clr
--    is_i_inverted => '0' -- optional inversion for i
--)
--port map (
--    o => s_commbus_clk_out, -- 1-bit output: buffer
--    ce => '1', -- 1-bit input: buffer enable
--    clr => '0', -- 1-bit input: asynchronous clear
--    i => p_clknet_in.clk100khz_osc -- 1-bit input: buffer
--);
i_bufg_out : BUFG port map(I =>  p_clknet_in.clk40_osc, O => s_commbus_clk_out);

s_commbus_remote.heartbeat <= s_rx_locked;
s_commbus_remote.qpll1refclksel<=s_header_rx(14 downto 12);
s_commbus_remote.gbt_clk_sel<=s_header_rx(15);
s_commbus_remote.main_sm<=s_header_rx(11 downto 8);
s_commbus_remote.write_flag <= s_header_rx(2);-- and s_header_rx(1) and s_header_rx(0);
s_commbus_remote.read_flag <= s_header_rx(2);--not(s_header_rx(2) and s_header_rx(1) and s_header_rx(0));
s_commbus_remote.leds <= s_header_rx(7 downto 4);
s_commbus_remote.i2c_busy <= s_header_rx(3);
s_commbus_remote.address <= s_address_rx;
s_commbus_remote.value <= s_value_rx;
s_commbus_remote.tx_busy <=s_transmitting;
s_commbus_remote.gbtx_select<= s_header_rx(1 downto 0);


--i_bufg : bufg port map (I => p_commbus_clk_in, O => s_commbus_clk_in);

--i_pll_commbus_clk : pll_commbus_clk
--   port map ( 
--  -- Clock out ports  
--   p_clk80_out => s_commbus_clk80,
--   p_clk160_out => s_commbus_clk160,
--  -- Status and control signals                
--   reset => s_pll_reset,
--   locked => s_pll_locked,
--   -- Clock in ports
--   p_clk_in => s_commbus_clk_in
-- );




--proc_detect_heartbeat: process(p_clknet_in.clk40_osc)
----variable v_rising_edge_counter, v_falling_edge_counter : integer;
----variable v_previous_clk : std_logic;
--variable v_counter : integer := 0;
--constant c_heartbeat_timeout : integer  := 1000;
--begin

--    if rising_edge(p_clknet_in.clk40_osc) then
--        s_counter_value<=std_logic_vector(to_unsigned(v_counter,8));
--        if p_master_reset_in = '0' then
--            if s_rx_locked = '1' then
--                s_commbus_remote.heartbeat<= '1';
--                v_counter:=0;
--            else
--                if v_counter > c_heartbeat_timeout then
--                    s_commbus_remote.heartbeat<= '0';
--                else
--                    v_counter:=v_counter+1;
--                end if;
                
--            end if;

--        end if;
--    end if;


--end process;


--proc_rx : process(s_commbus_clk_in)
--begin
--    if falling_edge(s_commbus_clk_in) then
--        if p_master_reset_in = '0' then
            
--            s_rx_word_buffer<=s_rx_word_buffer(71 downto 0) & p_commbus_rx_in;
        
--        else
--            s_rx_word_buffer<= (others=>'0');
--        end if;
--    end if;

--end process;

proc_rx_lock_and_decode : process(s_commbus_clk_in)
variable v_counter: integer:=0;
variable v_lock_counter: integer:=0;
constant c_lock_timeout: integer :=16;
type t_sm_lock is (st_waiting_for_lock, st_locking, st_locked_and_decoding);
variable v_sm_lock : t_sm_lock :=st_waiting_for_lock;
constant c_word_lenght: integer :=80;
constant c_extra_states: integer :=2;
begin
    
    if falling_edge(s_commbus_clk_in) then
    s_rx_word_buffer<=s_rx_word_buffer(71+8 downto 0) & p_commbus_rx_in;
--    s_counter_value<=std_logic_vector(to_unsigned(v_counter,8));
--    s_lock_counter<=std_logic_vector(to_unsigned(v_lock_counter,8));
        if p_master_reset_in = '0' then
            
            case v_sm_lock is
                when st_waiting_for_lock =>
                    s_rx_locked<='0';
                    v_lock_counter:=0;
                    v_counter:=0;
                    
                    if (s_rx_word_buffer(71+1+8 downto 68+1+8) = ((not p_db_side_in(0)) & "000")) and 
                    (s_rx_word_buffer(59+1 downto 56+1) = ((not p_db_side_in(0)) & "001")) and 
                    (s_rx_word_buffer(47+1 downto 44+1) = ((not p_db_side_in(0)) & "010")) and 
                    (s_rx_word_buffer(35+1 downto 32+1) = ((not p_db_side_in(0)) & "011")) and 
                    (s_rx_word_buffer(23+1 downto 20+1) = ((not p_db_side_in(0)) & "100")) and 
                    (s_rx_word_buffer(11+1 downto 8+1) = ((not p_db_side_in(0)) & "101")) then
                        s_rx_frame_pulse<='1';
                        v_sm_lock:=st_locking;
                        v_lock_counter:=1;
                        --v_counter:=1;
                    else
                        s_rx_frame_pulse<='0';
                        v_sm_lock:=st_waiting_for_lock;
                        v_lock_counter:=0;
                        --v_counter:=0;
                    
                    end if;
                    
                when st_locking =>
                    s_rx_locked<='0';
                    if v_counter<(c_word_lenght+c_extra_states) then
                        v_counter:=v_counter+1;
                        s_rx_frame_pulse<='0';
                    else
                        v_counter:=0;
                        if (s_rx_word_buffer(71+1+8 downto 68+1+8) = ((not p_db_side_in(0)) & "000")) and 
                        (s_rx_word_buffer(59+1 downto 56+1) = ((not p_db_side_in(0)) & "001")) and 
                        (s_rx_word_buffer(47+1 downto 44+1) = ((not p_db_side_in(0)) & "010")) and 
                        (s_rx_word_buffer(35+1 downto 32+1) = ((not p_db_side_in(0)) & "011")) and 
                        (s_rx_word_buffer(23+1 downto 20+1) = ((not p_db_side_in(0)) & "100")) and 
                        (s_rx_word_buffer(11+1 downto 8+1) = ((not p_db_side_in(0)) & "101")) then
                            s_rx_frame_pulse<='1';
                            if v_lock_counter<c_lock_timeout then
                                v_lock_counter:=v_lock_counter+1;
                            else
                                v_sm_lock:=st_locked_and_decoding;
                            end if;
                        else
                            s_rx_frame_pulse<='0';
                            v_lock_counter:=0;
                            v_sm_lock:=st_waiting_for_lock;
                        end if;
                    
                    end if;
                    
                when st_locked_and_decoding =>
                    s_rx_locked<='1';
                    
                    if v_counter<(c_word_lenght+c_extra_states) then
                        v_counter:=v_counter+1;
                        s_rx_frame_pulse<='0';
                    else
                        v_counter:=0;
                            if (s_rx_word_buffer(71+1+8 downto 68+1+8) = ((not p_db_side_in(0)) & "000")) and 
                            (s_rx_word_buffer(59+1 downto 56+1) = ((not p_db_side_in(0)) & "001")) and 
                            (s_rx_word_buffer(47+1 downto 44+1) = ((not p_db_side_in(0)) & "010")) and 
                            (s_rx_word_buffer(35+1 downto 32+1) = ((not p_db_side_in(0)) & "011")) and 
                            (s_rx_word_buffer(23+1 downto 20+1) = ((not p_db_side_in(0)) & "100")) and 
                            (s_rx_word_buffer(11+1 downto 8+1) = ((not p_db_side_in(0)) & "101")) then
                                s_rx_frame_pulse<='1';
                                s_header_rx <= s_rx_word_buffer(67+1+8 downto 60+1);
                                s_address_rx <= s_rx_word_buffer(55+1 downto 48+1);
                                if (s_rx_word_buffer(55+1 downto 48+1)) = "11111111" then
                                    s_commbus_remote.master_reset <= s_rx_word_buffer(3) and s_rx_word_buffer(2) and s_rx_word_buffer(1);
                                    s_commbus_remote.clknet_reset <= s_rx_word_buffer(6) and s_rx_word_buffer(5) and s_rx_word_buffer(4);
                                    s_commbus_remote.clknet_reset <= s_rx_word_buffer(9) and s_rx_word_buffer(8) and s_rx_word_buffer(7);
                                end if;
                                s_value_rx <= s_rx_word_buffer(43+1 downto 36+1) & s_rx_word_buffer(31+1 downto 24+1) & s_rx_word_buffer(19+1 downto 12+1) & s_rx_word_buffer(7+1 downto 0+1);
                            else
                                v_lock_counter:=0;
                                v_sm_lock:=st_waiting_for_lock;
                            end if;
                    end if;
                    
                when others=>
                    v_sm_lock:=st_waiting_for_lock;
            end case;
        else
            s_rx_word_buffer<= (others=>'0');
            s_header_rx<= (others=>'0');
            s_address_rx<= (others=>'0');
            s_value_rx<= (others=>'0');
        end if;
    end if;
    
end process;

--proc_rx_decoder : process(s_commbus_clk_in)
----variable v_counter: integer :=0;
----constant c_rx_locked_timeout : integer := 400;
--begin
--    if falling_edge(s_commbus_clk_in) then
--        if p_master_reset_in = '0' then
            
--            s_rx_word_buffer<=s_rx_word_buffer(71+8 downto 0) & p_commbus_rx_in;
            
--            if (s_rx_word_buffer(71+1+8 downto 68+1+8) = ((not p_db_side_in(0)) & "000")) and 
--            (s_rx_word_buffer(59+1 downto 56+1) = ((not p_db_side_in(0)) & "001")) and 
--            (s_rx_word_buffer(47+1 downto 44+1) = ((not p_db_side_in(0)) & "010")) and 
--            (s_rx_word_buffer(35+1 downto 32+1) = ((not p_db_side_in(0)) & "011")) and 
--            (s_rx_word_buffer(23+1 downto 20+1) = ((not p_db_side_in(0)) & "100")) and 
--            (s_rx_word_buffer(11+1 downto 8+1) = ((not p_db_side_in(0)) & "101")) then 
--                s_header_rx <= s_rx_word_buffer(67+1+8 downto 60+1);
--                s_address_rx <= s_rx_word_buffer(55+1 downto 48+1);
--                if (s_rx_word_buffer(55+1 downto 48+1)) = "11111111" then
--                    s_commbus_remote.master_reset <= s_rx_word_buffer(3) and s_rx_word_buffer(2) and s_rx_word_buffer(1);
--                    s_commbus_remote.clknet_reset <= s_rx_word_buffer(6) and s_rx_word_buffer(5) and s_rx_word_buffer(4);
--                    s_commbus_remote.clknet_reset <= s_rx_word_buffer(9) and s_rx_word_buffer(8) and s_rx_word_buffer(7);
--                end if;
--                s_value_rx <= s_rx_word_buffer(43+1 downto 36+1) & s_rx_word_buffer(31+1 downto 24+1) & s_rx_word_buffer(19+1 downto 12+1) & s_rx_word_buffer(7+1 downto 0+1);
--                --v_counter:=0;
--                --s_rx_locked<= '1';
--            else
----                if v_counter>c_rx_locked_timeout then
----                    s_rx_locked<= '0';    
----                else
----                    v_counter:=v_counter+1;
----                end if;
--            end if;
--        else
--            s_rx_word_buffer<= (others=>'0');
--            s_header_rx<= (others=>'0');
--            s_address_rx<= (others=>'0');
--            s_value_rx<= (others=>'0');
--        end if;
--    end if;
--end process;



proc_tx_encoder : process(s_commbus_clk_out)
type t_sm_tx is (st_tx, st_idle, st_update_data);
variable v_sm_tx : t_sm_tx :=st_update_data;
--variable v_tx_word_buffer : std_logic_vector(71 downto 0);
variable v_counter : integer :=0;
variable v_index : integer :=0;
constant c_word_lenght: integer :=80;
constant c_extra_states: integer :=2;

begin

    if rising_edge(s_commbus_clk_out) then
        
        
        if p_master_reset_in = '0' then
            
                    
            case v_sm_tx is

                when st_tx =>
                    if v_counter<72+8 then
                        p_commbus_tx_out<=s_tx_word_buffer(71+8-v_counter);
--                        s_tx_debug <= s_tx_word_buffer(71+8-v_counter);
                        v_counter := v_counter+1;
                        s_transmitting<='1';
                    else
                        v_sm_tx:= st_update_data;
                        v_counter := 0;
                        s_transmitting<='0';
                    end if;
                when st_update_data =>
                    s_transmitting<='0';
                    if p_db_reg_rx_in(adv_cfg_commbus_header_address)(18 downto 16) = "111" then
                        s_header_tx<= p_db_reg_rx_in(adv_cfg_commbus_header_address)(23+8 downto 16);
                        s_address_tx<= p_db_reg_rx_in(adv_cfg_commbus_header_address)(7 downto 0);
                        s_value_tx<= p_db_reg_rx_in(adv_cfg_commbus_value);
                        
                    else
                        s_header_tx(15) <= p_clknet_in.refclksel;
                        s_header_tx(14 downto 12) <=p_clknet_in.qpll1refclksel;-- p_qpll1refclksel_in;
                        s_header_tx(11 downto 8) <= p_main_st_in;
                        s_header_tx(7 downto 4) <= p_leds_in;
                        s_header_tx(3) <= p_i2c_busy_in;
                        s_header_tx(2 downto 0) <= "000";
                        s_address_tx <= std_logic_vector(to_unsigned(v_index,s_address_tx'length));
                        s_value_tx <= p_db_reg_rx_in(v_index);
                        if v_index < c_number_of_cfb_bus_regs then
                            v_index:=v_index+1;
                        else
                            v_index:=0;
                        end if;
                    end if;
                    v_sm_tx:= st_idle;
                when st_idle =>
                    s_transmitting<='0';
                    v_counter:=0;
                    v_sm_tx:= st_tx;
                    
                    s_tx_word_buffer(71+8 downto 68+8) <= p_db_side_in(0) & "000";
                    s_tx_word_buffer(67+8 downto 60) <= s_header_tx;
                    s_tx_word_buffer(59 downto 56) <= p_db_side_in(0) & "001";
                    s_tx_word_buffer(55 downto 48) <= s_address_tx;
                    s_tx_word_buffer(47 downto 44) <= p_db_side_in(0) & "010";
                    s_tx_word_buffer(43 downto 36) <= s_value_tx(31 downto 24);
                    s_tx_word_buffer(35 downto 32) <= p_db_side_in(0) & "011";
                    s_tx_word_buffer(31 downto 24) <= s_value_tx(23 downto 16);
                    s_tx_word_buffer(23 downto 20) <= p_db_side_in(0) & "100";
                    s_tx_word_buffer(19 downto 12) <= s_value_tx(15 downto 8);
                    s_tx_word_buffer(11 downto 8) <= p_db_side_in(0) & "101";
                    s_tx_word_buffer(7 downto 0) <= s_value_tx(7 downto 0);
                                                                            
                    
                when others =>
                    v_sm_tx:= st_idle;
                    
            end case;
        
        
        else
            v_sm_tx:= st_idle;
            v_counter := 0;
        end if;
    end if;
end process;

--i_vio_commbus_debug : vio_commbus_debug
--  PORT MAP (
--    clk => p_clknet_in.clk40_osc,
--    probe_in0(0) => s_commbus_remote.heartbeat,
--    probe_in1(0) => p_db_side_in(0),
--    probe_in2 => s_commbus_remote.main_sm,
--    probe_in3(0) => s_commbus_remote.gbt_clk_sel,
--    probe_in4 => s_commbus_remote.qpll1refclksel,
--    probe_in5 => s_commbus_remote.leds,
--    probe_in6(0) => s_commbus_remote.i2c_busy,
--    probe_in7(0) => s_commbus_remote.write_flag,
--    probe_in8(0) => s_commbus_remote.read_flag,
--    probe_in9 => s_commbus_remote.address,
--    probe_in10 => s_commbus_remote.value
--  );


--i_ila_commbus_debug : ila_commbus_debug
--PORT MAP (
--	clk => p_clknet_in.clk40_osc,
--	probe0(0) => p_clknet_in.clk8_osc, 
--	probe1(0) => p_commbus_clk_in, 
--	probe2(0) => s_rx_frame_pulse, 
--	probe3(0) => s_transmitting, 
--	probe4(0) => s_commbus_remote.heartbeat,
--	probe5 => s_rx_word_buffer(80 downto 1),
--	probe6 => s_counter_value,
--	probe7 => s_lock_counter
--);

end Behavioral;
