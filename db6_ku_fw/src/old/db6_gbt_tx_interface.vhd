
-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
library gbt;
use gbt.gbt_bank_package.all;
use gbt.vendor_specific_gbt_bank_package.all;
use gbt.gbt_banks_user_setup.all;

library tilecal;
use tilecal.db6_design_package.all;
use tilecal.tile_link_crc.all;

entity db6_gbt_tx_interface_v1 is      
	port (       
      -- General reset:
      GENERAL_RESET_I                             : in  std_logic;
                                                  
      -- Manual resets:                                                       
      GT0_MANUAL_RESET_TX_I                           : in  std_logic;
      GT1_MANUAL_RESET_TX_I                           : in  std_logic;
                      
      -- Clocks scheme --                                 
        CLKNET                                    : in t_db_clock_network;
        CLKIN_I                                   : in t_db_gty_clock_inputs;

      -- Serial lanes --
		-- GT0 link GTX pin pairs
	    GT0_TX_P_O  											: out std_logic;
		GT0_TX_N_O  											: out std_logic;
		
		-- GT1 link GTX pin pairs
 	    GT1_TX_P_O  											: out std_logic;
		GT1_TX_N_O  											: out std_logic;
		  
		-- GT0 link GBT interface port --
		GT0_GBT_TX_DATA_LG_I 									: in  std_logic_vector (115 downto 0);
		GT0_GBT_TX_DATA_HG_I 									: in  std_logic_vector (115 downto 0);
		-- GT01link GBT interface port --
		GT1_GBT_TX_DATA_LG_I 									: in  std_logic_vector (115 downto 0);
		GT1_GBT_TX_DATA_HG_I 									: in  std_logic_vector (115 downto 0);                      
      -- General control --                       

      GT0_MGT_READY_O                                 : out std_logic;
      GT1_MGT_READY_O                                 : out std_logic;

      GT0_RX_FSMRESET_DONE_O                          : out std_logic;        
                                               
      TX_FRAMECLK_O                               : out std_logic;
      TX_WORDCLK_O                                : out std_logic
   );
end db6_gbt_tx_interface_v1;

architecture structural of db6_gbt_tx_interface_v1 is  

   --=========--
   -- TX data --
   --=========--   
   
   signal GTYTXN, GTYTXP                    : std_logic_vector(1 downto 0);
   
   signal GT0_GBT_TX_DATA                   : std_logic_vector(115 downto 0);
   signal Scrambled_TX_DATA                    : std_logic_vector(115 downto 0);
   signal TX_FRAME                         : std_logic_vector(119 downto 0);
   signal txheader                         : std_logic_vector( 3 downto 0);
   signal TX_FRAME_240                      : std_logic_vector(239 downto 0);
   signal TX_FRAME_240_D                    : std_logic_vector(239 downto 0);
   signal TX_FRAME0_TMP                     : std_logic_vector(119 downto 0);
  
   signal gty_txactive                      : std_logic; -- gty transmitter active
   signal gty_usrclk                        : std_logic; -- 120 MHz
   signal gty_usrclk2                       : std_logic; -- 120 MHz
                                                  
   signal txgearbox_out_0                    : std_logic_vector(79 downto 0);
   signal txgearbox_out_1                    : std_logic_vector(79 downto 0);
   signal gty_inputbus                      : std_logic_vector(159 downto 0);

   signal reset_scrambler                   : std_logic := '0';
   signal reset_encoder                     : std_logic := '0';
   signal reset_gearbox                     : std_logic := '0';

   signal gearbox_aligned                   : std_logic := '0';
   signal gearbox_aligned_last              : std_logic := '0';

   --========================--                   
   -- GBT Bank clocks scheme --                   
   --========================--                   
 
   signal txFrameClk_from_txPll                   	: std_logic;   
   signal txFrameClk_from_rxPll                   	: std_logic;   
   signal txwordClk_from_txPll                   	: std_logic;   
   
   signal mgt_refclk_i                              : std_logic_vector(1 downto 0);

   signal gain_select_d, gain_select : std_logic := '0';  -- 1 for LG, 0 for hg

   signal qpllrefclksel : std_logic_vector(2 downto 0);


    -- test signals
--    signal crc : std_logic_vector(10 downto 0);
--    signal dummy_input : std_logic_vector(119 downto 0);
--    constant dummy_bcid : std_logic_vector(4 downto 0) := "11100";
--    constant test_pattern : std_logic_vector(99 downto 0) := 
--    "0100110001110000111100000111110000001111110000000111111100000000111111110000000001111111110101010101";

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

--    crc <= tile_link_crc_compute(test_pattern)(10 downto 0);
--    dummy_input <= txheader & test_pattern & dummy_bcid & crc;

    GT0_TX_P_O <= GTYTXP(0); 
    GT0_TX_N_O <= GTYTXN(0); 
    GT1_TX_P_O <= GTYTXP(1); 
    GT1_TX_N_O <= GTYTXN(1); 

    GT0_MGT_READY_O <= gty_txactive;
    GT1_MGT_READY_O <= gty_txactive;

-- Derive gain select signal from phase-shifted 40 MHz clocks 

select_gain_d : process(CLKNET.clk80)
begin
    if falling_edge(CLKNET.clk80) then
        gain_select_d <= CLKNET.clk40;
    end if; -- clock edge
end process; 

select_gain : process(CLKNET.clk80) -- 0 selects LG, 1 selects HG
begin
    if rising_edge(CLKNET.clk80) then
        gain_select <= gain_select_d;
    end if; -- clock edge
end process; 

-- Start with the scrambler. For now we only use the GT0 input data (assuming GT0 and GT1 are identical streams

multiplex_gain : process(gain_select)
begin
    if gain_select = '0' then
        GT0_GBT_TX_DATA <= GT0_GBT_TX_DATA_LG_I;
    else
        GT0_GBT_TX_DATA <= GT0_GBT_TX_DATA_HG_I;
    end if;
end process;

scrambler: entity gbt.gbt_tx_scrambler
   generic map(   
      GBT_BANK_ID => 1,  
		NUM_LINKS => 1,
		TX_OPTIMIZATION	=> STANDARD,
		TX_ENCODING	=> WIDE_BUS )
  port map (
      -- Reset & Clock --   
       TX_RESET_I   =>  reset_scrambler,
       TX_FRAMECLK_I => CLKNET.clk80,               
      -- Control --                                 
      TX_ISDATA_SEL_I => '1',     
      TX_HEADER_O =>  txheader, 
      --==============--           
      -- Data & Frame --           
      --==============--     
      -- Common:
      TX_DATA_I                => GT0_GBT_TX_DATA(83 downto 0),
      TX_COMMON_FRAME_O        => Scrambled_TX_DATA(83 downto 0),
      -- Wide-Bus:    
      TX_EXTRA_DATA_WIDEBUS_I     => GT0_GBT_TX_DATA(115 downto 84),
      TX_EXTRA_FRAME_WIDEBUS_O    => Scrambled_TX_DATA(115 downto 84)
   );
   
-- Next step is the encoder. We assume wide bus encoding on transmitter and receiver.
-- The TX output frame is 120 bits wide at 80 MHz 

 encoder: entity gbt.gbt_tx_encoder
      generic map(   
         GBT_BANK_ID => 1,  
           NUM_LINKS => 1,
           TX_OPTIMIZATION => STANDARD,
           RX_OPTIMIZATION => STANDARD,
           TX_ENCODING => WIDE_BUS,
           RX_ENCODING  => WIDE_BUS   
      )
      port map(
         -- Reset & Clocks --
         TX_RESET_I  => reset_encoder,
         TX_FRAMECLK_I    => CLKNET.clk80,
         -- Frame header --
         TX_HEADER_I     => txheader,      
         -- Frame --        
          TX_COMMON_FRAME_I   => Scrambled_TX_DATA( 83 downto 0),      
          TX_EXTRA_FRAME_WIDEBUS_I => Scrambled_TX_DATA( 115 downto 84),  
          TX_FRAME_O   => TX_FRAME_240_D(239 downto 120)
      );

-- Next step is to put the two 120 bit frames in parallel (240 bits total)

frame_shift : process (CLKNET.clk80)
begin
    if rising_edge(CLKNET.clk80) then
        TX_FRAME_240_D (119 downto 0) <= TX_FRAME_240_D(239 downto 120);
    end if;
end process; -- frame_0

frame_1 : process (CLKNET.clk40)
begin
    if falling_edge(CLKNET.clk40) then -- Latency shift of 12.5 ns from pipelined algorithm
        TX_FRAME_240 <= TX_FRAME_240_D;
    end if;
end process; -- frame_1

-- Finally, serialize the 240-bit frames to 80 bits at 120 MHz

tx_gearbox_0: entity gbt.gbt_tx_gearbox_latopt
   port map(
     -- Reset:
      TX_RESET_I        => reset_gearbox,
      -- Clocks:
      TX_WORDCLK_I      => gty_usrclk2, -- 120 MHz clock,
      TX_FRAMECLK_I     => CLKNET.clk40,
     -- Control --
      TX_MGT_READY_I    => gty_txactive,
 		-- Status  --
		TX_GEARBOX_READY_O		=> open,
		TX_PHALIGNED_O			=> gearbox_aligned,
		TX_PHCOMPUTED_O			=> open,
      -- Frame & Word --
      TX_FRAME_I                => TX_FRAME_240,
      TX_WORD_O                 => txgearbox_out_0
   );


-- Reference clock selection for the GTY transceivers.
--   Local reference clock is refclk(1), remote is refclk(0).
--   The refclksel signal is 0 for remote, 1 for local.

refclksel : process (CLKNET.refclksel)

begin
    case CLKNET.refclksel is        -- 0 for remote, 1 for local
        when '0' =>
            qpllrefclksel <= "001"; -- gtreflclk0 (remote)
        when others =>
            qpllrefclksel <= "010"; -- gtrefclk1 (local)
    end case;
end process; -- refclksel

 gty_inputbus <= txgearbox_out_0 & txgearbox_out_0;
 
 gty_inst : entity tilecal.db6_gty_tx_wrapper_v1
 
  PORT MAP (
   clk40 => CLKNET.clk40,
   oscclk => CLKNET.clk40_osc,
   tx_reset_in => GENERAL_RESET_I,
   userclk_tx_srcclk_out => open,
   userclk_tx_usrclk_out  => gty_usrclk,
   userclk_tx_usrclk2_out => gty_usrclk2,
   userclk_tx_active_out => gty_txactive,
   userdata_tx_in => gty_inputbus,
   gtrefclk0_in => CLKIN_I.gty_refclk_rem(0),
   gtrefclk1_in => CLKIN_I.gty_refclk_loc(0),
   qpll1refclksel_in => qpllrefclksel,
   gtytxn_out=> GTYTXN,
   gtytxp_out => GTYTXP
 
 );
 
 TX_WORDCLK_O <= gty_usrclk2;
 
 gbt_reset_controller : process(CLKNET.clk40)
    type reset_state is (idle, init_gearbox, init_scr_enc);
    variable state : reset_state := idle;
 begin
    if rising_edge(CLKNET.clk40) then
        gearbox_aligned_last <= gearbox_aligned;
        case state is
            when init_gearbox =>
                reset_scrambler <= '1';
                reset_encoder   <= '1';
                reset_gearbox   <= '1';
                state := idle;
            when init_scr_enc =>
                reset_scrambler <= '1';
                reset_encoder   <= '1';
                reset_gearbox   <= '0';
                state := idle;
            when others => -- idle
                reset_scrambler <= '0';
                reset_encoder   <= '0';
                reset_gearbox   <= '0';
                if GENERAL_RESET_I = '1' then
                    state := init_gearbox;
                elsif gearbox_aligned_last = '0' and gearbox_aligned = '1' then
                    state := init_scr_enc;
                else
                    state := idle;
                end if;
        end case;              
    end if; -- clock edge;
 end process;
 
end structural;
