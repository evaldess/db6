----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
-- 
-- Create Date: 02/22/2019 01:10:58 PM
-- Design Name: 
-- Module Name: db6_fpga_commbus_v1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity db6_fpga_commbus_v1 is
    Port ( p_clknet_in : in t_db_clock_network;
           p_db_side_in : in std_logic_vector(1 downto 0);
           p_master_reset_in : in STD_LOGIC;
           p_commbus_rx_in : in STD_LOGIC;
           p_commbus_tx_out : out STD_LOGIC;
           p_db_reg_rx_in       : in    t_db_reg_rx;
           
           p_commbus_remote_out : out t_commbus;
           --p_commbus_local_in : in t_commbus;
           p_leds_out : out  std_logic_vector(3 downto 0)
           
           );
end db6_fpga_commbus_v1;

architecture Behavioral of db6_fpga_commbus_v1 is
signal s_commbus_remote : t_commbus;
--signal s_commbus_local : t_commbus;

signal s_tx_word_buffer : std_logic_vector(71 downto 0);
signal s_rx_word_buffer : std_logic_vector(72 downto 0);

signal s_transmitting : std_logic;
signal s_header_tx, s_header_rx : std_logic_vector(7 downto 0);
signal s_address_tx, s_address_rx : std_logic_vector(7 downto 0);
signal s_value_tx, s_value_rx : std_logic_vector(31 downto 0);
signal s_rx_locked : std_logic;

--component pll_commbus_clk
--port
-- (-- Clock in ports
--  -- Clock out ports
--  p_clk80_out          : out    std_logic;
--  p_clk160_out          : out    std_logic;
--  -- Status and control signals
--  reset             : in     std_logic;
--  locked            : out    std_logic;
--  p_clk_in           : in     std_logic
-- );
--end component;

--debug
COMPONENT vio_configbus_debug
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(71 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out0 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe_out1 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe_out2 : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe_out3 : OUT STD_LOGIC_VECTOR(71 DOWNTO 0)
  );
END COMPONENT;

COMPONENT ila_commbus_debug

PORT (
	clk : IN STD_LOGIC;



	probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
	probe5 : IN STD_LOGIC_VECTOR(71 DOWNTO 0)
);
END COMPONENT  ;
signal s_tx_debug: std_logic;
signal s_counter_value: std_logic_vector(15 downto 0);

begin


p_commbus_remote_out <= s_commbus_remote;

p_leds_out<= p_clknet_in.clk100khz_osc & s_commbus_remote.heartbeat &s_transmitting& (s_commbus_remote.write_flag or s_commbus_remote.read_flag);

s_commbus_remote.heartbeat<=s_rx_locked;
s_commbus_remote.write_flag <= s_header_rx(2) and s_header_rx(1) and s_header_rx(0);
s_commbus_remote.i2c_busy <= s_header_rx(3);
s_commbus_remote.address <= s_address_rx;
s_commbus_remote.value <= s_value_rx;

proc_rx_decoder : process(p_clknet_in.clk100khz_osc)
variable v_counter: integer :=0;
constant c_rx_locked_timeout : integer := 400;
begin
    if falling_edge(p_clknet_in.clk100khz_osc) then
        if p_master_reset_in = '0' then
            
            s_rx_word_buffer<=s_rx_word_buffer(71 downto 0) & p_commbus_rx_in;
            
            if (s_rx_word_buffer(71+1 downto 68+1) = ((not p_db_side_in(0)) & "000")) and 
            (s_rx_word_buffer(59+1 downto 56+1) = ((not p_db_side_in(0)) & "001")) and 
            (s_rx_word_buffer(47+1 downto 44+1) = ((not p_db_side_in(0)) & "010")) and 
            (s_rx_word_buffer(35+1 downto 32+1) = ((not p_db_side_in(0)) & "011")) and 
            (s_rx_word_buffer(23+1 downto 20+1) = ((not p_db_side_in(0)) & "100")) and 
            (s_rx_word_buffer(11+1 downto 8+1) = ((not p_db_side_in(0)) & "101")) then 
                s_header_rx <= s_rx_word_buffer(67+1 downto 60+1);
                s_address_rx <= s_rx_word_buffer(55+1 downto 48+1);
                s_value_rx <= s_rx_word_buffer(43+1 downto 36+1) & s_rx_word_buffer(31+1 downto 24+1) & s_rx_word_buffer(19+1 downto 12+1) & s_rx_word_buffer(7+1 downto 0+1);
                v_counter:=0;
                s_rx_locked<= '1';
            else
                if v_counter>c_rx_locked_timeout then
                    s_rx_locked<= '0';    
                else
                    v_counter:=v_counter+1;
                end if;
            end if;
            
        else
            s_rx_word_buffer<= (others=>'0');
            s_header_rx<= (others=>'0');
            s_address_rx<= (others=>'0');
            s_value_rx<= (others=>'0');
        end if;
    end if;
end process;



proc_tx_encoder : process(p_clknet_in.clk100khz_osc)
type t_sm_tx is (st_tx, st_idle, st_update_data);
variable v_sm_tx : t_sm_tx :=st_update_data;
--variable v_tx_word_buffer : std_logic_vector(71 downto 0);
variable v_counter : integer :=0;
variable v_index : integer :=0;

begin

    if rising_edge(p_clknet_in.clk100khz_osc) then
    
        
        if p_master_reset_in = '0' then
            
                    
            case v_sm_tx is

                when st_tx =>
                    if v_counter<72 then
                        p_commbus_tx_out<=s_tx_word_buffer(71-v_counter);
                        s_tx_debug <= s_tx_word_buffer(71-v_counter);
                        v_counter := v_counter+1;
                        s_transmitting<='1';
                    else
                        v_sm_tx:= st_update_data;
                        v_counter := 0;
                        s_transmitting<='0';
                    end if;
                when st_update_data =>
                    s_transmitting<='0';
                    if p_db_reg_rx_in(adv_cfg_commbus_header_address)(18 downto 16) = "111" then
                        s_header_tx<= p_db_reg_rx_in(adv_cfg_commbus_header_address)(23 downto 16);
                        s_address_tx<= p_db_reg_rx_in(adv_cfg_commbus_header_address)(7 downto 0);
                        s_value_tx<= p_db_reg_rx_in(adv_cfg_commbus_value);
                    else
                        s_address_tx <= std_logic_vector(to_unsigned(v_index,8));
                        s_value_tx <= p_db_reg_rx_in(v_index);
                        if v_index < c_number_of_cfb_bus_regs then
                            v_index:=v_index+1;
                        else
                            v_index:=0;
                        end if;
                    end if;
                    v_sm_tx:= st_idle;
                when st_idle =>
                    s_transmitting<='0';
                    v_counter:=0;
                    v_sm_tx:= st_tx;
                    
                    s_tx_word_buffer(71 downto 68) <= p_db_side_in(0) & "000";
                    s_tx_word_buffer(67 downto 60) <= s_header_tx;
                    s_tx_word_buffer(59 downto 56) <= p_db_side_in(0) & "001";
                    s_tx_word_buffer(55 downto 48) <= s_address_tx;
                    s_tx_word_buffer(47 downto 44) <= p_db_side_in(0) & "010";
                    s_tx_word_buffer(43 downto 36) <= s_value_tx(31 downto 24);
                    s_tx_word_buffer(35 downto 32) <= p_db_side_in(0) & "011";
                    s_tx_word_buffer(31 downto 24) <= s_value_tx(23 downto 16);
                    s_tx_word_buffer(23 downto 20) <= p_db_side_in(0) & "100";
                    s_tx_word_buffer(19 downto 12) <= s_value_tx(15 downto 8);
                    s_tx_word_buffer(11 downto 8) <= p_db_side_in(0) & "101";
                    s_tx_word_buffer(7 downto 0) <= s_value_tx(7 downto 0);
                                                                            
                    
                when others =>
                
            end case;
        
        
        else
            v_sm_tx:= st_idle;
            v_counter := 0;
        end if;
    end if;
end process;

i_vio_configbus_debug : vio_configbus_debug
  PORT MAP (
    clk => p_clknet_in.clk40_osc,
    probe_in0 => s_header_rx,
    probe_in1 => s_address_rx,
    probe_in2 => s_value_rx,
    probe_in3 => s_rx_word_buffer(72 downto 1),
    probe_in4(15 downto 0) => s_counter_value,
    probe_in4(71 downto 16) => s_tx_word_buffer(71 downto 16),
    probe_in5(0) => s_commbus_remote.heartbeat,
    probe_in6(0) => s_rx_locked,
    probe_out0 => s_header_tx,
    probe_out1 => s_address_tx,
    probe_out2 => s_value_tx,
    probe_out3 => open
  );

i_ila_commbus_debug : ila_commbus_debug
PORT MAP (
	clk => p_clknet_in.clk40_osc,
	probe0(0) => s_rx_locked, 
	probe1(0) => s_rx_locked, 
	probe2(0) => p_commbus_rx_in, 
	probe3(0) => s_tx_debug, 
	probe4(0) => s_commbus_remote.heartbeat,
	probe5 => s_rx_word_buffer(72 downto 1)
);

end Behavioral;
