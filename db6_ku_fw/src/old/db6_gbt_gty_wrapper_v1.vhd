----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/08/2019 05:46:47 PM
-- Design Name: 
-- Module Name: db_gbt_gty_wrapper_v1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
-- XILINX libraries --
library UNISIM;
use UNISIM.VCOMPONENTS.all;

-- Custom libraries and packages:
library gbt;
use gbt.gbt_bank_package.all;
use gbt.vendor_specific_gbt_bank_package.all;
use gbt.gbt_banks_user_setup.all;

library tilecal;
use tilecal.db6_design_package.all;
use tilecal.tile_link_crc.all;


entity db6_gbt_gty_wrapper_v1 is
  port(
    -- signals for system operation only --
    PUSH_BUTTON_IN       : in  std_logic_vector(2 downto 0);
    DB_SIDE              : in  std_logic_vector(1 downto 0);
	 -- Main SFP signals --
    TXN_OUT              : out std_logic_vector(1 downto 0);
    TXP_OUT              : out std_logic_vector(1 downto 0);
    RXREADY              : in std_logic_vector(1 downto 0);
    LED_OUT              : out std_logic_vector(3 downto 0);
    -- signals available to the user --
    -- off-detector communication --
    CLKIN_I             : in t_db_gty_clock_inputs;
    clknet_i            : in t_db_clock_network;
    --CLKNET_O            : out t_db_clock_network;
    -- Configuration registers
    DB_config_reg       : in t_db_reg_rx;
    p_gbt_tx_data_in                          : in t_gbt_tx_data;
    
--    GBT0_TX_IN_LG           		: in  gbt_txbus;
--    GBT0_TX_IN_HG           		: in  gbt_txbus;
--    GBT1_TX_IN_LG           		: in  gbt_txbus;
--    GBT1_TX_IN_HG                   : in  gbt_txbus;

    -- Monitoring_signals
    GBT_WORDCLK_O   : out std_logic -- 120 MHz GTY word clock
    );

end db6_gbt_gty_wrapper_v1;

architecture RTL of db6_gbt_gty_wrapper_v1 is

  -- master reset signal --
  signal ibuf_ce_o					 	: std_logic := '0';
  signal startup_master_reset_i 		: std_logic := '0';
  signal gbt_reset               		: std_logic := '0';
  
  -- CPLL signals of transceiver GTY0 --
  signal cpllreset_i          : std_logic;

  signal gt0_gbt_tx_data_lg_i  : std_logic_vector (115 downto 0);
  signal gt0_gbt_tx_data_hg_i  : std_logic_vector (115 downto 0);
  signal gt1_gbt_tx_data_lg_i  : std_logic_vector (115 downto 0);
  signal gt1_gbt_tx_data_hg_i  : std_logic_vector (115 downto 0);
  
  -- error detect signals --
  signal gt0_error_count_reset_i : std_logic;
  signal gt0_error_count_o       : std_logic_vector(15 downto 0);
  signal gt0_error_count_time_o  : std_logic_vector(19 downto 0);
  signal gt1_error_count_reset_i : std_logic;
  signal gt1_error_count_o       : std_logic_vector(15 downto 0);
  signal gt1_error_count_time_o  : std_logic_vector(19 downto 0);

  -- FMC IO signals --
  signal g_bus_signal_i : std_logic_vector(11 downto 0) := (others => '0');
  signal g_bus_signal_o : std_logic_vector(11 downto 0) := (others => '0');
  signal h_bus_signal_i : std_logic_vector(11 downto 0) := (others => '0');
  signal h_bus_signal_o : std_logic_vector(11 downto 0) := (others => '0');
  signal j_bus_signal_i : std_logic_vector(11 downto 0) := (others => '0');
  signal j_bus_signal_o : std_logic_vector(11 downto 0) := (others => '0');
  signal k_bus_signal_i : std_logic_vector(11 downto 0) := (others => '0');
  signal k_bus_signal_o : std_logic_vector(11 downto 0) := (others => '0');

  -- Data generation signals --
  signal counter_reset_i     : std_logic;
  signal gt0_counter_start_i : std_logic;
  signal gt1_counter_start_i : std_logic;
  signal gt0_temp_counter_o  : std_logic_vector(7 downto 0);
  signal gt1_temp_counter_o  : std_logic_vector(7 downto 0);

  	
	-- CLOCK RELATED SIGNALS
	signal CLK_FEEDBACK							: std_logic;
	signal LOCKED									: std_logic;
 	signal fabricClk_from_userClockIbufgds : std_logic;    
	signal txframeclkfromgbt					: std_logic;
	signal txwordclkfromgbt 					: std_logic;
	signal refclk_mmcm_loc						: std_logic;
	

	----  RESET ----
	signal reset_from_genRst					: std_logic;
	signal gt0_manualResetTx_from_user		: std_logic;
   signal gt0_manualResetRx_from_user    	: std_logic;
	signal gt1_manualResetTx_from_user		: std_logic;
   signal gt1_manualResetRx_from_user		: std_logic;
	
	---- STATUS SIGNAL ----
   -- PLLs status
	
	signal refclk_mmcm_lock						: std_logic;
	signal txFrameClkPllLocked_from_gbt 		: std_logic; --CS
	signal txFrameClkPllLocked_from_gbt_QSFP1 : std_logic; --CS
	signal txFrameClkPllLocked_from_gbt_QSFP2 : std_logic; --CS

	signal gt0_mgtReady_from_gbt  				: std_logic;
	signal gt1_mgtReady_from_gbt  				: std_logic;

	signal gt0_testPatterSel_from_user  : std_logic_vector(1 downto 0):="10";
	signal gt1_testPatterSel_from_user  : std_logic_vector(1 downto 0):="10";
	signal gt0_txIsDataSel_from_user    : std_logic;
	signal gt1_txIsDataSel_from_user    : std_logic;
		
	
	-- SPI Interface
	signal spi_reset_i											: std_logic;
	signal spi_data_in_i										: std_logic_vector(31 downto 0);
	signal spi_data_out_i										: std_logic_vector(31 downto 0);
	signal spi_clk_out_i										: std_logic;
	signal init_spi_transfer_i									: std_logic;
	signal spi_transfer_done_i									: std_logic;
	signal spi_mosi_i											: std_logic;
	signal spi_miso_i											: std_logic;
	signal spi_le_i												: std_logic;
	signal spi_clk_i											: std_logic;
	signal spi_clk_buf											: std_logic;
	
	signal spi_operation_i										: std_logic;
	
	signal reset_tx			                             	    : std_logic_vector(1 downto 0);

	signal CLKFB_REC											: std_logic;
	
	-----------------------------------------------------------------------------------------------
	--============   GBT3 remote configuration and control  ===============--
	-----------------------------------------------------------------------------------------------
	signal GeneralResetFromsROD										: std_logic;

	signal PatternSelectFromsRODLink1									: std_logic;
	signal PatternSelectFromsRODLink2									: std_logic;

	signal ResetGbtTXfromsRODLink1										: std_logic;
	signal ResetGbtTXfromsRODLink2										: std_logic;	
	
	------ STATE MACHINE RESET ------
	type states is (INIT_ST, GBT_GENERAL_RESET_ST, RESET_DONE_ST);
	signal state : states := INIT_ST;
	
	
	constant TIME_CONSTANT_RESET_DURATION				: integer := 5;  
	signal Timer_reset,Timer_reset_rx1,Timer_reset_rx2 : integer range 0 to TIME_CONSTANT_RESET_DURATION;

--------fernando 02/09/2015--------------

signal DB_REG_3_IN : std_logic_vector(31 downto 0) := (others => '0');
signal DB_REG_3_IN_delayed	: std_logic_vector(31 downto 0):=(others=>'0');


signal fsm_pwdn :std_logic := '0';
signal fsm_rst_global :std_logic := '0';


-- Local clock signals

signal CLKNET : t_db_clock_network;
signal OSCCLK_IN : std_logic;
signal gbt_clk_sel, gbt_clk40_mmcm_reset : std_logic;
signal lastinput : integer := 0; -- GBTX RXREADY monitoring

signal clkin1_db_gbuf, clkin1_mb0_gbuf, clkin1_mb1_gbuf, osc_gbuf : std_logic;
signal clkin2_db_gbuf, clkin2_mb0_gbuf, clkin2_mb1_gbuf : std_logic;

signal refclock_O : std_logic_vector(1 downto 0);


-- Reset signals to 40 MHz MMCMs

signal watchdog_db_reset, watchdog_mb0_reset, watchdog_mb1_reset : std_logic := '0';
signal reset_mmcm_db, reset_mmcm_mb0, reset_mmcm_mb1             : std_logic := '0';



begin  -- RTL
 --===============================================================================================--
 ------------------------------       COMMANDS AND DATA MODE FROM SROD  ----------------------------
 --===============================================================================================--
 
 
DB_REG_3_IN <= DB_config_reg.db_strobe_reg; --   Temporary address. We need to assign GBTx reset strobes
 
--- STATE MACHINE TO HANDLE A GENERAL RESET FROM sROD -----
process(OSCCLK_IN,reset_from_genRst)
begin
	if rising_edge(OSCCLK_IN) then -- 100 MHz oscillator clock (connected below)
			case state is
				when INIT_ST=> 	
						GeneralResetFromsROD <= '0';
						Timer_reset			<= 0;
						if ((DB_REG_3_IN(3) ='1' and DB_side=x"01") or (DB_REG_3_IN(4) ='1' and DB_side=x"10") ) then
							state 				<= GBT_GENERAL_RESET_ST;
						else
							state				<= INIT_ST;
						end if;
				
				when GBT_GENERAL_RESET_ST=>
						GeneralResetFromsROD <= '1';
					if (Timer_reset = TIME_CONSTANT_RESET_DURATION) then
						state <= RESET_DONE_ST;
					else
						Timer_reset <= Timer_reset + 1;
						state <= GBT_GENERAL_RESET_ST;
					end if;			

			  when RESET_DONE_ST=> 
					GeneralResetFromsROD <= '0';
					if ((DB_REG_3_IN(3) ='1' and DB_side=x"01") or ((DB_REG_3_IN(4) ='1' and DB_side=x"10")) )then  
						if (gt0_mgtReady_from_gbt='1' and gt1_mgtReady_from_gbt='1') then
							state					<= INIT_ST;
						else
							state					<= RESET_DONE_ST;
						end if;
					end if;
			end case;
		end if;
end process;


  --##################################################################--
  --##            			GBT and GTY modules  			    	##--
  --##################################################################--

 GBT_WORDCLK_O <= txWordClkfromGbt;
-- THIS NEEDS TO BE UPDATED FOR THE NEW GBT --

  gt0_gbt_tx_data_lg_i <= p_gbt_tx_data_in.lg;
  gt0_gbt_tx_data_hg_i <= p_gbt_tx_data_in.hg;
  gt1_gbt_tx_data_lg_i <= p_gbt_tx_data_in.lg;
  gt1_gbt_tx_data_hg_i <= p_gbt_tx_data_in.hg;


  ----------------------------------------------------------------------------
  ---=============  Instantiation of the clocking network  ===============----
  ----------------------------------------------------------------------------
  
  --CLKNET_O <= CLKNET;
  clknet<=clknet_i;
  spi_clk_i <= CLKNET.clk10_osc;
  OSCCLK_IN <= CLKNET.clk100_osc;
  
--  oscillator : mmcm_osc  -- 100 MHz oscillator provides 10 and 100 MHz
--  port map
--  (
--    CLK_IN1 => osc_gbuf,
--    CLK_100 => CLKNET.clk100_osc,
--    CLK_10 =>  CLKNET.clk10_osc
--  );
  
--  osc_buf : bufg
--  port map (I => CLKIN_I.osc, O => osc_gbuf);
  
--  gbt_ck40_loc : mmcm_gbt40_loc  -- 40 MHz GBT clock for local FPGA logic 
--   port map
--  (
--    CLK_IN1 => clkin1_db_gbuf,
--    CLK_IN2 => clkin2_db_gbuf,
--    CLK_IN_SEL => gbt_clk_sel,
--    RESET => gbt_clk40_mmcm_reset,
--    CLK_40 =>  CLKNET.clk40db,
--    CLK_80 =>  CLKNET.clk80,
--    CLK_120 => CLKNET.clk120,
--    CLK_160 => CLKNET.clk160
--  );

    reset_mmcm_db <= gbt_clk40_mmcm_reset or watchdog_db_reset;

--  clkin1_db_buf : bufg
--  port map (I => CLKIN_I.clkin1_db, O => clkin1_db_gbuf);
--  clkin2_db_buf : bufg
--  port map (I => CLKIN_I.clkin2_db, O => clkin2_db_gbuf);

  
--  gbt_ck40_MB0 : mmcm_gbt40_loc
--   port map
--  (
--    CLK_IN1 => clkin1_mb0_gbuf,
--    CLK_IN2 => clkin2_mb0_gbuf,
--    CLK_IN_SEL => gbt_clk_sel,
--    RESET => gbt_clk40_mmcm_reset,
--    CLK_40 =>  CLKNET.clk40mb(0),
--    CLK_80 =>  open,
--    CLK_120 =>  open,
--    CLK_160 => open
--  );

    reset_mmcm_mb0 <= gbt_clk40_mmcm_reset or watchdog_mb0_reset;

--  clkin1_mb0_buf : bufg
--  port map (I => CLKIN_I.clkin1_mb0, O => clkin1_mb0_gbuf);
--  clkin2_mb0_buf : bufg
--  port map (I => CLKIN_I.clkin2_mb0, O => clkin2_mb0_gbuf);


--  gbt_ck40_MB1 : mmcm_gbt40_loc
--   port map
--  (
--    CLK_IN1 => clkin1_mb1_gbuf,
--    CLK_IN2 => clkin2_mb1_gbuf,
--    CLK_IN_SEL => gbt_clk_sel,
--    RESET => gbt_clk40_mmcm_reset,
--    CLK_40 =>  CLKNET.clk40mb(1),
--    CLK_80 =>  open,
--    CLK_120 => open,
--    CLK_160 => open
--  );
 
     reset_mmcm_mb1 <= gbt_clk40_mmcm_reset or watchdog_mb1_reset;
 
--  clkin1_mb1_buf : bufg
--  port map (I => CLKIN_I.clkin1_mb1, O => clkin1_mb1_gbuf);
--  clkin2_mb1_buf : bufg
--  port map (I => CLKIN_I.clkin2_mb1, O => clkin2_mb1_gbuf);

 
--  CLKNET.refclk <= CLKIN_I.refclock_O;
     
--  CLKNET.refclksel <= gbt_clk_sel;
 
 ---------------------------------------------------
 -- Dynamic selection of 40 MHz MMCM clock inputs --
 ---------------------------------------------------
  
 gbt_clock_selection : process(CLKNET.clk10_osc)
 
    variable input : integer := 0; -- 1 for local, 2 for remote, 3 for none
    type clocksel_state is (idle, set_source, reset, release);
    variable state: clocksel_state := idle;
    
begin
    if rising_edge (CLKNET.clk10_osc) then
        -- Choose clock input based on RXREADY inputs
        if RXREADY(0) = '0' and RXREADY(1) = '1' then
            input := 2;
        elsif RXREADY(0) = '0' and RXREADY(1) = '0' then
            input := 3;
        else
            input := 1;
        end if;
        lastinput <= input;
        -- Set the clock input and reset as necessary
        case state is
            when idle => 
                if input = 3 then
                    --CLKNET.pllreset <= '1';
                elsif input /= lastinput then
                    --CLKNET.pllreset <= '1';
                    state := set_source;
                else
                    --CLKNET.pllreset <= '0';
                end if;
            when set_source =>
                --CLKNET.pllreset <= '1';
                if input = 2 then
                    gbt_clk_sel <= '0'; -- Low selects CLKIN2
                else
                    gbt_clk_sel <= '1'; -- High selects CLKIN1
                end if;
                state := reset;
            when reset =>       -- Hold reset for one more clock cycle
               --CLKNET.pllreset <= '1';
               state := release;
            when release =>    -- Release reset and go back to idle 
                  --CLKNET.pllreset <= '0';
                  state := idle;
        end case;
    end if; -- clock
end process; -- gbt_clock_selection
   
--gbt_clk40_mmcm_reset <= CLKNET.pllreset;

 --------------------------------------------------
 -- This space reserved for a (watchdog process) --
 --------------------------------------------------

    watchdog_db_reset  <= '0';
    watchdog_mb0_reset <= '0';
    watchdog_mb1_reset <= '0';


   --===============--
   -- Clocks scheme -- 
   --===============--   
	
	fabricClk_from_userClockIbufgds <= OSCCLK_IN;
   
   --=========================--
   -- GBT Bank example design --
   --=========================--

--    gbt_reset <= CLKNET.pllreset;
   
   sfp_gbt_wrapper: entity tilecal.db6_gbt_tx_interface_v1       

		port map (
         -- Resets scheme:      
		 GENERAL_RESET_I 		=> gbt_reset,               
         --------------------------------------------
         GT0_MANUAL_RESET_TX_I     => gt0_manualResetTx_from_user,
		 GT1_MANUAL_RESET_TX_I     => gt1_manualResetTx_from_user,

         -- Clocks scheme:                           
         CLKNET                     => CLKNET,
         CLKIN_I                    => CLKIN_I,             
         -----------------------------------------------------
         -- Serial lanes:                            
         GT0_TX_P_O 					=> TXP_OUT(0),                
         GT0_TX_N_O 					=> TXN_OUT(0),                
			
         GT1_TX_P_O 					=> TXP_OUT(1),                
         GT1_TX_N_O 					=> TXN_OUT(1),                

         GT0_MGT_READY_O         => gt0_mgtReady_from_gbt,             
         GT1_MGT_READY_O         => gt1_mgtReady_from_gbt,             
         -- GBT Bank data:                           
		GT0_GBT_TX_DATA_LG_I 		=> gt0_gbt_tx_data_lg_i,	  
		GT0_GBT_TX_DATA_HG_I 		=> gt0_gbt_tx_data_hg_i,
		GT1_GBT_TX_DATA_LG_I 		=> gt1_gbt_tx_data_lg_i,	  
        GT1_GBT_TX_DATA_HG_I        => gt1_gbt_tx_data_hg_i,
 
         --------------------------------------------                          
         -- Latency measurement:                     
         TX_FRAMECLK_O 				=> txFrameClkfromGbt,        
         TX_WORDCLK_O 				=> txWordClkfromGbt
);
		
 
 
		 txFrameClkPllLocked_from_gbt 		<= txFrameClkPllLocked_from_gbt_QSFP1;
			
			-- GT0 control and link status
         gt0_mgtReady_from_gbt          <= gt0_mgtReady_from_gbt; 

			-- GT1 control and link status
         gt1_mgtReady_from_gbt            <= gt1_mgtReady_from_gbt;           
		



end RTL;
