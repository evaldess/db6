-----------------------------------------------------------------------------------------------------------------------------
-- Company: Stockholm University
-- Engineer: Eduardo Valdes Santurio
--           Sam Silverstein
-- 
-- Create Date: 25/05/2018 11:17:52 PM
-- Design Name: 
-- Module Name: db_configbus_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity db6_configbus_interface_v1 is
    Port (     --p_db_side_in             : in std_logic_vector(1 downto 0);
               p_master_reset_in        : in    std_logic;
               p_clknet_in              : in    t_db_clock_network;
               p_configbus_clknet_out  : out   t_configbus_clock_network;
               p_configbus_local_in       : in std_logic_vector (7 downto 0);
               p_configbus_remote_in       : in std_logic_vector (7 downto 0);      
               p_db_reg_rx_out      : out t_db_reg_rx;
               p_db_reg_rx_opposite_fpga_out      : out t_db_reg_rx;
               p_mb_jtag_out : out t_mb_jtag;
               p_commbus_remote_in : in t_commbus;
               p_leds_out : out std_logic_vector(3 downto 0)
    );

end db6_configbus_interface_v1;

architecture Behavioral of db6_configbus_interface_v1 is
   
    
    constant c_db_reg_rx : t_db_reg_rx:= ( 
    cfb_tx_control => "11111000000000000000000000001111",
    cfb_db_reg_mask => (others=>'1'),
    adv_cfg_gty_txdiffctrl =>"00000000000000000000001000010000",
    adv_cfg_gty_txpostcursor =>"00000000000000000000001000010000",
    adv_cfg_gty_txprecursor =>"00000000000000000000001000010000",
    adv_cfg_gty_txmaincursor =>"00000000000000000010000001000000",
    adc_config_module => "00000000000000000000000000011100",
    adc_readout_idelay3_0 => "00000100000000000000010000000000",
    adc_readout_idelay3_1 => "00000100000000000000010000000000",
    adc_readout_idelay3_2 => "00000100000000000000010000000000",
    adc_readout_idelay3_3 => "00000100000000000000010000000000",
    adc_readout_idelay3_4 => "00000100000000000000010000000000",
    adc_readout_idelay3_5 => "00000100000000000000010000000000",

    others=>(others=>'0')
    );
 
    signal s_db_reg_rx, s_db_reg_rx_configbus, s_db_reg_rx_commbus : t_db_reg_rx:= c_db_reg_rx;
--    (
--    cfb_tx_control => "00000000000000000000000000001111",
--    adv_cfg_gty_txdiffctrl =>"00000000000000000000001000010000",
--    adv_cfg_gty_txpostcursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txprecursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txmaincursor =>"00000000000000000010000001000000",
--    adv_cfg_firmware_version => c_fw_version,
--    others=>(others=>'0')
--    );

--    signal s_db_reg_rx_buffer : t_db_reg_rx:= (
--    adv_cfg_gty_txdiffctrl =>"00000000000000000000001000010000",
--    adv_cfg_gty_txpostcursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txprecursor =>"00000000000000000000001000010000",
--    adv_cfg_gty_txmaincursor =>"00000000000000000010000001000000",
--    adv_cfg_firmware_version => c_fw_version,
--    others=>(others=>'0')
--    );
    --signal s_db_reg_rx_commbus : t_db_reg_rx;
    signal s_db_reg_rx_opposite_fpga : t_db_reg_rx;
    
    --signal s_use_remote_bus : std_logic := '0';  -- 0 local, 1 remote  
    --signal s_orbit_strobe_int : std_Logic := '0';
    
    --signal s_db_configbus_input_buffer : std_logic_vector(7 downto 0);
    
    signal s_db_configbus_shift_register : std_logic_vector(71 downto 0);
    
    signal s_db_configbus_register_data_buffer : std_logic_vector(31 downto 0);
    signal s_db_configbus_register_data : std_logic_vector(31 downto 0);
    
    signal s_lhc_bunches_counter : std_logic_vector(31 downto 0);
    signal s_strobe_reg, s_strobe_reg_cfgbus, s_strobe_reg_commbus : std_logic_vector(31 downto 0);
    signal s_strobe_lenght : std_logic_vector(7 downto 0);
    signal s_strobe_data : std_logic_vector(23 downto 0);
    
    signal s_orbit_cross_shift_register : std_logic_vector(2*c_lhc_bunches_between_bcr downto 0);

    signal s_orbit_cross : std_logic;
    signal s_pre_bcr : std_logic;
    signal s_pos_bcr : std_logic;
    signal s_bcr : std_logic;
    signal s_bcr_bit : std_logic;
    signal s_lhc_bunch_counter : std_logic_vector(31 downto 0);
    
    signal s_data_address_buffer :std_logic_vector(7 downto 0);
    signal s_mb_jtag_buffer :std_logic_vector(7 downto 0);
    signal s_mb_jtag : t_mb_jtag;
    --signal s_data_address : std_logic_vector(15 downto 0);
        
    signal s_data_aligned: std_logic_vector(11 downto 0);
    signal s_strobe_bit, s_strobe_bit_configbus, s_strobe_bit_commbus : std_logic;
    
  
    --clk
    signal s_clk40_cfgbus : std_logic :='1';
    signal s_clk40_cfgbus_locked : std_logic;
--    signal s_watchdog_clk40_cfgbus, s_reset_watchdog_clk40_cfgbus : std_logic :='0';
    signal s_configbus_clk_net : t_configbus_clock_network;
    
--    signal s_chosen_cfgbus_clk80 : std_logic := '0';
    
--    COMPONENT vio_configbus_debug_v1
--      PORT (
--        clk : IN STD_LOGIC;
--        probe_in0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in4 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in7 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in8 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in9 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in10 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in11 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in12 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in13 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in14 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in15 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in16 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in17 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in18 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in19 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in20 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in21 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in22 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        probe_in23 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
--      );
--    END COMPONENT;

--COMPONENT ila_strobe_reg_debug

--PORT (
--	clk : IN STD_LOGIC;



--	probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0); 
--	probe1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
--	probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--	probe3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
--);
--END COMPONENT  ;

--signal s_ila_debug_counter : std_logic_vector(7 downto 0);

begin
    
    --output ports
    p_configbus_clknet_out<=s_configbus_clk_net;
    p_leds_out(3)<=s_configbus_clk_net.bcr_locked;
    p_leds_out(2)<=s_configbus_clk_net.bcr;
    p_leds_out(1)<=p_configbus_remote_in(7);
    p_leds_out(0)<=p_configbus_local_in(7);
    
    s_configbus_clk_net.clk80_cfgbus(0)<=p_clknet_in.clk80;
    s_configbus_clk_net.clk40_cfgbus(0)<=p_clknet_in.clk40;
    
        --s_bcr_bit <= s_pre_bcr or s_pos_bcr;
        proc_config_bus_shift_in_data : process(s_configbus_clk_net.clk80_cfgbus(0))
        begin
            
            if rising_edge(s_configbus_clk_net.clk80_cfgbus(0)) then

                if p_master_reset_in = '0' then
                    --shift in data and orbit cross
                    if p_clknet_in.refclksel = '1' then
                        s_db_configbus_shift_register<= s_db_configbus_shift_register(63 downto 0) & p_configbus_local_in;
                    else
                        s_db_configbus_shift_register<= s_db_configbus_shift_register(63 downto 0) & p_configbus_remote_in;
                    end if;
                    
                    --data buffer
                    s_db_configbus_register_data_buffer <= s_db_configbus_shift_register(15 downto 8) & s_db_configbus_shift_register(31 downto 24) & s_db_configbus_shift_register(47 downto 40) & s_db_configbus_shift_register(63 downto 56);
                    
              
                    --valid_bit and byte number alignment
                    s_data_aligned<= s_db_configbus_shift_register(70) & s_db_configbus_shift_register(54) & s_db_configbus_shift_register(38) & s_db_configbus_shift_register(22) & s_db_configbus_shift_register(65 downto 64) & s_db_configbus_shift_register(49 downto 48) & s_db_configbus_shift_register(33 downto 32) & s_db_configbus_shift_register(17 downto 16);
                    s_data_address_buffer<=s_db_configbus_shift_register(69 downto 66) & s_db_configbus_shift_register(53 downto 50);-- & s_db_configbus_shift_register(37 downto 34) & s_db_configbus_shift_register(21 downto 18);
                    s_mb_jtag_buffer<= s_db_configbus_shift_register(37 downto 34) & s_db_configbus_shift_register(21 downto 18);
                else    
                    s_data_address_buffer<= (others=> '0');
                    s_data_aligned<= (others=> '0');
                    s_db_configbus_register_data_buffer<= (others=> '0');
                    s_db_configbus_shift_register <= (others=> '0');
                    
                end if;

            end if;
        
        end process;
        
--        proc_select_bus_source: process(p_clknet_in.locked_db)
--        begin
--        if p_clknet_in.locked_db = '1' then
            s_db_reg_rx<=s_db_reg_rx_configbus;
            --s_strobe_reg<=s_strobe_reg_cfgbus;
--        else
--            s_db_reg_rx<=s_db_reg_rx_commbus;
--            --s_strobe_reg<=s_strobe_reg_commbus;
--        end if;
--        end process;
        
        
         proc_register_data_configbus : process(s_configbus_clk_net.clk80_cfgbus(0))--s_configbus_clk_net.clk40_cfgbus(0))--s_clk40_cfgbus)
        variable v_cfg_db_advanced_mode_address, v_cfg_db_advanced_mode_register_value : std_logic_vector(31 downto 0);
        type t_cfg_db_advanced_mode_sm is (st_idle, st_get_address, st_get_data,st_get_mask);
        variable sm_cfg_db_advanced_mode : t_cfg_db_advanced_mode_sm := st_idle;
        constant c_cfg_db_advanced_mode_startup_address : integer := 17;
       
        type t_sm_commbus is (st_wait_write, st_idle);
        variable v_sm_commbus : t_sm_commbus :=st_idle;
        begin
         
         if rising_edge(s_configbus_clk_net.clk80_cfgbus(0)) then--s_configbus_clk_net.clk40_cfgbus(0)) then--s_clk40_cfgbus) then
            
            --check that all clocks are locked
             if (p_clknet_in.locked_db = '1') then-- and (p_master_reset_in = '0') then
                
                --check alignment    
                 if s_data_aligned = "111100011011" then
                    --assign mb jtag signals

                    s_mb_jtag.tck <= s_mb_jtag_buffer(3);
                    s_mb_jtag.tms <= s_mb_jtag_buffer(2);
                    s_mb_jtag.tdi <= s_mb_jtag_buffer(1);
                    --check address -- i check first and last byte only but can be done differently
                    --if (s_data_address_buffer(15 downto 12)/="0000") and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(11 downto 8)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(7 downto 4)) and (s_data_address_buffer(15 downto 12) = s_data_address_buffer(3 downto 0)) then
                         --s_db_configbus_advanced_mode_register_address
                         

                         
                         if s_data_address_buffer(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_advanced_reg_value,4)) then
                            s_db_reg_rx_configbus(to_integer(unsigned(s_db_reg_rx(cfb_db_advanced_reg_address))))<=
                            (s_db_configbus_register_data_buffer and s_db_reg_rx(cfb_db_reg_mask))
                            or (s_db_reg_rx(to_integer(unsigned(s_db_reg_rx(cfb_db_advanced_reg_address)))) and (not s_db_reg_rx(cfb_db_reg_mask)));
                            s_strobe_bit_configbus<='0';
                         
                         elsif s_data_address_buffer(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_reg_mask,4)) then
                            s_db_reg_rx_configbus(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=s_db_configbus_register_data_buffer;
                            s_strobe_bit_configbus<='0';
                         elsif s_data_address_buffer(3 downto 0) = std_logic_vector(to_unsigned(cfb_db_advanced_reg_address,4)) then
                            s_db_reg_rx_configbus(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=(s_db_configbus_register_data_buffer);                   
                            
                         elsif s_data_address_buffer(3 downto 0) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                            s_strobe_bit_configbus<='1';
                            s_db_reg_rx_configbus(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=s_db_configbus_register_data_buffer;
                            
                         else
                            s_strobe_bit_configbus<='0'; 
                            s_db_reg_rx_configbus(to_integer(unsigned(s_data_address_buffer(3 downto 0))))<=
                                (s_db_configbus_register_data_buffer and s_db_reg_rx(cfb_db_reg_mask))
                                or (s_db_reg_rx(to_integer(unsigned(s_data_address_buffer(3 downto 0)))) and (not s_db_reg_rx(cfb_db_reg_mask)));   
                         
                         end if;
    
                    --end if;
                 else
                 

                 
                 end if;
                 --s_ila_debug_counter<=std_logic_vector(to_unsigned(v_counter,8));
                 
             else
                s_mb_jtag.tck <= '1';
                s_mb_jtag.tms <= '1';
                s_mb_jtag.tdi <= '1';

             end if;
         end if;
        end process;


--    proc_register_data_commbus : process(p_clknet_in.clk40_osc)
--        type t_sm_commbus is (st_wait_write, st_idle);
--        variable v_sm_commbus : t_sm_commbus :=st_idle;
--        begin
----            if p_clknet_in.locked_clk_db = '1' then
--                if rising_edge(p_clknet_in.clk40_osc) then
--                    case v_sm_commbus is
--                       when st_idle =>
--                           if p_commbus_remote_in.write_flag = '1' then
                           
--                               if p_commbus_remote_in.address = std_logic_vector(to_unsigned(cfb_strobe_reg,8)) then
--                                    s_strobe_bit_commbus<= '1';
--                               else
--                                    s_strobe_bit_commbus<= '0';
--                               end if;
                               
--                               s_db_reg_rx_commbus(to_integer(unsigned(p_commbus_remote_in.address)))<=
--                                   (p_commbus_remote_in.value and s_db_reg_rx(cfb_db_reg_mask))
--                                   or (s_db_reg_rx(to_integer(unsigned(s_data_address_buffer(3 downto 0)))) and (not s_db_reg_rx(cfb_db_reg_mask)));
--                               v_sm_commbus := st_wait_write;
--                           else

--                               s_db_reg_rx_opposite_fpga(to_integer(unsigned(p_commbus_remote_in.address)))<=p_commbus_remote_in.value;
                               
--                           end if;
--                       when st_wait_write =>
--                           if p_commbus_remote_in.write_flag = '0' then
--                               v_sm_commbus := st_idle;
--                           end if;
--                       when others =>
--                           v_sm_commbus := st_idle;
--                    end case;
--                end if;
--    end process;

    s_strobe_bit<= s_strobe_bit_configbus; -- or s_strobe_bit_commbus;
    proc_strobe_manager : process(s_configbus_clk_net.clk40_cfgbus(0))
    variable v_counter : integer := 0;
    type t_strobe_manager_sm is (st_idle, st_wait_for_strobe_bit, st_propagate_strobe);
    variable sm_strobe_manager : t_strobe_manager_sm := st_idle;
    variable v_reg_buffer : std_logic_vector(31 downto 0);
    variable v_strobe_lenght : integer := 0;
    begin
        if rising_edge(s_configbus_clk_net.clk40_cfgbus(0)) then
             --s_strobe_bit<= s_strobe_bit_configbus or s_strobe_bit_commbus;
             case sm_strobe_manager is
                when st_idle=>
                    if (s_strobe_bit='1') then
                    --if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
                        v_counter := 0;
                        sm_strobe_manager := st_wait_for_strobe_bit;
                        v_reg_buffer:= s_db_reg_rx(cfb_strobe_reg);--s_strobe_lenght&s_strobe_data; --s_db_reg_rx(cfb_strobe_reg);
                        v_strobe_lenght:= to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 16)));--(to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 24))));
                    end if;
                    s_strobe_reg <= (others=>'0');
                when st_wait_for_strobe_bit =>
                       if (s_strobe_bit='0') then
                           sm_strobe_manager:=st_propagate_strobe;
                       else
                           sm_strobe_manager := st_wait_for_strobe_bit;
                       end if;
                when st_propagate_strobe =>
                    if v_counter < v_strobe_lenght*(400900)  then
                        v_counter:=v_counter+1;
                        s_strobe_reg <= v_reg_buffer;
                    else
                        sm_strobe_manager := st_idle;
                        v_counter :=0;
                        s_strobe_reg <= (others=>'0');
                    end if;
                when others=>
                    sm_strobe_manager := st_idle;
            end case;
        end if;

    end process;
    
--    proc_strobe_manager_commbus : process(p_clknet_in.clk80_osc)
--    variable v_counter : integer := 0;
--    type t_strobe_manager_sm is (st_idle, st_wait_for_strobe_bit, st_propagate_strobe);
--    variable sm_strobe_manager : t_strobe_manager_sm := st_idle;
--    variable v_reg_buffer : std_logic_vector(31 downto 0);
--    variable v_strobe_lenght : integer := 0;
--    begin
--        if rising_edge(p_clknet_in.clk80_osc) then
--             --s_strobe_bit<= s_strobe_bit_configbus or s_strobe_bit_commbus;
--             case sm_strobe_manager is
--                when st_idle=>
--                    if (s_strobe_bit_commbus='1') then
--                    --if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
--                        v_counter := 0;
--                        sm_strobe_manager := st_wait_for_strobe_bit;
--                        v_reg_buffer:= s_db_reg_rx(cfb_strobe_reg);--s_strobe_lenght&s_strobe_data; --s_db_reg_rx(cfb_strobe_reg);
--                        v_strobe_lenght:= to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 16)));--(to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 24))));
--                    end if;
--                    s_strobe_reg_commbus <= (others=>'0');
--                when st_wait_for_strobe_bit =>
--                       if (s_strobe_bit_commbus='0') then
--                           sm_strobe_manager:=st_propagate_strobe;
--                       else
--                           sm_strobe_manager := st_wait_for_strobe_bit;
--                       end if;
--                when st_propagate_strobe =>
--                    if v_counter < v_strobe_lenght  then
--                        v_counter:=v_counter+1;
--                        s_strobe_reg_commbus <= v_reg_buffer;
--                    else
--                        sm_strobe_manager := st_idle;
--                        v_counter :=0;
--                        s_strobe_reg_commbus <= (others=>'0');
--                    end if;
--                when others=>
--                    sm_strobe_manager := st_idle;
--            end case;
--        end if;

--    end process;
    
--    proc_strobe_manager_configbus : process(s_configbus_clk_net.clk80_cfgbus)
--    variable v_counter : integer := 0;
--    type t_strobe_manager_sm is (st_idle, st_wait_for_strobe_bit, st_propagate_strobe);
--    variable sm_strobe_manager : t_strobe_manager_sm := st_idle;
--    variable v_reg_buffer : std_logic_vector(31 downto 0);
--    variable v_strobe_lenght : integer := 0;
--    begin
--        if rising_edge(s_configbus_clk_net.clk80_cfgbus) then
--             --s_strobe_bit<= s_strobe_bit_configbus or s_strobe_bit_commbus;
--             case sm_strobe_manager is
--                when st_idle=>
--                    if (s_strobe_bit_configbus='1') then
--                    --if s_data_address_buffer(15 downto 12) = std_logic_vector(to_unsigned(cfb_strobe_reg,4)) then
--                        v_counter := 0;
--                        sm_strobe_manager := st_wait_for_strobe_bit;
--                        v_reg_buffer:= s_db_reg_rx(cfb_strobe_reg);--s_strobe_lenght&s_strobe_data; --s_db_reg_rx(cfb_strobe_reg);
--                        v_strobe_lenght:= to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 16)));--(to_integer(unsigned(s_db_reg_rx(cfb_strobe_reg)(31 downto 24))));
--                    end if;
--                    s_strobe_reg_cfgbus <= (others=>'0');
--                when st_wait_for_strobe_bit =>
--                       if (s_strobe_bit_configbus='0') then
--                           sm_strobe_manager:=st_propagate_strobe;
--                       else
--                           sm_strobe_manager := st_wait_for_strobe_bit;
--                       end if;
--                when st_propagate_strobe =>
--                    if v_counter < v_strobe_lenght  then
--                        v_counter:=v_counter+1;
--                        s_strobe_reg_cfgbus <= v_reg_buffer;
--                    else
--                        sm_strobe_manager := st_idle;
--                        v_counter :=0;
--                        s_strobe_reg_cfgbus <= (others=>'0');
--                    end if;
--                when others=>
--                    sm_strobe_manager := st_idle;
--            end case;
--        end if;

--    end process;


        
        proc_sync_to_orbit: process(s_configbus_clk_net.clk40_cfgbus(0))
        constant c_watchdog_tries : integer := 31;
        variable v_counter : integer := 0;
        variable v_bcrlock : std_logic := '0';
        variable v_maxcount : integer := 0;
        variable v_bcr_watchdog : integer:= 0;
        type t_sm_sync_bcr is (st_get_a_bcr, st_verify_bcr_sync, st_working);
        variable st_sync_bcr : t_sm_sync_bcr :=st_get_a_bcr;

        begin
            
            if rising_edge(s_configbus_clk_net.clk40_cfgbus(0)) then
                if p_clknet_in.refclksel = '0' then
                    s_bcr_bit<= p_configbus_local_in(7);
                else
                    s_bcr_bit<= p_configbus_remote_in(7);
                end if;
                
            end if;
            
            if rising_edge(s_configbus_clk_net.clk40_cfgbus(0)) then
                if s_bcr_bit = '1' then
                    v_maxcount := v_counter;
                    if (v_maxcount = (c_lhc_bunches_between_bcr-1)) then
                        v_bcrlock := '1';
                        --s_configbus_clk_net.bcr <= '1';
                        
                     else
                        v_bcrlock := '0';
                        --s_configbus_clk_net.bcr <= '0';
                    end if;
                    v_counter := 0;                
                else
                    v_counter := v_counter + 1;
                    --s_configbus_clk_net.bcr <= '0';           
                end if;
                
                s_lhc_bunch_counter <= std_logic_vector(to_unsigned(v_counter,32));
                s_configbus_clk_net.bcr_locked <=  v_bcrlock;
                
                
            end if; -- Clock edge
        
        end process; -- proc_sync_to_orbit
        
        s_configbus_clk_net.bcr <= s_bcr_bit;
        

    --end generate;
    
    --assign the propper register values to the record previously done by: p_db_config_reg_out <= s_db_config_registers;
    --p_db_reg_rx_out<=s_db_config_registers;
    p_db_reg_rx_out(0 to cfb_strobe_reg-1) <= s_db_reg_rx(0 to cfb_strobe_reg-1);
    p_db_reg_rx_out(bc_number)<=s_lhc_bunch_counter;
    p_db_reg_rx_out(cfb_strobe_reg) <= s_strobe_reg;
    p_db_reg_rx_out(bc_number+1 to c_number_of_cfb_bus_regs) <= s_db_reg_rx(bc_number+1 to c_number_of_cfb_bus_regs);
    
    p_db_reg_rx_opposite_fpga_out<= s_db_reg_rx_opposite_fpga;
    
    p_mb_jtag_out<= s_mb_jtag;
--   i_ila_strobe_reg_debug : ila_strobe_reg_debug
--    PORT MAP (
--        clk => p_clknet_in.clk40_osc,
    
--        probe0(0) => s_strobe_bit, 
--        probe1(31 downto 24) => s_strobe_lenght,
--        probe1(23 downto 0) => s_strobe_data,
--        probe2 => s_strobe_reg,
--        probe3 => s_ila_debug_counter
--    );

   
   
--   p_db_reg_rx_out.db_register_zero <= s_db_config_registers(cfg_register_zero);
--   p_db_reg_rx_out.mb_adc_config <= s_db_config_registers(cfb_mb_adc_config);
--   p_db_reg_rx_out.mb_phase_config <= s_db_config_registers(cfb_mb_phase_config);
--   p_db_reg_rx_out.cs_config <= s_db_config_registers(cfb_cs_config);
--   p_db_reg_rx_out.cs_command <= s_db_config_registers(cfb_cs_command);
--   p_db_reg_rx_out.integrator_interval <= s_db_config_registers(cfb_integrator_interval);
--   p_db_reg_rx_out.db_bc_num_offset <= s_db_config_registers(cfb_bc_num_offset);
--   p_db_reg_rx_out.db_sem_control <= s_db_config_registers(cfb_sem_control);
--   p_db_reg_rx_out.hv_control <= s_db_config_registers(cfb_hv_control);
--   p_db_reg_rx_out.gbtx_reg_config <= s_db_config_registers(cfb_gbtx_reg_config);
--   p_db_reg_rx_out.db_control <= s_db_config_registers(cfb_db_control);
--   p_db_reg_rx_out.db_tx_control <= s_db_config_registers(cfb_tx_control);
--   p_db_reg_rx_out.mb_control <= s_db_config_registers(cfb_mb_control);
--   p_db_reg_rx_out.db_debug <= s_db_config_registers(cfb_db_debug);
--   p_db_reg_rx_out.db_advanced_mode <= s_db_config_registers(cfb_db_debug);
--   p_db_reg_rx_out.db_strobe_reg <= s_db_config_registers(cfb_strobe_reg);
--   p_db_reg_rx_out.db_bc_number <= s_lhc_bunch_counter;
--   p_db_reg_rx_out.db_gty_txdiffctrl <= s_db_config_registers(adv_cfg_gty_txdiffctrl);
--   p_db_reg_rx_out.db_gty_txpostcursor <= s_db_config_registers(adv_cfg_gty_txpostcursor);
--   p_db_reg_rx_out.db_gty_txprecursor <= s_db_config_registers(adv_cfg_gty_txprecursor);
--   p_db_reg_rx_out.db_gty_txmaincursor <= s_db_config_registers(adv_cfg_gty_txmaincursor);
--   p_db_reg_rx_out.db_txrawtestword <= s_db_config_registers(adv_cfg_txrawtestword);
    
--    i_vio_configbus_debug_v1 : vio_configbus_debug_v1
--  PORT MAP (
--    clk => p_clknet_in.clk40,
--    probe_in0 => s_db_config_registers_remote(0),
--    probe_in1 => s_db_config_registers_remote(1),
--    probe_in2 => s_db_config_registers_remote(2),
--    probe_in3 => s_db_config_registers_remote(3),
--    probe_in4 => s_db_config_registers_remote(4),
--    probe_in5 => s_db_config_registers_remote(5),
--    probe_in6 => s_db_config_registers_remote(6),
--    probe_in7 => s_db_config_registers_remote(7),
--    probe_in8 => s_db_config_registers_remote(8),
--    probe_in9 => s_db_config_registers_remote(9),
--    probe_in10 => s_db_config_registers_remote(10),
--    probe_in11 => s_db_config_registers_remote(11),
--    probe_in12 => s_db_config_registers_remote(12),
--    probe_in13 => s_db_config_registers_remote(13),
--    probe_in14 => s_db_config_registers_remote(14),
--    probe_in15 => s_db_config_registers_remote(15),
--    probe_in16 => s_db_config_registers_remote(16),
--    probe_in17 => s_db_config_registers_remote(17),
--    probe_in18 => s_db_config_registers_remote(18),
--    probe_in19 => s_db_config_registers_remote(19),
--    probe_in20 => s_db_config_registers_remote(20),
--    probe_in21 => s_db_config_registers_remote(21),
--    probe_in22 => s_db_config_registers_remote(22),
--    probe_in23 => s_db_config_registers_remote(23)
--  );

        
end Behavioral;



    