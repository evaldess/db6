
--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Eduardo Valdes eduardo.valdes@cern.ch
-- Engineer:              Sam Silverstein silver@fysik.su.se
--                                                                                                 
-- Project Name:          piro_gbtx_i2c_interface                                                                
-- Module Name:           GBT top                                        
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         Xilinx Kintex 7                                                         
-- Tool version:          ISE 14.7                                                               
--                                                                                                   
-- Version:               1.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            			DESCRIPTION
--
--                        22/03/2018   1.0       Eduardo Valdes Santurio   	Firmware for the controlling the I2C configuration/monitoring of the GBTx / TileCal DaughterBoard
--
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_gbtx_i2c_interface is
  Port (       
				p_clk100_in 		: in std_logic; -- 100 MHz clock
				p_db_side : in std_logic_vector(1 downto 0);
				p_gbtx_db_side : in std_logic_vector(1 downto 0);
				p_read_write_operation_in : in std_logic;
				p_trigger_i2c_operation_in : in std_logic;
				p_busy_out                 : out std_logic;
				p_wipe_gbtx_registers      : in  std_logic;
				p_gbtx_default_configuration : in std_logic;
				p_user_address_in: in std_logic_vector(15 downto 0);
				p_user_data_in: in std_logic_vector(7 downto 0);
				p_user_data_out: out std_logic_vector(7 downto 0);
				p_scl_inout 	: inout std_logic;
				p_sda_inout    : inout std_logic;
				p_bus_busy      :out  std_logic;
				p_leds_out : out std_logic_vector(3 downto 0)
				
				

    );
end db6_gbtx_i2c_interface;

architecture Behavioral of db6_gbtx_i2c_interface is

--leds buffering for debug
signal s_leds_out : std_logic_vector(3 downto 0) := (others=>'0' ); 

-- trigger and config signals
signal s_trigger_i2c_operation : std_logic := '0';
signal s_i2c_read_write_operation : std_logic;


--i2c signals
signal s_gbtx_reg_address_read : STD_LOGIC_VECTOR(15 DOWNTO 0);
signal s_gbtx_reg_address_write : STD_LOGIC_VECTOR(15 DOWNTO 0);
signal s_gbtx_reg_default_data : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal s_gbtx_reg_read_data : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal s_gbtx_reg_write_data : STD_LOGIC_VECTOR(7 DOWNTO 0);
signal s_i2c_clk : std_logic;


COMPONENT db6_i2c_master_driver IS
GENERIC(
	input_clk : INTEGER := 10000000;   --input clock speed from user logic in 10*KHz
	bus_clk   : INTEGER := 10000);   --speed the i2c bus (scl) will run at in 10*KHz
PORT(
	clk       : IN     STD_LOGIC;                    --system clock
    p_db_side : in     std_logic_vector(1 downto 0); -- db side
    p_verify_bus :in   std_logic; -- verify if the bus is being used
	reset_n   : IN     STD_LOGIC;                    --active low reset
	ena       : IN     STD_LOGIC;                    --latch in command
	addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
	rw        : IN     STD_LOGIC;                    --'0' is write, '1' is read
	data_wr   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
	busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
	data_rd   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
	ack_error : INOUT STD_LOGIC;                    --flag if improper acknowledge from slave
	sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
	scl       : INOUT  STD_LOGIC;                   --serial clock output of i2c bus
	--test0     : OUT  STD_LOGIC;
	--test1     : OUT  STD_LOGIC;
	p_bus_busy : out std_logic;
	sda_mon   : out std_logic;
	scl_mon   : out std_logic
	 );	 
end component;


signal s_scl_inout, s_sda_inout,s_scl_inout_b, s_sda_inout_b : std_logic :='1';
signal s_reset_n : std_logic := '1';
signal s_ena, s_rw, s_busy, s_ack_error : std_logic;
signal s_addr: std_logic_vector (6 downto 0);
signal s_data_rd, s_data_wr: std_logic_vector(7 downto 0);
signal s_verify_bus : std_logic;
signal s_bus_busy : std_logic;

--I2C state machine control signals
type t_i2c_general_state IS ( st_idle, st_busy, st_stop, st_wait_for_bus_free, st_set_reset, st_set_address_msb, st_set_address_lsb, st_trigger_register_operation, st_debounce_trigger);

--removed byPiro
signal s_start_register_address : std_logic_vector(15 downto 0) := X"0000";
--signal s_ending_register_address   : std_logic_vector(15 downto 0) := X"016d"; -- 365 decimal

signal sm_i2c : t_i2c_general_state  := st_idle;
signal s_i2c_busy_rising_edge, s_i2c_busy_falling_edge : std_logic := '0';
signal s_previous_busy : std_logic:= '0';

signal s_wipe_gbtx_registers     :  std_logic := '0';
signal s_gbtx_default_configuration : std_logic:= '0';
signal s_gbtx_side : std_logic_vector (1 downto 0);

    signal s_gbtx_register_default_configuration_array : t_gbtx_register_configuration_array := 
    ((
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 000 4-7 fineDel 8-15 coarseDel
    X"00", X"00", X"00", X"00", X"00", X"00", X"23", X"03", X"03", X"03", -- 010 17-23 frequency
    X"23", X"03", X"03", X"03", X"ff", X"01", X"7f", X"00", X"00", X"00", -- 020 24 chReset 25 pllReset
    X"00", X"00", X"10", X"00", X"0d", X"f2", X"00", X"0f", X"04", X"08", -- 030 RXcontrol forcetx  
    X"00", X"00", X"00", X"00", X"00", X"00", X"15", X"15", X"15", X"00", -- 040
    X"07", X"00", X"3f", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 050
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 060 
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 070
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 080
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 090
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 100
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 110
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 120
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 130
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 140
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 150
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 160
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 170
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 180
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 190
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 200
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 210
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 220
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 230
    X"00", X"00", X"00", X"00", X"38", X"00", X"00", X"00", X"00", X"00", -- 240  Eport settings A
    X"00", X"00", X"00", X"00", X"01", X"00", X"ff", X"05", X"01", X"55", -- 250
    X"05", X"00", X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"00", -- 260
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 270
    X"00", X"15", X"00", X"05", X"00", X"00", X"00", X"00", X"00", X"00", -- 280
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 290
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 300
    X"00", X"00", X"00", X"77", X"77", X"77", X"ff", X"0f", X"07", X"00", -- 310
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"88", X"88", X"88", -- 320
    X"88", X"88", X"01", X"00", X"ff", X"05", X"01", X"55", X"05", X"00", -- 330  Eport settings A
    X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"01", X"00", X"ff", -- 340  Eport settings B
    X"05", X"01", X"55", X"05", X"00", X"55", X"05", X"01", X"55", X"05", -- 350
    X"00", X"55", X"00", X"00", X"00", X"aa"                              -- 360
    ),
    (
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 000 4-7 fineDel 8-15 coarseDel
    X"00", X"00", X"00", X"00", X"00", X"00", X"23", X"03", X"03", X"03", -- 010 17-23 frequency
    X"23", X"03", X"03", X"03", X"ff", X"01", X"7f", X"00", X"00", X"00", -- 020 24 chReset 25 pllReset
    X"00", X"00", X"10", X"00", X"0d", X"f2", X"00", X"0f", X"04", X"08", -- 030 RXcontrol forcetx  
    X"00", X"00", X"00", X"00", X"00", X"00", X"15", X"15", X"15", X"00", -- 040
    X"07", X"00", X"3f", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 050
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 060 
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 070
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 080
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 090
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 100
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 110
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 120
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 130
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 140
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 150
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 160
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 170
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 180
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 190
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 200
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 210
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 220
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 230
    X"00", X"00", X"00", X"00", X"38", X"00", X"00", X"00", X"00", X"00", -- 240  Eport settings A
    X"00", X"00", X"00", X"00", X"01", X"00", X"ff", X"05", X"01", X"55", -- 250
    X"05", X"00", X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"00", -- 260
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 270
    X"00", X"15", X"00", X"05", X"00", X"00", X"00", X"00", X"00", X"00", -- 280
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 290
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 300
    X"00", X"00", X"00", X"77", X"77", X"77", X"ff", X"0f", X"07", X"00", -- 310
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"88", X"88", X"88", -- 320
    X"88", X"88", X"01", X"00", X"ff", X"05", X"01", X"55", X"05", X"00", -- 330  Eport settings A
    X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"01", X"00", X"ff", -- 340  Eport settings B
    X"05", X"01", X"55", X"05", X"00", X"55", X"05", X"01", X"55", X"05", -- 350
    X"00", X"55", X"00", X"00", X"00", X"aa"                              -- 360
    )
    );
    

signal s_gbtx_register_configuration_array : t_gbtx_register_configuration_array := 
    ((
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 000 4-7 fineDel 8-15 coarseDel
    X"00", X"00", X"00", X"00", X"00", X"00", X"23", X"03", X"03", X"03", -- 010 17-23 frequency
    X"23", X"03", X"03", X"03", X"ff", X"01", X"7f", X"00", X"00", X"00", -- 020 24 chReset 25 pllReset
    X"00", X"00", X"10", X"00", X"0d", X"f2", X"00", X"0f", X"04", X"08", -- 030 RXcontrol forcetx  
    X"00", X"00", X"00", X"00", X"00", X"00", X"15", X"15", X"15", X"00", -- 040
    X"07", X"00", X"3f", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 050
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 060 
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 070
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 080
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 090
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 100
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 110
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 120
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 130
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 140
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 150
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 160
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 170
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 180
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 190
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 200
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 210
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 220
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 230
    X"00", X"00", X"00", X"00", X"38", X"00", X"00", X"00", X"00", X"00", -- 240  Eport settings A
    X"00", X"00", X"00", X"00", X"01", X"00", X"ff", X"05", X"01", X"55", -- 250
    X"05", X"00", X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"00", -- 260
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 270
    X"00", X"15", X"00", X"05", X"00", X"00", X"00", X"00", X"00", X"00", -- 280
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 290
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 300
    X"00", X"00", X"00", X"77", X"77", X"77", X"ff", X"0f", X"07", X"00", -- 310
    X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"88", X"88", X"88", -- 320
    X"88", X"88", X"01", X"00", X"ff", X"05", X"01", X"55", X"05", X"00", -- 330  Eport settings A
    X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"01", X"00", X"ff", -- 340  Eport settings B
    X"05", X"01", X"55", X"05", X"00", X"55", X"05", X"01", X"55", X"05", -- 350
    X"00", X"55", X"00", X"00", X"00", X"00"                              -- 360
    ),
    (
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 000 4-7 fineDel 8-15 coarseDel
        X"00", X"00", X"00", X"00", X"00", X"00", X"23", X"03", X"03", X"03", -- 010 17-23 frequency
        X"23", X"03", X"03", X"03", X"ff", X"01", X"7f", X"00", X"00", X"00", -- 020 24 chReset 25 pllReset
        X"00", X"00", X"10", X"00", X"0d", X"f2", X"00", X"0f", X"04", X"08", -- 030 RXcontrol forcetx  
        X"00", X"00", X"00", X"00", X"00", X"00", X"15", X"15", X"15", X"00", -- 040
        X"07", X"00", X"3f", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 050
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 060 
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 070
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 080
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 090
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 100
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 110
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 120
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 130
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 140
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 150
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 160
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 170
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 180
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 190
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 200
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 210
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 220
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 230
        X"00", X"00", X"00", X"00", X"38", X"00", X"00", X"00", X"00", X"00", -- 240  Eport settings A
        X"00", X"00", X"00", X"00", X"01", X"00", X"ff", X"05", X"01", X"55", -- 250
        X"05", X"00", X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"00", -- 260
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 270
        X"00", X"15", X"00", X"05", X"00", X"00", X"00", X"00", X"00", X"00", -- 280
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 290
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"00", -- 300
        X"00", X"00", X"00", X"77", X"77", X"77", X"ff", X"0f", X"07", X"00", -- 310
        X"00", X"00", X"00", X"00", X"00", X"00", X"00", X"88", X"88", X"88", -- 320
        X"88", X"88", X"01", X"00", X"ff", X"05", X"01", X"55", X"05", X"00", -- 330  Eport settings A
        X"55", X"05", X"01", X"55", X"05", X"00", X"55", X"01", X"00", X"ff", -- 340  Eport settings B
        X"05", X"01", X"55", X"05", X"00", X"55", X"05", X"01", X"55", X"05", -- 350
        X"00", X"55", X"00", X"00", X"00", X"00"                              -- 360
        )
    );
    
    signal s_gbtx_register_readout_array : t_gbtx_register_readout_array := ( (others => "00"), (others => "00"));

--    signal s_side_debug : std_logic_vector(0 downto 0);
--    signal s_address_debug : std_logic_vector(15 downto 0); 
--    signal s_data_debug : std_logic_vector(7 downto 0); 
--    COMPONENT vio_0
--      PORT (
--        clk : IN STD_LOGIC;
--        probe_in0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
--        probe_in1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--        probe_out0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
--        probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--        probe_out2 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
--      );
--    END COMPONENT;

begin


p_leds_out<=s_leds_out;
--s_leds_out(3)<= s_i2c_read_write_operation;
p_leds_out(3)<= s_bus_busy;
p_bus_busy <= s_bus_busy;

-----------------------------------------------------
-- State machine to configure local or remote GBTx --
-----------------------------------------------------

proc_i2c_transaction : process(p_clk100_in)
    
    variable v_start_register, v_register_index, v_end_register : integer := 0;
    variable v_counter : integer:=0;
    variable v_gbtx_lut_index : integer:=0;
    variable v_previous_scl, v_previous_sda : std_logic :='0';
    constant gbtx_i2c_arbitration_constant :integer :=50000000;
begin

    if rising_edge (p_clk100_in) then
      s_previous_busy <= s_busy;                       --capture the value of the previous i2c busy signal
      IF(s_previous_busy = '0' AND s_busy = '1') THEN  --i2c busy just went high
        s_i2c_busy_rising_edge <= '1';                       -- ready to execute next operation
      ELSE
        s_i2c_busy_rising_edge <= '0';                       --wait until operation is finished
      END IF;
 
      IF(s_previous_busy = '1' AND s_busy = '0') THEN  --i2c busy just went high
        s_i2c_busy_falling_edge <= '1';                       -- ready to execute next operation
      ELSE
        s_i2c_busy_falling_edge  <= '0';                       --wait until operation is finished
      END IF;
      
      

      CASE sm_i2c IS
      -- I2C controller is idle.  release control of bus, and wait for init
        when st_idle =>
               
                p_busy_out <= '0';
                s_ena <= '0';
                s_reset_n <= '0';
                v_register_index := 0;
                
                --set up propper address range, gbtx has only 365 writeable regs and 4xx readeable
                if s_i2c_read_write_operation = '0' then
                    v_start_register := 0;
                    v_end_register := 366;
                else
                    v_start_register := 0;
                    v_end_register := 435;
                end if;
                
               
                s_start_register_address<= std_logic_vector(to_unsigned(v_start_register,16));
                
                --buffer in signals
                s_i2c_read_write_operation <= p_read_write_operation_in;
                s_gbtx_default_configuration<=p_gbtx_default_configuration;
                s_wipe_gbtx_registers<=p_wipe_gbtx_registers;
                
                --gbtx side signal multiplexer to avoid use of illegal addresses
                case p_gbtx_db_side is
                    when "01" =>
                        s_gbtx_side<=p_gbtx_db_side;
                        v_gbtx_lut_index:=0;
                    when "10" =>
                        s_gbtx_side<=p_gbtx_db_side;
                        v_gbtx_lut_index:=1;
                    when others =>
                        s_gbtx_side<=p_db_side;
                        v_gbtx_lut_index:=to_integer(unsigned(p_db_side))-1;
                end case;
                
                --gbtx address and data mod from configbus (cfb_gbtx_reg_config)
                s_gbtx_register_configuration_array(v_gbtx_lut_index)(to_integer(unsigned(p_user_address_in)))<=p_user_data_in;
                p_user_data_out<=s_gbtx_register_readout_array(to_integer(unsigned(p_gbtx_db_side))-1)(to_integer(unsigned(p_user_address_in)));
                
                s_leds_out(2 downto 0)<="000";

                --trigger code
                if s_trigger_i2c_operation = '1' then
                  sm_i2c <= st_set_reset;
                else
                  sm_i2c <= st_idle;
                end if;
                
--        when st_wait_for_bus_free=>
                
--                --connect ports to the buffers
--                s_scl_inout_b<=p_scl_inout;
--                s_sda_inout_b<=p_sda_inout;
                
--                --buffers sda and scl
--                v_previous_scl := s_scl_inout;
--                v_previous_sda := s_sda_inout;
--                --keeps sda and sdl in z state and the driver in reset mode 
--                s_reset_n <= '0';
                
--                --debug
--                s_probe_ila_in3<=std_logic_vector(to_unsigned(v_counter,16));
--                s_probe_ila_in1<=v_previous_scl & v_previous_scl;
                
--                --checks if there are any variations in them in an amount of time
--                if (v_counter <  to_integer(unsigned(p_db_side))*gbtx_i2c_arbitration_constant) then
--                    if (v_previous_scl = s_scl_inout_b) and (v_previous_sda = s_sda_inout_b) then 
--                        v_counter:=v_counter+1;
--                    else
--                        v_counter:=0;
--                    end if;
--                else
--                --no variations then check if they are equal and move forward if so, if not then come back to
--                    if (v_previous_scl=v_previous_sda) then
--                        sm_i2c <= st_set_reset;
--                    else
--                        v_counter:=0;
--                        sm_i2c <= st_wait_for_bus_free;
--                    end if;
                
--                end if;
                
--                sm_i2c <= st_set_address_lsb; 
                
--                s_leds_out(2 downto 0)<="001"; 
 
        when st_set_reset =>
                s_reset_n <= '1';
                sm_i2c <= st_set_address_lsb;
                s_verify_bus <='1';-- and s_probe_out0(15); 
                
                s_leds_out(2 downto 0)<="010";
                              
        
        WHEN st_set_address_lsb =>
              s_verify_bus <='1';--  and s_probe_out0(15); 
              p_busy_out <= '1';  
              s_reset_n <= '1';
              
              s_ena <= '1';                            
              s_addr <= "00000"&s_gbtx_side;                    
              s_rw <= '0';                           -- Write operation  
              s_data_wr <= s_start_register_address(7 downto 0);    -- Transmit lower 8 bits of starting register address
              if s_i2c_busy_rising_edge = '1' then 
                sm_i2c <= st_set_address_msb;
              end if;
              
              s_leds_out(2 downto 0)<="011";

        WHEN st_set_address_msb =>
              s_verify_bus <='1';-- and s_probe_out0(15); 
              p_busy_out <= '1';
              s_reset_n <= '1';
              
              s_ena <= '1';                            
              s_addr <= "00000"&s_gbtx_side;                    
              s_rw <= '0';                             
              s_data_wr <= s_start_register_address(15 downto 8); -- Transmit upper 8 bits of starting register address
                    
              if s_i2c_busy_rising_edge = '1' then
                v_register_index := v_start_register+1;
                sm_i2c <= st_trigger_register_operation;
              end if;
              
              s_leds_out(2 downto 0)<="100";
              
        WHEN st_trigger_register_operation =>
              s_verify_bus <='1';-- and s_probe_out0(15); 
              p_busy_out <= '1';
              s_reset_n <= '1';
              
              s_addr <= "00000"&s_gbtx_side; 
              s_rw <= s_i2c_read_write_operation; --'0';

              
              if s_i2c_read_write_operation = '0' then
                if s_wipe_gbtx_registers = '0' then
                    if s_gbtx_default_configuration = '0' then
                        s_data_wr <= s_gbtx_register_configuration_array(v_gbtx_lut_index)(v_register_index); -- Transmit 8 bits of register data
                    else
                        s_data_wr <= s_gbtx_register_default_configuration_array(v_gbtx_lut_index)(v_register_index); -- Transmit 8 bits of register data
                    end if;
                else
                    s_data_wr <= x"00";
                end if;
              else
                if s_i2c_busy_falling_edge = '1' then
                    s_gbtx_register_readout_array(v_gbtx_lut_index)(v_register_index-1) <=s_data_rd; --read register                
                end if;
              end if;
              
              if s_i2c_busy_rising_edge = '1' then
              
                  v_register_index := v_register_index + 1;
                  if v_register_index > v_end_register+1 then -- Writing the final word....
                    sm_i2c <= st_stop;  
                    s_ena <= '0';
                  else
                    sm_i2c <= st_trigger_register_operation;
                    s_ena <= '1';
                  end if;                                            
                  
              end if;
              
              s_leds_out(2 downto 0)<="101";

        WHEN st_stop => -- Wait for I2C master to finish the last operation
              s_verify_bus <='1';-- and s_probe_out0(15);               p_busy_out <= '1';
              s_reset_n <= '1';
              s_ena <= '0';
              if s_i2c_busy_falling_edge = '1' then
                sm_i2c <= st_idle;
              end if;
              s_leds_out(2 downto 0)<="110";
        WHEN OTHERS =>
            sm_i2c <= st_idle;
            
        END CASE; 
     end if; -- clock edge
end process; -- i2c_control


i_db6_i2cmaster : db6_i2c_master_driver
GENERIC MAP(
	input_clk => 100000000,   --input clock speed from user logic in 10*KHz
	bus_clk => 10000)     --speed the i2c bus (scl) will run at in 10*KHz
PORT MAP(
	clk        => p_clk100_in, --s_i2c_clk,         --system clock
    p_db_side  => p_db_side, -- db side
    p_verify_bus => s_verify_bus,-- verify if the bus is being used
	reset_n    => s_reset_n,     --active low reset
	ena        => s_ena,         --latch in command
	addr       => s_addr,        --address of target slave
	rw         => s_rw,          --'0' is write, '1' is read
	data_wr    => s_data_wr,     --data to write to slave
	busy       => s_busy,        --indicates transaction in progress
	data_rd    => s_data_rd, --data_rd,     --data read from slave
	ack_error  => s_ack_error,   --flag if improper acknowledge from slave
	sda        => p_sda_inout,         --serial data output of i2c bus
	scl        => p_scl_inout,
	p_bus_busy => s_bus_busy,
	sda_mon    => open, -- s_probe_ila_in1(0),--s_sda_ila(0),
	scl_mon    => open -- s_probe_ila_in1(1)--s_scl_ila(0)
	);	 
	

--s_trigger_i2c_operation <= p_trigger_i2c_operation_in; -- or s_debug_gbtx_i2c_trigger(0); --old not-debounced signal

proc_trigger_monitor : process(p_clk100_in)
type t_trigger_monitor_sm is (st_idle,st_debouncer);
variable v_trigger_monitor_sm : t_trigger_monitor_sm := st_idle;
begin
	if rising_edge(p_clk100_in) then
		case v_trigger_monitor_sm is
			when st_idle =>
				s_trigger_i2c_operation<='0';
				if (p_trigger_i2c_operation_in='1') then -- or (p_user_config_in(31)='1') then
					s_trigger_i2c_operation<='1';
					v_trigger_monitor_sm:=st_debouncer;
				end if;
			when st_debouncer=>
				s_trigger_i2c_operation<='0';
				if (p_trigger_i2c_operation_in='0') then --and (p_user_config_in(31)='0')then
					v_trigger_monitor_sm:=st_idle;
				end if;
			when others=>
		end case;
	end if;
end process;
    
--proc_debug : process(p_clk100_in)
--begin
--    if rising_edge(p_clk100_in) then    
--        s_data_debug <= s_gbtx_register_configuration_array(to_integer(unsigned(s_side_debug)))(to_integer(unsigned(s_address_debug)));
--    end if;
--end process;

--i_vio_0 : vio_0
--  PORT MAP (
--  clk => p_clk100_in,
--  probe_in0 => s_address_debug,
--  probe_in1 => s_data_debug,
--  probe_out0 => s_address_debug,
--  probe_out1 =>  s_side_debug,
--  probe_out2 => open
--);
end Behavioral;