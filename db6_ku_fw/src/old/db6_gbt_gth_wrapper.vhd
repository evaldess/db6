----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes
--           Samuel Silverstein
-- Create Date: 08/28/2018 08:34:57 PM
-- Design Name: 
-- Module Name: gbt_sfp_gth_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: gbt gth wrapper
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
library gbt;
use gbt.gbt_bank_package.all;
use gbt.vendor_specific_gbt_bank_package.all;
use gbt.gbt_banks_user_setup.all;

library tilecal;
use tilecal.db6_design_package.all;
use tilecal.tile_link_crc.all;

entity db6_gbt_gth_wrapper is      
	port (       
      -- general reset:
      p_master_reset_in                             : in  std_logic;
      p_gbt_reset_in                                : in std_logic;
      p_gth_reset_in                                : in std_logic;
                                                  
      -- manual resets:                                                       
      p_gt0_reset_in                           : in  std_logic;
      p_gt1_reset_in                           : in  std_logic;
                      
      -- clocks scheme --                                 
      p_clknet_in                             : in t_db_clock_network;
      p_gth_clknet_in                         : in t_db_gth_clock_inputs;
      p_gth_clknet_out                        : out t_db_gth_clock_outputs;
      --p_gth_refclk_in                         : in t_diff_pair;

      -- serial lanes --
		-- gt0 link gtx pin pairs
	  p_tx_sfp_p_out  											: out std_logic_vector(1 downto 0);
      p_tx_sfp_n_out  											: out std_logic_vector(1 downto 0);
		  
      p_gbt_tx_data_in                          : in t_gbt_tx_data;
        
      -- general control --                       

      p_gt0_mgt_ready_out                                 : out std_logic;
      p_gt1_mgt_ready_out                                 : out std_logic;
                                               
      p_tx_frameclk_out                               : out std_logic;
      p_tx_wordclk_out                                : out std_logic;

      p_gth_txready_out           : out std_logic;
      p_qpll1lock_out         : out std_logic;
      p_gbt_ready_out           : out std_logic;
      
      --p_qpll1refclksel_in           : in std_logic_vector(2 downto 0);
      p_db_reg_rx_in : in  t_db_reg_rx;
      
      p_leds_out : out  std_logic_vector(3 downto 0)

      
   );
end db6_gbt_gth_wrapper;

architecture behavioral of db6_gbt_gth_wrapper is


   --=========--
   -- tx data --
   --=========--   
   
   signal s_gthtxn, s_gthtxp                    : std_logic_vector(1 downto 0);
   
   signal s_gt0_gbt_tx_data                   : std_logic_vector(115 downto 0);
   signal s_scrambled_tx_data                    : std_logic_vector(115 downto 0);
   signal s_tx_frame                         : std_logic_vector(119 downto 0);
   signal s_txheader                         : std_logic_vector( 3 downto 0);
   signal s_tx_frame_240                      : std_logic_vector(239 downto 0);
   signal s_tx_frame_240_d                    : std_logic_vector(239 downto 0);
   signal s_tx_frame0_tmp                     : std_logic_vector(119 downto 0);
  
   signal s_gth_txactive                      : std_logic; -- gth transmitter active
   signal s_gth_txready                      : std_logic; -- gth transmitter active
   signal s_qpll1lock                      : std_logic_vector(0 downto 0); -- gth transmitter active
--   signal s_gth_usrclk                        : std_logic; -- 120 mhz
--   signal s_gth_usrclk2                       : std_logic; -- 120 mhz
                                                  
   signal s_txgearbox_out_0                    : std_logic_vector(79 downto 0);
   signal s_txgearbox_out_1                    : std_logic_vector(79 downto 0);
   signal s_gth_inputbus                      : std_logic_vector(159 downto 0);

   signal s_reset_scrambler                   : std_logic := '0';
   signal s_reset_encoder                     : std_logic := '0';
   signal s_reset_gearbox                     : std_logic := '0';

   signal s_gearbox_aligned                   : std_logic := '0';
   signal s_gearbox_aligned_last              : std_logic := '0';
   signal s_gearbox_ready                     : std_logic :='0';

   --========================--                   
   -- gbt bank clocks scheme --                   
   --========================--                   
 
   signal s_txframeclk_from_txpll                   	: std_logic;   
   signal s_txframeclk_from_rxpll                   	: std_logic;   
   signal s_txwordclk_from_txpll                   	: std_logic;   
   
   signal s_mgt_refclk_i                              : std_logic_vector(1 downto 0);

   signal s_gain_select_d, s_gain_select : std_logic := '0';  -- 1 for lg, 0 for hg

   signal s_gth_clknet_in : t_db_gth_clock_inputs;
  
--gth control
  attribute keep : string;
  signal s_gthclkoutputs_buffer  : t_db_gth_clock_outputs;
  signal s_clk80, s_clk40 : std_logic_vector(1 downto 0);
  signal s_gth_tx_control : t_gth_tx_control;
  attribute keep of s_gth_tx_control : signal is "true";

  signal s_gtpowergood, s_txuserrdy, s_gttxreset, s_cpllreset : std_logic_vector(1 downto 0) := "00";
  signal s_txresetdone : std_logic_vector(1 downto 0) := "00";
  signal s_tx_reset_powerup, s_tx_reset_clkchange, s_tx_reset, s_reset_bypass, s_manual_reset : std_logic_vector(0 downto 0);
  --signal s_trigger_tx_reset : std_logic := '0';
  signal s_qpll1pd : std_logic_vector(0 downto 0) := "0";
  signal s_userclk_reset : std_logic_vector(0 downto 0) := "0";
  signal s_reset_buff_bypass : std_logic_vector(0 downto 0) := "0";
  signal s_tx_reset_done : std_logic_vector(0 downto 0) := "0";
  signal s_txpmaresetdone, s_txprgdivresetdone: std_logic_vector(1 downto 0);
  signal s_userclk_tx_usrclk_buf, s_userclk_tx_usrclk2_buf, s_userclk_tx_srcclk_buf : std_logic_vector(0 downto 0) := "0";
  signal s_userclk_tx_active : std_logic_vector(0 downto 0) := "0";
  signal s_qpll1refclk_lastsel : std_logic_vector(2 downto 0);
  signal s_qpllrefclksel : std_logic_vector(2 downto 0);
  signal s_txpd_in : std_logic_vector(3 downto 0);
  signal s_tx_active_out : std_logic:= '0';
  signal s_gth_txready_out : std_logic:= '0';
  signal s_gbt_ready_out : std_logic :='0';
  --signal s_gtgrefclk : std_logic_vector(0 downto 0);
  
  signal s_word_inject_mode : std_logic;
  --signal s_word_to_inject, s_userdata_tx_in : STD_LOGIC_VECTOR(159 DOWNTO 0) := (others => '0');
  
  constant c_txdiffctrl : STD_LOGIC_VECTOR(9 DOWNTO 0) := "1111111111"; -- Driver swing control (10110 = 809 mV)
  constant c_txpostcursor : STD_LOGIC_VECTOR(9 DOWNTO 0) :=  "0000000000"; -- Post-cursor TX pre-emphasis (10000 = 4.44 dB)
  constant c_txprecursor : STD_LOGIC_VECTOR(9 DOWNTO 0) := "0000000000"; -- Pre-cursor TX pre-emphasis (10000 = 4.44 dB)
  constant c_txswing : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- TX swing control (0 for full-swing)
  constant c_txinhibit : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- set tx outputs to 1

--  signal s_txdiffctrl, vio_txdiffctrl : STD_LOGIC_VECTOR(9 DOWNTO 0) := c_txdiffctrl; -- Driver swing control (10110 = 809 mV)
--  signal s_txpostcursor, vio_txpostcursor : STD_LOGIC_VECTOR(9 DOWNTO 0) :=  c_txpostcursor; -- Post-cursor TX pre-emphasis (10000 = 4.44 dB)
--  signal s_txprecursor, vio_txprecursor : STD_LOGIC_VECTOR(9 DOWNTO 0) := c_txprecursor; -- Pre-cursor TX pre-emphasis (1000 = 4.44 dB)
  --signal s_txswing : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- use for pciexpress
--  signal s_txmaincursor : STD_LOGIC_VECTOR(13 DOWNTO 0) := "10000001000000"; -- main cursor for signal swing control
--  signal s_txelecidle  : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- set tx outputs to common mode
--  signal s_txinhibit : STD_LOGIC_VECTOR(1 DOWNTO 0) := "00"; -- set tx outputs to 1


  attribute keep of p_gbt_tx_data_in : signal is "true";

COMPONENT db6_gth
  PORT (
    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(159 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(159 DOWNTO 0);
    gtgrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtgrefclk1_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk11_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtgrefclk_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    txdiffctrl_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    txmaincursor_in : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    txpostcursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    txprecursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    txswing_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    txsysclksel_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txoutclkfabric_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
  );
END COMPONENT;


--advanced
--COMPONENT gth_tx_2
--  PORT (
--    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_rx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_rx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_rx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_rx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(159 DOWNTO 0);
--    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(159 DOWNTO 0);
--    gtgrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtgrefclk1_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtrefclk11_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    qpll1refclksel_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtgrefclk_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
--    gthrxn_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
--    gthrxp_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
--    txdiffctrl_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    txoutclksel_in : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
--    txpostcursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    txprecursor_in : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    txpllclksel_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    txswing_in : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
--    txsysclksel_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    gtpowergood_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    gthtxn_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    gthtxp_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    txoutclk_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    txoutclkfabric_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--    txresetdone_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
--  );
--END COMPONENT;


--debug
signal s_leds_out : std_logic_vector(3 downto 0);

COMPONENT vio_gtgrefclk_debug
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out0 : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out2 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

signal s_qpll1refclksel_debug : std_logic_vector(2 downto 0):="111";
signal s_gtrefclk_debug : std_logic:='1';
signal s_reset_debug : std_logic := '0';

--=================================================================================================--
begin                 --========####   architecture body   ####========-- 
--=================================================================================================--



    p_gt0_mgt_ready_out <= s_gth_txactive;
    p_gt1_mgt_ready_out <= s_gth_txactive;
    p_gth_txready_out<= s_gth_txready_out;

-- multiplex gain
proc_gain_mux : process(p_clknet_in.refclk80)--p_clknet_in.clk80)
begin
    
    if falling_edge(p_clknet_in.refclk80) then --p_clknet_in.clk80) then
--        if ((p_db_reg_rx_in(cfb_db_debug)(9) = '1') and (p_db_reg_rx_in(cfb_db_debug)(17) = '1') and (p_db_reg_rx_in(cfb_db_debug)(25) = '1')) then
--            s_gt0_gbt_tx_data<= s_gth_tx_control.txrawtestword(115 downto 0);
            
        --debug condition
--        s_enable_gbt_word_inject_flag_from_vio<="0";
        
--        elsif  s_enable_gbt_word_inject_from_vio = "1" then
--            s_enable_gbt_word_inject_flag_from_vio<="1";
--            s_gt0_gbt_tx_data<=s_gbt_word_to_inject_from_vio(115 downto 0);
            
--        else
            --debug
--            s_enable_gbt_word_inject_flag_from_vio<="0";
            
            
            if p_clknet_in.refclk40= '1' then --p_clknet_in.clk40 = '1' then
                s_gt0_gbt_tx_data <= p_gbt_tx_data_in.lg;
            else
                s_gt0_gbt_tx_data <= p_gbt_tx_data_in.hg;
            end if;
        end if;

--    end if;
    
end process;

p_leds_out<=s_leds_out;


i_scrambler: entity gbt.gbt_tx_scrambler
   generic map(   
        gbt_bank_id => 1,  
		num_links => 1,
		tx_optimization	=> standard,
		tx_encoding	=> wide_bus )
  port map (
      -- reset & clock --   
       tx_reset_i   =>  s_reset_scrambler,
       tx_frameclk_i => p_clknet_in.refclk80,--p_clknet_in.clk80,               
      -- control --                                 
       tx_isdata_sel_i => '1',     
       tx_header_o =>  s_txheader, 
      --==============--           
      -- data & frame --           
      --==============--     
      -- common:
      tx_data_i                => s_gt0_gbt_tx_data(83 downto 0),
      tx_common_frame_o        => s_scrambled_tx_data(83 downto 0),
      -- wide-bus:    
      tx_extra_data_widebus_i     => s_gt0_gbt_tx_data(115 downto 84),
      tx_extra_frame_widebus_o    => s_scrambled_tx_data(115 downto 84)
   );
   
-- next step is the encoder. we assume wide bus encoding on transmitter and receiver.
-- the tx output frame is 120 bits wide at 80 mhz 

 encoder: entity gbt.gbt_tx_encoder
      generic map(   
           gbt_bank_id => 1,  
           num_links => 1,
           tx_optimization => standard,
           rx_optimization => standard,
           tx_encoding => wide_bus,
           rx_encoding  => wide_bus   
      )
      port map(
         -- reset & clocks --
         tx_reset_i  => s_reset_encoder,
         tx_frameclk_i    => p_clknet_in.refclk80, --p_clknet_in.clk80,
         -- frame header --
         tx_header_i     => s_txheader,      
         -- frame --        
          tx_common_frame_i   => s_scrambled_tx_data( 83 downto 0),      
          tx_extra_frame_widebus_i => s_scrambled_tx_data( 115 downto 84),  
          tx_frame_o   => s_tx_frame_240_d(239 downto 120)
      );

-- next step is to put the two 120 bit frames in parallel (240 bits total)

proc_frame_shift : process (p_clknet_in.refclk80)--p_clknet_in.clk80)
begin
    if rising_edge(p_clknet_in.refclk80) then --p_clknet_in.clk80) then
        s_tx_frame_240_d (119 downto 0) <= s_tx_frame_240_d(239 downto 120);
    end if;
end process; -- frame_0

proc_frame_1 : process (p_clknet_in.refclk40)--p_clknet_in.clk40)
begin
    if falling_edge(p_clknet_in.refclk40) then --p_clknet_in.clk40) then -- latency shift of 12.5 ns from pipelined algorithm
        s_tx_frame_240 <= s_tx_frame_240_d;
    end if;
end process; -- frame_1

-- finally, serialize the 240-bit frames to 80 bits at 120 mhz

tx_gearbox_0: entity gbt.gbt_tx_gearbox_latopt
   port map(
     -- reset:
      tx_reset_i        => s_reset_gearbox,
      -- clocks:
      tx_wordclk_i      => s_userclk_tx_usrclk2_buf(0), -- 120 mhz clock,
      tx_frameclk_i     => p_clknet_in.refclk40, --p_clknet_in.clk40,
     -- control --
      tx_mgt_ready_i    => s_gth_txactive,
 		-- status  --
		tx_gearbox_ready_o		=> s_gearbox_ready,
		tx_phaligned_o			=> s_gearbox_aligned,
		tx_phcomputed_o			=> open,
      -- frame & word --
      tx_frame_i                => s_tx_frame_240,
      tx_word_o                 => s_txgearbox_out_0
   );

--debug when 
--proc_gth_word_mux : process(s_enable_gth_word_inject_from_vio(0))
--begin
--    if s_enable_gth_word_inject_from_vio(0) = '0' then
        s_gth_inputbus <= s_txgearbox_out_0 & s_txgearbox_out_0;
--        s_enable_gth_word_inject_flag_from_vio <= "0";
--    else
--        s_gth_inputbus <= s_gth_word_to_inject_from_vio;
--        s_enable_gth_word_inject_flag_from_vio <= "1";
--    end if;
 
--end process;

 p_tx_wordclk_out <= s_userclk_tx_usrclk2_buf(0);
 
 p_gbt_ready_out <= s_gbt_ready_out;
 proc_gbt_reset_controller : process(p_clknet_in.refclk40)--p_clknet_in.clk40)
    type sm_gbt is (st_idle, st_init_gearbox, st_init_scr_enc);
    variable st_gbt : sm_gbt := st_idle;
 begin
    if rising_edge(p_clknet_in.refclk40) then --p_clknet_in.clk40) then
        if (p_master_reset_in = '1') or (p_clknet_in.locked_db = '0') or (s_qpll1lock = "0") or (p_gbt_reset_in = '1') then-- or (s_gth_txready_out ='0') then
            s_reset_scrambler <= '1';
            s_reset_encoder   <= '1';
            s_reset_gearbox   <= '1';
            st_gbt := st_init_gearbox;
            --s_gearbox_aligned_last <= s_gearbox_aligned;
        else
            case st_gbt is
                when st_init_gearbox =>
                    s_reset_scrambler <= '1';
                    s_reset_encoder   <= '1';
                    s_reset_gearbox   <= '1';
                    s_gbt_ready_out <= '0';
                    st_gbt := st_init_scr_enc;
                when st_init_scr_enc =>
                    if (s_gearbox_ready = '0') then 
                        s_reset_scrambler <= '1';
                        s_reset_encoder   <= '1';
                        s_reset_gearbox   <= '0';
                        s_gbt_ready_out <= '0';
                    else
                        st_gbt := st_idle;
                    end if;
                when st_idle =>
                    s_reset_scrambler <= '0';
                    s_reset_encoder   <= '0';
                    s_reset_gearbox   <= '0';
                    s_gbt_ready_out <= '1';

                when others =>
                    st_gbt := st_init_gearbox;
            end case;
        end if;
    end if; -- clock edge;
 end process;



i_db6_gth  : db6_gth
  PORT MAP (
    gtwiz_userclk_tx_reset_in => s_tx_reset,
    gtwiz_userclk_tx_srcclk_out => s_userclk_tx_srcclk_buf,
    gtwiz_userclk_tx_usrclk_out => s_userclk_tx_usrclk_buf,
    gtwiz_userclk_tx_usrclk2_out => s_userclk_tx_usrclk2_buf,
    gtwiz_userclk_tx_active_out => s_userclk_tx_active,
    gtwiz_userclk_rx_reset_in => "0",
    gtwiz_userclk_rx_srcclk_out => open,
    gtwiz_userclk_rx_usrclk_out => open,
    gtwiz_userclk_rx_usrclk2_out => open,
    gtwiz_userclk_rx_active_out => open,
    gtwiz_buffbypass_tx_reset_in => "0",
    gtwiz_buffbypass_tx_start_user_in => "0",
    gtwiz_buffbypass_tx_done_out => open,
    gtwiz_buffbypass_tx_error_out => open,
    gtwiz_buffbypass_rx_reset_in => "0",
    gtwiz_buffbypass_rx_start_user_in => "0",
    gtwiz_buffbypass_rx_done_out => open,
    gtwiz_buffbypass_rx_error_out => open,
    gtwiz_reset_clk_freerun_in(0) => p_clknet_in.clk40_osc,
    gtwiz_reset_all_in => s_tx_reset,
    gtwiz_reset_tx_pll_and_datapath_in => s_tx_reset,
    gtwiz_reset_tx_datapath_in => s_tx_reset,
    gtwiz_reset_rx_pll_and_datapath_in => s_tx_reset,
    gtwiz_reset_rx_datapath_in => "0",
    gtwiz_reset_rx_cdr_stable_out => open,
    gtwiz_reset_tx_done_out => s_tx_reset_done,
    gtwiz_reset_rx_done_out => open,
    gtwiz_userdata_tx_in => s_gth_inputbus,
    gtwiz_userdata_rx_out => open,
    gtgrefclk0_in => p_gth_clknet_in.gth_gtgrefclk,--p_clknet_in.clk80_cfgbus(0),
    gtgrefclk1_in => p_gth_clknet_in.gth_gtgrefclk,--p_clknet_in.clk80_cfgbus(0),
    gtrefclk01_in => p_gth_clknet_in.gth_refclk_rem,
    gtrefclk11_in => p_gth_clknet_in.gth_refclk_loc,
    --qpll1refclksel_in => p_clknet_in.qpll1refclksel,
    qpll1refclksel_in => p_clknet_in.qpll1refclksel,--s_qpll1refclksel_debug,
    qpll1lock_out => s_qpll1lock,
    qpll1outclk_out => open, --s_gthclkoutputs_buffer.gth_gtgclk,
    qpll1outrefclk_out => open, --s_gthclkoutputs_buffer.gth_gtgrefclk,
    gtgrefclk_in => "00",
    gthrxn_in => "00",
    gthrxp_in => "00",
    txdiffctrl_in => s_gth_tx_control.txdiffctrl(7 downto 0),
    --txoutclksel_in => "011011", --TXOUTCLKSEL = 3'b011 or 3'b100: TXPLLREFCLK_DIV1 or TXPLLREFCLK_DIV2 is the input reference clock to the CPLL or QPLL, depending on the TXSYSCLKSEL setting. TXPLLREFCLK is the recommended clock for general usage and is required for the TX buffer bypass mode
    txmaincursor_in => s_gth_tx_control.txmaincursor,
    txpostcursor_in => s_gth_tx_control.txpostcursor,
    txprecursor_in => s_gth_tx_control.txprecursor,
    --txpllclksel_in => "1010",
    txswing_in => c_txswing,
    txsysclksel_in => "1111",
    gtpowergood_out => s_gtpowergood,
    gthtxn_out => p_tx_sfp_n_out,
    gthtxp_out => p_tx_sfp_p_out,
    rxpmaresetdone_out => open,
    --txoutclk_out => s_gthclkoutputs_buffer.gth_gtgrefclk,
    txoutclkfabric_out => s_gthclkoutputs_buffer.gth_gtgrefclk_fabric,
    txpmaresetdone_out => open,
    txprgdivresetdone_out => open,
    txresetdone_out => s_txresetdone
  );



gen_gthrefout : for g in 0 to 1 generate

--p_gth_clknet_out.gth_gtgrefclk_fabric(g)<=s_gthclkoutputs_buffer.gth_gtgrefclk_fabric(g);
--s_clk80(g)<=s_gthclkoutputs_buffer.gth_gtgrefclk_fabric(g);
--s_clk40(g)<=s_gthclkoutputs_buffer.gth_gtgrefclk_fabric_div(g);
p_gth_clknet_out.gth_gtgrefclk_fabric(g)<=s_gthclkoutputs_buffer.gth_gtgrefclk_fabric(g);
p_gth_clknet_out.gth_gtgrefclk(g)<=s_gthclkoutputs_buffer.gth_gtgrefclk(g);
p_gth_clknet_out.gth_gtgrefclk_fabric_div(g)<=s_gthclkoutputs_buffer.gth_gtgrefclk_fabric_div(g);

--i_gthrefout_fabric_bufg : bufg port map (i => s_gthclkoutputs_buffer.gth_gtgrefclk_fabric(g), o => p_gth_clknet_out.gth_gtgrefclk_fabric(g));
i_gthrefout_fabric_bufgce_div : bufgce_div
generic map (
    bufgce_divide => 2, -- 1-8
    -- programmable inversion attributes: specifies built-in programmable inversion on specific pins
    is_ce_inverted => '0', -- optional inversion for ce
    is_clr_inverted => '0', -- optional inversion for clr
    is_i_inverted => '0' -- optional inversion for i
    )
    port map (
    o => s_gthclkoutputs_buffer.gth_gtgrefclk_fabric_div(g), -- 1-bit output: buffer
    ce => '1', -- 1-bit input: buffer enable
    clr => s_gthclkoutputs_buffer.bufgce_div_ctrl_reset(g), -- 1-bit input: asynchronous clear
    i => s_gthclkoutputs_buffer.gth_gtgrefclk_fabric(g) -- 1-bit input: buffer
);


--proc_phase_sync : process(s_gthclkoutputs_buffer.gth_gtgrefclk_fabric(0))
--type t_phase_sync_sm is (st_initialize, st_wait_for_trigger, st_idle);
--variable v_phase_sync_sm : t_phase_sync_sm := st_initialize;
--variable v_counter: integer:=0;
--constant c_delay: integer:=8000000;
--begin
    
--    if rising_edge(s_gthclkoutputs_buffer.gth_gtgrefclk_fabric(0)) then
    
--        if p_master_reset_in = '0' then
        
--            case v_phase_sync_sm is             
--                when st_initialize =>
--                    if p_clknet_in.clk40 = '0' then
--                        if v_counter<c_delay then
--                            v_counter:=v_counter+1;
--                        else
--                            v_counter:=0;
--                            v_phase_sync_sm:= st_wait_for_trigger;
--                        end if;
                        
--                    end if;
--                    s_gthclkoutputs_buffer.bufgce_div_ctrl_reset(g)<='1';
--                when st_wait_for_trigger =>
--                    if p_clknet_in.clk40 = '1' then
--                        v_phase_sync_sm:= st_idle;
--                        s_gthclkoutputs_buffer.bufgce_div_ctrl_reset(g)<='0';
--                    end if;
--                when st_idle=>
                    s_gthclkoutputs_buffer.bufgce_div_ctrl_reset(g)<='0';
--                when others =>
--                    v_phase_sync_sm:= st_initialize;
--            end case;
        
--        else
        
--            v_phase_sync_sm:=st_initialize;
        
--        end if;
    
    
--    end if;

--end process;


--i_bufg_gt_refclk : bufg_gt
--port map (
--o => p_gth_clknet_out.gth_gtgrefclk(g), -- 1-bit output: buffer
--ce => '1', -- 1-bit input: buffer enable
--cemask => '0', -- 1-bit input: ce mask
--clr => '0', -- 1-bit input: asynchronous clear
--clrmask => '0', -- 1-bit input: clr mask
--div => "000", -- 3-bit input: dymanic divide value
--i => s_gthclkoutputs_buffer.gth_gtgrefclk(g) -- 1-bit input: buffer
--);
end generate;

--i_bufg_gt_clk : bufg_gt
--port map (
--o => p_gth_clknet_out.gth_gtgclk(0), -- 1-bit output: buffer
--ce => '1', -- 1-bit input: buffer enable
--cemask => '0', -- 1-bit input: ce mask
--clr => '0', -- 1-bit input: asynchronous clear
--clrmask => '0', -- 1-bit input: clr mask
--div => "000", -- 3-bit input: dymanic divide value
--i => s_gthclkoutputs_buffer.gth_gtgclk(0) -- 1-bit input: buffer
--);


--    proc_sft_control_mux : process(s_enable_gth_control_from_vio(0))
--    begin
--        if s_enable_gth_control_from_vio = "0" then
--            s_enable_gth_control_from_vio_flag <="0";
            s_gth_tx_control.txdiffctrl<= p_db_reg_rx_in(adv_cfg_gth_txdiffctrl)(9 downto 0);
            s_gth_tx_control.txpostcursor <= p_db_reg_rx_in(adv_cfg_gth_txpostcursor)(9 downto 0);
            s_gth_tx_control.txprecursor<= p_db_reg_rx_in(adv_cfg_gth_txprecursor)(9 downto 0);
            s_gth_tx_control.txmaincursor<= p_db_reg_rx_in(adv_cfg_gth_txmaincursor)(13 downto 0);
--        else
--            --debug
--            s_enable_gth_control_from_vio_flag <="1";
--            s_gth_tx_control.txdiffctrl<= s_txdiffctrl_from_vio;
--            s_gth_tx_control.txpostcursor <= s_txpostcursor_from_vio;
--            s_gth_tx_control.txprecursor<= s_txprecursor_from_vio;
--            s_gth_tx_control.txmaincursor<= s_txmaincursor_from_vio;
--        end if;
--    end process;


--    proc_get_txrawtestword : process(p_clknet_in.clk40)
--    begin
--        if rising_edge(p_clknet_in.clk40) then
--            case p_db_reg_rx_in(adv_cfg_txrawtestword)(31 downto 30) is
--                when "00"=>
--                    s_gth_tx_control.txrawtestword(29 downto 0)<=p_db_reg_rx_in(adv_cfg_txrawtestword)(29 downto 0); 
--                when "01"=>
--                    s_gth_tx_control.txrawtestword(59 downto 30)<=p_db_reg_rx_in(adv_cfg_txrawtestword)(29 downto 0); 
--                when "10"=>
--                    s_gth_tx_control.txrawtestword(89 downto 60)<=p_db_reg_rx_in(adv_cfg_txrawtestword)(29 downto 0); 
--                when "11"=>
--                    s_gth_tx_control.txrawtestword(119 downto 90)<=p_db_reg_rx_in(adv_cfg_txrawtestword)(29 downto 0); 
--                when others=>
--            end case;
--        end if;
--    end process;

---------------------------------------------------
 -- gth reset synchronization --
 ---------------------------------------------------


--debug
--s_gtpowergood <= "11";
--s_qpll1lock <= "1";
--s_txresetdone <= "11";
proc_gth_reset_sync : process (p_clknet_in.clk40_osc)
        type sm_gth is (st_get_clk40_locked,st_get_powergood, st_release_reset, st_wait_lock,st_wait_reset_done, st_buffreset, st_idle);
        variable st_gth : sm_gth := st_get_powergood;
        variable v_counter : integer := 0;
        constant c_reset_timeout : integer := 500000;
        constant c_powergood_timeout : integer := 200000;
        constant c_tx_reset_timeout: integer := 200000;
        constant c_buffer_reset_timeout : integer := 2000000;
    begin
        if rising_edge(p_clknet_in.clk40_osc) then
            p_qpll1lock_out<=s_qpll1lock(0);
            s_leds_out(3)<=s_qpll1lock(0);
            if (p_master_reset_in = '0') and (p_gth_reset_in = '0') then--and (s_reset_debug = '0') then
                s_qpll1pd <= "0";
                s_txpd_in <= "0000";
                v_counter := v_counter + 1;
                case st_gth is
                    when st_get_clk40_locked =>  -- wait for GTPOWERGOOD, then assert tx_reset
                        s_leds_out(2 downto 0)<= "001";
                        s_gth_txready_out <= '0';
                        s_tx_reset <= "1";
                        if (p_clknet_in.locked_db='1') then
                            st_gth := st_get_powergood;
                        end if;
                    when st_get_powergood =>  -- wait for GTPOWERGOOD, then assert tx_reset
                        s_leds_out(2 downto 0)<= "010";
                        s_gth_txready_out <= '0';
                        s_tx_reset <= "1";
                        if (s_gtpowergood = "11") then
                            st_gth := st_release_reset;
                            v_counter := 0;
                        else
                            if v_counter>c_powergood_timeout then                             
                                if (s_gtpowergood(0) = '1') or (s_gtpowergood(1) = '1') then
                                    st_gth := st_release_reset;
                                    v_counter := 0;
                                end if;
                            end if;
                        end if;
                    when st_release_reset =>  -- wait 5 us, then deassert tx_reset
                        s_leds_out(2 downto 0)<= "011";
                        s_gth_txready_out <= '0'; 
                        if v_counter > c_reset_timeout then
                            s_tx_reset <= "0";
                            st_gth := st_wait_lock;
                            v_counter := 0;
                        end if;
                    when st_wait_lock =>  -- wait for tx reset to be done, then reset buffer bypass
                        s_leds_out(2 downto 0)<= "100";
                        s_tx_reset <= "0";
                        s_gth_txready_out <= '0';
                        if s_qpll1lock = "1" then
                            v_counter := 0;
                            st_gth := st_wait_reset_done;
                        end if;                    
                    when st_wait_reset_done =>
                        s_leds_out(2 downto 0)<= "101";
                        s_tx_reset <= "0";
                        s_gth_txready_out <= '0';
                        if s_txresetdone ="11" then
                            v_counter := 0;
                            st_gth := st_buffreset;
                        else
                            if v_counter>c_tx_reset_timeout then
                                if (s_txresetdone(0) or s_txresetdone(1)) = '1' then
                                    v_counter := 0;
                                    st_gth := st_buffreset;
                                end if;
                            end if;
                        end if;
                    when st_buffreset => -- deassert GTTXRESET and wait for 20 us
                        s_leds_out(2 downto 0)<= "110";
                        s_tx_reset <= "0";
                        s_gth_txready_out <= '0';
                        if v_counter > c_buffer_reset_timeout then
                            s_reset_bypass <= "0"; -- optional signal for buffer bypass
                            st_gth := st_idle;
                        else
                            s_reset_bypass <= "1";
                        end if;
                    when st_idle => -- idle state
                        s_leds_out(2 downto 0)<= "111";
                        s_tx_reset <= "0";
                        s_gth_txready_out <= '1';
                    when others=>
                end case;
            else
                s_qpll1pd <= "1";
                s_txpd_in <= "1111";
                s_leds_out(3)<=p_clknet_in.clk5hz_osc;
                s_leds_out(2 downto 0)<= "000";
                s_gth_txready_out <= '0';
                s_tx_reset <= "1";
                st_gth := st_get_clk40_locked;
            end if;
        end if; -- clock edge
    end process;


--i_vio_gtgrefclk_debug : vio_gtgrefclk_debug
--  PORT MAP (
--    clk => p_clknet_in.clk40_osc,
--    probe_in0 => p_clknet_in.qpll1refclksel,
--    probe_in1 => s_qpll1refclksel_debug,
--    probe_in2(0) => s_reset_debug,
--    probe_in3 => s_tx_reset,
--    probe_in4 => s_qpll1lock,
--    probe_in5(0) => s_gtrefclk_debug,
--    probe_out0 => s_qpll1refclksel_debug,
--    probe_out1(0) => s_gtrefclk_debug,
--    probe_out2(0) => s_reset_debug
--  );

end behavioral;


