----------------------------------------------------------------------------------
-- company: 
-- engineer: Sam Silverstein 
--           Eduardo Valdes
--           Steffen Muschter
--           Henrik Aekrstedt
--           Alberto Valero
--           Fernando Carrio --
-- create date: 08/28/2018 04:15:56 pm
-- design name: 
-- module name: db6_system - behavioral
-- project name: 
-- target devices: 
-- tool versions: 
-- description: 
-- 
-- dependencies: 
-- 
-- revision:
-- revision 0.01 - file created
-- additional comments:
-- 
----------------------------------------------------------------------------------

-- common libraries --
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
-- xilinx libraries --
library unisim;
use unisim.vcomponents.all;
-- user defined libraries --

library tilecal;
use tilecal.db6_design_package.all;


library gbt;
use gbt.gbt_bank_package.all;
use gbt.vendor_specific_gbt_bank_package.all;
use gbt.gbt_banks_user_setup.all;

entity db6_system is
    generic (g_priority_side : std_logic_vector(1 downto 0) := "01");
  port(
    -- signals for system operation only --
    p_db_side_in                    : in  std_logic_vector(1 downto 0);
	-- main sfp signals --
    p_txn_out                         : out std_logic_vector(1 downto 0);
    p_txp_out                         : out std_logic_vector(1 downto 0);
    --gbtx signals
    p_db_rxready_in                 : in std_logic_vector(1 downto 0);
    --p_system_gbtx_rxready_out : out std_logic_vector(1 downto 0);
    --leds/debug
    p_leds_system_out                      : out std_logic_vector(3 downto 0);
    --clocks
    p_clkinputs_in                       : in t_db_clock_inputs;
    p_gty_clknet_in                         : in t_db_gty_clock_inputs;
    p_clknet_out                        : out t_db_clock_network;

    -- data to serialize
    p_gbt0_tx_lg_in           		: in  gbt_txbus;
    p_gbt0_tx_hg_in           		: in  gbt_txbus;
    p_gbt1_tx_lg_in           		: in  gbt_txbus;
    p_gbt1_tx_hg_in                 : in  gbt_txbus;

    -- monitoring_signals
    --p_gbt_wordclk_out   : out std_logic; -- 120 mhz gty word clock;
    p_master_reset_in   : in std_logic;
    
     --configbus
     p_configbus_local_in       : in std_logic_vector (7 downto 0);
     p_configbus_remote_in       : in std_logic_vector (7 downto 0);
     p_configbus_clk_net_out  : out   t_configbus_clock_network;
     p_bcr_out                   :out std_logic_vector(1 downto 0); -- 0 local, 1 remote
     p_db_config_reg_out      : out t_config_reg_array;
     p_orbit_strobe_out       : out std_logic;
     p_configbus_ready_out    : out std_logic;
     p_leds_configbus_out : out std_logic_vector(3 downto 0);
    
     --gbtx interface
     p_gbtx_db_side : in std_logic_vector(1 downto 0);
     p_gbtx_read_write_operation_in : in std_logic;
     p_gbtx_trigger_i2c_operation_in : in std_logic;
     p_gbtx_busy_out                 : out std_logic;
     p_wipe_gbtx_registers      : in  std_logic;
     p_gbtx_default_configuration : in std_logic;
     p_gbtx_user_address_in: in std_logic_vector(15 downto 0);
     p_gbtx_user_data_in: in std_logic_vector(7 downto 0);
     p_gbtx_user_data_out: out std_logic_vector(7 downto 0);
     p_gbtx_scl_inout     : inout std_logic;
     p_gbtx_sda_inout    : inout std_logic;
     p_leds_gbtx_out : out std_logic_vector(3 downto 0);
     
     --serial_id
     p_serial_id_trigger_i2c_operation : in std_logic;
     p_serial_id_busy_out : out std_logic;
     p_serial_id_scl_inout : inout std_logic;
     p_serial_id_sda_inout : inout std_logic
     
     
    );

end db6_system;

architecture rtl of db6_system is

--signal s_system_gbtx_rxready : std_logic_vector(1 downto 0);

signal s_gty_tx_control : t_gty_tx_control;

component gbt_sfp_gty_wrapper      
	port (       
      -- general reset:
      p_master_reset_in                             : in  std_logic;
                                                  
      -- manual resets:                                                       
      p_gt0_reset_in                           : in  std_logic;
      p_gt1_reset_in                           : in  std_logic;
                      
      -- clocks scheme --                                 
      p_clknet_in                                    : in t_db_clock_network;
      p_gty_clknet_in                         : in t_db_gty_clock_inputs;
      

      -- serial lanes --
		-- gt0 link gtx pin pairs
	  p_gt0_tx_p_out  											: out std_logic;
      p_gt0_tx_n_out  											: out std_logic;
		
		-- gt1 link gtx pin pairs
 	  p_gt1_tx_p_out 											: out std_logic;
      p_gt1_tx_n_out  											: out std_logic;
		  
		-- gt0 link gbt interface port --
      p_gt0_gbt_tx_data_lg_in 									: in  std_logic_vector (115 downto 0);
	  p_gt0_gbt_tx_data_hg_in 									: in  std_logic_vector (115 downto 0);
		-- gt01link gbt interface port --
	  p_gt1_gbt_tx_data_lg_in 									: in  std_logic_vector (115 downto 0);
	  p_gt1_gbt_tx_data_hg_in 									: in  std_logic_vector (115 downto 0);                      
      -- general control --                       

      p_gt0_mgt_ready_out                                 : out std_logic;
      p_gt1_mgt_ready_out                                 : out std_logic;

      p_tx_frameclk_out                               : out std_logic;
      p_tx_wordclk_out                                : out std_logic;

      p_gty_txready_out           : out std_logic;
      p_qpll1lock_out         : out std_logic;
      
      --transceiver tuning
      p_gty_tx_control_in : in t_gty_tx_control
     
   );
end component;


--configbus

  signal s_db_config_registers : t_config_reg_array;
  signal s_configbus_clk_net : t_configbus_clock_network;
  signal s_configbus_loc : std_logic_vector(7 downto 0);
  signal s_configbus_rem : std_logic_vector(7 downto 0);
  signal s_configbus_clk : std_logic_vector(1 downto 0);
  signal s_orbit_strobe, s_configbus_ready : std_logic;
  
component db6_configbus_interface
    Port (     --p_db_side_in             : in std_logic_vector(1 downto 0);
               p_master_reset_in        : in    std_logic;
               p_clknet_in              : in    t_db_clock_network;
               p_configbus_clk_net_out  : out   t_configbus_clock_network;
               p_configbus_local_in       : in std_logic_vector (7 downto 0);
               p_configbus_remote_in       : in std_logic_vector (7 downto 0);      
               --p_gbtx_rxready_in           : in std_logic_vector(1 downto 0); -- 0 local, 1 remote 
               --p_system_gbtx_rxready_in    : in std_logic_vector(1 downto 0); -- 0 local, 1 remote
               --p_bcr_out                   :out std_logic_vector(1 downto 0); -- 0 local, 1 remote
               p_bcr_out                   :out std_logic;
               p_db_config_reg_out      : out t_config_reg_array;
               p_orbit_strobe_out       : out std_logic;
               p_configbus_ready_out    : out std_logic;
               p_leds_out : out std_logic_vector(3 downto 0)
    );
end component;

  -- master reset signal --
  signal s_gbt_reset               		: std_logic := '0';
  signal s_cfgbus_reset                 : std_logic := '0';
  
  -- data signals 
  signal s_gt0_gbt_tx_data_lg_i  : std_logic_vector (115 downto 0);
  signal s_gt0_gbt_tx_data_hg_i  : std_logic_vector (115 downto 0);
  signal s_gt1_gbt_tx_data_lg_i  : std_logic_vector (115 downto 0);
  signal s_gt1_gbt_tx_data_hg_i  : std_logic_vector (115 downto 0);
  
  	
	-- clock related signals
   
	signal s_tx_frameclk_from_gbt					: std_logic;
	signal s_tx_wordclk_from_gbt 					: std_logic;

	----  reset ----
	signal s_gt0_reset_tx		: std_logic;
	signal s_gt1_reset_tx		: std_logic;

	
	---- status signal ----
   -- plls status
	
	signal refclk_mmcm_lock						: std_logic;

	signal s_gt0_mgtready_from_gbt  				: std_logic;
	signal s_gt1_mgtready_from_gbt  				: std_logic;

	signal gt0_testpattersel_from_user  : std_logic_vector(1 downto 0):="10";
	signal gt1_testpattersel_from_user  : std_logic_vector(1 downto 0):="10";
	signal gt0_txisdatasel_from_user    : std_logic;
	signal gt1_txisdatasel_from_user    : std_logic;
		
	
	-----------------------------------------------------------------------------------------------
	--============   gbt3 remote configuration and control  ===============--
	-----------------------------------------------------------------------------------------------
	signal s_master_reset          										: std_logic;
    signal s_db_rxready                                                 : std_logic_vector(1 downto 0);



--clocks interface
component db6_clock_interface
    port(
    --clocks
    p_clkinputs_in                       : in t_db_clock_inputs;
    p_clknet_out                        : out t_db_clock_network;
        
    --control signals
    p_master_reset_in   : in std_logic;
    p_clkin_sel_in   : in std_logic
    );

end component;

signal s_clknet : t_db_clock_network;
signal s_gbt_clk_sel, s_gbt_clk40_mmcm_reset : std_logic;

-- counters for blinking lights and other diagnostics

    --signal s_blink_osc, s_blink_40mhz_local, s_blink_40mhz_remote: std_logic := '0';
    --signal blink_counter_40: std_logic_vector(24 downto 0) := (others=>'0');

signal s_qpll1lock         : std_logic:='1';
signal s_gty_txready       : std_logic:='1';


--gbtx config and control
component db6_gbtx_i2c_interface 
  Port (       
				p_clk100_in 		: in std_logic; -- 100 MHz clock
				p_db_side : in std_logic_vector(1 downto 0);
				p_gbtx_db_side : in std_logic_vector(1 downto 0);
				p_read_write_operation_in : in std_logic;
				p_trigger_i2c_operation_in : in std_logic;
				p_busy_out                 : out std_logic;
				p_wipe_gbtx_registers      : in  std_logic;
				p_gbtx_default_configuration : in std_logic;
				p_user_address_in: in std_logic_vector(15 downto 0);
				p_user_data_in: in std_logic_vector(7 downto 0);
				p_user_data_out: out std_logic_vector(7 downto 0);
				p_scl_inout 	: inout std_logic;
				p_sda_inout    : inout std_logic;
				p_leds_out : out std_logic_vector(3 downto 0)
				
				

    );
end component;


component db6_serial_id_i2c_interface 
  Port ( 
  
  p_clk100_in : in std_logic;
  p_trigger_i2c_operation : in std_logic;
  p_busy_out : out std_logic;
  p_serial_id_scl_inout : inout std_logic;
  p_serial_id_sda_inout : inout std_logic;
  p_serial_id_data_out :out std_logic_vector(63 downto 0)
  
  );
end component;

begin  -- rtl

--clknet
p_clknet_out<= s_clknet;

-- system signals 
--p_system_gbtx_rxready_out <= s_system_gbtx_rxready;
p_db_config_reg_out <= s_db_config_registers;
 
  --##################################################################--
  --##            			gbt and gty modules  			    	##--
  --##################################################################--


  s_gt0_gbt_tx_data_lg_i <= p_gbt0_tx_lg_in.data;
  s_gt0_gbt_tx_data_hg_i <= p_gbt0_tx_hg_in.data;
  s_gt1_gbt_tx_data_lg_i <= p_gbt1_tx_lg_in.data;
  s_gt1_gbt_tx_data_hg_i <= p_gbt1_tx_hg_in.data;


  ----------------------------------------------------------------------------
  ---=============  instantiation of the clocking network  ===============----
  ----------------------------------------------------------------------------
 
 i_db6_clock_interface : db6_clock_interface 
 port map(
 p_clkinputs_in =>p_clkinputs_in,
 p_clknet_out =>s_clknet,
  --control signals
 p_master_reset_in => s_gbt_clk40_mmcm_reset,
 p_clkin_sel_in => s_gbt_clk_sel
 );

 ---------------------------------------------------
 -- dynamic selection of 40 mhz mmcm clock inputs --
 ---------------------------------------------------
  
 gbt_clock_selection : process(s_clknet.clk100)
 
    --variable input : integer := 0; -- 1 for local, 2 for remote, 3 for none
    type sm_clocksel_state is (st_initialize, st_idle_loc, st_idle_rem, st_set_source, st_reset, st_release,st_led_play_shift_up,st_led_play_shift_down);
    variable sm_state: sm_clocksel_state := st_initialize;
    variable st_chosen_state: sm_clocksel_state := st_idle_loc;
    variable v_rxready_buffer : std_logic_vector(1 downto 0);
    variable v_counter : integer:=0;
    constant c_stability: integer:=10000;
    constant c_led_shift_lock_time:integer :=30;
    constant c_led_shift_debug_lock_time :integer :=254;
    constant c_led_shift_lock_timeout: integer :=1022;
    variable v_play_leds: std_logic_vector (3 downto 0):="0001";
    constant c_play_leds : integer:= 500000;
    variable v_state_counter : integer :=0;
    
begin
    if rising_edge (s_clknet.clk10) then

        --general reset from configbus
        if (p_master_reset_in ='1') then
            sm_state:=st_initialize;
        else

            case sm_state is
                when st_initialize =>
                    --s_clknet.all_clks_ready<= '0';
                    p_leds_system_out<="0000";
                    --s_system_gbtx_rxready <= "00";
                    s_gbt_reset <= '1';
                    s_cfgbus_reset <= '1';
                    s_gbt_clk40_mmcm_reset <= '1';
                    s_gt0_reset_tx<= '0';
                    s_gt1_reset_tx<= '0';
                    v_rxready_buffer:=p_db_rxready_in;
                    if (v_counter<c_stability) then
                        if ((p_db_rxready_in(0) = v_rxready_buffer(0)) or (p_db_rxready_in(1) = v_rxready_buffer(1))) and (p_db_rxready_in/="00") then
                            v_counter:=v_counter+1;
                        else
                            sm_state:=st_initialize;
                            v_counter:=0;
                        end if;
                    else
                        sm_state:=st_set_source;
                        v_counter:=0;
                    end if;
    
                when st_set_source =>
                    --s_clknet.all_clks_ready<= '0';
                    p_leds_system_out<="0000";
                    --s_system_gbtx_rxready <= "00"; 
                    v_rxready_buffer:=p_db_rxready_in;
                    sm_state := st_reset;
                    s_gbt_reset <= '1';
                    s_cfgbus_reset <= '1';
                    s_gbt_clk40_mmcm_reset <= '1';
                    
                    -- this selects preferrably side a
                    -- very very descriptive :p
                    case p_db_rxready_in is
                        when "00" =>
                            sm_state:=st_initialize;
                        when "01" =>
                        case p_db_side_in is
                            when "01"=> 
                                s_gbt_clk_sel <= '1';
                                st_chosen_state := st_led_play_shift_up;
                            when "10" =>
                                s_gbt_clk_sel <= '1';
                                st_chosen_state := st_led_play_shift_up; 
                            when others =>
                        end case;
                        when "10" => 
                            case p_db_side_in is
                                when "01"=> 
                                    s_gbt_clk_sel <= '0';
                                    st_chosen_state := st_led_play_shift_down;
                                when "10" =>
                                    s_gbt_clk_sel <= '0';
                                    st_chosen_state := st_led_play_shift_down; 
                                when others =>
                            end case;
                        when "11" =>
                            case p_db_side_in is
                                when "01"=> 
                                    s_gbt_clk_sel <= '1';
                                    st_chosen_state := st_led_play_shift_up;
                                when "10" =>
                                    s_gbt_clk_sel <= '0';
                                    st_chosen_state := st_led_play_shift_down; 
                                when others =>
                            end case;
                        when others=>
                        
                    end case;    
    
                when st_reset =>       -- hold reset for one more clock cycle
                   --s_clknet.all_clks_ready<= '0';
                   p_leds_system_out<="0000";
                   --s_system_gbtx_rxready <= "00";
                   v_rxready_buffer:=p_db_rxready_in;
                   s_gbt_reset <= '1';
                   s_cfgbus_reset <= '1';
                   s_gbt_clk40_mmcm_reset <= '1';
                   sm_state := st_release;
                   
                when st_release =>    -- release reset and to chosen state
                   --s_clknet.all_clks_ready<= '0';
                   p_leds_system_out<="0000";
                   --s_system_gbtx_rxready <= "00";
                   v_rxready_buffer:=p_db_rxready_in;
                   s_gbt_reset <= '0';
                   s_cfgbus_reset <= '1';
                   s_gbt_clk40_mmcm_reset <= '0';
                   v_play_leds:= "0001";
                   sm_state := st_chosen_state;                
    
                 when st_led_play_shift_up =>
                   --s_clknet.all_clks_ready<= '0';
                   s_gbt_clk_sel <= '1';
                   s_gbt_reset <= '0';
                   s_cfgbus_reset <= '1';
                   s_gbt_clk40_mmcm_reset <= '0';
                   --s_system_gbtx_rxready <= "00";
                   
                   p_leds_system_out<=v_play_leds;
                      if (v_counter > c_play_leds) then
                          v_counter:=0;
                          v_state_counter:=v_state_counter+1;
                          
                          if v_state_counter > c_led_shift_lock_timeout then
                            v_play_leds:=s_gty_txready&s_qpll1lock&s_clknet.locked_db&s_clknet.clk1hz;
                          else
                            v_play_leds:=v_play_leds(2 downto 0) & v_play_leds(3);
                          end if;
                          
                          -- give time for the devices to lock
                          if v_state_counter > c_led_shift_lock_time then
                                --v_state_counter:=0;
                                --ready
                                if ((s_qpll1lock='1') and (s_gty_txready='1') and (s_clknet.locked_db='1')) then
                                  sm_state:= st_idle_loc;
                                  v_counter:=0;
                                  v_state_counter:=0;
                                
                                
                                elsif (v_rxready_buffer(0)/=p_db_rxready_in(0)) then
                                  sm_state:=st_initialize;
                                end if;
                                --timeout
                                if (v_state_counter > c_led_shift_lock_timeout) then
                                    sm_state:=st_initialize;
                                    v_state_counter:=0;
                                end if;  
                           end if;
                      else
                          v_counter:=v_counter+1;
                      end if;             
    
                 when st_led_play_shift_down =>
                        --s_clknet.all_clks_ready<= '0';
                        s_gbt_clk_sel <= '0';
                        s_gbt_reset <= '0';
                        s_gbt_clk40_mmcm_reset <= '0';
                        s_cfgbus_reset <= '1';
                        --s_system_gbtx_rxready <= "00";
                        
                        p_leds_system_out<=v_play_leds;
                           if (v_counter > c_play_leds) then
                               v_counter:=0;
                               v_state_counter:=v_state_counter+1;
                               
                               if v_state_counter > c_led_shift_debug_lock_time then
                                 v_play_leds:=s_gty_txready&s_qpll1lock&s_clknet.locked_db&s_clknet.clk1hz;
                               else
                                 v_play_leds:=v_play_leds(0) & v_play_leds(3 downto 1);
                               end if;
                               
                               -- give time for the devices to lock and checks lock status
                               if v_state_counter > c_led_shift_lock_time then
                                   --v_state_counter:=0;
                                   --ready
                                   if ((s_qpll1lock='1') and (s_gty_txready='1') and (s_clknet.locked_db='1')) then
                                       sm_state:= st_idle_rem;
                                       v_counter:=0;
                                       v_state_counter:=0;
                                   elsif (v_rxready_buffer(1)/=p_db_rxready_in(1)) then
                                       sm_state:=st_initialize;
                                   end if;
                               end if;
                               
                               --timeout
                               if (v_state_counter > c_led_shift_lock_timeout) then
                                    sm_state:=st_initialize;
                                    v_state_counter:=0;
                               end if;                               
                           else
                               v_counter:=v_counter+1;
                           end if;             

            
    
                 when st_idle_loc =>
                    --s_clknet.all_clks_ready<= s_qpll1lock and s_clknet.locked_db;
                    s_gbt_clk_sel <= '1';
                    s_gbt_reset <= '0';
                    s_gbt_clk40_mmcm_reset <= '0';
                    s_cfgbus_reset <= '0';
                    --s_system_gbtx_rxready <= "01";            
                    --v_rxready_buffer:=p_db_rxready_in;
                    p_leds_system_out(0) <= s_clknet.clk1hz;
                    p_leds_system_out(1) <= s_qpll1lock;
                    p_leds_system_out(2) <= s_clknet.locked_db;
                    p_leds_system_out(3) <= s_gbt_clk_sel;                
                    if (v_rxready_buffer(0)/=p_db_rxready_in(0)) then
                        sm_state:=st_initialize;
                    end if;
    
                when st_idle_rem =>
                    --s_clknet.all_clks_ready<= s_qpll1lock and s_clknet.locked_db;
                    s_gbt_clk_sel <= '0';
                    s_gbt_reset <= '0';
                    s_gbt_clk40_mmcm_reset <= '0';
                    s_cfgbus_reset <= '0';
                    --s_system_gbtx_rxready <= "10";
                    --v_rxready_buffer:=p_db_rxready_in;
                    p_leds_system_out(0) <= s_clknet.clk1hz;
                    p_leds_system_out(1) <= s_qpll1lock;
                    p_leds_system_out(2) <= s_clknet.locked_db;
                    p_leds_system_out(3) <= s_gbt_clk_sel;                
                    --rxready changes
                    if (v_rxready_buffer(1)/=p_db_rxready_in(1)) then
                        sm_state:=st_initialize;
                    end if;
                    
                when others=>
    
    
            end case;
        end if;
    end if; -- clock
end process; -- gbt_clock_selection



--   =========================--
--    gbt db gty                --
--   =========================--
    s_gty_tx_control.txdiffctrl<= s_db_config_registers(adv_cfg_gty_txdiffctrl)(9 downto 0);
    s_gty_tx_control.txpostcursor <= s_db_config_registers(adv_cfg_gty_txpostcursor)(9 downto 0);
    s_gty_tx_control.txprecursor<= s_db_config_registers(adv_cfg_gty_txprecursor)(9 downto 0);
    s_gty_tx_control.txmaincursor<= s_db_config_registers(adv_cfg_gty_txmaincursor)(13 downto 0);
    proc_get_txrawtestword : process(s_clknet.clk40)
    begin
        if rising_edge(s_clknet.clk40) then
            case s_db_config_registers(adv_cfg_txrawtestword)(31 downto 30) is
                when "00"=>
                    s_gty_tx_control.txrawtestword(29 downto 0)<=s_db_config_registers(adv_cfg_txrawtestword)(29 downto 0); 
                when "01"=>
                    s_gty_tx_control.txrawtestword(59 downto 30)<=s_db_config_registers(adv_cfg_txrawtestword)(29 downto 0); 
                when "10"=>
                    s_gty_tx_control.txrawtestword(89 downto 60)<=s_db_config_registers(adv_cfg_txrawtestword)(29 downto 0); 
                when "11"=>
                    s_gty_tx_control.txrawtestword(119 downto 90)<=s_db_config_registers(adv_cfg_txrawtestword)(29 downto 0); 
                when others=>
            end case;
        end if;
    end process;

    
   i_gbt_sfp_gty_wrapper:  gbt_sfp_gty_wrapper
		port map (
         -- resets scheme:      
		 p_master_reset_in 		=> s_gbt_reset,               
         --------------------------------------------
         p_gt0_reset_in     => s_gt0_reset_tx,
		 p_gt1_reset_in     => s_gt1_reset_tx,

         -- clocks scheme:                           
         p_clknet_in                     => s_clknet,
         p_gty_clknet_in                    => p_gty_clknet_in,
         
         -- serial lanes:                            
         p_gt0_tx_p_out 					=> p_txp_out(0),                
         p_gt0_tx_n_out 					=> p_txn_out(0),                
			
         p_gt1_tx_p_out 					=> p_txp_out(1),                
         p_gt1_tx_n_out 					=> p_txn_out(1),                

         p_gt0_mgt_ready_out         => s_gt0_mgtready_from_gbt,             
         p_gt1_mgt_ready_out         => s_gt1_mgtready_from_gbt,             
         -- gbt bank data:                           
		 p_gt0_gbt_tx_data_lg_in 		=> s_gt0_gbt_tx_data_lg_i,	  
		 p_gt0_gbt_tx_data_hg_in 		=> s_gt0_gbt_tx_data_hg_i,
		 p_gt1_gbt_tx_data_lg_in 		=> s_gt1_gbt_tx_data_lg_i,	  
         p_gt1_gbt_tx_data_hg_in        => s_gt1_gbt_tx_data_hg_i,
 
         --------------------------------------------                          
         -- latency measurement:                     
         p_tx_frameclk_out 				=> s_tx_frameclk_from_gbt,        
         p_tx_wordclk_out 				=> s_tx_wordclk_from_gbt,
         p_gty_txready_out          => s_gty_txready,
         p_qpll1lock_out            => s_qpll1lock,
         
         p_gty_tx_control_in => s_gty_tx_control
         
);


--configbus interface
    p_configbus_clk_net_out<=s_configbus_clk_net;
    i_db6_configbus_interface : db6_configbus_interface
        port map (
               --p_db_side_in             => p_db_side_in,
               p_master_reset_in        =>  s_cfgbus_reset,
               p_clknet_in              => s_clknet,
               p_configbus_clk_net_out  => s_configbus_clk_net, 
               p_configbus_local_in     => p_configbus_local_in,
               p_configbus_remote_in    =>  p_configbus_remote_in,    
               --p_gbtx_rxready_in        =>  p_db_rxready_in,
               --p_system_gbtx_rxready_in => s_system_gbtx_rxready,
               p_bcr_out                => open,   
               p_db_config_reg_out      => s_db_config_registers,
               p_orbit_strobe_out       => s_orbit_strobe,
               p_configbus_ready_out    => s_configbus_ready,
               p_leds_out => p_leds_configbus_out
);  
        
--gbtx interface
    i_db6_gbtx_i2c_interface : db6_gbtx_i2c_interface
          port map (       
				p_clk100_in => s_clknet.clk100,
				p_db_side => p_db_side_in,
                p_gbtx_db_side => p_gbtx_db_side,
                p_read_write_operation_in => p_gbtx_read_write_operation_in,
                p_trigger_i2c_operation_in => p_gbtx_trigger_i2c_operation_in,
                p_busy_out => p_gbtx_busy_out,
                p_wipe_gbtx_registers=> p_wipe_gbtx_registers,
                p_gbtx_default_configuration=> p_gbtx_default_configuration,
                p_user_address_in => p_gbtx_user_address_in,
                p_user_data_in => p_gbtx_user_data_in,
                p_user_data_out=>p_gbtx_user_data_out,
                p_scl_inout  => p_gbtx_scl_inout,
                p_sda_inout  => p_gbtx_sda_inout,
                p_leds_out => p_leds_gbtx_out
);

--serial_id_interface
  i_db6_serial_id_i2c_interface : db6_serial_id_i2c_interface
    port map (
        p_clk100_in => s_clknet.clk100,
        p_trigger_i2c_operation => p_serial_id_trigger_i2c_operation,
        p_busy_out => p_serial_id_busy_out,
        p_serial_id_scl_inout => p_serial_id_scl_inout,
        p_serial_id_sda_inout => p_serial_id_sda_inout,
        p_serial_id_data_out => open
        );
        

end rtl;