----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes
--           Sam Silverstein
-- 
-- Create Date: 09/04/2018 08:40:04 PM
-- Design Name: 
-- Module Name: db6_gbt_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: gbt module for tilecal db rev 5
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Custom libraries and packages:
library gbt;
use gbt.gbt_bank_package.all;
use gbt.vendor_specific_gbt_bank_package.all;
use gbt.gbt_banks_user_setup.all;

library tilecal;
use tilecal.db6_design_package.all;
use tilecal.tile_link_crc.all;

entity db6_gbt_wrapper is
	port (       
  -- general reset:
  p_master_reset_in                             : in  std_logic;
                                              
  -- manual resets:                                                       
  p_gt0_reset_in                           : in  std_logic;
  p_gt1_reset_in                           : in  std_logic;
                  
  -- gt0 link gbt interface port --
  p_gt0_gbt_tx_data_lg_in                                     : in  std_logic_vector (115 downto 0);
  p_gt0_gbt_tx_data_hg_in                                     : in  std_logic_vector (115 downto 0);
    -- gt01link gbt interface port --
  p_gt1_gbt_tx_data_lg_in                                     : in  std_logic_vector (115 downto 0);
  p_gt1_gbt_tx_data_hg_in                                     : in  std_logic_vector (115 downto 0);                      
  -- general control --                       

  p_gt0_mgt_ready_out                                 : out std_logic;
  p_gt1_mgt_ready_out                                 : out std_logic;
                                           
  p_tx_frameclk_out                               : out std_logic;
  p_tx_wordclk_out                                : out std_logic
   
);
end db6_gbt_wrapper;

architecture Behavioral of db6_gbt_wrapper is

   --=========--
   -- tx data --
   --=========--   
   
   signal s_gtytxn, s_gtytxp                    : std_logic_vector(1 downto 0);
   
   signal s_gt0_gbt_tx_data                   : std_logic_vector(115 downto 0);
   signal s_scrambled_tx_data                    : std_logic_vector(115 downto 0);
   signal s_tx_frame                         : std_logic_vector(119 downto 0);
   signal s_txheader                         : std_logic_vector( 3 downto 0);
   signal s_tx_frame_240                      : std_logic_vector(239 downto 0);
   signal s_tx_frame_240_d                    : std_logic_vector(239 downto 0);
   signal s_tx_frame0_tmp                     : std_logic_vector(119 downto 0);
  
   signal s_gty_txactive                      : std_logic; -- gty transmitter active
   signal s_gty_txready                      : std_logic; -- gty transmitter active
   signal s_qpll1lock                      : std_logic; -- gty transmitter active
   signal s_gty_usrclk                        : std_logic; -- 120 mhz
   signal s_gty_usrclk2                       : std_logic; -- 120 mhz
                                                  
   signal s_txgearbox_out_0                    : std_logic_vector(79 downto 0);
   signal s_txgearbox_out_1                    : std_logic_vector(79 downto 0);
   signal s_gty_inputbus                      : std_logic_vector(159 downto 0);

   signal s_reset_scrambler                   : std_logic := '0';
   signal s_reset_encoder                     : std_logic := '0';
   signal s_reset_gearbox                     : std_logic := '0';

   signal s_gearbox_aligned                   : std_logic := '0';
   signal s_gearbox_aligned_last              : std_logic := '0';
   signal s_gearbox_ready                     : std_logic :='0';

   --========================--                   
   -- gbt bank clocks scheme --                   
   --========================--                   
 
   signal s_txframeclk_from_txpll                   	: std_logic;   
   signal s_txframeclk_from_rxpll                   	: std_logic;   
   signal s_txwordclk_from_txpll                   	: std_logic;   
   
   signal s_mgt_refclk_i                              : std_logic_vector(1 downto 0);

   signal s_gain_select_d, s_gain_select : std_logic := '0';  -- 1 for lg, 0 for hg

   signal s_qpll1lock_out : std_logic;
   signal s_qpllrefclksel : std_logic_vector(2 downto 0);



begin


end Behavioral;
