--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Eduardo Valdes eduardo.valdes@cern.ch, eduardo.valdes@fysik.su.se, pirovaldes@gmail.com
-- Engineer:              Sam Silverstein silver@fysik.su.se
--                                                                                                 
-- Project Name:          piro_db_serial_i2c_interface                                                                
-- Module Name:           GBT top                                        
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         Xilinx Kintex Ultrascale+                                                        
-- Tool version:          Vivado 2017.3                                                               
--                                                                                                   
-- Version:               1.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            			DESCRIPTION
--
--                        10/04/2018   1.0       Eduardo Valdes Santurio   	Firmware for the controlling the serial id chip DS28CM00
--                                                                          https://datasheets.maximintegrated.com/en/ds/DS28CM00.pdf)
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

--https://datasheets.maximintegrated.com/en/ds/DS28CM00.pdf)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity db6_serial_id_i2c_interface is
  Port ( 
  
  p_clk100_in : in std_logic;
  p_trigger_i2c_operation : in std_logic;
  p_busy_out : out std_logic;
  p_serial_id_scl_inout : inout std_logic;
  p_serial_id_sda_inout : inout std_logic;
  p_serial_id_data_out :out std_logic_vector(63 downto 0);
  p_leds_out :out  std_logic_vector(3 downto 0)
  
  );
end db6_serial_id_i2c_interface;

architecture Behavioral of db6_serial_id_i2c_interface is

COMPONENT db6_i2c_master_driver IS
GENERIC(
	input_clk : INTEGER := 10000000;   --input clock speed from user logic in 10*KHz
	bus_clk   : INTEGER := 10000);   --speed the i2c bus (scl) will run at in 10*KHz
PORT(
	clk       : IN     STD_LOGIC;                    --system clock
	p_db_side : in     std_logic_vector(1 downto 0); -- db side
    p_verify_bus :in   std_logic; -- verify if the bus is being used
	reset_n   : IN     STD_LOGIC;                    --active low reset
	ena       : IN     STD_LOGIC;                    --latch in command
	addr      : IN     STD_LOGIC_VECTOR(6 DOWNTO 0); --address of target slave
	rw        : IN     STD_LOGIC;                    --'0' is write, '1' is read
	data_wr   : IN     STD_LOGIC_VECTOR(7 DOWNTO 0); --data to write to slave
	busy      : OUT    STD_LOGIC;                    --indicates transaction in progress
	data_rd   : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0); --data read from slave
	ack_error : INOUT STD_LOGIC;                    --flag if improper acknowledge from slave
	sda       : INOUT  STD_LOGIC;                    --serial data output of i2c bus
	scl       : INOUT  STD_LOGIC;                   --serial clock output of i2c bus
	--test0     : OUT  STD_LOGIC;
	--test1     : OUT  STD_LOGIC;
	sda_mon   : out std_logic;
	scl_mon   : out std_logic
	 );	 
end component;

--debug
signal s_vio_trigger_operation : std_logic_vector(0 downto 0):= "0";
--COMPONENT vio_serial_id_debug
--  PORT (
--    clk : IN STD_LOGIC;
--    probe_in0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in4 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in5 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in6 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in7 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_in8 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
--  );
--END COMPONENT;



  --still havent figured out the right way
  -- polynomial: x^8 + x^5 + x^4 + 1
  -- data width: 56
  -- convention: the first serial bit is D[55]
  
  function f_calculate_crc_DS28CM00
    (Data: std_logic_vector(55 downto 0);
     crc:  std_logic_vector(7 downto 0))
    return std_logic_vector is

    variable d:      std_logic_vector(55 downto 0);
    variable c:      std_logic_vector(7 downto 0);
    variable newcrc: std_logic_vector(7 downto 0);

  begin
    d := Data;
    c := crc;

    newcrc(0) := d(53) xor d(52) xor d(51) xor d(50) xor d(49) xor d(48) xor d(45) xor d(44) xor d(42) xor d(40) xor d(39) xor d(38) xor d(34) xor d(33) xor d(32) xor d(31) xor d(25) xor d(24) xor d(23) xor d(21) xor d(18) xor d(15) xor d(14) xor d(11) xor d(10) xor d(9) xor d(6) xor d(4) xor d(3) xor d(0) xor c(0) xor c(1) xor c(2) xor c(3) xor c(4) xor c(5);
    newcrc(1) := d(54) xor d(53) xor d(52) xor d(51) xor d(50) xor d(49) xor d(46) xor d(45) xor d(43) xor d(41) xor d(40) xor d(39) xor d(35) xor d(34) xor d(33) xor d(32) xor d(26) xor d(25) xor d(24) xor d(22) xor d(19) xor d(16) xor d(15) xor d(12) xor d(11) xor d(10) xor d(7) xor d(5) xor d(4) xor d(1) xor c(1) xor c(2) xor c(3) xor c(4) xor c(5) xor c(6);
    newcrc(2) := d(55) xor d(54) xor d(53) xor d(52) xor d(51) xor d(50) xor d(47) xor d(46) xor d(44) xor d(42) xor d(41) xor d(40) xor d(36) xor d(35) xor d(34) xor d(33) xor d(27) xor d(26) xor d(25) xor d(23) xor d(20) xor d(17) xor d(16) xor d(13) xor d(12) xor d(11) xor d(8) xor d(6) xor d(5) xor d(2) xor c(2) xor c(3) xor c(4) xor c(5) xor c(6) xor c(7);
    newcrc(3) := d(55) xor d(54) xor d(53) xor d(52) xor d(51) xor d(48) xor d(47) xor d(45) xor d(43) xor d(42) xor d(41) xor d(37) xor d(36) xor d(35) xor d(34) xor d(28) xor d(27) xor d(26) xor d(24) xor d(21) xor d(18) xor d(17) xor d(14) xor d(13) xor d(12) xor d(9) xor d(7) xor d(6) xor d(3) xor c(0) xor c(3) xor c(4) xor c(5) xor c(6) xor c(7);
    newcrc(4) := d(55) xor d(54) xor d(51) xor d(50) xor d(46) xor d(45) xor d(43) xor d(40) xor d(39) xor d(37) xor d(36) xor d(35) xor d(34) xor d(33) xor d(32) xor d(31) xor d(29) xor d(28) xor d(27) xor d(24) xor d(23) xor d(22) xor d(21) xor d(19) xor d(13) xor d(11) xor d(9) xor d(8) xor d(7) xor d(6) xor d(3) xor d(0) xor c(2) xor c(3) xor c(6) xor c(7);
    newcrc(5) := d(55) xor d(53) xor d(50) xor d(49) xor d(48) xor d(47) xor d(46) xor d(45) xor d(42) xor d(41) xor d(39) xor d(37) xor d(36) xor d(35) xor d(31) xor d(30) xor d(29) xor d(28) xor d(22) xor d(21) xor d(20) xor d(18) xor d(15) xor d(12) xor d(11) xor d(8) xor d(7) xor d(6) xor d(3) xor d(1) xor d(0) xor c(0) xor c(1) xor c(2) xor c(5) xor c(7);
    newcrc(6) := d(54) xor d(51) xor d(50) xor d(49) xor d(48) xor d(47) xor d(46) xor d(43) xor d(42) xor d(40) xor d(38) xor d(37) xor d(36) xor d(32) xor d(31) xor d(30) xor d(29) xor d(23) xor d(22) xor d(21) xor d(19) xor d(16) xor d(13) xor d(12) xor d(9) xor d(8) xor d(7) xor d(4) xor d(2) xor d(1) xor c(0) xor c(1) xor c(2) xor c(3) xor c(6);
    newcrc(7) := d(55) xor d(52) xor d(51) xor d(50) xor d(49) xor d(48) xor d(47) xor d(44) xor d(43) xor d(41) xor d(39) xor d(38) xor d(37) xor d(33) xor d(32) xor d(31) xor d(30) xor d(24) xor d(23) xor d(22) xor d(20) xor d(17) xor d(14) xor d(13) xor d(10) xor d(9) xor d(8) xor d(5) xor d(3) xor d(2) xor c(0) xor c(1) xor c(2) xor c(3) xor c(4) xor c(7);
    return newcrc;
  end f_calculate_crc_DS28CM00;

signal s_calculated_crc_DS28CM00 : std_logic_vector(7 downto 0);

signal s_reset_n : std_logic := '1';
signal s_ena, s_rw, s_ack_error : std_logic;
signal s_slave_address : std_logic := '1';

constant s_DS28CM00_addr: std_logic_vector (6 downto 0):= "1010000";
signal s_addr: std_logic_vector (6 downto 0):= "1010000";
signal data, s_data_rd, s_data_wr, data_to_write: std_logic_vector(7 downto 0);

signal s_serial_id_array : t_serial_id_array := (others=> x"00");



--to setup package
--type t_serial_id_array is array (0 to 8) of std_logic_vector(7 downto 0);
--signal s_serial_id_array : t_serial_id_array := (others=> x"00");

type t_i2c_general_state IS ( st_idle, st_busy, st_stop, st_set_address, st_trigger_register_operation, st_debounce_trigger);
signal sm_i2c : t_i2c_general_state  := st_idle;
signal s_i2c_busy_rising_edge, s_i2c_busy_falling_edge : std_logic := '0';
signal s_previous_busy, s_busy : std_logic:= '0';

signal s_trigger_i2c_operation : std_logic := '0';
signal s_i2c_read_write_operation : std_logic := '1';

begin

-----------------------------------------------------
-- State machine to configure local or remote GBTx --
-----------------------------------------------------

i2c_transaction : process(p_clk100_in)
    
    variable v_start_register :integer := 0; 
    variable v_register_index :integer := 0; 
    variable v_end_register : integer := 8;
    variable wait_for_i2c : std_logic := '0';
    
begin

    p_serial_id_data_out <= s_serial_id_array(0) & s_serial_id_array(1) & s_serial_id_array(2) & s_serial_id_array(3) & s_serial_id_array(4) & s_serial_id_array(5) & s_serial_id_array(6) & s_serial_id_array(7);

    if rising_edge (p_clk100_in) then
      s_previous_busy <= s_busy;                       --capture the value of the previous i2c busy signal
      IF(s_previous_busy = '0' AND s_busy = '1') THEN  --i2c busy just went high
        s_i2c_busy_rising_edge <= '1';                       -- ready to execute next operation
      ELSE
        s_i2c_busy_rising_edge <= '0';                       --wait until operation is finished
      END IF;
 
      IF(s_previous_busy = '1' AND s_busy = '0') THEN  --i2c busy just went high
        s_i2c_busy_falling_edge <= '1';                       -- ready to execute next operation
      ELSE
        s_i2c_busy_falling_edge  <= '0';                       --wait until operation is finished
      END IF;
      
      CASE sm_i2c IS
      -- I2C controller is idle.  release control of bus, and wait for init
        when st_idle =>
        
                p_busy_out <= '0';
                s_ena <= '0';
                v_register_index := 0;
                --v_start_register := to_integer(unsigned(s_starting_register_address));
                --v_end_register := to_integer(unsigned(s_ending_register_address));
                
                if s_i2c_read_write_operation = '0' then
                    v_start_register:=8;
                    v_end_register:=8;
                else
                    v_start_register:=0;
                    v_end_register:=7;
                end if;
                
                p_leds_out <= s_serial_id_array(0)(3 downto 0);
                
                if s_trigger_i2c_operation = '1' then
                  if s_reset_n = '1' then 
                      sm_i2c <= st_debounce_trigger;
                  end if;
                else
                  sm_i2c <= st_idle;
                end if;
        
        when st_debounce_trigger =>
            p_busy_out <= '1';
            if s_trigger_i2c_operation = '0' then
                sm_i2c<=st_set_address;
            end if;
            
            
        WHEN st_set_address =>
              
                
              p_leds_out <= "1111";
              p_busy_out <= '1';
              s_ena <= '1';                            
              s_addr <= s_DS28CM00_addr;                    
              s_rw <= '0';                -- Write operation  
              s_data_wr <= "00000000";    -- Set address pointer
              if s_i2c_busy_rising_edge = '1' then 
                sm_i2c <= st_trigger_register_operation;
              end if;

              
        WHEN st_trigger_register_operation =>
              p_leds_out <= "1111";
              p_busy_out <= '1';
              s_addr <= s_DS28CM00_addr; 
              s_rw <= s_i2c_read_write_operation; --'0';

              
              if s_i2c_read_write_operation = '0' then
                s_data_wr <= s_serial_id_array(v_register_index); -- Transmit 8 bits of register data
              else
                if s_i2c_busy_falling_edge = '1' then
                    s_serial_id_array(v_register_index-1) <=s_data_rd; --read register              
                end if;
              end if;
              
              if s_i2c_busy_rising_edge = '1' then
              
                  v_register_index := v_register_index + 1;
                  if v_register_index > v_end_register+1 then -- Writing the final word....
                    s_ena <= '0';
                    sm_i2c <= st_stop;  
                  else
                    s_ena <= '1';
                    sm_i2c <= st_trigger_register_operation;
                  end if;                                            
                  
              end if;

        WHEN st_stop => -- Wait for I2C master to finish the last operation
              p_leds_out <= "1111";
              p_busy_out <= '1';
              s_ena <= '0';
              if s_i2c_busy_falling_edge = '1' then
                sm_i2c <= st_idle;
              end if;

        WHEN OTHERS =>
            sm_i2c <= st_idle;
            
        END CASE; 
     end if; -- clock edge
end process; -- i2c_control

i_db6_i2c_master_driver : db6_i2c_master_driver
GENERIC MAP(
	input_clk => 10000,   --input clock speed from user logic in 10*KHz
	bus_clk => 1)     --speed the i2c bus (scl) will run at in 10*KHz
PORT MAP(
	clk        => p_clk100_in, --s_i2c_clk,         --system clock
	p_db_side  => "00",
    p_verify_bus => '0', -- verify if the bus is being used
	reset_n    => s_reset_n,     --active low reset
	ena        => s_ena,         --latch in command
	addr       => s_addr,        --address of target slave
	rw         => s_rw,          --'0' is write, '1' is read
	data_wr    => s_data_wr,     --data to write to slave
	busy       => s_busy,        --indicates transaction in progress
	data_rd    => s_data_rd, --data_rd,     --data read from slave
	ack_error  => s_ack_error,   --flag if improper acknowledge from slave
	sda        => p_serial_id_sda_inout,         --serial data output of i2c bus
	scl        => p_serial_id_scl_inout,
	sda_mon    => open,--s_sda_ila(0),
	scl_mon    => open--s_scl_ila(0)
	);	 
	


s_trigger_i2c_operation <= p_trigger_i2c_operation;
--s_trigger_i2c_operation <= s_vio_trigger_operation(0);

--i_vio_serial_id_debug : vio_serial_id_debug
--  PORT MAP (
--    clk => p_clk100_in,
--    probe_in0 => f_reverse_std_logic_vector(s_serial_id_array(0)),
--    probe_in1 => f_reverse_std_logic_vector(s_serial_id_array(1)),
--    probe_in2 => f_reverse_std_logic_vector(s_serial_id_array(2)),
--    probe_in3 => f_reverse_std_logic_vector(s_serial_id_array(3)),
--    probe_in4 => f_reverse_std_logic_vector(s_serial_id_array(4)),
--    probe_in5 => f_reverse_std_logic_vector(s_serial_id_array(5)),
--    probe_in6 => f_reverse_std_logic_vector(s_serial_id_array(6)),
--    probe_in7 => f_reverse_std_logic_vector(s_serial_id_array(7)),
--    probe_in8 => s_calculated_crc_DS28CM00,
--    probe_out0 => s_vio_trigger_operation
--  );

--s_calculated_crc_DS28CM00 <= f_calculate_crc_DS28CM00(f_reverse_std_logic_vector(s_serial_id_array(6))&f_reverse_std_logic_vector(s_serial_id_array(5))&f_reverse_std_logic_vector(s_serial_id_array(4))&f_reverse_std_logic_vector(s_serial_id_array(3))&f_reverse_std_logic_vector(s_serial_id_array(2))&f_reverse_std_logic_vector(s_serial_id_array(1))&f_reverse_std_logic_vector(s_serial_id_array(0)),x"FF");


end Behavioral;
