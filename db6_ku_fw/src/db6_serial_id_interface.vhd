--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Eduardo Valdes eduardo.valdes@cern.ch, eduardo.valdes@fysik.su.se, pirovaldes@gmail.com
-- Engineer:              Sam Silverstein silver@fysik.su.se
--                                                                                                 
-- Project Name:          piro_db_serial_i2c_interface                                                                
-- Module Name:           GBT top                                        
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         Xilinx Kintex Ultrascale+                                                        
-- Tool version:          Vivado 2019.2                                                               
--                                                                                                   
-- Version:               1.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            			DESCRIPTION
--
--                        10/04/2020   1.0       Eduardo Valdes Santurio   	Firmware for the controlling the serial id chip DS28CM00
--                                                                          https://datasheets.maximintegrated.com/en/ds/DS28CM00.pdf)
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

--https://datasheets.maximintegrated.com/en/ds/DS28CM00.pdf)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library tilecal;
use tilecal.db6_design_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity db6_serial_id_interface is
  Port ( 
    p_master_reset_in : in std_logic;
    p_clknet_in : in t_db_clknet;
    p_db_reg_rx_in : in t_db_reg_rx;
    
    p_scl_inout 	: inout std_logic;
    p_sda_inout    : inout std_logic;
    
    p_serial_id_interface_out : out t_serial_id_interface;
    
    p_leds_out :out  std_logic_vector(3 downto 0)
  
  );
end db6_serial_id_interface;

architecture Behavioral of db6_serial_id_interface is
    attribute IOB: string;
    attribute keep: string;
    attribute dont_touch: string;
    
  --still havent figured out the right way
  -- polynomial: x^8 + x^5 + x^4 + 1
  -- data width: 56
  -- convention: the first serial bit is D[55]
  
  function f_calculate_crc_ds28cm00
    (data: std_logic_vector(55 downto 0);
     crc:  std_logic_vector(7 downto 0))
    return std_logic_vector is

    variable d:      std_logic_vector(55 downto 0);
    variable c:      std_logic_vector(7 downto 0);
    variable newcrc: std_logic_vector(7 downto 0);

  begin
    d := data;
    c := crc;

    newcrc(0) := d(53) xor d(52) xor d(51) xor d(50) xor d(49) xor d(48) xor d(45) xor d(44) xor d(42) xor d(40) xor d(39) xor d(38) xor d(34) xor d(33) xor d(32) xor d(31) xor d(25) xor d(24) xor d(23) xor d(21) xor d(18) xor d(15) xor d(14) xor d(11) xor d(10) xor d(9) xor d(6) xor d(4) xor d(3) xor d(0) xor c(0) xor c(1) xor c(2) xor c(3) xor c(4) xor c(5);
    newcrc(1) := d(54) xor d(53) xor d(52) xor d(51) xor d(50) xor d(49) xor d(46) xor d(45) xor d(43) xor d(41) xor d(40) xor d(39) xor d(35) xor d(34) xor d(33) xor d(32) xor d(26) xor d(25) xor d(24) xor d(22) xor d(19) xor d(16) xor d(15) xor d(12) xor d(11) xor d(10) xor d(7) xor d(5) xor d(4) xor d(1) xor c(1) xor c(2) xor c(3) xor c(4) xor c(5) xor c(6);
    newcrc(2) := d(55) xor d(54) xor d(53) xor d(52) xor d(51) xor d(50) xor d(47) xor d(46) xor d(44) xor d(42) xor d(41) xor d(40) xor d(36) xor d(35) xor d(34) xor d(33) xor d(27) xor d(26) xor d(25) xor d(23) xor d(20) xor d(17) xor d(16) xor d(13) xor d(12) xor d(11) xor d(8) xor d(6) xor d(5) xor d(2) xor c(2) xor c(3) xor c(4) xor c(5) xor c(6) xor c(7);
    newcrc(3) := d(55) xor d(54) xor d(53) xor d(52) xor d(51) xor d(48) xor d(47) xor d(45) xor d(43) xor d(42) xor d(41) xor d(37) xor d(36) xor d(35) xor d(34) xor d(28) xor d(27) xor d(26) xor d(24) xor d(21) xor d(18) xor d(17) xor d(14) xor d(13) xor d(12) xor d(9) xor d(7) xor d(6) xor d(3) xor c(0) xor c(3) xor c(4) xor c(5) xor c(6) xor c(7);
    newcrc(4) := d(55) xor d(54) xor d(51) xor d(50) xor d(46) xor d(45) xor d(43) xor d(40) xor d(39) xor d(37) xor d(36) xor d(35) xor d(34) xor d(33) xor d(32) xor d(31) xor d(29) xor d(28) xor d(27) xor d(24) xor d(23) xor d(22) xor d(21) xor d(19) xor d(13) xor d(11) xor d(9) xor d(8) xor d(7) xor d(6) xor d(3) xor d(0) xor c(2) xor c(3) xor c(6) xor c(7);
    newcrc(5) := d(55) xor d(53) xor d(50) xor d(49) xor d(48) xor d(47) xor d(46) xor d(45) xor d(42) xor d(41) xor d(39) xor d(37) xor d(36) xor d(35) xor d(31) xor d(30) xor d(29) xor d(28) xor d(22) xor d(21) xor d(20) xor d(18) xor d(15) xor d(12) xor d(11) xor d(8) xor d(7) xor d(6) xor d(3) xor d(1) xor d(0) xor c(0) xor c(1) xor c(2) xor c(5) xor c(7);
    newcrc(6) := d(54) xor d(51) xor d(50) xor d(49) xor d(48) xor d(47) xor d(46) xor d(43) xor d(42) xor d(40) xor d(38) xor d(37) xor d(36) xor d(32) xor d(31) xor d(30) xor d(29) xor d(23) xor d(22) xor d(21) xor d(19) xor d(16) xor d(13) xor d(12) xor d(9) xor d(8) xor d(7) xor d(4) xor d(2) xor d(1) xor c(0) xor c(1) xor c(2) xor c(3) xor c(6);
    newcrc(7) := d(55) xor d(52) xor d(51) xor d(50) xor d(49) xor d(48) xor d(47) xor d(44) xor d(43) xor d(41) xor d(39) xor d(38) xor d(37) xor d(33) xor d(32) xor d(31) xor d(30) xor d(24) xor d(23) xor d(22) xor d(20) xor d(17) xor d(14) xor d(13) xor d(10) xor d(9) xor d(8) xor d(5) xor d(3) xor d(2) xor c(0) xor c(1) xor c(2) xor c(3) xor c(4) xor c(7);
    return newcrc;
  end f_calculate_crc_ds28cm00;

constant c_ds28cm00_addr: std_logic_vector (6 downto 0):= "1010000";

signal s_i2c : t_i2c;
signal s_serial_id_interface : t_serial_id_interface;
signal s_leds_out : std_logic_vector(3 downto 0);


attribute keep of s_serial_id_interface : signal is "TRUE";
attribute dont_touch of s_serial_id_interface : signal is "TRUE";


begin

-----------------------------------------------------
-- State machine to configure local or remote GBTx --
-----------------------------------------------------

p_serial_id_interface_out <= s_serial_id_interface;
p_leds_out <= s_leds_out;

i_db6_i2c_master : entity tilecal.db6_i2c_master
  port map(
    p_clknet_in => p_clknet_in,                    --system clock
    p_master_reset_in => s_i2c.reset,                    --active low reset
    p_ena_in => s_i2c.ena_in,                    --latch in command
    p_addr_in => s_i2c.addr_in, --address of target slave
    p_rw_in => s_i2c.rw_in,                    --'0' is write, '1' is read
    p_data_in => s_i2c.data_in, --data to write to slave
    p_busy_out => s_i2c.busy_out,                    --indicates transaction in progress
    p_data_out => s_i2c.data_out, --data read from slave
    p_ack_error_buffer => s_i2c.ack_error,                    --flag if improper acknowledge from slave
    p_sda_inout => p_sda_inout, --serial data output of i2c bus
    p_scl_inout => p_scl_inout                   --serial clock output of i2c bus
);


proc_i2c_transaction : process(p_clknet_in.clk40)

    attribute IOB: string;
    attribute keep: string;
    attribute dont_touch: string;
    
    type t_main_sm is (st_initialize, st_idle, st_busy, st_stop, st_wait_for_bus_free, st_set_reset, st_set_address, st_trigger_register_operation, st_debounce_trigger);
    variable v_main_sm : t_main_sm := st_initialize;
        
    constant c_start_register :integer := 0; 
    variable v_register_index :integer := 0; 
    constant c_end_register : integer := 8;
    
    variable v_i2c_operation : std_logic := '0';
    variable v_i2c_busy : std_logic;
    variable v_serial_id_array : t_serial_id_array := (others=> x"00");
    
    
    
begin

    if rising_edge (p_clknet_in.clk40) then
        
        

        s_serial_id_interface.serial_number <= v_serial_id_array(1) & v_serial_id_array(2) & v_serial_id_array(3) & v_serial_id_array(4) & v_serial_id_array(5) & v_serial_id_array(6);
        s_serial_id_interface.family_code<=v_serial_id_array(0);

        s_serial_id_interface.crc_read <= v_serial_id_array(7);
        s_serial_id_interface.crc_calculated <= f_calculate_crc_ds28cm00(s_serial_id_interface.family_code&s_serial_id_interface.serial_number, s_serial_id_interface.crc_read);        
        if (s_serial_id_interface.crc_calculated = s_serial_id_interface.crc_read) then
            s_serial_id_interface.crc_ok <= '1';
        else
            s_serial_id_interface.crc_ok <= '0';
        end if;
        
        v_i2c_operation := '1';
        s_i2c.rw_in <= v_i2c_operation; --'0';
             
        if p_master_reset_in = '1' then
            v_main_sm:= st_initialize;
        end if;
        
        v_i2c_busy:=s_i2c.busy_out;
                     
        case v_main_sm is


            when st_initialize=>
                s_serial_id_interface.busy <= '1';
                s_i2c.reset <= '1';
                v_register_index := 0; 
                v_main_sm:=st_set_reset;
                s_leds_out(2 downto 0)<="001"; 
                      
            when st_set_reset =>
                s_serial_id_interface.busy <= '1';
                s_i2c.reset <= '0';
                v_main_sm := st_set_address;
                v_register_index := 0;
                if (s_i2c.busy_out = '0') then 
                    v_main_sm := st_trigger_register_operation;
                end if;

                s_leds_out(2 downto 0)<="010";                                  
 
            when st_set_address =>
          --s_verify_bus <='1';--  and s_probe_out0(15); 
                s_serial_id_interface.busy <= '1';  
                s_i2c.reset <= '0';
                
                s_i2c.ena_in <= '1';                            
                s_i2c.addr_in <= c_ds28cm00_addr;                    
                s_i2c.rw_in <= '0';                           -- write operation  
                s_i2c.data_in  <= std_logic_vector(to_unsigned(c_start_register,8));    -- transmit lower 8 bits of starting register address
                
                if (v_i2c_busy = '0') and (s_i2c.busy_out = '1') then 
                    v_main_sm := st_trigger_register_operation;
                end if;

            when st_trigger_register_operation =>
                --s_verify_bus <='1';-- and s_probe_out0(15); 
                s_serial_id_interface.busy <= '1';  
                s_i2c.reset <= '0';
                
                s_i2c.addr_in <= c_ds28cm00_addr; 
                
                
                if v_i2c_operation = '0' then
                    s_i2c.data_in  <= x"00";
                else
                    if (v_i2c_busy = '1') and (s_i2c.busy_out = '0') then
                        v_serial_id_array(v_register_index) := s_i2c.data_out; --read register
                    end if;
                end if;
                
                if (v_i2c_busy = '0') and (s_i2c.busy_out = '1') then
                
                  v_register_index := v_register_index + 1;
                  if v_register_index > c_end_register+1 then -- writing the final word....
                    v_main_sm := st_stop;  
                    s_i2c.ena_in <= '0';
                  else
                    v_main_sm := st_trigger_register_operation;
                    s_i2c.ena_in <= '1';
                  end if;                                            
                  
                end if;
    
                s_leds_out(2 downto 0)<="101";
                
             
    
                s_leds_out(2 downto 0)<="011";
  
            when st_stop => -- wait for i2c master to finish the last operation
                s_serial_id_interface.busy <= '1';
                s_i2c.reset <= '0';
                s_i2c.ena_in <= '0';
                if (v_i2c_busy = '1') and (s_i2c.busy_out = '0') then
                    v_main_sm := st_idle;
                end if;
                s_leds_out(2 downto 0)<="110";
                
            when st_idle =>
                
                
                s_i2c.ena_in <= '0';            
                s_serial_id_interface.busy <= '0';
              
                v_register_index := 0;
                
                s_leds_out <= v_serial_id_array(0)(3 downto 0);
                
            when others =>
                v_main_sm := st_idle;           
        end case;
        
    end if; -- clock edge
end process; -- i2c_control


--s_calculated_crc_DS28CM00 <= f_calculate_crc_DS28CM00(f_reverse_std_logic_vector(s_serial_id_array(6))&f_reverse_std_logic_vector(s_serial_id_array(5))&f_reverse_std_logic_vector(s_serial_id_array(4))&f_reverse_std_logic_vector(s_serial_id_array(3))&f_reverse_std_logic_vector(s_serial_id_array(2))&f_reverse_std_logic_vector(s_serial_id_array(1))&f_reverse_std_logic_vector(s_serial_id_array(0)),x"FF");


end Behavioral;
