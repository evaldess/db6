----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
--           Samuel Silverstein
--           Alberto Valero
-- Create Date: 10/03/2018 05:14:49 PM
-- Design Name: 
-- Module Name: db5_cis_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;

entity db6_cis_interface is
  Port ( 
        p_clknet_in                        : in t_db_clknet;
        p_master_reset_in       : in std_logic;
        p_db_reg_rx_in  : in t_db_reg_rx;
        p_tph_out               : out t_mb_diff_pair;
        p_tpl_out               : out t_mb_diff_pair
  );
  
  
end db6_cis_interface;

architecture Behavioral of db6_cis_interface is


    signal s_cis_enable     : std_logic                     := '0';
    signal s_cis_gain       : std_logic                     := '0';
    signal s_cis_bcid_charge       	: std_logic_vector(11 downto 0) := x"000";
    signal s_cis_bcid_discharge       : std_logic_vector(11 downto 0) := x"000";
    signal s_tph, s_tpl : t_mb_std_logic;
    signal s_bc_number : std_logic_vector(31 downto 0);
begin

	s_cis_enable 			<= p_db_reg_rx_in(cfb_cis_config)(0);
	s_cis_gain 				<= p_db_reg_rx_in(cfb_cis_config)(1);  
	s_cis_bcid_charge 		<= p_db_reg_rx_in(cfb_cis_config)(13 downto 2);  
	s_cis_bcid_discharge 	<= p_db_reg_rx_in(cfb_cis_config)(25 downto 14);
    s_bc_number <= p_db_reg_rx_in(bc_number);

    i_tph_q0_OBUFDS : OBUFDS
    generic map (IOSTANDARD => "DIFF_HSTL_I_18")
        port map (
        O => p_tph_out.q0.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB =>p_tph_out.q0.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_tph.q0 -- 1-bit input: Buffer input
        );

    i_tph_q1_OBUFDS : OBUFDS
    generic map (IOSTANDARD => "DIFF_HSTL_I_18")
        port map (
        O => p_tph_out.q1.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB =>p_tph_out.q1.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_tph.q1 -- 1-bit input: Buffer input
        );


    i_tpl_q0_OBUFDS : OBUFDS
    generic map (IOSTANDARD => "DIFF_HSTL_I_18")
        port map (
        O => p_tpl_out.q0.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_tpl_out.q0.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_tpl.q0 -- 1-bit input: Buffer input
        );


    i_tpl_q1_OBUFDS : OBUFDS
    generic map (IOSTANDARD => "DIFF_HSTL_I_18")
        port map (
        O => p_tpl_out.q1.p, -- 1-bit output: Diff_p output (connect directly to top-level port)
        OB => p_tpl_out.q1.n, -- 1-bit output: Diff_n output (connect directly to top-level port)
        I => s_tpl.q1 -- 1-bit input: Buffer input
        );


proc_cis_driver_q0 : process(p_clknet_in.tp_clk40.q0)
type t_sm_cis is (st_idle , st_charge_lg, st_charge_hg);
variable sm_cis, sm_cis_next   : t_sm_cis ;
begin
	if (rising_edge(p_clknet_in.tp_clk40.q0)) then
	
		if (p_master_reset_in = '1' or s_cis_enable ='0') then
			sm_cis := st_idle;
		else
			sm_cis := sm_cis_next;
			
				case sm_cis is
                    when st_idle => 
                        s_tph.q0     <= '0';
                        s_tpl.q0     <= '0';
                        if (s_bc_number(11 downto 0) = s_cis_bcid_charge and s_cis_enable ='1') then
                            if (s_cis_gain ='0') then
                                sm_cis_next := st_charge_lg;
                            else
                                sm_cis_next := st_charge_hg;
                            end if;
                        else
                            sm_cis_next := st_idle;
                        end if;

                    when st_charge_lg => 
                        s_tph.q0     <= '0';
                        s_tpl.q0     <= '1';
                        if ( s_bc_number(11 downto 0) = s_cis_bcid_discharge) then
                            sm_cis_next := st_idle;
                        else 
                            sm_cis_next := st_charge_lg;
                        end if;

                    when st_charge_hg => 
                        s_tph.q0     <= '1';
                        s_tpl.q0     <= '0';
                        if ( s_bc_number(11 downto 0) = s_cis_bcid_discharge) then
                            sm_cis_next := st_idle;
                        else 
                            sm_cis_next := st_charge_hg;
                        end if;
                    when others=>
                        sm_cis_next := st_idle;
                end case;

			
		end if;
	end if;
end process;

proc_cis_driver_q1 : process(p_clknet_in.tp_clk40.q1)
type t_sm_cis is (st_idle , st_charge_lg, st_charge_hg);
variable sm_cis, sm_cis_next   : t_sm_cis ;
begin
	if (rising_edge(p_clknet_in.tp_clk40.q1)) then
	
		if (p_master_reset_in = '1' or s_cis_enable ='0') then
			sm_cis := st_idle;
		else
			sm_cis := sm_cis_next;
			
				case sm_cis is
                    when st_idle => 
                        s_tph.q1     <= '0';
                        s_tpl.q1     <= '0';
                        if (s_bc_number(11 downto 0) = s_cis_bcid_charge and s_cis_enable ='1') then
                            if (s_cis_gain ='0') then
                                sm_cis_next := st_charge_lg;
                            else
                                sm_cis_next := st_charge_hg;
                            end if;
                        else
                            sm_cis_next := st_idle;
                        end if;

                    when st_charge_lg => 
                        s_tph.q1     <= '0';
                        s_tpl.q1     <= '1';
                        if ( s_bc_number(11 downto 0) = s_cis_bcid_discharge) then
                            sm_cis_next := st_idle;
                        else 
                            sm_cis_next := st_charge_lg;
                        end if;

                    when st_charge_hg => 
                        s_tph.q1     <= '1';
                        s_tpl.q1     <= '0';
                        if ( s_bc_number(11 downto 0) = s_cis_bcid_discharge) then
                            sm_cis_next := st_idle;
                        else 
                            sm_cis_next := st_charge_hg;
                        end if;
                    when others=>
                        sm_cis_next := st_idle;
                end case;

			
		end if;
	end if;
end process;


end Behavioral;
