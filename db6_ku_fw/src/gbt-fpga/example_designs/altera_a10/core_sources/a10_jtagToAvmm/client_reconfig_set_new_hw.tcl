package require -exact qsys 15.1


# module properties
set_module_property NAME {client_reconfig_set}
set_module_property DISPLAY_NAME {Client Reconfiguration Set}

# default module properties
set_module_property VERSION {1.0}
set_module_property GROUP {default group}
set_module_property DESCRIPTION {default description}
set_module_property AUTHOR {author}

set_module_property COMPOSITION_CALLBACK compose
set_module_property opaque_address_map false

add_parameter NCHAN INTEGER 1 "Clients"
set_parameter_property NCHAN DEFAULT_VALUE 1
set_parameter_property NCHAN DISPLAY_NAME "Clients"
set_parameter_property NCHAN TYPE INTEGER
set_parameter_property NCHAN UNITS None
set_parameter_property NCHAN DESCRIPTION "Number of clients in the target system"
set_parameter_property NCHAN HDL_PARAMETER false

proc compose { } {

    set nchan [get_parameter_value NCHAN]

    # Instances and instance parameters
    # (disabled instances are intentionally culled)
 
    add_instance clk_0 clock_source 15.1
    set_instance_parameter_value clk_0 {clockFrequency} {100000000.0}
    set_instance_parameter_value clk_0 {clockFrequencyKnown} {1}
    set_instance_parameter_value clk_0 {resetSynchronousEdges} {NONE}

    add_instance mm_bridge altera_avalon_mm_bridge 15.1
    set_instance_parameter_value mm_bridge {DATA_WIDTH} {32}
    set_instance_parameter_value mm_bridge {SYMBOL_WIDTH} {8}
    set_instance_parameter_value mm_bridge {ADDRESS_WIDTH} {4}
    set_instance_parameter_value mm_bridge {USE_AUTO_ADDRESS_WIDTH} {1}
    set_instance_parameter_value mm_bridge {ADDRESS_UNITS} {WORDS}
    set_instance_parameter_value mm_bridge {MAX_BURST_SIZE} {1}
    set_instance_parameter_value mm_bridge {MAX_PENDING_RESPONSES} {1}
    set_instance_parameter_value mm_bridge {LINEWRAPBURSTS} {0}
    set_instance_parameter_value mm_bridge {PIPELINE_COMMAND} {1}
    set_instance_parameter_value mm_bridge {PIPELINE_RESPONSE} {1}

    for {set i 0} {$i < $nchan} {incr i 1} {

	# add_instance reconfig_mgmt_$i reconfig_mgmt 1.0 
	add_instance gbt_phy_$i reconfig_mgmt 1.0 
	#add_instance gbt_atxpll_$i reconfig_mgmt 1.0  
	add_instance gbt_fpll_$i reconfig_mgmt 1.0 	
    }

    # connections and connection parameters
    add_connection clk_0.clk mm_bridge.clk clock

    add_connection clk_0.clk_reset mm_bridge.reset reset

    set ia 0

    for {set i 0} {$i < $nchan} {incr i 1} {

	# add_connection clk_0.clk reconfig_mgmt_$i.clock clock

	add_connection clk_0.clk gbt_phy_$i.clock clock

	#add_connection clk_0.clk gbt_atxpll_$i.clock clock

	add_connection clk_0.clk gbt_fpll_$i.clock clock

	# add_connection clk_0.clk_reset reconfig_mgmt_$i.reset reset

	add_connection clk_0.clk_reset gbt_phy_$i.reset reset

	#add_connection clk_0.clk_reset gbt_atxpll_$i.reset reset

	add_connection clk_0.clk_reset gbt_fpll_$i.reset reset


	# add_connection mm_bridge.m0 reconfig_mgmt_$i.s0 avalon
	# set_connection_parameter_value mm_bridge.m0/reconfig_mgmt_$i.s0 arbitrationPriority {1}
	# set_connection_parameter_value mm_bridge.m0/reconfig_mgmt_$i.s0 baseAddress $ia
	# set_connection_parameter_value mm_bridge.m0/reconfig_mgmt_$i.s0 defaultConnection {0}

        # set ia [expr $ia + 0x4000]

	add_connection mm_bridge.m0 gbt_phy_$i.s0 avalon
	set_connection_parameter_value mm_bridge.m0/gbt_phy_$i.s0 arbitrationPriority {1}
	set_connection_parameter_value mm_bridge.m0/gbt_phy_$i.s0 baseAddress $ia
	set_connection_parameter_value mm_bridge.m0/gbt_phy_$i.s0 defaultConnection {0}

        set ia [expr $ia + 0x8000]

	#add_connection mm_bridge.m0 gbt_atxpll_$i.s0 avalon
	#set_connection_parameter_value mm_bridge.m0/gbt_atxpll_$i.s0 arbitrationPriority {1}
	#set_connection_parameter_value mm_bridge.m0/gbt_atxpll_$i.s0 baseAddress $ia
	#set_connection_parameter_value mm_bridge.m0/gbt_atxpll_$i.s0 defaultConnection {0}
 
    #     set ia [expr $ia + 0x4000]
 
	add_connection mm_bridge.m0 gbt_fpll_$i.s0 avalon
	set_connection_parameter_value mm_bridge.m0/gbt_fpll_$i.s0 arbitrationPriority {1}
	set_connection_parameter_value mm_bridge.m0/gbt_fpll_$i.s0 baseAddress $ia
	set_connection_parameter_value mm_bridge.m0/gbt_fpll_$i.s0 defaultConnection {0}

        set ia [expr $ia + 0x8000]

    }

    # exported interfaces
    add_interface s0 avalon slave
    set_interface_property s0 EXPORT_OF mm_bridge.s0

    add_interface clk clock sink
    set_interface_property clk EXPORT_OF clk_0.clk_in

    add_interface reset reset sink
    set_interface_property reset EXPORT_OF clk_0.clk_in_reset
    for {set i 0} {$i < $nchan} {incr i 1} {


	add_interface gbt_phy_${i}_s0 conduit end
 	set_interface_property gbt_phy_${i}_s0 EXPORT_OF gbt_phy_$i.s01
	add_interface gbt_phy_${i}_reset conduit end
 	set_interface_property gbt_phy_${i}_reset EXPORT_OF gbt_phy_$i.reset1
	
	#add_interface gbt_atxpll_${i}_s0 conduit end
 	#set_interface_property gbt_atxpll_${i}_s0 EXPORT_OF gbt_atxpll_$i.s01
	#add_interface gbt_atxpll_${i}_reset conduit end
 	#set_interface_property gbt_atxpll_${i}_reset EXPORT_OF gbt_atxpll_$i.reset1	
	
	add_interface gbt_fpll_${i}_s0 conduit end
 	set_interface_property gbt_fpll_${i}_s0 EXPORT_OF gbt_fpll_$i.s01
	add_interface gbt_fpll_${i}_reset conduit end
 	set_interface_property gbt_fpll_${i}_reset EXPORT_OF gbt_fpll_$i.reset1	

    }

    # interconnect requirements
    set_interconnect_requirement {$system} {qsys_mm.clockCrossingAdapter} {HANDSHAKE}
    set_interconnect_requirement {$system} {qsys_mm.maxAdditionalLatency} {1}
    set_interconnect_requirement {$system} {qsys_mm.insertDefaultSlave} {FALSE}
}
