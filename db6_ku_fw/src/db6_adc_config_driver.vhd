--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               Stockholm University                                                        
-- Engineer:              Eduardo Valdes Santurio eduardo.valdes@cern.ch, eduardo.valdes@fysik.su.se
--                                                                                                 
-- Project Name:          Register Config module for the ADC deserializer for LTC2264-12                                                                
-- Module Name:           ADC_top                                        
--                                                                                                 
-- Language:              VHDL                                                                 
--                                                                                                   --
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
library tilecal;
use tilecal.db6_design_package.all;




entity db6_adc_config_driver is
  Port ( 
        p_master_reset_in       : in std_logic;
        p_clknet_in 			: in  t_db_clknet;

        p_adc_register_config_from_readout_in : in t_adc_register_config;
        p_adc_register_config_from_configbus_in   : in t_adc_register_config;
        p_adc_config_done_out   : out std_logic;
        p_fe_data_in           : in std_logic_vector(31 downto 0);
        p_fe_data_out           : out std_logic_vector(31 downto 0);
        p_leds_out              : out std_logic_vector(3 downto 0)        
  
  );
end db6_adc_config_driver;


architecture Behavioral of db6_adc_config_driver is

signal s_fe_data : std_logic_vector(31 downto 0);
signal s_adcs_config_flag : std_logic;

signal s_adc_register_config_buffer : t_adc_register_config := c_adc_register_init_config;
signal s_adc_registers_buffer : t_adc_registers := c_adc_registers_init;
signal s_mb_fpga_buffer : std_logic_vector(2 downto 0) := "100";
signal s_mb_pmt_buffer : std_logic_vector(1 downto 0):= "11";

begin

p_fe_data_out <= s_fe_data;
p_adc_config_done_out <= s_adcs_config_flag;

proc_config_mb_adc : process(p_clknet_in.clk40)
type t_adc_config_sm is (st_reset_adc, st_limbo, st_config_reg1, st_config_reg2,st_config_reg3, st_config_reg4, st_trigger_debounce, st_idle);
--variable v_state	:std_logic_vector(3 downto 0):= x"1";
variable v_adc_config_st, v_next_st : t_adc_config_sm := st_idle;
variable v_counter	: integer :=0;--:unsigned(31 downto 0):= (others=>'0');
variable v_mb_reset   :std_logic := '0';
begin
	if rising_edge(p_clknet_in.clk40) then
                
        if p_master_reset_in = '0' then
        
				case v_adc_config_st is
				    when st_limbo =>
                    s_adcs_config_flag <= '0';
                    s_fe_data(31 downto 24)    <= x"00"; --non used
                    s_fe_data(23 downto 21)     <= "000"; --trigger action: 23 - T, 22 - E, 21 - R
                    s_fe_data(20 downto 18)     <= "000"; --fpga select: 100 - all, 000 - A0, 001 - A1, ...
                    s_fe_data(17 downto 16)     <= "00"; -- Tube select: 00 - tube 1, ... 11 - all tubes 
                    s_fe_data(15 downto 13)     <= "000"; -- command?
                    s_fe_data(12 downto 8)     <= "00000"; -- address
                    s_fe_data(7 downto 0)         <= x"00"; -- data
                    
                    if v_counter < 20000000 then --;x"f" then
                        v_counter := v_counter + 1;
                        v_adc_config_st := st_limbo;
                    else
                        v_counter   :=0;-- := (others=>'0');            
                        v_adc_config_st    := v_next_st;
                    end if;                        
                    when st_reset_adc =>
                        s_adcs_config_flag <= '0';
                        s_fe_data(31 downto 24)    <= x"00"; --non used
                        s_fe_data(23 downto 21)     <= "100"; --trigger action: 23 - T, 22 - E, 21 - R
                        s_fe_data(20 downto 18)     <= s_mb_fpga_buffer;--"100"; --fpga select: 100 - all, 000 - A0, 001 - A1, ...
                        s_fe_data(17 downto 16)     <= s_mb_pmt_buffer;--"11"; -- Tube select: 00 - tube 1, ... 11 - all tubes 
                        s_fe_data(15 downto 13)     <= "100"; -- command?
                        s_fe_data(12 downto 8)     <= "00000"; -- address
                    
                        
                        s_fe_data(7 downto 0)         <= s_adc_registers_buffer(0);--x"00";--s_adc_registers(0);--x"80"; -- data (re0 0 bit 7 resets the adcs)
                        v_next_st:= st_config_reg1;
                        if v_counter < 10000000 then --x"ffff" then
                            v_counter := v_counter + 1;
                            v_adc_config_st :=st_reset_adc;
                        else
                            v_counter := 0;--    := (others=>'0');            
                            v_adc_config_st    := st_limbo;
                        end if;
                        p_leds_out <= "0000";
                        
                    when st_config_reg1 =>
                       s_adcs_config_flag <= '0'; 
                       s_fe_data(31 downto 24)    <= x"00"; --non used
                       s_fe_data(23 downto 21)     <= "100"; --trigger action: 23 - T, 22 - E, 21 - R
                       s_fe_data(20 downto 18)     <= s_mb_fpga_buffer;--"100"; --fpga select: 100 - all, 000 - A0, 001 - A1, ...
                       s_fe_data(17 downto 16)     <= s_mb_pmt_buffer;--"11"; -- Tube select: 00 - tube 1, ... 11 - all tubes 
                       s_fe_data(15 downto 13)     <= "100"; -- command?
                       s_fe_data(12 downto 8)     <= "00001"; -- address
                       s_fe_data(7 downto 0)         <= s_adc_registers_buffer(1); --s_adc_registers(1);--x"00"; -- data (re0 0 bit 7 resets the adcs)
                        v_next_st:= st_config_reg2;
                        if v_counter < 10000000 then --x"ffff" then
                            v_counter := v_counter + 1;
                            v_adc_config_st := st_config_reg1;
                        else
                            v_counter    :=0;--:= (others=>'0');            
                            v_adc_config_st    := st_limbo;
                        end if;
                        
                        p_leds_out <= "0001";
                    when st_config_reg2 =>
                        s_adcs_config_flag <= '0'; 
                        s_fe_data(31 downto 24)    <= x"00"; --non used
                        s_fe_data(23 downto 21)     <= "100"; --trigger action: 23 - T, 22 - E, 21 - R
                        s_fe_data(20 downto 18)     <= s_mb_fpga_buffer;--"100"; --fpga select: 100 - all, 000 - A0, 001 - A1, ...
                        s_fe_data(17 downto 16)     <= s_mb_pmt_buffer;--"11"; -- Tube select: 00 - tube 1, ... 11 - all tubes 
                        s_fe_data(15 downto 13)     <= "100"; -- command?
                        s_fe_data(12 downto 8)     <= "00010"; -- address
                        s_fe_data(7 downto 0)         <= s_adc_registers_buffer(2);--s_adc_registers(2);--x"b5"; -- data (re0 0 bit 7 resets the adcs)
                        v_next_st:= st_config_reg3;
                        if v_counter < 10000000 then --x"ffff" then
                            v_counter := v_counter + 1;
                            v_adc_config_st := st_config_reg2;
                        else
                            v_counter    :=0;--:= (others=>'0');            
                            v_adc_config_st    := st_limbo;
                        end if;
                        p_leds_out <= "0011";
                        
                    when st_config_reg3 => 
                        s_adcs_config_flag <= '0'; 
                        s_fe_data(31 downto 24)    <= x"00";                    
                        s_fe_data(23 downto 21)     <= "100";
                        s_fe_data(20 downto 18)     <= s_mb_fpga_buffer;--"100";
                        s_fe_data(17 downto 16)     <= s_mb_pmt_buffer;--"11";
                        s_fe_data(15 downto 13)     <= "100";
                        s_fe_data(12 downto 8)     <= "00011";
                        s_fe_data(7 downto 0)         <= s_adc_registers_buffer(3);--s_adc_registers(3);-- x"b5";
                        v_next_st:= st_config_reg4;
                        if v_counter < 10000000 then --x"ffff" then
                            v_counter := v_counter + 1;
                            v_adc_config_st := st_config_reg3;
                        else
                            v_counter    := 0; -- (others=>'0');            
                            v_adc_config_st    := st_limbo;
                        end if;
                        p_leds_out <= "0111";
                        
                    when st_config_reg4 => 
                        s_adcs_config_flag <= '0'; 
                        s_fe_data(31 downto 24)    <= x"00";                    
                        s_fe_data(23 downto 21)     <= "100";
                        s_fe_data(20 downto 18)     <= s_mb_fpga_buffer;--"100";
                        s_fe_data(17 downto 16)     <= s_mb_pmt_buffer;--"11";
                        s_fe_data(15 downto 13)     <= "100";
                        s_fe_data(12 downto 8)     <= "00100";
                        s_fe_data(7 downto 0)         <= s_adc_registers_buffer(4);--s_adc_registers(4); --x"b5";
                        v_next_st:= st_trigger_debounce;
                        if v_counter < 10000000 then --x"ffff" then
                            v_counter := v_counter + 1;
                            v_adc_config_st := st_config_reg4;
                        else
                            v_counter    := 0;--(others=>'0');            
                            v_adc_config_st    := st_limbo;
                        end if;
                        p_leds_out <= "1111";
                    
                    when st_trigger_debounce =>
                        s_adcs_config_flag <= '0';
                        if p_adc_register_config_from_readout_in.trigger_mb_adc_config = '0' and p_adc_register_config_from_configbus_in.trigger_mb_adc_config = '0' then
                            v_adc_config_st    := st_idle;
                        end if;
                    
					when st_idle => 
                        
                        s_adcs_config_flag <= '1';
                        v_adc_config_st    := st_idle;
                        
                        if p_adc_register_config_from_readout_in.mode = '0' then
                                
                            s_adc_registers_buffer <= p_adc_register_config_from_readout_in.adc_registers; 

                            s_mb_fpga_buffer <=  p_adc_register_config_from_configbus_in.mb_fpga_select;
                            s_mb_pmt_buffer <=  p_adc_register_config_from_configbus_in.mb_pmt_select;
                              
                           
                            if p_adc_register_config_from_configbus_in.trigger_mb_adc_config = '1' then
                                s_fe_data(31 downto 24)    <= x"00"; --non used
                                s_fe_data(23 downto 21)     <= "000"; --trigger action: 23 - T, 22 - E, 21 - R
                                s_fe_data(20 downto 18)     <= "000"; --fpga select: 100 - all, 000 - A0, 001 - A1, ...
                                s_fe_data(17 downto 16)     <= "00"; -- Tube select: 00 - tube 1, ... 11 - all tubes 
                                s_fe_data(15 downto 13)     <= "000"; -- command?
                                s_fe_data(12 downto 8)     <= "00000"; -- address
                                s_fe_data(7 downto 0)         <= x"00"; -- data
                                v_counter    :=0;--:= (others=>'0');            
                                v_adc_config_st    := st_reset_adc;    
                                s_adcs_config_flag <= '0';
                            else
                                s_fe_data <= p_fe_data_in;
                            end if;

                        else
                                
                            s_mb_fpga_buffer <= p_adc_register_config_from_readout_in.mb_fpga_select;
                            s_mb_pmt_buffer <= p_adc_register_config_from_readout_in.mb_pmt_select;
                            s_adc_registers_buffer <= p_adc_register_config_from_readout_in.adc_registers;
                            
                            if p_adc_register_config_from_readout_in.trigger_mb_adc_config = '1' then
                                s_fe_data(31 downto 24)    <= x"00"; --non used
                                s_fe_data(23 downto 21)     <= "000"; --trigger action: 23 - T, 22 - E, 21 - R
                                s_fe_data(20 downto 18)     <= "000"; --fpga select: 100 - all, 000 - A0, 001 - A1, ...
                                s_fe_data(17 downto 16)     <= "00"; -- Tube select: 00 - tube 1, ... 11 - all tubes 
                                s_fe_data(15 downto 13)     <= "000"; -- command?
                                s_fe_data(12 downto 8)     <= "00000"; -- address
                                s_fe_data(7 downto 0)         <= x"00"; -- data
                                v_counter    :=0;--:= (others=>'0');            
                                v_adc_config_st    := st_reset_adc;    
                                s_adcs_config_flag <= '0';
                            else
                                s_fe_data <= p_fe_data_in;
                            end if;
                 
                        end if;
                        
                        p_leds_out <= "0110";

                        
					when others => 
						s_fe_data(31 downto 24)	<= x"00";					
						s_fe_data(23 downto 21) 	<= "000";
						s_fe_data(20 downto 18) 	<= "000";
						s_fe_data(17 downto 16) 	<= "00";
						s_fe_data(15 downto 13) 	<= "000";
						s_fe_data(12 downto 8) 	<= "00000";
						s_fe_data(7 downto 0) 		<= x"00";
						v_adc_config_st	:= st_reset_adc;
						s_adcs_config_flag <= '1';
				end case;
				
		else
            
            s_fe_data(31 downto 24)    <= x"00"; --non used
            s_fe_data(23 downto 21)     <= "000"; --trigger action: 23 - T, 22 - E, 21 - R
            s_fe_data(20 downto 18)     <= "000"; --fpga select: 100 - all, 000 - A0, 001 - A1, ...
            s_fe_data(17 downto 16)     <= "00"; -- Tube select: 00 - tube 1, ... 11 - all tubes 
            s_fe_data(15 downto 13)     <= "000"; -- command?
            s_fe_data(12 downto 8)     <= "00000"; -- address
            s_fe_data(7 downto 0)         <= x"00"; -- data
            v_counter    :=0;--:= (others=>'0');            
            v_adc_config_st    := st_reset_adc;    
            s_adcs_config_flag <= '0';
            s_mb_fpga_buffer <= "100";
            s_mb_pmt_buffer <= "11";
            s_adc_registers_buffer <= c_adc_registers_init;
		
		end if;
	end if;
end process;

end Behavioral;

