----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Eduardo Valdes Santurio
-- 
-- Create Date: 04/23/2020 04:15:18 PM
-- Design Name: 
-- Module Name: db6_mainboard_interface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

library tilecal;
use tilecal.db6_design_package.all;

entity db6_mainboard_interface is
  Port ( 
        p_master_reset_in : in std_logic;
        p_clknet_in                        : in t_db_clknet;
        p_db_reg_rx_in : in t_db_reg_rx;
        
        --adc readout
        p_adc_bitclk_in : in t_adc_clk_in;
        p_adc_frameclk_in : in t_adc_clk_in;
        p_adc_lg_data_in : in t_adc_data_in;
        p_adc_hg_data_in : in t_adc_data_in;
        
        --mb_driver
        p_ssel_out         : out t_mb_diff_pair;
        p_sclk_out         : out t_mb_diff_pair;
        p_sdata_out     : out t_mb_diff_pair;
        p_sdata_in    : in  t_mb_diff_pair;

        --cis interface
        p_tph_out               : out t_mb_diff_pair;
        p_tpl_out               : out t_mb_diff_pair;
        
        p_mb_interface_out          : out t_mb_interface;
        
        p_leds_out : out std_logic_vector(3 downto 0)
  );
end db6_mainboard_interface;

architecture Behavioral of db6_mainboard_interface is
attribute IOB: string;
attribute keep: string;
attribute dont_touch: string;


--adc readout
signal s_adc_readout_control : t_adc_readout_control:=
(
--        adc_mode	: std_logic;
        db_side              => "1",
        channel_reset       => (others => '0'),
		fc_idelay_ctrl_reset => (others => '0'),
        fc_idelay_load  => (others => '0'),
        fc_idelay_en_vtc => (others => '0'),
        fc_idelay_count => (others =>(others => '0')),

        lg_idelay_ctrl_reset => (others => '0'),
        lg_idelay_load => (others => '0'),
        lg_idelay_en_vtc => (others => '0'),
        lg_idelay_count => (others =>(others => '0')),
        lg_bitslip => (others =>(others => '0')),
        
        hg_idelay_ctrl_reset => (others => '0'),
        hg_idelay_load => (others => '0'),
        hg_idelay_en_vtc => (others => '0'),
        hg_bitslip => (others =>(others => '0')),
        hg_idelay_count => (others =>(others => '0')),
        
        channel_enable_test_pattern => (others=>'0'),
        channel_reset_test_pattern => (others=>'0'),
        channel_lg_data_test_pattern => (others => (others=> '0')),
        channel_hg_data_test_pattern => (others => (others=> '0')),

        channel_lg_pedestal_test_lower => (others => (others=> '0')),
        channel_lg_pedestal_test_higher => (others => (others=> '0')),
        channel_hg_pedestal_test_lower => (others => (others=> '0')),
        channel_hg_pedestal_test_higher => (others => (others=> '0')),
        
        adc_config_done => '0'

);
signal s_adc_readout : t_adc_readout;
attribute keep of s_adc_readout_control, s_adc_readout : signal is "TRUE";
attribute dont_touch of s_adc_readout_control, s_adc_readout : signal is "TRUE";


--mb driver

signal s_mb_driver : t_mb_driver;
signal s_mb_txword_in      :  std_logic_vector (31 downto 0);
--signal s_mb_rxword_out       : t_mb_rxword;
--signal s_mb_done_out       : t_mb_std_logic;
attribute keep of s_mb_txword_in, s_mb_driver : signal is "TRUE";
attribute dont_touch of s_mb_txword_in, s_mb_driver : signal is "TRUE";

--adc_config
signal s_adc_register_config_from_configbus, s_adc_register_config_from_readout : t_adc_register_config := c_adc_register_init_config;

attribute keep of s_adc_register_config_from_configbus, s_adc_register_config_from_readout : signal is "TRUE";

begin

p_mb_interface_out.adc_readout <= s_adc_readout;

i_db6_adc_interface : entity tilecal.db6_adc_interface_fifo
  Port map (
        p_master_reset_in => p_master_reset_in,
        p_clknet_in => p_clknet_in,
        p_adc_bitclk_in => p_adc_bitclk_in,
        p_adc_frameclk_in => p_adc_frameclk_in,
        p_adc_lg_data_in => p_adc_lg_data_in,
        p_adc_hg_data_in => p_adc_hg_data_in,
        p_adc_readout_control_in => s_adc_readout_control,
        p_adc_readout_out => s_adc_readout,
        p_leds_out       => open
  );

        s_adc_readout_control.db_side <= p_clknet_in.db_side;
        
        s_adc_readout_control.fc_idelay_count <= (others => (others=> '0'));
        s_adc_readout_control.fc_idelay_ctrl_reset <= ((others=> '0'));
        s_adc_readout_control.fc_idelay_load<= ((others=> '0'));
        s_adc_readout_control.fc_idelay_en_vtc<= ((others=> '0'));
      
        s_adc_readout_control.lg_idelay_count(0)<= p_db_reg_rx_in(adc_readout_idelay3_lg_0)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(0)<= p_db_reg_rx_in(adc_readout_idelay3_hg_0)(8 downto 0);
        s_adc_readout_control.lg_idelay_count(1)<= p_db_reg_rx_in(adc_readout_idelay3_lg_1)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(1)<= p_db_reg_rx_in(adc_readout_idelay3_hg_1)(8 downto 0);
        s_adc_readout_control.lg_idelay_count(2)<= p_db_reg_rx_in(adc_readout_idelay3_lg_2)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(2)<= p_db_reg_rx_in(adc_readout_idelay3_hg_2)(8 downto 0);
        s_adc_readout_control.lg_idelay_count(3)<= p_db_reg_rx_in(adc_readout_idelay3_lg_3)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(3)<= p_db_reg_rx_in(adc_readout_idelay3_hg_3)(8 downto 0);
        s_adc_readout_control.lg_idelay_count(4)<= p_db_reg_rx_in(adc_readout_idelay3_lg_4)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(4)<= p_db_reg_rx_in(adc_readout_idelay3_hg_4)(8 downto 0);
        s_adc_readout_control.lg_idelay_count(5)<= p_db_reg_rx_in(adc_readout_idelay3_lg_5)(8 downto 0);
        s_adc_readout_control.hg_idelay_count(5)<= p_db_reg_rx_in(adc_readout_idelay3_hg_5)(8 downto 0);
        
        s_adc_readout_control.lg_idelay_ctrl_reset(0)<= p_db_reg_rx_in(adc_readout_idelay3_lg_0)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(0)<= p_db_reg_rx_in(adc_readout_idelay3_hg_0)(15);
        s_adc_readout_control.lg_idelay_ctrl_reset(1)<= p_db_reg_rx_in(adc_readout_idelay3_lg_1)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(1)<= p_db_reg_rx_in(adc_readout_idelay3_hg_1)(15);
        s_adc_readout_control.lg_idelay_ctrl_reset(2)<= p_db_reg_rx_in(adc_readout_idelay3_lg_2)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(2)<= p_db_reg_rx_in(adc_readout_idelay3_hg_2)(15);
        s_adc_readout_control.lg_idelay_ctrl_reset(3)<= p_db_reg_rx_in(adc_readout_idelay3_lg_3)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(3)<= p_db_reg_rx_in(adc_readout_idelay3_hg_3)(15);
        s_adc_readout_control.lg_idelay_ctrl_reset(4)<= p_db_reg_rx_in(adc_readout_idelay3_lg_4)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(4)<= p_db_reg_rx_in(adc_readout_idelay3_hg_4)(15);
        s_adc_readout_control.lg_idelay_ctrl_reset(5)<= p_db_reg_rx_in(adc_readout_idelay3_lg_5)(15);
        s_adc_readout_control.hg_idelay_ctrl_reset(5)<= p_db_reg_rx_in(adc_readout_idelay3_hg_5)(15);        
        
        s_adc_readout_control.lg_idelay_load(0)<=p_db_reg_rx_in(adc_readout_idelay3_lg_0)(9);
        s_adc_readout_control.hg_idelay_load(0)<=p_db_reg_rx_in(adc_readout_idelay3_hg_0)(9);
        s_adc_readout_control.lg_idelay_load(1)<=p_db_reg_rx_in(adc_readout_idelay3_lg_1)(9);
        s_adc_readout_control.hg_idelay_load(1)<=p_db_reg_rx_in(adc_readout_idelay3_hg_1)(9);
        s_adc_readout_control.lg_idelay_load(2)<=p_db_reg_rx_in(adc_readout_idelay3_lg_2)(9);
        s_adc_readout_control.hg_idelay_load(2)<=p_db_reg_rx_in(adc_readout_idelay3_hg_2)(9);
        s_adc_readout_control.lg_idelay_load(3)<=p_db_reg_rx_in(adc_readout_idelay3_lg_3)(9);
        s_adc_readout_control.hg_idelay_load(3)<=p_db_reg_rx_in(adc_readout_idelay3_hg_3)(9);
        s_adc_readout_control.lg_idelay_load(4)<=p_db_reg_rx_in(adc_readout_idelay3_lg_4)(9);
        s_adc_readout_control.hg_idelay_load(4)<=p_db_reg_rx_in(adc_readout_idelay3_hg_4)(9);
        s_adc_readout_control.lg_idelay_load(5)<=p_db_reg_rx_in(adc_readout_idelay3_lg_5)(9);
        s_adc_readout_control.hg_idelay_load(5)<=p_db_reg_rx_in(adc_readout_idelay3_hg_5)(9);
        
        s_adc_readout_control.lg_idelay_en_vtc(0)<=p_db_reg_rx_in(adc_readout_idelay3_lg_0)(10);
        s_adc_readout_control.hg_idelay_en_vtc(0)<=p_db_reg_rx_in(adc_readout_idelay3_hg_0)(10);
        s_adc_readout_control.lg_idelay_en_vtc(1)<=p_db_reg_rx_in(adc_readout_idelay3_lg_1)(10);
        s_adc_readout_control.hg_idelay_en_vtc(1)<=p_db_reg_rx_in(adc_readout_idelay3_hg_1)(10);
        s_adc_readout_control.lg_idelay_en_vtc(2)<=p_db_reg_rx_in(adc_readout_idelay3_lg_2)(10);
        s_adc_readout_control.hg_idelay_en_vtc(2)<=p_db_reg_rx_in(adc_readout_idelay3_hg_2)(10);
        s_adc_readout_control.lg_idelay_en_vtc(3)<=p_db_reg_rx_in(adc_readout_idelay3_lg_3)(10);
        s_adc_readout_control.hg_idelay_en_vtc(3)<=p_db_reg_rx_in(adc_readout_idelay3_hg_3)(10);
        s_adc_readout_control.lg_idelay_en_vtc(4)<=p_db_reg_rx_in(adc_readout_idelay3_lg_4)(10);
        s_adc_readout_control.hg_idelay_en_vtc(4)<=p_db_reg_rx_in(adc_readout_idelay3_hg_4)(10);
        s_adc_readout_control.lg_idelay_en_vtc(5)<=p_db_reg_rx_in(adc_readout_idelay3_lg_5)(10);
        s_adc_readout_control.hg_idelay_en_vtc(5)<=p_db_reg_rx_in(adc_readout_idelay3_hg_5)(10);
        
        s_adc_readout_control.lg_bitslip(0)<=p_db_reg_rx_in(adc_readout_idelay3_lg_0)(14 downto 11);
        s_adc_readout_control.hg_bitslip(0)<=p_db_reg_rx_in(adc_readout_idelay3_hg_0)(14 downto 11);
        s_adc_readout_control.lg_bitslip(1)<=p_db_reg_rx_in(adc_readout_idelay3_lg_1)(14 downto 11);
        s_adc_readout_control.hg_bitslip(1)<=p_db_reg_rx_in(adc_readout_idelay3_hg_1)(14 downto 11);
        s_adc_readout_control.lg_bitslip(2)<=p_db_reg_rx_in(adc_readout_idelay3_lg_2)(14 downto 11);
        s_adc_readout_control.hg_bitslip(2)<=p_db_reg_rx_in(adc_readout_idelay3_hg_2)(14 downto 11);
        s_adc_readout_control.lg_bitslip(3)<=p_db_reg_rx_in(adc_readout_idelay3_lg_3)(14 downto 11);
        s_adc_readout_control.hg_bitslip(3)<=p_db_reg_rx_in(adc_readout_idelay3_hg_3)(14 downto 11);
        s_adc_readout_control.lg_bitslip(4)<=p_db_reg_rx_in(adc_readout_idelay3_lg_4)(14 downto 11);
        s_adc_readout_control.hg_bitslip(4)<=p_db_reg_rx_in(adc_readout_idelay3_hg_4)(14 downto 11);
        s_adc_readout_control.lg_bitslip(5)<=p_db_reg_rx_in(adc_readout_idelay3_lg_5)(14 downto 11);
        s_adc_readout_control.hg_bitslip(5)<=p_db_reg_rx_in(adc_readout_idelay3_hg_5)(14 downto 11);
        
        s_adc_readout_control.channel_lg_data_test_pattern(0)<=p_db_reg_rx_in(adc_readout_test_pattern_lg_0)(13 downto 0);
        s_adc_readout_control.channel_hg_data_test_pattern(0)<=p_db_reg_rx_in(adc_readout_test_pattern_hg_0)(13 downto 0);
        s_adc_readout_control.channel_lg_data_test_pattern(1)<=p_db_reg_rx_in(adc_readout_test_pattern_lg_1)(13 downto 0);
        s_adc_readout_control.channel_hg_data_test_pattern(1)<=p_db_reg_rx_in(adc_readout_test_pattern_hg_1)(13 downto 0);
        s_adc_readout_control.channel_lg_data_test_pattern(2)<=p_db_reg_rx_in(adc_readout_test_pattern_lg_2)(13 downto 0);
        s_adc_readout_control.channel_hg_data_test_pattern(2)<=p_db_reg_rx_in(adc_readout_test_pattern_hg_2)(13 downto 0);
        s_adc_readout_control.channel_lg_data_test_pattern(3)<=p_db_reg_rx_in(adc_readout_test_pattern_lg_3)(13 downto 0);
        s_adc_readout_control.channel_hg_data_test_pattern(3)<=p_db_reg_rx_in(adc_readout_test_pattern_hg_3)(13 downto 0);
        s_adc_readout_control.channel_lg_data_test_pattern(4)<=p_db_reg_rx_in(adc_readout_test_pattern_lg_4)(13 downto 0);
        s_adc_readout_control.channel_hg_data_test_pattern(4)<=p_db_reg_rx_in(adc_readout_test_pattern_hg_4)(13 downto 0);
        s_adc_readout_control.channel_lg_data_test_pattern(5)<=p_db_reg_rx_in(adc_readout_test_pattern_lg_5)(13 downto 0);
        s_adc_readout_control.channel_hg_data_test_pattern(5)<=p_db_reg_rx_in(adc_readout_test_pattern_hg_5)(13 downto 0);

        s_adc_readout_control.channel_enable_test_pattern<=p_db_reg_rx_in(cfb_db_debug)(16 downto 11);
        s_adc_readout_control.channel_reset_test_pattern<=p_db_reg_rx_in(cfb_strobe_reg)(16 downto 11);

        s_adc_readout_control.channel_lg_pedestal_test_lower(0)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_0)(13 downto 0);
        s_adc_readout_control.channel_lg_pedestal_test_higher(0)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_0)(29 downto 16);
        s_adc_readout_control.channel_lg_pedestal_test_lower(1)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_1)(13 downto 0);
        s_adc_readout_control.channel_lg_pedestal_test_higher(1)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_1)(29 downto 16);
        s_adc_readout_control.channel_lg_pedestal_test_lower(2)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_2)(13 downto 0);
        s_adc_readout_control.channel_lg_pedestal_test_higher(2)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_2)(29 downto 16);
        s_adc_readout_control.channel_lg_pedestal_test_lower(3)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_3)(13 downto 0);
        s_adc_readout_control.channel_lg_pedestal_test_higher(3)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_3)(29 downto 16);
        s_adc_readout_control.channel_lg_pedestal_test_lower(4)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_4)(13 downto 0);
        s_adc_readout_control.channel_lg_pedestal_test_higher(4)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_4)(29 downto 16);
        s_adc_readout_control.channel_lg_pedestal_test_lower(5)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_5)(13 downto 0);
        s_adc_readout_control.channel_lg_pedestal_test_higher(5)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_lg_5)(29 downto 16);

        s_adc_readout_control.channel_hg_pedestal_test_lower(0)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_0)(13 downto 0);
        s_adc_readout_control.channel_hg_pedestal_test_higher(0)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_0)(29 downto 16);
        s_adc_readout_control.channel_hg_pedestal_test_lower(1)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_1)(13 downto 0);
        s_adc_readout_control.channel_hg_pedestal_test_higher(1)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_1)(29 downto 16);
        s_adc_readout_control.channel_hg_pedestal_test_lower(2)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_2)(13 downto 0);
        s_adc_readout_control.channel_hg_pedestal_test_higher(2)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_2)(29 downto 16);
        s_adc_readout_control.channel_hg_pedestal_test_lower(3)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_3)(13 downto 0);
        s_adc_readout_control.channel_hg_pedestal_test_higher(3)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_3)(29 downto 16);
        s_adc_readout_control.channel_hg_pedestal_test_lower(4)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_4)(13 downto 0);
        s_adc_readout_control.channel_hg_pedestal_test_higher(4)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_4)(29 downto 16);
        s_adc_readout_control.channel_hg_pedestal_test_lower(5)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_5)(13 downto 0);
        s_adc_readout_control.channel_hg_pedestal_test_higher(5)<=p_db_reg_rx_in(adc_readout_pedestal_stability_test_hg_5)(29 downto 16);

-- mainboard driver
p_mb_interface_out.mb_reset.q0 <= not p_db_reg_rx_in(cfb_db_debug)(9);
p_mb_interface_out.mb_reset.q1 <= not p_db_reg_rx_in(cfb_db_debug)(10);
p_mb_interface_out.mb_driver <= s_mb_driver;
i_db6_mainboard_driver : entity tilecal.db6_mainboard_driver
	port map(
        p_db_side_in     => "01",
        p_master_reset_in => p_master_reset_in,
        p_clknet_in       => p_clknet_in,
        p_ssel_out         => p_ssel_out,
        p_sclk_out         => p_sclk_out,
        p_sdata_out         => p_sdata_out,
        p_sdata_in      => p_sdata_in, 
        p_mb_txword_in      => s_mb_txword_in,
        p_mb_rxword_out       => s_mb_driver.rxword_out, --s_mb_rxword_out,
        p_done_out       => s_mb_driver.done_out, --s_mb_done_out,
        p_leds_out       => open 
);

s_adc_register_config_from_configbus.mb_fpga_select <= p_db_reg_rx_in(adc_config_module)(2 downto 0);
s_adc_register_config_from_configbus.mb_pmt_select <= p_db_reg_rx_in(adc_config_module)(4 downto 3);
s_adc_register_config_from_configbus.adc_registers(1) <= p_db_reg_rx_in(adc_register_config)(31 downto 24);
s_adc_register_config_from_configbus.adc_registers(2) <= p_db_reg_rx_in(adc_register_config)(23 downto 16);     
s_adc_register_config_from_configbus.adc_registers(3) <= p_db_reg_rx_in(adc_register_config)(15 downto 8);
s_adc_register_config_from_configbus.adc_registers(4) <= p_db_reg_rx_in(adc_register_config)(7 downto 0);
s_adc_register_config_from_configbus.mode <= p_db_reg_rx_in(adc_config_module)(31);
s_adc_register_config_from_configbus.trigger_mb_adc_config <= p_db_reg_rx_in(adc_config_module)(30);

s_adc_register_config_from_readout <= s_adc_readout.mb_adc_config_control;

i_db6_adc_config_driver  : entity tilecal.db6_adc_config_driver 
    port map( 
        p_master_reset_in    => p_master_reset_in, --p_adc_config_reset_in,--p_db_reg_rx_in(cfb_strobe_reg)(c_adc_config_reset_bit),
        p_clknet_in 			=> p_clknet_in,
        p_adc_register_config_from_readout_in => s_adc_register_config_from_readout,
        p_adc_register_config_from_configbus_in => s_adc_register_config_from_configbus,
        p_adc_config_done_out   => s_adc_readout_control.adc_config_done,
        p_fe_data_out           => s_mb_txword_in,
        p_fe_data_in            =>  p_db_reg_rx_in(cfb_mb_adc_config),
        p_leds_out => open
  );

i_db6_cis_interface : entity tilecal.db6_cis_interface
  port map( 
        p_clknet_in           => p_clknet_in,
        p_master_reset_in     => p_master_reset_in,
        p_db_reg_rx_in  =>p_db_reg_rx_in,
        p_tph_out               => p_tph_out,
        p_tpl_out               => p_tpl_out
  );

p_leds_out(3) <= s_mb_driver.done_out.q0;
p_leds_out(2) <= s_mb_driver.done_out.q1;
p_leds_out(1) <= s_adc_readout.readout_initialized;
p_leds_out(0) <= s_adc_readout_control.adc_config_done;


end Behavioral;
