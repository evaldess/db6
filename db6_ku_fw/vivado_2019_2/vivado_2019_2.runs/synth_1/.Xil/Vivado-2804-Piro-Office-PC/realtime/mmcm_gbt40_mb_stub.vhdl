-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mmcm_gbt40_mb is
  Port ( 
    p_clk40_out : out STD_LOGIC;
    p_clk40_90_out : out STD_LOGIC;
    p_clk40_180_out : out STD_LOGIC;
    p_clk40_270_out : out STD_LOGIC;
    p_clk80_out : out STD_LOGIC;
    p_clk280_out : out STD_LOGIC;
    p_clk560_out : out STD_LOGIC;
    p_daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    p_dclk_in : in STD_LOGIC;
    p_den_in : in STD_LOGIC;
    p_din_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    p_dout_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    p_drdy_out : out STD_LOGIC;
    p_dwe_in : in STD_LOGIC;
    p_psclk_in : in STD_LOGIC;
    p_psen_in : in STD_LOGIC;
    p_psincdec_in : in STD_LOGIC;
    p_psdone_out : out STD_LOGIC;
    p_reset_in : in STD_LOGIC;
    p_input_clk_stopped_out : out STD_LOGIC;
    p_clkfb_stopped_out : out STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_cddcdone_out : out STD_LOGIC;
    p_cddcreq_in : in STD_LOGIC;
    p_clk_in : in STD_LOGIC
  );

end mmcm_gbt40_mb;

architecture stub of mmcm_gbt40_mb is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
begin
end;
