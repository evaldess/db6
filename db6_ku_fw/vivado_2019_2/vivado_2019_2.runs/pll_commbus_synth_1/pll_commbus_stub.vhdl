-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Fri Dec 11 17:43:01 2020
-- Host        : Piro-Office-PC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               X:/db6_ku_fw/vivado_2019_2/vivado_2019_2.runs/pll_commbus_synth_1/pll_commbus_stub.vhdl
-- Design      : pll_commbus
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity pll_commbus is
  Port ( 
    p_commbusclk_out : out STD_LOGIC;
    p_bitclk_out : out STD_LOGIC;
    p_daddr_in : in STD_LOGIC_VECTOR ( 6 downto 0 );
    p_dclk_in : in STD_LOGIC;
    p_den_in : in STD_LOGIC;
    p_din_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    p_dout_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    p_drdy_out : out STD_LOGIC;
    p_dwe_in : in STD_LOGIC;
    p_reset_in : in STD_LOGIC;
    p_locked_out : out STD_LOGIC;
    p_clk_in : in STD_LOGIC
  );

end pll_commbus;

architecture stub of pll_commbus is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "p_commbusclk_out,p_bitclk_out,p_daddr_in[6:0],p_dclk_in,p_den_in,p_din_in[15:0],p_dout_out[15:0],p_drdy_out,p_dwe_in,p_reset_in,p_locked_out,p_clk_in";
begin
end;
