
�
Command: %s
53*	vivadotcl2b
Nsynth_design -top xlx_ku_mgt_ip -part xcku035-fbva676-1-c -mode out_of_context2default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xcku0352default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xcku0352default:defaultZ17-349h px� 
Z
Loading part %s157*device2'
xcku035-fbva676-1-c2default:defaultZ21-403h px� 
�
%s*synth2�
yStarting RTL Elaboration : Time (s): cpu = 00:00:04 ; elapsed = 00:00:04 . Memory (MB): peak = 1270.129 ; gain = 234.172
2default:defaulth px� 
�
synthesizing module '%s'%s4497*oasys2!
xlx_ku_mgt_ip2default:default2
 2default:default2M
7x:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip.v2default:default2
622default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2.
xlx_ku_mgt_ip_gtwizard_top2default:default2
 2default:default2Z
Dx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gtwizard_top.v2default:default2
1752default:default8@Z8-6157h px� 
�
%s
*synth2�
�	Parameter C_CHANNEL_ENABLE bound to: 192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001101 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_PCIE_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_PCIE_CORECLK_FREQ bound to: 250 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_COMMON_SCALING_FACTOR bound to: 1 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_CPLL_VCO_FREQUENCY bound to: 2404.800000 - type: double 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_FORCE_COMMONS bound to: 0 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_FREERUN_FREQUENCY bound to: 120.240000 - type: double 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_GT_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_GT_REV bound to: 17 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_INCLUDE_CPLL_CAL bound to: 2 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_ENABLE_COMMON_USRCLK bound to: 0 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_USER_GTPOWERGOOD_DELAY_EN bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_SIM_CPLL_CAL_BYPASS bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_LOCATE_COMMON bound to: 0 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_LOCATE_RESET_CONTROLLER bound to: 0 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter C_LOCATE_USER_DATA_WIDTH_SIZING bound to: 0 - type: integer 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER bound to: 0 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_LOCATE_IN_SYSTEM_IBERT_CORE bound to: 2 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_LOCATE_RX_USER_CLOCKING bound to: 1 - type: integer 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER bound to: 0 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_LOCATE_TX_USER_CLOCKING bound to: 1 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_RESET_CONTROLLER_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RX_BUFFBYPASS_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_RX_BUFFER_BYPASS_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_RX_BUFFER_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter C_RX_CB_DISP bound to: 8'b00000000 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter C_RX_CB_K bound to: 8'b00000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_RX_CB_MAX_LEVEL bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_RX_CB_LEN_SEQ bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_RX_CB_NUM_SEQ bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2�
w	Parameter C_RX_CB_VAL bound to: 80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter C_RX_CC_DISP bound to: 8'b00000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_RX_CC_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_RESET_SEQUENCE_INTERVAL bound to: 0 - type: integer 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter C_RX_CC_K bound to: 8'b00000000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_RX_CC_LEN_SEQ bound to: 1 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_RX_CC_NUM_SEQ bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_RX_CC_PERIODICITY bound to: 5000 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2�
w	Parameter C_RX_CC_VAL bound to: 80'b00000000000000000000000000000000000000000000000000000000000000000000000000000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_RX_COMMA_M_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_RX_COMMA_M_VAL bound to: 10'b1010000011 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_RX_COMMA_P_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_RX_COMMA_P_VAL bound to: 10'b0101111100 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_RX_DATA_DECODING bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_RX_ENABLE bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RX_INT_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RX_LINE_RATE bound to: 4.809600 - type: double 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_RX_MASTER_CHANNEL_IDX bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_RX_OUTCLK_BUFG_GT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_RX_OUTCLK_FREQUENCY bound to: 120.240000 - type: double 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_RX_OUTCLK_SOURCE bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_RX_PLL_TYPE bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2�
�	Parameter C_RX_RECCLK_OUTPUT bound to: 192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_RX_REFCLK_FREQUENCY bound to: 160.320000 - type: double 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_RX_SLIDE_MODE bound to: 1 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_RX_USER_CLOCKING_CONTENTS bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_RX_USER_CLOCKING_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter C_RX_USER_CLOCKING_RATIO_FSRC_FUSRCLK bound to: 1 - type: integer 
2default:defaulth p
x
� 
~
%s
*synth2f
R	Parameter C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 bound to: 1 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_RX_USER_CLOCKING_SOURCE bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_RX_USER_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_RX_USRCLK_FREQUENCY bound to: 120.240000 - type: double 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter C_RX_USRCLK2_FREQUENCY bound to: 120.240000 - type: double 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_SECONDARY_QPLL_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
~
%s
*synth2f
R	Parameter C_SECONDARY_QPLL_REFCLK_FREQUENCY bound to: 257.812500 - type: double 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_TOTAL_NUM_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_TOTAL_NUM_COMMONS bound to: 1 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_TOTAL_NUM_COMMONS_EXAMPLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TXPROGDIV_FREQ_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TXPROGDIV_FREQ_SOURCE bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter C_TXPROGDIV_FREQ_VAL bound to: 240.480000 - type: double 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_TX_BUFFBYPASS_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_TX_BUFFER_BYPASS_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_TX_BUFFER_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_TX_DATA_ENCODING bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TX_ENABLE bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_TX_INT_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_TX_LINE_RATE bound to: 9.619200 - type: double 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TX_MASTER_CHANNEL_IDX bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TX_OUTCLK_BUFG_GT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_TX_OUTCLK_FREQUENCY bound to: 240.480000 - type: double 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_TX_OUTCLK_SOURCE bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_TX_PLL_TYPE bound to: 1 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_TX_REFCLK_FREQUENCY bound to: 160.320000 - type: double 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_TX_USER_CLOCKING_CONTENTS bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_TX_USER_CLOCKING_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter C_TX_USER_CLOCKING_RATIO_FSRC_FUSRCLK bound to: 1 - type: integer 
2default:defaulth p
x
� 
~
%s
*synth2f
R	Parameter C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 bound to: 1 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_TX_USER_CLOCKING_SOURCE bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_TX_USER_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_TX_USRCLK_FREQUENCY bound to: 240.480000 - type: double 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter C_TX_USRCLK2_FREQUENCY bound to: 240.480000 - type: double 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys20
xlx_ku_mgt_ip_gtwizard_gthe32default:default2
 2default:default2\
Fx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gtwizard_gthe3.v2default:default2
1432default:default8@Z8-6157h px� 
�
%s
*synth2�
�	Parameter C_CHANNEL_ENABLE bound to: 192'b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001101 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_PCIE_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_PCIE_CORECLK_FREQ bound to: 250 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_COMMON_SCALING_FACTOR bound to: 1 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter C_CPLL_VCO_FREQUENCY bound to: 2404.800000 - type: double 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_FREERUN_FREQUENCY bound to: 120.240000 - type: double 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_GT_REV bound to: 17 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_INCLUDE_CPLL_CAL bound to: 2 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_ENABLE_COMMON_USRCLK bound to: 0 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_LOCATE_RESET_CONTROLLER bound to: 0 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter C_LOCATE_USER_DATA_WIDTH_SIZING bound to: 0 - type: integer 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter C_LOCATE_RX_BUFFER_BYPASS_CONTROLLER bound to: 0 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_LOCATE_RX_USER_CLOCKING bound to: 1 - type: integer 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter C_LOCATE_TX_BUFFER_BYPASS_CONTROLLER bound to: 0 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_LOCATE_TX_USER_CLOCKING bound to: 1 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_RESET_CONTROLLER_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RX_BUFFBYPASS_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_RX_BUFFER_BYPASS_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_RX_BUFFER_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_RX_DATA_DECODING bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_RX_ENABLE bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RX_INT_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RX_LINE_RATE bound to: 4.809600 - type: double 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_RX_MASTER_CHANNEL_IDX bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_RX_OUTCLK_BUFG_GT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_RX_PLL_TYPE bound to: 2 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_RX_USER_CLOCKING_CONTENTS bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_RX_USER_CLOCKING_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
~
%s
*synth2f
R	Parameter C_RX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 bound to: 1 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_RX_USER_CLOCKING_SOURCE bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_RX_USER_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_TOTAL_NUM_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_TOTAL_NUM_COMMONS bound to: 1 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TXPROGDIV_FREQ_ENABLE bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TXPROGDIV_FREQ_SOURCE bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_TX_BUFFBYPASS_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_TX_BUFFER_BYPASS_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_TX_BUFFER_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_TX_DATA_ENCODING bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TX_ENABLE bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_TX_INT_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TX_MASTER_CHANNEL_IDX bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter C_TX_OUTCLK_BUFG_GT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_TX_PLL_TYPE bound to: 1 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter C_TX_USER_CLOCKING_CONTENTS bound to: 0 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter C_TX_USER_CLOCKING_INSTANCE_CTRL bound to: 0 - type: integer 
2default:defaulth p
x
� 
~
%s
*synth2f
R	Parameter C_TX_USER_CLOCKING_RATIO_FUSRCLK_FUSRCLK2 bound to: 1 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter C_TX_USER_CLOCKING_SOURCE bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_TX_USER_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
�
%s
*synth2o
[	Parameter P_COMMON_ENABLE bound to: 48'b000000000000000000000000000000000000000000000001 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter P_TX_MASTER_CH_PACKED_IDX bound to: 0 - type: integer 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter P_RX_MASTER_CH_PACKED_IDX bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter P_USE_CPLL_CAL bound to: 0 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter P_CPLL_CAL_FREQ_COUNT_WINDOW bound to: 16'b0011111010000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter P_CPLL_CAL_TXOUTCLK_PERIOD bound to: 18'b000000111110100000 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter P_CPLL_CAL_WAIT_DEASSERT_CPLLPD bound to: 16'b0000000100000000 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter P_CPLL_CAL_TXOUTCLK_PERIOD_DIV100 bound to: 18'b000000000000101000 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter P_CDR_TIMEOUT_FREERUN_CYC bound to: 26'b00000011100001110101001000 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys26
"xlx_ku_mgt_ip_gthe3_common_wrapper2default:default2
 2default:default2b
Lx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gthe3_common_wrapper.v2default:default2
562default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2
 2default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/gtwizard_ultrascale_v1_7_gthe3_common.v2default:default2
552default:default8@Z8-6157h px� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_BIAS_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_BIAS_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_BIAS_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_BIAS_CFG3 bound to: 16'b0000000001000000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_BIAS_CFG4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_COMMON_BIAS_CFG_RSVD bound to: 10'b0000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_COMMON_COMMON_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_COMMON_COMMON_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_COMMON_POR_CFG bound to: 16'b0000000000000100 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL0_CFG0 bound to: 16'b0011001000011100 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL0_CFG1 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_COMMON_QPLL0_CFG1_G3 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL0_CFG2 bound to: 16'b0000000001001000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_COMMON_QPLL0_CFG2_G3 bound to: 16'b0000000001001000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL0_CFG3 bound to: 16'b0000000100100000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL0_CFG4 bound to: 16'b0000000000001001 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_COMMON_QPLL0_CP bound to: 10'b0000011111 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_COMMON_QPLL0_CP_G3 bound to: 10'b1111111111 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_QPLL0_FBDIV bound to: 66 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_COMMON_QPLL0_FBDIV_G3 bound to: 80 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_COMMON_QPLL0_INIT_CFG0 bound to: 16'b0000001010110010 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_QPLL0_INIT_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL0_LOCK_CFG bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_COMMON_QPLL0_LOCK_CFG_G3 bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_QPLL0_LPF bound to: 10'b1111111100 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_QPLL0_LPF_G3 bound to: 10'b0000010101 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_COMMON_QPLL0_REFCLK_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL0_SDM_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL0_SDM_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL0_SDM_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL1_CFG0 bound to: 16'b0011001000011100 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL1_CFG1 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_COMMON_QPLL1_CFG1_G3 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL1_CFG2 bound to: 16'b0000000001000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_COMMON_QPLL1_CFG2_G3 bound to: 16'b0000000001000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL1_CFG3 bound to: 16'b0000000100100000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_QPLL1_CFG4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_COMMON_QPLL1_CP bound to: 10'b0000011111 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_COMMON_QPLL1_CP_G3 bound to: 10'b1111111111 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_QPLL1_FBDIV bound to: 60 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_COMMON_QPLL1_FBDIV_G3 bound to: 80 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_COMMON_QPLL1_INIT_CFG0 bound to: 16'b0000001010110010 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_QPLL1_INIT_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL1_LOCK_CFG bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_COMMON_QPLL1_LOCK_CFG_G3 bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_QPLL1_LPF bound to: 10'b1111111110 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_QPLL1_LPF_G3 bound to: 10'b0000010101 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_COMMON_QPLL1_REFCLK_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL1_SDM_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL1_SDM_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_COMMON_QPLL1_SDM_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_RSVD_ATTR0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_RSVD_ATTR1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_RSVD_ATTR2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_COMMON_RSVD_ATTR3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_RXRECCLKOUT0_SEL bound to: 2'b00 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_RXRECCLKOUT1_SEL bound to: 2'b00 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter GTHE3_COMMON_SARC_EN bound to: 1'b1 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter GTHE3_COMMON_SARC_SEL bound to: 1'b0 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_COMMON_SDM0DATA1_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_SDM0DATA1_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_COMMON_SDM0INITSEED0_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_COMMON_SDM0INITSEED0_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_SDM0_DATA_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_COMMON_SDM0_WIDTH_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_COMMON_SDM1DATA1_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_SDM1DATA1_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_COMMON_SDM1INITSEED0_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_COMMON_SDM1INITSEED0_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_SDM1_DATA_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_COMMON_SDM1_WIDTH_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_COMMON_SIM_RESET_SPEEDUP bound to: TRUE - type: string 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_COMMON_SIM_VERSION bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_COMMON_BGBYPASSB_VAL bound to: 1'b1 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_COMMON_BGMONITORENB_VAL bound to: 1'b1 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter GTHE3_COMMON_BGPDB_VAL bound to: 1'b1 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_COMMON_BGRCALOVRD_VAL bound to: 5'b11111 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_BGRCALOVRDENB_VAL bound to: 1'b1 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_DRPADDR_VAL bound to: 9'b000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_COMMON_DRPCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_COMMON_DRPDI_VAL bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter GTHE3_COMMON_DRPEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter GTHE3_COMMON_DRPWE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_GTGREFCLK0_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_GTGREFCLK1_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTNORTHREFCLK00_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTNORTHREFCLK01_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTNORTHREFCLK10_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTNORTHREFCLK11_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_GTREFCLK00_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_GTREFCLK01_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_GTREFCLK10_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_GTREFCLK11_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTSOUTHREFCLK00_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTSOUTHREFCLK01_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTSOUTHREFCLK10_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_GTSOUTHREFCLK11_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_PMARSVD0_VAL bound to: 8'b00000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_PMARSVD1_VAL bound to: 8'b00000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLL0CLKRSVD0_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLL0CLKRSVD1_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_QPLL0LOCKDETCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_COMMON_QPLL0LOCKEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_COMMON_QPLL0PD_VAL bound to: 1'b1 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLL0REFCLKSEL_VAL bound to: 3'b001 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_QPLL0RESET_VAL bound to: 1'b1 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLL1CLKRSVD0_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLL1CLKRSVD1_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_QPLL1LOCKDETCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_COMMON_QPLL1LOCKEN_VAL bound to: 1'b1 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_COMMON_QPLL1PD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLL1REFCLKSEL_VAL bound to: 3'b001 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_QPLL1RESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLLRSVD1_VAL bound to: 8'b00000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLLRSVD2_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLLRSVD3_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLLRSVD4_VAL bound to: 8'b00000000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_COMMON_RCALENB_VAL bound to: 1'b1 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_COMMON_BGBYPASSB_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_COMMON_BGMONITORENB_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_COMMON_BGPDB_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_BGRCALOVRD_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_BGRCALOVRDENB_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_DRPADDR_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_COMMON_DRPCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_COMMON_DRPDI_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_COMMON_DRPEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_COMMON_DRPWE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_GTGREFCLK0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_GTGREFCLK1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTNORTHREFCLK00_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTNORTHREFCLK01_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTNORTHREFCLK10_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTNORTHREFCLK11_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_GTREFCLK00_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_GTREFCLK01_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_GTREFCLK10_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_GTREFCLK11_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTSOUTHREFCLK00_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTSOUTHREFCLK01_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTSOUTHREFCLK10_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_GTSOUTHREFCLK11_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_COMMON_PMARSVD0_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_COMMON_PMARSVD1_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLL0CLKRSVD0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLL0CLKRSVD1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_QPLL0LOCKDETCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_COMMON_QPLL0LOCKEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_QPLL0PD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_COMMON_QPLL0REFCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLL0RESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLL1CLKRSVD0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_COMMON_QPLL1CLKRSVD1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_COMMON_QPLL1LOCKDETCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_COMMON_QPLL1LOCKEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_QPLL1PD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_COMMON_QPLL1REFCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_COMMON_QPLL1RESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_COMMON_QPLLRSVD1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_COMMON_QPLLRSVD2_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_COMMON_QPLLRSVD3_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_COMMON_QPLLRSVD4_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_COMMON_RCALENB_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2 
GTHE3_COMMON2default:default2
 2default:default2P
:D:/apps/xilinx/Vivado/2019.2/scripts/rt/data/unisim_comp.v2default:default2
166882default:default8@Z8-6157h px� 
a
%s
*synth2I
5	Parameter BIAS_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter BIAS_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter BIAS_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter BIAS_CFG3 bound to: 16'b0000000001000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter BIAS_CFG4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter BIAS_CFG_RSVD bound to: 10'b0000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter COMMON_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter COMMON_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter POR_CFG bound to: 16'b0000000000000100 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL0_CFG0 bound to: 16'b0011001000011100 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL0_CFG1 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter QPLL0_CFG1_G3 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL0_CFG2 bound to: 16'b0000000001001000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter QPLL0_CFG2_G3 bound to: 16'b0000000001001000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL0_CFG3 bound to: 16'b0000000100100000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL0_CFG4 bound to: 16'b0000000000001001 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter QPLL0_CP bound to: 10'b0000011111 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter QPLL0_CP_G3 bound to: 10'b1111111111 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter QPLL0_FBDIV bound to: 66 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter QPLL0_FBDIV_G3 bound to: 80 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter QPLL0_INIT_CFG0 bound to: 16'b0000001010110010 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter QPLL0_INIT_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL0_LOCK_CFG bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter QPLL0_LOCK_CFG_G3 bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter QPLL0_LPF bound to: 10'b1111111100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter QPLL0_LPF_G3 bound to: 10'b0000010101 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter QPLL0_REFCLK_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL0_SDM_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL0_SDM_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL0_SDM_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL1_CFG0 bound to: 16'b0011001000011100 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL1_CFG1 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter QPLL1_CFG1_G3 bound to: 16'b0001000000011000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL1_CFG2 bound to: 16'b0000000001000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter QPLL1_CFG2_G3 bound to: 16'b0000000001000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL1_CFG3 bound to: 16'b0000000100100000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter QPLL1_CFG4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter QPLL1_CP bound to: 10'b0000011111 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter QPLL1_CP_G3 bound to: 10'b1111111111 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter QPLL1_FBDIV bound to: 60 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter QPLL1_FBDIV_G3 bound to: 80 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter QPLL1_INIT_CFG0 bound to: 16'b0000001010110010 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter QPLL1_INIT_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL1_LOCK_CFG bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter QPLL1_LOCK_CFG_G3 bound to: 16'b0010000111101000 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter QPLL1_LPF bound to: 10'b1111111110 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter QPLL1_LPF_G3 bound to: 10'b0000010101 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter QPLL1_REFCLK_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL1_SDM_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL1_SDM_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter QPLL1_SDM_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RSVD_ATTR0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RSVD_ATTR1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RSVD_ATTR2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RSVD_ATTR3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RXRECCLKOUT0_SEL bound to: 2'b00 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RXRECCLKOUT1_SEL bound to: 2'b00 
2default:defaulth p
x
� 
O
%s
*synth27
#	Parameter SARC_EN bound to: 1'b1 
2default:defaulth p
x
� 
P
%s
*synth28
$	Parameter SARC_SEL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter SDM0DATA1_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter SDM0DATA1_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SDM0INITSEED0_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter SDM0INITSEED0_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter SDM0_DATA_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter SDM0_WIDTH_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter SDM1DATA1_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter SDM1DATA1_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter SDM1INITSEED0_0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter SDM1INITSEED0_1 bound to: 9'b000000000 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter SDM1_DATA_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter SDM1_WIDTH_PIN_SEL bound to: 1'b0 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter SIM_MODE bound to: FAST - type: string 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SIM_RESET_SPEEDUP bound to: TRUE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SIM_VERSION bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
GTHE3_COMMON2default:default2
 2default:default2
12default:default2
12default:default2P
:D:/apps/xilinx/Vivado/2019.2/scripts/rt/data/unisim_comp.v2default:default2
166882default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2
 2default:default2
22default:default2
12default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/gtwizard_ultrascale_v1_7_gthe3_common.v2default:default2
552default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys26
"xlx_ku_mgt_ip_gthe3_common_wrapper2default:default2
 2default:default2
32default:default2
12default:default2b
Lx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gthe3_common_wrapper.v2default:default2
562default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys27
#xlx_ku_mgt_ip_gthe3_channel_wrapper2default:default2
 2default:default2c
Mx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gthe3_channel_wrapper.v2default:default2
562default:default8@Z8-6157h px� 
^
%s
*synth2F
2	Parameter MASTER_EN bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2
 2default:default2f
Px:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/gtwizard_ultrascale_v1_7_gthe3_channel.v2default:default2
552default:default8@Z8-6157h px� 
a
%s
*synth2I
5	Parameter NUM_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_ACJTAG_DEBUG_MODE bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_ACJTAG_MODE bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_ACJTAG_RESET bound to: 1'b0 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_ADAPT_CFG0 bound to: 16'b1111100000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_ADAPT_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter GTHE3_CHANNEL_ALIGN_COMMA_DOUBLE bound to: FALSE - type: string 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_ALIGN_COMMA_ENABLE bound to: 10'b0000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ALIGN_COMMA_WORD bound to: 1 - type: integer 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_ALIGN_MCOMMA_DET bound to: FALSE - type: string 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_ALIGN_MCOMMA_VALUE bound to: 10'b1010000011 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_ALIGN_PCOMMA_DET bound to: FALSE - type: string 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_ALIGN_PCOMMA_VALUE bound to: 10'b0101111100 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_A_RXOSCALRESET bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_A_RXPROGDIVRESET bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_A_TXPROGDIVRESET bound to: 1'b0 
2default:defaulth p
x
� 
|
%s
*synth2d
P	Parameter GTHE3_CHANNEL_CBCC_DATA_SOURCE_SEL bound to: ENCODED - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_CDR_SWAP_MODE_EN bound to: 1'b0 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter GTHE3_CHANNEL_CHAN_BOND_KEEP_ALIGN bound to: FALSE - type: string 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_CHAN_BOND_MAX_SKEW bound to: 1 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_1_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_1_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_1_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_1_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_1_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_2_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_2_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_2_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_2_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_2_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_2_USE bound to: FALSE - type: string 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_CHAN_BOND_SEQ_LEN bound to: 1 - type: integer 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_CLK_CORRECT_USE bound to: FALSE - type: string 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_CLK_COR_KEEP_IDLE bound to: FALSE - type: string 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_CLK_COR_MAX_LAT bound to: 20 - type: integer 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_CLK_COR_MIN_LAT bound to: 18 - type: integer 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_CLK_COR_PRECEDENCE bound to: TRUE - type: string 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_CLK_COR_REPEAT_WAIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_1_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_1_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_1_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_1_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_1_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_2_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_2_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_2_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_2_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_2_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_2_USE bound to: FALSE - type: string 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_CLK_COR_SEQ_LEN bound to: 1 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CPLL_CFG0 bound to: 16'b0110011111111000 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CPLL_CFG1 bound to: 16'b1010010010101100 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_CPLL_CFG2 bound to: 16'b0101000000000111 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_CPLL_CFG3 bound to: 6'b000000 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_CPLL_FBDIV bound to: 3 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_CPLL_FBDIV_45 bound to: 5 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_CPLL_INIT_CFG0 bound to: 16'b0000001010110010 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_CPLL_INIT_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_CPLL_LOCK_CFG bound to: 16'b0000000111101000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_CPLL_REFCLK_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_DDI_CTRL bound to: 2'b00 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_DDI_REALIGN_WAIT bound to: 15 - type: integer 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_DEC_MCOMMA_DETECT bound to: FALSE - type: string 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_DEC_PCOMMA_DETECT bound to: FALSE - type: string 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter GTHE3_CHANNEL_DEC_VALID_COMMA_ONLY bound to: FALSE - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_DFE_D_X_REL_POS bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_DFE_VCM_COMP_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_DMONITOR_CFG0 bound to: 10'b0000000000 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_DMONITOR_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_ES_CLK_PHASE_SEL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_ES_CONTROL bound to: 6'b000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_ES_ERRDET_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_ES_EYE_SCAN_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_ES_HORZ_OFFSET bound to: 12'b000000000000 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_ES_PMA_CFG bound to: 10'b0000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_ES_PRESCALE bound to: 5'b00000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUALIFIER0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUALIFIER1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUALIFIER2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUALIFIER3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUALIFIER4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUAL_MASK0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUAL_MASK1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUAL_MASK2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUAL_MASK3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_ES_QUAL_MASK4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_ES_SDATA_MASK0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_ES_SDATA_MASK1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_ES_SDATA_MASK2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_ES_SDATA_MASK3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_ES_SDATA_MASK4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_EVODD_PHI_CFG bound to: 11'b00000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_EYE_SCAN_SWAP_EN bound to: 1'b0 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_FTS_DESKEW_SEQ_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_FTS_LANE_DESKEW_CFG bound to: 4'b1111 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter GTHE3_CHANNEL_FTS_LANE_DESKEW_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_GEARBOX_MODE bound to: 5'b00000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_GM_BIAS_SELECT bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_LOCAL_MASTER bound to: 1'b1 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_OOBDIVCTL bound to: 2'b00 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_OOB_PWRUP bound to: 1'b0 
2default:defaulth p
x
� 
|
%s
*synth2d
P	Parameter GTHE3_CHANNEL_PCI3_AUTO_REALIGN bound to: OVR_1K_BLK - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_PCI3_PIPE_RX_ELECIDLE bound to: 1'b0 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_PCI3_RX_ASYNC_EBUF_BYPASS bound to: 2'b00 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_PCI3_RX_ELECIDLE_EI2_ENABLE bound to: 1'b0 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_PCI3_RX_ELECIDLE_H2L_COUNT bound to: 6'b000000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_PCI3_RX_ELECIDLE_H2L_DISABLE bound to: 3'b000 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_PCI3_RX_ELECIDLE_HI_COUNT bound to: 6'b000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_PCI3_RX_ELECIDLE_LP4_DISABLE bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_PCI3_RX_FIFO_DISABLE bound to: 1'b0 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter GTHE3_CHANNEL_PCIE_BUFG_DIV_CTRL bound to: 16'b0001000000000000 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter GTHE3_CHANNEL_PCIE_RXPCS_CFG_GEN3 bound to: 16'b0000001010100100 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_PCIE_RXPMA_CFG bound to: 16'b0000000000001010 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter GTHE3_CHANNEL_PCIE_TXPCS_CFG_GEN3 bound to: 16'b0010010010100100 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_PCIE_TXPMA_CFG bound to: 16'b0000000000001010 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_PCS_PCIE_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_PCS_RSVD0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_PCS_RSVD1 bound to: 3'b000 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_PD_TRANS_TIME_FROM_P2 bound to: 12'b000000111100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_PD_TRANS_TIME_NONE_P2 bound to: 8'b00011001 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_PD_TRANS_TIME_TO_P2 bound to: 8'b01100100 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_PLL_SEL_MODE_GEN12 bound to: 2'b00 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_PLL_SEL_MODE_GEN3 bound to: 2'b11 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_PMA_RSV1 bound to: 16'b1111000000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_PROCESS_PAR bound to: 3'b010 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RATE_SW_USE_DRP bound to: 1'b1 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RESET_POWERSAVE_DISABLE bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXBUFRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_RXBUF_ADDR_MODE bound to: FAST - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXBUF_EIDLE_HI_CNT bound to: 4'b1000 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXBUF_EIDLE_LO_CNT bound to: 4'b0000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_RXBUF_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
}
%s
*synth2e
Q	Parameter GTHE3_CHANNEL_RXBUF_RESET_ON_CB_CHANGE bound to: TRUE - type: string 
2default:defaulth p
x
� 

%s
*synth2g
S	Parameter GTHE3_CHANNEL_RXBUF_RESET_ON_COMMAALIGN bound to: FALSE - type: string 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter GTHE3_CHANNEL_RXBUF_RESET_ON_EIDLE bound to: FALSE - type: string 
2default:defaulth p
x
� 

%s
*synth2g
S	Parameter GTHE3_CHANNEL_RXBUF_RESET_ON_RATE_CHANGE bound to: TRUE - type: string 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXBUF_THRESH_OVFLW bound to: 0 - type: integer 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_RXBUF_THRESH_OVRD bound to: FALSE - type: string 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_RXBUF_THRESH_UNDFLW bound to: 0 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXCDRFREQRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXCDRPHRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXCDR_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_CFG0_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXCDR_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_CFG1_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXCDR_CFG2 bound to: 16'b0000011111100110 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_CFG2_GEN3 bound to: 16'b0000011111100110 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXCDR_CFG3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_CFG3_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXCDR_CFG4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_CFG4_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXCDR_CFG5 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_CFG5_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXCDR_FR_RESET_ON_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXCDR_HOLD_DURING_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_LOCK_CFG0 bound to: 16'b0100010010000000 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_LOCK_CFG1 bound to: 16'b0101111111111111 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter GTHE3_CHANNEL_RXCDR_LOCK_CFG2 bound to: 16'b0111011111000011 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXCDR_PH_RESET_ON_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RXCFOK_CFG0 bound to: 16'b0100000000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RXCFOK_CFG1 bound to: 16'b0000000001100101 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RXCFOK_CFG2 bound to: 16'b0000000000101110 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_RXDFELPMRESET_TIME bound to: 7'b0001111 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_RXDFELPM_KL_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_RXDFELPM_KL_CFG1 bound to: 16'b0000000000000010 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_RXDFELPM_KL_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXDFE_CFG0 bound to: 16'b0000101000000000 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXDFE_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_GC_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_GC_CFG1 bound to: 16'b0111100001110000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_GC_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H2_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H2_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H3_CFG0 bound to: 16'b0100000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H3_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H4_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H4_CFG1 bound to: 16'b0000000000000011 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H5_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H5_CFG1 bound to: 16'b0000000000000011 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H6_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H6_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H7_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H7_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H8_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H8_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H9_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_H9_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HA_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HA_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HB_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HB_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HC_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HC_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HD_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HD_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HE_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HE_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HF_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_HF_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_OS_CFG0 bound to: 16'b1000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_OS_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_UT_CFG0 bound to: 16'b1000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_UT_CFG1 bound to: 16'b0000000000000011 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_VP_CFG0 bound to: 16'b1010101000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXDFE_VP_CFG1 bound to: 16'b0000000000110011 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_RXDLY_CFG bound to: 16'b0000000000011111 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXDLY_LCFG bound to: 16'b0000000000110000 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_RXELECIDLE_CFG bound to: Sigcfg_4 - type: string 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter GTHE3_CHANNEL_RXGBOX_FIFO_INIT_RD_ADDR bound to: 4 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_RXGEARBOX_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXISCANRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_RXLPM_CFG bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_RXLPM_GC_CFG bound to: 16'b0001000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXLPM_KH_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXLPM_KH_CFG1 bound to: 16'b0000000000000010 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXLPM_OS_CFG0 bound to: 16'b1000000000000000 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RXLPM_OS_CFG1 bound to: 16'b0000000000000010 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOOB_CFG bound to: 9'b000000110 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RXOOB_CLK_CFG bound to: PMA - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXOSCALRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXOUT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXPCSRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_RXPHBEACON_CFG bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RXPHDLY_CFG bound to: 16'b0010000000100000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_RXPHSAMP_CFG bound to: 16'b0010000100000000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_RXPHSLIP_CFG bound to: 16'b0110011000100010 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXPH_MONITOR_SEL bound to: 5'b00000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_RXPI_CFG0 bound to: 2'b00 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_RXPI_CFG1 bound to: 2'b00 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_RXPI_CFG2 bound to: 2'b00 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_RXPI_CFG3 bound to: 2'b00 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_RXPI_CFG4 bound to: 1'b1 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_RXPI_CFG5 bound to: 1'b1 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RXPI_CFG6 bound to: 3'b011 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter GTHE3_CHANNEL_RXPI_LPM bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RXPI_VREFSEL bound to: 1'b0 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RXPMACLK_SEL bound to: DATA - type: string 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXPMARESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXPRBS_ERR_LOOPBACK bound to: 1'b0 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_RXPRBS_LINKACQ_CNT bound to: 15 - type: integer 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_RXSLIDE_AUTO_WAIT bound to: 7 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RXSLIDE_MODE bound to: PCS - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXSYNC_MULTILANE bound to: 1'b1 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RXSYNC_OVRD bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXSYNC_SKIP_DA bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RX_AFE_CM_EN bound to: 1'b0 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_RX_BIAS_CFG0 bound to: 16'b0000101010110100 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RX_BUFFER_CFG bound to: 6'b000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RX_CAPFF_SARC_ENB bound to: 1'b0 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_RX_CLK25_DIV bound to: 7 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RX_CLKMUX_EN bound to: 1'b1 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RX_CLK_SLIP_OVRD bound to: 5'b00000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RX_CM_BUF_CFG bound to: 4'b1010 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RX_CM_BUF_PD bound to: 1'b0 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_RX_CM_SEL bound to: 2'b11 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RX_CM_TRIM bound to: 4'b1010 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RX_CTLE3_LPF bound to: 8'b00000001 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RX_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RX_DDI_SEL bound to: 6'b000000 
2default:defaulth p
x
� 
z
%s
*synth2b
N	Parameter GTHE3_CHANNEL_RX_DEFER_RESET_BUF_EN bound to: TRUE - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RX_DFELPM_CFG0 bound to: 4'b0110 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RX_DFELPM_CFG1 bound to: 1'b1 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_RX_DFELPM_KLKH_AGC_STUP_EN bound to: 1'b1 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RX_DFE_AGC_CFG0 bound to: 2'b10 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RX_DFE_AGC_CFG1 bound to: 3'b100 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RX_DFE_KL_LPM_KH_CFG0 bound to: 2'b01 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RX_DFE_KL_LPM_KH_CFG1 bound to: 3'b100 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RX_DFE_KL_LPM_KL_CFG0 bound to: 2'b01 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RX_DFE_KL_LPM_KL_CFG1 bound to: 3'b100 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_RX_DFE_LPM_HOLD_DURING_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter GTHE3_CHANNEL_RX_DISPERR_SEQ_MATCH bound to: TRUE - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RX_DIVRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RX_EN_HI_LR bound to: 1'b0 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_RX_EYESCAN_VS_CODE bound to: 7'b0000000 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RX_EYESCAN_VS_NEG_DIR bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RX_EYESCAN_VS_RANGE bound to: 2'b00 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RX_EYESCAN_VS_UT_SIGN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RX_FABINT_USRCLK_FLOP bound to: 1'b0 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_RX_INT_DATAWIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RX_PMA_POWER_SAVE bound to: 1'b0 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter GTHE3_CHANNEL_RX_PROGDIV_CFG bound to: 0.000000 - type: double 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RX_SAMPLE_PERIOD bound to: 3'b111 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_RX_SIG_VALID_DLY bound to: 11 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RX_SUM_DFETAPREP_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RX_SUM_IREF_TUNE bound to: 4'b0000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RX_SUM_RES_CTRL bound to: 2'b00 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RX_SUM_VCMTUNE bound to: 4'b0000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RX_SUM_VCM_OVWR bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RX_SUM_VREF_TUNE bound to: 3'b000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RX_TUNE_AFE_OS bound to: 2'b10 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RX_WIDEMODE_CDR bound to: 1'b0 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_RX_XCLK_SEL bound to: RXUSR - type: string 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_SAS_MAX_COM bound to: 64 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_SAS_MIN_COM bound to: 36 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_SATA_BURST_SEQ_LEN bound to: 4'b1110 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_SATA_BURST_VAL bound to: 3'b100 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter GTHE3_CHANNEL_SATA_CPLL_CFG bound to: VCO_3000MHZ - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_SATA_EIDLE_VAL bound to: 3'b100 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_SATA_MAX_BURST bound to: 8 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_SATA_MAX_INIT bound to: 21 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_SATA_MAX_WAKE bound to: 7 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_SATA_MIN_BURST bound to: 4 - type: integer 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_SATA_MIN_INIT bound to: 12 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_SATA_MIN_WAKE bound to: 4 - type: integer 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter GTHE3_CHANNEL_SHOW_REALIGN_COMMA bound to: FALSE - type: string 
2default:defaulth p
x
� 
}
%s
*synth2e
Q	Parameter GTHE3_CHANNEL_SIM_RECEIVER_DETECT_PASS bound to: TRUE - type: string 
2default:defaulth p
x
� 
v
%s
*synth2^
J	Parameter GTHE3_CHANNEL_SIM_RESET_SPEEDUP bound to: TRUE - type: string 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_SIM_TX_EIDLE_DRIVE_LEVEL bound to: 1'b0 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_SIM_VERSION bound to: 2 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TAPDLY_SET_TX bound to: 2'b00 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TEMPERATUR_PAR bound to: 4'b0010 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_TERM_RCAL_CFG bound to: 15'b100001000010000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TERM_RCAL_OVRD bound to: 3'b000 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TRANS_TIME_RATE bound to: 8'b00001110 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TST_RSV0 bound to: 8'b00000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TST_RSV1 bound to: 8'b00000000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_TXBUF_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 

%s
*synth2g
S	Parameter GTHE3_CHANNEL_TXBUF_RESET_ON_RATE_CHANGE bound to: TRUE - type: string 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_TXDLY_CFG bound to: 16'b0000000000001001 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_TXDLY_LCFG bound to: 16'b0000000001010000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXDRVBIAS_N bound to: 4'b1010 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXDRVBIAS_P bound to: 4'b1010 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_TXFIFO_ADDR_CFG bound to: LOW - type: string 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter GTHE3_CHANNEL_TXGBOX_FIFO_INIT_RD_ADDR bound to: 4 - type: integer 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_TXGEARBOX_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TXOUT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXPCSRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_TXPHDLY_CFG0 bound to: 16'b0010000000100000 
2default:defaulth p
x
� 
r
%s
*synth2Z
F	Parameter GTHE3_CHANNEL_TXPHDLY_CFG1 bound to: 16'b0000000001110101 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_TXPH_CFG bound to: 16'b0000100110000000 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXPH_MONITOR_SEL bound to: 5'b00000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_TXPI_CFG0 bound to: 2'b00 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_TXPI_CFG1 bound to: 2'b00 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_TXPI_CFG2 bound to: 2'b00 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_TXPI_CFG3 bound to: 1'b1 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_TXPI_CFG4 bound to: 1'b1 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_TXPI_CFG5 bound to: 3'b000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXPI_GRAY_SEL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPI_INVSTROBE_SEL bound to: 1'b1 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter GTHE3_CHANNEL_TXPI_LPM bound to: 1'b0 
2default:defaulth p
x
� 
y
%s
*synth2a
M	Parameter GTHE3_CHANNEL_TXPI_PPMCLK_SEL bound to: TXUSRCLK2 - type: string 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXPI_PPM_CFG bound to: 8'b00000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPI_SYNFREQ_PPM bound to: 3'b001 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXPI_VREFSEL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXPMARESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXSYNC_MULTILANE bound to: 1'b1 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_TXSYNC_OVRD bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXSYNC_SKIP_DA bound to: 1'b0 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_TX_CLK25_DIV bound to: 7 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TX_CLKMUX_EN bound to: 1'b1 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_TX_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TX_DCD_CFG bound to: 6'b000010 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_TX_DCD_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TX_DEEMPH0 bound to: 6'b000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TX_DEEMPH1 bound to: 6'b000000 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TX_DIVRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_TX_DRIVE_MODE bound to: DIRECT - type: string 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_TX_EIDLE_ASSERT_DELAY bound to: 3'b100 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_TX_EIDLE_DEASSERT_DELAY bound to: 3'b011 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TX_EML_PHI_TUNE bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TX_FABINT_USRCLK_FLOP bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TX_IDLE_DATA_ZERO bound to: 1'b0 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_TX_INT_DATAWIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter GTHE3_CHANNEL_TX_LOOPBACK_DRIVE_HIZ bound to: FALSE - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TX_MAINCURSOR_SEL bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TX_MARGIN_FULL_0 bound to: 7'b1001111 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TX_MARGIN_FULL_1 bound to: 7'b1001110 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TX_MARGIN_FULL_2 bound to: 7'b1001100 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TX_MARGIN_FULL_3 bound to: 7'b1001010 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TX_MARGIN_FULL_4 bound to: 7'b1001000 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TX_MARGIN_LOW_0 bound to: 7'b1000110 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TX_MARGIN_LOW_1 bound to: 7'b1000101 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TX_MARGIN_LOW_2 bound to: 7'b1000011 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TX_MARGIN_LOW_3 bound to: 7'b1000010 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TX_MARGIN_LOW_4 bound to: 7'b1000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TX_MODE_SEL bound to: 3'b000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TX_PMADATA_OPT bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TX_PMA_POWER_SAVE bound to: 1'b0 
2default:defaulth p
x
� 
t
%s
*synth2\
H	Parameter GTHE3_CHANNEL_TX_PROGCLK_SEL bound to: PREPI - type: string 
2default:defaulth p
x
� 
x
%s
*synth2`
L	Parameter GTHE3_CHANNEL_TX_PROGDIV_CFG bound to: 20.000000 - type: double 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TX_QPI_STATUS_EN bound to: 1'b0 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_TX_RXDETECT_CFG bound to: 14'b00000000110010 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TX_RXDETECT_REF bound to: 3'b100 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TX_SAMPLE_PERIOD bound to: 3'b111 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TX_SARC_LPBK_ENB bound to: 1'b0 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_TX_XCLK_SEL bound to: TXUSR - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_USE_PCS_CLK_PHASE_SEL bound to: 1'b0 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter GTHE3_CHANNEL_WB_MODE bound to: 2'b00 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_CFGRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_CLKRSVD0_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_CLKRSVD1_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_CPLLLOCKDETCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_CPLLLOCKEN_VAL bound to: 1'b1 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_CPLLPD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_CPLLREFCLKSEL_VAL bound to: 3'b001 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_CPLLRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_DMONFIFORESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_DMONITORCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_DRPADDR_VAL bound to: 9'b000000000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_DRPCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_DRPDI_VAL bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_DRPEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_DRPWE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_EVODDPHICALDONE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_EVODDPHICALSTART_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_EVODDPHIDRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_EVODDPHIDWREN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_EVODDPHIXRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_EVODDPHIXWREN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_EYESCANMODE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_EYESCANRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_EYESCANTRIGGER_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTGREFCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_GTHRXN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter GTHE3_CHANNEL_GTHRXP_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_GTNORTHREFCLK0_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_GTNORTHREFCLK1_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTREFCLK0_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTREFCLK1_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_GTRESETSEL_VAL bound to: 1'b0 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_GTRSVD_VAL bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTRXRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_GTSOUTHREFCLK0_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_GTSOUTHREFCLK1_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTTXRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_LOOPBACK_VAL bound to: 3'b000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_LPBKRXTXSEREN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_LPBKTXRXSEREN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_PCIEEQRXEQADAPTDONE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_PCIERSTIDLE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_PCIERSTTXSYNCSTART_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_PCIEUSERRATEDONE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_PCSRSVDIN_VAL bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_PCSRSVDIN2_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_PMARSVDIN_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_QPLL0CLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_QPLL0REFCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_QPLL1CLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_QPLL1REFCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RESETOVRD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RSTCLKENTX_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RX8B10BEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXBUFRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXCDRFREQRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXCDRHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXCDROVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXCDRRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXCDRRESETRSV_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXCHBONDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXCHBONDI_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXCHBONDLEVEL_VAL bound to: 3'b000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXCHBONDMASTER_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXCHBONDSLAVE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXCOMMADETEN_VAL bound to: 1'b1 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFEAGCCTRL_VAL bound to: 2'b01 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXDFEAGCHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFEAGCOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXDFELFHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFELFOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFELPMRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFETAP10HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP10OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFETAP11HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP11OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFETAP12HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP12OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFETAP13HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP13OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFETAP14HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP14OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFETAP15HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP15OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP2HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP2OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP3HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP3OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP4HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP4OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP5HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP5OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP6HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP6OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP7HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP7OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP8HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP8OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFETAP9HOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFETAP9OVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXDFEUTHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFEUTOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXDFEVPHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFEVPOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXDFEVSEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXDFEXYDEN_VAL bound to: 1'b1 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXDLYBYPASS_VAL bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RXDLYEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXDLYOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXDLYSRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXELECIDLEMODE_VAL bound to: 2'b11 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXGEARBOXSLIP_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RXLATCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RXLPMEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXLPMGCHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXLPMGCOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXLPMHFHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXLPMHFOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXLPMLFHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXLPMLFKLOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXLPMOSHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXLPMOSOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXMCOMMAALIGNEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXMONITORSEL_VAL bound to: 2'b00 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXOOBRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXOSCALRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RXOSHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOSINTCFG_VAL bound to: 4'b1101 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXOSINTEN_VAL bound to: 1'b1 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXOSINTHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOSINTOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOSINTSTROBE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXOSINTTESTOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXOSOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOUTCLKSEL_VAL bound to: 3'b010 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXPCOMMAALIGNEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXPCSRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_RXPD_VAL bound to: 2'b00 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXPHALIGN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXPHALIGNEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXPHDLYPD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXPHDLYRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXPHOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXPLLCLKSEL_VAL bound to: 2'b00 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXPMARESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXPOLARITY_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXPRBSCNTRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXPRBSSEL_VAL bound to: 4'b0000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXPROGDIVRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RXQPIEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RXRATE_VAL bound to: 3'b000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXRATEMODE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RXSLIDE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXSLIPOUTCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXSLIPPMA_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXSYNCALLIN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RXSYNCIN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXSYNCMODE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXSYSCLKSEL_VAL bound to: 2'b00 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXUSERRDY_VAL bound to: 1'b1 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_RXUSRCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXUSRCLK2_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_SIGVALIDCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter GTHE3_CHANNEL_TSTIN_VAL bound to: 20'b00000000000000000000 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_TX8B10BBYPASS_VAL bound to: 8'b00000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TX8B10BEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXBUFDIFFCTRL_VAL bound to: 3'b000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXCOMINIT_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXCOMSAS_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXCOMWAKE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_TXCTRL0_VAL bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_TXCTRL1_VAL bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXCTRL2_VAL bound to: 8'b00000000 
2default:defaulth p
x
� 
�
%s
*synth2�
�	Parameter GTHE3_CHANNEL_TXDATA_VAL bound to: 128'b00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter GTHE3_CHANNEL_TXDATAEXTENDRSVD_VAL bound to: 8'b00000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXDEEMPH_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXDETECTRX_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXDIFFCTRL_VAL bound to: 4'b1100 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXDIFFPD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXDLYBYPASS_VAL bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_TXDLYEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXDLYHOLD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXDLYOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXDLYSRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXDLYUPDOWN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXELECIDLE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXHEADER_VAL bound to: 6'b000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXINHIBIT_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXLATCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TXMAINCURSOR_VAL bound to: 7'b1000000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXMARGIN_VAL bound to: 3'b000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXOUTCLKSEL_VAL bound to: 3'b101 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXPCSRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter GTHE3_CHANNEL_TXPD_VAL bound to: 2'b00 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXPDELECIDLEMODE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXPHALIGN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXPHALIGNEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXPHDLYPD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPHDLYRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXPHDLYTSTCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXPHINIT_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXPHOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXPIPPMEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXPIPPMOVRDEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXPIPPMPD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXPIPPMSEL_VAL bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_TXPIPPMSTEPSIZE_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXPISOPD_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPLLCLKSEL_VAL bound to: 2'b10 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXPMARESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXPOLARITY_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXPOSTCURSOR_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXPOSTCURSORINV_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPRBSFORCEERR_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPRBSSEL_VAL bound to: 4'b0000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXPRECURSOR_VAL bound to: 5'b00000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPRECURSORINV_VAL bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPROGDIVRESET_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXQPIBIASEN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXQPISTRONGPDOWN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXQPIWEAKPUP_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXRATE_VAL bound to: 3'b000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXRATEMODE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXSEQUENCE_VAL bound to: 7'b0000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_TXSWING_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXSYNCALLIN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXSYNCIN_VAL bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXSYNCMODE_VAL bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXSYSCLKSEL_VAL bound to: 2'b11 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXUSERRDY_VAL bound to: 1'b1 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TXUSRCLK_VAL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXUSRCLK2_VAL bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_CFGRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_CLKRSVD0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_CLKRSVD1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_CPLLLOCKDETCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_CPLLLOCKEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_CPLLPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_CPLLREFCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_CPLLRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_DMONFIFORESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_DMONITORCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_DRPADDR_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_DRPCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_DRPDI_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_DRPEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_DRPWE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_EVODDPHICALDONE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_EVODDPHICALSTART_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_EVODDPHIDRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_EVODDPHIDWREN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_EVODDPHIXRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_EVODDPHIXWREN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_EYESCANMODE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_EYESCANRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_EYESCANTRIGGER_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_GTGREFCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTHRXN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTHRXP_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_GTNORTHREFCLK0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_GTNORTHREFCLK1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_GTREFCLK0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_GTREFCLK1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_GTRESETSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_GTRSVD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_GTRXRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_GTSOUTHREFCLK0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_GTSOUTHREFCLK1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_GTTXRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_LOOPBACK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_LPBKRXTXSEREN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_LPBKTXRXSEREN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter GTHE3_CHANNEL_PCIEEQRXEQADAPTDONE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_PCIERSTIDLE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter GTHE3_CHANNEL_PCIERSTTXSYNCSTART_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_PCIEUSERRATEDONE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_PCSRSVDIN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_PCSRSVDIN2_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_PMARSVDIN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_QPLL0CLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_QPLL0REFCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_QPLL1CLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_QPLL1REFCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RESETOVRD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RSTCLKENTX_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RX8B10BEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXBUFRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXCDRFREQRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXCDRHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXCDROVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXCDRRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXCDRRESETRSV_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXCHBONDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXCHBONDI_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXCHBONDLEVEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXCHBONDMASTER_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXCHBONDSLAVE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXCOMMADETEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFEAGCCTRL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXDFEAGCHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXDFEAGCOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFELFHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFELFOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFELPMRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXDFETAP10HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXDFETAP10OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXDFETAP11HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXDFETAP11OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXDFETAP12HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXDFETAP12OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXDFETAP13HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXDFETAP13OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXDFETAP14HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXDFETAP14OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXDFETAP15HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_RXDFETAP15OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP2HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP2OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP3HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP3OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP4HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP4OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP5HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP5OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP6HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP6OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP7HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP7OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP8HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP8OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFETAP9HOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXDFETAP9OVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFEUTHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFEUTOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDFEVPHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXDFEVPOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXDFEVSEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXDFEXYDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDLYBYPASS_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXDLYEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDLYOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXDLYSRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXELECIDLEMODE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXGEARBOXSLIP_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXLATCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXLPMEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXLPMGCHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXLPMGCOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXLPMHFHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXLPMHFOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXLPMLFHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXLPMLFKLOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXLPMOSHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXLPMOSOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXMCOMMAALIGNEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXMONITORSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOOBRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXOSCALRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXOSHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOSINTCFG_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXOSINTEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXOSINTHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXOSINTOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_RXOSINTSTROBE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter GTHE3_CHANNEL_RXOSINTTESTOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXOSOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXOUTCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_RXPCOMMAALIGNEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXPCSRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_RXPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXPHALIGN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXPHALIGNEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXPHDLYPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXPHDLYRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXPHOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXPLLCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXPMARESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXPOLARITY_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXPRBSCNTRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXPRBSSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_RXPROGDIVRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXQPIEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_RXRATE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXRATEMODE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_RXSLIDE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_RXSLIPOUTCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXSLIPPMA_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXSYNCALLIN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXSYNCIN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_RXSYNCMODE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_RXSYSCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXUSERRDY_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_RXUSRCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_RXUSRCLK2_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_SIGVALIDCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter GTHE3_CHANNEL_TSTIN_TIE_EN bound to: 1'b1 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TX8B10BBYPASS_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TX8B10BEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXBUFDIFFCTRL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXCOMINIT_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXCOMSAS_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXCOMWAKE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXCTRL0_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXCTRL1_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXCTRL2_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXDATA_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_TXDATAEXTENDRSVD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXDEEMPH_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXDETECTRX_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXDIFFCTRL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXDIFFPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXDLYBYPASS_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXDLYEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXDLYHOLD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXDLYOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXDLYSRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXDLYUPDOWN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXELECIDLE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXHEADER_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXINHIBIT_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXLATCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXMAINCURSOR_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXMARGIN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXOUTCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXPCSRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter GTHE3_CHANNEL_TXPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_TXPDELECIDLEMODE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPHALIGN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPHALIGNEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPHDLYPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXPHDLYRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXPHDLYTSTCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXPHINIT_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXPHOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPIPPMEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter GTHE3_CHANNEL_TXPIPPMOVRDEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPIPPMPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXPIPPMSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TXPIPPMSTEPSIZE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXPISOPD_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPLLCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXPMARESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXPOLARITY_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXPOSTCURSOR_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter GTHE3_CHANNEL_TXPOSTCURSORINV_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TXPRBSFORCEERR_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXPRBSSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXPRECURSOR_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TXPRECURSORINV_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter GTHE3_CHANNEL_TXPROGDIVRESET_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXQPIBIASEN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter GTHE3_CHANNEL_TXQPISTRONGPDOWN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter GTHE3_CHANNEL_TXQPIWEAKPUP_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter GTHE3_CHANNEL_TXRATE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXRATEMODE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXSEQUENCE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter GTHE3_CHANNEL_TXSWING_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXSYNCALLIN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXSYNCIN_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter GTHE3_CHANNEL_TXSYNCMODE_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter GTHE3_CHANNEL_TXSYSCLKSEL_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXUSERRDY_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter GTHE3_CHANNEL_TXUSRCLK_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter GTHE3_CHANNEL_TXUSRCLK2_TIE_EN bound to: 1'b0 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2!
GTHE3_CHANNEL2default:default2
 2default:default2P
:D:/apps/xilinx/Vivado/2019.2/scripts/rt/data/unisim_comp.v2default:default2
156362default:default8@Z8-6157h px� 
Y
%s
*synth2A
-	Parameter ACJTAG_DEBUG_MODE bound to: 1'b0 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter ACJTAG_MODE bound to: 1'b0 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter ACJTAG_RESET bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ADAPT_CFG0 bound to: 16'b1111100000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ADAPT_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter ALIGN_COMMA_DOUBLE bound to: FALSE - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter ALIGN_COMMA_ENABLE bound to: 10'b0000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ALIGN_COMMA_WORD bound to: 1 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter ALIGN_MCOMMA_DET bound to: FALSE - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter ALIGN_MCOMMA_VALUE bound to: 10'b1010000011 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter ALIGN_PCOMMA_DET bound to: FALSE - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter ALIGN_PCOMMA_VALUE bound to: 10'b0101111100 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter A_RXOSCALRESET bound to: 1'b0 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter A_RXPROGDIVRESET bound to: 1'b0 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter A_TXPROGDIVRESET bound to: 1'b0 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter CBCC_DATA_SOURCE_SEL bound to: ENCODED - type: string 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter CDR_SWAP_MODE_EN bound to: 1'b0 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter CHAN_BOND_KEEP_ALIGN bound to: FALSE - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter CHAN_BOND_MAX_SKEW bound to: 1 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_1_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_1_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_1_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_1_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CHAN_BOND_SEQ_1_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_2_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_2_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_2_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CHAN_BOND_SEQ_2_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CHAN_BOND_SEQ_2_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CHAN_BOND_SEQ_2_USE bound to: FALSE - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CHAN_BOND_SEQ_LEN bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter CLK_CORRECT_USE bound to: FALSE - type: string 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter CLK_COR_KEEP_IDLE bound to: FALSE - type: string 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CLK_COR_MAX_LAT bound to: 20 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CLK_COR_MIN_LAT bound to: 18 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter CLK_COR_PRECEDENCE bound to: TRUE - type: string 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter CLK_COR_REPEAT_WAIT bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_1_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_1_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_1_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_1_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter CLK_COR_SEQ_1_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_2_1 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_2_2 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_2_3 bound to: 10'b0000000000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CLK_COR_SEQ_2_4 bound to: 10'b0000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter CLK_COR_SEQ_2_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter CLK_COR_SEQ_2_USE bound to: FALSE - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CLK_COR_SEQ_LEN bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CPLL_CFG0 bound to: 16'b0110011111111000 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CPLL_CFG1 bound to: 16'b1010010010101100 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter CPLL_CFG2 bound to: 16'b0101000000000111 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter CPLL_CFG3 bound to: 6'b000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter CPLL_FBDIV bound to: 3 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter CPLL_FBDIV_45 bound to: 5 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CPLL_INIT_CFG0 bound to: 16'b0000001010110010 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter CPLL_INIT_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter CPLL_LOCK_CFG bound to: 16'b0000000111101000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter CPLL_REFCLK_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter DDI_CTRL bound to: 2'b00 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter DDI_REALIGN_WAIT bound to: 15 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter DEC_MCOMMA_DETECT bound to: FALSE - type: string 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter DEC_PCOMMA_DETECT bound to: FALSE - type: string 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter DEC_VALID_COMMA_ONLY bound to: FALSE - type: string 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter DFE_D_X_REL_POS bound to: 1'b0 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter DFE_VCM_COMP_EN bound to: 1'b0 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter DMONITOR_CFG0 bound to: 10'b0000000000 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter DMONITOR_CFG1 bound to: 8'b00000000 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter ES_CLK_PHASE_SEL bound to: 1'b0 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter ES_CONTROL bound to: 6'b000000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter ES_ERRDET_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter ES_EYE_SCAN_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ES_HORZ_OFFSET bound to: 12'b000000000000 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter ES_PMA_CFG bound to: 10'b0000000000 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter ES_PRESCALE bound to: 5'b00000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUALIFIER0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUALIFIER1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUALIFIER2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUALIFIER3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUALIFIER4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUAL_MASK0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUAL_MASK1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUAL_MASK2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUAL_MASK3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter ES_QUAL_MASK4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter ES_SDATA_MASK0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter ES_SDATA_MASK1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter ES_SDATA_MASK2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter ES_SDATA_MASK3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter ES_SDATA_MASK4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EVODD_PHI_CFG bound to: 11'b00000000000 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter EYE_SCAN_SWAP_EN bound to: 1'b0 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FTS_DESKEW_SEQ_ENABLE bound to: 4'b1111 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter FTS_LANE_DESKEW_CFG bound to: 4'b1111 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter FTS_LANE_DESKEW_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter GEARBOX_MODE bound to: 5'b00000 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter GM_BIAS_SELECT bound to: 1'b0 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter LOCAL_MASTER bound to: 1'b1 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter OOBDIVCTL bound to: 2'b00 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter OOB_PWRUP bound to: 1'b0 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter PCI3_AUTO_REALIGN bound to: OVR_1K_BLK - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter PCI3_PIPE_RX_ELECIDLE bound to: 1'b0 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PCI3_RX_ASYNC_EBUF_BYPASS bound to: 2'b00 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter PCI3_RX_ELECIDLE_EI2_ENABLE bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter PCI3_RX_ELECIDLE_H2L_COUNT bound to: 6'b000000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PCI3_RX_ELECIDLE_H2L_DISABLE bound to: 3'b000 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PCI3_RX_ELECIDLE_HI_COUNT bound to: 6'b000000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter PCI3_RX_ELECIDLE_LP4_DISABLE bound to: 1'b0 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter PCI3_RX_FIFO_DISABLE bound to: 1'b0 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter PCIE_BUFG_DIV_CTRL bound to: 16'b0001000000000000 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter PCIE_RXPCS_CFG_GEN3 bound to: 16'b0000001010100100 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PCIE_RXPMA_CFG bound to: 16'b0000000000001010 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter PCIE_TXPCS_CFG_GEN3 bound to: 16'b0010010010100100 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PCIE_TXPMA_CFG bound to: 16'b0000000000001010 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter PCS_PCIE_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter PCS_RSVD0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter PCS_RSVD1 bound to: 3'b000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter PD_TRANS_TIME_FROM_P2 bound to: 12'b000000111100 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter PD_TRANS_TIME_NONE_P2 bound to: 8'b00011001 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PD_TRANS_TIME_TO_P2 bound to: 8'b01100100 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter PLL_SEL_MODE_GEN12 bound to: 2'b00 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter PLL_SEL_MODE_GEN3 bound to: 2'b11 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PMA_RSV1 bound to: 16'b1111000000000000 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter PROCESS_PAR bound to: 3'b010 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter RATE_SW_USE_DRP bound to: 1'b1 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RESET_POWERSAVE_DISABLE bound to: 1'b0 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RXBUFRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter RXBUF_ADDR_MODE bound to: FAST - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RXBUF_EIDLE_HI_CNT bound to: 4'b1000 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RXBUF_EIDLE_LO_CNT bound to: 4'b0000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter RXBUF_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter RXBUF_RESET_ON_CB_CHANGE bound to: TRUE - type: string 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter RXBUF_RESET_ON_COMMAALIGN bound to: FALSE - type: string 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter RXBUF_RESET_ON_EIDLE bound to: FALSE - type: string 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter RXBUF_RESET_ON_RATE_CHANGE bound to: TRUE - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXBUF_THRESH_OVFLW bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter RXBUF_THRESH_OVRD bound to: FALSE - type: string 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter RXBUF_THRESH_UNDFLW bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RXCDRFREQRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RXCDRPHRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXCDR_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_CFG0_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXCDR_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_CFG1_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXCDR_CFG2 bound to: 16'b0000011111100110 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_CFG2_GEN3 bound to: 16'b0000011111100110 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXCDR_CFG3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_CFG3_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXCDR_CFG4 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_CFG4_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXCDR_CFG5 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_CFG5_GEN3 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RXCDR_FR_RESET_ON_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RXCDR_HOLD_DURING_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_LOCK_CFG0 bound to: 16'b0100010010000000 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_LOCK_CFG1 bound to: 16'b0101111111111111 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter RXCDR_LOCK_CFG2 bound to: 16'b0111011111000011 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RXCDR_PH_RESET_ON_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RXCFOK_CFG0 bound to: 16'b0100000000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RXCFOK_CFG1 bound to: 16'b0000000001100101 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RXCFOK_CFG2 bound to: 16'b0000000000101110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter RXDFELPMRESET_TIME bound to: 7'b0001111 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter RXDFELPM_KL_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter RXDFELPM_KL_CFG1 bound to: 16'b0000000000000010 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter RXDFELPM_KL_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXDFE_CFG0 bound to: 16'b0000101000000000 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXDFE_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_GC_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_GC_CFG1 bound to: 16'b0111100001110000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_GC_CFG2 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H2_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H2_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H3_CFG0 bound to: 16'b0100000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H3_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H4_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H4_CFG1 bound to: 16'b0000000000000011 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H5_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H5_CFG1 bound to: 16'b0000000000000011 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H6_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H6_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H7_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H7_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H8_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H8_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H9_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_H9_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HA_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HA_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HB_CFG0 bound to: 16'b0010000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HB_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HC_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HC_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HD_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HD_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HE_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HE_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HF_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_HF_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_OS_CFG0 bound to: 16'b1000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_OS_CFG1 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_UT_CFG0 bound to: 16'b1000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_UT_CFG1 bound to: 16'b0000000000000011 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_VP_CFG0 bound to: 16'b1010101000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXDFE_VP_CFG1 bound to: 16'b0000000000110011 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter RXDLY_CFG bound to: 16'b0000000000011111 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXDLY_LCFG bound to: 16'b0000000000110000 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter RXELECIDLE_CFG bound to: Sigcfg_4 - type: string 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter RXGBOX_FIFO_INIT_RD_ADDR bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter RXGEARBOX_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RXISCANRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter RXLPM_CFG bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter RXLPM_GC_CFG bound to: 16'b0001000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXLPM_KH_CFG0 bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXLPM_KH_CFG1 bound to: 16'b0000000000000010 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXLPM_OS_CFG0 bound to: 16'b1000000000000000 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RXLPM_OS_CFG1 bound to: 16'b0000000000000010 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RXOOB_CFG bound to: 9'b000000110 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RXOOB_CLK_CFG bound to: PMA - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RXOSCALRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RXOUT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RXPCSRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter RXPHBEACON_CFG bound to: 16'b0000000000000000 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RXPHDLY_CFG bound to: 16'b0010000000100000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter RXPHSAMP_CFG bound to: 16'b0010000100000000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter RXPHSLIP_CFG bound to: 16'b0110011000100010 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter RXPH_MONITOR_SEL bound to: 5'b00000 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter RXPI_CFG0 bound to: 2'b00 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter RXPI_CFG1 bound to: 2'b00 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter RXPI_CFG2 bound to: 2'b00 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter RXPI_CFG3 bound to: 2'b00 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter RXPI_CFG4 bound to: 1'b1 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter RXPI_CFG5 bound to: 1'b1 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter RXPI_CFG6 bound to: 3'b011 
2default:defaulth p
x
� 
P
%s
*synth28
$	Parameter RXPI_LPM bound to: 1'b0 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter RXPI_VREFSEL bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RXPMACLK_SEL bound to: DATA - type: string 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RXPMARESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RXPRBS_ERR_LOOPBACK bound to: 1'b0 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter RXPRBS_LINKACQ_CNT bound to: 15 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter RXSLIDE_AUTO_WAIT bound to: 7 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RXSLIDE_MODE bound to: PCS - type: string 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter RXSYNC_MULTILANE bound to: 1'b1 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter RXSYNC_OVRD bound to: 1'b0 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter RXSYNC_SKIP_DA bound to: 1'b0 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter RX_AFE_CM_EN bound to: 1'b0 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter RX_BIAS_CFG0 bound to: 16'b0000101010110100 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter RX_BUFFER_CFG bound to: 6'b000000 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RX_CAPFF_SARC_ENB bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter RX_CLK25_DIV bound to: 7 - type: integer 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter RX_CLKMUX_EN bound to: 1'b1 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter RX_CLK_SLIP_OVRD bound to: 5'b00000 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter RX_CM_BUF_CFG bound to: 4'b1010 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter RX_CM_BUF_PD bound to: 1'b0 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter RX_CM_SEL bound to: 2'b11 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter RX_CM_TRIM bound to: 4'b1010 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RX_CTLE3_LPF bound to: 8'b00000001 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RX_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter RX_DDI_SEL bound to: 6'b000000 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter RX_DEFER_RESET_BUF_EN bound to: TRUE - type: string 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RX_DFELPM_CFG0 bound to: 4'b0110 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter RX_DFELPM_CFG1 bound to: 1'b1 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RX_DFELPM_KLKH_AGC_STUP_EN bound to: 1'b1 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter RX_DFE_AGC_CFG0 bound to: 2'b10 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RX_DFE_AGC_CFG1 bound to: 3'b100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RX_DFE_KL_LPM_KH_CFG0 bound to: 2'b01 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RX_DFE_KL_LPM_KH_CFG1 bound to: 3'b100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RX_DFE_KL_LPM_KL_CFG0 bound to: 2'b01 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RX_DFE_KL_LPM_KL_CFG1 bound to: 3'b100 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter RX_DFE_LPM_HOLD_DURING_EIDLE bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter RX_DISPERR_SEQ_MATCH bound to: TRUE - type: string 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter RX_DIVRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter RX_EN_HI_LR bound to: 1'b0 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter RX_EYESCAN_VS_CODE bound to: 7'b0000000 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RX_EYESCAN_VS_NEG_DIR bound to: 1'b0 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter RX_EYESCAN_VS_RANGE bound to: 2'b00 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RX_EYESCAN_VS_UT_SIGN bound to: 1'b0 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter RX_FABINT_USRCLK_FLOP bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter RX_INT_DATAWIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RX_PMA_POWER_SAVE bound to: 1'b0 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter RX_PROGDIV_CFG bound to: 0.000000 - type: double 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter RX_SAMPLE_PERIOD bound to: 3'b111 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter RX_SIG_VALID_DLY bound to: 11 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RX_SUM_DFETAPREP_EN bound to: 1'b0 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RX_SUM_IREF_TUNE bound to: 4'b0000 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter RX_SUM_RES_CTRL bound to: 2'b00 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter RX_SUM_VCMTUNE bound to: 4'b0000 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter RX_SUM_VCM_OVWR bound to: 1'b0 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter RX_SUM_VREF_TUNE bound to: 3'b000 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter RX_TUNE_AFE_OS bound to: 2'b10 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter RX_WIDEMODE_CDR bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter RX_XCLK_SEL bound to: RXUSR - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter SAS_MAX_COM bound to: 64 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter SAS_MIN_COM bound to: 36 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter SATA_BURST_SEQ_LEN bound to: 4'b1110 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter SATA_BURST_VAL bound to: 3'b100 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter SATA_CPLL_CFG bound to: VCO_3000MHZ - type: string 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter SATA_EIDLE_VAL bound to: 3'b100 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter SATA_MAX_BURST bound to: 8 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter SATA_MAX_INIT bound to: 21 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SATA_MAX_WAKE bound to: 7 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter SATA_MIN_BURST bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter SATA_MIN_INIT bound to: 12 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SATA_MIN_WAKE bound to: 4 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter SHOW_REALIGN_COMMA bound to: FALSE - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter SIM_MODE bound to: FAST - type: string 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter SIM_RECEIVER_DETECT_PASS bound to: TRUE - type: string 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SIM_RESET_SPEEDUP bound to: TRUE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SIM_TX_EIDLE_DRIVE_LEVEL bound to: 1'b0 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SIM_VERSION bound to: 2 - type: integer 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter TAPDLY_SET_TX bound to: 2'b00 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TEMPERATUR_PAR bound to: 4'b0010 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter TERM_RCAL_CFG bound to: 15'b100001000010000 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter TERM_RCAL_OVRD bound to: 3'b000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter TRANS_TIME_RATE bound to: 8'b00001110 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter TST_RSV0 bound to: 8'b00000000 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter TST_RSV1 bound to: 8'b00000000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter TXBUF_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
q
%s
*synth2Y
E	Parameter TXBUF_RESET_ON_RATE_CHANGE bound to: TRUE - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter TXDLY_CFG bound to: 16'b0000000000001001 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter TXDLY_LCFG bound to: 16'b0000000001010000 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter TXDRVBIAS_N bound to: 4'b1010 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter TXDRVBIAS_P bound to: 4'b1010 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter TXFIFO_ADDR_CFG bound to: LOW - type: string 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter TXGBOX_FIFO_INIT_RD_ADDR bound to: 4 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter TXGEARBOX_EN bound to: FALSE - type: string 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter TXOUT_DIV bound to: 1 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter TXPCSRESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter TXPHDLY_CFG0 bound to: 16'b0010000000100000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter TXPHDLY_CFG1 bound to: 16'b0000000001110101 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter TXPH_CFG bound to: 16'b0000100110000000 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter TXPH_MONITOR_SEL bound to: 5'b00000 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter TXPI_CFG0 bound to: 2'b00 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter TXPI_CFG1 bound to: 2'b00 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter TXPI_CFG2 bound to: 2'b00 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter TXPI_CFG3 bound to: 1'b1 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter TXPI_CFG4 bound to: 1'b1 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter TXPI_CFG5 bound to: 3'b000 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter TXPI_GRAY_SEL bound to: 1'b0 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TXPI_INVSTROBE_SEL bound to: 1'b1 
2default:defaulth p
x
� 
P
%s
*synth28
$	Parameter TXPI_LPM bound to: 1'b0 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter TXPI_PPMCLK_SEL bound to: TXUSRCLK2 - type: string 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter TXPI_PPM_CFG bound to: 8'b00000000 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TXPI_SYNFREQ_PPM bound to: 3'b001 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter TXPI_VREFSEL bound to: 1'b0 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter TXPMARESET_TIME bound to: 5'b00011 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter TXSYNC_MULTILANE bound to: 1'b1 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter TXSYNC_OVRD bound to: 1'b0 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter TXSYNC_SKIP_DA bound to: 1'b0 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter TX_CLK25_DIV bound to: 7 - type: integer 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter TX_CLKMUX_EN bound to: 1'b1 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter TX_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter TX_DCD_CFG bound to: 6'b000010 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter TX_DCD_EN bound to: 1'b0 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter TX_DEEMPH0 bound to: 6'b000000 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter TX_DEEMPH1 bound to: 6'b000000 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter TX_DIVRESET_TIME bound to: 5'b00001 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter TX_DRIVE_MODE bound to: DIRECT - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter TX_EIDLE_ASSERT_DELAY bound to: 3'b100 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter TX_EIDLE_DEASSERT_DELAY bound to: 3'b011 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter TX_EML_PHI_TUNE bound to: 1'b0 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter TX_FABINT_USRCLK_FLOP bound to: 1'b0 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TX_IDLE_DATA_ZERO bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter TX_INT_DATAWIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter TX_LOOPBACK_DRIVE_HIZ bound to: FALSE - type: string 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TX_MAINCURSOR_SEL bound to: 1'b0 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter TX_MARGIN_FULL_0 bound to: 7'b1001111 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter TX_MARGIN_FULL_1 bound to: 7'b1001110 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter TX_MARGIN_FULL_2 bound to: 7'b1001100 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter TX_MARGIN_FULL_3 bound to: 7'b1001010 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter TX_MARGIN_FULL_4 bound to: 7'b1001000 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter TX_MARGIN_LOW_0 bound to: 7'b1000110 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter TX_MARGIN_LOW_1 bound to: 7'b1000101 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter TX_MARGIN_LOW_2 bound to: 7'b1000011 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter TX_MARGIN_LOW_3 bound to: 7'b1000010 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter TX_MARGIN_LOW_4 bound to: 7'b1000000 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter TX_MODE_SEL bound to: 3'b000 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter TX_PMADATA_OPT bound to: 1'b0 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TX_PMA_POWER_SAVE bound to: 1'b0 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter TX_PROGCLK_SEL bound to: PREPI - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter TX_PROGDIV_CFG bound to: 20.000000 - type: double 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter TX_QPI_STATUS_EN bound to: 1'b0 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter TX_RXDETECT_CFG bound to: 14'b00000000110010 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TX_RXDETECT_REF bound to: 3'b100 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TX_SAMPLE_PERIOD bound to: 3'b111 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter TX_SARC_LPBK_ENB bound to: 1'b0 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter TX_XCLK_SEL bound to: TXUSR - type: string 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter USE_PCS_CLK_PHASE_SEL bound to: 1'b0 
2default:defaulth p
x
� 
P
%s
*synth28
$	Parameter WB_MODE bound to: 2'b00 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
GTHE3_CHANNEL2default:default2
 2default:default2
42default:default2
12default:default2P
:D:/apps/xilinx/Vivado/2019.2/scripts/rt/data/unisim_comp.v2default:default2
156362default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2
 2default:default2
52default:default2
12default:default2f
Px:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/gtwizard_ultrascale_v1_7_gthe3_channel.v2default:default2
552default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys27
#xlx_ku_mgt_ip_gthe3_channel_wrapper2default:default2
 2default:default2
62default:default2
12default:default2c
Mx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gthe3_channel_wrapper.v2default:default2
562default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_tx2default:default2
 2default:default2j
Tx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_tx.v2default:default2
552default:default8@Z8-6157h px� 
i
%s
*synth2Q
=	Parameter P_BUFFER_BYPASS_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter P_TOTAL_NUMBER_OF_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter P_MASTER_CHANNEL_POINTER bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_BUFFBYPASS_TX_IDLE bound to: 2'b00 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter ST_BUFFBYPASS_TX_DEASSERT_TXDLYSRESET bound to: 2'b01 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter ST_BUFFBYPASS_TX_WAIT_TXSYNCDONE bound to: 2'b10 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_BUFFBYPASS_TX_DONE bound to: 2'b11 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2E
1gtwizard_ultrascale_v1_7_7_reset_inv_synchronizer2default:default2
 2default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v2default:default2
552default:default8@Z8-6157h px� 
`
%s
*synth2H
4	Parameter FREQUENCY bound to: 512 - type: integer 
2default:defaulth p
x
� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v2default:default2
722default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v2default:default2
732default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v2default:default2
742default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v2default:default2
752default:default8@Z8-5534h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2E
1gtwizard_ultrascale_v1_7_7_reset_inv_synchronizer2default:default2
 2default:default2
72default:default2
12default:default2e
Ox:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v2default:default2
552default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2?
+gtwizard_ultrascale_v1_7_7_bit_synchronizer2default:default2
 2default:default2_
Ix:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_bit_sync.v2default:default2
552default:default8@Z8-6157h px� 
V
%s
*synth2>
*	Parameter INITIALIZE bound to: 5'b00000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FREQUENCY bound to: 512 - type: integer 
2default:defaulth p
x
� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2_
Ix:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_bit_sync.v2default:default2
712default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2_
Ix:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_bit_sync.v2default:default2
722default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2_
Ix:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_bit_sync.v2default:default2
732default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2_
Ix:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_bit_sync.v2default:default2
742default:default8@Z8-5534h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2?
+gtwizard_ultrascale_v1_7_7_bit_synchronizer2default:default2
 2default:default2
82default:default2
12default:default2_
Ix:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_bit_sync.v2default:default2
552default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_tx2default:default2
 2default:default2
92default:default2
12default:default2j
Tx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_tx.v2default:default2
552default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_rx2default:default2
 2default:default2j
Tx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_rx.v2default:default2
552default:default8@Z8-6157h px� 
i
%s
*synth2Q
=	Parameter P_BUFFER_BYPASS_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter P_TOTAL_NUMBER_OF_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter P_MASTER_CHANNEL_POINTER bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_BUFFBYPASS_RX_IDLE bound to: 2'b00 
2default:defaulth p
x
� 
n
%s
*synth2V
B	Parameter ST_BUFFBYPASS_RX_DEASSERT_RXDLYSRESET bound to: 2'b01 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter ST_BUFFBYPASS_RX_WAIT_RXSYNCDONE bound to: 2'b10 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_BUFFBYPASS_RX_DONE bound to: 2'b11 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_rx2default:default2
 2default:default2
102default:default2
12default:default2j
Tx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_rx.v2default:default2
552default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:default2
 2default:default2b
Lx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_reset.v2default:default2
552default:default8@Z8-6157h px� 
p
%s
*synth2X
D	Parameter P_FREERUN_FREQUENCY bound to: 120.240000 - type: double 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter P_USE_CPLL_CAL bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter P_TX_PLL_TYPE bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter P_RX_PLL_TYPE bound to: 2 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter P_RX_LINE_RATE bound to: 4.809600 - type: double 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter P_CDR_TIMEOUT_FREERUN_CYC bound to: 26'b00000011100001110101001000 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter ST_RESET_ALL_INIT bound to: 3'b000 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter ST_RESET_ALL_BRANCH bound to: 3'b001 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter ST_RESET_ALL_TX_PLL bound to: 3'b010 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ST_RESET_ALL_TX_PLL_WAIT bound to: 3'b011 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter ST_RESET_ALL_RX_DP bound to: 3'b100 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter ST_RESET_ALL_RX_PLL bound to: 3'b101 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_RESET_ALL_RX_WAIT bound to: 3'b110 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter ST_RESET_ALL_DONE bound to: 3'b111 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter P_TX_PLL_RESET_FREERUN_CYC bound to: 10'b0000000111 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter ST_RESET_TX_BRANCH bound to: 3'b000 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter ST_RESET_TX_PLL bound to: 3'b001 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_RESET_TX_DATAPATH bound to: 3'b010 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter ST_RESET_TX_WAIT_LOCK bound to: 3'b011 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ST_RESET_TX_WAIT_USERRDY bound to: 3'b100 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter ST_RESET_TX_WAIT_RESETDONE bound to: 3'b101 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ST_RESET_TX_IDLE bound to: 3'b110 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter P_RX_PLL_RESET_FREERUN_CYC bound to: 10'b0011110010 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter ST_RESET_RX_BRANCH bound to: 3'b000 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter ST_RESET_RX_PLL bound to: 3'b001 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_RESET_RX_DATAPATH bound to: 3'b010 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter ST_RESET_RX_WAIT_LOCK bound to: 3'b011 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ST_RESET_RX_WAIT_CDR bound to: 3'b100 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter ST_RESET_RX_WAIT_USERRDY bound to: 3'b101 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter ST_RESET_RX_WAIT_RESETDONE bound to: 3'b110 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ST_RESET_RX_IDLE bound to: 3'b111 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2A
-gtwizard_ultrascale_v1_7_7_reset_synchronizer2default:default2
 2default:default2a
Kx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_sync.v2default:default2
552default:default8@Z8-6157h px� 
`
%s
*synth2H
4	Parameter FREQUENCY bound to: 512 - type: integer 
2default:defaulth p
x
� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2a
Kx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_sync.v2default:default2
722default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2a
Kx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_sync.v2default:default2
732default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2a
Kx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_sync.v2default:default2
742default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
	ASYNC_REG2default:default2
TRUE2default:default2a
Kx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_sync.v2default:default2
752default:default8@Z8-5534h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2A
-gtwizard_ultrascale_v1_7_7_reset_synchronizer2default:default2
 2default:default2
112default:default2
12default:default2a
Kx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_reset_sync.v2default:default2
552default:default8@Z8-6155h px� 
�
-case statement is not full and has no default155*oasys2b
Lx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_reset.v2default:default2
1642default:default8@Z8-155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:default2
 2default:default2
122default:default2
12default:default2b
Lx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_reset.v2default:default2
552default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_tx2default:default2
 2default:default2h
Rx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_tx.v2default:default2
552default:default8@Z8-6157h px� 
j
%s
*synth2R
>	Parameter P_TX_USER_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter P_TX_DATA_ENCODING bound to: 0 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter P_TOTAL_NUMBER_OF_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_tx2default:default2
 2default:default2
132default:default2
12default:default2h
Rx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_tx.v2default:default2
552default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2
 2default:default2h
Rx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_rx.v2default:default2
552default:default8@Z8-6157h px� 
j
%s
*synth2R
>	Parameter P_RX_USER_DATA_WIDTH bound to: 40 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter P_RX_DATA_DECODING bound to: 0 - type: integer 
2default:defaulth p
x
� 
o
%s
*synth2W
C	Parameter P_TOTAL_NUMBER_OF_CHANNELS bound to: 3 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2
 2default:default2
142default:default2
12default:default2h
Rx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_rx.v2default:default2
552default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
xlx_ku_mgt_ip_gtwizard_gthe32default:default2
 2default:default2
152default:default2
12default:default2\
Fx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gtwizard_gthe3.v2default:default2
1432default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
xlx_ku_mgt_ip_gtwizard_top2default:default2
 2default:default2
162default:default2
12default:default2Z
Dx:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_gtwizard_top.v2default:default2
1752default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
xlx_ku_mgt_ip2default:default2
 2default:default2
172default:default2
12default:default2M
7x:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip.v2default:default2
622default:default8@Z8-6155h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[59]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[58]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[57]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[56]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[55]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[54]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[53]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[52]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[51]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[50]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[49]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[48]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[47]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[46]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[45]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[44]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[43]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[42]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[41]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[40]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[39]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[38]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[37]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[36]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[35]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[34]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[33]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[32]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[27]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[26]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[25]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[24]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[23]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[22]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[21]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[20]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2+
GTHE3_CHANNEL_TSTIN[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2<
(gtwizard_ultrascale_v1_7_7_gthe3_channel2default:default2*
GTHE3_CHANNEL_TSTIN[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2-
GTHE3_COMMON_BGBYPASSB[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default20
GTHE3_COMMON_BGMONITORENB[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2)
GTHE3_COMMON_BGPDB[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2.
GTHE3_COMMON_BGRCALOVRD[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2.
GTHE3_COMMON_BGRCALOVRD[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2.
GTHE3_COMMON_BGRCALOVRD[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2.
GTHE3_COMMON_BGRCALOVRD[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2.
GTHE3_COMMON_BGRCALOVRD[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default21
GTHE3_COMMON_BGRCALOVRDENB[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD0[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2,
GTHE3_COMMON_PMARSVD1[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2;
'gtwizard_ultrascale_v1_7_7_gthe3_common2default:default2+
GTHE3_COMMON_RCALENB[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[383]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[382]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[381]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[380]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[379]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[378]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[377]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[376]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[375]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[374]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[373]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[372]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[371]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2@
,gtwizard_ultrascale_v1_7_7_gtwiz_userdata_rx2default:default2"
rxdata_in[370]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
%s*synth2�
yFinished RTL Elaboration : Time (s): cpu = 00:00:07 ; elapsed = 00:00:07 . Memory (MB): peak = 1359.281 ; gain = 323.324
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:07 ; elapsed = 00:00:08 . Memory (MB): peak = 1359.281 ; gain = 323.324
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:07 ; elapsed = 00:00:08 . Memory (MB): peak = 1359.281 ; gain = 323.324
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.1302default:default2
1359.2812default:default2
0.0002default:defaultZ17-268h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2S
=x:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_ooc.xdc2default:default2
inst	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2S
=x:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip_ooc.xdc2default:default2
inst	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2O
9x:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip.xdc2default:default2
inst	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2O
9x:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip.xdc2default:default2
inst	2default:default8Z20-847h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2M
9x:/db6_ku_fw/src/ip/xlx_ku_mgt_ip/synth/xlx_ku_mgt_ip.xdc2default:default23
.Xil/xlx_ku_mgt_ip_propImpl.xdc2default:defaultZ1-236h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST2default:default2!
CPLLREFCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST2default:default2
RXSYSCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST2default:default2
RXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST2default:default2
TXSYSCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST2default:default2
TXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST2default:default2!
CPLLREFCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST2default:default2
RXSYSCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST2default:default2
RXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST2default:default2
TXSYSCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST2default:default2
TXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST2default:default2!
CPLLREFCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST2default:default2
RXSYSCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST2default:default2
RXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST2default:default2
TXSYSCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST2default:default2
TXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_common.gen_common_container[0].gen_enabled_common.gthe3_common_wrapper_inst/common_inst/gthe3_common_gen.GTHE3_COMMON_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_common.gen_common_container[0].gen_enabled_common.gthe3_common_wrapper_inst/common_inst/gthe3_common_gen.GTHE3_COMMON_PRIM_INST2default:default2"
QPLL1REFCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[0].GTHE3_CHANNEL_PRIM_INST2default:default2
TXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[1].GTHE3_CHANNEL_PRIM_INST2default:default2
TXOUTCLKSEL2default:default8Z38-277h px� 
�
�The instance '%s' has %s pins that are not tied constant, so the corresponding mux will select the input(s) having the worst case (highest frequency) clock(s) for automatic derivation of generated clocks184*timing2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST	�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst/channel_inst/gthe3_channel_gen.gen_gthe3_channel_inst[2].GTHE3_CHANNEL_PRIM_INST2default:default2
TXOUTCLKSEL2default:default8Z38-277h px� 
8
Deriving generated clocks
2*timingZ38-2h px� 
�
Parsing XDC File [%s]
179*designutils2h
RX:/db6_ku_fw/vivado_2019_2/vivado_2019_2.runs/xlx_ku_mgt_ip_synth_1/dont_touch.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2h
RX:/db6_ku_fw/vivado_2019_2/vivado_2019_2.runs/xlx_ku_mgt_ip_synth_1/dont_touch.xdc2default:default8Z20-178h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
1419.8012default:default2
0.0002default:defaultZ17-268h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common24
 Constraint Validation Runtime : 2default:default2
00:00:002default:default2 
00:00:00.0572default:default2
1419.8552default:default2
0.0552default:defaultZ17-268h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
Finished Constraint Validation : Time (s): cpu = 00:00:13 ; elapsed = 00:00:14 . Memory (MB): peak = 1419.855 ; gain = 383.898
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
N
%s
*synth26
"Loading part: xcku035-fbva676-1-c
2default:defaulth p
x
� 
B
 Reading net delay rules and data4578*oasysZ8-6742h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:13 ; elapsed = 00:00:14 . Memory (MB): peak = 1419.855 ; gain = 383.898
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:13 ; elapsed = 00:00:14 . Memory (MB): peak = 1419.855 ; gain = 383.898
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2S
?gen_gtwiz_buffbypass_tx_main.gen_auto_mode.sm_buffbypass_tx_reg2default:default2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_tx2default:defaultZ8-802h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2S
?gen_gtwiz_buffbypass_rx_main.gen_auto_mode.sm_buffbypass_rx_reg2default:default2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_rx2default:defaultZ8-802h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2$
sm_reset_all_reg2default:default2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:defaultZ8-802h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2#
sm_reset_tx_reg2default:default2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:defaultZ8-802h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2#
sm_reset_rx_reg2default:default2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:defaultZ8-802h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_                  iSTATE |                               00 |                               00
2default:defaulth p
x
� 
.
%s
*synth2
*
2default:defaulth p
x
� 
�
%s
*synth2�
lST_BUFFBYPASS_TX_DEASSERT_TXDLYSRESET |                               01 |                               01
2default:defaulth p
x
� 
�
%s
*synth2{
gST_BUFFBYPASS_TX_WAIT_TXSYNCDONE |                               10 |                               10
2default:defaulth p
x
� 
�
%s
*synth2s
_   ST_BUFFBYPASS_TX_DONE |                               11 |                               11
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2S
?gen_gtwiz_buffbypass_tx_main.gen_auto_mode.sm_buffbypass_tx_reg2default:default2

sequential2default:default2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_tx2default:defaultZ8-3354h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_                  iSTATE |                               00 |                               00
2default:defaulth p
x
� 
.
%s
*synth2
*
2default:defaulth p
x
� 
�
%s
*synth2�
lST_BUFFBYPASS_RX_DEASSERT_RXDLYSRESET |                               01 |                               01
2default:defaulth p
x
� 
�
%s
*synth2{
gST_BUFFBYPASS_RX_WAIT_RXSYNCDONE |                               10 |                               10
2default:defaulth p
x
� 
�
%s
*synth2s
_   ST_BUFFBYPASS_RX_DONE |                               11 |                               11
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2S
?gen_gtwiz_buffbypass_rx_main.gen_auto_mode.sm_buffbypass_rx_reg2default:default2

sequential2default:default2B
.gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_rx2default:defaultZ8-3354h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_      ST_RESET_TX_BRANCH |                              000 |                              000
2default:defaulth p
x
� 
�
%s
*synth2s
_         ST_RESET_TX_PLL |                              001 |                              001
2default:defaulth p
x
� 
�
%s
*synth2s
_    ST_RESET_TX_DATAPATH |                              010 |                              010
2default:defaulth p
x
� 
�
%s
*synth2s
_   ST_RESET_TX_WAIT_LOCK |                              011 |                              011
2default:defaulth p
x
� 
�
%s
*synth2s
_ST_RESET_TX_WAIT_USERRDY |                              100 |                              100
2default:defaulth p
x
� 
�
%s
*synth2u
aST_RESET_TX_WAIT_RESETDONE |                              101 |                              101
2default:defaulth p
x
� 
�
%s
*synth2s
_        ST_RESET_TX_IDLE |                              110 |                              110
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2#
sm_reset_tx_reg2default:default2

sequential2default:default2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:defaultZ8-3354h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_      ST_RESET_RX_BRANCH |                              000 |                              000
2default:defaulth p
x
� 
�
%s
*synth2s
_         ST_RESET_RX_PLL |                              001 |                              001
2default:defaulth p
x
� 
�
%s
*synth2s
_    ST_RESET_RX_DATAPATH |                              010 |                              010
2default:defaulth p
x
� 
�
%s
*synth2s
_   ST_RESET_RX_WAIT_LOCK |                              011 |                              011
2default:defaulth p
x
� 
�
%s
*synth2s
_    ST_RESET_RX_WAIT_CDR |                              100 |                              100
2default:defaulth p
x
� 
�
%s
*synth2s
_ST_RESET_RX_WAIT_USERRDY |                              101 |                              101
2default:defaulth p
x
� 
�
%s
*synth2u
aST_RESET_RX_WAIT_RESETDONE |                              110 |                              110
2default:defaulth p
x
� 
�
%s
*synth2s
_        ST_RESET_RX_IDLE |                              111 |                              111
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2#
sm_reset_rx_reg2default:default2

sequential2default:default2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:defaultZ8-3354h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_     ST_RESET_ALL_BRANCH |                              000 |                              001
2default:defaulth p
x
� 
�
%s
*synth2s
_     ST_RESET_ALL_TX_PLL |                              001 |                              010
2default:defaulth p
x
� 
�
%s
*synth2s
_ST_RESET_ALL_TX_PLL_WAIT |                              010 |                              011
2default:defaulth p
x
� 
�
%s
*synth2s
_      ST_RESET_ALL_RX_DP |                              011 |                              100
2default:defaulth p
x
� 
�
%s
*synth2s
_     ST_RESET_ALL_RX_PLL |                              100 |                              101
2default:defaulth p
x
� 
�
%s
*synth2s
_    ST_RESET_ALL_RX_WAIT |                              101 |                              110
2default:defaulth p
x
� 
�
%s
*synth2s
_                  iSTATE |                              110 |                              111
2default:defaulth p
x
� 
�
%s
*synth2s
_       ST_RESET_ALL_INIT |                              111 |                              000
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2$
sm_reset_all_reg2default:default2

sequential2default:default2:
&gtwizard_ultrascale_v1_7_7_gtwiz_reset2default:defaultZ8-3354h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:14 ; elapsed = 00:00:14 . Memory (MB): peak = 1419.855 ; gain = 383.898
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 192   
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      2 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input      1 Bit        Muxes := 13    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      1 Bit        Muxes := 26    
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
f
%s
*synth2N
:Module gtwizard_ultrascale_v1_7_7_reset_inv_synchronizer 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
`
%s
*synth2H
4Module gtwizard_ultrascale_v1_7_7_bit_synchronizer 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
c
%s
*synth2K
7Module gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_tx 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
c
%s
*synth2K
7Module gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_rx 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
b
%s
*synth2J
6Module gtwizard_ultrascale_v1_7_7_reset_synchronizer 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
[
%s
*synth2C
/Module gtwizard_ultrascale_v1_7_7_gtwiz_reset 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 24    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input      1 Bit        Muxes := 13    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      1 Bit        Muxes := 26    
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2o
[Part Resources:
DSPs: 1700 (col length:120)
BRAMs: 1080 (col length: RAMB18 120 RAMB36 60)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.txdlysreset_out_reg[0]2default:default2
FDRE2default:default2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.txdlysreset_out_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.rxdlysreset_out_reg[0]2default:default2
FDRE2default:default2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.rxdlysreset_out_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.txdlysreset_out_reg[1]2default:default2
FDRE2default:default2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.txdlysreset_out_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.rxdlysreset_out_reg[1]2default:default2
FDRE2default:default2�
�inst/gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst/gen_gtwizard_gthe3.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.rxdlysreset_out_reg[2]2default:defaultZ8-3886h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:15 ; elapsed = 00:00:16 . Memory (MB): peak = 1419.855 ; gain = 383.898
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:21 ; elapsed = 00:00:22 . Memory (MB): peak = 1557.453 ; gain = 521.496
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
}Finished Timing Optimization : Time (s): cpu = 00:00:21 ; elapsed = 00:00:22 . Memory (MB): peak = 1568.367 ; gain = 532.410
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Technology Mapping : Time (s): cpu = 00:00:21 ; elapsed = 00:00:22 . Memory (MB): peak = 1581.195 ; gain = 545.238
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
vFinished IO Insertion : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
K
%s*synth23
+------+--------------+------+
2default:defaulth px� 
K
%s*synth23
|      |Cell          |Count |
2default:defaulth px� 
K
%s*synth23
+------+--------------+------+
2default:defaulth px� 
K
%s*synth23
|1     |CARRY8        |     4|
2default:defaulth px� 
K
%s*synth23
|2     |GTHE3_CHANNEL |     3|
2default:defaulth px� 
K
%s*synth23
|3     |GTHE3_COMMON  |     1|
2default:defaulth px� 
K
%s*synth23
|4     |LUT1          |    14|
2default:defaulth px� 
K
%s*synth23
|5     |LUT2          |    14|
2default:defaulth px� 
K
%s*synth23
|6     |LUT3          |    27|
2default:defaulth px� 
K
%s*synth23
|7     |LUT4          |    25|
2default:defaulth px� 
K
%s*synth23
|8     |LUT5          |    28|
2default:defaulth px� 
K
%s*synth23
|9     |LUT6          |    29|
2default:defaulth px� 
K
%s*synth23
|10    |MUXF7         |     1|
2default:defaulth px� 
K
%s*synth23
|11    |FDCE          |    20|
2default:defaulth px� 
K
%s*synth23
|12    |FDPE          |    40|
2default:defaulth px� 
K
%s*synth23
|13    |FDRE          |   196|
2default:defaulth px� 
K
%s*synth23
|14    |FDSE          |     6|
2default:defaulth px� 
K
%s*synth23
+------+--------------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
�
%s
*synth2�
�+------+-----------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|      |Instance                                                                                                                     |Module                                               |Cells |
2default:defaulth p
x
� 
�
%s
*synth2�
�+------+-----------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|1     |top                                                                                                                          |                                                     |   408|
2default:defaulth p
x
� 
�
%s
*synth2�
�|2     |  inst                                                                                                                       |xlx_ku_mgt_ip_gtwizard_top                           |   408|
2default:defaulth p
x
� 
�
%s
*synth2�
�|3     |    \gen_gtwizard_gthe3_top.xlx_ku_mgt_ip_gtwizard_gthe3_inst                                                                |xlx_ku_mgt_ip_gtwizard_gthe3                         |   408|
2default:defaulth p
x
� 
�
%s
*synth2�
�|4     |      \gen_gtwizard_gthe3.gen_channel_container[0].gen_enabled_channel.gthe3_channel_wrapper_inst                            |xlx_ku_mgt_ip_gthe3_channel_wrapper                  |     3|
2default:defaulth p
x
� 
�
%s
*synth2�
�|5     |        channel_inst                                                                                                         |gtwizard_ultrascale_v1_7_7_gthe3_channel             |     3|
2default:defaulth p
x
� 
�
%s
*synth2�
�|6     |      \gen_gtwizard_gthe3.gen_common.gen_common_container[0].gen_enabled_common.gthe3_common_wrapper_inst                    |xlx_ku_mgt_ip_gthe3_common_wrapper                   |     2|
2default:defaulth p
x
� 
�
%s
*synth2�
�|7     |        common_inst                                                                                                          |gtwizard_ultrascale_v1_7_7_gthe3_common              |     2|
2default:defaulth p
x
� 
�
%s
*synth2�
�|8     |      \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_rxresetdone_inst  |gtwizard_ultrascale_v1_7_7_bit_synchronizer          |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|9     |      \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[0].bit_synchronizer_txresetdone_inst  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_0        |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|10    |      \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[1].bit_synchronizer_rxresetdone_inst  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_1        |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|11    |      \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[1].bit_synchronizer_txresetdone_inst  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_2        |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|12    |      \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[2].bit_synchronizer_rxresetdone_inst  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_3        |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|13    |      \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gen_ch_xrd[2].bit_synchronizer_txresetdone_inst  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_4        |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|14    |      \gen_gtwizard_gthe3.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst                                 |gtwizard_ultrascale_v1_7_7_gtwiz_reset               |   310|
2default:defaulth p
x
� 
�
%s
*synth2�
�|15    |        bit_synchronizer_gtpowergood_inst                                                                                    |gtwizard_ultrascale_v1_7_7_bit_synchronizer_10       |     6|
2default:defaulth p
x
� 
�
%s
*synth2�
�|16    |        bit_synchronizer_gtwiz_reset_rx_datapath_dly_inst                                                                    |gtwizard_ultrascale_v1_7_7_bit_synchronizer_11       |     6|
2default:defaulth p
x
� 
�
%s
*synth2�
�|17    |        bit_synchronizer_gtwiz_reset_rx_pll_and_datapath_dly_inst                                                            |gtwizard_ultrascale_v1_7_7_bit_synchronizer_12       |     7|
2default:defaulth p
x
� 
�
%s
*synth2�
�|18    |        bit_synchronizer_gtwiz_reset_tx_datapath_dly_inst                                                                    |gtwizard_ultrascale_v1_7_7_bit_synchronizer_13       |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|19    |        bit_synchronizer_gtwiz_reset_tx_pll_and_datapath_dly_inst                                                            |gtwizard_ultrascale_v1_7_7_bit_synchronizer_14       |     8|
2default:defaulth p
x
� 
�
%s
*synth2�
�|20    |        bit_synchronizer_gtwiz_reset_userclk_rx_active_inst                                                                  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_15       |     9|
2default:defaulth p
x
� 
�
%s
*synth2�
�|21    |        bit_synchronizer_gtwiz_reset_userclk_tx_active_inst                                                                  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_16       |     8|
2default:defaulth p
x
� 
�
%s
*synth2�
�|22    |        bit_synchronizer_plllock_rx_inst                                                                                     |gtwizard_ultrascale_v1_7_7_bit_synchronizer_17       |    12|
2default:defaulth p
x
� 
�
%s
*synth2�
�|23    |        bit_synchronizer_plllock_tx_inst                                                                                     |gtwizard_ultrascale_v1_7_7_bit_synchronizer_18       |    10|
2default:defaulth p
x
� 
�
%s
*synth2�
�|24    |        bit_synchronizer_rxcdrlock_inst                                                                                      |gtwizard_ultrascale_v1_7_7_bit_synchronizer_19       |     7|
2default:defaulth p
x
� 
�
%s
*synth2�
�|25    |        reset_synchronizer_gtwiz_reset_all_inst                                                                              |gtwizard_ultrascale_v1_7_7_reset_synchronizer        |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|26    |        reset_synchronizer_gtwiz_reset_rx_any_inst                                                                           |gtwizard_ultrascale_v1_7_7_reset_synchronizer_20     |     8|
2default:defaulth p
x
� 
�
%s
*synth2�
�|27    |        reset_synchronizer_gtwiz_reset_rx_datapath_inst                                                                      |gtwizard_ultrascale_v1_7_7_reset_synchronizer_21     |     6|
2default:defaulth p
x
� 
�
%s
*synth2�
�|28    |        reset_synchronizer_gtwiz_reset_rx_pll_and_datapath_inst                                                              |gtwizard_ultrascale_v1_7_7_reset_synchronizer_22     |     6|
2default:defaulth p
x
� 
�
%s
*synth2�
�|29    |        reset_synchronizer_gtwiz_reset_tx_any_inst                                                                           |gtwizard_ultrascale_v1_7_7_reset_synchronizer_23     |     9|
2default:defaulth p
x
� 
�
%s
*synth2�
�|30    |        reset_synchronizer_gtwiz_reset_tx_datapath_inst                                                                      |gtwizard_ultrascale_v1_7_7_reset_synchronizer_24     |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|31    |        reset_synchronizer_gtwiz_reset_tx_pll_and_datapath_inst                                                              |gtwizard_ultrascale_v1_7_7_reset_synchronizer_25     |     6|
2default:defaulth p
x
� 
�
%s
*synth2�
�|32    |        reset_synchronizer_rx_done_inst                                                                                      |gtwizard_ultrascale_v1_7_7_reset_inv_synchronizer_26 |     7|
2default:defaulth p
x
� 
�
%s
*synth2�
�|33    |        reset_synchronizer_tx_done_inst                                                                                      |gtwizard_ultrascale_v1_7_7_reset_inv_synchronizer_27 |     7|
2default:defaulth p
x
� 
�
%s
*synth2�
�|34    |        reset_synchronizer_txprogdivreset_inst                                                                               |gtwizard_ultrascale_v1_7_7_reset_synchronizer_28     |     5|
2default:defaulth p
x
� 
�
%s
*synth2�
�|35    |      \gen_gtwizard_gthe3.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst                         |gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_rx       |    30|
2default:defaulth p
x
� 
�
%s
*synth2�
�|36    |        \gen_gtwiz_buffbypass_rx_main.gen_auto_mode.bit_synchronizer_masterphaligndone_inst                                  |gtwizard_ultrascale_v1_7_7_bit_synchronizer_7        |     6|
2default:defaulth p
x
� 
�
%s
*synth2�
�|37    |        \gen_gtwiz_buffbypass_rx_main.gen_auto_mode.bit_synchronizer_mastersyncdone_inst                                     |gtwizard_ultrascale_v1_7_7_bit_synchronizer_8        |     7|
2default:defaulth p
x
� 
�
%s
*synth2�
�|38    |        \gen_gtwiz_buffbypass_rx_main.gen_auto_mode.reset_synchronizer_resetdone_inst                                        |gtwizard_ultrascale_v1_7_7_reset_inv_synchronizer_9  |     9|
2default:defaulth p
x
� 
�
%s
*synth2�
�|39    |      \gen_gtwizard_gthe3.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst                         |gtwizard_ultrascale_v1_7_7_gtwiz_buffbypass_tx       |    30|
2default:defaulth p
x
� 
�
%s
*synth2�
�|40    |        \gen_gtwiz_buffbypass_tx_main.gen_auto_mode.bit_synchronizer_master_phaligndone_inst                                 |gtwizard_ultrascale_v1_7_7_bit_synchronizer_5        |     6|
2default:defaulth p
x
� 
�
%s
*synth2�
�|41    |        \gen_gtwiz_buffbypass_tx_main.gen_auto_mode.bit_synchronizer_master_syncdone_inst                                    |gtwizard_ultrascale_v1_7_7_bit_synchronizer_6        |     7|
2default:defaulth p
x
� 
�
%s
*synth2�
�|42    |        \gen_gtwiz_buffbypass_tx_main.gen_auto_mode.reset_synchronizer_resetdone_inst                                        |gtwizard_ultrascale_v1_7_7_reset_inv_synchronizer    |     9|
2default:defaulth p
x
� 
�
%s
*synth2�
�+------+-----------------------------------------------------------------------------------------------------------------------------+-----------------------------------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
u
%s
*synth2]
ISynthesis finished with 0 errors, 0 critical warnings and 1093 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Runtime : Time (s): cpu = 00:00:15 ; elapsed = 00:00:24 . Memory (MB): peak = 1588.738 ; gain = 492.207
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:00:24 ; elapsed = 00:00:26 . Memory (MB): peak = 1588.738 ; gain = 552.781
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0892default:default2
1588.7382default:default2
0.0002default:defaultZ17-268h px� 
e
-Analyzing %s Unisim elements for replacement
17*netlist2
52default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
1625.2622default:default2
0.0002default:defaultZ17-268h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
762default:default2
1192default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:00:332default:default2
00:00:432default:default2
1625.2622default:default2
1132.1952default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0012default:default2
1625.2622default:default2
0.0002default:defaultZ17-268h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2i
UX:/db6_ku_fw/vivado_2019_2/vivado_2019_2.runs/xlx_ku_mgt_ip_synth_1/xlx_ku_mgt_ip.dcp2default:defaultZ17-1381h px� 
�
'%s' is deprecated. %s
384*common2#
use_project_ipc2default:default2A
-This option is deprecated and no longer used.2default:defaultZ17-576h px� 
�
<Added synthesis output to IP cache for IP %s, cache-ID = %s
485*coretcl2!
xlx_ku_mgt_ip2default:default2$
bccda2121bd363272default:defaultZ2-1648h px� 
Q
Renamed %s cell refs.
330*coretcl2
412default:defaultZ2-1174h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0012default:default2
1625.2622default:default2
0.0002default:defaultZ17-268h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2i
UX:/db6_ku_fw/vivado_2019_2/vivado_2019_2.runs/xlx_ku_mgt_ip_synth_1/xlx_ku_mgt_ip.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2�
pExecuting : report_utilization -file xlx_ku_mgt_ip_utilization_synth.rpt -pb xlx_ku_mgt_ip_utilization_synth.pb
2default:defaulth px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Fri Dec 11 17:43:09 20202default:defaultZ17-206h px� 


End Record