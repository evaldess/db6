// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Fri Dec 11 17:43:02 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               X:/db6_ku_fw/vivado_2019_2/vivado_2019_2.runs/sr_ram_commbus_synth_1/sr_ram_commbus_sim_netlist.v
// Design      : sr_ram_commbus
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_commbus,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module sr_ram_commbus
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  sr_ram_commbus_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "60" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_14" *) (* downgradeipidentifiedwarnings = "yes" *) 
module sr_ram_commbus_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  sr_ram_commbus_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
X9v7zvbGtj31BbxjWvooUglDCm3RblvQJ5jqdvaRCPeekjc0rd68098bjZCIiDEUdK9P/YvKyMep
BjEly7BNXzJG3Tc6//mqXqWCNUYAKz9myIswwO5JE571D17n6QcvWrDU2qi0VoCnYTy308pLaCYf
22SNBSISLgDxinha34O3ur2M+owsMBRVp++g71byvh33+2Hv7hUm+p779bEWN0hrsPhK5H7TKuW1
wWZlZVhnQ/qZXXp3TBqLOw3UcFwEcrBzSRozOPtaB1uqluH5Ol1lN+6ATAPwArziF4UP0BEk0NE9
HtFEBiGPEV8z7h4VbhGe7OCmWCbfgr6F1BniPg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
cxLHgXh7oGvn8pXj8KNMOQq5OFai4TtQtRfVSey3A+LZFJBXaH1JR9NShhvjBGnJ+5ixsTuAD0sC
A1lEdn7w1WHLW/a2lMNgELEfQuS8MPHswFzy6DId0YSRoHStVOYGzdeP2jbR/DfBxYaDc3HJGNql
qNkGkvTDAye3oypEcgBzIgVmoMxpOW47fwJ46GuQLycalX8mTHwVCeoxRbbWPtXQ+JKY9q7mZKno
WcRjbLja/gymRREKLAGozCw+0GPWnEjtAOygdD3J7cyn0JxUMkWxRwh52v5MVHIgCktyV/uIdLXV
YcZQc4iqeXEMayphNNmOG5iIfZKeUywTdS2CIQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9152)
`pragma protect data_block
OB0LlQUYR9GkRn3RCp/tPyTJTj7yFyii/UKQQKF0ZRYHJKBsuId59PukHjhdpPgag8sJQuIW/2hw
+PlmNSbMQNxzd0rHg7Za8w+mVTzk/kj95uIZ3bPP9PZED4uly332zALsD33mKgPJ/37d9Ji+oP/w
13Va5AmVH6XiU3Fcu2x4ze3bzaUWFXxrqDsvoR2bCfbXNYbTj4JasMuCQlRl3ju9q4MCf4/MvR4f
FtA5R+AVy9IoKBAbC0baPMhR4eJQyhhOcsX22fxpYty8RZClzU1IiJzakOMfIxd79wbEA/RAUZxM
ELksQYSpF1wYG85+xjlK1OjaY3tTUSyRp2wtSczWiEtDvSGaV7nAUBFj8dch38wB39YWbQGUyfJ5
mVY0N7R8XJrOhe1tY4+4fThqkTCuWo6si+iQbODyQ01LWhSJhOKrl58+IGZ2YQPkrNyycqbSYgGS
fjY6R9aPG0zG4M2uTwQ/KP0WHsVq0XEjhNyr4+jNliKIK9jf+/gALbFE4UCqHY+II6exFWEDOyAQ
5XnuJj36ArfEScD8gLNtIFV9Ys8RUTZmSzH7F+/wuuGNH5guV+S29cI3I2x2nM8aKHmfacqFWaP+
zF97aghwjn2LMbmn07KZhzFknd/eva60tjt0CPswPdFS2WRhcW7d3gTIAp5kraPkt5/82DUYzil9
9vqHT9PxUzZlfSTKq1x03KCJIA03uC6HPh95mqz1nLIMbn2ej0yuOTIg+y40CK6H/Rp8XZkFR3MZ
m7C8HF2dXxkoHJRP8cdlurWXTKav57GMVlLVqvqJWQIND3W7UF9IK3GHeVw1lD9pM8inUzg2TQre
M78AtNeWQvSkr3Znea7fA2J0W8Lgi6rfEP8UOHoxP6FbRWmfidMTdmFrNBXNI4WmkQsz4H2wblqY
Y74xYWQxFHJB1Nv3Unj2P7ECTOhXRkmCrjGICcVK+vKMgscMVkJTSZ1Yu8jnWbrwpbeMhGrUthGv
+QY/ymDf1TGBlE5+6raVtFfn8JfXXDwZ9EyTBrpJusHJifTkkccZPKTbCov0JQcN+j0XWdVtq96P
o9KicfZhGtg99FmmaAxjX8d3gxKjqDPheACJeldnSWCHR9ocgug3f/odQ1j1ZRpHBJ1JJ7ihTAPy
zrI95inrWgv6KBU7o0oOQgr4ASjw0nSa2WrwWLMNKVMoGlA+oS99xqw7Rl2eOEqelmjRgzyjxfMD
3YIpIG4qF7JXAfk3XRlfOH0+WCWXbIZTbshhv2GAuDGGsAW0KS6pge7MBfYUuoGvFm7LdIl5xqhk
xUHyyshPYgALBkLIvRSwu7xRco4EvSlYRSUHqFajIuekc1xpncZOnQsmuiudEQjbF9eF71ciDM/w
mzjJlD49sPc3Pz9DgF+xQEauNVnjSvtVmgGhf/eFqoiuoKsemoysqJ/oLSWg8zc7/c1N02ObAoOt
U0AuS2K8nMCv9PdKVdGWsrn35mm3qs+mCVLC+MivhpAsGFtF1ue6QnklTR4aiNGJRenDAfiwUfSb
6W/MkgKdcFVkxHi1XN8ecV09znMte1na5CfhnO7Y2eUtssEaMxBS2a1PU/lgjdP6JFo1SRB6LltJ
5/n3y9qAvc62olsRlA0iIO0TQCgS59CS+yVeZM6kHjxFtEgB80C9+ioxstJjrilbWiqO4UNqU+hZ
Snq2TnhhHv2fWz0dHMlgVgzrcgEhzomgx2XxBaEkjk+zZlJg0mCdII++GtSWs8eFP56qgFlGDKLL
ZR8FQYS7eMf7urpGn9vTouqLgqfDF0WjPa1fc1oeGuNLnX+rYqn2kQ0GLB8lBhhcdA8iv082BxTP
cyaI9aem/BKsWFBe+3zIrWbG5LeEFyPuTga930cBkb5HDQhp3t59H+hujcb4+I90i4CQ8sUUJs0T
HziH3gYs+ArdvOVP6nVZc/VxgmOywA2rUM5wjKyWPqflfa7V5zVYeAoSJFkZ2WBiZ+HDh3IRNlb8
ai2NWg0bWAgEs/YNt2zg9uFkYTVAgglxLED1PMdRPeOQg03LRBxBk1L/sG8G1B7crwbCRnBjTOW/
t9ERmAR9YUjLwqAUA+sTT5gmUGiJclGMyGWRKGCNSvjpxJh4xH47bc+vl9tjPSULctIdg/xgV0dy
+qB2mUGSaLIW65GhuM+C3pKL2YT92MGEWxduvPR0kR02uwEL32knBUc2z22a0NVaEAHpCBzU7lhE
Hskb/LyX+tEuVMV500WIOTtFQgStrVENlh9YAD66LQzVTwzlkEzv2jsQnyXQZxXbcLMPbqQyVjBa
Et+mMAkVj1otLSxNv9oADRp2c9Od7o++LyBdxj51ol2MURpQMTGD8Gb0vOHFG8/sPs7jLD1fDVev
bjQ3cc8m7FcHjL6H2Oz943rWuWc0P0w38u/zNIDSzF39RPl/N92cti2z5i6UfKbCQ3xhwXL/CJIM
sQ/adgtCkAcQfkxVV4L5jAFR0RJjobh4e0vi9SD2kAkkghRcLpBgv5l9thwav0szCikaXbB2tzv2
0M00tBNDrdGd9Hj8Xn7dZMnL95Jf2Fg3ArNKvwFL/dPx/rwtGI8q9MmZiwciD/oDJP2fXyuDgQEL
HbfY7Thp/OE2H4BMoeGXi2yM0kEcoAJOzqIvzuAl8uKcFuwVy2KU63sYVH+bM6xI156TwJe2P/Bs
zavujh3tY1RjnRUFxIfR/CCUiwPdxdXCWfiAnIs8LH1QjSnGSGwRqvNBMyLPpvTg8e1d1MV4jVS9
D2pi1/GB+F07abTxtyY3NUiyPstUDP8poAf275hhb4fxtDydteGoGuGNXs8Fu+EZ53euP1XFBPyd
RiXI/N0AS4LlyT4T8SO+TEVN/ZMoe00hOoNF5OaUWU353enBruI9xoPmtOTgsQTFWD+hLT2KsCu2
MVLH4K4t3KesJh/qr5rUbcoLT8F9Lz3XE9N/rwZNNJxfiKnX8X4/Wd+YDEgjg5uJB5AfffFJypru
XTK0YoGqxIEnoIgZC4rtV3cTOmELExFWOGC1PpxTuE4fk22HwC9UJkOvpvXoBdWa3Iz6XKriMVvI
97MtAZ2M4n4GqGlVV4QyMJ7jiOio6J6cHicm4wxry1kCv5Ey88sYzU043yxf8N13eNHmYahFVR0D
vCImU5orWVE20IsYXtwpBpE66dOFVj0g6qad7PWl7nsXtBUU80kd+9aqwvrBXxqGHMdkrjbhY0sL
3Lsmf9ihSNteHzsisuYOUO/2U+SU7d1B94bAQA+Ii5Z1MBqH9YR5Cs9zJw0nJWhxm2ueVT6lgbfn
oSgqCWVTPWNVUYBKKHh70qUf+uGdj3uJ2QCLPdZn31T4H1umI70bfnlV87cyeK9tZMgmwlFFXuuR
n2eDMt6VHM6dI7FEdX5qsda7sTEbs3bB2N/4oRE0acE0oQkPpTa70kTBI0usTIlhxTIJyZmheTbv
RxY6VpzwstTqAU0tPn7idkpXHxBZeZK/LjCF0HFx7lPdJIMyMDOoxT5GmltYeaodGM85W+iNSCjh
GY7weyngwbATYHC1n2eWzJy/09R67+5tRUw00P54UObdyDQEBxjrsjyru0kOX6xzfhueHKaCuiTt
aJ23QU0oUtB6Y7M+ULvkuHJh050OxJ6qP2dVL/q2xyA02yjhuzVSPlt0gUstICOL0GnEMsWURX+k
jpAp17gy4Zy+JvwUhDWefXaCK48I+QdKoeo4za3jGuR3JzeA7gbG1XaRojISeKccCfk49Ngt9zqT
paZOo0dm7WvZf9Dvs7r9jsS97gMBbWRXZ89kjv8LAHYAWXK4tf/S+AaShbvVilsySqDha9YGeI5K
Z2uZz7NiueP3lRTNJS22EfctrYIoWog524Skec4CrY+2H90n3F+RYaqxPeZuIm0E806ahJkej92p
rXF28JY6tUFKuV0y9NeFdJKYLVwmfbeiWBGnfX3cai1JRRh6XwZOyIb9MMdCf+Vm9uP/rsUe7z0N
stwU44H6UCD+2jrFmPNjOvdVgIuYSejAk8gcjukCqG2bDbi2HoA4ehyuVw+FxywAjx6ADx/KPGts
PdzpF0cJNdbnMVuQ9H/Er4cs39Mqqt36G7ozdrPN3mdMtQVtkwuhwQwUGSB8TFZQzvfzKnOv3P5l
fzwum702TZ2RLnhYagpSjfTAWQrHn9PJp2LhG1k0qLin9S0m57X9AN+qcVLs3ub+XuCAyPaPvycn
rSQcI/jK3Tv/Y9ICtJrRHmcmPrgcQvbG0RtHInKSHUEhOGckCWeVrCzB3Rn4xaMwiqZt048a/KAr
caliLsDyH9FZ6ppVDPdpEsY5eyisjKKgu1XtBGo7X5sZyUQoOEI043HI/tXSaRVNZNnkPQk/WrX4
8TOiraaND6NBoJjw4f2URuDjESsPRPtZrvnYcvkDaF2MRuGe6qeBYEO9WUD7OsHqexqD4i0zwTAB
kIknH9ZCO6MnhPOI8kiF2vFKViiArU25S890ATRvB9/N/nRIWT5u9QJVkg0a8dl/YW9v1L9OMK3P
SVR0/KsPatAwP/MgaYtjozgJy1n/uL3ZQFs5qC2Z61OLqhLGEGtu99783kff13WaJB34+L/I4QQo
LZu+xQn7f6/DwYnqmWME778T01VtlRPrYnZluUzefiuPXnQhDBfEZ3hQATwggOQwavHalr3cLR1K
rcaAJL31lRZxCkgBO+EdCQV4wj4LVe5jY0wz7TfhpnJMAw7SV6uveZaljGZdKE85ip7/k/CTE/hQ
+coBz5u8mVaqv7fGbsPV2VbNY9LG9KDTmAnIY3hFGv1ZiZJbMCM43wG3171HSRMxVKo8XVYFZpp/
bxFr9ob2yuGVivQwynhTAUBKdTrUwhw6hA/+m6Rn7Y4bjF2tB1XlO4CoxsJxrlP/OdEazNkWysRr
qz1uqgMWcKvxixHNf7nyvt5EslrRnSl4QfmJu7lZ7MAmBs2QjNkdVSMK6eiIwyr8JVswI2BK37Rm
5WgCy0zSHthzDhH88n3xab5zDaUfl59xoknY1+eLwdHtRCZjsyqR0JLuhwyzt0dqw0RtWo1lzaw7
uBSEKShMSBRNKvz93nFSQwFGJbOMN5yzMP1cJ5qjLa0ByH/KhAa++jwvAEmZMv+wsccDaV7XIZtI
HIOVDGI3lHvKq9SQqjlYL4SHRxAKn3PXDJJbWVhmUL3BYPV/owdCni8OLsDwY1wPr89dj86TnTaU
sh1kz/FOWLD+X0CdZ1mdzW+pWSaf+U+UX7DTdIdpVYyyOvcnmJIuUYncXkEpQOI8RErXU9wD2NXe
Dv4/KSfC0QxCWWvTM5iMiNZtU4NouaCR/Mn2BrcPb9GK4FDg3WwTuZobGGxW9ms4xttkZtdjrh8Z
7y9b+zax4hWR6lDAFbNYGe1swmc5uaVipIGRWySav1I/wNX6WrCalYmbYSOh4Ox9F5P/BMSvRcaM
Rw75B82IkdAeUWkl0knu2q9YZTxMd9JyIBe/1TQyVj5DPyMPuP9jyDbsrCMHBlcQp4OwBPcUyJhT
54BoD4RZQ4ZN3FYNlTDHT1l/InjpB4QDjRCBM5AtvRBnAC6wW7srsbRJRdliXgN/YDsfqmJli40z
BdVUDM2ld1Bsy+76eGyVumkkzbKoTpKVATVyl33Tk7X+oZwfzH1iGmvw7zoUBnbRQemGRrElj0sU
efgeAHmJFiWNqFK9lIXO/ROyVhyDQk0fIzPlIPgVnN9OW7TVvSorMNxvUDewyFZtZesVKapizvZq
6QhVJ6pu/E9QEGFBuLY7Il7BUILqyIr+yLmKUbl1W7Ri6TK6U4OdRNF0YqC1Q7Ieudj0ZJr/ORkH
mCRVfNiwp4fiiamzYr2DFhOihadgc2AuO+5MewEtPMc2+guEsQrn7DzNy1qZvkTqHJD+wqyBLFQu
LqFFsQYIwSw+0wYaHh8M1WJiMoeMOXh2zH2rk0yglBoA9JMO1hlEG6xdby2dzi95cqjgqcohe3PN
To4xo/nRja4jwyfu9vlwN2iijD1dCuVH5M3Us9zj4U2kfZE1xZFAwbvvzc7uMUDPO1HmunxuJr9o
DzJwdb3ecrfdYZP6RU2AWQafGnl6ovvOPMqUXEGF3ptFt/3qm5fgqD/V4AhUusPLChcjmrpyoutl
NAOl1agH37w6mAFatkFB9C8oVCphDkvUCJSnzdoYqzum5dzJ0ztGSoKMpcVm93l24uuubGwiQmIH
j+pTVoPXM+w+EHid2aAsl7aVpcdrbBgdbNzVvESBKvp+BqJSZIrBmDg/Lb9NlWHh0NTCFKU2PZup
1UrjJI7YrylH32CaX7dXK9kzojdMCnGSu8GIQOYcus3mYUldvdGY2swiOjB+OSeMGOJO1/gLMce4
WgybFvqXlrYcFm7dJY6k5LttCIMmkCGPRf+hkbnllnAc07Aw7F1uzekxUF1isR8A6/p6TkeITCs0
8x25SKhap92vTPXk3AVzEb5CzSWnO2PJEArxlMscZfXpUuOPjRC3NvalQSxc6Bz+NU+eeJPKgmBh
CoNdnY8uVdfRU1ADC5m8bzBDqOV0gnotk8xY5GVoJrFV1sDBaiseRwu/Rht3LzvlIRDTs3+iwQI9
/1tCcomsSdfpr17Efuj7cfvkXUWvsP3uMsutmaQPiCiXrEU8qmGP2qwXKPfmaVWqhPTPY3dnx6dh
/kGptASsN54lf9jLxCo5UO1n5D0rSYVSSgygtoO5AlJjvupicjzxbp3E+QqHYAWoRYhwdTOgnNKR
oMvB21+DlY9wEMxZmy5kHujXWAoejMy397xELUnuoLs9exUCFC36NlkjnM2bV8ccKbobyRQGcR13
LYeYcg4NhUEpCFauw+nJtkoZXoL27eqR9k+hUz7gdUppGUlB9C3YA1gnyr4JGt1jpBy5G47+aar7
VqI6E3tkEgTUXeCTG0ozb7OxAtSMnBzZRZWKL9Hwkd4cWljtAkuXDESHtZQ3vFo3Sszy36UaVEhC
Z+17LjDN7EkP7FnRxqV5hL1bqvkC0nl3dpAc9MI743Tamk7H8suv/lS0X6TnZ3QOvKVihRpRHQDt
RwvBuFtcn+WQL76aoYwMzjxIWlp1gDLCkEJWDSY3rDDIB0ah54xwqTHAqFp38chfmxSoUhtijSCm
uQ7WOAbOlUFMjME0UJMo11fFwSejGUBB0RUryncBzQBRoFtRrXDCRjduacuzniLC/RD380SIy+b4
r0EKNS0Zu5YuOkPWKbLgXyQnWv39NvYkvAXE3PU89XBG+0nGveIyEsjeG+oq03rWJYKHqEoGoJRS
9nUTvxsWm1XVJa/g4/NLTcTviJweCbVBlBtUQLanCdWPNpR9fnRm4OPFFotkNiocwysc5FdaRXyE
MssGoCZ3My+CaJy8xApgV2yknJidUI3HbP5J9SHhBedwacrtE6/DJYaxEhvQyubKGHFv0pzqlmq1
4ok9ATuyAYZa1tuoX77D+5Rjbax/VW32CoveaTZT57TvSQC7cfF0qTnGgDwE6DMfnxbK+5rucs5h
aqxifcfrTUrojeSRfzhKUG+w1NLyEOvVsJ8gGzka9wjOgYK01N3Ib4yAoFGFS7gacI66bRlfdpCk
sPXpnbAIjU1+CIbQQFQW/p8iKnroiz6kWlxRJ4v8dtSxvvAE1gqWU+gQ2OK2y4lXNpn9mT3sU7p9
e2zwOwMBKF90J666ox0/RBng4DJ4MkDH+ExZiIVUsNaHhEhThUIWUH+U4XdgvthZEA4Mnicc9kDs
Jgz5j20Muxu6RTADsFR//xnnHvcsYSYFhGWe2d0v+P9Wh530k0yXjP1vzJ3pgCMvAf0a3NYxPawW
Hv67WxOEYuqke/6iK0x31OcNqJ2QJwDcuLr0DEGZf8bEl8u1tHmfCoODiqiXNQShz/8OXEZIfu3G
nT/WMMHoNP/S9WzEy0lgIKsnGddqu9LGv8nwWqgUSYYOG8vlaRZXHnZHO4EiHv9Z8UaYFjB2eHlR
aIyGp+pcEkkebPucItVuh29hFqNM/wm1e8FaxsQckShDEdodeP2SR9/Ival52lXrwkiZ33xgBoAP
qnScEvsmNsU7yAvLaSVsaSKW/6EyKVJ0bmZlcG0IbyUnY0sY8N7iNmf6SdNUnP7FK3sWObGUWerH
AP+By3dk0vXkDychOJTNLQM43+fYOAvUVr4S5gBqO1oOCbDTkMI/fqc0RpDcaRxrU2imwgTg0NIS
Li96/UrgJsKMgtS2GX7juRO0flJC88OpCyJ3RkB9VkodvJL/8ZWDjk1+taeJItqMqF90Vi112KZt
4FFBerQ1/vyle0Ip3yLifEzpbl0UGudbi7smJeZwvppX7dlJd0WEt6TVKAzvsIl3LilYiz+s5D2p
F1NcF8Hcyco15dIozUnqhZjrinzObnK1fCrX+PHIRgA58cba1QsN2h1ZPH3li0YLm+QL5NYQazf2
g32Yj/CQAfdvsBaqxbiu6N0d0PUqMx3+Sq8m80G7K32ZXGoLBuuxtzbJZXpIqL8Nfep2DiKa5gRn
7tbZbjK2IpupN61S6OqlZrsaxLSZNpCzNUOarqtJaI/VUwDKn3VeViBNEI9REUeBiFJIjl2t6eIu
SVKWWxfZtzmgIO6yZ61+q+a40N3Nl/ZK8dfxWAXkwwAVYrxDbxs3bZyLvSYI8U9fuRlQdYsdqzHd
3Ywx3xMW93+abU/1qZPaJzlsr4kEfqnQhuBtF/wKUxPcJ4kv2uVQGTz+e0YGUIKpezBCamlTBLpC
aHMXATqIZ5wi7+qbldLa1Um3AZU7nqDff6cPkGrBnVOWjNT9fYtLYFApJGLMZTJQc+l/5MhKWKQn
6TA2VtRpXZ1QYvXqFqDg3Yp0kzyXY4xyk7PtIh5ZhhSwNQr5uSbWmjEqu+dqBpfxUHLEl7Da4wqU
V8+KMBG/+L2Jx49Eb68WZfdTruXYvzZvVInJUKq/tzWfGlcn3+2uNu7T1g3Y4YmvN5d+ecyVcILa
4ablh9ivRM3d7Wbbvzazm3UzeD1c8r1qV7ETLt67XHKMAwDGY0+2MHZcu2/Yd8/3xLex53D66HWg
AdpbzPnTjASTI0qDwxfj/7y8F0/373QO0Bn/LhmC3G0/ElwDKe1KTPa8sKZuEjDgs8FphYdkz4Kk
ier/Ad9vQkWwfH86MvmTO8nLqgaGgLYkDWG8LzX3xR7vR+r5aM59PlNSYgAYxb5Hh6Kdlac773YT
tuAW5NFfsx5mpkjSSACmhe421qh8URFBhxigLkmOcY87mgp54jVX4o5xplehlMvDdAId+DThdo9G
fbU3mlMfXx5y5MtVbNgKCv28ArROGojjK1dAlC3/uXfz96YglgDe1G/0DNFjnnU1+FbdbnwRRh2c
YmNdYJI2jUqY9yEczREAv/Oc1zH123vcW/1jyGSJC7i43evn0CUo7pZEIfG2bnqZu0Wz+Exv24aq
sBV5GwV8+oKMwIW/qq5m/oaorDlvevtNLwFVFmHD4VNZKpyBsUpMPqimJn0mVp0NrF2R0st5brUh
JSC92wTwqWpcxOJ0aUxEvqwC52YExs9D54o6ZRjVT2Pa4ZiE6niJ1+HW3arht/qDE8CiouKHD84Z
oWb4FXihVnyjEMukujtEiVY0WPauuNsSrAqDHAc8c3/6osNB4NxnaE1binjhWdeKJ74G1fm6VxMx
LWRkVCsOQ/NrFoH47iIunbj1EGol7YVT5Ydm+to4U6oJZIvn1dAPeLP+/m1NOzAgBI4gVib9m47c
1rdgtTuX3dHMSjPPKsQidmahxWSE5Ht+OWYV3sa0eKb//nNPulbGL9udE5LNMDRXUP7mLKVgu8OJ
8Z0e+NRHWFvMVN393uM1c76CYdRGrva48dSY9zHz3IRoYcjw2XpLuRDPrIYgIVj76XXRAiKroPkq
mGnKtbdGlPfiGS+wM5NiAKpQBHfETWEnCUdRQZCdTT4QpFI21eOLxzAUiCy/DeGa1hrYpfNnE04N
pF43XP5g92JJioQKWwC6QaJ9aOJF5bNuGs6D5WtRxluP6eXNDAEq5ady6cc1wYLoXvsHLFeITkdQ
TeNl5zSMtiKoRFU9tlnMTWLUJnw81y0tchtOM478CQ2lADAQ4SPblOtXh4K14ra2n3xM+MR51bOg
Ytq4Z5luMAbvJyQxL6zP0rLwt2s9JHK88Wo6Sy3cQ3t9uzWII8pe0c66GZl4NRO2BJSD6DncMgnN
D1HC2K2wylXehqukeHIZX+dR8PoHkLEVkT1ftN4aJKdKxLtGv1YXHPl1iq+X7/JedNdQk9bY2deb
deM1f4TetzgTtvis0ZXtvh8NHNPu+3Vvowmwy4ldIXjjv2y+NMlA7pJQANU84c0xmqkjuLV0SAjN
6ImhHCtuR2CO3asnraKEa+oM+PXAVBJJz2JVM0zu25Kgd/w6YqfXFog+/ng2ZQ+Y7INJIKjIo34+
aVAUPGvj97cPEux2ildCswe2eLBPYPG8MYEv6DYW1mK18egwFGZ9LzwrD0q9Q6s2VpTbz1XNq0oV
hS6KzuTb50v87t2sjyC4qoGW9EjW8duPceF/xn5i3M7EJcGOsXmg6mTrT6IL7k+zkX97TgqYqpJ4
jIJB3BaAK83P4Ypk4TnrhIOgIjNTrfIAbDQtKs9lriBMspIXFgHpsjyuepg0hBsKv3DkdgSCUH9c
no0GHrWX/jR9gPtfjAKDZLD1qeZMzqZdq0pBu31x142jOE6wzI9Fbnx9DAYpsarP8ZAQHcs+vZyu
hs9KgPVMm1691R7Gjd0yOKwye+u2mNYpWrV21qqXwb71rbI1AGe22728Qv56gFHdcJzngIu2+Wyi
X/Qkvt660vOJo0Md2cJ7m0YKzfWrnQoREdrE32dUvjyhXmHR2QvMmszOtdT/fw4AaS6/gTjv6A+D
awONEewTqEdM+CGkFj5ueqTiEr+eCTCIrkFnGGQ3oSxDS3PVbl5bXyriQp0rH6Q34k4NNN5sn7CS
Anj5F6fV7KYwAp0ls2T1oaBLz9KvuyEgrWEynzg86Rb6KiERwViwBiFV+LHRL3sOm+cgajL4kl7i
30TzG8iMBiFAq7puy0YCEh1a1wTev6ub6uTp+6+PiNn+j06ArY9tnO+TDaeYAgKiR+qAVJqZcVHx
BsGybInzVyDwFCGgcZNY5xreljaZOsLPOpCdDPvb49c+qllvDjgjj4dZklng4WyCs0x1NcgFHUfa
PXku1TucJgURL8A8tBzbeyXM6oPRajtbFUw4uxsGV4ORl6PRL8zVUm8k+c97WDxkhZCCwB9zlJ8x
gy63qixuTc5Q0fV+5ICFJi0xfVA/3SPljILO0/druahLRumm6doupO+UMVtyn6aCcHr/NbMwkQvF
c9AaHfLyy85ZpymZz7Zt9ntzxRed40zAcQEnKOFAF5tbs+/dC9frqAz2M4DHGcmGvqV9kFzoeRcu
bRVfHRxs6Dg6KUkKUtXKczvwR8oyxS84Frq+PtYwMItfN8PUpEVmj/UA8SM3W9qoevytRphwyo4Q
1OqpMu/8gM0r9JRWNiiALWIcMNqC4vPfrABENmGueM5r3PA69ZXvtKDUWwmB7sdSre5k4TIJKRC/
HevT0+o93yuKWY0hNgBFodyrL8ePoOIn4vQSDrz42on7d9K9gGFWGbaDzngbAh83xf89m9lRok3c
OciIA7XWfHMKAgxE+q4B9X25fKVLG3aA4wcCrxg5GS4Kj04uwLFs0Dj/60FalKBYjVOfIcvUIgEy
o+zXoF8pwYueodhq6Njlp5/VE/sns3UGJytIYfosyv47ysW/F5rRLd12CyVU/5BMXTDBnqrQssIu
PI7R1yhitU5VrZErmCUAljQrwIIe7H6mtI7MkbmgAM+SlTSod4BnsNQLquu6Us5nnoS/+CF7hkUE
tpPm6jyFpkF7yAXKPp+Xvm44oVgn5smq7WZwdXT5YZZsU+BRRoUa2oKzca0gEjvZGHtUlC/Oa8qy
rdb2V2OvHyEhroc74SuISd+nFOfcJJDE6N2wwtUYSDLHfk21smuD9CjwhSThMP6dl0fRzIp3KLfp
K6EFo4jpbdZTOTVHrAWqj7oI4iCZZ7fuTRjXgucJ051Yn5HkFXVH1rlNW9OVEoJsL0iu+BS15MO7
DoChDGkli/OLR77mHirrQRuhU9FDOpYserzGy4gO7GPvdiEt4rWoU4q4nJUdViZDf9zxT3RDlw+/
sTrIiFTCLSLwKBnNts7OjrMy318DeldI/SSg2R84DKAApFcAcnGIXanY7ns0imydi/cxGB18xnZ4
6wVIVCB9uJKWYGR3AZsiyYD5/T9E3ASbWRcrr3j+R18=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
