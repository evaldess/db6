-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Fri Dec 11 17:43:03 2020
-- Host        : Piro-Office-PC running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               X:/db6_ku_fw/vivado_2019_2/vivado_2019_2.runs/sr_ram_commbus_synth_1/sr_ram_commbus_sim_netlist.vhdl
-- Design      : sr_ram_commbus
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku035-fbva676-1-c
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
qdgLvBYPbmhlXKdGHVaezkj5ANBLV2DEidxEHVMqCN6TJpSh78WcVWbvZAdw4vUHCjRY26w+OeYw
/e4/5+97ZS7tUrkXJMAs2YoNqHGHfCn24PkrQEr/iOsxNuffsj4waxuKmlJGAR+AYWdc6EejEom4
M2SkCsMHFtqBDUtDs50+7g9antndpJ5XrQqJeUTVnSYwSRvwkDca7ZM0g8sEwOrDcdmLFla3UJ4u
pmy/eWJByGmsuNiXb6AePC4sz/n/Bmk+IK7tktczAUwtbQwi5GtqAy4tvpjLbpZyieyQREBffO0J
vrZLwpwEz8ovEtRYFKsANt7puUZduRoMeKw++w==

`protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
gVbivYRwhVZPl3wL8UIDdI5oa42GfphQnxBXjrro3XQ7Bw78TAuRAgE+EQDxz8OHPiqNvHf/d0cD
p4FTkG5vhDFH6MfKMMydwF8EV0ywIbWPF093SXBTsgErSLupDDFjNZvCCF/oYklBdvJ3eROhR7na
C+bewNZd0xS7jmAYkv37W2qVFyaGWIXVVLUlQlHk8Q46+XW7vkYmWRs71hoz9GqfpqxcasxwCIvR
mNXJv9bvLTPlEb4tnId6TpS3HIMtJKolUiOP15Kbx5J97eSwvw2ku+QikJORQFSi8Jm6fXHtfb2R
NSUF/N9TPGdJ2zlNUr2B5NwyCzpAw5G09wJLxg==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 18784)
`protect data_block
mH7tBVUe74gTXFlc+GaYQcwGqNOqIfSSkDZG8ymJZHFvKQUo9jtMUPS2ebRl0O9u6qC3ajaWDPL0
pw50CUFo5EpVqNspe2tmq9DhZ/zdqbynzb1aI5V8gnuSDaGUufG9olvsAcLRrZYZirhK/bsEOavc
+VVDfqyKtqorQZIinD6F0Q5o4BT/K7nS7F7ETMxBDOO4SiXhSbqLeE1OUZ9bsGKUB6+iWH4LjgKM
9BCYY/mQUzuqz7ygvUCjW8UHwzERUPo/L2oLveXfbGbD3RkjqCWwXzYQWWv/L97F88EZCtntJKQT
/gT65BgkK3HuEjvXUcWjZXAzHM9tSg2EUsK2YPEFgukeCRa/AJncdklxteT9B+lCVRLkZVsdGHJD
rGwl4DmPCCZFVOSh/eW/zEw6UGRg9zLe9O9llSk11IfkwDsIjkV4+xaRgnokE9b8RgdYu7PNfrSG
KpZZ+cx+x0E1L3EH7u7SXQDLb5ulUYcCqJoDUaCRvL4e1AHX73kS0aLYn28HjowVCyILezJJMHiE
JJZySqMIYN5THVnT+udedETyurkSdrXHs+htMj0vW2Pwl4QVFBOo+Mq9O4JBqBYQh69NErlQE7YZ
aB5e6sA2lrRAnxbxH9LdR1DgCEkCYfq4LAweqG/TdegRCOVn5nKcb9NWGy760Qluwmz+k7kkhNa+
IQ5jbfz2serk17oQ1im+7wf1/3/3ykOh5J/kec63VOHZJGRxGQysubXtbBm06N+EuI2r8r5AxIUz
EwGv2rto0p6qs/KK0LA+W9hQsU8YnfVR5yjLcV3TgQdWtgytriR/ZRjjQ+qxCMU+IKPDHQ2YHz/j
6jeLQMFFE/qpweBVn7YLz3+/5zmCE1zaYqQcG8TJ7FFP0DhCHh5D+shg+mhdtnTECl8Vh2wgr4z3
i4tSAIGayOx0PFcHYuwaK2GbcZW+RniBl4eLVxK/sf07AwMIK3qyQ8DERq2fPzeWD8eEtNvgjza5
b0LioQgHCUcIbgDC3Z3BpkdkzXuhi9LWgun0ZSfQ3qmnF7Q32N32RfDPMjAQuRQD7COrBjUQzTsy
mZM+XABWuVIummdx+BT4HjL64wvFO3pj2XddoNuDdsOVJV0hG6Fb4VGLM4xGtQRSaZYltk7gc+fS
rzyFN0obifulLZctXArMJIyYzdJ/qIyAZ2niGYhzoMhr429272k2R9FTA6GHQPPhrcTk08LOwtDG
A/Efrd8ImIiF/Le70twLQyL1QoJiZUrHPDH/60t8+OVN4N7Hv5JGraJuUNesEbVjUlxQGpCdWU/k
ggu38JfzNtomL8ELabudhdbc36rPTaQQFPW+yi1VSbHbvv6UuY0DQai2cKYgtLldO8suvbLZ4tcH
56Rl9MZJz1isWvJ7NXLGp8oMRwOynAc+C5Bt/6dVtmh2LNIQaHNqFSoYJcoh6i18e0R8pycAzYTM
jgM4LOQ7NI5jTlsNAspbTVSDASCbIp0DG28yXo+EtkMmGQyuqAPegsNiy77+RZxF+eMVR08Z3na0
rZySBjd6W5aHPadYISHsdbPwF8DJvWLGV775IuChBCokR3gcc28kHaa1o5JfaVWDAjhQ82HSHMgh
gh5gGv564iZPggKEDVqphifBg5vg/h5cq3jZQVPP7t8w4OLOtK3tiVnYkELhAaNKxW5rLgS5ym7v
uRebQsVcxHXB9V4t9m4dDCBioB0CIser8f7yQ5mkM9Ijj8iXUx7W8KmlxxPOpECdr+BpY/C/AKYk
PISQkzslWYbiTkk5hgHcvnO6eaK1PYKFFl0hKKHeplDgijla1+AIFctd0Pq1MVWi8z00HC8F1jJg
4yDxkLnyAbyv8pxyIxZVVPwGZEfbjUhq+a3xss//AOxOLvgR5QPb/DBWTnpKgamFl96vIVhaSdZg
322FqCS0OsuXkx4irZ7kUj7D0EMnQDpkbW4O52g3o3cLdcA4IE9ogKY3Yt79+rdUBXy2EYaK05j9
IIpzIAis86srCpxjLt1fOpQgRwUvJA+27byOlHDuTKhFMkMP8iZG4YCNF+pr+fPFJ8cachgv3cC7
APoV+U/CiR6UVi6neamHpVkNWzMoasRqoR0CtCIEL/UOPpLHPs2afUWfdc+m8qrbvhqG0PC7wcWu
/tcM7kpBsNEXv7VZjaWB4zQLGWOyU3/OsxJis0XWfk+Xd39tXzx/JjHragbHrEcFrwhSgJZu0nJP
Uv/b5jOw27K9rDmSTAjP4AE3VSCcje7xU46NOeZU4GrIFTQt25Zo3LFYkua5DoDo3m+Vs14gaJjP
0dqaumHMILRFOmU1KpPT9I2g6MX93jimvbvOLfXX1/sN6NtMvIHgTPiLxwXXbZkQOzV85gIEOSho
ZJQ1ayIa0wF90LvhmbF7Rq5nRf2hQrPAmkofwIvWDZ85T/kbG0Fm7YN0XIikzbEU3ONmpqkwdcb4
8BatQS09jTBRAkE/AcAr9DwmjPnrCELOhw13O12aGRmNjxsFtKwOnyQLC+7eF6ROB1i1ubXwN+TA
h4qDLyn6Ax2t66gBeA1t2IKxKkxgwEXlmQbLOGLFEKW4tx3t4nUluxDKHT3QUGNpW1dKLfuT6ygY
ISsM4FIKm2aMVXn/ARzGG8o62IXzc00JIObsnVPJNqcBR1t90F7iqxp65YqzwcJAW6alUIf6pbOQ
atnWIpdpOkcTylz+wHxaQNmelLJG7IMN2gRGG3Zxk6wBBS5eMTZhLMC4DskVuG/TdDwkHMiQCkeO
DOVg4i9dXzXf/xS0Ed77gwyoCfCTVUTlw7d7uK/mBHsGWlPQw/HUxaM2NxRNfOebnpW32gfTsIkV
eeeLvaGjMSSLCTKuLPaFsaHSTjEZocROepfdzWSCFGMNUd78rrMhuq8Z0siK3eVSbPn+J8FnSwER
MsFeor5jsIKdZWoSbHjh2KZJSu5VTCAPm08MiwTv7uLerqTsoQP1uvnHtUQPXwKFRdzVbOrRTvMJ
HRpKVeJlbVs42Hnf/KGXdcPgyYruuL2dqnxIYK7Ak8/apWQAD5ChExqiJAqvijrLfjDBXWyLfVXS
PFSgqcnb0oLTYt3ruEoUp27I9ui56N8ipq1EHBbZkF74q5fcVA7TarjvoPwKK0DjIIU6FKBmzG4x
XFN4CuzVE0r0jJnGznT2gjZvnqRC+IeISvgkZoD2UZNbfGWEDFGehw+gaLkxW2/HvM0hX4laqLz+
nj5LZOSjrGbmSH6QUgIC2tx416fuY2PtpzjDO9U+iYT5DHNaRKZOKuy5BJJbtC7ncWndnjeXLfDD
4KJSD2mh32H57Yu+QV8oaisU9yAuB8TfWeIyau4fCC5sQ46iFDs10qbzHLBYqCPS2pLJ+PE0QfPr
gdSsvNzD1xHGsLRrB0JA1efHNO/zh+x2Dnge4rx+aHgs0dZpGB3KEBF14olnJb0U7LR+l5ZkTVU+
uXbJHwrBrzJBUxQANanPipt7UgKCFK7dRzrD7gdpxRMJUaHYAqTGhGRyZpRZHxuH6KruOpVkfzFN
GZ5k1wbqcqlr3HzWV904CKBBe3Nn9LimeLVliOuhARGdU+qQlQXwXCeB3hBpYmvYb+K5SAM3auRa
JYvQv6tDsvqnz0eZeWqbYqtZSUL7+2gge4yUEjTDRV2Lo27p+L6f14J2hVMeJ1iAYBBJZIj1J0BR
sUQ7XVt6uIJQ1nBzudiy/TaJbHKII25a4kbIjZQMOQgHwFuNjK0mlRMzK+fvCkn1wCeKRlQ95NyE
1Fl0TRcPIRG2+6ruSabWCtsEsIgfBub6qJU/EmkqTfSBLcilnNw6Gcxo1hm4+USKYwAjd30oeHxx
W8ONEWACZrxLqKCTl3nZH2Ahr5xDx6g1S7aypXJNpanFxE8H0bkWij6BqJSqzCmyx+/1d0NmMNFT
7QEeJa3/yq2lMbqKHeHeQM7hkrTLirVpKd6BBmMmrVGaMZgCllIdvAHGa908EIGTERF23iR0hqPP
OPklrZGRZl39Gy1GkKOGbOlliCaXUimpQQO6iFrj8J7RgavktLKxitr9NCv6/kttxr/v/9zxOXBJ
VM9qcIJqS3cPdOoF0cN3B3yyyhd5nSoli48m41gwcyM5qh3dFsW8kGW8MDS3aGujW2CivWsaYsDo
fr4Dw969EeGR0TvpssvA2194pSCl6BalxvfuNln65n587XL6L+FXPzMQJdSWa2zCt2SxR6EypWPn
YPIOET58gEoN5gsH70Wm1iDOeTIL6My4nIVMrMhEptPIdU0jSRSdZVYIopEceqDJWLh7iyTX+e0T
drGcHRvt29WRxT/gL6JO+P858TYT7qohodDRSiBrfnK+8NnSWxwSSUkkux4kyM5lTmXGhD/P62wW
8+yj9o4O4qCFEFv+BLJaUpHqUMGO05Fx1KNkm1vg7Vlj3DKTC6XsM9X561J8yJoGGzs3PuVrILRA
NVWM6UT3QgCOR88qardBd4UzWLwHcGPf80noz1K+s3STGeD8U/LrwhHuzAqFsn5/zmTXaqukMY6F
m2lnOVUdSEZmmIzfjtYBM/4eI9Au7M/moBYVxjPIL4zAAZ290x+9hxoCUkXsvC2lYjBujaTmAZbp
96eYvMSOTS00tde6V/eq494i4oFJkY0dFPBqYM2qYs8AQ0EpGDAlG6s/VgeSC779yqgwax67HJwB
OYTKcX5ltoU8TP7CrArdX0NHRlzz4dt0J3h6BwI/Tk+fMbqGLuLbW5JMHbHu8sPVEgJEpLyoDi7q
iduUFZrDi/EFigAz5etMD9DnkHbFLzqGzB6qDLGpUHCKQtMNQltsDL6mmCoibenXmQr1PZOcvU/V
cBUwQyB3mUlz78t/ZePsbx9fMl+dbDo0w9Ka+AiBpIVdLQzdJHZz7CM6jyXWVPj8hlZToE/YD/RK
xGcLeAd4F3GOyPDo1lHUoqvqSaSojBTT/D2uJPKfdq82OBe4HkoyN6Q+TKyKmVuKdCX4rDqHuuoW
UlUUZQr9A+wLRKYk2zTDhm2yd14uuN1mvrNv67RnTw6wjNxB3Xb8LcozkXwHJLEP098PxY9ObpNh
QfpH+SZLcOKKmnyCb3VSbfo8sN/RqyUlQj4H0FqWY7/lHsrat3ugUzjBwcKacsZglF3GqrW5xTTg
xi4etLDrRrpQ8Ydp3yH0hyC0JOGTahhRJerxXREuhrQmnC0uaPvv36WsMhiI8AEDL704nwDC3tX4
MRwmTSC30Bb8jrJuof8v2B293sOQIxcyHuclHsGChTk0/Z6S9IPV7ZpLJC2tbMj2XYRmmJQKOndz
DxP513zsthlby3Iy8y6GkOyVnvxJOu0KJVBQui4yA0t47/+6gsMhpSwSruqV8CueTYw2lB5I9Z+4
8T0KE1yovV+MIMrquFaEjvVWXDDfpjmIC9hcgB/lP7yE6JMkERQK7QbtFJ6krb+h41ogI08C4EDp
RBOEM1cVaUeDhI4dTtMyXbRvYYnb1d42QCKr2X6rmaSk1oO2E9qw4x8SsFwc/e0A4BN9J0kHUx4z
2e1Oz6AEFUqtYcyMyiso3dLQSxSE+h5dAjg0mFPTnThFJSPSu7t141j1OmJxLR9aACaAvlJQSqAf
UNI6HZ3wgpo9R3RCQ+BCOPLkEv5+ee40UzzlF/xxZhzLJ9KVqvPT27MvMW9bYkUc3078g5mx1LL7
wzWWIwP/1LfKrR09nDLM4BwrYz1WVcA9qi2J+u3yv8SRlrNHwuQ2ORSR6uiFBaKYgaeTpk2e8fV8
XagEGRRzalc2LK5YquypM7plMnt0+Fb5WuPyLhJmKOIugl7/Vq2FVHdxSJR83K00IuY/vLJ6lR44
HOdYVON/WBi/PQBMR/hcSTHoX8jDTUtwdnACr5DFGnKxxGDFQdIMbLC3ZmsC2q9MukyjLdJuh/0f
aWhFn3KXlqnEdfdxjbqLevPACgP1RiOc2NkEd6wR2LGyABVXtX1xyRvAqTkd8gVgHMOsl/mUbRY4
1yt42ZyuXsbdMCMX2frpVatplna+nR9g/vK6I3j/YDZmslj7ea4R1IKWvlNspmhds5bujEII0U/N
cZjOuUFsoQl4KUZu6uDJ5hyfcmL+O6dhb0EqcvWjztvszNOpefxVBYYFJeqBBIZznYpCM+f7aBGG
FEyd5nVQGrRfEae4t53j5BqSWPXZjaXvBRLdp2OOFdqRvXf/4kvCotlPXO/172s7IQaM0jBIW6Ve
K0BNF/HLLwXV0HFRdDX3PVC5hpYi0tr3AYHHmVcmosfL/x71f3vTY1awr0Zr+Zm+Loae+bYQCmoK
xH/HI20inoSQLCxbs3UVIMM0Ucjgt5FbqlnsKggbQkxIjR6aZZt1rf7THca/jqG1g3Z0lddn25Ft
cCCefl+DWNiXs/V9biCgohX7iqAWVQ+KyY8iNVHr6nr+2A1+hdJ7VJPyVfHFK92I3wmxNAmOVHO6
PMG2L0AeWunWi9I7if/bIq1cZ7OW2PDDunfG2HlUjzL8iyUEdtQgXBZ0/2N3tJJB9LKL0TYvpOyx
fX0Wi678JLCgWZkTWjCytCGtGu2wKx5+ht/bX5Vm0mffTwhS/95wWoAba6v4CqFVcx0+8g6U1zzs
9PI+BiJbJPBcSzjIclplup+Qx+dDqWrNFoCUaqMgryDfIruWYFKZFgqFEBcXg5A7B9fCDHZ/rShg
McaXsHJZRXmIo2pSzqGlYoaxbBi1mX836g3kYqGKAmsxWVeasFHpMLtUr0E1DMxt/s8rufOLLTDL
71KA2ZN7xjUXSRvPZnmjw/ktyJSVaryj0n5K83CpZ04NxndP8sxCpBgn1ss34y8X04XXlcTZ5sM7
B15fdR/WSBITvp0gGe1HQmpCf88kRQraz7lEI7QeyzJUiTtFSsl9mYpjKDUnGE30Ts1NlUnx37i+
nPjkyLGTIxyOLMhS+30tWNST8xZ8STSalUKa5G566LnV6ctUk9v34jgPTtX8t5FnCNlLhTIkHdHc
0/ZSN4G3EUph/GNufZ0brw9nyUiRl77LTe4WEly4HXZyjAsA+BGTEv3PTQhyzJULGGnKkR1Aj+jX
nRaqOI/KJYAySmJanNFJJcSBIjOmipZ7cHScdVSImWCe1sYeIyh9IvA82Ea6xDutVxEsv4aEB4Bo
O+WqiMAZs/CmcxUoifZt21+zPIaZ1SlF2NXaMDOhfeFrarc+i/4CFaVe0e6VcLR4Jh+RV4d3/xVS
mrUey+atXGxv7aMYmxhOv9yQ89SWXio9tM/9qOmhplAcfLHA8TGrt2+/GaBObcIkOw11lOz775Ps
WQITpjfFNeb/h5BrbCNgkhsFcWMIA1RyeLlCl8h4orj6WS7pu5Fu1SGbspL5Speeet9+ofURyNCQ
SBOlwbWBdX0d2TQaCU06SyKo2sh08GCwSXfX4S2fROpK0YoJngwuy8zmlSNxn9IzqMTyZkBFX183
FlwIexEQp0vGa7l8pgcuDJuXAXA0OHt5ok4suqaco0X1egozGtocbG2ZZRTTeakHH05i8fyroDtp
ZDj+VHIJszaJRmcfWGvOPDSsUJZde1RXnbWr5zDjFiChSrKMEdDslSlXCJuwIHqiteDuM4kEwbBT
G8eZTo4NpJIoh3Z4x5+ek5BCu7JpmGXQtW4XYuHxdxOoU7hc+wEN862vNzo2J410YbainQlf+uC4
5zJK+hqCq4efc6+hp7jyGTHUaO9iXnbL/Ekbu6NBg8xTBEFBiLHiK4Hsd5bGWHcoFF1pVuWPiyDc
vcLgeWhKadq6KJHNiikopPYN8Oa5T1FLkDd3UlqEptld5HpK2lEhMQMlJQBNItcqKolSXUHiiRJh
sZ0U4IxektG2BhGPHXP31u/FUCaCIwO83BZspVKhowBv4C5lrTvtIg5CjB09ZzQMGxBEFHWHS/xm
GrgGBN0EP4i6olVryZxSrC2uoy6GLGc5k4g+FPaZOeWW413lKvuGhzGYzRvgEfk4lgZAbWz7sJmr
FmgA9DzCOlRn7XTZx2boIB7DpkWFxp4VqHwG+yKC7uOcxKcGEPtlOv4L3CsefXbCNXKRU56QrU3K
x62LAyiSizwedzTo4Lwuz8EA3uqsksGjsPPmBbAy9M07y0kCKWeyvUaCqedUiXIVZJ7Q6yLInHys
daGPbD6PKuKAEsIHQDAOQ4DA7bUhM/ypDasE2GrPaPkpRNoUJ4GBQSa/IIuZzlL0hFN1ssQM5R8t
4jICkrs1O6KK58xhmiAnvIkgbvv/S1WG3aKkzBd8NLjB76ip42YDE5RLRJG4pajR3Td2+HyMMA67
uFeEuZv2CBOkdiTCdsFF5eAb70PsScyTJZ16wVCCZ7K8XNJHCHhjlmR6beX12geetlcKLkBDHrGp
2Lc4UIXoqxrIbsrnm7L5sjwWLGnAJ510wbaegUSDjKaEEDy4xNzNmsPYRn02/xFXa+yMuG3VcxBs
auAwu+eBsxYJ4GWmSzfRLZKWlveeiTeBHoixJiGh4w+qGmdlEUpa1UjgHXNAUnkttv/oO/HM+hv8
RW3kz6z2pPXNgU0d4mVyZ2FYoo0/++eG9XMZLo6Y7efQWA0a7CREApzOwgW7C6K9p+zA39fHRdeK
oTQQFCEpXMUTl6OfGJsTC0EjjgmH1obVAKyRx2PEiVJQgyKXXXvtbBfK/vreOa6H4zdZ22UV7kIa
MiBL7WEOWQnRW4EL+9f+mZJuQ83HnXHWTB0RzLb+n1pTxV6lvnq2Pf3MmoFKYEAcyObiAEl4DdPL
LuOePCaOykkvLFZxpDcHkxMHo/Xd6845RW1Q+ShtTnnZG/+b8Dv+5nl2V8Cxev0NgACMZ+zKRTWG
JVu0A70IOTuQivOyCsMfEDwyVurNAGlFoLj4ZHy9F89W8cxG45Emqwlfz1pWvPl4i8bATchQtf3t
LwJAy9wfQNB58k8KgDm7ZHj5U5RKompmIPQ+kpv0ykBWAkQeUPlRJLczCZ9n0JIGfvy2kBDcEaW6
GAhWEm/u028z8Yrf4+jzZzBNSTfRXVyCyIYbzOLbdLrIAePdwxE0NKMS8JGyWRjYUE8YfaBa3w0+
LSXRvo+F7YpM+eAS70IteZuNdbdRbnMKuWlmHD9HFa1pKGpjklOY2gTX50BSDwwzNgQ5el72Y8e9
0iSIQePLTM8uZ5koDwzUA+TG/esORdcjZczqdlKc8YUVp+KjWLpjVJRLpN32uLYz5o/v0WtULdus
mzE4UJZ/iGyPjmiiGzelQgfJv28klA8FGrXbxDjfXBojOOBbA846K3XP1dAfIb+kFhKo0fEU7zpK
EbvfUpRdM8I0chHRWHD72sXqTx3vl2BPhWQBRKhLjnTahnbcT/x3cLAFr30pGl0IZ0ifzmuDG84M
1OTK4nWuhH35AB/spseFCMdFHmri8+qoXZL0wMGHPsdstc8OQYfulmIdZLdUpYBpZupABiNu806z
uDg5p6iTPe4VirWlZ+368CmlRx07/zDZGfGpvvOxEHPbWlsuZ7yLYPeWsRnDd9NHg1rSOcJ/qiEL
5d3+h/LT57s4PWpCIQVfK6YzAwQb9edS0XRJznCB4IizA81mUc8P3lmX7q9MYkslng2XwPvj/DtG
9PiLwKN7keEYfoTrgLhSBJM05jnDh6JaaFCNctWdPO6eExQVTEf9t3MpKIfCMsM/4iZU4Xwwhhqq
Jo8diAFnTlqCfIKS2/ME2u35cictJBkhf53BG1Jzxq8fIvIFiIFsJtmQ/3noGUvUBqIaRi976HCc
mpxAdop5L1upbSs1jTaNt7W3KpSXH4y0npY+5Umdfe+fq8TVPCcGauVgnWJegKIfe+Aq6aG4qR7P
Fv2kJBqKfCuZYi4feugdv2UXhAfUssDIL3PC3sh8T8tyH01knHtS7irfhq+HI9fG4M+Fmf9nrko/
NPUw2eI0ljE1s4X7GRaoiEKVoK5EgYI7p1Fb870FNo5DFUSfycJcoGOTqbVZaV5eAV9j3ia7rI5x
hHX7w6oP1USxOfza8+MyAyUHXUXf2HMYv3+zybeg8fdnv5DXZloZES26uff5tD7lQTouFVvlKsqq
OlrC+xhCf3roMei0nHUjl7DVTOcc3qgU/tSl/dUyFXrZ0fU2rSwdCUta5ZxqxpttBtPHpND+zwBf
y7k7KgE6Uiv54odDykhldzeXaq7PGjjzIxMo6Y0mTcqb/eUeIcr+sndE06XswEb29T1IxWY5nVhC
HO6g5PvUsStI/YNBL87TbDp31LFwfKIT1zbtqkXiIyGhVIOJBdIU0IiNuLdu+FusNuHVkObA0A8/
6T8uaNEwBH7iMW7afF7sz07lKQtDui0Oj0ATWIsG60FEkAcWG/fRv2kah7ZdfukPyKmZptwhib//
fmmWAFUvHXyhB7oIVMxF/aZmZoH4Vwnpbg9whEupgYxygLKVlDlRNjPfupGHd/EdZ0dkQFuSTqVA
2r68Vq84ESKk4ZfvoLHDpojaipmq9JwuygnGu3D6PkmcP60ZPOoxEaZVpTV10VNGn3E8yhU5tfgG
+8h5hclXAegDIBOps0ag7IPcmM0vSUI3kCbqF4hz/hw9Cf3oqE7bisXYycCZhVyCikzCKC4xTHUx
o1qT4xCrritLtcYOgRLBVlWEJuIM5nEkDGqQH9ecZf2UEDB073LeE3kkZLwJrceZhsJGAiuH1VB/
wSfm5tk+slEYyycMPwDyf8vMRGjSUoxeZivNBlXIDvgT5ONCsQvWdHyRUfEs+6pVdwpvN+KALscp
EyAC9sUSLguxFs1IrBrEYBVEw5tXAPeo0V4cKbe53nvI3QZvrBFSPPfc86/iiN7ry+pXcW4tADIB
HYPumBd2vpU5TQIOVSc0vRsATkAK0b003Z8eSUk5QEFdzWGEppFcIzAQJauNgWbbFvOjqv9/yh9r
oAQaIOt65Zee6XlrfMfjAK91roebscoJ3GBgVuLN3DYmZ/dG7pBUB/kpmHOQSZh2SorrJkkkIlDU
h/t52awmKOGZaUNORVaISrNYLcPMmvsVo7nZTh3A0/YkeNnInIFl8zOqytwqWrVTgIPVd5vQEovz
4oFrzpPHwC+ynpBUc2tFa5AQFacqJ0dIAsOKydHXP79fEZ7xvAC0lVHxlxyV1e3S7J1hvHqirux8
Hc1ucqdCsjEJzcxI5Lj3tcMCTRzWTPj5uwo9tR1Ughl8/+j3Fu1PluGcDx3N9SpDcBoetYwXlsaG
92PA09fuEpodr/+z+Xd9P4+U9/hydAcV38tlDgwypBXS+1vAK15oQ504hYnMQCFw8+JEZGLj3swn
OdlxxWKN+Er1x8JpI2KzeTXzmya8h6F8s1Vv1umhHlN3TUXrqoIB/jmIDLm6spkEmt+nM9qWX1vy
bSuzfG1kR8ZP9Jc0JVtuqbklFPj7MMrXtK1nPdwX6eolmonZp4kyXa2yX+YrN/71tEaeyrW2mRAu
A9wXC5WDv2r4Y1J4DDHpaJV1wf8rrNzK/KAMCvyGFNjfP/KqAmItpVXV0rhVc09XhAcO3oRTS/CB
U4pWZNB3DTEUHH19RVTKhYP49limtl9+8XNEu1QeIULcQ2rH3ywcpjXOzKJCMw7jOgVegUwrBMkO
lG4MMFZLJzdyZr/8n3/wHfjqIGyhzp21nfTXyo/BYRbOuCPt+Vaz3aNBcmChaM2F2fdiQB9j2MeD
UNP7tpkampYMljI7YvQcjWrP6CiF1nVRsbHV5tTXHSTQT7JzglkkDmIwrLhsPzAl7k3lv6WUhnYi
3cdt19Z8hT8JtvecpN8tV/wPblnDx9wSFE2+RMWeaVl5AQYnnpMohcL1NXsDoeU0WOdmUc4lib8J
J55mkYe2lUyDjJOq1CTxsQ045irMladT0uBy6JHpIqXr1MX5+8GxjePKIiCxQyztg9OWK6/nZbZM
46Ua2vz3ON4e+OX2uBHFDb0Lop+S3eLeF8w3sYQSJN2dPx2+qWHKfKEgWSJyCEiUyFMztb5i/AqP
HMy19i7vsX/bQFm6HSQjNf5wOr74b/pVC+QMhNyO34AcjiF8YqL63sztc4lTArKeXrivZK0W1HsZ
xTK+w0f8i/t3pbNIMYL+gHjNIHyjBvKaDaiEF7TiDLPEl22coH07mQZ4lpURc+EyYKC5aWCrZsra
kvITkJKQFpPP1TqVtaSWqVG0IdcREIojE5JgR3T/4fYutlUqbZKw/jPkz2koH/s1tW/9nXJiTzIk
ljIiaPuUDXU2g5oRchs7J+m9KczQWC69NMFl1+qMaNz2KI+avawlrfXfHTH2rmYUbDCNTUjpw6r+
bs9IF7GN2uCoZaDafJzn5cHZ8NshXUSmuEHu2s1+A2E/q0Q3SJhaLMdmxzE0hL8lmlgq0P3NHqZC
5OjJE7WbdPro27vFjTa5QjG+YJK8hbvMicSBxg/gmANLd1kyRyHer9sCeamnbT6b7SCJvpfhCD+J
iY+FsBuaWp9Suyvib3L82WTVrN2eTjghdAyv512dA/3nkmzjfqgzl78g1cfXia+3U8mOKMZAkv5v
WExYT5+UhL8yECFyJT2Fk9j34R9uxFSZjnaaEBPP7gds0+fm9jwP02JDhkJvDtq2UOxuJZ1RtDZE
MwMZbO05t3MMA4fMQLOk8GY+OLToLzy8IkuGeDPqekv8Eac4CrTBaSZcpEBqxti2NAPMuC5PjEUh
lmNfsSAT9nsrg0iEEPdHtFkrLYZexGBvLCmVNFoxvjQfJWt1pQveT/XqEJB59rZ2cR6sWrWPMqtA
ouMww6PSwV8PMiJaSVG9KwxgEOF1e+dBVe8bR8jijW/b+GdZl/7F3DsJqSr0JISAc2uHbsL0uu+W
nPQ3nzh4U0V83sdd/IaA427qNHbUjqgIcRwS4o0GpNYiJ0sF07EWk3OOVPcL3qwHCmCOc6HnXcXs
3TCWYVimrl07HUazM5URsqYhzMVJJbUaN1ReIVvhlahtBoLhsD9WOdGuSIhYO082TTZ9P9Z3abBf
PaKADiEMoFMRuI1Yd8fVk0EKK3eL7wN9m8X1SgtRv7Sho80vyzE8TNJyVx1i89HSU9INMAt7UY8v
JafxWfjpjoitSgDf32ewngPzJsI5BRHXaaZFcav3FejKdw57b78qyh6r65X+JzeQ43Aoy8NXArNX
XQwsicc/WcT2h4B9pWVL8NTbMYtTaVC9+xVqG38mAfNlGx4X47Qiwm0OQs5UQk2eHo5jCidaSdBJ
9mV5BP+B+AoKqUrfG4/Wal93wkjlx9986l55QiglB9JKkPiXbBhOFFCmx11CmCllSRN4fFMOZYNg
UAgQaH/fbCIfglbpwg4tFJ/OdSPGQwZRS9qG9RcugUFMDACkMNmKkRJevd4d9EwHHgqyQaSN5DxH
lIrD63ofnO/l6eBCQ4xMjSq/Q1ElrYEmyn3N244WDgBUUjM358DG0u1aQx0L+r+5LfbJQRFYsQQ3
GkEETjenG9QoqU9m/m71R0hfWdTWvHNHCopt6Y5/S6aHD/rR1p+o91F1nl75hAbaVqbwlrK1EIM4
NYtFEkBTMvBhM97Z6OKJpqr8QYOc7DpEFBh+J+2RiWw6B1Yfs26+s1QlHEh7w4SDDx9BgWqZOxgi
8+BIBGPfP9Bftp2ZJ3DXpTTDtoeV1tCVqDjla8ZKBI4BbmKTk0zOigMd5q5OF6AGUa6WWt1Fg2Ip
wDNIPG0WtakmJleBwveW84wQC0loSLHVyjdEerENpSp9/iOFCuA1iiaeC4Q/qKQE0jSqWCYWgl5E
6RhH1gjY/k7AFo9w9yCv0HJF3lRYzNm6xorSsbtfE+EOghX9Kodwgt6lHHsM6TeqC7Q82vgN73K9
xe0C70EdMd1DIQQM6VCD9PCNWDyU20JBSh9pNxS0FESYHdXNoiEQbQeAHADs6dO7Ct0QCFC9FWOV
eaeBqwHovIWHEN1gOHSZ+AHVXnk/iXoVXOYQeiJtckLDmbwrKIkKMyFOxdBLZY6W/1+1Gh/QffQf
i1tETprQsJo4Kafjcvp+gH8FW3khfL/h0WS2LZ+aeMvnHmGt666stfF6x+ZoSwNNwNZeDuIzzFZp
kLXBkFb2BOtd4xN9OmF9aLbI5TNR7sDr/b0dYBXiqFxRtLnZW6tudpiWEzwmxbb1yzuSXJoBcXez
CmAAz0GjDJDXW2Sn8hTwDX0ASYkRcR9ZlXnV5mPpXQ6981dlMDZRjD59QJlFwg+9e4WoheIcLOLo
Ukwmldz5hZCkMgY9/X+2fuudKTSNGDBJ82JLH5ClQler2rtoZfw6+Hgq3rrNnwu2yzWnHNGCHgy7
1Kihrj+r1jyf6Q0aLa+xeCoZHQeyEVmKmIrTp35tg1S3xBYtbcwiWfa9GvIQqE+du2sb/wXVgXq+
P/yHNjw1ITpRtgAoKO+68PruDSRbxArIuXBAwRxLl81OGAOEHFqWfRwN8ThITUi8rwdYXqNvuWpx
giJAPQn6Nc2H/pIx9YO0wKijx+eOHU3pYl4v3NpdQdSqrBrAD5jwogZfpufP9F5dKRcK34dZXJYD
3cnbQ8Ovlr8x3T/6/rbFhkmCW8Y32Tgy+WU3f6ZPUaudhI/lQu4eMgL+YOikneFYuN2f+hEj6Oc1
7+wZtTjnGaywaKWsYQx03FsdwmmepmH8FWkjWiqmdh7GqAaHF7kTrDAdXyywVsndzNAWzNtP/0rq
BLLRjKlCJDQWIKFhCQh4obcQczEtJH4ZHaiObRoHg3advzGoAAslHRBvQvMH2O5gqhHHLVTyVsp8
xTOnoxALJLxmEendf+mxzMJyTLM9V6UB0vPf8ZeCWjsa+0fDkP9dNYnEaFNunRkQBMNrpbxfueEq
FVIi7QCb33yXMHrVyV0IcpzI74PUL8orbirbreERiTE4/cZMwpIFPPEL+tHUedCKXKcECzhp+Iqk
ViNDe+sI1Bl3qrQIiU8UV0Kn3r5aszukGfWBsen6DIa+AuRVITdo5zqQFSMl+pDNTVORzPAvvWqd
WN6azo8Hc9zq7F5iscVOgz+ZPFcTW6mh3JlzqdiFA+uct5Gbfq2FNFGeWuNMx2XXDp6uoV8XNHjO
Dhy2rm/t/InUPWHLXXVjQY2ewZPoiDAtzRrN+A4M2PAKtus8Y83bakMv4hKVu1RbTTUg1/Ky08Dd
XungGrqnYAvBuZZan6TGp9T68QC/1CMaH7vKc9tYs4J3meyujxtxRZKzxpAfuL+RgOtw+/GS07jq
UIzheTA7GfWxt2CdG6EGy5cGAJG2qivjh976cmbseK8dSqaVVKakw6zLtUkhdq4jhXemJYpKS8i3
fcAMAKUzMd2+Sf1D7AVXsZ/imywsdYobOHjKI3+1QfTKrSnPEnlQlbUHcfEatk2wdWrt5FOmd5n9
jkObTjAnilMREFjn7xqWGAgwc5dZSvHD8WHlidIBm4f6mTF0BfXXtcH0lRGVTBMXFDbmObgvdb6/
JtqQBEnurPR6uAyJI6knbIaxvVkjmux4UJF5izFMg/3UNgwqVihMuo5PWf5WcWGAalaNtsPupmZK
POHHxs5yNPtegLpn0H3OVT0Iv2WgszrOGL6yypf+WxMKb+PIYe5akZpPgUyaXWO45vwJ1Br8gzJC
zA2+ORwkqMEtX5EcgjdFX8Hdrhd+CrnIYXbv1auaSCrRiQwK9LZQeVtURenGio+b6tdL8GZeLmUJ
PWnJ4jm9VJTFQkftn+2VylCElP4kbRpD59yiLMYuyU5eWhMjjOEfQCKtjGbkpGM2Cbs1pzZLnfAM
NcaYvnL+VluzkVLBIVDNjD62s5WYhxiy3OOxfy2jwDMr8RKCQd4OBpuGkIEA6lzY/BFspmO6B9No
snli/s5bIkM7+jkxqULEjufZdaqBqfnwaqNmyN73j0sSc/ayZiqTojK8A7UtKQLTymuG8d5HZ8tk
MWYGAlAvhVYtiFFnsj4ou3Hi325EvnkPIrrVzjEKSVw9NdByeUKmPvtLYiyFm7tJH1UMaU4352TX
lUV5rurP/En4hGzsMTiVll9+YGIQy2kA20ytEohfyY1U+i+ZVG/zh+R6BqYrAYqGpTeI+TfNDuuy
P2zmCnoasuzqyz0AhfcwoG4vh2T3R/hzsJKhES03XWcFW+w4D5RQJLhom/0bRg8MLrl3NqyV79Go
8pJqsYbszobJdkeTFuXbDkYMZtQNsa3J/2S7fYj2Ae4NB4Gr8yOFI0CSRkBxUf0mcqbxJIVm3N3T
RQeY9/GJydGDT2qd+lmKu7lNFzlKWGy/q38IjKlHq+4LuA3j93o2cnu19d7FSIhQ2AAUSGpU2Rrx
RBLjUF3EzRRX4u4iL2gcQWd07cGRqQUbZaL8VXKRB0uZPJ204NGMwDetH1ARp3ChybKj//dliPfX
D3O6VYlDZFVaP/fEGaBUcisSKfcjI6bud2kmj9ZpMzYaPMQHpezTloYaTTuhs6yiLDI0IEZs5Rfw
d++6VRPWBoSi1byS6ODWB4yhl7mYqw43ReynvhV7qmnt0KpLyjnKjqzCvbx1aC0etMx0a3bN40ya
EKXBIItyTJtiVq8sdLn3mZO3u/k6RLkfN+gLuudjFNNYL+OzyreOYe5ywygdn4pIpkO57ShYA6+e
qEr+OMdjw6X11s7phUcnhwuOSz/axenQfFeCccEuhSGcDHfXKD02b3BN5kXDlpcNZ/2vpDe8dgcJ
Y3kqt5HNmWzBqtFRn9yge7THCCMLJk+F3kwLvfdS1s1YexrWoPzKG1LKN6vQaGCGrDPGPdTASlkF
/8TPzssyWO48S+jizB1gsx++C8X7Wbi5lprzjyD2qBCkSuI3L2F6kTf0Nf7SLVRmpQRPckU0caPh
jl0+FBQZHc1gFBhLT5/xpamXL+3r4tCvBNJ/9WDCqOwasnxtRU+5Mby4wj6h8lkS8Fm9jW0pdASB
6OMcUerYoF/kwKz7UW1Tls3hQ7GnNSNphVgfiP4Vg8PJS/0o713wa09T+E/pAgA8/SeNYIfJmIy5
U13awqLXY28z/y3WGo7dsGf9sW6Ybtr97nENEV0NN54lqxcoA0ITXX02H7ybe/0PVbFRqNao7hP+
82PmoJwED0EltIrO6L4s1ucUWmM+bKon3rGualtAKVK9SGcGB/INaw91EYqTmnCvhZFg48+GzgwR
8vnxS0/k6KWbaN6juw67qitelX6TqqpOf6QHdVfnmEIaXCffRpNBXH1/dWpFeVoaYtvvOI4Q6oTg
wzsXG1ILRFpJeOh6KCBgt2WSZVgruk8E/g/ODyxESsi6evfmnxCAsaQbtgBqV0ujiVde7cvkhYMR
I11xImNnvlhWO5BHwFhwaSkplgnf/bLGL4ylx/li3cHAKM8yjH4XB9242eCvOntCxJv8GMrskkMm
94BHUHUaErFOS4s8jkJZn3j1N5v/QsjIKlmFNHvvvTHfppa+H5x9TazrIs4vwmdqVMT2ewk7Zg4f
yjQf9yQ3PouOzzlJfkD6bJKdznoxc4pibEvcP6fz2zIf18b6XHuiqkXiN/cNXcdjX/cR3/ybjwOj
4tx/ufBctyX+IlnrY8frFfOQjeyHNfycZM8DFVCqO8d4Izvmljmh3ImOK5VMYGHAG67YLrvaX4Eo
82XyRgMIie+2973oerVcpFMlxK0Q+HY4tx9Gs1G2TAh5Ej9uSJfGahTK8Sgn+tmmBJ2HnHyxakKq
UbjEXcNf38FQEtQbyUo4ZxOa3Pl87+9+4JUPJ3XPfI9hq9O7qoYbbVFJIYdGZfWoGaZKUBceQvTt
tvciJyXHfH3n3PYnZ2lT8PsYRLAZv23CfWeb3HRjuDHGTRyjfXZO7xpaf0vibwndtPB+swGd8cCz
U00inamzCgJ4CmD8epT37pebowS19OXWy8CTbZMJMkCvmZc4wt1Nu3EVU8hSjME6437RHyJ7l3LM
J8lHaVMb+asC7TjDKiDpsVEai9vobMalEKAx68Ggr+U18eHGcwkw236D0jkFeKxS8N78i8sig5js
lN9utRD4X/eojGUzuxOLklccjT8nSeqnRyTbdbBWwvwE9V9Sn3gG3aRC2NhFcHv7M3Yr4TYx6bDj
v8MdVnSabiX+LcZGHBWsKBrfYWmeuJFB/TxuirtTfIfT18PUEkHNkN4O/3QJnicGuGIP8gJh/XOE
UNSOWhPa8lsgaiJwmDE4MIsRoTP7q2k21RCPy6INt0IBZzeleHMI5D4YOVFHgUZfsTaT6qv//iHQ
dC94TEvtbyHSaCLAW4qBvxoQvlJPeu+Er5o5+b9hrtKz+JRRWCBbbJZot26kUgzzssBAKaFyKPYy
zTmue09Y5GngvWMfcu+2/gUzy5JC++kf24PjE8spLGSRLZRLle1KDt9uN0K8wTDIxjE8fDvk384o
bmLThZ36Rmr6EURKycBXRXbUOMkalHr9P89ngg6KWST3ZZgLd6+f6qNX7dJB/gNCWM5R4NKkl0tG
Sb+euptdN155ITBGYdqf0JWkYiA3GLMCSHgaLqksQwSTUJoKeQqLfeMl266bkp78ZVKKqxk93aF7
8Rd722Fm8biUO5tOVwOAg8U0/78EwLnKykjmpoipFOBVMtTTgDk+nmn37i+FGmJqqT7KPF0dITLb
Ymd/8LaifwxqOyWxowIXh8jU6eZIQKdon25PzdaFV7Ok2gIMv47WiHL2A223nE7buQJC4Af6wP+i
C5zYNh8P2K3Iz7H1ZvmzmrTw+G2pXqzlUFgxr2Pa8gogkTMvEpsfQE1f6VrKNW1clLwDfafXOwtS
QrBv6rnm8ByYe/Wl8+d1iOdN95mngBzJYqkz/weXzidNAd6CngDHFlLHHffAVMrSuslEqJ8X209k
0mxy1yrW5cgnvQPFlAKgiPL99QESv5pGz/9Cf+NZ3XY/PsIwB2vOm7g5a6rh2r8xYKua1wOp5kUU
8rfD5KEcx8w1nrrVDPbdSOVJhX1dHo2KwivkFDArHHXgQyJ0QiXdJ2xGXxUwrBx4lIjihrfz8cLx
PXRnnYTWYhObzlNu/wynFbeBvt2lB0wwLYohd33zY/vX0ykH0SyKrlieIUR8kQzoyfx7ZaiNp/9M
vF8/Oh0JvDKVDu6eDYvfwQ5Ki72ghcI++vejl3sfgbBeJiQIRv/o/pHLvCSUayNFzjEogGzSNkMH
/3UD6t+D1J+3vqIFfHahjOjm4xSCjeRUv76sM+NNhQX5CoIjgkAnK5k+rYu6sp7kuAaLTwfmUr9w
yY6UbRm6Ui1nIZa8lZ5AfL3PfUO0sm84i2BcD2vAkCZuqHpOybDZqQA6MAHB/7G8RBGInbaV9bXk
uyYGyq3++VbJ6RyDqWDWgMFGmXD2ZIhceFXp/uEVC3D+F9ofVmdYuKG+HCtcWc+KB6NSSofORHDC
aIt4HD+330qAJRb2g5IWjhYneN+79UudjCLN5dDCjf4EfdDCKWHWfgB/83HdhVnpMYfhVRqdUpTF
gq4KThvc25D18E2Z/YbrOxbi6viC7NUv4RRNA1YFy6q0plq8tT51Q31VamYcqrjTTDcPhmzS1eJI
nOh760LPQmAOx3uDXr7ST4Meoj8X2WvqPhAg+lARfxDVkVclZPWIHcpgLzYBQfsgkXn+E2QfBBOl
wYsLIAtOT+SGfjdQUkXshvbZg+6rGIveAFX/7RFoCg1gSYjKXhQeQ1la1aYGXC23doVXgsQKE/eZ
9gDEUVI6afvWkC5D1dCUO3RuMw0eVA3fI/sI9qII67u4w3LfiRNjdGnQscJm50YuI7GiQy0uo9wJ
pRkV65RtMD/02ExAzHIKoi6b7GeeHwBGhTaZqhzleTze2Sed/9szGw21rOYi/o5b3aNsh2hGHMYW
b+Dx4zJCy2/v0U5jpBb5hI0gX8kdOI7ZFAatvcn/uXoB5iIH3Tzv8dhzgHp9ILrqILwyP3L2IEq5
MHcppJVJaQW88x0FX9xuQkBCVvdx5XA0QmU7aoDBMIeCVxRJXjxkFFLRnSx7mh4RZxJDzDVoMffM
wRQEZOrvCtE7opk7VOF5JnKq0PTh8u9MeteUvfVbW7MQyDiPx7VRdWjt9S5Mm9ONGE1gB8h7oSmi
ItR7xf7nGLXXpDOxb239c0uewdUSzG3d2zqbatrGUN+UHFR166E3e971SomqtQ2r3vwUXz81bh+D
PE70hokSMgU2Kxu5ArB0nQIL3PqGFvdTXf6vk5+/o3yCnpIlj2sY8+ShriNotVW7U8Xu5h7AlW4f
NzbjVFiBbN9dvr2rM9S4qeTXMi0jR9JGXH2M4nUJDt2GiuegWOSyheYXqe6A0VTwnYzLqoBlKo77
vikD64M0M8earb98cDZEoaWpaVBVbmJTvzANx3nV9NupYf33jAZt0V2T23i1ucU/s2dyCaX8lMAJ
P4lODrwR9jV0TczTjmlTjAFTmdVQAgKotxzQe5iPdpakh0Smgmh35YcRDga4QsUiLv+ZvFkkdiq3
/FuTnkvT5T1aQr0Ccpt7pNIUE1VQ+r/uogyX3jcgPC4a5ybgGnNaPSuwqRyT/KQHyDYvwngpvy+o
58ySrx9LGfm8nSvWVMpVhoKkZwh9wPGWuaiCljLU7YKhBENBdNuQykQkzDCg3jvb7W72x9zT1VOA
HfWjrflPJaB6eVMwVdY+dN0A9d65LGxW0JNk1tbZa1qQ4YeSaW8LEEdoWnVp5MS3dbosA5dCoqJh
FHUmhXzIWnIrDwGIlsqSo18efJEDKkYYjx75rXGTGU/qv2H4T5zZbVm5jmJP5uUOAOaolLENvNwY
OkQvZkI90h0t9e48x1wJoTl3INEMW1UMk91E3npkq0DkL8la/XQy2VzV0kTqPuY9UkknyIO2CJkr
ulY1PcX8JAUyxIwnIndwZcvwF6kpqptdAE0RAltkCmOi99RzMToCA2k1dT7Au45LS2ECISOJEOrZ
Mu/7MexJsXq38Fiz+w8LxhAEJxs5ZWfsDIu05im1h+P3cOc1c1xHIQHn6oB7lcg/COCpmiHus2gn
JrUuVruY7FKTFzGP+uT+T/5rbsXIYm1OzQgkoVYCyYdhJLx3yye2uv3Alhzf6DDcsWDQqJQdhmy3
eoBCaF4pWQqy3U44AtJhrNpESp+1KYsFKFQZZjYpm+VI+a+1pKjUv3qlc4tiazpcl/Nox7mN0LAl
hIn/A/Rmn4gMrU/HzQkuEIzSMJt/Vi3N+plgNqPZVMvK0WJP3ub2Q5zxUI8Xe5zRVt0TK9ZNiNeV
tNkmeP4wXEJFvcEmnx/6S39HS2s9V492bh0ULkY/PzLXVVheNSxME19ELGd3xkSB7sGJ9ntrxG21
4XXcxibOAwAd6AljO2u3rmdnC0E8C87MUPxLEMjiIVgH7DKnZhxK3FU3hIYR5+ffx0BotQRVHy39
haHSGxiDTqNW+vujTktAzfH/k+Ag8d+2rQaneWb5RZ5GV78AjyvBAUa5tVwaNJprql3gYHJSz2Ex
3+8w0XSZz4Q35+EDsvcij1tbHYRv211XyRLwS39OBZTUTQLdCqYPdS6symTI5Z1SFKTr+Lc0/ng+
RkwBTsx4HNwRtJYjKAr1wp2wBrrj+xDt+LFdaV/q5sZJ7zn4eB5t8GG40plGq2uh85Fbvjx8oWKD
kAZo6lJOA1W5dvj1adiy10FXCDT7NYNN0DllaP9g3IyzsqF26ow4JQvDoFOnPQoxZjYsfwi49okG
h6SUJ5n9555lmB1FfCArUxbplqdRUVLT42Miv+WpeEQAcn0Tqxiye4YuHxC3bv6Vc1ciGmiR0Q7X
4GbEblGjMEvED+pJ744FjWPTYUDGmcdGDfgJYK/6ad39gK6urbXirtsMDTf/yrJCZlQnamG/qzLx
MyAlV3ffc8f1grw522TUzJ8iQhUXgd0XQSIyEGm1IP1XkpHvCPNDFdnlivOYT6Tj1L+ACoroaCDB
zKYGo6SkCNLMDHNz2l6S6aCe3gEgAc5k6YL0RhWqGTpEGNg47kELwthjcKHg1hCuDuJ54R6ptf5z
BHRw8iD4QgLEh6OQbwul4+C8Bfq6EFEl4eOgM5qlRuS2HO7zeIxQe+8rFq0LnW+N16bdA9twVNUm
jnRdMMBRtDzinn3fXisBBdm/FYPOJRjPMWqO0DX07NoUOe+e0LNnJ2DbpdDT1oZ3tF6frhGcqc8T
5oJmKVtZePMr9yyGSzcKeupXYIjCcT+UKFcZK1WNRiRkwGrNbXsZLvuR3jMVDt3pHOUMeMTH+pLJ
G4HBQhhYdn8gT96pvG1tkXdCzpN83G3YMhBPpB2LMfG9RZU7zJM6VafMW8ibtKLTf4hQvQOyB50b
K0E0j2BTkHer3PW5pH6WHdPcXGfi/B/jE7ynkiYUE8C1/+Px8JGDjkwc69AwpI0HczzKLh2iTGYg
TRy0zA9RuJLBncihkPHUNd2pGCXewQzd9GMDyMPxPYKsQhwSJvmgs6BSW6Y2FQeJRVl4a9KEZ8Cr
TycC4k/5gZgyGAj/omERb5Bl3bf1FGutdQkXHFilJZEXwISeu5p2vnG60HdeeW2zAI4Dxh+YXXar
/jt+cg7AwZ1fYm2XuTqQnIjyacHrtfqmAPLDujo2clkjqWG92RgNJOKDc/xfPxprC15KhCaOIfc+
wReDf9Jq7xcexTAXSn0uH01pstXrfMxkgT/tBX2LRlAz5Vv7OmHk4yAMs98EHxUGQ79LIgbW2ADE
gg6CkoI2lzVOYxtX3W3DhaBf9F5b/S9tuXj7PQnCgOiYts9ePZii6JtrFxf68ietDoncSaT7BpU7
aQKomUdKgAErzm4BBg7x1ikmkTWk6lGZ2cuLwVY0iWqXKZ8DjHTso46k3XHOpXZzVv7qPw2dnik5
/GWQJZumBDw2IJYPNUxyQsji9Kj7rvzosDhnX3rHgN5zP32Elogz2SRo72Z73M1uJVbXDAcexggy
mUnQUsajo71Yjjz1p8Euf37E4r4AyxOpgGgJh9ggYMELjvcWjVvklWIwuBgRAR4ZdwSxXjizZIqb
TaWp9/xFa/Lnsc0DLzKDV3ECD0lyJv9USg87rvvJVYZqKltI9kBQSj4psAH8niC1zVYACsUVDIBV
NqkPpM+XPSLCXehCks9++ya2gxGjuLIIrboz/yHqeWtQbkcByxu6uolxfixC+GamhMmFwinScMcb
9V2lmV4hqnNpuq/1Q1j0Ypu9QLDYxVpQ9IjDo2Ez65X2MquxiyzTXZDkQbuAuk8kStvhZXZN6J3u
AQuxXSsJU1quJR1J48+nEAI5SUw3gFkW9yA1oUxs1Ybq+mcGwGhbvPReKwhZUE7S22728GSIUkV0
n8uog4gCTgMaCjqi+Vs8wbcuLYZEQXVkH9Z0rCzkcIuiFbGQeazxUyAVBtJz4W/r2oPJlCs0CjHv
2KYFyKb/9awsTFpZ7WkV15LZ4upO/iGUHrXUBmIp9uaiSovi0ZFRf73qaJLmsym4/VkaIeWL/Hbh
7cTK6eKSu9KIxHcXYb2hy+LZidj0KJCBBOD4SIxY0q3XiMsMT+TgDC8+DNcrzTxz4LCQs5HUOikG
2YJGh4Ddeqr9St/oJ9Tco6Wz9iJOn9Tk+1JOdLY5lc4M6M2EYHktzpEBvBqUOcg+7UEeH6eKVo3s
rzO3hwuSsO1zu48A/GVoWyBZ0FcaM83z8zDJ0hRb9ykRAIcGLz0iRlC/7s3kmNQTjMEJh7cKPwnx
Ni6vTX/m+8+fdVTpBFNmlMbHPkysBqBXqcXZRDLmKmTDR0kE3TxYRtk1ArWoCk6ynM14SQDmzjHP
+wiORWKfrVsjHC+4qOcClH3hyrf3gqlm4IKnW9M4wud83JSGqbRSIfoCbAk5URdPft8T0bZu7qKv
WUCesgL0nwLL3+SCkO/K547d02ARWeh7n+NF5vSXFzztdUPN8LHvNsOIhdwJ07Rppxe7JZnj/nog
TrTSAL9G/PT1vdc8Zg1s1r1k+R2WpipphNrV/idp3E3961gE4ysHdAzbWqnmNF5Xvz4lFVlHHp2A
QvaDhk515/2bQ4jU2gRFiOWHB748iqrIImXEIxZx8hjmAvTitZRcfEgMf7yxx19ylJ/V5xidh1lq
1ELdyeJFM3VaEtgOPzgQAXwDOuw8fyOJmttLXhAFG681mN0hdBEc0YeaP2Ef2FTuB4a/NDjODlSr
5JN9SDdTZJAMFxdwHEd7seXS94jkp7nekn4gm9HBpVtngpuu7wQzqDNMDLCt6ZbjBFf7bBnz3o1Z
25ljVMuqs5u2Uu2Mq1CfY2pN3Cin8EO4JB4oAbfP8MbqJxW3VI/8HoQWTxTDmC1rDXcC5oeay+iw
IWk9+S4nugSrUN/993eOVOXzerCG13ldjNfQlRheDM2XTYf6PP+/JyLKaEl7WG/AXi6iwv55ii19
WmZI1lwR/yWbO7L9ak85ZSVA7DA7rtCUHupP+TvDXwVdonxAeCVoBc8WTKRs+7eWuhKz44wUhb/b
uzcwrzYEzFD+0GMJ10z5e2ShIA6KDhq5FqtHlgvdDUUI+G8cEPE+qn09WkuVQilIOUBJzedTB2cS
5L65htKEV7Iq7pbKIjM8RzVXqvW7hiNS1jIdu/E60iM1GXgQjKAqd2B4zqoXab957cjiuFpvThm0
wJ5a3SHugRRzroEQSqaQ3ilmEtR8C57tVz5Yn/R4kJGQaOTrb1LqyTj2CJwlyb6Zh7vhFPcHa3sK
uPSVnAP7PGOfMNaAiuVoae3eKesBibIqRfrEs2Xno+bdwVG5cbpNt/4IzysHfEU6vjVg3WarZPHY
KsqT/ZIQ0AB8fgIXUpX64kUKihuPD3sIx7aMjzzD/rm2kvbdZlq6mMuHfz9G5yMtZRnLP8eLKPRz
JWGSYSjDzczS1tnSlmLdHwtQtfHKy4Tx0aaaHUGmEX/V3pWUxpSSj1xpARBE4p8XR/sCXVaQI8w/
SUQmIccsjbMu8K2D4g8LWCluQAy3Z0O64hvRkW0VzfIm6OvrcAVtFUtqhcMcsuR6DDVB0vwrcngS
nKLkZzwBybey1to44mTbRIUhPtawGMvKyB5UikfAQ1nACQqryhi3q3zlRlPyMTRF5Obosin2bRdQ
VaOH5JcIbOO4gmHpcLlJZGpvcu0/c3iw0RFzp+nAWZR+OspLzK8u2bTAEgEXmdJu1k6z0mMt1xuL
Rei5KnQmAmO7u5cHtke94PgbXKtDq7LA2H95ENa0n4ZvGs9LbCpS5H5Cd5EMFN6xMMt7tOflHOsP
XF5Bk1S9ypLkMUi0Nn3ukcFTo+ynymVCEaY1wiesT8s3huxr2pNrSEZjsG7SQbpt5kydTu2AT1yE
4mC41osDe0dKnnFdqkxToGItWdlJwKARd9aNq0HcPw==
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sr_ram_commbus_c_shift_ram_v12_0_14 is
  port (
    A : in STD_LOGIC_VECTOR ( 3 downto 0 );
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    SINIT : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute C_ADDR_WIDTH : integer;
  attribute C_ADDR_WIDTH of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 4;
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_DEPTH : integer;
  attribute C_DEPTH of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 60;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "./";
  attribute C_HAS_A : integer;
  attribute C_HAS_A of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_MEM_INIT_FILE : string;
  attribute C_MEM_INIT_FILE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "no_coe_file_loaded";
  attribute C_OPT_GOAL : integer;
  attribute C_OPT_GOAL of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_PARSER_TYPE : integer;
  attribute C_PARSER_TYPE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_READ_MIF : integer;
  attribute C_READ_MIF of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_REG_LAST_BIT : integer;
  attribute C_REG_LAST_BIT of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_SHIFT_TYPE : integer;
  attribute C_SHIFT_TYPE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 1;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 0;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is 2;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "kintexu";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "c_shift_ram_v12_0_14";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of sr_ram_commbus_c_shift_ram_v12_0_14 : entity is "yes";
end sr_ram_commbus_c_shift_ram_v12_0_14;

architecture STRUCTURE of sr_ram_commbus_c_shift_ram_v12_0_14 is
  attribute C_AINIT_VAL of i_synth : label is "00";
  attribute C_HAS_CE of i_synth : label is 1;
  attribute C_HAS_SCLR of i_synth : label is 1;
  attribute C_HAS_SINIT of i_synth : label is 0;
  attribute C_HAS_SSET of i_synth : label is 1;
  attribute C_SINIT_VAL of i_synth : label is "00";
  attribute C_SYNC_ENABLE of i_synth : label is 0;
  attribute C_SYNC_PRIORITY of i_synth : label is 1;
  attribute C_WIDTH of i_synth : label is 2;
  attribute c_addr_width of i_synth : label is 4;
  attribute c_default_data of i_synth : label is "00";
  attribute c_depth of i_synth : label is 60;
  attribute c_elaboration_dir of i_synth : label is "./";
  attribute c_has_a of i_synth : label is 0;
  attribute c_mem_init_file of i_synth : label is "no_coe_file_loaded";
  attribute c_opt_goal of i_synth : label is 0;
  attribute c_parser_type of i_synth : label is 0;
  attribute c_read_mif of i_synth : label is 0;
  attribute c_reg_last_bit of i_synth : label is 1;
  attribute c_shift_type of i_synth : label is 0;
  attribute c_verbosity of i_synth : label is 0;
  attribute c_xdevicefamily of i_synth : label is "kintexu";
  attribute downgradeipidentifiedwarnings of i_synth : label is "yes";
begin
i_synth: entity work.sr_ram_commbus_c_shift_ram_v12_0_14_viv
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity sr_ram_commbus is
  port (
    D : in STD_LOGIC_VECTOR ( 1 downto 0 );
    CLK : in STD_LOGIC;
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    SSET : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of sr_ram_commbus : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of sr_ram_commbus : entity is "sr_ram_commbus,c_shift_ram_v12_0_14,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of sr_ram_commbus : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of sr_ram_commbus : entity is "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614";
end sr_ram_commbus;

architecture STRUCTURE of sr_ram_commbus is
  attribute C_AINIT_VAL : string;
  attribute C_AINIT_VAL of U0 : label is "00";
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 1;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 1;
  attribute C_HAS_SINIT : integer;
  attribute C_HAS_SINIT of U0 : label is 0;
  attribute C_HAS_SSET : integer;
  attribute C_HAS_SSET of U0 : label is 1;
  attribute C_SINIT_VAL : string;
  attribute C_SINIT_VAL of U0 : label is "00";
  attribute C_SYNC_ENABLE : integer;
  attribute C_SYNC_ENABLE of U0 : label is 0;
  attribute C_SYNC_PRIORITY : integer;
  attribute C_SYNC_PRIORITY of U0 : label is 1;
  attribute C_WIDTH : integer;
  attribute C_WIDTH of U0 : label is 2;
  attribute c_addr_width : integer;
  attribute c_addr_width of U0 : label is 4;
  attribute c_default_data : string;
  attribute c_default_data of U0 : label is "00";
  attribute c_depth : integer;
  attribute c_depth of U0 : label is 60;
  attribute c_elaboration_dir : string;
  attribute c_elaboration_dir of U0 : label is "./";
  attribute c_has_a : integer;
  attribute c_has_a of U0 : label is 0;
  attribute c_mem_init_file : string;
  attribute c_mem_init_file of U0 : label is "no_coe_file_loaded";
  attribute c_opt_goal : integer;
  attribute c_opt_goal of U0 : label is 0;
  attribute c_parser_type : integer;
  attribute c_parser_type of U0 : label is 0;
  attribute c_read_mif : integer;
  attribute c_read_mif of U0 : label is 0;
  attribute c_reg_last_bit : integer;
  attribute c_reg_last_bit of U0 : label is 1;
  attribute c_shift_type : integer;
  attribute c_shift_type of U0 : label is 0;
  attribute c_verbosity : integer;
  attribute c_verbosity of U0 : label is 0;
  attribute c_xdevicefamily : string;
  attribute c_xdevicefamily of U0 : label is "kintexu";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
  attribute x_interface_info : string;
  attribute x_interface_info of CE : signal is "xilinx.com:signal:clockenable:1.0 ce_intf CE";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of CE : signal is "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH";
  attribute x_interface_info of CLK : signal is "xilinx.com:signal:clock:1.0 clk_intf CLK";
  attribute x_interface_parameter of CLK : signal is "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute x_interface_info of SCLR : signal is "xilinx.com:signal:reset:1.0 sclr_intf RST";
  attribute x_interface_parameter of SCLR : signal is "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute x_interface_info of SSET : signal is "xilinx.com:signal:data:1.0 sset_intf DATA";
  attribute x_interface_parameter of SSET : signal is "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef";
  attribute x_interface_info of D : signal is "xilinx.com:signal:data:1.0 d_intf DATA";
  attribute x_interface_parameter of D : signal is "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef";
  attribute x_interface_info of Q : signal is "xilinx.com:signal:data:1.0 q_intf DATA";
  attribute x_interface_parameter of Q : signal is "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef";
begin
U0: entity work.sr_ram_commbus_c_shift_ram_v12_0_14
     port map (
      A(3 downto 0) => B"0000",
      CE => CE,
      CLK => CLK,
      D(1 downto 0) => D(1 downto 0),
      Q(1 downto 0) => Q(1 downto 0),
      SCLR => SCLR,
      SINIT => '0',
      SSET => SSET
    );
end STRUCTURE;
