// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed May 27 14:13:13 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/fifo_gbt_encoder_synth_1/fifo_gbt_encoder_stub.v
// Design      : fifo_gbt_encoder
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_2_5,Vivado 2019.2_AR72614" *)
module fifo_gbt_encoder(clk, srst, din, wr_en, rd_en, injectdbiterr, 
  injectsbiterr, sleep, dout, full, wr_ack, overflow, empty, valid, underflow, sbiterr, dbiterr, 
  wr_rst_busy, rd_rst_busy)
/* synthesis syn_black_box black_box_pad_pin="clk,srst,din[115:0],wr_en,rd_en,injectdbiterr,injectsbiterr,sleep,dout[115:0],full,wr_ack,overflow,empty,valid,underflow,sbiterr,dbiterr,wr_rst_busy,rd_rst_busy" */;
  input clk;
  input srst;
  input [115:0]din;
  input wr_en;
  input rd_en;
  input injectdbiterr;
  input injectsbiterr;
  input sleep;
  output [115:0]dout;
  output full;
  output wr_ack;
  output overflow;
  output empty;
  output valid;
  output underflow;
  output sbiterr;
  output dbiterr;
  output wr_rst_busy;
  output rd_rst_busy;
endmodule
