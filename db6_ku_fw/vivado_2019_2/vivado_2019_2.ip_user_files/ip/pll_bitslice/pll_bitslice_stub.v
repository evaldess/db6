// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Fri Apr 24 15:21:26 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/pirovaldes/Documents/PostDoc/TileCal/db6/db6_fw/vivado_2019_2/vivado_2019_2.runs/pll_bitslice_synth_1/pll_bitslice_stub.v
// Design      : pll_bitslice
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module pll_bitslice(clk_out1, clk_out2, p_daddr_in, p_dclk_in, 
  p_den_in, p_din_in, p_dout_out, p_drdy_out, p_dwe_in, p_reset_in, p_locked_out, p_clk280_in)
/* synthesis syn_black_box black_box_pad_pin="clk_out1,clk_out2,p_daddr_in[6:0],p_dclk_in,p_den_in,p_din_in[15:0],p_dout_out[15:0],p_drdy_out,p_dwe_in,p_reset_in,p_locked_out,p_clk280_in" */;
  output clk_out1;
  output clk_out2;
  input [6:0]p_daddr_in;
  input p_dclk_in;
  input p_den_in;
  input [15:0]p_din_in;
  output [15:0]p_dout_out;
  output p_drdy_out;
  input p_dwe_in;
  input p_reset_in;
  output p_locked_out;
  input p_clk280_in;
endmodule
