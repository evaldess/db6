// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Wed May 27 20:03:05 2020
// Host        : Piro-Office-PC running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode funcsim -rename_top sr_ram_adc_iddr -prefix
//               sr_ram_adc_iddr_ shift_ram_adc_iddr_sim_netlist.v
// Design      : shift_ram_adc_iddr
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "shift_ram_adc_iddr,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module sr_ram_adc_iddr
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  sr_ram_adc_iddr_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "7" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* downgradeipidentifiedwarnings = "yes" *) 
module sr_ram_adc_iddr_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "7" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  sr_ram_adc_iddr_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Fh2dfpMKdK/ed0AQs1X3CGm2LZxUODYXp34m776ilDEyY7QGUChyobkJRhPPajyUZ/5+uwEe+IxT
1SHMqUKbOIXbjYyc/pcnNOPBHco9Xp5GbguXrD/lcwhqpbiF6SmIKipohHDG5TlT8coXeUp4zsvX
UKM5u+b6/LX14SftdQNvkTNG1zk+K722THXYkv+i6Pt6RLZA3pMZtAeoGSjSsKQy0NBNKUsnk52M
qDtoNHHYM8LRK0lzwCLjqOZqy6a/z/Hf4Ut4YZOU+4xHHL/rchHGK5GNPSnTkL3welwoyju5oVBR
HqfjssWf/mZbRYQojNU6/SIivktiCfvS/wyZkA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QTn1pDwv3HU8BrIbbIGPtaXh72h3o32mK7rhPe+fNCxbgjkg+YQfEB7PZxBa9mck6gdFFqS3KOrc
s4RK4TVkF8nOR8xfywXYMHUVbB1P0Jmw4gvNOfb3HvrArtFEisawuukTQ0h0tBGBeWn80uQz1JZb
Q3EE1z3nasl6cYvRFle3OeumiJM0nC89Imt3WE8lmt1+N9TlhHk+AgvsyhqDxM7Mrn5c7JipNdhh
gzpDTr0jVoPbj5DTMe2sk4yhJN4UEW3mcFkijj1AOGMPYmJdOLda0GJF/901LcIYOSQwece0m6ET
9epS9o6AZEKbn1D/Ig9WRnaosSwxRlUgU+eX7A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7232)
`pragma protect data_block
oPm6pgxTsHRYthynQh1GGWSYR1xHQ9xCO5B5Ogz7CHMPNacq7zHa90pfw6RPgimIYaYRO3cMaOnO
J+4bzp/n9DXrwtC8COeccsDM5TQQ56JdDWNTferZsOHrW08n3kBpqvDPvf64Vx1Wxw529pBdvZXf
43zv0guedHiwc8gsRow7eZVanpfEDab+2Q7+uH+ECyb2dV817DWO/Kjyg1Ecmj9M06rmv+52Z36g
w/zeFPLzFKFPuJbXyEge+Sq57/zbUAr2IxwrKgf/COMaiLnxGzd+XiV67IqIdotjB6KCC5O//C1c
YwJivRHsv2fYeb6ChJbjeJmlnLX94vbnMBOKR02h4zK7K+Lk0xmHnf0rTtsSwpqhVwvUftEKZXVW
/TDbTdqSwwk6eDCcvxP20uzmHbNTQ5ahnl84ocaQDt4WxR8FpPhM1MUV8PSgPCaJFWDGK8e02FBS
U0fxufqcmjoymNKDI2d4r5NVVQ4lM5Nhz4CQWAYQPp8dWhyxMDI36C+Qf8FREbTTX4VdEfyeviSM
Z8UW+ErLxOeiflvXcP+e04XyurOHfTjH8JlWmHumnZ84Os8C1/mm0cjRXkhdDnFscPvGaDY6ztEa
z0dPmF7pFeIy2vLzT2ddbJagySDF85lTIquWeQxZjnZNFVFapDwpSaTi+53zaQu27A+eIiSKJMlu
gF1fkwXoWn3PPKyD8cLW04URLwIxVrGoywhpwCMZDNkKqplHuLuuzsIknJRQiAh8blkqaMK50EpO
1u5E/LTrv5mf1UJX3K99/hBOLp0e/MDRMkeUuJoYnJpMF07khbpQwlHp2ZTs0D3VXA07trhAuNQo
8iGxslydFT5Tzul83aTVTfBoZ0ROH7m1woS/+iGTQv5PuSHde1WTxweOR4NRl/5PeadjWjadnTqo
m+qTeKEeDOpzwPTEF+3wmqiseTDUwLPShUPd6/KhsvdHvyiYjlN63RpYWh76792O8ByyzBfMkfX8
pWoowaVhDBuuKlCrg3NTX91HvDA14Ma6KhJNu6Mtbn4RfnQc7C0A/wlj0W8wmcFY1H1kCTywyS83
CPUpHywnh+KGcJYHSQTnhnOk9idcJaIlVXPL+170hjURIN1t1k6+NZjvdlYnqdNTy8GHKf5TdBAR
PGvijjiVFYXoyh1NOqDTDbIm6a3C4QVckI0LLDnoWqfMJmA+ydlryGNlRHcVY5mHt92tWnnNSxSl
KY//g/OwS30QMS+bSg9xXyocH5vAvGuWV6vtK1ObFc9p7QW7G+OO4eJm39plsMxZrT6aQlZvNBpx
+1LbG0ZyYoQZF0sFH0CVsuPnK5If8sWnX3BC+6iUmIrbWTNQlZQkzB7J/fDWtBGOi1RrQRbneUqk
dmbZT/fwAFXi5U4VpJ9upLGdwdXeEx97GTUIdPwyY6z1BFplJ/+kVo34huVq43Hbx5XNkBsFTFz2
OV0Ss0F0K+MAQFwx5bYnagb0myKe25tN3pFUtKy1Q5xsk6Bk7LltdojXlIDiBBYDyeHEVOomXjSR
P5q5voHorBtKQZ0N/qkzB8+mAWZKx/ZQfgQqEyBUmv/PVkhKC9H3l/2Rc3OIjkvk9aW1BxRj30kZ
LUAaaMh9enig8dPQMWJBENqWT8MwC90YmKVpdEDYcoWvgG7Wpkq9aJ9Pzi7mdKyFt6t9uTrDKEb4
lMnFJlhg7X9sMrTy7DD+59yxM17votfrSmcusAFCF7el2YDheAIvVRgL2l5+oIc4ye82SVn9cb3b
pfIU+Xb9lZvf+LI8Aikl7MuAU7wUtuh3tnh+kq7qXCL7Fgzhpt/M7nBr24P2FEFzlLfiPX4tAvoU
DUx2UZocWZhDgaI/kV5m0kjLkMbuqNuFFXqdwKzWpK8yUG+ba9Ztnsggo3+FFmtAghkN4qlea1Y0
g73Y1y53nW/cmma9Wi3jg0Ab4gPrlPS0zQxZZzNbmVbLhoLhXcNpttuOe/7JnM7RJDG2Q+18eRB8
xmv7vj38LMrUTusSc6CH6nhUQNkO8RWiQiNOQqDMOOhQJ7aGadOE41bsQQAM/hWksE5S1UPE0eR7
Z7SLyaY7y3Pm7v5Mj2w74f8IMAmNQkPWaWLOyd/aqLwNT5SpYYZ6GDamOs5+sJ7b8w713W3V1AU0
ajMReklTOVEMDkHJm9E+6AmLSFcxUdjosoaNp1KZ+//4m6A1KDpjjxc0Y5U+fJhb9Zal5BGO1aYD
lqf9rz5RK+9vAxXJ4KI5mTLxhBhc0fp6gMCzOKHIv6RxMozUCotDBGxEkF/h3mfBUez1B4hOkN+Z
CUJru7uFkWbbDxBuodXvKZzyts7gFpaMNsOErt8TZ0uDbyqvAdZW9BqbZ/FZfgpBLoimbNVP5uKF
T4sF7LO06SkDjdfuU3tgiDyNQEWfoTxQI3UNlIVII3MBXGMQs7SCcKCXvwYkvNtR2sjP+glnaTFo
9CZeTqx82A4rn/P9N0xMGJInaibYHAZULrTBJ+wjz/LdoiiLAECjRwtekJCRi9e50fZ2MMkgbkEG
dsUcTJeLplySy0CbXjWvZa7hQdugS2/sGLaID/kAMHAUK0s5dQQQ9cWhA+JI0rbiLgDJfwIAQ3Nr
yGjrUK2BF1W8XBIIXPqDHumXobzBSBp9iwlWLHGdMQ9Q7zJt9qL6/buLKQw/NNXjYv3k3RbVYUH2
kYeCl4Rr4AqfmUq2YL7SOAAJ86Bpx/G9dVADDaXbbqlZoxyUu5hYnCtn4duJhAtjh5NNUeY3zZyN
a2/eVYTlovnTc2eXm9n5jU/UhTXkSk2ttbMt5mIqmAX3LzUW2FYzJkjR8fu59tIf3gTd10qyplvK
WFfB8WWMhv/FvNbmfvyJDN+ePLtFMOaJWvDYPteswKHfjLUkrFT/qZXOmlZieMSOOr34NCzObQGY
tOmwZdEeXQW0BWKe7ueB4+INE08JdfT5cW1CXVnY/mM3OeJmX2N5ULw8R5nnLUiKJME2IwOUvy2j
epbla3HZQdUGPpaxsrOiWcSklzomsZPC4h9osnVzCNoYXea6rX0usPtE6NpQVHy+9Fa+3eHI4JzC
1tskD3j9dOX+iB9qMgSX19mn4HC4wZs/7chrTtwnpQmiQ1b2gurJ3TB7+3IdXA5ynA0GVHDn/6do
WHmYlL/eYrDMwMX6ojvaWqW1IlN274ZI0K+hc68yhdV/D+xwvo6l41q6xXD5RVvDGaRQhDE6FOrS
P1+oWd7A1qMdGIucSnQjKBovzIgrIdpO8Sn2inALQ5Ath1rhmjL/4Td4E175JalDRtM6WRd/ophy
qUScmg2C96z8O+cKUq1H0JSb8GPzSnE2YpwOaJ4pwrLEJ7kyJuYLEQRcqTVwtWCq76gjsZ99hOF8
u0RhTCXkiDrw/gKkLNdNoIYc3xzwiV6xLRm9UdtIzHqiN85siFXGD78FfWQBdk/WAHw0m0ICoQbX
AuZiVnWyAp6vDYiIF9WVkR0LbswAAeaAfcr/UVu++Z4ECinEeZjZnjnok2hZ09fMXFegqbryznR3
67K9g0AJUhjxnOY3wo5w5JgulLBI2tBvefyDe+wHKSRZjAMi7MCTSUMp4aPqNZnKwsR8K2lDH2+q
kW5W6eR1YVeuMcKS1MjvFcdHGsX2Z7VhgM+fv6Nibr8E5fA3JOi29K7iLM9ElcnmjK5jSE4gqUrT
NBbFPebyhOYQ4KvoV13JEO+wzHhpEty2XlXT9bvGHPYNVRXowQSsFdvXIvpZOy7igpQSzNJmmX1Y
Lr1oypUi4aOf8Y26vNMbi73TVentnQyly8a8AKqckZc1VrkV8nYlwv0HhJ7h++Y4v2NayzjxYDF6
a2/N3AT4zCsmS6jBYLEuXXYTGPV9/i2UQmYMxwaNSG1md+Dk2JZYCWzSffq2oV92yqmuLtkNBTvR
6tzrDksQCK5xjeHirGDSUDVkdvtHBPOcbpngAU29MZdhrZP8T5FSwZz6X7U8ZxE7eVpmufBqssZa
Y9Jr744FFUF4xQh4zIdjTnVDqaB8C6CzaeRsEXrfYwCClHQiGnCAq2UTIkNjfUCY6w3kvczN+zxJ
TmrUZ6fPMFZSmWysCxNVn8acydczcPu36b28U5jcCyLGZ855kcZKwXsBwekWw8hEKjLu5LoKHpI1
yaEPZ8FkHTGq6j0x6z/7d5bDURlT8uRLVbYEd9pZCct1kpu5jcBy/JJe+/eARpg8stkCmcGHSnLr
/+KxiZQMKT1dEuXy2CfvAfMnWiRjF7Iy7R7Bt4UneP/3jQOs/0QGjATcemb39TrDyDd5N9Nnho2n
NipuDibXGPEXB042XpqFbZlrK40XZCQybAyajaLJ4JMMi/UeHtDNLP0IkH/h8gRU8s6HuuXXhAED
eRKFO3ns/W+5+X2uoBQ2iZXHUCKijiyUmuKM1k9oLR1wbwuX6LOd9FKe12xMDCypXElPpY1wnCtk
5azatJgIJz5aLx/F/J/dpUls3oixXCiv9cKIo3cZEHs9L/dECQ0hffSnj6pK/IINDpnwyz8/lhGN
AelYpHAQpOuEOXKCc+5k9gU7QnIwbrJ2iivC+Lqujmk89/yT2UZIg7ApwxO9f/5XFdxieocl0NWV
DFTr5iyWbotrCBBeqAIhIBFaTEqk+/MvBN7119yaH2RDVcmt/t5GCbAdOJ24LX7+13cWWooGsgxN
1k7HOOg7K8eARE6LuYaj3GAQrmhOaprCElfU1ztdgsQ8fCxhddcXxOJN3hRDvXaSt+wARqjH0OjB
UbWdbcKXt9RJkfdMmz74xnEp3THPcRu/EE1WwKYu+z3WuqBlkV1jJJ491o4Q8IqJrwPhCGN5Y8um
Lid6eDQVouqFb8BdUUa8MEfW5zxqS6MGan2LtLLVrGGXMmSRQOj1zT1z24a+ZT5FCo8ahvbzE36/
IwM8F+Bu6LHl1mZ8Fy6taEoGVLH7k8ZBb/t7vFj9VPw2cTLq7bQ9COh8LJM/KKGQYsqz/Nkq8mV6
88ztdZL0gqE+SvS0wO0VlZMs112+oi3KzU0HlZRBKxRx0A4kOjDcAkfFsRKj4znqOxjH2apS3Xkq
Dfxevs41cD+a61qmUkimqWV3Vd2FjHXjWH6E7Iu9/UAVzqu+reDGGc+KgX9fuP9shcJFZ2VAaD8s
oob+qz18cCUmmeqVC6KU6wVokZYgsOhdJ3Gr++nxrNE56lEhOls+9ckOcrAllrVRSfIsHPyHe27z
hr/JlKwNYP6zHpOAZoG9EjXXSq7qoP/ffjR0rUE4hMxL3mTBm9zL/1KS3x/4C6BCPoSRx7T/dO2E
DiXaC+v/mpvWEetMUjcwnfg/IWgL1t+xOiTJTCSWUMPMoQ67qCpU9IO+2trmfA50cWu2FKDYABUX
PufyDs2Mp98oOqat1gq3WEt0JVfvZzizDMzUhnTEFkA9TZt6Ojj1t7dF8rAKjnhiU+EDs4M1K9x1
oNHpuvCHOBBBRSZh7KerrsZHgaCjW+uULsu1zEvN5iuyAMogGzASYC5lxtHZWgHvSj4ceqPOfytT
CKNo05Jfq2sq9ABTx+Aqv7PpZVQk0IuZw/66JsFrQ8vxT21AaciAWvAq9uHJvNeqfE3sybvPt5cG
uG5NheknDXqa3gwh5a2KQse1tIZd4eL9Wu3pWPe3REVKJlT5gPBYycoeMIUzzrA4ERhgYyFJwer8
NwDaaQf5HWH3YLLJyOjoS9YAeUDkxMNe+cnCPoJMlwHMMjxtC0/yrkbsVJFYZb84sYdr7Nn2uHRN
zEOxX9Y4GjDx/w/Kw5sQbXKFtvTIB+DgZ1F49lI7YURcDzR0ahKOawRPz5G0TvTwHP2u3oeVPke5
pb5L8tYaQsUuv8LELtfHmRjpA1wVLCoW0c4TVshLXYqgV6JGkS4qKtSpPtgE/4h0P2dK0yH8FC2S
U932Bq+21E+2F6KV15wxMGqHbwAjWEVFVCGhKcDBWQt67cB/Hp2vQmffFptsC9+/8vjkQV6rwVJH
LQ4PeMC/CmBrrGXN4+bCY4LXQ2RrZvhREC8K9O8Z636hSqktlUz/UPfd3IonTyermRh5Hn+a5rZ7
bMSTkyufDvxpFU6dCPomCOg4PnRMvnIZKKjmquL8YuQhzHVv+xLC5xnvGgPrNV0maH4PCR9NgZXc
ArmNEbL3HPH370QxBN5crmT+6ruSKPsmgcFwBSxZ/+IOwKRx02Epj79ze0Fy/u1Rs8QzWFQxb5Mk
PuvlwVdPRcgSAeYIKDWG5VSGTcLbJ+MnrynMdeTP4zuidEyIGs+UOK8yRG2qA/SPykcuMjcWjHRS
SSgcphh02bJfQGCTfSgMyrHwWGh4iMlgJ4Ba/nEzt5JRqZKL1C438RmddLgbiuudn44Lrw5ayZtx
TdOA46LMHWtN1cyC398YzyNA+UYkLrR43o9AhpiLz5azB+AJQkPyWrEz+nDJXTiLPT9QK0D7TNRB
32VwX4VjptOt/S/UfpJ0P3L/LhtmFhK3KC9KBkB11Yit3bpsfCPhgKeTLdH0vWmfWbI62vB11j6s
OlONSMDD3cHVL+zrn30SkGsZtPBWHiNUbLjJ+0VvsqW9OqIVTKhdmYnvQ16t5cNvIuZqxgoZHUfR
gs/kZbQuareSAhje6fKfepTcKTVpA7dkngmn56keNYGP8ZqYSsViacdkhPXklL10tBHOT2FJAbgq
UNWudI9wrT6nLulPg9sQEpGgDKrN/hkEoBCTy3N1rXN05Dxg0deHj+jeE2K4FWTlYHCeKnU2VvdZ
lXDl2yGTa1RP8KV4Qiezx9dHfOo5QjXDlttuxtO30Z5RA2gpi7CbjnLIEtxjskLEEwCkfqbYveB2
w7XHuqAndRBdLWoC6M3T9APJHfkIlpr2zE6DRzBNL8D5G1ArkZgsJOOUTxcR75Xcvqff3eLgTWH1
TaMX+REpIxOPwwbY+gpaX6GAJYimnIt9bY6NsVNB0dsNzLtMTf0xN77SxGo1Fw6GvyeOLn78TU2X
YKGkxilU5Z75A1m+9SF/Pi2av1LStA+9B4szucExAZo/j7zuhnS8ppsdRdNQzKwZj2dy2KcyW8u9
58OesLTif1tUzzATs7dXOrrWqU3fGcm1YTo4iSrcSWGgOVbhBercx4FnHdSD5GhL837iZoR/QjWk
HpApq4MUCV3YjIWFeju8AeUwfhG2M8xksjLm08OjTc2wQ1Q9keNmrX3//y/HzvTIhOySl0KD3T1E
Z8cBNGYgeN3+O6R8yG9PzSrwNIcYdIh3l4mxhdsvPI1lPOl4lMuN8qLO6g5UYmDpWgsPMiGJTq1U
nJZhe2Taf3u+wExG+2apEyIfFm9m4LZgHYvSEtDg38XTj7+f/TGNzCQpnYIX/luMA/uENa0PhBk8
5tWAk/MDkyyRvd4s8E/w0AVJBgmjDRR4XnQMc3HeTJHeGSjYZ463wDzJ3zK23fz0qmQXoU34Qkrt
oJx9zTlTjBsdidpr2fOSoMEE/Fv+9q+NiCzR/J1ZM8DEHLo0PYAvNKrSwlcvuPrOYo1S9LEfflH/
61Zg0fo8NC3VUKS4zkPJORfK4UGAMBibkmfaM9XM2SpjfKtIaZ6a1C4b5gLTj+3hCRTOkJ0m0+8C
VW1BEKpM6j/VaZa4fSY3kc+IRai0LNm85leMih2a9FG4iRTo/7xLlrtgwSuiD2YwkmPLpdBPcfBS
h/0t7uB11PTOoVoIbOcZW+k3gWrxWhx+YRmOUV1C9Gu10IyKAYHGOe5b8CdvAAFI/6wWct9M+Vt1
y01BujWpBhtB+F9RC9LFNPTg4Xce2Fwe2kzinD/iw8+POas9TTGU8ctbFnfKY1OzymHAFCAg7jll
BQlwczl4dQRjre/OBDt2Cw9jwAQu9X4i15jXhNHoE0aZHVYe8EbAnB2AEJnUt00OuImqzQbEcvqy
OjAwoXDX2bUEKBrWdOWQa7rU9Zi3TTSZBKiRkKuUCWSbNpMEnGUnt3qAkauwaD1bxPpuqqvl97n9
hUcWgUYwbiW85GcXepGY7Bd/+pg9Wxb/I0CI9Hb8Tqw6UpwVP1rsC/wICXyiySFi4ymPm1TAPynn
xv8lgJcTD+qLsGht8RlGgmkzEWcNJ3bpsSbXG0K/wsSu8m6eIvHkR0cW9k29mp10oY8c/oHwfwHt
dTzCSg4WKVmh/KOjOTb8GfAYtgA1TftQMcOBXutKaFUTt1Et/zMdskDczCAB46rNgC5uWa/8+0MM
gJOLQoCzlxt6dOBGe2SKJR9pSgVm+a6cXnOyfbF4elhuOPPscpAjHWxnLr5de2qbJ7axcJKCuXq6
hCo1YEkZNr/WnZv+nWlKNV13j/9Qw41G0et/iWsdlYd4Mgu6OWS+Ro6uxkqAZAfMgrlq9licJuXR
RkS7kEEy4z5YT+fvLBjLBqw0lse5lOBNU6LOcngViv63NWcJ0XHrTuUz9oQ192wP6eg9hYWKT0km
ZYOupf0EJmSnhtmD/7zGro41BRJWARrsEbKnHyxfb+isCpfUPlO6gZ5VYe4lPKvBAopuh7Qhhx3u
ilWLWFRb3pbRVXRZZrq/II3k6Cl1sS3eve9JsjZqAju7PKTh4NXY2ffpeag6fisQDRg7GduQmPdz
GtWCORd76DPrBKmuRYBpKRZyqWos/DlPXe1LmzEZsGFBuk8NQuJ6bzyXkc0hwW48YZBnbY+3MbnH
Auk4cH6DGkOaRrpfl+siRh8vocCCMFRmzspWjInDV8CI4E0Utaue2ef3eH80JdgBFXABsoYNfu/T
fb/8DeOgZJ2Z7gIur53WejoblngCkw2dy6xLNI/ZLhtaeDC3G210XyuEDaVFvMD5QYAK4qcpixCX
iQ+v7gPMHY6EmcaJU+s6HZ9FLnV/QNgjN/qo6GdzU4LUtCDR5ZHVx3KdTMqrxYUpzjJdfcRkEW5e
CjayZLj4f8H5e7b5unhirjxtQCAWyH+WNs1MxMkgLMWemYmd7RAst64VsdxCKOjoVZ0+imWYEJxQ
3V6LEYuI+BLVn9OfmWTeU5lFXmDB5e30dLuq2Do6NMtMXjn1TwaCujlHXJe9CBCpSMgVa9Hhyqke
0blizGl6VYXgQOTgQi+vRcEEdGBgVIOwjWQVyFFaCOV5VE3zi1n5ieKyQ4td0MfE6RfZ3gyJoneB
ClsmbIoKCftovRH4+bLq++AtMjaxvJEjeZi0VEhSF0VUGa2yPLA3Mh1NF5ScYOh14Zj25TrCCYY+
NxyA5th5BRsMcoz5Rl8bgQIUg6AbU4i91bpFufrphTMG67O3BuwbonvUbuA1cWYgr9ahYvVvfBad
Gyi2QNv6XsYK8qhlJexsFJNDw/vPshcvuu5WbY/KS6P2oY7N21tABYIR6ZGWba8SjqugFTCZOrDz
yBdG6n7RrZJphaN1h7FeTDZ49cVwC7VdAaClUZv06wYo9/prKhR9svgXdIsrP7zL44gNgLHzcJLs
XS+vh3MICkcOZBSgAb5pJytEeCoDRkqLdNRwSWNGtLUSQwSkfsgLdOf2aLOR8kKy2btp6Nvx+/XT
5sPf05HsTFB5al4qMRcxDMdJMjOhGSHKmW6LpNCrHmAcOdPT
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
