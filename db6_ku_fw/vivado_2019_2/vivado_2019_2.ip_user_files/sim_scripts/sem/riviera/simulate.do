onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+sem -L xpm -L sem_ultra_v3_1_12 -L xil_defaultlib -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.sem xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {sem.udo}

run -all

endsim

quit -force
