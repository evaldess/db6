// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Fri Dec 11 17:43:02 2020
// Host        : Piro-Office-PC running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ sr_ram_commbus_sim_netlist.v
// Design      : sr_ram_commbus
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xcku035-fbva676-1-c
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "sr_ram_commbus,c_shift_ram_v12_0_14,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_14,Vivado 2019.2_AR72614" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (D,
    CLK,
    CE,
    SCLR,
    SSET,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [1:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:clockenable:1.0 ce_intf CE" *) (* x_interface_parameter = "XIL_INTERFACENAME ce_intf, POLARITY ACTIVE_HIGH" *) input CE;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 sclr_intf RST" *) (* x_interface_parameter = "XIL_INTERFACENAME sclr_intf, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input SCLR;
  (* x_interface_info = "xilinx.com:signal:data:1.0 sset_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME sset_intf, LAYERED_METADATA undef" *) input SSET;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14 U0
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule

(* C_ADDR_WIDTH = "4" *) (* C_AINIT_VAL = "00" *) (* C_DEFAULT_DATA = "00" *) 
(* C_DEPTH = "60" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "0" *) 
(* C_HAS_CE = "1" *) (* C_HAS_SCLR = "1" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "1" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "1" *) 
(* C_SHIFT_TYPE = "0" *) (* C_SINIT_VAL = "00" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "2" *) 
(* C_XDEVICEFAMILY = "kintexu" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [3:0]A;
  input [1:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [1:0]Q;

  wire CE;
  wire CLK;
  wire [1:0]D;
  wire [1:0]Q;
  wire SCLR;
  wire SSET;

  (* C_AINIT_VAL = "00" *) 
  (* C_HAS_CE = "1" *) 
  (* C_HAS_SCLR = "1" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "1" *) 
  (* C_SINIT_VAL = "00" *) 
  (* C_SYNC_ENABLE = "0" *) 
  (* C_SYNC_PRIORITY = "1" *) 
  (* C_WIDTH = "2" *) 
  (* c_addr_width = "4" *) 
  (* c_default_data = "00" *) 
  (* c_depth = "60" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "1" *) 
  (* c_shift_type = "0" *) 
  (* c_verbosity = "0" *) 
  (* c_xdevicefamily = "kintexu" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_14_viv i_synth
       (.A({1'b0,1'b0,1'b0,1'b0}),
        .CE(CE),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(SCLR),
        .SINIT(1'b0),
        .SSET(SSET));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
jKLNR6CFLXNaFIX4EgFderMxPnKvpk4F9e4rB0Z3eM53MFOGJNJgkVTyQHI3/mIWOAReZVwoVOMa
CdAhgWGvBg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S9g8iGOMco4oFYFI4TkAP1q7tC6YdaKcKnkZE7b8B1VOvr1zofUKAItPH7rdgXy1xJT5veYU9CMB
1a6xkY/7hrMk2un8LzBXxNY3CU5Bicpo5xvFJFwxXUw2rsZfzzw96pA+9XCQOKRH4TLd3b9RF6St
0jOdYl4JHV8zrfKdmxY=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T9dmjYx2RI0RbX6wqo4nWU0ad1An+UnLDs5SJii98PTuke7wRIDUcgwzVXGZhnqgRDMGrxGdV3bv
2TG3EcxZKQwTVnAC6QQoZX/EtMHghnA62m/5NpXmoLwh5qm/MLJ1GcevcOyCUPonSVz0GOgxnvwj
ooQgeh9D1jd4jba778m7tqjzyqrMu2wlx/9bVUabKnRucVtEhLrCSutcfwtKRjcjEslE32+ANJJO
LU1E9xHWQKY0Ykt2thHoAW/gEGE3TgPPSeS1uMgC9gpn3KeR1GWNFmz/5i6v7Pure2Hjx7n/xHnI
reb33XFnLAOOS5csVRvU6rhvZeRoqLN9Ju5zBw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z4MAcGwOirs8ueHe0/LAJt93fwBMCERy9UlyN0pxTk9Tu06Hakd4P9cZvnfzA7zREYXMIBu2NDPA
+322PzRY4McOROTi9fUMbDa3sq4QlE99HePrmhLC9MCN16iXhbU+HBEFNxdCuVK/qDkcEHSOzIkz
ISv7GfjVXM9ytGOZjadyXWLpl+dtetGHtMec8w91cjipLXbo2ywr8DccFy2Q+uIfn9whyWTv3WTK
w8NeftqkhVPZqMJIv942kdyaigmw+FAOB+eg4fWaELYnDgvofFaanVzUBmReOY7/b3LQoUhotNip
TF4puoXTeoGR0ir2Fw1i4DrX8pQhZYrHf0g2Fw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RhMVl/dQLgd6Em3cvXWswCuyQybcYHVY6fBYkTB+0qwPgxUd1H6xUy5MSLur1rc0+xMO7DV0gkc9
m7J2qnyE4PeY648BXoQQvdkIDs3cDfJUIMzBSJRhAzANt/GvnCfPAPUqQ+RK/y3xKJwLsMukWXHR
t1HX/5OpB6TQZHZYE4vz2lTGPGbVIW3QDoyrjz61tA/jsHUVGJvZ47VdBmfldxPqiY+Vh9e3dl75
JmttiC9La0yOzL+SocwWzDn/QZbcRHMsTtLWlhxlY2wXUCss3GHmb0o9kugY6zDzV+5nG9yCW628
du+GA9eci/G4jwl4JXZ6p/WPZm5Kh58Sk5SgqQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
Z6wIEFjRiUpcYIEu93tUzSRYb0cut/OLoYvuGPmJyBKSi2zPwapeByA928Z27t6xeV5W3znd08OP
jgjBqsSWHmyKGPK5eXde25Rc7IZneNvK+sw4HV/jPYtO1qybQvKRnWu8hrBhMhyAA1aL1U4QhZ0j
OVNZp1DTIxg4hiigHOc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
M6YZEFH9Zpi1+cHBSrOstRid3w06pA53vGHrYgHzFGeKeyyHqjgt7TSqiheP5aW8KTNRQvg6odJR
cQXh8v30NMYu/jZmXni3nFsFUTUEXNB/ePMil1PPUrf9TNxaYXBqeX3zB6GdK72zXdmYAQQJsXm3
TD92LB1fEOaj3R0/tHYpufRdGd9ixd+Chdi8l5QOJjm+yeF3y5TfCTs0lUF+EsV39HM15hn/yqbA
gT+ibQT1xr8NpGHcWrdEkzmjH4Sn+dW0cT9kU4XilATPF50SYk2ecvCzISKLFkmNR9pfut/nGA+t
DPxZ51VLTruJmPjK9LFCbh2X38O5lo+z5+P8tA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KfvJFdvhmWTKbQ5Jxri/BeSIQO81bjo+x9EfkeRcMGW0X6ByjZDkAzxfNMlSiensyevMJMtYPImZ
QLedqWGrPYexifiq6cCXFqk8Ltq9l5wruSZyV43D0ysRcxj4KEmXC/8PpzjDp5HlvFJFOJ+D3g6t
NM7RYRIRIXaF8CskZw0jsmkaV1T83Anz/mZ/uZ2VBOchUsPeuvhUsVWM+cLnpjlbkKWXTtBltE9K
o4i/EdrpFyh9UMZS+xmXkJ+At1Ky5wvIPoNFGMpkkGQACazCdVYLc9yp6bpOYlB/gizgo2+PRrAM
svam1uLoF4FsN5wTcCULaxZrksdIcF+IAZUtMA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
qg+Kw4/mCEAcUCXEtWOKBfIP/7U5+Vz5e9d+CJQAgbEY805k5ONAXsX2ZqzVCXemCGbSJolel8jd
Q8AUi3wa24ILnaXB+HWrNNzIJEsLsIr9OGS1ugXLGU/WjUHQV//Wzhk4FB0bNRl9N1KdfPun0PWr
Jj44L558/FVx5CSlobPLGMd3XP22Hz0rYB/w8TXRzYWMn/txFMGyq89VXKL6/uKsQrnPCBPMv9Ea
w1QMsQPqhYW9jYOseXcdDM2AZdAuQEMDKPr5ErpPDxRrIq6lNO8Ab93y4SYQcCP4JnEn/7UBrPQl
c9hNtDOh6J9VXyPJGFQSilvUFZFT4RWyS2lraw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
zGtPZuIeGUnqSoCpqXboc3PMcOwqpvS+7JXhS6jbn/TgGNtFugY5qlM3yZTiiwMNa9wO29gxTBYt
mc5bISOhiWLal+g2AYbJRh78lANGXjZEnmiKS5aXTQqaBV4RpACwzMly1qiOSJVnhVs7g5hEVgVe
SH5T0WPvi2yMLxEwkAMNYfofaeB/UJopd47UJKYvEOPYzmdt++agtPlfop7j+Y0jVOaPQB9cQ0eT
gJY1B7d1muZzVMbq5GfI0QYB09B11mmnztFvI1MiVzCNJpj25hcXbNj1mlwVdb/BlQG7f80cUTlN
F39vP0FmJh+dMJTzdLiZZpaevFdEyc5xxvYGag==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9136)
`pragma protect data_block
hcvnP8qBr+HMihirK+AMvZ0eudzzU131ilXbooJMdfv4XF+26KWzkAhURvsKQeO6SNQh+lHPvimu
Zvut3cq+4Pb0aZpneQ/MEkkw6MPuj5HDTNn5fY7o+FNKU+dDlUN55FsBqYgwh0Z8ldGJdcv5Wecz
ltsZFspDd6gfktLzrYgRPcvAAb27KLiuDOydtUZGsTSRBTNnWX7JRCHqX+RrE1TAdD1z7pgjrSi+
3QHiciZJxvf/xoeVIXKsPwzdKJK5ubaC1VXpr0GoOlcxcvHIKVjCdJ9otP/pSFJcVPuGbbKAgYm/
TArGEBH1CvlEMckBPD1RY+inCko8oBOXho48PxhYyCX5+P2FVGKoIWagrJfLqhCMLLPhjKki+I8G
mNFdLsiocJqXtG5KJjq0XIBbUGjpOysyLa23e4jgTOdmqcIi/+YzuIx1PwWztKJkHzAg7JywdGbo
iwbctSlrxVRvkx7Jkb7mFk1QjURk1TTV+lxpckpncWOQS9P1GnkZdBztssuDopYQAwRDTybq5oNT
fZHQt35bkpOkSBiQHepgfb2EMjGnW7VJEZBzHkvD6wilYa+1V70TggVA363t9WMigLlQMGnYTgq/
s/iuWzuyp5T3tZ3R+T3w9/sKbIMtyhf0NAgPbO+XLKBW3X4in9SOAuafnGhhioaSyje0U/yNqywe
JJsx4QEcEBsCu0wSaef3x+x+16iZZ20W9avFO74hfnNZU9FQbx50ORtWaNG1U+m6sg6vxEp8Lvx1
w8pWNRfZ9bmVxIf9BqrqpbiTiNFPXkgcw6C+g87qyzYI7G05JacGpwovNsAVXaIMMOJVsyFNd0VU
IMCO8NG48a76ciw3cyLUoHxu3/JB8LYju+Y1B6LPfUNNew6JqvNPe7yMzLQ2HUTbDWbJHuvMiaQz
/ouqCwgFAJvFDY8sU6OU4KR+kDmGwjNI8Rs1WChoI5leKqZ+OPmusdj1XWde4cdzTFlVDBaPVLz3
y9wQga/Qn1GkUbpKgVP1NT1tAxVMoKGoLuBjd+s1A9tltnEQ7UVAUWz7v6iyTpWt7uZerL0VQkMA
4ZBez6svQsQOehtGCzInxI7PVWQCxpIaBOTkPhZZTj7oh06mkAEWE0qvlhAUhPobkBU0Jx555BJ2
wO0R5ZrPgTZW8IZ8lcQK56DwEy4KG4UOoA6+jQTvl2h7lRj7yf0WxIZHf/VjBfEiiU7iUEGHpwZd
P3wfx7KnooyshwlVhwsDo3MAKOGQ7taNJRwW195Vg+0Qx5vtbdnzwiL5mQtJQte4W7a8uNEZKMzp
gCBEzPlVmNjLJd/mO7KXX5h7cAnZgWgyenipjfJc/PHelLos5LNo6w9DFYPDG3SwlVBLfZMzPyol
VvAJRCoZDPK7/N86GlWGO0x6mwp2omuJHJwD4VuTNKNDzkThmix+nC1wlxbIf9qi6fhlE+tfcG0O
0eIzIlrmosvPK3tdxYK+xpA925BvJMvuvSN/QNzKmDajJL4Ptdz5CHL4Wo1EDKS0AaTXI8UkQP//
0IY9vto1oLawn7pNUpvDLhmMCzSO/OCoJ1Ymss7w1yNuV8sMrvq4L1ypVfvg+LQu/5IYFdvOKH4e
ys2SiVNWNY21Gv28fEgZhgmf2VkyuEziLeEqrzgcxm9/UUSDfKYRLryOaPWRH4nFLdP3qkuLnxcw
93BvzocRdN5nrwE0w9VI2Qdr9ImkagzrJqqqF3y+/PWRctmqxVtOgBbbaFHq2faYv1GMcSTsrj/Q
DAuWnpma4WV/u6m5z5GycjPQ0iJXNEqGy5c66sAWUBWJXQ247OU+Trb4AeutS1PYExMWFtV7cXz1
9yx+zTQfxWuL4Qu0mBgLsMXdWJSNTM6Ht9gc/oSVymA5a1YX7/HQFtqY0v45SJ2N1v2ZFhHiPf33
4k9dgLFHGDmgqscFzMDPz0Ja4yMMreR/6MS4s6OZSiKb5svm7jiKQLrlL9DXBrg6pCqQXZhEC3Uu
56nvnxCJNoLIlmCrTZ1WSMKbL14upZjVj+7vDTs0Ytld4JeffuscI2Mu1KG2QtNvsSlnF4Z1e6V2
5/H3ash5vlX5gnCOcfpWBqoXEzkdRE3LHsyu49lb8MTcqC2efL/ijy9Wi1cGJdtwDGlfBr2FuDiq
Vm9Yfy2YyYpTRFOzjtZOzN51rfRJHqe4MuF4/kbdKKIOfy9DeNwCpQkAs7PMB0sOFhqTZAzGU6ET
/5JyUhGvNUf96h/fnHEMKQ0uB6TC2d6npUZoBnJ/6RuKMEO/O0zV/kZxvrJepjD37PtVQeNs8NTM
Jj6h9wI3XcbrloUoVOn/+IjsS96SzloBdCwo8JIDEQYs7xv859KXvYGvZ2tNEAaUTim/TMer3b/J
2up3JbzazV22zGgCHI7rCr8cuZhdqQx/3TCVMgYpOEZkMUpzY5mhIFPTJRq1VccGYX0Jc2B/mlKG
gWUe4rlo4ajtNKbe9Q7Wr6lpo9XbI+6rvKVf+twmpktjtiZdW4yDMs6bDNzb6EcPPRhLNFPrdsif
w33X+wtbymosTx+xOUCImQyCq9Z41moBcWZhP5ZF8u9lTj0CTrflltg2zBjjwRNMLKn+z/oLlbdc
m8DsF6uH6frEIA6lCgyVAl/3zbQKPXe7DYv7tdOJcer0Yc9pS4YoktAWhiaz4/B4rAD0psWCCEK7
xl/zY96tGUOQgdS481IFiN5b7XkgXEwOdCBbXtfmpqSu84utbqnXme/9eXQmvgFW8GsHwLnc3ufC
vX/wnKPICaBlO56t4FVOaeGGWW/h7dHmmbFpxrYhNDkKvH4HK8/kquqsqexcxJe9EjVkqxFkvO7y
zrpWyZ1DuCVfn93s+//Kv1IgvRyAUxkIe//Rk3c+wx5BMzTKKXVTdXJU+aZxX5R6LtxqEeUC9irg
WOkqpEISsNQX6bLGNthini4abOgTuBuJGw4Cq6q+uuiEzkk7dbozokNGWhZzd6P/V75im3s9pU1D
/FqMMzVzzo2a+6I+YnGsVNBpHG2fm0FjOy6c5hvMab3oTr+vs8QyXvxCckrxOpCOyyeBqkRiuoJR
9b0IUXMvpKN63RLhqWfqILb6PY72SJn52ngWYffUelVLpweUIGcSNTSKG1QQBhrS+e/PdHx4qZ5/
LEvVTTqdz2lP+S+DoSX4MqFk9pKdTjHAqgnqWBOaZsHRTSUbCeyZJn8cfTGdM1Jo2TWWLNI8XMYD
NfcSHJpeiFJYyduQusvxIkVztfivLgOpHrWGTaOF2FfU+Jefm+NVOgFBfv5+M+49l3ywnUCZxh5p
azrUGoPpsOvIGVZCY+hCf1kUCgNlEUhyNGgPQezAZajaF9ymGXZfvezdPqDdEn5H5MWN2GeCi3ay
MKYUK7o9z7rV9lRq9/sPkRqIoeVRRjJinyDR+Gb/Ml31cDCE90gvC8X0jxrkoLpcq+wWsdlBtlCd
vIjMS2t7Rk0UclK38cE0719W1cHtdQLDlOmlx/zmIlwyVee/PPRNsShVpaIJ0fEEUocqPqF/KwAB
ncFgSq9snMtnYHnpvvtZc9OA6gpRp2RF/rK2hD7xFDg4m8vM113crGJ691hD2sl2sOhXDrYJdMa8
+BhAHH/a+H9UcoW6oLsS9BLWpv7bw+5RfB54anrKNwfDF+/FLmFpYV5/gQdDpdNgfn024lGDR6w2
3Goc4r6Tqk+zxt+RmPnIWl3/yu4f+qipAxbQAYTjMrtAkvK087z5WElKdM0vXIVl2JvwX1liiGUM
jxIvfTrs2O58BMuCp4xu7fdQIHI8uVlwb0qmQl874kGrh8zwPVkMx7TbwmqH5VLE+fyGi9wrUk48
Sj+Jh3a7tLmGFG4s9Q605GokynuVVppF+0VmT5dhsFqPbql/qZs4S8gM/AvZTH2JJeTjOl4YYF5b
HmDs84TivP8GFUQFjX72QJf3pEsJ99dc2Mr8GyqKJ3GsCCVmfmAOsq+BikxrCIJjV24kvKinHc8W
VoVYfB42e85QqueIM4fRzGG1yT4jlF6wr6x2qy743m1zrI3gZT4Kib4a8JdyWCwHqrVyLjVwaciw
UnkgKJRobgIwrMz36EgM/IEk7ZIPlsD95KcVTNjCzvHEgZ4w3jjMXIKazLIzyJ/gJPEcIrzYlwG1
rdxBsvP4NItp3b8K6AXQkPL8EeJMpMMy4ZvgGPBVjiBoNKEpn1PcD7GBFMYDbcrCNmW50D0Q8ylO
3OSdKUz4TwLHqjVpzEuOEZRh6n4AYU4jC4mK3Cyx/tYIZQa7qC/5MEk2ejFVTl8g8ij8olJsAKRM
/dHsnuHw9pwYmzfs9IydY/dmf9UmmW2ez3kR35yDa00QPFBxTpma+czj3NyjiiYhwzKHps8toqh2
if0I7asEEtKOnL6MZyxtcdukB1r0fupxZq5NPxPvt2B7Ipj3y+EWq5dJC/plexN4g94VSkG5pnXW
sUHHNd/aKfEzoZY74ISRN2nNSjwfDIThc24dbteybI/ce3+Pt+7AeyrsOl0eLMkrzqi4Rn0Uu2b9
897OmzxuHQMj/uXf10HISvfWPZavyN1WDPVy8mnHLDZdNqa5KNvC5tgoM1CoHf72O0O6JzuQUza+
Bldbej9hYE9RtxlRUw9qXKay05rmlZuKxVtmXiSsHG4GyJnljuNHlVh5u3C2GGgx9EjIagiK7bMx
T1HeskMsd6GuIlUEZ214D9qnoB5yVUplpYpe2gsiAjVHCGE4BMf7Wc/lChv/J+lE9HEDsvy9kNIu
p3x8AYVrx9gIcC07Ug7NlT6AhITOudiZ1ICRXjwj2BIKE+UM/ko5Qu3RvDNx9wVVqrD9+XniTNd4
cHtZIYjS9cgcGbp3Foe9pstMKVrbcJmYegv6ZdwBnd0FyntjRiIFBv7R+CQ0bK3x8YCpPdZ5kny3
SXZg+zrmnVfms84WKBq8kHZR6OeXJTGydBXwgg3bKRN3unA8FvEOIis87fe9LH780hQK66vq3RRT
R5u10sy+lBXKCEw0nWF2QRcDhghA8MAdIshVGkjbM6JaCErcwTb/iZUwlqBC+KX99GT0Xaonm5l8
1l6xRpLAykJxZog0/t+zG9GIFTtqJ7ITBvl3ONcqtiKAxJtbejXhWS+aB0PEbLlGx8ROI3S3yXPF
MUNtySLBHDd8gteVSeoS1ICPBbgPQJcBEJsxICEGb+efb812Oj4pCxNtqkRreUquuK9W7AUmINQR
dQB1qTzZF0Cwd4LqskHKDHVLRthhCKwhf8P9QIX9BGSJECRuQ0V03oDTfU1rKdlkIYybiFUAiGyC
iDJl5c2cQy1vi4U1LvjMPWBH1Rm6jVXHpjVTorhvM3O08nbBS69wwG+ywkOwUTyoWxEnKSPWgBSX
KRfW3AG/Y09R31cZADhUcUwKv5W9sTcRnc6qAwHikp3nUGI1zxjPlhQdg/J970pnMWSnRQLftE9z
RHcWegrnJIov+CR02L65iTUPJGV3dZr48KNqVm/tXTPS2ak6y79E4l6Cz3HkFTCTaFGAG5oYI9JS
nKYAGGOrZmfSxsGTFbZk22Vc85POUv6Fa/3zcF8BMd0gTeO/CdIeNHR55v//WjiHGknHwVGQYGCg
hzM9fW+bmMszaEvqOy5OIBZIGMNAmgxdfDKKEkJ4DdDx2LE8gEGlnctWMQM9xAjmk7WlTWeNZeYW
2hEZXeyf/GU9eGVItFplQdyeAWLH22v/LPm+uGZoE00SKuXVJXi2LxhPK/zHGoJkAQ0YnniGa6d5
oHJPTNgcWmxbqRbtmUnR9Jugg0dXP4tr4xmQR1inhHctDRoAllPNxsx6MHx5Pe9pr4lFurLdgFTx
IheoBfFshI4YEbs/1Wew1J5cUqEU1YwKx//tgv1GsVra6+Die2RwvQmPn7BH0rj9NDfnS44AgSZW
Gi3qssYoioiar9xQzaMAU58RNujGmBo2YGhQ6HusOrZX67b5jhMo0uBMPqlbYpEXJqLtAxCkeAyX
jFbyThXlhcHOhrLnrg4lgZtvm5Er1m+FLVBlZKTB3rt94O/0ZS98rXfO6Ld/odk/yBPq7R0O3u5O
Xa5p2vTLv42HZaPiF1epSRFo91hE0YpwUB16SI6bA7LBO1LqMBJoqeezqf2N5VHrwWVlGijHODW5
E3rvJcyJMpjQP28qtgeLq2rruVxkAdkmcXtQWa35QoNIROaI0N+KT/UmLwfMe4s5ay68wV2y3SZT
FC0VKwXd7GwymitAUsYSjhyNCbi1QpCD+DnUxskaZUp0UpqeR6Uolz7ihARLsuwFCUzTMzAW6Mwl
wAnkqRXaKtW5g48260z6NxXsSB6hdzDHGWaldnzwleBVgVh9TIq0+yxi59dWnb/ZNpTupg5ledv7
X+DSqXE5AyvGzIQQtyIHsKbUZkjQ9G4FJJJ57QZ/moizgPc1p3CUqyJ6XKojipQF+Q/xQG7zcJSL
7F9IYOdeZhBvNQqP6Ns1gIbQhQqHGnqOCBLclukFi7xfxUxvSvcSQ3nA7T3A8Sw5famDuKZZ19ZM
3VqG2e8wx58rsUAFeQxG0dft5Vw8xDw5jVynz+3OpHPBCkgVTr4N7SJgAWtk1BuTvJkKi2hXimpu
ZA0KQffLFQP7q2VcXBkG4zrwkHByFCqnSVnUv8mG0Ws2v21Saw8W9NewvKSUJiWJAlOyamcb92ir
hDhI9uK2RcopyFuaXznHHhkEBw7GlxZuagf67lTkVxJG6vd8iEx3wDb8c00dcGb8PrJlfSQzYAZY
bn8So6+A+6iUL3bt7W0OuInM5FPOjvd4QospskIXViHtPgcwxua+LNQjTXsCbVIFrLh8MUhFaMR/
3c29nR21CHKKcpYk3cBADl6JAbyVUymXSVRD29lDGyFH7rND2xaTPs5koGGuT1eLIWkimMeNKpMv
EkZMnt4SqeoVnC28IlucJMQLSsMhTHwJptd0EN9alyC7xJGu4Ak4nLrIjkgjy/dGYwsXgzim8r/V
+gvYa35df5CTkSG8e6+/WqglsFzYGGJRlSpo+jxQeM2IHhiws9bn+D5yirrTxF/MDv8w8VssBTe3
XG2fg422VL8DKZZekKsLH+KVsdNpMG1qkhRp979BdKXDYm5j6IQaxNUAuUXfTnFFGkmaQNdtmsDv
43C72IdoYnjL4i9sd/95McVEYANHjSFQBNDcO+q7evlxrBJH9hlMt6WnqhcZ4XC4vCgqBfvSUyel
wRStSCox/H9SPd7qNKppePjKQBbslYG7zS4G7eVc6tXwnj8Mv23IaJftNxK80t2qevS/XKJOUDig
3JlC2Ft3bCwllxVdYfzxpFWCSLqzJbW5dit2bqZQZ+5zrTdv7if/bCZUnqa/LbddqU6MhuC7We51
DNgOvY9+9foLelrDfTf8sNxeaCRUcQh84k9N66AM3fnCbu51dT0VV51aAmdf4E+4d6XR2mj31sOL
4drQiEmKaqTcx3ep6+pnjriWvwauWyECXFFJKjIS+sWE/Rap/BFvXShnXipPoxQqORtB24ZhF+V1
QB4um37l3NakzurYIj/U0RRvOo/A5HGTIVhDvhLhQTOR1N5mFWQPCSFcvqL8K8PZkDkzxTYb5vHn
wEbf3ZeFPn+bCoR1oEAm4Of51+2pJcvKq5LSSWIwtfOVet0nMk8pHBMyRBnkNRK69ifjEYYxgswq
MPepmR52OrpXg/E3ZHoNeYs5IjwKRPgDLPGFpz45UXF4Q9VECPliXgzdq+M2Jl2+qj+9N/DTNO6v
Lx3mCdDUSxOvoFYn1epDHFj/+wMMnpaY2cAUZyVFFTS9DY90cn5A9EeQqc3uaSYf/+G1WGlVQ5U4
A693voabGEeBP4BgSB3ThOD+0mBZyE9WJK+5ElNitG+rNzd1ouZ9u3Pwu6idCHNdhwIIRrGOLSvw
DFH+aaPFFv1KjBUmHRInIf/jo0E4wr3s5QSqn+ceiVpe1S7FGEBoRpOKfBxOSOOoB5dBxqU36tav
0zfa8TviDkZtpsvGXP/tlxm3et3sUU3VH4Qar8kC9V+dkLNYyeS5+eHGjgrUgPEZgqxS8U63663X
nt/8gegsie3eDExUijPrpF59v2js0CBRZht3ZPGAaPbJvMiDRabWc3WYL1hNqu9+37f1/eWrsIky
lbr04DaqRptrG62eYqie7dgLO8k+W6g3t8kyDb5pz1eBEPHVDSM5LWSHrt07o4fkBw60FIyYxrNo
tsKIms1sc6/Es/WEy6Kab8cJ8thZN7lZCMiLjsUFivGHQnUNrXkpCC29sWkCaWOxnZ0YnjQfkhr4
Q/6gQm6pdCosNGk8uZkpA8ObtABXP9eO8XwuYwaHimFRIsokd4p+frBomZ0iklOlsFoQukvvcZ5a
dM0k8YoWf9HSRMVrf3DwgdAgW2oQjVpDqgJdDYUOapecMkEXsRP5FuvEa0VPejskQ21SUqWPRet8
P7qPOobyAgpZ8U1I+5jIXSAKytPNZiCCGQ1LaabK4gmVtsyTInDNlBTt2j7C8mxP6lRq1wXKtNVl
zWDfyvcNhnIRS1LEYiTOp+rAa+jbEiISyR4k69QvMc4VzEUksm/4xMvmaMIUgmK8lZVDXrWmWaLk
br1Q47TTh2BnAcLCaChbZtchTWdIdeEFJHZb1TLHMGWmOExaKsk3jl/GrN2lAj30oVJC6h0cGGF8
BLlICsGqCA0il3xqH6ROP+iGTAljoCfyx+hB9YMok/ZH5WqeyIGdXRqr+6nFzC0N1M7be0ZMSSSj
ts5h8XNIMZGkyE6QybMsOg3gpz084VOumGM82Bv6wFluPcpHnPLT+nbz4c4KttnZ2frbFYbIBMnw
uUHww4Uy4s00O73znZIqlVY52WEBAs/aM2V9jKV58SXKNfLU2SByxAsfRjTNRVG70G+Jmk3EKNhm
Aeh7gpe49uDBX1JpaqqwaB7iA2Eup12vC5lVTFzhI/nJPUb0S2BlAvXdXGOKOHQ/Jjcp33yXJEBH
MnWsWwXVyMM+uWz+zr9adHn/V0oCFFfTIBVnxanVZ4+2/+3fu9Z9MQRT0kFSwrpTapA6X6N+CJY2
KW3DOka4HzuP1YGcozWdjlgqT295zp1dFHWArEo49SboV2bmEtJ8yYSfMjHaDJSDpeZfDBCzi4Mu
rwqqPcsKcU2qI97wOzk+CjQFYIKQ5Y2jJHL7Ujk3LkDSwzVCMrirfenmijVANpU3p+0IZ5gN61rq
7nO7KtQcgyNv1MZydd31qQR2uLXYiKyXCFeIAPNsYvFOv/yL++rgfO8kJ+XOX8UbTKFKfk4lvkSC
h7MQaslmR1Svo+Yv4L7mpI44FJmPQ0fY1KYRJ64kQgRGIxHma0R6fAUuLq7INXMakqFwbflnguAF
OibU3KopylxvpC2SEMc4pYFuPDHf3oaaVDh101NMv/Q4G7Fni6B9Rf3NrTdyvDk/xJefLlXCj818
2tiAhZ0iUW3+cAhawAbpgZXnlM0fK5lQP4LazAEDzVb4/NZxRAKxF9M0DmSSvBuvemvqhVMUXw6m
bzSnXSDmjJNuJhoO+UPg5THSlj/WjYCS+GlsZN6cmyuua/ErXU6wT9AVgywfVVqm9T1v820PePxg
ZhSdjUJy+6kgdsTJ51Aak7q85nEsB4C3OafR8+3fxZoK4zDsFRiKmG6jvuwb1QA4PWZwdBSlzZIn
ENQgrH04XXrE7RBMlTyFkvKUjP7RfgX3taDxcqZthJGgRznT2xLRj9s0e5rn8asMVPJ+UZgceStl
TNaf2MWd29P0d36wbPHBvqknS1Xb4HqoaGsWkWLc05/TliyLvlJj1M48BTXMy5802Rze4ymFdNMI
cJkTcQ2tZC6Lnt6J7FFNQR4b40UU4uknaVbyHQORkksJYlFGzvI4U85f4Dy/tkMnmS1G3o58qnnH
gAj75tHdA/NOKi930OUmNgGBh4Kl/TJzaxQRqE4D+J1/sdPeeJVteQj3RPIP5Is1BQgfG87jU4Ku
Rd4fcJDyBh9zCNQYwXcEvzlxJfVp4Zc/Ftm5Z8izEeB/P56ZDE2TKBE+NTmpIpDsMkikSZ1+o+ke
WIJ9rwFhYYZcH/943xHSJuc+YCgfjSSXEMhGIQujDsO7eEwj1hG69Ll0Uz0ww7Ieu2RSpgoOf7OM
EKCDIdLTTupDkj2Z4g1J/SM236SqS9F2sxd/vOHRRi22WhzwlbGOh+X5niSgVe1jqmZlHkYYe1wl
Ghssa4EJoJbZ9fvYo1qRfagtvYuDKp8mqKzb2thYi+hDkAQJnbIAxwEidv+0sswi6rU8G8aRbjpG
ziVasVdsdoUJhm4/c7w1W7uMWSrwm/jgkuVmyOz2fYAAsxNBNyfLyv1beolsoWgJ2weCgwISSe/Q
xFODwpaZttut70+X3rFjoVKrAuBxUMXyLjikIYYUuyCFsFtOQJ6rZkjVW1Tm8n2BhBeIMD1C6KTZ
rYKQmhctgn/CBB4pOEJJTrvbL6kJC4HdOdS8skZasboMsrRpFPhalkdZeM98v9iecJM/0JBz1CfH
a9sfAdkoXcwUbllCdjl4GHLndrmRse5iWdC19E/mtadOIWg3+40GiLnAIeBncm9xKMjSUzA9bV6Z
3lgTwO4oDzSOKBQRcMBmPj2d/zHMkxAPZxrvt8SpqAaSrTvmSpeunpoYB5sFh6zIR3JAsoB3Z6vl
sOOCm+e83INLwxn+QcKZtNVMKIi8efJE2ZFVZO7FVqfRNIpGFQxVOoIruRe4s7Bvvw9K6c4qpkeW
O7FONe9KGcFPGOm+7Evf7Hg3Yi/s+NznhYTUeOnN4aYKK+VHBJ4hyZun5Ho8OuWUTPxvDJRCYP3F
zF1Ots/ZlC2IEtP1DkmiSI36fRXCDf+OAtsaPSgD1kSUCn41o1KCpGMT+eRZo/FfGFw0VXlZLH95
ZdooIVOLWxEAlV82lC4NnmO+mWzfO0DKHp3MnaCmXNhD+LOjoGvsNC/AATJMtir9AzWvM6pLYD3n
CFrb8HOqBDMJ3Vu/aL/YDi+BcN2lmtJuhy/orui4DDaQCZijNnE8I9AEmlazcBq8XvAosu+1738v
E3rbzaGqlj8QyRRJYbLYeA04HQ0ps6PygoO8Ye8ocL/4A1CP5pZxDSr5eqTn21X/DztFR0L+uNp3
9p8FJ43h+c1HVrlEHXIyeVUNLOHXIXx1UP12Um4FvXxjdVcpvqEUFEhi1YzXOetivLCfGnNRuRjR
teGc9LEQbXS1Ajdz1LJC0WnMca05wMzQa9oBrJLqhBFnWj0kQr1lER4prwTyYIS9Ll6zkoa3E51v
R+WDajA6YBG0c9F3weHEcK6Hk3snU6VQMGHhx9EnmfE/B/PlbYqMJKYk+M68p4rxRaYtyDquj9Sa
mplialQt4oVlwEcM6cq0qDiW/cK3mwnr9k2zsvQoQcKLxvntwtf2NIRuDtAL+GhhtnynRtUl9G24
FuvGsdJniUAhloPaiESCyos/DBamxsdOMnt5IJKqxuMRsBYvbfAjYWE0JMnUmroVTOKeD8nVfMDL
3T1+lWYM4uc0DmtnihpUv7cJZI1yX9W2cFOQgsCQVs0Cl+nQeA1IubpKGjQG6INw9MLo+eZdvRMS
j5tGPo5JoRBqYcPAQom7pVVLsgvYyqEhzBSo7U5lGJUD13Pwws/H3vqRLZtCEBB6jwkibCx1JloW
f3RZ3CBs/rl9FzymgmgkLi0toUMqOYk+VF3kqOQbvd9sjM/+CpsO0cwtm71SUt20FXBh0CqnczDC
aONLitUY6kIb60sJv51VwUw9dGxGKSPA/RfkB0DCpQgp3et/ZhPd6mS+e/xAu1eXCD13f/ZEgpTI
y7xgB5VimEliORcknimM8EESwSPBwOowDlB7sJRfdNOm2GkDh80qxh5sGY5lEdiEOngHL6m++GUm
0YSzNC0wTt3kuKfbMpYjRiTYOxAyNPALkflaDTd2mxYUccf8Y+aKfNTHplwC3R8HXnUq6e41M8yR
KBc0wOugFiuhNiWcddnGEFIEstGbwEmmpnOBhDgmBWfB8nWmZzOL4pWqpmJKz0fJW8hiuTR0Q3Ki
OXdKNY+qFUC3+9Km0Hj0e8IT0r8BpomRuEGaCbWuTWpOJhN1N4tTV9ZYNCcaLINF0ePchu5gHfVT
IyKi0JnZw9hw2HiG7BcKAZW22d1qMhq+tMpsRbZjsNubpRLFW0rP82pM1AELe13qsS5FgxWzr+Rc
Vup7GU8gp3z5P6Ui+KKEWOG3MJo/13tA6SQ97EPDs4MqQ49EqXa13c+QL+TpcnDfdpWOehprFHGd
9llw0FgJj5U67RYYfyku3w==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
