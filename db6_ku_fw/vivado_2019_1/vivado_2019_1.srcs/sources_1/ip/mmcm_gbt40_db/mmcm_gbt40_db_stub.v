// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Aug  6 19:36:52 2019
// Host        : siplx16.fysik.su.se running 64-bit Fedora release 27 (Twenty Seven)
// Command     : write_verilog -force -mode synth_stub
//               /scratch/edval/PhD/TileCal/Daughterboard_rev5/dbv5_production_fw/vivado_2019_1/vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db/mmcm_gbt40_db_stub.v
// Design      : mmcm_gbt40_db
// Purpose     : Stub declaration of top-level module interface
// Device      : xcku3p-ffvb676-2LV-e
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module mmcm_gbt40_db(p_clk40_out, p_clk80_out, reset, 
  p_input_clk_stopped_out, p_clkfb_stopped_out, p_locked_out, p_clkin1_in)
/* synthesis syn_black_box black_box_pad_pin="p_clk40_out,p_clk80_out,reset,p_input_clk_stopped_out,p_clkfb_stopped_out,p_locked_out,p_clkin1_in" */;
  output p_clk40_out;
  output p_clk80_out;
  input reset;
  output p_input_clk_stopped_out;
  output p_clkfb_stopped_out;
  output p_locked_out;
  input p_clkin1_in;
endmodule
