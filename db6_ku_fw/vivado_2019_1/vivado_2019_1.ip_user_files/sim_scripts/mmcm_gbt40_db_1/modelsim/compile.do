vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xil_defaultlib
vlib modelsim_lib/msim/xpm

vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib
vmap xpm modelsim_lib/msim/xpm

vlog -work xil_defaultlib -64 -incr -sv "+incdir+../../../ipstatic" \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \

vcom -work xpm -64 -93 \
"/tools/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../ipstatic" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db_1/mmcm_gbt40_db_clk_wiz.v" \
"../../../../vivado_2019_1.srcs/sources_1/ip/mmcm_gbt40_db_1/mmcm_gbt40_db.v" \

vlog -work xil_defaultlib \
"glbl.v"

