onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib db6_gth_opt

do {wave.do}

view wave
view structure
view signals

do {db6_gth.udo}

run -all

quit -force
